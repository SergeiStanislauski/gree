/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.codec.Base64;
import net.gree.asdk.core.util.Url;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.Manifest.permission;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is native input base class.
 * @author GREE, Inc.
 *
 */
public abstract class PostingActivityBase extends Activity implements InputFilter{

  private static final String TAG = "PostingActivityBase";
  private static final int DIALOG_SPOT_ACTION = 0;
  private static final int DIALOG_LOCATION_PERMISSION = 1;
  private static final int DIALOG_LOCATION_SETTINGS = 2;
  private static final int DIALOG_LOCATION_SETTINGS_HALF_ENABLED = 3;
  
  private static final String KEY_LOCATION_PERMISSION = "location_permission";
  private static final String KEY_LOCATION_CONFIRM_NO_MORE = "location_provider_confirm_no_more";
  
  private static final int DIALOG_BUTTON_CHANGE = 0;
  private static final int DIALOG_BUTTON_DELETE = 1;
  private static final int DIALOG_BUTTON_CANCEL = 2;
  private static final int PHOTO_STACK_1 = 1;
  private static final int PHOTO_STACK_2 = 2;
  private static final int PHOTO_STACK_3 = 3;
  private static final int PHOTO_STACK_4 = 4;
  private static final int PHOTO_STACK_5 = 5;
  protected Button postButton_;
  protected Button cancelButton_;
  protected Button spotButton_;
  protected Button imageButton_;
  protected Button emojiButton_;
  protected int messageLimit_;
  protected EditText title_;
  protected EditText message_;
  protected Intent intentData_;
  protected LinearLayout spotInfoBar_;
  protected TextView spotText_;
  protected View photoStackBar_;
  protected ImageView thumbnail_;
  protected EmojiPaletteView emojiPalette_;
  protected int stackNumber_ = 1;
  protected String[] imageData_;
  protected ImageUploader imageUploader_ = null;
  protected boolean titleRequired_;
  protected boolean textRequired_;
  protected boolean photoRequired_;
  protected String spotId_; // also used as is-set flag
  protected String spotName_;
  protected int thumbSize_;
  protected String modalJsonParams_;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setUp();

    imageData_ = new String[5];

    intentData_ = getIntent();
    if (intentData_.getStringExtra("error") != null) {
      new AlertDialog.Builder(PostingActivityBase.this)
      .setMessage(intentData_.getStringExtra("error"))
      .setPositiveButton(android.R.string.ok, null)
      .show();
    }

    titleRequired_ = intentData_.getBooleanExtra("titleRequired", false);
    textRequired_ = intentData_.getBooleanExtra("textRequired", false);
    photoRequired_ =  intentData_.getBooleanExtra("photoRequired", false);

    String title_string = intentData_.getStringExtra("title");
    if (title_string != null) {
      TextView title = (TextView)findViewById(RR.id("gree_title"));
      title.setText(intentData_.getStringExtra("title"));
    }


    postButton_ = (Button) findViewById(RR.id("gree_postButton"));
    postButton_.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        //assert postStringLength_ <= messageLimit_;
        assert message_.getText().length() > 0;
        emojiPalette_.saveRecentEmojiIcon();
        if (intentData_.getStringExtra("callback") != null) {
          Intent intent = new Intent(PostingActivityBase.this, DashboardActivity.class);
          if (title_ != null) {
            intent.putExtra("title", title_.getText().toString());
          }
          intent.putExtra("text", message_.getText().toString());
          if (null != spotId_) {
            intent.putExtra("spotId", spotId_);
            intent.putExtra("spotName", spotName_);
          }
          for (int i = 0; i < imageData_.length; i++) {
            if (imageData_[i] != null) {
              DashboardStorage.putString(PostingActivityBase.this, "image" + i, imageData_[i]);
            }
          }

          intent.putExtra("callbackId", intentData_.getStringExtra("callback"));
          intent.putExtra("params", modalJsonParams_);
          setResult(RESULT_OK, intent);

          savePost(intent);
          finish();
        } else {
          setResult(RESULT_CANCELED, null);
          finish();
        }
      }
    });
    String button_label = intentData_.getStringExtra("button");
    if (button_label != null) {
      postButton_.setText(button_label);
    }

    postButton_.setEnabled(false);

    cancelButton_ = (Button) findViewById(RR.id("gree_cancelButton"));
    cancelButton_.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        cancelDialog();
      }
    });

    boolean useSpot = intentData_.getBooleanExtra("useSpot", false) && hasLocationPermission();
    spotButton_ = (Button) findViewById(RR.id("gree_spotButton"));
    if (useSpot) {
      spotButton_.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (hasLocationPermitted()) {
            if (null == spotId_) {
              openSpotList();
            } else {
              PostingActivityBase.this.showDialog(DIALOG_SPOT_ACTION);
            }
          } else {
            PostingActivityBase.this.showDialog(DIALOG_LOCATION_PERMISSION);
          }
        }
      });
    } else {
      spotButton_.setVisibility(View.GONE);
    }

    boolean usePhoto = intentData_.getBooleanExtra("usePhoto", false);
    imageButton_ = (Button) findViewById(RR.id("gree_imageButton"));
    if (usePhoto) {
      imageButton_.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          stackNumber_ = 1;
          imageUploader_.showSelectionDialog();
        }
      });
    } else {
      imageButton_.setVisibility(View.GONE);
    }

    emojiPalette_ = (EmojiPaletteView) findViewById(RR.id("gree_emoji_palette_view"));
    boolean useEmoji = intentData_.getBooleanExtra("useEmoji", false);
    emojiButton_ = (Button) findViewById(RR.id("gree_emojiButton"));
    if (useEmoji) {
      emojiButton_.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (EmojiController.getEmojiCount(getApplicationContext()) <= 0) {
            Toast.makeText(PostingActivityBase.this,
              PostingActivityBase.this.getString(RR.string("gree_posting_no_emoji_message")), Toast.LENGTH_SHORT)
              .show();
          }
          View bar = findViewById(RR.id("gree_posting_toolbar"));
          bar.setVisibility(View.GONE);
          emojiPalette_.setVisibility(View.VISIBLE);
          InputMethodManager inputMethodManager =
              (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
          inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
      });
    } else {
      emojiButton_.setVisibility(View.GONE);
    }

    View upper = findViewById(RR.id("gree_posting_layout_upper"));
    upper.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (MotionEvent.ACTION_UP == event.getAction()) {
          message_.requestFocus();
          InputMethodManager inputMethodManager =
              (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
          inputMethodManager.showSoftInput(message_, 0);
          return true;
        }
        return false;
      }
    });

    thumbnail_ = (ImageView) findViewById(RR.id("gree_thumbnail"));
    String imageUrl = intentData_.getStringExtra("image");
    if (imageUrl != null && imageUrl.startsWith("http")) {
      thumbnail_.setVisibility(View.VISIBLE);
      try {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpResponse httpResponse;
        httpResponse = client.execute(new HttpGet(imageUrl));
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          HttpEntity httpEntity = httpResponse.getEntity();
          InputStream in;
          in = httpEntity.getContent();

          thumbnail_.setImageBitmap(BitmapFactory.decodeStream(in));
          thumbnail_.invalidate();
        }

      } catch (ClientProtocolException e) {
        GLog.printStackTrace(TAG, e);
      } catch (IOException e) {
        GLog.printStackTrace(TAG, e);
      } catch (IllegalStateException e) {
        GLog.printStackTrace(TAG, e);
      }  catch (Exception e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    spotText_ = (TextView) findViewById(RR.id("gree_spot_text"));
    spotInfoBar_ = (LinearLayout) findViewById(RR.id("gree_spot_header"));
    spotInfoBar_.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        PostingActivityBase.this.showDialog(DIALOG_SPOT_ACTION);
      }
    });

    photoStackBar_ = findViewById(RR.id("gree_photo_stack_bar"));
    int photoCount = intentData_.getIntExtra("photoCount", 0);
    if (photoCount > 1) {
      photoStackBar_.setVisibility(View.VISIBLE);
      if (photoCount > 5)
        photoCount = 5;
      switch (photoCount) {
        case PHOTO_STACK_5:
          ImageView image5 = (ImageView)findViewById(RR.id("gree_photo_stack_5"));
          image5.setVisibility(View.VISIBLE);
          image5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
              stackNumber_ = PHOTO_STACK_5;
              imageUploader_.showSelectionDialog();
            }
          });
        case PHOTO_STACK_4:
          ImageView image4 = (ImageView)findViewById(RR.id("gree_photo_stack_4"));
          image4.setVisibility(View.VISIBLE);
          image4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
              stackNumber_ = PHOTO_STACK_4;
              imageUploader_.showSelectionDialog();
            }
          });
        case PHOTO_STACK_3:
          ImageView image3 = (ImageView)findViewById(RR.id("gree_photo_stack_3"));
          image3.setVisibility(View.VISIBLE);
          image3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
              stackNumber_ = PHOTO_STACK_3;
              imageUploader_.showSelectionDialog();
            }
          });
        case PHOTO_STACK_2:
          ImageView image2 = (ImageView)findViewById(RR.id("gree_photo_stack_2"));
          image2.setVisibility(View.VISIBLE);
          image2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
              stackNumber_ = PHOTO_STACK_2;
              imageUploader_.showSelectionDialog();
            }
          });
          ImageView image1 = (ImageView)findViewById(RR.id("gree_photo_stack_1"));
          image1.setVisibility(View.VISIBLE);
          image1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
              stackNumber_ = PHOTO_STACK_1;
              imageUploader_.showSelectionDialog();
            }
          });
      }
    }

    Resources r = getResources();
    thumbSize_ = r.getInteger(RR.integer("gree_post_thumbnail_image_size"));

    imageUploader_ = new ImageUploader(this, new ImageUploader.ImageUploaderCallback() {
      @Override
      public void callback(ImageUploader.ImageData data) {
        setImageElement(data.mBase64, data.mBitmap, stackNumber_);
      }
    });

    for (int i = 0; i < 5; i++) {
      setDefaultImage(intentData_.getStringExtra("image" + i), i + 1);
    }

  }

  /**
   * Saves post data
   * @param intent intent sent to dashboard, with whole data to post
   */
  protected void savePost(Intent intent) {
    String prefName = intent.getStringExtra("callbackId");
    SharedPreferences pref = getSharedPreferences(prefName, MODE_PRIVATE);
    Editor edit = pref.edit();
    Bundle extras = intent.getExtras();

    edit.putString("title", extras.getString("title"));
    edit.putString("text", extras.getString("text"));

    /// image savin may not need?
    // OnClick saves images to DashboardStorage
    for (int i = 0; i < imageData_.length; ++i) {
      edit.putString("image" + i, imageData_[i]);
    }
    edit.putString("spotId", extras.getString("spotId"));
    edit.putString("spotName", extras.getString("spotName"));
    edit.putString("params", extras.getString("params"));
    edit.putBoolean("isFailure", false);

    edit.commit();
  }

  /**
   * Restores post data if marked as post is failed
   * @param intent
   */
  protected void processSavedPost(Intent intent) {
    String prefName = intent.getStringExtra("callback");
    SharedPreferences pref = getSharedPreferences(prefName, MODE_PRIVATE);

    if (pref.getBoolean("isFailed", false)) {
      message_.setText(pref.getString("text", ""));
      for (int i = 0; i < imageData_.length; ++i) {
        imageData_[i] = pref.getString("image" + i, null);
        if (imageData_[i] != null) {
          try {
            setImageElement(imageData_[i], base64ToBitmap(imageData_[i]), i);
          } catch (IOException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
      }
      spotId_ = pref.getString("spotId", null);
      spotName_ = pref.getString("spotName", null);
      modalJsonParams_ = pref.getString("params", (new JSONObject()).toString());

      if (spotId_ != null) {
        setSpotSelected();
      }
    }
    
    Editor edit = pref.edit();
    edit.clear();
    edit.commit();

  }

  private void setSpotSelected() {
    spotText_.setText(spotName_);
    spotInfoBar_.setVisibility(View.VISIBLE);
    spotButton_.setBackgroundResource(RR.drawable("gree_btn_post_spot_selected_selector"));    
  }

  private void setDefaultImage(String base64, int number) {
    if (base64 != null) {
      try {
        byte[] imageAsBytes = Base64.decode(base64.getBytes());
        setImageElement(base64, BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length), number);
      } catch (IOException e) {
        GLog.printStackTrace(TAG, e);
      }
    }
  }

  /**
   * post process of image discarding
   * check whether posting is enable or not
   */
  protected void afterImageDiscarded() {
    photoStackBar_.setVisibility(View.GONE);

    Editable messageEditable = message_.getText();
    postButton_.setEnabled(messageEditable.toString().length() > 0);
  }

  /**
   * post process of image selecting
   * enable posting
   */
  protected void afterImageSelected() {
    postButton_.setEnabled(isPostEnableState());
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Resources res = getResources();
    if (resultCode == RESULT_OK) {
      if (requestCode == res.getInteger(RR.integer("gree_request_code_get_image"))) {
        imageUploader_.uploadUri(null, data);
      } else if (requestCode == res.getInteger(RR.integer("gree_request_code_capture_image"))) {
        imageUploader_.uploadImage(null, data);
      } else if (requestCode == res.getInteger(RR.integer("gree_request_code_get_spot_info"))) {
        modalJsonParams_ = data.getStringExtra("params");

        if ((null != spotInfoBar_) && (null != spotButton_)) {
          spotId_ = data.getStringExtra("spotId");
          if (null != spotId_) {
            spotName_ = data.getStringExtra("spotName");
            setSpotSelected();
          } else {
            spotInfoBar_.setVisibility(View.GONE);
            spotButton_.setBackgroundResource(RR.drawable("gree_btn_post_spot_default_selector"));
          }
        }
        postButton_.setEnabled(isPostEnableState());
      } else {
        super.onActivityResult(requestCode, resultCode, data);
      }
    }
  }

  @Override
  public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
    if (source.toString().matches("\\n")) {
      return "";
    } else {
      return source;
    }
  }

  /**
   * confirm to discard posting
   */
  protected void cancelDialog() {
    emojiPalette_.saveRecentEmojiIcon();
    new AlertDialog.Builder(this)
    .setTitle(android.R.string.dialog_alert_title)
    .setMessage(RR.string("gree_posting_cancel_dialog_message"))
    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        setResult(RESULT_CANCELED, null);
        finish();
      }
    }).setNegativeButton(RR.string("gree_button_cancel"), null).show();
  }

  /**
   * @return true if post is enable
   */
  protected boolean isPostEnableState() {
    boolean hasMessage = ((null != message_) && (message_.getText().length() > 0));
    boolean hasTitle = ((null != title_) && (title_.getText().length() > 0));
    boolean hasPhoto = ((null != photoStackBar_) && (photoStackBar_.getVisibility() == View.VISIBLE));
    boolean hasSpot = ((null != spotInfoBar_) && (spotInfoBar_.getVisibility() == View.VISIBLE));
    if ((textRequired_ && !hasMessage) ||
        (titleRequired_ && !hasTitle) ||
        (photoRequired_ && !hasPhoto)) {
      return false;
    }

    return (hasMessage || hasTitle || hasPhoto || hasSpot);
  }

  /**
   * open spot list "modal dialog" (implemented by Activity)
   */
  private void openSpotList() {
    // Parameter check
    String viewName = intentData_.getStringExtra("spotView");
    if ((null == viewName) || (viewName.length() == 0)) {
      return;
    }

    // Check Location service status
    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    List<String> enabledProviders = lm.getProviders(true);
    boolean networkProviderEnabled = false;
    boolean gpsProviderEnabled = false;
    for (String provider : enabledProviders) {
      if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
        networkProviderEnabled = true;
      } else if (provider.equals(LocationManager.GPS_PROVIDER)) {
        gpsProviderEnabled = true;
      }
    }

    if (!networkProviderEnabled && !gpsProviderEnabled) {
      // Open settings dialog
      this.showDialog(DIALOG_LOCATION_SETTINGS);
      return;
    } else if (!networkProviderEnabled || !gpsProviderEnabled) {
      if (!DashboardStorage.getBoolean(this, KEY_LOCATION_CONFIRM_NO_MORE, false)) {
        this.showDialog(DIALOG_LOCATION_SETTINGS_HALF_ENABLED);
        return;
      }
    }

    openModalActivity();
  }
  
  private void openModalActivity() {
    String viewName = intentData_.getStringExtra("spotView");
    Resources r = getResources();
    Intent intent = new Intent(PostingActivityBase.this, ModalActivity.class);
    intent.putExtra("view", viewName);
    intent.putExtra("title", r.getString(RR.string("gree_location_spot_list_title")));
    intent.putExtra(ModalActivity.EXTRA_BASE_URL, Url.getSnsUrl());
    intent.putExtra(ModalActivity.EXTRA_TYPE, ModalActivity.TYPE_LOCATION);
    startActivityForResult(intent, r.getInteger(RR.integer("gree_request_code_get_spot_info")));
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    Dialog dialog = null;
    switch (id) {
      case DIALOG_SPOT_ACTION:
        dialog = createSpotActionDialog();
        break;
      case DIALOG_LOCATION_PERMISSION:
        dialog = createLocationPermissionDialog();
        break;
      case DIALOG_LOCATION_SETTINGS:
        dialog = createLocationSettingsDialog();
      case DIALOG_LOCATION_SETTINGS_HALF_ENABLED:
        dialog = createLocationSettingsHalfEnabledDialog();
      default:
        break;
    }

    return dialog;
  }

  /**
   * check conditions related to location features,
   * then switch which dialog to show
   * @return Dialog to show
   */
  private Dialog createSpotActionDialog() {
    String[] items = {
                      getString(RR.string("gree_button_location_change")),
                      getString(RR.string("gree_button_location_delete")),
                      getString(RR.string("gree_button_cancel"))
    };

    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DIALOG_BUTTON_CHANGE: // change
            dialog.dismiss();
            openSpotList();
            break;
          case DIALOG_BUTTON_DELETE: // delete
            spotId_ = null;
            spotName_ = null;
            spotInfoBar_.setVisibility(View.GONE);
            spotButton_.setBackgroundResource(RR.drawable("gree_btn_post_spot_default_selector"));
            postButton_.setEnabled(isPostEnableState());
            dialog.dismiss();
            break;
          case DIALOG_BUTTON_CANCEL: // cancel
            dialog.dismiss();
            break;
          default:
            break;
        }
      }
    };

    AlertDialog.Builder builder = new AlertDialog.Builder(this).setItems(items, listener);
    builder.setTitle(RR.string("gree_location_dialog_title"));
    return builder.create();
  }

  /**
   * ask user to access locations
   * @return Dialog to show
   */
  private Dialog createLocationPermissionDialog() {
    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DialogInterface.BUTTON_POSITIVE:
            DashboardStorage.putBoolean(PostingActivityBase.this, KEY_LOCATION_PERMISSION, true);
            dialog.dismiss();
            openSpotList();
            break;
          case DialogInterface.BUTTON_NEGATIVE:
            dialog.dismiss();
            break;
          default:
            break;
        }
      }
    };

    String mainText = getResources().getString(RR.string("gree_location_permission_main_message"));
    String subText = getResources().getString(RR.string("gree_location_common_sub_message"));
    String message = mainText + System.getProperty("line.separator") + subText;

    AlertDialog.Builder builder = new AlertDialog.Builder(this)
    .setMessage(message)
    .setCancelable(true)
    .setPositiveButton(RR.string("gree_button_yes"), listener)
    .setNegativeButton(RR.string("gree_button_no"), listener);
    return builder.create();
  }

  /**
   * introduce to system location setting
   * @return Dialog to show
   */
  private Dialog createLocationSettingsDialog() {
    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DialogInterface.BUTTON_POSITIVE:
            dialog.dismiss();
            try {
              startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            } catch (final ActivityNotFoundException e) {
              GLog.printStackTrace(TAG, e);
            }
            break;
          case DialogInterface.BUTTON_NEGATIVE:
            dialog.dismiss();
            break;
          default:
            break;
        }
      }
    };

    String mainText = getResources().getString(RR.string("gree_location_settings_main_message"));
    String subText = getResources().getString(RR.string("gree_location_common_sub_message"));
    String message = mainText + System.getProperty("line.separator") + subText;

    AlertDialog.Builder builder = new AlertDialog.Builder(this)
    .setMessage(message)
    .setCancelable(true)
    .setPositiveButton(RR.string("gree_button_settings"), listener)
    .setNegativeButton(RR.string("gree_button_cancel"), listener);
    return builder.create();
  }
  
  private Dialog createLocationSettingsHalfEnabledDialog() {
    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    final View layout = inflater.inflate(RR.layout("gree_location_dialog_settings_both"), null);
    
    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DialogInterface.BUTTON_POSITIVE:
            dialog.dismiss();
            try {
              startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            } catch (final ActivityNotFoundException e) {
              GLog.printStackTrace(TAG, e);
            }
            break;
            
          case DialogInterface.BUTTON_NEGATIVE:
            openModalActivity();
            dialog.dismiss();
            break;
        }
        
        CheckBox chk = (CheckBox) layout.findViewById(RR.id("gree_location_no_more_confirm"));
        DashboardStorage.putBoolean(PostingActivityBase.this, KEY_LOCATION_CONFIRM_NO_MORE, chk.isChecked());
      }
    };
    
    
    AlertDialog.Builder builder = new AlertDialog.Builder(this)
    .setView(layout)
    .setCancelable(true)
    .setPositiveButton(RR.string("gree_button_settings"), listener)
    .setNegativeButton(RR.string("gree_button_skip"), listener);
    return builder.create();
  }

  /**
   * User's permission about location
   * @return true if the user already permitted this app to access location
   */
  public boolean hasLocationPermitted() {
    return DashboardStorage.getBoolean(this, KEY_LOCATION_PERMISSION, false);
  }

  abstract protected void setUp();

  /**
   * processes bitmap and set into specified thumbnail position
   * @param base64 Base64 encoded image
   * @param bitmap bitmap to be set
   * @param number thumbnail number to be set
   */
  private void setImageElement(String base64, Bitmap bitmap, int number) {
    photoStackBar_.setVisibility(View.VISIBLE);
    View photoButton = findViewById(RR.id("gree_imageButton"));
    photoButton.setVisibility(View.GONE);

    final ImageView image;
    switch(number) {
      case 5:
        image = (ImageView)findViewById(RR.id("gree_photo_stack_5"));
        imageData_[4] = base64;
        break;
      case 4:
        image = (ImageView)findViewById(RR.id("gree_photo_stack_4"));
        imageData_[3] = base64;
        break;
      case 3:
        image = (ImageView)findViewById(RR.id("gree_photo_stack_3"));
        imageData_[2] = base64;
        break;
      case 2:
        image = (ImageView)findViewById(RR.id("gree_photo_stack_2"));
        imageData_[1] = base64;
        break;
      default:
        image = (ImageView)findViewById(RR.id("gree_photo_stack_1"));
        imageData_[0] = base64;
        break;
    }
    Bitmap portered = porterDuffImage(bitmap);

    image.setVisibility(View.VISIBLE);
    image.setImageBitmap(portered);
    image.setBackgroundDrawable(null);
    image.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        new AlertDialog.Builder(PostingActivityBase.this)
        .setTitle(android.R.string.dialog_alert_title)
        .setMessage(RR.string("gree_posting_discard_image_message"))
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            image.setImageResource(RR.drawable("gree_image_stack"));
            image.setVisibility(View.GONE);
            imageData_[stackNumber_ - 1] = null;

            View photoButton = findViewById(RR.id("gree_imageButton"));
            photoButton.setVisibility(View.VISIBLE);

            afterImageDiscarded();
          }
        }).setNegativeButton(RR.id("gree_cancelButton"), null).show();
      }
    });

    afterImageSelected();
  }

  /**
   * create corner-rounded square bitmap image shown as thumbnail
   * @param bmp input bitmap
   * @return processed bitmap
   */
  protected Bitmap porterDuffImage(Bitmap bmp) {
    Bitmap clipArea = Bitmap.createBitmap(thumbSize_, thumbSize_, Bitmap.Config.ARGB_8888);
    Canvas c = new Canvas(clipArea);
    c.drawRoundRect(new RectF(0, 0, thumbSize_, thumbSize_), 10, 10, new Paint(Paint.ANTI_ALIAS_FLAG));

    Bitmap newImage = Bitmap.createBitmap(thumbSize_, thumbSize_, Bitmap.Config.ARGB_8888);

    Canvas canvas = new Canvas(newImage);
    Paint paint = new Paint();
    canvas.drawBitmap(clipArea, 0, 0, paint);

    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

    int srcWidth = bmp.getWidth();
    int srcHeight = bmp.getHeight();
    if (srcWidth >= srcHeight) {
      canvas.drawBitmap(bmp, new Rect((srcWidth-srcHeight)/2, 0, (srcWidth + srcHeight)/2, srcHeight), 
        new Rect(0, 0, thumbSize_, thumbSize_), paint);    
    } else {
      canvas.drawBitmap(bmp, new Rect(0, (srcHeight - srcWidth) / 2, srcWidth, (srcHeight + srcWidth) / 2), 
        new Rect(0, 0, thumbSize_, thumbSize_), paint);    
    }
    return newImage;
  }

  /**
   * convert from emoji tag to image
   * @param context
   * @param message user input
   */
  protected static void convertEmojiText(Context context, EditText message) {
    Pattern p = Pattern.compile("<emoji +id ?=\"(\\d{1,3})\">");
    Matcher m = p.matcher(message.getText().toString());
    SpannableStringBuilder sb = (SpannableStringBuilder) message.getText();
    while (m.find()) {
      int index = m.start();
      int id = Integer.parseInt(m.group(1));
      String emojiCode = "<emoji id=\"" + id + "\">";
      Bitmap src = EmojiController.getEmoji(id);
      Matrix mtx = new Matrix();
      final float scale = context.getResources().getDisplayMetrics().density;
      mtx.postScale(scale, scale);
      Bitmap rst = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), mtx, true); 
      src.recycle();
      sb.setSpan(new ImageSpan(context, rst), index, index + emojiCode.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
    message.setText(sb);
    message.invalidate();
  }

  /**
   * Android application permission about location
   * @return true if this app has ACCESS_FINE_LOCATION permission
   */
  protected boolean hasLocationPermission() {
    PackageManager pm = getPackageManager();
    return pm.checkPermission(permission.ACCESS_FINE_LOCATION, getPackageName()) == PackageManager.PERMISSION_GRANTED;
  }


  private Bitmap base64ToBitmap(String base64) throws IOException {
    byte[] data = Base64.decode(base64.getBytes());
    return BitmapFactory.decodeByteArray(data, 0, data.length);
  }
}
