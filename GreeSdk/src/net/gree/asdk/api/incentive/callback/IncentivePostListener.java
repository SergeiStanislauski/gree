package net.gree.asdk.api.incentive.callback;

import org.apache.http.HeaderIterator;

/**
 * The callback interface that goes with IncentiveController.post
 */
public interface IncentivePostListener {
	/**
	 * On a successful call to IncentiveController.post, this method is called. 
	 * It will be passed a String representation of the HTTP POST response.
	 * 
	 * @param result the HTTP POST response
	 */
  void onSuccess(String response);

  /**
   * On an unsuccessful call to IncentiveController.post, this method is called. 
   * 
   * @param responseCode the response code received from the server
   * @param headers a type-safe iterator for Header objects.
   * @param response a String representation of the response from the server.
   */
  void onFailure(int responseCode, HeaderIterator headers, String response);
}
