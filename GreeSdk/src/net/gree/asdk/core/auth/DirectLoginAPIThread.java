package net.gree.asdk.core.auth;

import android.net.Uri;
import net.gree.asdk.core.Injector;

public class DirectLoginAPIThread implements Runnable{
  Uri mUri;
  private static OAuthNormalDao sAuthorizer;
  
  public DirectLoginAPIThread(Uri url){
    mUri = url;
  }

  @Override
  public void run() {
    getAuthToken().retrieveAccessToken(mUri);   
  }
  
  private OAuthNormalDao getAuthToken() {
    if (null == sAuthorizer) {
      sAuthorizer = Injector.getInstance(OAuthNormalDao.class);
    }
    return sAuthorizer;
  }

}
