/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample;

import android.os.Bundle;
import android.util.Log;
import android.widget.*;

import net.gree.asdk.api.GreePlatform;

public class AboutActivity extends BaseActivity {
  private static final String TAG = "AboutActivity";

  @Override
  public void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(R.layout.about_page);
    
    setInfo();
  }

  /**
   * Fill the about page TextViews with information about the sdk and app
   */
  private void setInfo(){
    ((TextView) findViewById(R.id.about_sdk_version)).setText(GreePlatform.getSdkVersion());
    ((TextView) findViewById(R.id.about_sdk_build)).setText(GreePlatform.getSdkBuild());
    ((TextView) findViewById(R.id.about_app_version)).setText(GreePlatform.getAppVersion());
  }
  
  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }
  
  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpBackButton();
    setUpAutoLoadMore();
  }
  
  @Override
  protected void sync(boolean fromStart) {
   

  }

}
