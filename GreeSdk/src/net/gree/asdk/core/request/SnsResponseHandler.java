/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import org.apache.http.HeaderIterator;

public class SnsResponseHandler extends ResponseHandler<String> {

  public SnsResponseHandler(OnResponseCallback<String> listener) {
    super(listener);
  }

  @Override
  protected void onFailure(int responseCode, HeaderIterator headers, String response, String reason) {
    if (mListener != null) {
      // this override is made because sns api's requirement
      mListener.onFailure(responseCode, headers, "" + reason + ":" + response);
    }
  }

}
