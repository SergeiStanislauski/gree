/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.registtrack;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.auth.AuthorizeContext;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

public class RegistTrack {
  private static final String TAG = RegistTrack.class.getSimpleName();
  private String mUserKey;

  public RegistTrack() {
    mUserKey = AuthorizeContext.getUserKey();
  }

  /**
   * Interface for listening to results for updating profile
   */
  public interface RegistTrackListener {
    public void onSuccess();
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Trigger the request to get regcode and entcode
   * @param action action name
   * @param listener the listener to follow the request
   */
  public void request(String action, RegistTrackListener listener) {
    request(action, null, null, listener);
  }

  /**
   * Trigger the request to get regcode and entcode in case of upgrade
   * @param action action name
   * @param regcode 
   * @param entcode
   * @param listener the listener to follow the request
   */
  public void request(String action, String regcode, String entcode, final RegistTrackListener listener) {
    JSONObject params = new JSONObject();
    try {
      params.put("action_name", action);
      params.put("context", mUserKey);
      if (regcode != null) {
        params.put("reg_code", regcode);
      }
      if (entcode != null) {
        params.put("ent_code", entcode);
      }
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(0, null, e.getMessage());
        return;
      }
    }
    new JsonClient().http(Url.getLogRegisttrack(), BaseClient.METHOD_POST, params.toString(), false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onSuccess();
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
}
