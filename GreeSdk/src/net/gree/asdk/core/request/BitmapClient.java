/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import java.util.Map;

import android.graphics.Bitmap;

import net.gree.asdk.core.request.Constants.OAUTH_TYPE;
import net.gree.asdk.core.request.helper.MethodHelper;


public class BitmapClient {

  public void oauth(String url, String method, Map<String, String> headers, boolean sync,
      OnResponseCallback<Bitmap> listener) {
    BitmapConverter converter = new BitmapConverter();
    ResponseHandler<Bitmap> handler = new ResponseHandler<Bitmap>(listener);
    new GreeRequest(url, MethodHelper.parseString(method)).header(headers).request(converter,
        handler);

  }

  public void oauth(String url, int methodGet, Object headers, boolean sync,
      OnResponseCallback<Bitmap> listener) {
    BitmapConverter converter = new BitmapConverter();
    ResponseHandler<Bitmap> handler = new ResponseHandler<Bitmap>(listener);
    new GreeRequest(url, MethodHelper.parseInt(methodGet)).request(converter, handler);

  }

  public void oauthForceSync(String url, int methodGet, Object headers,
      OnResponseCallback<Bitmap> listener) {
    BitmapConverter converter = new BitmapConverter();
    ResponseHandler<Bitmap> handler = new ResponseHandler<Bitmap>(listener);
    new GreeRequest(url, MethodHelper.parseInt(methodGet)).sync(true).request(converter, handler);
  }

  public void oauthForceSyncNoneAuth(String url, int methodGet, Object headers,
      OnResponseCallback<Bitmap> listener) {
    BitmapConverter converter = new BitmapConverter();
    OAUTH_TYPE auth = OAUTH_TYPE._NONE;
    ResponseHandler<Bitmap> handler = new ResponseHandler<Bitmap>(listener).oauth(auth);
    new GreeRequest(url, MethodHelper.parseInt(methodGet)).sync(true).oauth(auth).request(converter, handler);
  }
}
