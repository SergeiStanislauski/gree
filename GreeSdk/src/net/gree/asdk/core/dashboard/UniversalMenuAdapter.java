/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.util.ArrayList;

import org.json.JSONObject;

import net.gree.asdk.core.RR;
import net.gree.asdk.core.cache.ImageCache;
import net.gree.asdk.core.cache.ImageFetcher;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Data Management of Universal Menu.
 * @author GREE, Inc.
 *
 */
public class UniversalMenuAdapter extends BaseAdapter {
  public static final int TYPE_PROFILE = 1;
  public static final int TYPE_ITEM = 2;
  public static final int TYPE_HEADER = 3;
  public static final int TYPE_SEEMORE = 4;
  private ArrayList<BindData> mArray;
  private LayoutInflater mInflater;
  private ImageFetcher mImageFetcher;

  public UniversalMenuAdapter(Context context, ImageFetcher imageFetcher) {
    mArray = new ArrayList<BindData>();
    mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    mImageFetcher = imageFetcher;
  }

  /**
   * Data for Universal Menu
   */
  class BindData {
  }
  
  class ProfileData extends BindData {
    String mName;
    String mNickName;
    String mImage;
    int mCommandType;
    JSONObject mParams;
  }
  
  class HeaderData extends BindData{
    String mTitle;
  }
  
  class ItemData extends BindData {
    String mTitle;
    String mImage;
    int mBadge;
    int mCommandType;
    JSONObject mParams;
    boolean mIsThumbnail;
  }
  
  class SeeMoreData extends BindData {
    String mTitle;
  }

  /**
   * adding Data to Universal Menu List for Profile
   * @param name User Name
   * @param nickName User Nick name
   * @param image User Photo
   * @param command_type Command type of user click
   * @param params command's params
   */
  public void addProfile(String name, String nickName, String image, int command_type,  JSONObject params) {
    ProfileData data = new ProfileData();
    data.mName = name;
    data.mNickName = nickName;
    data.mImage = image;
    data.mCommandType = command_type;
    data.mParams = params;
    mArray.add(data);
  }

  /**
   * adding Data to Universal Menu List for Header
   * @param title header title
   */
  public void addHeader(String title) {
    HeaderData data = new HeaderData();
    data.mTitle = title;
    mArray.add(data);
  }

  /**
   * adding Data to Universal Menu for Item
   * @param title item title
   * @param image_url item thumbnail url
   * @param badge badge for update
   * @param command_type Command type of user click
   * @param params command's params
   * @param isThumbnail  whether need Thumbnail or not.
   */
  public void addItem(String title, String image_url, int badge, int command_type,  JSONObject params, boolean isThumbnail) {
    ItemData data = new ItemData();
    data.mTitle = title;
    data.mImage = image_url;
    data.mBadge = badge;
    data.mCommandType = command_type;
    data.mParams = params;
    data.mIsThumbnail = isThumbnail;
    mArray.add(data);
  }

  /**
   * adding "see more" item.
   */
  public void addSeeMore() {
    SeeMoreData data = new SeeMoreData();
    mArray.add(data);
  }
  
  /**
   * clear UniversalMenu data
   */
  public void clear() {
    mArray.clear();
  }

  @Override
  public int getCount() {
    return mArray.size();
  }

  @Override
  public Object getItem(int position) {
    return mArray.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View view;
    BindData data = (BindData)getItem(position);
    if (data instanceof HeaderData){
      if (convertView == null || convertView.getId() != RR.id("gree_universalmenu_header_root")) {
        view = mInflater.inflate(RR.layout("gree_universalmenu_header_layout"), null);
      } else {
        view = convertView;
      }
      HeaderData header = (HeaderData)data;
      TextView text = (TextView)view.findViewById(RR.id("gree_universalmenu_header_title"));
      text.setText(header.mTitle);
    } else if (data instanceof SeeMoreData){
      if (convertView == null || convertView.getId() != RR.id("gree_universalmenu_seemore_root")) {
        view = mInflater.inflate(RR.layout("gree_universalmenu_seemore_layout"), null);
      } else {
        view = convertView;
      }
    } else if (data instanceof ProfileData) {
      if (convertView == null || convertView.getId() != RR.id("gree_universalmenu_profile_root")) {
        view = mInflater.inflate(RR.layout("gree_universalmenu_profile_layout"), null);
      } else {
        view = convertView;
      }
      ProfileData profile = (ProfileData)data;
      ImageView image = (ImageView)view.findViewById(RR.id("gree_universalmenu_profile_image"));
      mImageFetcher.loadImage(profile.mImage, image, 0, 68, 68);
      TextView text = (TextView)view.findViewById(RR.id("gree_universalmenu_profile_nickname"));
      text.setText(profile.mNickName);
      TextView userName = (TextView)view.findViewById(RR.id("gree_universalmenu_profile_username"));
      userName.setText(profile.mName);
    }  else {
      if (convertView == null || convertView.getId() != RR.id("gree_universalmenu_item_root")) {
        view = mInflater.inflate(RR.layout("gree_universalmenu_item_layout"), null);
      } else {
        view = convertView;
      }
      ItemData item = (ItemData)data;
      TextView text = (TextView)view.findViewById(RR.id("gree_universalmenu_item_title"));
      text.setText(item.mTitle);
      ImageView image = (ImageView)view.findViewById(RR.id("gree_universalmenu_item_image"));
      if (item.mIsThumbnail) {
        image.setVisibility(View.VISIBLE);
        mImageFetcher.loadImage(item.mImage, image, 0, 44, 44);
      } else {
        image.setVisibility(View.GONE);
      }
      TextView badge = (TextView)view.findViewById(RR.id("gree_universalmenu_item_badge"));
      if (item.mBadge > 0) {
        badge.setVisibility(View.VISIBLE);
        badge.setText(Integer.toString(item.mBadge));
      } else {
        badge.setVisibility(View.GONE);
      }
    }

    return view;
  }

  @Override
  public boolean isEnabled(int position) {
    BindData data = (BindData)getItem(position);
    if (data instanceof HeaderData) {
      return false;
    }
    return super.isEnabled(position);
  }
}
