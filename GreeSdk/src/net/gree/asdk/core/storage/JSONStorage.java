/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.storage;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import net.gree.asdk.core.GLog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;

/**
 * JSON Database
 * 
 * @author GREE, Inc.
 * 
 */
public class JSONStorage {
  private static final String TAG = "JSONStorage";
  public static final String COLUM_ID = "_id";
  public static final String COLUM_FILTER = "_filter";
  public static final String COLUM_DATE = "_date";
  public static final String COLUM_BLOB = "_image";
  public static final String COLUM_JSON = "_json_string";
  public static final String COLUM_FILTER_SUB = "_filtersub";
  public static final String COLUM_KEY = "_key";
  public static final int COLUM_INDEX_ID = 0;
  public static final int COLUM_INDEX_FILTER = 1;
  public static final int COLUM_INDEX_DATE = 2;
  public static final int COLUM_INDEX_BLOB = 3;
  public static final int COLUM_INDEX_JSON = 4;
  public static final int COLUM_INDEX_FILTER_SUB = 5;
  public static final int COLUM_INDEX_KEY = 6;

  private static final String DB_NAME = "json_data.db";
  public static final String TABLE_NAME = "json_data";

  private DbHelper mDbHelper;
  private String[] mFilter;

  private String mCreateTableQuery;

  private ReadWriteLock mLock = new ReentrantReadWriteLock();

  /**
   * Public constructor
   * @param context
   */
  public JSONStorage(Context context) {
    mCreateTableQuery = "create table if not exists " + TABLE_NAME + "(" + COLUM_ID
        + " integer primary key autoincrement," + COLUM_FILTER + " text," + COLUM_DATE
        + " text," + COLUM_BLOB + " blob," + COLUM_JSON + " text," + COLUM_FILTER_SUB
        + " text," + COLUM_KEY + " text unique);";

    mDbHelper =
        new DbHelper(context, DB_NAME, 1, TABLE_NAME, mCreateTableQuery);
  }

  /**
   * get readable database
   * @return readable database
   */
  public SQLiteDatabase getReadableDatabase() {
    return mDbHelper.getReadableDatabase();
  }


  /**
   * get writable database
   * @return writabe database
   */
  public SQLiteDatabase getWritableDatabase() {
    return mDbHelper.getWritableDatabase();
  }

  /**
   * @deprecated
   * get loading curosr of the table
   * @param where SQL where query. '?' is expanded as selection[n]
   * @param selection wildcard contents
   * @return database cursor
   */
  public Cursor getDataFilterCursor(String where, String[] selection) {
    SQLiteDatabase db = mDbHelper.getReadableDatabase();
    GLog.i(TAG, "[GET FILTER CURSOR] where:" + where);
    for (int i = 0; i < selection.length; i++) {
      GLog.i(TAG, "[GET FILTER CURSOR] selection:" + selection[i]);
    }

    Cursor cursor = null;
    try {
      cursor = db.query(TABLE_NAME, getColums(), where, selection, null, null, null);
    } catch (SQLException e) {
      GLog.printStackTrace(TAG, e);
    }
    return cursor;
  }

  /**
   * get loading curosr of the table with limit
   * @param where SQL where query. '?' is expanded as selection[n]
   * @param selection wildcard contents
   * @param limit limit count of rows
   * @return database cursor
   */
  public Cursor getDataFilterCursor(String where, String[] selection, int limit) {
    Cursor cursor = null;
    try {
      mLock.readLock().lock();
      SQLiteDatabase db = mDbHelper.getReadableDatabase();
      GLog.i(TAG, "[GET FILTER CURSOR] where:" + where);
      for (int i = 0; i < selection.length; i++) {
        GLog.i(TAG, "[GET FILTER CURSOR] selection:" + selection[i]);
      }

      try {
        cursor = db.query(TABLE_NAME, getColums(), where, selection, null, null, null, String.valueOf(limit));
      } catch (SQLException e) {
        GLog.printStackTrace(TAG, e);
      }
    } finally {
      mLock.readLock().unlock();
    }
    return cursor;
  }

  /**
   * get loading curosr of the table with limit
   * @param where SQL where query. '?' is expanded as selection[n]
   * @param selection wildcard contents
   * @param orderBy order query
   * @param limit limit count of rows
   * @return database cursor
   */
  public Cursor getDataFilterCursor(String where, String[] selection, String orderBy, int limit) {
    Cursor cursor = null;
    SQLiteDatabase db = null;

    try {
      mLock.readLock().lock();
      db = mDbHelper.getReadableDatabase();
      GLog.i(TAG, "[GET FILTER CURSOR] where:" + where);
      for (int i = 0; i < selection.length; i++) {
        GLog.i(TAG, "[GET FILTER CURSOR] selection:" + selection[i]);
      }


      try {
        cursor = db.query(TABLE_NAME, getColums(), where, selection, null, null, orderBy, String.valueOf(limit));
      } catch (SQLException e) {
        GLog.printStackTrace(TAG, e);
      }
    } finally {
      mLock.readLock().unlock();
    }
    return cursor;
  }
  /**
   * @param cv insert data. Content is column name, Value is value of column
   * @return inserted row id
   * insert a row
   */
  public long insert(ContentValues cv) {
    long id = 0;

    try {
      mLock.writeLock().lock();
      if (cv.containsKey(COLUM_ID)) {
        GLog.e(TAG, "Culom _id is autoincrement.");
        return 0;
      }
      SQLiteDatabase db = mDbHelper.getWritableDatabase();
      id = db.insert(TABLE_NAME, null, cv);

      if (id > 0) {
        GLog.i(TAG, "[Insert] success:" + id);
      } else {
        GLog.e(TAG, "Failed to insert into " + TABLE_NAME);
      }
    } finally {
      mLock.writeLock().unlock();
    }

    return id;
  }

  /**
   * Insert a list of rows
   * @param cvs rows list
   * @return count of successfully inserted rows
   */
  public int insert(List<ContentValues> cvs) {
    int count = 0;
    SQLiteDatabase db = null;

    try {
      mLock.writeLock().lock();
      db = mDbHelper.getWritableDatabase();

      for(ContentValues cv : cvs) {
        long id = insert(cv, db);
        count += id > 0 ? 1 : 0;
      }

    } finally {
      mLock.writeLock().unlock();
    }
    return count;
  }

  private long insert(ContentValues cv, SQLiteDatabase db) {

    if (cv.containsKey(COLUM_ID)) {
      GLog.e(TAG, "Culom _id is autoincrement.");
      return 0;
    }
    long id = db.insert(TABLE_NAME, null, cv);

    if (id > 0) {
      GLog.i(TAG, "[Insert] success:" + id);
    } else {
      GLog.e(TAG, "Failed to insert into " + TABLE_NAME);
    }

    return id;    
  }

  public long insertWithCurrentDate(ContentValues cv) {
    cv.put(JSONStorage.COLUM_DATE, String.valueOf(System.currentTimeMillis()));
    return insert(cv);
  }


  /**
   * Replaces a row, use as is insert or update
   * @param cv replacing row
   * @return row id
   */
  public long replace(ContentValues cv) {
    SQLiteDatabase db = null;

    try {
      mLock.writeLock().lock();

      db = mDbHelper.getWritableDatabase();
      String key = cv.getAsString(JSONStorage.COLUM_KEY);
      String[] selection = {key};
      Cursor cursor = db.query(TABLE_NAME, getColums(),  COLUM_KEY + " = ?" , selection, null, null, null);

      if (cursor.getCount() <= 0) {
        return insert(cv);
      } else {
        cursor.moveToFirst();
        long id = cursor.getInt(COLUM_INDEX_ID);
        cv.put(COLUM_ID, id);
        long ret = db.replace(TABLE_NAME, null, cv);
        return ret;
      }
    } finally {
      mLock.writeLock().unlock();
    }
  }

  /**
   * Insert or update for rows
   * @param cvs ContentValues list to insert or update
   * @return number of successfully inserted or update rows
   */
  public int replace(List<ContentValues> cvs) {
    Map<Long, Boolean> ret = new HashMap<Long, Boolean>();
    SQLiteDatabase db = null;

    try {
      mLock.writeLock().lock();
      db = mDbHelper.getWritableDatabase();

      for (ContentValues cv : cvs) {
        String key = cv.getAsString(JSONStorage.COLUM_KEY);
        String[] selection = {key};
        Cursor cursor = db.query(TABLE_NAME, getColums(),  COLUM_KEY + " = ?" , selection, null, null, null);

        if (cursor.getCount() <= 0) {
          long id = insert(cv, db);
          ret.put(id, true);
        } else {
          cursor.moveToFirst();
          long id = cursor.getInt(COLUM_INDEX_ID);
          cv.put(COLUM_ID, id);
          long result = db.replace(TABLE_NAME, null, cv);
          ret.put(result, true);
        }    

        cursor.close();
      }
    } finally {
      mLock.writeLock().unlock();
    }


    return ret.size();
  }

  /**
   * delete row(s)
   * @param where SQL where phrase. '?' is expanded as selection[n]
   * @param selection wildcard contents
   * @return deleted count of rows
   */
  public int delete(String where, String[] selection) {
    SQLiteDatabase db = null;
    int count = 0;
    try {
      mLock.writeLock().lock();
      db = mDbHelper.getWritableDatabase();
      count = db.delete(TABLE_NAME, where, selection);
    } finally {
      mLock.writeLock().unlock();
    }

    return count;
  }

  /**
   * convert Bitmap to byte[]
   * @param bmp input
   * @param quality Q value of PNG
   * @return byte arrayed bitmap
   */
  public byte[] getByteFromBitmap(Bitmap bmp, int quality) {
    if (bmp == null) {
      return null;
    }
    byte[] ret = null;
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    bmp.compress(CompressFormat.PNG, quality, byteStream);
    ret = byteStream.toByteArray();
    return ret;
  }

  public void setSubFilterList(String filter) {
    List<String> list = new ArrayList<String>();
    Cursor c = null;
    SQLiteDatabase db = null;
    try {
      mLock.readLock().lock();

      db = mDbHelper.getReadableDatabase();
      c = db.query(TABLE_NAME, getColums(), null, null, null, null, null);
      for (int i = 0; i < c.getCount(); i++) {
        c.moveToPosition(i);
        if (filter != null && !c.getString(COLUM_INDEX_FILTER).equals(filter)) {
          continue;
        }
        String str = c.getString(COLUM_INDEX_FILTER_SUB);
        if (str.length() > 0 && !list.contains(str)) {
          GLog.i(TAG, "[Filter_List] add:" + str);
          list.add(str);
        }
      }
      mFilter = list.toArray(new String[0]);
    } finally {
      c.close();
      mLock.readLock().unlock();
    }

  }

  public String[] getSubFilterList() {
    return mFilter;
  }

  /**
   * @return colums
   * get columns list
   */
  public String[] getColums() {
    return new String[] {COLUM_ID, COLUM_FILTER, COLUM_DATE, COLUM_BLOB, COLUM_JSON,
                         COLUM_FILTER_SUB, COLUM_KEY};
  }

  public void logRecord(Cursor c) {
    for (int i = 0; i < c.getCount(); i++) {
      c.moveToPosition(i);
      GLog.i(TAG, "[RECORD] |" + c.getInt(COLUM_INDEX_ID) + "|" + c.getString(COLUM_INDEX_FILTER)
        + "|" + c.getString(COLUM_INDEX_DATE) + "|" + c.getBlob(COLUM_INDEX_BLOB).length + "|"
        + c.getString(COLUM_INDEX_JSON) + "|" + c.getString(COLUM_INDEX_FILTER_SUB) + "|" + c.getString(COLUM_INDEX_KEY) + "|");
    }
  }

  /**
   * Execute create table query
   */
  public void createTable() {
    SQLiteDatabase db = null;
    try {
      mLock.writeLock().lock();
      db = mDbHelper.getWritableDatabase();
      db.execSQL(mCreateTableQuery);
    } finally {
      mLock.writeLock().unlock();
    }

  }

  /**
   * Execute drop table query
   */
  public void dropTable() {
    SQLiteDatabase db = null;

    try {
      mLock.writeLock().lock();
      db = mDbHelper.getWritableDatabase();
      db.execSQL("drop table if exists " + TABLE_NAME + ";");
    } finally {
      mLock.writeLock().unlock();
    }
  }
}
