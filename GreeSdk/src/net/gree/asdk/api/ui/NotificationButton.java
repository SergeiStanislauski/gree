/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.api.ui;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.notifications.NotificationCounts;
import net.gree.asdk.core.notifications.NotificationCounts.Listener;
import net.gree.asdk.core.notifications.ui.NotificationPage;
import net.gree.asdk.api.ui.AsyncErrorDialog;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * <p>ImageButton Widget class for calling the notification board View.<p>
 * <p>
 * Displays the notification board view when the button is tapped.<br>
 * <p>
 * If you use the notification button, 
 * you can embed this class in your Xml files by simply adding the following lines:
 * </p>
 * <pre>
 * {@code
    <net.gree.asdk.api.ui.NotificationButton
      android:id="@+id/gree_game_notification_button"
      android:layout_width="wrap_content"
      android:layout_height="wrap_content"
      android:layout_toRightOf="@id/gree_dashboard_button"
      android:layout_marginLeft="27dp"
      layout="@layout/gree_game_notification_button"
      />
 * }
 * </pre>
 * @author GREE, Inc.
 */
public class NotificationButton extends RelativeLayout implements Listener {

  ImageButton mIconButton;
  NotificationCounts mNotificationCounts;

/**
 * Default constructor
 * @param context - application context.
 */
  public NotificationButton(Context context) {
    super(context);
    init(context);
  }

/**
 * Constructor with attributes
 * @param context - application context.
 * @param attrs - this view attributes.
 */
  public NotificationButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

/**
 * Constructor with attributes
 * @param context - application context.
 * @param attrs - this view attributes.
 * @param defStyle - default style.
 */
  public NotificationButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context);
  }

  private void init(Context context) {
    mNotificationCounts = Injector.getInstance(NotificationCounts.class);
    mNotificationCounts.addListener(this);
    View rootView =
        LayoutInflater.from(context)
            .inflate(RR.layout("gree_game_notification_button"), this, true);

    mIconButton = (ImageButton) rootView.findViewById(RR.id("gree_notificationBackground"));
    if (mIconButton != null) {
      mIconButton.setBackgroundResource(RR.drawable("gree_status_game_notification_selector"));

      mIconButton.setOnClickListener(new OnClickListener() {
        public void onClick(View view) {
          if (!isInEditMode()) {
            if (AsyncErrorDialog.shouldShowErrorDialog(getContext())) {
              AsyncErrorDialog dialog = new AsyncErrorDialog(getContext());
              dialog.show();
              return;
            }
            NotificationPage.launch((Activity) getContext());
            Logger.recordLog("pg", "game", "", null);
          }
        }
      });
    }

    if (!isInEditMode()) {
      updateNotificationCount();
    }
  }

  private void updateNotificationCount() {
    if ((mNotificationCounts == null) || (mIconButton == null)) {
      GLog.e("NotificationButton", "mNotificationCounts/mIconButton is null.");
      return;
    }

    int gameCounts =
        mNotificationCounts.getNotificationCount(NotificationCounts.TYPE_APP)
            + mNotificationCounts.getNotificationCount(NotificationCounts.TYPE_SNS);
    setNotificationCount(gameCounts);
    if (gameCounts > 0) {
      mIconButton.setBackgroundResource(RR
          .drawable("gree_status_game_notification_with_badge_selector"));
    } else {
      mIconButton.setBackgroundResource(RR.drawable("gree_status_game_notification_selector"));
    }

  }

/**
 * Sets the notification count on the icon.
 * @param count
 */
  public void setNotificationCount(int count) {
    TextView textView = (TextView) this.findViewById(RR.id("gree_notificationCount"));
    if (count > 0) {
      if (count >= 100) {
        textView.setText("99+");
      } else {
        textView.setText("" + count);
      }
      if (null != textView) {
        textView.setVisibility(View.VISIBLE);
        textView.requestLayout();
      }
    } else {
      if (null != textView) {
        textView.setVisibility(View.GONE);
      }
    }
  }

/**
 * Updates the notification count asynchronously on the icon.<br>
 */
  @Override
  public void onUpdate() {
    updateNotificationCount();
  }
}
