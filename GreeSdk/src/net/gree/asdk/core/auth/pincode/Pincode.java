/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth.pincode;


import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.auth.AuthorizeContext;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.auth.OAuthStorage;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.util.Url;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class Pincode implements IPinCode {
  private static final String TAG = Pincode.class.getSimpleName();
  private String mUserKey = null;

  public Pincode() {
    mUserKey = AuthorizeContext.getUserKey();
  }

  /**
   * Interface for listening to results for requesting pincode
   */
  public interface RequestListener {
    public void onSuccess();
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  @Override
  public void requestSMS(String userId, String countryCode, String phoneNumber, int year, int month, int day, int type, final RequestListener listener) {
    //select the end point
    String url = Url.getRegisterBySmsPincode();
    if (type == IPinCode.TYPE_UPGRADE) {
      url = Url.getUpgradeBySmsPincode();  
    } else if (type == IPinCode.TYPE_REAUTHORIZE) {
      url = Url.getReauthorizeBySmsPincode();
    }
    
    JSONObject params = new JSONObject();
    try {
      if (userId != null) {
        params.put("user_id", userId);
      }
      params.put("country", countryCode);
      params.put("telno", phoneNumber);
      if (year > 0 && month > 0 && day > 0) {
        params.put("birth", createBirthJson(year, month, day));
      }
      params.put("context", mUserKey);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(0, null, e.getMessage());
      }
      return;
    }
    OnResponseCallback<String> responseListener = new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onSuccess();
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    };

    //send the request
    if (type == IPinCode.TYPE_REAUTHORIZE) {
      new JsonClient().oauth2(url, BaseClient.METHOD_POST, params.toString(), false, responseListener);
    } else {
      new JsonClient().http(url, BaseClient.METHOD_POST, params.toString(), false, responseListener);
    }
  }

  @Override
  public void requestIVR(String userId, String countryCode, String phoneNumber, int year, int month, int day, int type, final RequestListener listener) {
    //select the end point
    String url = Url.getRegisterBySmsPincode();
    if (type == IPinCode.TYPE_UPGRADE) {
      url = Url.getUpgradeBySmsPincode();  
    } else if (type == IPinCode.TYPE_REAUTHORIZE) {
      url = Url.getReauthorizeBySmsPincode();
    }
    
    JSONObject params = new JSONObject();
    try {
      if (userId != null) {
        params.put("user_id", userId);
      }
      params.put("country", countryCode);
      params.put("telno", phoneNumber);
      if (year > 0 && month > 0 && day > 0) {
        params.put("birth", createBirthJson(year, month, day));
      }
      params.put("is_ivr", 1);
      params.put("context", mUserKey);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(0, null, e.getMessage());
      }
      return;
    }
 
    //Prepare listener
    OnResponseCallback<String> responseListener = new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onSuccess();
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    };

    //send the request
    if (type == IPinCode.TYPE_REAUTHORIZE) {
      new JsonClient().oauth2(url, BaseClient.METHOD_POST, params.toString(), false, responseListener);
    } else {
      new JsonClient().http(url, BaseClient.METHOD_POST, params.toString(), false, responseListener);
    }
  }

  /**
   * Interface for listening to results for commiting pincode
   */
  public interface CommitListener {
    public void onSuccess();
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /* (non-Javadoc)
   * @see net.gree.asdk.core.auth.IPinCode#commit(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, net.gree.asdk.core.auth.Pincode.CommitListener)
   */
  @Override
  public void commit(String userId, String countryCode, String phoneNumber, String pincode, 
                     String nickname, int year, int month, int day, final int type, final CommitListener listener) {
    //select the end point
    String url = Url.getRegisterBySmsCommit();
    if (type == IPinCode.TYPE_UPGRADE) {
      url = Url.getUpgradeBySmsCommit();  
    } else if (type == IPinCode.TYPE_REAUTHORIZE) {
      url = Url.getReauthorizeBySmsCommit();
    }

    //set the parameters
    JSONObject params = new JSONObject();
    try {
      if (userId != null) {
        params.put("user_id", userId);
      }
      params.put("country", countryCode);
      params.put("telno", phoneNumber);
      params.put("pincode", pincode);
      if (!TextUtils.isEmpty(nickname)) {
        params.put("nick_name", nickname);
      }
      if (year > 0 && month > 0 && day > 0) {
        params.put("birth", createBirthJson(year, month, day));
      }
      params.put("context", mUserKey);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(0, null, e.getMessage());
      }
      return;
    }
    
    //prepare the listener
    OnResponseCallback<String> responseListener = new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        GLog.d(TAG, "commit result : " + response);
        if (type == IPinCode.TYPE_UPGRADE) {
          if (listener != null) {
            listener.onSuccess();
          }
        }
        else {
          //save the gssid
          try {
            JSONObject json = new JSONObject(response);
            String token = json.getString("oauth_token");
            String secret = json.getString("oauth_token_secret");
            String userId = json.getString("user_id");
            OAuthStorage storage = new OAuthStorage(Core.getInstance().getContext());
            storage.setToken(token);
            storage.setSecret(secret);
            storage.setUserId(userId);
            Injector.getInstance(IAuthorizer.class).setTokenWithSecret(token, secret);
            saveGssIdFromHeader(headers);
            if (listener != null) {
              listener.onSuccess();
            }
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
            if (listener != null) {
              listener.onFailure(responseCode, headers, response);
            }
          }
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    };
    
    //send the request
    if (type == IPinCode.TYPE_REAUTHORIZE) {
      new JsonClient().oauth2(url, BaseClient.METHOD_POST, params.toString(), false, responseListener);
    } else {
      new JsonClient().http(url, BaseClient.METHOD_POST, params.toString(), false, responseListener);
    }
  }

  private JSONObject createBirthJson(int year, int month, int day) throws JSONException {
    JSONObject birth = new JSONObject();
    birth.put("year", String.format("%04d", year));
    birth.put("month", String.format("%02d", month));
    birth.put("day", String.format("%02d", day));
    return birth;
  }

  private void saveGssIdFromHeader(HeaderIterator headers) {
    while (headers.hasNext()) {
      Header header = headers.nextHeader();
      if (header.getName().equals("Set-Cookie")) {
        String heaserValue = header.getValue();
        String[] values = heaserValue.split(";");
        for (String value : values) {
          if (value.contains(CookieStorage.getGssIdKey() + "=")) {
            String[] gssidKeyValue = value.split("=");
            if (gssidKeyValue.length > 1) {
              String gssid = gssidKeyValue[1];
              GLog.d(TAG, "gssid=" + gssid);
              CookieStorage.setSessionsWithEncrypt(CookieStorage.getGssIdKey() + "=" + gssid);
            }
          }
        }
      }
    }
  }
}
