/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.cache.ImageFetcher;
import net.gree.asdk.core.codec.AesEnc;
import net.gree.asdk.core.request.Constants;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.track.Tracker;
import net.gree.asdk.core.track.Tracker.UploadStatus;
import net.gree.asdk.core.track.Tracker.Uploader;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.UtilWrapper;
import net.gree.oauth.signpost.OAuth.DebugListener;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import java.lang.ref.SoftReference;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * <p>Class representing one achievement</p>
 * Achievements can be set for each application in Developer Center.
 * One achievement registered with the Developer Center links with one instance of this class.
 * This class does not provide means of direct instantiation because this class is designed to be instantiated through the method {@link #loadAchievements() list()}.<br>
 * <br>
 * The following shows an example of codes. For other examples, see the sample application.<br>
 * <br>
 * Example of obtaining the achievement list of the current user
 * <code><pre>
       Achievement.loadAchievements(startIndex, pageSize, this);
 * </pre></code>
 * This will implements AchievementListUpdateListener
 * <code><pre>
  public void onSuccess(int index, int totalListSize, Achievement[] requestedElements) {
    endLoading();
    startIndex += pageSize;
    if (requestedElements.length < pageSize) {
      doneLoading = true;
    }
    for (int i = 0; i < requestedElements.length; i++) {
      data.add(requestedElements[i]);
    }
    adapter.notifyDataSetChanged();
    updateProfileNumber();
    showProfileSecondLine();
    showProfileFirstLine();

  }
  
  public void onFailure(int responseCode, HeaderIterator headers, String response) {
    endLoading();
    Log.e(TAG, "onFailure");
    Toast.makeText(AchievementListActivity.this, R.string.sync_failed, Toast.LENGTH_SHORT).show();
  }
 * </pre></code>
 
 * Example of unlocking the specified achievement
 * <code><pre>
     public void unlock() {
             achievement.unlock(new OnResponseListener&lt;Void&gt;() {
                     public void onSuccess(Void response) {
                             Log.e("Achievement", "achievement.unlock() succeeded.");
                     }

                     public void onFailure(String response) {
                             Log.e("Achievement", "achievement.unlock() failed.");
                     }
             });
     }
 * </pre></code>
 * To allow for repeatedly checking unlock of an achievement, the means of locking an achievement is provided in the following way.<BR>
 * This means is provided for testing which will be conducted during application development. Note that if it is used on a server in the production environment of GREE, an error will occur.
 * <code><pre>
     public void lock(Achievement achievement) {
             HashMap&lt;String,Object&gt; params = new HashMap&lt;String,Object&gt;();
             params.put("achievementDetailId", achievement.getId());
             new GreeRequest().makeGreeRequest("/sgpachievement/&#064;me/&#064;self/&#064;app", GreeRequest.METHOD_DELETE, params, new OnResponseListener&lt;JSONObject&gt;(){
                     public void onSuccess(JSONObject response) {
                             Log.e("Achievement", "Achievement.lock() succeeded.");
                     }

                     public void onFailure(String response) {
                             Log.e("Achievement", "Achievement.lock() failed.");
                     }
             });
     }
 * </pre></code>
 * @author GREE, Inc.
 * @since 2.0.0
 *
 */
public class Achievement {

  private static final String TAG = "Achievement";
  private static final String TRACK_TYPE = "AchievementUnlocking";

  private static final String KEY_ID = "id";
  private static final String KEY_NAME = "name";
  private static final String KEY_DESCRIPTION = "description";
  private static final String KEY_STATUS = "status";
  private static final String KEY_DETAIL_ID = "achievementDetailId";
  private static final String KEY_NONCE = "nonce";
  private static final String KEY_HASH = "hash";
  private static final String KEY_SECRET_FLAG = "secret";
  private static final String KEY_SCORE = "score";
  // for JSON
  private static final String UNLOCKED = "thumbnail_url";
  private static final String LOCKED = "lock_thumbnail_url";
  
  private Map<String, String> mParams;

  static boolean isDebug = false;
  static boolean isVerbose = false;

 
  /**
   * Sets the debug flag which enables debug logging.
   * @param debug mode on or off
   */
  public static void setDebug(boolean debug) {
    isDebug = debug;
    DebugListener.debugging[0] = debug;
  }

 
  /**
   * Sets the verbose flag for this class.
   * 
   * @param verbose mode on or off
   */
  public static void setVerbose(boolean verbose) {
    isVerbose = verbose;
  }

 
  /**
   * This is the listener for locking and unlocking achievements.
   */
  public interface AchievementChangeListener {
   
   /**
     * This method is called in response to the {@link #unlock(AchievementChangeListener, listener)} method
     * when the request for change achievement status is success. 
     */
    void onSuccess();

   
    /**
     * This method is called in response to the {@link #unlock(AchievementChangeListener, listener)} method
     * when the request for change achievement status is success. 
     * @param responseCode : network error code
     * @param headers : the response header iterator.
     * @param response : error message body.
     */
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  private AchievementChangeListener mUnlockListener;

  private static HashMap<String, SoftReference<Achievement>> trackerStack =
      new HashMap<String, SoftReference<Achievement>>();

  /**
   * The implementation class signature of Uploader might be stored in the db and it will be
   * reflective loaded by Tracker. Moreover proguard maybe change the class signature, so it uses a
   * named static class here. In addition, the following rules should be put in the proguard.cfg.
   * 
   * <pre>
   *   -keep class net.gree.asdk.core.track.Tracker$Uploader { *; }
   *   -keep class net.gree.asdk.api.Achievement { *; }
   * </pre>
   * 
   */
  private static class UploaderImpl implements Uploader {
    @Override
    public void upload(final String type, final String key, final String value,
        final UploadStatus cb) {
      tryUnLock(key, new OnResponseCallback<Void>() {
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, Void response) {
          if (cb != null) {
            cb.onSuccess(type, key, value);
          }
          debug("OnSuccess " + key);
          // Check if we still have a reference to the object we were trying to unlock
          SoftReference<Achievement> sAchievement = trackerStack.remove(key);
          if (sAchievement != null) {
            Achievement achievement = sAchievement.get();
            if (achievement != null) {
              debug("Unlocking local achievement object");
              achievement.mParams.put(KEY_STATUS, "0");
              AchievementChangeListener listener = achievement.mUnlockListener;
              if (listener != null) {
                debug("Notifying Success to listener for key " + key);
                listener.onSuccess();
              } else {
                debug("No Notifier " + key);
              }
            } else {
              debug("Object was drop by map for " + key);
            }
          } else {
            debug("No ref for " + key);
          }
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          if (cb != null) {
            cb.onFailure(type, key, value, responseCode, response);
          }

          // Check if we still have a reference to the object we were trying to unlock
          SoftReference<Achievement> sAchievement = trackerStack.get(key);
          if (sAchievement != null) {
            Achievement achievement = sAchievement.get();
            if (achievement != null) {
              AchievementChangeListener listener = achievement.mUnlockListener;
              if (listener != null) {
                listener.onFailure(responseCode, headers, response);
              }
            }
          }
        }
      });
    }
  }

  private static Uploader uploader = new UploaderImpl();

 
  /**
   * This is the listener for receiving the list of achievements.
   */
  public interface AchievementListUpdateListener {
   
    /**
     * This method is called in response to the {@link #loadAchievements(int count, int startIndex, AchievementListUpdateListener listener)} method.
     * @param index This is the index from which the requested elements have been loaded. The value is -1 if unknown.
     * @param totalListSize This is the total number of elements in the blocked user list. The value is -1 if unknown.
     * @param requestedElements The list of achievements, represented by an Achievement object array.
     * This might be empty if this game does not have any achievements.  
     */
    void onSuccess(int index, int totalListSize, Achievement[] requestedElements);

   
   
    /**
     * This method is called in response to a failure when trying to load the achievements.
     * @param responseCode An integer representing the code of the http response.
     * @param headers A Header object representing the headers of the http response.
     * @param response A String representing the body of the http response.
     */
   
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  private Achievement() {}

 
 
  /**
   * Constructor
   * The returned achievement will be empty except for the achievement ID.
   * This object should be used only for unlocking.
   * @param achievementId the achievement ID as defined in the developer center.
   */
 
  public Achievement(String achievementId) {
    mParams = new HashMap<String, String>();
    mParams.put(KEY_ID, achievementId);
  }

 
   /**
   * Gets the achievement ID.
   * @return Achievement ID
   */
  public String getId() {
    return mParams.get(KEY_ID);
  }

 
   /**
   * Gets the achievement name.
   * @return Achievement name
   */
  public String getName() {
    return mParams.get(KEY_NAME);
  }

 
   /**
   * Gets the image icon for unlocked status.<br>
   * Synchronized call; if the image is not ready, it returns null.
   * @return Icon image for unlocked status, null otherwise.
   */
  public Bitmap getIcon() {
	  if(mParams!=null){
		  if (isUnlocked()) {
			  return getImageFetcher().getBitmap(mParams.get(UNLOCKED));
		  } else {
			  return getImageFetcher().getBitmap(mParams.get(LOCKED));
		  }
	  }
	  return null;
  }

 
  /**
   * Method to fetch the description about the achievement.
   * @return Description about the achievement
   */
  public String getDescription() {
    return mParams.get(KEY_DESCRIPTION);
  }

 
  /**
   * Returns the score of the achievement.
   * @return Score of the achievement
   */
  public int getScore() {
    return getParamAsInt(KEY_SCORE);
  }

  private int getParamAsInt(String key) {
    int value = 0;
    try {
      value = Integer.parseInt(mParams.get(key));
    } catch (NumberFormatException e) {
      GLog.printStackTrace(TAG, e);
    }
    return value;
  }

 
  /**
   * Returns whether the achievement is unlocked or not.
   * @return true if the achievement is unlocked, false otherwise. 
   */
  public boolean isUnlocked() {
    String status = mParams.get(KEY_STATUS);
    return status != null && status.equals("0");
  }


 
  /**
   * Returns whether the achievement is a secret achievement or not.
   * @return true if the achievement is secret, false otherwise
   */
  public boolean isSecret() {
    String status = mParams.get(KEY_SECRET_FLAG);
    if (status != null && status.equals("1")) {
      return true;
    }
    return false;
  }

 
  /**
   * Starts loading the specified icon image for lock status.
   * @param listener Listener that processes the result.
   * @return true if the loading has started; false otherwise.
   */
  public boolean loadThumbnail(final IconDownloadListener listener) {
    if (isUnlocked()) {
    	getImageFetcher().loadImage(mParams.get(UNLOCKED), listener);
    } else {
    	getImageFetcher().loadImage(mParams.get(LOCKED),  listener);
    }
    return true;
  }

  private void jSonToParams(JSONObject obj) throws JSONException {
    mParams = new HashMap<String, String>();
    Iterator<?> i = obj.keys();
    while (i.hasNext()) {
      String key = (String) i.next();
      debug("PARSING :" + key + " " + (String) obj.getString(key));
      mParams.put(key, (String) obj.getString(key));
    }
    // load the images async here
    getImageFetcher().loadImage(mParams.get(LOCKED), null);
    getImageFetcher().loadImage(mParams.get(UNLOCKED), null);
  }

 
  /**
   * Sends an unlock request.
   * @param listener Listener that processes the result
   */
  public void unlock(final AchievementChangeListener listener) {
    if (getUtilWrapper().canOptOutOfGREE() && !getAuthorizer().hasOAuthAccessToken()) {
      new Handler(Looper.getMainLooper()).post(new Runnable() {
        public void run() {
          listener.onFailure(0, null, Constants.GRADE0_ERROR_MESSAGE);
        }
      });
      return;
    }
    String key = getId();
    mUnlockListener = listener;
    trackerStack.put(key, new SoftReference<Achievement>(this));
    String userId = getAuthorizer().getOAuthUserId();
    if (userId == null) {
      listener.onFailure(0, null, Constants.GRADE0_ERROR_MESSAGE);
      return;
    }
    verbose("unlock " + key + " for user " + userId);
    Injector.getInstance(Tracker.class).track(userId, TRACK_TYPE, key, "true", uploader);
  }

  private static IAuthorizer sAuthorizer;

  private static IAuthorizer getAuthorizer() {
    if (null == sAuthorizer) {
      sAuthorizer = Injector.getInstance(IAuthorizer.class);
    }
    return sAuthorizer;
  }
  
  private static ImageFetcher mImageFetcher;
  
  /**
   * Creating a new static instance of the Image Fetcher
   */
  private static ImageFetcher getImageFetcher(){
	if (null == mImageFetcher) {
		mImageFetcher = Injector.getInstance(ImageFetcher.class);
	}
	  return mImageFetcher; 
  }
  

  private static UtilWrapper sUtilWrapper;

  private static UtilWrapper getUtilWrapper() {
    if (null == sUtilWrapper) {
      sUtilWrapper = new UtilWrapper();
    }
    return sUtilWrapper;
  }

  private static void tryUnLock(final String id, final OnResponseCallback<Void> listener) {
    verbose("Unlocking " + id);
    String userId = getAuthorizer().getOAuthUserId();
    if (userId == null) {
      listener.onFailure(0, null, Constants.GRADE0_ERROR_MESSAGE);
      return;
    }
    String detailId = id;
    String nonceString = new SimpleDateFormat("yyyy-MMddHHmmssZ", Locale.US).format(new Date());
    String secretString = userId + nonceString;
    SecretKeySpec secretKey = new SecretKeySpec(secretString.getBytes(), "HmacSHA1");
    Mac mac;
    try {
      mac = Mac.getInstance("HmacSHA1");
      mac.init(secretKey);
      byte[] hashByte = mac.doFinal(detailId.getBytes());
      String hashString = AesEnc.toHex(hashByte).toLowerCase(); // PlatformAPI compares in LowerCase

      Map<String, Object> params = new HashMap<String, Object>();
      params.put(KEY_DETAIL_ID, detailId);
      params.put(KEY_NONCE, nonceString);
      params.put(KEY_HASH, hashString);

      final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
      final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_SGPACHIEVEMENT_POST);
      manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
      Request request = new Request(CoreData.getParams());
      request.oauthGree("/sgpachievement/@me/@self/@app", "post", params, null, false,
          new OnResponseCallback<String>() {

            @Override
            public void onSuccess(int responseCode, HeaderIterator headers, String response) {
              debug("Unlock Success " + id);
              manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
              manager.flushData(performData);
              listener.onSuccess(responseCode, headers, null);
            }

            @Override
            public void onFailure(int responseCode, HeaderIterator headers, String response) {
              debug("Unlock failed " + responseCode + " " + response);
              manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
              manager.flushData(performData);
              listener.onFailure(responseCode, headers, response);
            }
          });
    } catch (NoSuchAlgorithmException e) {
      listener.onFailure(0, null, e.toString());
    } catch (InvalidKeyException e) {
      listener.onFailure(0, null, e.toString());
    }
  }

 
  /**
   * <b>This is for testing purposes only. It will fail on production server. <b><br>
   * You should remove any call to this method before releasing your project.
   * 
   * @param listener to be notified when locking is done.
   */
  public void lock(final AchievementChangeListener listener) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("achievementDetailId", getId());
    debug("Locking " + getName());
    Request request = new Request(CoreData.getParams());
    request.oauthGree("/sgpachievement/@me/@self/@app", "delete", params, null, false,
        new OnResponseCallback<String>() {
          @Override
          public void onSuccess(int responseCode, HeaderIterator headers, String response) {
            mParams.put(KEY_STATUS, "1");
            listener.onSuccess();
          }

          @Override
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            GLog.e("Achievement", "Achievement.lock() failed.");
            listener.onFailure(responseCode, headers, response);
          }
        });
  }

 
  /**
   * Sends a request for obtaining a part of the achievement list of a specified user.
   * @param startIndex The start element, indexed from 1. You can pass -1 for default.
   * @param count Number of users to be obtained by one request. You can pass -1 for default.
   * @param listener The AchievementListUpdateListener that processes the result.
   */
  public static void loadAchievements(int startIndex, int count,
      AchievementListUpdateListener listener) {
    if (startIndex == 0) {
      throw new InvalidParameterException("StartIndex must be 1 or higher in OpenSocial api");
    }
    Map<String, Object> params = new HashMap<String, Object>();
    if (startIndex > 0) {
      params.put("startIndex", String.valueOf(startIndex));
    }

    if (count > 0) {
      params.put("count", String.valueOf(count));
    }
    loadAchievements(params, listener);
  }

  private static void loadAchievements(Map<String, Object> params, final AchievementListUpdateListener listener) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    verbose("Fetching " + params.get("startIndex") + " " + params.get("count"));
    params.put("appVersion", Core.getAppVersion());
    makeRequest("/sgpachievement/@me/@self/@app", params, new OnResponseCallback<String>() {
      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        listener.onFailure(responseCode, headers, response);
      }

      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        try {
          debug("Received : " + response);
          JSONObject responseObj = new JSONObject(response);
          Achievement[] achievements = null;
          int total = -1;
          int startIndex = -1;

          try {
            total = responseObj.getInt("totalResults");
          } catch (JSONException e) {
            GLog.d(TAG, "response don't have totalResults");
          }

          try {
            startIndex = responseObj.getInt("startIndex");
          } catch (JSONException e) {
            GLog.d(TAG, "response don't have startIndex");
          }

          JSONArray jsonArray = responseObj.getJSONArray("entry");
          achievements = new Achievement[jsonArray.length()];
          for (int i = 0; i < jsonArray.length(); i++) {
            debug("ACHIEVEMENT " + (startIndex + i));
            Achievement achievement = new Achievement();
            achievement.jSonToParams(jsonArray.getJSONObject(i));
            achievements[i] = achievement;
          }
          listener.onSuccess(startIndex, total, achievements);
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  private static void makeRequest(String action, Map<String, Object> params,
      OnResponseCallback<String> listener) {
    debug("Requesting : " + action);
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_SGPACHIEVEMENT_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    Request request = new Request(CoreData.getParams());

    if (CoreData.containsKey(InternalSettings.EnablePerformanceLogging) && CoreData.get(InternalSettings.EnablePerformanceLogging).equals("true")) {
      final OnResponseCallback<String> mListener = listener;
      request.oauthGree(action, "get", params, null, false, new OnResponseCallback<String>() {
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, String response) {
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          mListener.onSuccess(responseCode, headers, response);
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
          manager.flushData(performData);
          mListener.onFailure(responseCode, headers, response);
        }
      });
    }
    else {
      request.oauthGree(action, "get", params, null, false, listener);
    }
  }

  private static void debug(String msg) {
    if (isDebug) {
      GLog.d(TAG, msg);
    }
  }

  private static void verbose(String msg) {
    if (isVerbose) {
      GLog.v(TAG, msg);
    }
  }
}
