/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.socialgraph.request;

import java.util.TreeMap;

import org.apache.http.HeaderIterator;

import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.Request;
import net.gree.asdk.api.GreeUser.GreeUserListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.vendor.com.google.gson.Gson;

/**
 * Request calls to the people api,
 * used to retrieve list of friends
 */
public class PeopleRequest {

  private static final String TAG = "PeopleRequest";
  private static boolean isDebug = false;
  private static boolean isVerbose = false;

  // List of all GreeUser fields, used when null is passed for fields list.
  private static final String ALL_FIELDS = "id,nickname,displayName,userGrade,region,subregion,language,timezone,aboutMe,"
      + "birthday,profileUrl,thumbnailUrl,thumbnailUrlSmall,thumbnailUrlLarge,thumbnailUrlXlarge,"
      + "thumbnailUrlHuge,gender,age,bloodType,hasApp,userHash,userType,userSpecified";
  // List of limited GreeUser fields, used when limited user info is necessary (especially for non-friend / noHasApp users)
  private static final String LIMITED_FIELDS = "id,nickname,displayName,thumbnailUrl,thumbnailUrlSmall,thumbnailUrlLarge,thumbnailUrlXlarge,thumbnailUrlHuge";

  /**
   * What the GreeUser API potentially returns for each user.
   */
  private static class Response {
    public GreeUser[] entry;
    public String startIndex;
    public String totalResults;
    public String itemsPerPage;
  }

  /**
   * Core communication operation for calls that return users results.
   * 
   * @param url
   * @param offset
   * @param count
   * @param listener
   */
  public void getPeople(final String url, int offset, int count,
          final GreeUserListener listener) {
	  getPeople(url, false, offset, count, listener);
  }
  
  /**
   * Core communication operation for calls that return users results.
   * 
   * @param url
   * @param isLimitedInfo
   * @param offset
   * @param count
   * @param listener
   */
  public void getPeople(final String url, boolean isLimitedInfo, int offset, int count,
                                    final GreeUserListener listener) {
    Request request = new Request();
    TreeMap<String, Object> args = new TreeMap<String, Object>();
    args.put("startIndex", Integer.toString(offset));
    args.put("count", Integer.toString(count));
    if(isLimitedInfo) args.put("fields", LIMITED_FIELDS);
    else args.put("fields", ALL_FIELDS);
    if (isDebug) {
      GLog.d(TAG, url);
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_PEOPLE_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(url, "GET", args, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (isDebug) {
          GLog.d(TAG, "Http response:" + json);
        }
        try {
          Response response = Injector.getInstance(Gson.class).fromJson(json, Response.class);
          if (isVerbose) {
            GLog.d(TAG, "result:" + response.startIndex + " " + response.itemsPerPage + " " + response.totalResults);
          }
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          if (listener != null) {
            listener.onSuccess(Integer.parseInt(response.startIndex), Integer.parseInt(response.totalResults), response.entry);
          }
        } catch (Exception ex) {
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
          manager.flushData(performData);
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
}
