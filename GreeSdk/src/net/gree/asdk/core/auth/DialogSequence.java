/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import net.gree.asdk.core.util.Url;
import android.content.Context;

public class DialogSequence extends AuthorizeSequenceBase {

  public DialogSequence(Context context, String serviceCode) {
    super(context, serviceCode);
  }

  @Override
  protected void run() {
    StringBuilder url = new StringBuilder(Url.getIdTopUrl());
    if (mServiceCode != null) {
      url.append('&').append(AuthorizeSequenceBase.QUERY_PARAM_SERVICE_CODE).append('=').append(mServiceCode);
    }
    //sets up the login activity | calls the Activity that displays a popup window
    SetupActivity.setup(mContext, url.toString(), mListener);
  }
}
