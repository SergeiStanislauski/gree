/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.incentive;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HeaderIterator;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.incentive.IncentiveController;
import net.gree.asdk.api.incentive.callback.IncentiveGetListener;
import net.gree.asdk.api.incentive.model.IncentiveEvent;
import net.gree.asdk.api.incentive.model.IncentiveEventArray;
import net.gree.platformsample.BaseActivity;
import net.gree.platformsample.HelpActivity;
import net.gree.platformsample.R;

public class IncentiveGetActivity extends BaseActivity implements IncentiveGetListener {
  private static final String TAG = IncentiveGetActivity.class.getSimpleName();
  private IncentiveGETAdapter mAdapter;
  private ArrayList<IncentiveEvent> mData = new ArrayList<IncentiveEvent>();

  // private ListView list;
  private RadioGroup payloadType;
  
  // CONSTANTS
  private static final int RETURN_COUNT = 10;
  private static final String PAYLOAD_TYPE = "payload_type";
  private static final String COUNT = "count";


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(R.layout.incentive_get);

    // Initializes the UI
    list = (ListView) findViewById(R.id.incentive_get_lv);
    payloadType = (RadioGroup) findViewById(R.id.payloadType);
    // selection listeners
    payloadType.setOnCheckedChangeListener(RadioCheckChangeListener);
    startIndex = 0;

    if (mAdapter == null) {
      // init the adapter
      mAdapter = new IncentiveGETAdapter(this, mData);
    }

    // set the footer
    setUpAutoLoadMore();

    // set the adapter
    list.setAdapter(mAdapter);
  }

  // increments out next fetch point index
  private void incrementIndex() {
    startIndex = startIndex + RETURN_COUNT;
  }

  private void initRequest() {
   
    HashMap<String, String> params = new HashMap<String, String>();
    String vpayload_type = getPaylodType();
    
    // adds the payload type
    if (!TextUtils.isEmpty(vpayload_type)) {
      params.put(PAYLOAD_TYPE, vpayload_type);
    }
    
    // adds the count parameter
    params.put(COUNT, Integer.toString(RETURN_COUNT));
    // Adds the starting index
    params.put("startIndex", Integer.toString(startIndex));

    // fires off the request
    Log.e(TAG, "REQUEST!! startIndex:" + startIndex);
    IncentiveController.get(false, params, this);
  }

  // determines which payload to obtain based on the radio group
  private String getPaylodType() {
    int id = payloadType.getCheckedRadioButtonId();
    // determining the value
    int vpayloadType = 1;
    if (id == R.id.radio0) {
      vpayloadType = 1;
    } else if (id == R.id.radio1) {
      vpayloadType = 2;
    }
    return Integer.toString(vpayloadType);
  }

  // parse the arrary returned into an ArrayList for our listview
  private void parseIncentiveEvent(IncentiveEvent[] entry) {
    for (int i = 0; i < entry.length; i++) {
      mData.add(entry[i]);
    }
  }


  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) {
      return;
    }
    setUpBackButton();
  }

  @Override
  protected void onSaveInstanceState(final Bundle outState) {
//    outState.putSerializable("data", (Serializable) mData);
//    outState.putInt("index", startIndex);
  }

  @Override
  protected void sync(boolean fromStart) {
    if(loading){
      return;
    }
    
    startLoading();
    initRequest();
  }

  private OnCheckedChangeListener RadioCheckChangeListener = new OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
      Log.e(TAG,"onCheckedChanged");
      
      // reset the listview
      mData.clear();
      mAdapter.notifyDataSetChanged();
      
      // reset the counter
      startIndex = 0;
      
      // kick off a new request
      sync(true);
    }
  };

  @Override
  public void onOpenHelp(View v) {
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.incentive_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }

  @Override
  public void onSuccess(IncentiveEventArray result) {
    // log the success
    endLoading();
    Log.e(TAG, "onSuccess");
    
//    Log.e(TAG, result.toString());
//    Log.e(TAG, String.valueOf(result.getTotalResults()));

    Toast.makeText(IncentiveGetActivity.this, "GET Success!", Toast.LENGTH_SHORT).show();
    // parse events to ArrayList
    parseIncentiveEvent(result.getEntry());
    
    // update the adapter
    mAdapter.notifyDataSetChanged();
    incrementIndex();
    
    if (result == null || result.getEntry() == null || result.getEntry().length < RETURN_COUNT) {
      doneLoading = true;
    }

  }

  @Override
  public void onFailure(int responseCode, HeaderIterator headers, String response) {
    endLoading();
    StringBuilder sb = new StringBuilder();
    sb.append("onFailure:");
    sb.append(" responseCode:" + responseCode);
    sb.append(" response:" + response);
    // log failure
    Log.e(TAG, sb.toString());
    Toast.makeText(IncentiveGetActivity.this, "GET Failure", Toast.LENGTH_SHORT).show();
  }
}
