/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.socialgraph.request;

import java.util.TreeMap;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.gree.asdk.api.Request;
import net.gree.asdk.api.GreeUser.GreeIgnoredUserListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.request.OnResponseCallback;

/**
 * Request call to get the list of ignored users.
 */
public class IgnoredUsersRequest {
  
  private static final String TAG = "IgnoredUsersRequest";
  private static boolean isDebug = false;
  private static boolean isVerbose = false;
  
  // List of all GreeUser fields, used when null is passed for fields list.
  private static final String ALL_FIELDS = "id,nickname,displayName,userGrade,region,subregion,language,timezone,aboutMe,"
      + "birthday,profileUrl,thumbnailUrl,thumbnailUrlSmall,thumbnailUrlLarge,"
      + "thumbnailUrlHuge,gender,age,bloodType,hasApp,userHash,userType";
  
  /**
   * Core communication operation for calls that return ignored users results.
   * 
   * @param url
   * @param offset
   * @param count
   * @param listener
   */
  public void getPeople(final String url, int offset, final int count,
                                        final GreeIgnoredUserListener listener) {
    Request request = new Request();
    TreeMap<String, Object> args = new TreeMap<String, Object>();
    args.put("startIndex", Integer.toString(offset));
    args.put("count", Integer.toString(count));
    args.put("fields", ALL_FIELDS);
    if (isDebug) {
      GLog.d(TAG, url);
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_IGNORELIST_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(url, "GET", args, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (isVerbose) {
          GLog.d(TAG, "Http response:" + json);
        }
        JSONObject responseObj;

        int total = 1;
        int startIndex = 1;

        try {
          responseObj = new JSONObject(json);

          try {
            total = responseObj.getInt("totalResults");
          } catch (JSONException e) { 
            GLog.d(TAG, "response don't have totalResult");
          }

          try {
            startIndex = responseObj.getInt("startIndex");
          } catch (JSONException e) { 
            GLog.d(TAG, "response don't have startIndex");
          }

          // Get the user's ids
          String[] list = null;
          if (total > 1) {
            JSONArray entries = responseObj.getJSONArray("entry");
            int length = entries.length();
            list = new String[length];
            for (int i = 0; i < length; i++) {
              JSONObject ids = entries.getJSONObject(i);
              list[i] = ids.getString("ignorelistId");
            }
          } else if (total <= 0) {
            // nothing to do
            if (isVerbose) {
              GLog.d(TAG, "No ignored users");
            }
          } else {
            // for some strange reason when we request ignore user, we get a single entry instead of
            // an array with 1 entry
            String entry = ((JSONObject) responseObj).getString("entry");
            if (entry != null && 0 < entry.length()) {
              list = new String[1];
              try {
                JSONObject obj = responseObj.getJSONObject("entry");
                list[0] = obj.getString("ignorelistId");
              } catch (JSONException e) {
                JSONArray entries = responseObj.getJSONArray("entry");
                JSONObject ids = entries.getJSONObject(0);
                list[0] = ids.getString("ignorelistId");
              }
            } else {
              total = 0;
              list = null;
            }
          }
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          if (listener != null) {
            listener.onSuccess(startIndex, total, list);
          }
        } catch (JSONException e) {
          if (listener != null) {
            listener.onFailure(responseCode, headers, "" + e.getMessage() + " : Invalid server response, invalid json format : "+json);
          }
          GLog.printStackTrace(TAG, e);
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

}
