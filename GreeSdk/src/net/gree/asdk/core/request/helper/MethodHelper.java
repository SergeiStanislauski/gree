/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request.helper;

import net.gree.asdk.core.request.Constants.HTTP_METHOD;

public class MethodHelper {
  public static HTTP_METHOD parseString(String method) {
    // {"GET","POST","PUT","DELETE"};
    if ("get".equals(method.toLowerCase())) {
      return HTTP_METHOD.GET;
    } else if ("post".equals(method.toLowerCase())) {
      return HTTP_METHOD.POST;
    } else if ("put".equals(method.toLowerCase())) {
      return HTTP_METHOD.PUT;
    } else if ("delete".equals(method.toLowerCase())) {
      return HTTP_METHOD.DELETE;
    } else {
      throw new IllegalStateException();
    }
  }

  public static HTTP_METHOD parseInt(int method) {
//    public static final int METHOD_GET = 0;
//    public static final int METHOD_POST = 1;
//    public static final int METHOD_PUT = 2;
//    public static final int METHOD_DELETE = 3;
    if (0 == method) {
      return HTTP_METHOD.GET;
    } else if (1 == method) {
      return HTTP_METHOD.POST;
    } else if (2 == method) {
      return HTTP_METHOD.PUT;
    } else if (3 == method) {
      return HTTP_METHOD.DELETE;
    } else {
      throw new IllegalStateException();
    }
  }
}
