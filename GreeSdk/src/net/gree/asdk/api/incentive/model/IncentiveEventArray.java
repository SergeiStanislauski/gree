package net.gree.asdk.api.incentive.model;

/**
 * Model object for incentive event array.
 * 
 * This is POJO Object for IncentiveController.
 * 
 * It contains a collection of IncentiveEvent.
 * 
 * @see {@link IncentiveEvent}
 * @see {@link IncentiveController}
 */
public class IncentiveEventArray {
  private IncentiveEvent[] entry;
  private int startIndex;
  private int totalResults;
  private int itemsPerPage;
  private String filtered;
  private String sorted;
  private int hasNext;
  private String updatedSince;

  /**
   * Gets an Array of the Incentive Events.
   * 
   * @see {@link IncentiveEvent}
   * @return Incentive Event Array
   */
  public IncentiveEvent[] getEntry() {
    return entry;
  }

  /**
   * Sets the Incentive Event Array.
   * 
   * @param entry parameter representing the list of Incentive events.
   */
  public void setEntry(IncentiveEvent[] entry) {
    this.entry = entry;
  }

  /**
   * Gets the start of the result index.
   * 
   * @see {@link IncentiveController.KEY_STARTINDEX}
   * @return start index of the Result
   */
  public int getStartIndex() {
    return startIndex;
  }

  /**
   * Sets of start of the result index.
   * 
   * @see {@link IncentiveController.KEY_STARTINDEX}
   * @param startIndex parameter representing from where to begin the 
   * results returned 
   */
  public void setStartIndex(int startIndex) {
    this.startIndex = startIndex;
  }

  /**
   * Gets a count of the results returned.
   * 
   * @return count of result
   */
  public int getTotalResults() {
    return totalResults;
  }

  /**
   * Sets the count of the results returned to a specific number. Max is 100.
   * 
   * @param totalResults number of desired results returned with the request.
   */
  public void setTotalResults(int totalResults) {
    this.totalResults = totalResults;
  }

  /**
   * Get the number of item per page.
   * 
   * @see {@link IncentiveController.KEY_COUNT}
   * @return number of items per page
   */
  public int getItemsPerPage() {
    return itemsPerPage;
  }

  /**
   * Sets the number of items to be displayed per page.
   * 
   * @param itemsPerPage parameter representing how many items should be displayed per page.
   */
  public void setItemsPerPage(int itemsPerPage) {
    this.itemsPerPage = itemsPerPage;
  }

  /**
   * Gets the String value for which the results are filtered on.
   * 
   * @return the filtered String
   */
  public String getFiltered() {
    return filtered;
  }

  /**
   * Sets the String which the results are filtered on. 
   * 
   * @param filtered String to filter
   */
  public void setFiltered(String filtered) {
    this.filtered = filtered;
  }

  /**
   * Gets a String representation of how the results are sorted.
   * 
   * @return a type of sorting String, if the result is sorted. 
   */
  public String getSorted() {
    return sorted;
  }

  /**
   * Sets the type of sorting on the results. 
   * 
   * @param sorted the type of sorting to apply on the results
   */
  public void setSorted(String sorted) {
    this.sorted = sorted;
  }

  /**
   * Gets the number of pages of results in relationship to the current page.
   * 
   * @return number of "next" pages
   */
  public int getHasNext() {
    return hasNext;
  }

  /**
   * Sets the number of pages forward in relationship to the current page.
   * 
   * @param hasNext the number of "next" pages the developer wants to set.
   */
  public void setHasNext(int hasNext) {
    this.hasNext = hasNext;
  }

  /**
   * Gets a String representation of the time since the last update.
   * 
   * @return the time that has elapsed since the last update.
   */
  public String getUpdatedSince() {
    return updatedSince;
  }

  /**
   * Sets the value of the time since the last update.
   * 
   * @param updatedSince parameter to represent the value of when 
   * the last update was to occur on this event.
   */
  public void setUpdatedSince(String updatedSince) {
    this.updatedSince = updatedSince;
  }

  /**
   * Simple toString formatter. Obtains the String in a fashion 
   * similar to JSON object's representation.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("{");

    sb.append("startIndex:");
    sb.append(startIndex);
    sb.append(",");

    sb.append("totalResults:");
    sb.append(totalResults);
    sb.append(",");

    sb.append("itemsPerPage:");
    sb.append(itemsPerPage);
    sb.append(",");

    sb.append("filtered:");
    sb.append(filtered);
    sb.append(",");

    sb.append("sorted:");
    sb.append(sorted);
    sb.append(",");

    sb.append("hasNext:");
    sb.append(hasNext);
    sb.append(",");

    sb.append("updatedSince:");
    sb.append(updatedSince);
    sb.append(",");

    sb.append("entry:[");
    for (IncentiveEvent one : entry) {
      sb.append(one.toString());
    }
    sb.append("]");
    sb.append("}");
    return sb.toString();
  }
}
