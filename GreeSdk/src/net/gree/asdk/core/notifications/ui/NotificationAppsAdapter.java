/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.notifications.ui;

import java.io.File;
import java.io.IOException;
import java.util.List;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.cache.DiskLruCache;
import net.gree.asdk.core.notifications.CacheReadMarker;
import net.gree.asdk.core.notifications.NotificationDownloader;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.storage.JSONStorage;
import net.gree.asdk.core.ui.RoundCornerImage;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

public class NotificationAppsAdapter extends NotificationAdapterBase {
  private final static String TAG = NotificationAppsAdapter.class.getName();

  public final static String FILTER_TARGET = "notification_target";
  public final static String FILTER_OTHERS = "notification_other";

  private final static int TARGET_LOAD_LIMIT = 3;

  public NotificationAppsAdapter(Context context, DisplayMetrics metrics, List<NotificationData> objects) {
    super(context, metrics, objects);
  }

  /**
   * Load "App notifications" data and place into adapter's objects list
   * @return count of loaded notifications
   */
  public int loadNotifications() {
    JSONStorage storage = Injector.getInstance(JSONStorage.class);
    SQLiteDatabase db = storage.getReadableDatabase();

    mNotificationType = NotificationFragment.NOTIFICATION_APPS;
    mDisplayObjects.clear();
    Resources r = getContext().getResources();

    NotificationData separator1 = new NotificationData();
    separator1.cellType = CELL_TYPE_SEPARATOR;
    separator1.text = r.getString(RR.string("gree_notification_title_invite"));
    mDisplayObjects.add(separator1);

    Bitmap origIcon = BitmapFactory.decodeResource(r, RR.drawable("gree_notification_game_default"));
    RoundCornerImage rci = new RoundCornerImage(origIcon);

    try {
      loadInvite(db);
    } catch (IOException e1) {
    }

    int appsCount = 0;
    try {
      appsCount = loadTarget(db, rci.getRoundedBitmap());
      if (appsCount <= 0) {
        addNoNotification();
      }
    } catch (IOException e) {

    }

    int othersCount = loadOthers(db, rci.getRoundedBitmap());

    if (othersCount >= LOAD_LIMIT) {
      NotificationData autopagerize = new NotificationData();
      autopagerize.cellType = CELL_TYPE_AUTOPAGERIZE;
      autopagerize.text = r.getString(RR.string("gree_notification_loading"));
      mDisplayObjects.add(autopagerize);        
    }

    markAsRead("game", new OnResponseCallback<String>(){
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        CacheReadMarker targetMarker = new CacheReadMarker("notification_target");
        targetMarker.execute();

        CacheReadMarker othersMarker = new CacheReadMarker("notification_other");
        othersMarker.execute();
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, response);
      }

    });

    mContentCount = othersCount;
    return othersCount + appsCount;
  }

  /**
   * Load "Apps invide" data and place onto adapter's objects list
   */
  private void loadInvite(SQLiteDatabase db) throws IOException{
    File dir = getContext().getDir(NotificationDownloader.CACHE_DIR, Activity.MODE_PRIVATE);
    DiskLruCache cache = null;
    NotificationData data = new NotificationData();

    try {
      cache = DiskLruCache.open(dir, NotificationDownloader.APP_VERSION, 1, NotificationDownloader.CACHE_MAX_SIZE);
      DiskLruCache.Snapshot snapshot = cache.get(NotificationDownloader.CACHE_KEY);

      if (snapshot == null) {
        return;
      } else {
        String storedText = snapshot.getString(0);
        JSONObject json = new JSONObject(storedText);
        data.json = (JSONObject) json.get("invite");
        data.text = data.text = data.json.getString("message");
        data.cellType = CELL_TYPE_SEEALL;
        mDisplayObjects.add(data);
        snapshot.close();
      }
    } catch (IOException e) {
      GLog.printStackTrace(TAG, e);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    } finally {
      if (cache != null) { cache.close(); }
    }
  }

  private int loadTarget(SQLiteDatabase db, Bitmap defIcon) throws IOException{
    File dir = getContext().getDir(NotificationDownloader.CACHE_DIR, Activity.MODE_PRIVATE);
    DiskLruCache cache = null;
    int appsCount = 0;

    try {
      cache = DiskLruCache.open(dir, NotificationDownloader.APP_VERSION, 1, NotificationDownloader.CACHE_MAX_SIZE);
      DiskLruCache.Snapshot snapshot = cache.get(NotificationDownloader.CACHE_KEY);

      if (snapshot == null) {
        return 0;
      } else {
        NotificationData separator2 = new NotificationData();
        String storedString = snapshot.getString(0);
        JSONObject all = new JSONObject(storedString);
        separator2.text = all.getString("app_name");
        separator2.cellType = CELL_TYPE_SEPARATOR;
        mDisplayObjects.add(separator2);

        appsCount = loadByFilter(db, "notification_target", defIcon, TARGET_LOAD_LIMIT);

        if (appsCount >= TARGET_LOAD_LIMIT) {
          Resources r = getContext().getResources();
          NotificationData seeall = new NotificationData();
          seeall.cellType = CELL_TYPE_SEEALL;
          JSONObject target = all.getJSONObject("target");
          seeall.json = target;
          seeall.text = r.getString(RR.string("gree_notification_see_all"));
          mDisplayObjects.add(seeall);
        }

        snapshot.close();
      }
    } catch (IOException e) {
      GLog.printStackTrace(TAG, e);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    } finally {
      if (cache != null) { cache.close(); }
    }

    return appsCount;
  }


  private int loadOthers(SQLiteDatabase db, Bitmap defIcon) {
    Resources r = getContext().getResources();

    NotificationData separator3 = new NotificationData();
    separator3.cellType = CELL_TYPE_SEPARATOR;
    separator3.text = r.getString(RR.string("gree_notification_title_other"));
    mDisplayObjects.add(separator3);

    int othersCount = loadByFilter(db, "notification_other", defIcon, LOAD_LIMIT);

    if (othersCount <= 0) {
      NotificationData noNotification = new NotificationData();
      noNotification.cellType = CELL_TYPE_NO_NOTIFICATION;
      mDisplayObjects.add(noNotification);
    }

    return othersCount;
  }

}
