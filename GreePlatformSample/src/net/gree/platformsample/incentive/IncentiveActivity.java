/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.incentive;

import net.gree.asdk.api.GreePlatform;
import net.gree.platformsample.BaseActivity;
import net.gree.platformsample.HelpActivity;
import net.gree.platformsample.R;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;


public class IncentiveActivity extends BaseActivity {
  private static final String TAG = IncentiveActivity.class.getSimpleName();
private RelativeLayout post, read;
  
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.incentive_page);
    
    // init the page variables
    post = (RelativeLayout)findViewById(R.id.incentives_rl_wrapper_top);
    read = (RelativeLayout)findViewById(R.id.incentives_rl_wrapper_bot);
    // set the click listener
    read.setOnClickListener(IncentiveClickListener);
    post.setOnClickListener(IncentiveClickListener);
  }


  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) {
      return;
    }
    setUpBackButton();
    setUpAutoLoadMore();
  }

  @Override
  protected void sync(boolean fromStart) {}

  @Override
  public void onOpenHelp(View v) {
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.incentive_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }
  
  private OnClickListener IncentiveClickListener = new OnClickListener(){
	@Override
	public void onClick(View v) {
	  // on post click
	  if(v.equals(read)){
		IncentiveActivity.this.startActivity(new Intent(IncentiveActivity.this,
		            IncentiveGetActivity.class));
	  }
	  // on read click
	  if(v.equals(post)){
		IncentiveActivity.this.startActivity(new Intent(IncentiveActivity.this,
		            IncentivePostActivity.class));
	  }
	}
	  
  };
}
