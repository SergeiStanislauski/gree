/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.wallet;

import java.util.TreeMap;

import net.gree.asdk.api.Request;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.vendor.com.google.gson.Gson;

import org.apache.http.HeaderIterator;

public class BalanceData {
  /** Represents common GREE coin is bought in Japan */
  public static final int COIN_TYPE_GREE_JCOIN = 1;
  /** Represents application-specific coin is bought in Japan */
  public static final int COIN_TYPE_SPECIFIC_JCOIN = 2;
  /** Represents the common GREE coin is bought in other than Japan */
  public static final int COIN_TYPE_GREE_GCOIN = 3;
  /** Represents application-specific coin is bought in other than Japan */
  public static final int COIN_TYPE_SPECIFIC_GCOIN = 4;

  private String user_id;
  private String platform;
  private String balance;
  private int coin_type;

  protected class Response {
    public BalanceData entry;
  }

  private BalanceData() { };

  /**
   * Interface which receives the results of the Balance API
   */
  public interface BalanceDataListener {
    /**
     * Called upon successful completion of getting the balance for the current user.
     * @param balance structure of balance data which you request.
     */
    public void onSuccess(BalanceData balance);
    /**
     * Called when something went wrong when trying to get the balance for the current user.
     * @param responseCode Integer representing the http response code.
     * @param headers Object representing the headers of the http response.
     * @param response String representing the body of the http response.
     */
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Loads the balance for the current user.
   * @param listener receives results of this load.
   */
  public static void load(final BalanceDataListener listener) {
    TreeMap<String,Object> args = new TreeMap<String,Object>();
    args.put("platform", "android");
    new Request().oauthGree("/balance/@me/@self", "GET", args, null, false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        Gson gson = Injector.getInstance(Gson.class);
        Response res = gson.fromJson(response, Response.class);
        if (listener != null) {
          listener.onSuccess(res.entry);
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  /**
   * Gets the user ID for the current user.
   * @return an alphanumeric user ID for the current user
   */
  public String getUserId() { return user_id; }

  /**
   * Gets the platform name for the current device.
   * @return A String representing the platform name for the current device
   */
  public String getPlatform() { return platform; }

  /**
   * Gets the balance for the current user.
   * @return the balance for the current user
   */
  public long getBalance() { return Long.parseLong(balance); }

  /**
   * Gets the type of coin.
   * @return one of coin type like COINT_TYPE_GREE_JCOIN, COIN_TYPE_SPECIFIC_GCOIN etc...
   */
  public int getCoinType() { return coin_type; }
}
