package net.gree.asdk.core.auth.pincode;

import net.gree.asdk.core.auth.pincode.Pincode.CommitListener;
import net.gree.asdk.core.auth.pincode.Pincode.RequestListener;


public interface IPinCode {

  /**
   * Create a new account and confirm it by sms
   */
  public static final int TYPE_REGISTER = 0;
  
  /**
   * Upgrade an existing account by sms
   */
  public static final int TYPE_UPGRADE = 1;
  
  /**
   * Reauthorize an existing account by using the userId, birthday and phone number
   * all must match the current device to be reauthorized correctly,
   */
  public static final int TYPE_REAUTHORIZE = 2;
  
  /**
   * Trigger the requests to generate a pincode.
   * 
   * @param userId the userId (needed on reauthorize only)
   * @param countryCode the country code in lower case ISO 3166
   * @param phoneNumber the phone number to receive the sms
   * @param year year of the birthday
   * @param month month of the birthday
   * @param day day of the birthday
   * @param type one of TYPE_REGISTER, TYPE_UPGRADE, TYPE_REAUTHORIZE
   * @param listener the listener to follow the request
   */
  public abstract void requestSMS(String userId, String countryCode, String phoneNumber, int year, int month,
      int day, int type, final RequestListener listener);

  /**
   * Trigger the requests to generate a pincode by IVR
   * 
   * @param userId the userId (needed on reauthorize only)
   * @param countryCode the country code in lower case ISO 3166
   * @param phoneNumber the phone number to receive the sms
   * @param year year of the birthday
   * @param month month of the birthday
   * @param day day of the birthday
   * @param type one of TYPE_REGISTER, TYPE_UPGRADE, TYPE_REAUTHORIZE
   * @param listener the listener to follow the request
   */
  public void requestIVR(String userId, String countryCode, String phoneNumber, int year, int month, int day,
      int type, final RequestListener listener);

  /**
   * Trigger the requests to authorize the phone number with pincode.
   * 
   * @param userId the userId (needed on reauthorize only)
   * @param countryCode the country code in lower case ISO 3166
   * @param phoneNumber the phone number to receive the sms
   * @param pincode the pin code received by SMS
   * @param nickname the nickname
   * @param year year of the birthday
   * @param month month of the birthday
   * @param day day of the birthday
   * @param type one of TYPE_REGISTER, TYPE_UPGRADE, TYPE_REAUTHORIZE
   * @param listener the listener to follow the request
   */
  public abstract void commit(String userId, String countryCode, String phoneNumber, String pincode,
      String nickname, int year, int month, int day, int type, final CommitListener listener);

}
