/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api.ui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.TreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.ui.AbsServiceDialog;
import net.gree.asdk.core.util.Url;

/**
 * Class which represents the dialog for sending a Share message with an image.<br>
 * <p>
 * When a share is successfully sent, the result of the shared message are sent as JSON data to notify the sender.
 * </p>
 * Sample code:
 * <code><pre>
 * Handler handler = new Handler() {
 *   public void handleMessage(Message message) {
 *     switch(message.what) {
 *       case ShareDialog.OPENED:
 *         Log.d("Share", "ShareDialog opened.");
 *         break;
 *       case ShareDialog.CLOSED:
 *         Log.d("Share", "ShareDialog closed.");
 *         Log.d("Share", "result:" + message.obj.toString());
 *         break;
 *     }
 *   }
 * };
 *
 * ShareDialog dialog = new ShareDialog(Activity.this);
 * dialog.setHandler(handler);
 *
 * TreeMap&lt;String,Object&gt; map = new TreeMap&lt;String,Object&gt;();
 * map.put("message", "message of the share dialog");
 * map.put("image", bitmap);
 * dialog.setParams(map);
 * dialog.show();
 * </pre></code>
 * @author GREE, Inc.
 *
 */
public class ShareDialog extends AbsServiceDialog {
  private static final String TAG = "ShareDialog";

/**
 * Handler event which is notified when the share dialog is started/opened.<pre>
 */
  public static final int OPENED = 1;
/**
 * <p>
 * Handler event which notified when share dialog is finished/closed.<br>
 * This event has the additional parameter "message.obj" that shares results in the handler's message.
 * </p>
 */
  public static final int CLOSED = 2;

  private static final String ENDPOINT = Url.getShareDialogUrl();
  private static final String SERVICE_CODE = "SH0";

  /**
   * Default constructor.
   * @param context The Context the Dialog is to run it.
   */
  public ShareDialog(Context context) {
    super(context);

    //set the dialog type for TaskEventListeners
    mClassType = GreePlatformListener.CLASS_SHARE_DIALOG;

    setRequestType(TYPE_REQUEST_METHOD_POST);

    setIsClearHistory(true);
    setTitleType(TITLE_TYPE_STRING);
    setTitle(GreePlatform.getRString(RR.string("gree_dialog_title_share")));
  }

  /**
   * Sets the event handler.
   * @param handler event handler.
   */
  @Override
  public void setHandler(Handler handler) {
    super.setHandler(handler);
  }

 
  /**
   * Shows the dialog.
   */
  @Override
  public final void show() {
    mPerformData = mManager.createData(PerformanceFlowIndex.SHARE);
    super.show();
    GLog.d(TAG, "show()");
  }

  @Override
  protected void onShow() {
    setPostData(buildPostData());
  }

/**
 * Sets the optional parameters.
 * Parameter is passed as TreeMap object.
 * @param params TreeMap object which having the following keys as elements.
 * @param message message shown as shareDialog's message.
 * @param image bitmap object shown as shareDialog's screenshot.
 */
  public void setParams(TreeMap<String,Object> params) {
    super.setParams(params);
  }

  @Override protected int getOpenedEvent() { return OPENED; }

  @Override protected int getClosedEvent() { return CLOSED; }

  @Override
  protected String getEndPoint() { return ENDPOINT; }

  @Override
  protected String getServiceName() { return "share"; }

  @Override
  protected String getServiceCode() { return SERVICE_CODE; }

  @Override
  protected boolean launchService(String from, String action, String target, JSONObject params) {
    @SuppressWarnings("unchecked")
    Iterator<String> nameItr = params.keys();
    TreeMap<String, Object> outMap = new TreeMap<String, Object>();
    while(nameItr.hasNext()) {
      String name = nameItr.next();
      try {
        outMap.put(name, params.getString(name));
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }
    setParams(outMap);
    return super.launchService(from, action, target, params);
  }
  
  @Override
  protected void notifyServiceResult(String from, String action, JSONObject params) {
    String postData = buildPostData();
    if (params != null) {
      JSONObject query_params = params.optJSONObject("query_params");
      if (query_params != null) {
        final StringBuilder postBuilder = new StringBuilder(postData);
        @SuppressWarnings("unchecked")
        Iterator<String> keys = query_params.keys();
        while (keys.hasNext()) {
          String key = keys.next();
          try {
            String value = query_params.getString(key);
            postBuilder.append("&").append(key).append("=").append(URLEncoder.encode(value, "UTF-8"));
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          } catch (UnsupportedEncodingException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
        postData = postBuilder.toString();
      }
    }
    setPostData(postData);
    super.notifyServiceResult(from, action, params);
  }

 
  /**
   * Get displayed URL used in WebView
   * @return display URL
   */
  public String getDisplayUrl(){
    return getEndPoint();
  }
}
