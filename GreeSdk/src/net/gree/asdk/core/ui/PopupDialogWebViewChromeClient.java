package net.gree.asdk.core.ui;

import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

public class PopupDialogWebViewChromeClient extends WebChromeClient {
  private static final int PROGRESS_MAX = 100;
  private ProgressBar mProgress;
  
  public PopupDialogWebViewChromeClient(ProgressBar progress) {
    mProgress = progress;
  }
  
  @Override
  public void onProgressChanged(WebView view, int newProgress) {
    super.onProgressChanged(view, newProgress);
    if(mProgress != null) {
      mProgress.setVisibility(newProgress == PROGRESS_MAX ? View.INVISIBLE : View.VISIBLE);
      mProgress.setProgress(newProgress);
    }
  }
}
