/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TreeMap;

import net.gree.asdk.core.Injector;
import org.apache.http.HeaderIterator;

import android.util.Log;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.vendor.com.google.gson.Gson;

/**
 * <p>
 * FriendCode API provides the friend code function (create, verify, delete, control history and get various information) on invitation of inputting code.<br>
 * Friend code is published one per application, one per user, and is a random code of seven digits.<br>
 * Friend code has a time limit. You can set up it freely for 180 days and the default duration is 180 days.
 * </p>
 * <p>
 * The following is an example of codes.<br>
 * Example of creating new friend code:
 * </p>
 * <code><pre>
 * String expireTime = "2012-05-23T12:00:00+0000";
 *
 * FriendCode.requestCode(expireTime, new CodeListener() {
 *   public void onSuccess(FriendCode.Code code) {
 *     Toast.makeText(this, "FriendCode.requestCode success.[code:" + code.getCode() + " expire:" + code.getExpireTime() + "]", Toast.LENGTH_LONG).show();
 *   }
 *   public void onFailure(int responseCode, HeaderIterator headers, String response) {
 *     Toast.makeText(this, "FriendCode.requestCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
 *   }
 * });
 * </pre></code>
 * <p>
 * Example of loading a friend code.
 * </p>
 * <code><pre>
 * FriendCode.loadCode(new CodeListener() {
 *   public void onSuccess(FriendCode.Code code) {
 *     Toast.makeText(this, "FriendCode.loadCode success.[code:" + code.getCode() + " expire:" + code.getExpireTime() + "]", Toast.LENGTH_LONG).show();
 *   }
 *   public void onFailure(int responseCode, HeaderIterator headers, String response) {
 *     Toast.makeText(this, "FriendCode.loadCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
 *   }
 * });
 * </pre></code>
 * <p>
 * Example of verifying another user's friend code.
 * </p>
 * <code><pre>
 * String entryCode = "ABC1234";
 *
 * FriendCode.verifyCode(entryCode, new SuccessListener() {
 *   public void onSuccess() {
 *     Toast.makeText(this, "FriendCode.verifyCode success.", Toast.LENGTH_LONG).show();
 *   }
 *   public void onFailure(int responseCode, HeaderIterator headers, String response) {
 *     Toast.makeText(this, "FriendCode.verifyCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
 *   }
 * });
 * </pre></code>
 * <p>
 * Example of deleting a friend code.
 * </p>
 * <code><pre>
 * FriendCode.deleteCode(new SuccessListener() {
 *   public void onSuccess() {
 *     Toast.makeText(this, "FriendCode.deleteCode success.", Toast.LENGTH_LONG).show();
 *   }
 *   public void onFailure(int responseCode, HeaderIterator headers, String response) {
 *     Toast.makeText(this, "FriendCode.deleteCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
 *   }
 * });
 * </pre></code>
 * <p>
 * Example of loading the information of friend codes you already typed.
 * </p>
 * <code><pre>
 * FriendCode.loadOwner(new OwnerGetListener() {
 *   public void onSuccess(FriendCode.Data owner) {
 *     Toast.makeText(this, "FriendCode.loadOwner success.[ownId:" + owner.getUserId() + "]", Toast.LENGTH_LONG).show();
 *   }
 *   public void onFailure(int responseCode, HeaderIterator headers, String response) {
 *     Toast.makeText(this, "FriendCode.loadOwner failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
 *   }
 * });
 * </pre></code>
 * <p>
 * Example of loading the user id list after you input your friend code.
 * </p>
 * <code><pre>
 * int startIdx = 1;
 * int counts = 10;
 *
 * FriendCode.loadFriends(startIdx, counts, new EntryListGetListener() {
 *   public void onSuccess(int startIndex, int itemsPerPage, int totalResults, FriendCode.Data[] entries) {
 *     Toast.makeText(this, "FriendCode.loadFriends success.[results:" + totalResults + " sIdx:" + startIndex + " parPage" + itemsPerPage + "]", Toast.LENGTH_LONG).show();
 *   }
 *   public void onFailure(int responseCode, HeaderIterator headers, String response) {
 *     Toast.makeText(this, "FriendCode.loadFriends failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
 *   }
 * });
 * </pre></code>
 * @author GREE, Inc.
 */
public class FriendCode {
  private static final String TAG = "FriendCode";
  private static boolean debug = false;

  // Data class of FriendCode structure.
 
/**
 * Data class, friend code information structure.
 */
  public class Code {
    private String code;
    private String expire_time;

   
  /**
   * Gets the friends code String.
   * @return friend code string.
   */
    public String getCode()             { return code; }
   
  /**
   * Gets the expired time from the friends code class.
   * @return friend code's expire time string.
   */
    public String getExpireTime()       { return expire_time; }
  }

  // Data class of user data structure related to friend code.
 
/**
 * Friend User Data structure, related to friend code function.
 */
  public class Data {
    private String id;

   
  /**
   * Gets the GREE UserId String.
   * @return GREE user id string.
   */
    public String getUserId() { return id; }
  }

 
  /**
   * Delivers friend code results and friend code information. <br>
   * This is used when creating a friend code and when inquiring about a code that is already created.
   */
  public interface CodeListener {
   
  /**
   * This interface is called when the friend listener successfully process.
   * @param code structure of friend code data which you request.
   */
    public void onSuccess(Code code);

   
  /**
   * Called when an error occurs when trying to create new a friend code.
   * @param responseCode : network error code.
   * @param headers : the response header iterator.
   * @param response : error message body.
   */
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

 
  /**
   * Interface dealing with the entry user list based on input from the friend code results.
   * This is used when getting the entry user list based on the input of a friend code.
   */
  public interface EntryListGetListener {
   
  /**
   * Called on a successful retrieval of the entry user list.
   * @param startIndex absolute index which we want to retrieve the entry user list.
   * @param itemsPerPage max number of items which we want displayed per page.
   * @param totalResults max number of items retrieved in one request from the server.
   * @param entries array of user information, inputs based on friend code.
   */
    public void onSuccess(int startIndex, int itemsPerPage, int totalResults, Data[] entries);
   
  /**
   * Called when a error occurs during retrieval of the entry user list.
   * @param responseCode network error code.
   * @param headers the response header iterator.
   * @param response error message body.
   */
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

 
  /**
   * Interface that listens to the handling of obtaining owner information.
   */
  public interface OwnerGetListener {
   
  /**
   * Called on a successful retrieval of the owner information.
   * @param owner Data structure of owner information who have friend code you verified.
   */
    public void onSuccess(Data owner);
   
  /**
   * Called when a error occurs during retrieval of the owner information.
   * @param responseCode the network error code.
   * @param headers the response header iterator.
   * @param response error message body.
   */
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

 
  /**
   * Interface that handles the results of FriendCode method calls.
   * 
   */
  public interface SuccessListener {
 
  /**
   * Called on a successful retrieval of the friend code.
   */
    public void onSuccess();
    
   
  /**
   * Called when a error occurs during retrieval of friend code.
   * @param responseCode the network error code.
   * @param headers the response header iterator.
   * @param response error message body.
   */
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Data class of return Data structure from create method.
   */
  private class CodeCreateResponse {
    public Code entry;
  }

  /**
   *  Data class of return Data structure from getOwner method.
   */
  private class OwnerGetResponse {
    public Data entry;
  }

  /**
   *  Data class of return Data structure from getEntryList method.
   */
  private class EntryListGetResponse {
    public int totalResults;
    public int itemsPerPage;
    public int startIndex;
    public Data[] entry;
  }

  /**
   *  Data class of return Data structure from getCode method.
   */
  private class CodeGetResponse {
    public Code entry;
  }

 
/**
 * Loads the Friend Codes after they are already created by the server.
 *
 * @param listener - It receives results.
 */
  public static void loadCode(final CodeListener listener) {
    Request request = new Request();
    String action = "/friendcode/@me/@self";
    if (debug) { GLog.d(TAG + ":loadCode", action); }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_FRIENDCODE_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(action, "GET", null, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) { GLog.d(TAG + ":loadCode", "Http response:" + json); }
        CodeGetResponse response;
        try {
          response = Injector.getInstance(Gson.class).fromJson(json, CodeGetResponse.class);
        } catch (Exception ex) {
          GLog.printStackTrace(TAG + ":loadCode", ex);
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
          return;
        }
        if (listener != null && response != null) {
          listener.onSuccess(response.entry);
        }
      }
      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

 
/**
 * Loads the entry user id list, based on input of friend code to the server.
 *
 * @param startIndex absolute index which we want to get friends from on the server. Default is 1.
 * @param count : Max num we want to get from the server. default is 10.
 * @param listener : It receives results.
 */
  public static void loadFriends(int startIndex, int count, final EntryListGetListener listener) {
    Request request = new Request();
    String action = "/friendcode/@me/@friends";
    if (debug) { GLog.d(TAG + ":loadFriends", action); }

    TreeMap<String, Object> args = new TreeMap<String, Object>();
    if (startIndex > 0) {
      // Optional data. If not input or invalid num, use default parameter "1".
      args.put("startIndex", Integer.toString(startIndex));
    }
    if (count > 0) {
      // Optional data. If not input or invalid num, use default parameter "10".
      args.put("count", Integer.toString(count));
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_FRIENDCODE_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(action, "GET", args, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) { GLog.d(TAG + ":loadFriends", "Http response:" + json); }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
        manager.flushData(performData);
        EntryListGetResponse response;
        try {
          response = Injector.getInstance(Gson.class).fromJson(json, EntryListGetResponse.class);
        } catch (Exception ex) {
          GLog.printStackTrace(TAG + ":loadFriends", ex);
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
          return;
        }
        if (listener != null && response != null) {
          listener.onSuccess(response.startIndex, response.itemsPerPage, response.totalResults, response.entry);
        }
      }
      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

 
/**
 * Loads the owner information from user input friend code.
 *
 * @param listener receives results of this load.
 */
  public static void loadOwner(final OwnerGetListener listener) {
    Request request = new Request();
    String action = "/friendcode/@me/@owner";
    if (debug) { GLog.d(TAG + ":loadOwner", action); }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_FRIENDCODE_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(action, "GET", null, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) { GLog.d(TAG + ":loadOwner", "Http response:" + json); }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
        manager.flushData(performData);
        OwnerGetResponse response;
        try {
          response = Injector.getInstance(Gson.class).fromJson(json, OwnerGetResponse.class);
        } catch (Exception ex) {
          GLog.printStackTrace(TAG + ":loadOwner", ex);
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
          return;
        }
        if (listener != null) {
          listener.onSuccess(response.entry);
        }
      }
      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

 
/**
 * Creates a new friend code.
 *
 * @param expireTime the date which you want to set the friend code to expire. Format is UTC String - "YYYY-MM-DDThh:mm:ss+hhmm".
 * @param listener which the receives results of this call.
 * @return boolean returns false if there is a parse error, otherwise true.
 */
  public static boolean requestCode(String expireTime, final CodeListener listener) {
    //Check the formatting of expireTime i.e. YYYY-MM-DDThh:mm:ss+hhmm
    if (expireTime == null || expireTime.length() != 24){
      return false;
    }
    else{
      //Create a SimpleDateFormat to compare our expireTime input with
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
      
      //Try to parse our expireTime with the set format
      try{
        sdf.parse(expireTime);
      }
      catch (ParseException e){
        return false;
      }
    }
    
    Request request = new Request();
    String action = "/friendcode/@me";
    if (debug) { GLog.d(TAG + ":requestCode", action); }
    TreeMap<String, Object> data = new TreeMap<String, Object>();
    if (expireTime != null) {
      data.put("expire_time", expireTime);
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_FRIENDCODE_POST);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(action, "POST", data, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) { GLog.d(TAG + ":requestCode", "Http response:" + json); }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
        manager.flushData(performData);
        CodeCreateResponse response;
        try {
          response = Injector.getInstance(Gson.class).fromJson(json, CodeCreateResponse.class);
        } catch (Exception ex) {
          GLog.printStackTrace(TAG + ":requestCode", ex);
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
          return;
        }
        if (listener != null && response != null) {
          listener.onSuccess(response.entry);
        }
      }
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
    return true;
  }

 
/**
 * Verifies the user's friend code.
 *
 * @param code user's friend code which you want to verify.
 * @param listener receives results of the call.
 */
  public static void verifyCode(String code, final SuccessListener listener) {
    Request request = new Request();
    // if not exist friendcode, throw exception. It is necessary.
    if (code == null) { throw new InvalidParameterException("code must be exist when entry method called."); }
    String action = "/friendcode/@me/" + code;
    if (debug) { GLog.d(TAG + ":verifyCode", action); }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_FRIENDCODE_POST);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(action, "POST", null, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) { GLog.d(TAG + ":verifyCode", "Http response:" + json); }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
        manager.flushData(performData);
        if (listener != null) {
          listener.onSuccess();
        }
      }
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

 
/**
 * Deletes the friend code from the server.
 *
 * @param listener receives the results of the call to remove the friend code.
 */
  public static void deleteCode(final SuccessListener listener) {
    Request request = new Request();
    String action = "/friendcode/@me";
    if (debug) { GLog.d(TAG + ":deleteCode", action); }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_FRIENDCODE_DELETE);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    request.oauthGree(action, "DELETE", null, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) { GLog.d(TAG + ":deleteCode", "Http response:" + json); }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
        manager.flushData(performData);
        if (listener != null) {
          listener.onSuccess();
        }
      }
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
}
