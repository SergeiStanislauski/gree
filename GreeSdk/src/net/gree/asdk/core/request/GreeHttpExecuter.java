/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.protocol.HttpContext;

import net.gree.asdk.core.GLog;

public class GreeHttpExecuter implements IGreeHttpExecuter {

  private static final String TAG = GreeHttpExecuter.class.getSimpleName();
  protected DefaultRedirectHandler mRedirectHandler = new DefaultRedirectHandler(){
    public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
      return false;
    }
  };

  @Override
  public HttpResponse execute(HttpUriRequest _request) {
    HttpResponse response = null;
    try {
      GreeHttpClient client = new GreeHttpClient();
        // Add temporarily to check redirected response
      client.setRedirectHandler(mRedirectHandler);
      // gzip request
      _request.setHeader("Accept-Encoding", "gzip");
      // Prevent HTTP 1.1 pipelining.
      _request.setHeader("Connection", "close");
      try {
        response = client.execute(_request);
      } catch (Exception ex) {
        RequestLogger.getInstance().e(TAG, "RealHttpExecuter.execute failed:" + ex.toString());
      }
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
    return response;
  }
}
