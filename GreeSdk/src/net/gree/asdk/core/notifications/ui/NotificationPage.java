/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.notifications.ui;

import java.util.TreeMap;

import android.app.Activity;

import net.gree.asdk.api.auth.Authorizer.AuthorizeListener;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.dashboard.DashboardActivity;
import net.gree.asdk.core.notifications.MessageDispatcher;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;

public class NotificationPage {

  public static final int LAUNCH_TYPE_LIST_AUTO_SELECT                  = 0;
  public static final int LAUNCH_TYPE_SPECIFIED_INTERNAL_ACTION         = 1;
  public static final int LAUNCH_TYPE_SPECIFIED_EXTERNAL_URL            = 2;
  public static final int LAUNCH_TYPE_PLATFORMAPP_LIST                  = 4;
  public static final int LAUNCH_TYPE_PLATFORMAPP_REQUEST_DETAIL        = 5;
  public static final int LAUNCH_TYPE_PLATFORMAPP_MESSAGE_DETAIL        = 6;

  private static final String ACTION_SHOW_PLATFORMAPP_LIST      = "?action=ggp_timeline&filter=game&header=off";

  private static final String SERVICE_CODE_GAME = "NB0";
  

  private static final String ENDPOINT = Url.getNotificationBoardUrl();

  public static  String getNotificationPlatformAppListUrl() { return ENDPOINT + ACTION_SHOW_PLATFORMAPP_LIST; }
  private static String getNotificationPlatformRequestDetailUrl(String notificationId, String from) { return Url.getGamesUrl() + "service/request/detail/" + notificationId + "?from=" + from; }
  private static String getNotificationPlatformMessageDetailUrl(String notificationId, String from) { return Url.getGamesUrl() + "service/message/detail/" + notificationId + "?from=" + from; }

  private NotificationPage() {}

  public static boolean launch(Activity activity) {
    return launch(activity, LAUNCH_TYPE_PLATFORMAPP_LIST);
  }

  public static boolean launch(Activity activity, int type) {
    return launch(activity, type, null);
  }

  public static boolean launch(final Activity activity, int type, TreeMap<String, Object> params) {
    final MessageDispatcher md = Injector.getInstance(MessageDispatcher.class);
    final String openUrl = getUrl(type, params);
    if (Util.canOptOutOfGREE() && !Injector.getInstance(IAuthorizer.class).hasOAuthAccessToken()) {
      String serviceCode = null;
      switch (type) {
        case LAUNCH_TYPE_PLATFORMAPP_LIST:
          serviceCode = SERVICE_CODE_GAME;
          break;
        default:
          break;
      }
      Injector.getInstance(IAuthorizer.class).authorize(activity, serviceCode, new AuthorizeListener() {
        public void onAuthorized() {
          md.dismissAll(activity.getApplicationContext());
          DashboardActivity.show(activity, openUrl);
        }
        public void onError() {}
        public void onCancel() {}
      }, null);
      return false;
    }
    md.dismissAll(activity.getApplicationContext());
    DashboardActivity.show(activity, openUrl);
    return true;
  }

  private static String getUrl(int type, TreeMap<String, Object> params) {
    String openUrl = ENDPOINT;
    
    if (params == null) {
      params = new TreeMap<String, Object>();
    }
    switch (type) {
      case LAUNCH_TYPE_LIST_AUTO_SELECT:
        openUrl = ENDPOINT;
        break;
      case LAUNCH_TYPE_SPECIFIED_INTERNAL_ACTION:
        openUrl = ENDPOINT;
        if (params.containsKey("action")) {
          openUrl += params.get("action");
        }
        break;
      case LAUNCH_TYPE_SPECIFIED_EXTERNAL_URL:
        openUrl = ENDPOINT;
        if (params.containsKey("url")) {
          openUrl = (String) params.get("url");
        }
        break;
      case LAUNCH_TYPE_PLATFORMAPP_LIST:
        openUrl = getNotificationPlatformAppListUrl();
        break;
      case LAUNCH_TYPE_PLATFORMAPP_REQUEST_DETAIL:
        if (params.containsKey("info-key")) {
          String from = "";
          if (params.containsKey("from")) {
            from = (String) params.get("from");
          }
           openUrl = getNotificationPlatformRequestDetailUrl((String) params.get("info-key"), from);
        }
        break;
      case LAUNCH_TYPE_PLATFORMAPP_MESSAGE_DETAIL:
        if (params.containsKey("info-key")) {
          String from = "";
          if (params.containsKey("from-uid")) {
            from = (String) params.get("from");
          }
          openUrl = getNotificationPlatformMessageDetailUrl((String) params.get("info-key"), from);
        }
        break;
    }
    return openUrl;
  }
}
