/**
 * This package provides annotations that can be used with {@link com.google.net.gree.gson.Gson}.
 * 
 * @author Inderjeet Singh, Joel Leitch
 */
package net.gree.vendor.com.google.gson.annotations;