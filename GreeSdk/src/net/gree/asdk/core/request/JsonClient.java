/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.entity.StringEntity;

import net.gree.asdk.core.analytics.LogDataInputStream;
import net.gree.asdk.core.request.Constants.HTTP_METHOD;
import net.gree.asdk.core.request.Constants.OAUTH_TYPE;
import net.gree.asdk.core.request.helper.MethodHelper;


public class JsonClient {
  public void oauth(String url, String method, Map<String, String> headers, boolean sync,
      OnResponseCallback<String> listener) {
    oauthInternal(url, method, headers, null, sync, listener);
  }

  public void oauth(String url, String method, Map<String, String> headers, String json,
      boolean sync, OnResponseCallback<String> listener) {
    oauthInternal(url, method, headers, json, sync, listener);
  }

  /**
   * Method do the real job
   */
  public void oauth(String url, String method, Object headers, LogDataInputStream mInputStream,
      boolean sync, OnResponseCallback<String> listener) {
    GreeChunkedInputStreamEntity reqEntity = new GreeChunkedInputStreamEntity(mInputStream, -1);
    Map<String, String> jsonHeader = new HashMap<String, String>(1);
    // this is direct add header
    jsonHeader.put("Content-Type", "application/json");
    StringConverter converter = new StringConverter();
    ResponseHandler<String> handler = new ResponseHandler<String>(listener);
    new GreeRequest(url, MethodHelper.parseString(method)).header(jsonHeader).entity(reqEntity)
        .sync(sync).request(converter, handler);
  }

  /**
   * Method do the real job
   */
  private void oauthInternal(String url, String method, Map<String, String> headers, String json,
      boolean sync, OnResponseCallback<String> listener) {
    Map<String, String> jsonHeader = new HashMap<String, String>();
    // this is direct add header
    jsonHeader.put("Content-Type", "application/json");
    if (headers != null)
      jsonHeader.putAll(headers);
    StringEntity entity = null;
    try {
      if (json != null) {
        entity = new StringEntity(json, "UTF-8");
      }
    } catch (UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    StringConverter converter = new StringConverter();
    ResponseHandler<String> handler = new ResponseHandler<String>(listener);
    new GreeRequest(url, MethodHelper.parseString(method)).header(jsonHeader).entity(entity)
        .sync(sync).request(converter, handler);
  }

  public void oauth2(String url, String method, String json, boolean sync,
      OnResponseCallback<String> listener) {
    oauth2(url, MethodHelper.parseString(method), json, sync, listener);
  }

  public void oauth2(String url, int method, String json, boolean sync,
      OnResponseCallback<String> listener) {
    oauth2(url, MethodHelper.parseInt(method), json, sync, listener);
  }

  private void oauth2(String url, HTTP_METHOD methodType, String json, boolean sync,
       OnResponseCallback<String> listener) {
    Map<String, String> jsonHeader = new HashMap<String, String>(1);
    // this is direct add header
    jsonHeader.put("Content-Type", "application/json");
    StringEntity entity = null;
    try {
      if (json != null) {
        entity = new StringEntity(json, "UTF-8");
      }
    } catch (UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    StringConverter converter = new StringConverter();
    OAUTH_TYPE auth = OAUTH_TYPE._2LEGGED;
    ResponseHandler<String> handler = new ResponseHandler<String>(listener).oauth(auth);
    new GreeRequest(url, methodType).header(jsonHeader).entity(entity)
        .oauth(auth).sync(sync).request(converter, handler);
  }

  /**
   * Http request without OAuth. Use {@link #oauth} or {@link #oauth2} if the request should be
   * authorized.
   * 
   * @param url
   * @param method
   * @param json
   * @param sync
   * @param listener
   */
  public void http(String url, int method, String json, boolean sync,
      OnResponseCallback<String> listener) {
    Map<String, String> jsonHeader = new HashMap<String, String>(1);
    // this is direct add header
    jsonHeader.put("Content-Type", "application/json");
    StringConverter converter = new StringConverter();
    StringEntity entity = null;
    try {
      if (json != null) {
        entity = new StringEntity(json, "UTF-8");
      }
    } catch (UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    OAUTH_TYPE auth = OAUTH_TYPE._NONE;
    ResponseHandler<String> handler = new ResponseHandler<String>(listener).oauth(auth);
    new GreeRequest(url, MethodHelper.parseInt(method)).header(jsonHeader)
        .oauth(auth).sync(sync).entity(entity).request(converter, handler);
  }
}
