/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.track;

import java.util.List;

interface TrackItemStorage {

  /**
   * Generate the seal and save the item.
   * 
   * @param item TrackItem to save
   */
  void save(TrackItem item);

  /**
   * Delete the item
   * 
   * @param item TrackItem to delete
   */
  void delete(TrackItem item);

  /**
   * Find all pending items and check the seal. If the seal isn't match, just delete the item.
   * 
   * @return
   */
  List<TrackItem> findAndCheckPendingUpload();
}
