/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.util;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

/**
 * The class that deal with all core data
 */
public class CoreData {
  private static Map<String, Object> sCoreParams = new HashMap<String, Object>();

  private CoreData() {
    // prevent new
  }

  public static void initialize(Context context, Map<String, Object> coreParams) {
    if (context == null) {
      throw new IllegalArgumentException("Null context when initialize CoreData");
    }
    if (coreParams == null) {
      throw new IllegalArgumentException("Null Params when initialize CoreData");
    }
    sCoreParams = coreParams;
  }

  @SuppressWarnings("unused")
  private static final String TAG = CoreData.class.getSimpleName();

  public static boolean containsKey(String key) {
    return sCoreParams.containsKey(key);
  }

  public static Map<String, Object> getParams() {
    return sCoreParams;
  }

  public static String get(String key) {
    try {
      return Util.nullS(sCoreParams.get(key));
    } catch (Exception ex) {}
    return "";
  }

  /**
   * Returns String from specific key
   * 
   * @param key the key
   * @param defaultValue defaultValue
   * @return the key
   */
  public static String get(String key, String defaultValue) {
    try {
      if (sCoreParams.containsKey(key)) {
        return Util.nullS(sCoreParams.get(key));
      }
    } catch (Exception ex) {}
    return defaultValue;
  }

  /**
   * Returns map from specific key
   * 
   * @param key the key
   * @param value the value
   * @return
   */
  public static Map<String, Object> put(String key, String value) {
    sCoreParams.put(key, value);
    return sCoreParams;
  }
}
