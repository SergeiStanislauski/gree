/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.connect;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.entity.StringEntity;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.GreeRequest;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.request.SnsResponseHandler;
import net.gree.asdk.core.request.StringConverter;
import net.gree.asdk.core.request.helper.MethodHelper;

/**
 * Like API.
 * You can 'like' to user's posts (for example mood or photo) by using this class.
 */
public class Like {
  private static final String LOG_TAG = Like.class.getSimpleName();
  private String mApiUrl;
  private GreeRequest mGreeRequest;

  private String mRequestedUrl;
  private int mRequestedMethod;

  public String getApiUrl() {
    return mApiUrl;
  }

  public String getRequestedUrl() {
    return mRequestedUrl;
  }

  public int getRequestedMethod() {
    return mRequestedMethod;
  }

  /**
   * Create the Like API.
   * @param url Connect API URL information.
   */
  public Like(Url url) {
    mApiUrl = url.getConnectUrl() + "/api/rest/like/";
  }

  private String createLikeMoodUrl(String userId, String moodId) {
    StringBuilder sb = new StringBuilder(mApiUrl);
    sb.append(userId).append('/').append(moodId).append("/mood");
    return sb.toString();
  }

  private String createLikePhotoUrl(String userId, String photoId) {
    StringBuilder sb = new StringBuilder(mApiUrl);
    sb.append(userId).append('/').append(photoId).append("/photo");
    return sb.toString();
  }

  /**
   * Like the mood.
   * {@link net.gree.asdk.core.connect.Mood}
   * @param userId user id which own the mood.
   * @param moodId mood id which will be liked.
   * @param listener listener which is receiving result of api call.
   */
  public void likeMood(String userId, String moodId, OnResponseCallback<String> listener) {
    String url = createLikeMoodUrl(userId, moodId);
    request(url, BaseClient.METHOD_POST, listener);
  }

  /**
   * Unlike the mood.
   * {@link net.gree.asdk.core.connect.Mood}
   * @param userId user id which own the mood.
   * @param moodId mood id which will be unliked.
   * @param listener listener which is receiving result of api call.
   */
  public void unlikeMood(String userId, String moodId, OnResponseCallback<String> listener) {
    String url = createLikeMoodUrl(userId, moodId);
    request(url, BaseClient.METHOD_DELETE, listener);
  }

  /**
   * Like the photo.
   * {@link net.gree.asdk.core.connect.Photo}
   * @param userId user id which own the mood.
   * @param photoId photo id which will be liked.
   * @param listener listener which is receiving result of api call.
   */
  public void likePhoto(String userId, String photoId, OnResponseCallback<String> listener) {
    String url = createLikePhotoUrl(userId, photoId);
    request(url, BaseClient.METHOD_POST, listener);
  }

  /**
   * Unlike the photo.
   * {@link net.gree.asdk.core.connect.Photo}
   * @param userId user id which own the mood.
   * @param photoId photo id which will be unliked.
   * @param listener listener which is receiving result of api call.
   */
  public void unlikePhoto(String userId, String photoId, OnResponseCallback<String> listener) {
    String url = createLikePhotoUrl(userId, photoId);
    request(url, BaseClient.METHOD_DELETE, listener);
  }

  private void request(String url, int method, OnResponseCallback<String> listener) {
    try {
      StringEntity entity = new StringEntity("{}", "UTF-8");
      Map<String, String> headers = new HashMap<String, String>();
      headers.put("Content-Type", "application/json");

      StringConverter converter = new StringConverter();
      SnsResponseHandler handler = new SnsResponseHandler(listener);

      if (mGreeRequest == null) {
        new GreeRequest(url, MethodHelper.parseInt(method)).entity(entity).header(headers).sync(true)
        .request(converter, handler);
      } else {
        mGreeRequest.entity(entity).header(headers).sync(true).request(converter, handler);
      }
    } catch (UnsupportedEncodingException e) {
      GLog.printStackTrace(LOG_TAG, e);
    }

    mRequestedUrl = url;
    mRequestedMethod = method;
  }

}
