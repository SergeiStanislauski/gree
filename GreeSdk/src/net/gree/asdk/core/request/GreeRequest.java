/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;

import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.Constants.HTTP_METHOD;
import net.gree.asdk.core.request.Constants.OAUTH_TYPE;
import net.gree.vendor.com.google.gson.Gson;

/**
 * A request for Gree API, this is an abstract class. Subclass should implement
 * {@link #convertResponseBody} that transform response to class T.
 * 
 * @param <T> the type that the need to return, should be the same as T in
 *        {@link OnResponseCallback}
 */
public class GreeRequest {
  private static final String TAG = GreeRequest.class.getSimpleName();

  // Required
  protected String mUrl;
  protected HTTP_METHOD mMethodType;

  // Optional
  protected OAUTH_TYPE mOauthType = OAUTH_TYPE._3LEGGED;
  protected Map<String, String> mHeaders = null;
  protected HttpEntity mEntity = null;
  protected boolean mSync = false;

  protected Gson mGson;

  /**
   * initializer
   * 
   * @param url
   * @param methodType
   * @param listener
   */
  public GreeRequest(String url, HTTP_METHOD methodType) {
    mUrl = url;
    mMethodType = methodType;
    mGson = Injector.getInstance(Gson.class);
  }

  /**
   * Chain setter for the oauthType, see {@link OAUTH_TYPE}
   * 
   * @param oauthType
   * @return this
   */
  public GreeRequest oauth(OAUTH_TYPE oauthType) {
    mOauthType = oauthType;
    return this;
  }

  /**
   * Chain setter for the header
   * 
   * @param headers
   * @return {@code this}
   */
  public GreeRequest header(Map<String, String> headers) {
    mHeaders = headers;
    return this;
  }

  /**
   * Chain setter for the entity
   * 
   * @param entity
   * @return this
   */
  public GreeRequest entity(HttpEntity entity) {
    mEntity = entity;
    return this;
  }

  /**
   * Chain setter
   * 
   * @param sync
   * @return this
   */
  public GreeRequest sync(boolean sync) {
    mSync = sync;
    return this;
  }

  /**
   * Chain setter for parameter, will flatter it to url
   * 
   * @param para
   * @return this
   */
  public GreeRequest parameter(Map<String, String> para) {
    if ((para != null) && (!para.isEmpty())) {
      mUrl += "/?" + RequestUtil.toQueryString(para);
    }
    return this;
  }



  public <T> void request(IConverter<T> converter, ResponseHandler<T> handler) {
    // factory goes here
    GreeBaseClient client = GreeHttpClientFactory.getClient();
    if (client == null) {
      redirect2fail(handler, "client is null");
      return;
    }
    if (mUrl == null) {
      redirect2fail(handler, "url is null");
      return;
    }
    URI uri = getUri(mUrl);
    if (uri == null) {
      redirect2fail(handler, "url could not convert to uri");
      return;
    }
    client.request(uri, mMethodType, mHeaders, mEntity, mSync, handler, converter, mOauthType);
  }

  private <T> void redirect2fail( ResponseHandler<T> handler, String reason) {
    handler.onFailure(HttpStatus.SC_BAD_REQUEST, null, null, reason);
  }

  private URI getUri(String url) {
    try {
      return new URL(url).toURI();
    } catch (URISyntaxException e) {
      RequestLogger.getInstance().e(TAG, e.getMessage());
    } catch (MalformedURLException e) {
      RequestLogger.getInstance().e(TAG, e.getMessage());
    }
    return null;
  }

  /**
   * Getter for url
   * 
   * @return url
   */
  public String getUrl() {
    return mUrl;
  }

  /**
   * Getter for http method
   * 
   * @return HTTP_METHOD
   */
  public HTTP_METHOD getMethodType() {
    return mMethodType;
  }


  /**
   * Getter for OAUTH_TYPE
   * 
   * @return OAUTH_TYPE
   */
  public OAUTH_TYPE getOauthType() {
    return mOauthType;
  }

  /**
   * Getter for header
   * 
   * @return header Map
   */
  public Map<String, String> getHeaders() {
    return mHeaders;
  }

  /**
   * Getter for entity
   * 
   * @return HttpEntity
   */
  public HttpEntity getEntity() {
    return mEntity;
  }

  /**
   * Getter for sync
   * 
   * @return if the request is sync
   */
  public boolean isSync() {
    return mSync;
  }
}
