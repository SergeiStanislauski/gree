/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.codec;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES Encoding Class
 * 
 * @author GREE
 * 
 */
public class AesEnc {
  /**
   * Converts the specified string into its encrypted string with seed string.
   * @param seed the seed to encrypt with
   * @param cleartext the string to convert
   * @return the encrypted string
   * @throws Exception the below exception 
   * <BR> NullPointerException - if algorithm is null.  
   * <BR> IllegalArgumentException - if the key data or the algorithm name is null or if the key data is empty.  
   * <BR> IllegalBlockSizeException - if the size of the resulting bytes is not a multiple of the cipher block size. 
   * <BR> BadPaddingException - if the padding of the data does not match the padding scheme. 
   * <BR> IllegalStateException - if this cipher instance is not initialized for encryption or decryption.  
   * <BR> NoSuchAlgorithmException - if no installed provider can provide the transformation, or it is null, empty or in an invalid format. 
   * <BR> NoSuchPaddingException - if no installed provider can provide the padding scheme in the transformation.  
   * <BR> IndexOutOfBoundsException - if index < 0 or index >= length().  
   */
  public static String encrypt(String seed, String cleartext) throws Exception {
    byte[] rawKey = getRawKey(seed.getBytes());
    byte[] result = encrypt(rawKey, cleartext.getBytes());
    return toHex(result);
  }

  /**
   * Converts the specified string into its decrypted string with seed string.
   * @param seed the seed to decrypt with
   * @param encrypted the string to convert
   * @return the decrypted string
   * @throws Exception the below exception 
   * <BR> NullPointerException - if algorithm is null.  
   * <BR> IllegalArgumentException - if the key data or the algorithm name is null or if the key data is empty.  
   * <BR> IllegalBlockSizeException - if the size of the resulting bytes is not a multiple of the cipher block size. 
   * <BR> BadPaddingException - if the padding of the data does not match the padding scheme. 
   * <BR> IllegalStateException - if this cipher instance is not initialized for encryption or decryption.  
   * <BR> NoSuchAlgorithmException - if no installed provider can provide the transformation, or it is null, empty or in an invalid format. 
   * <BR> NoSuchPaddingException - if no installed provider can provide the padding scheme in the transformation.  
   * <BR> IndexOutOfBoundsException - if index < 0 or index >= length().  
   */
  public static String decrypt(String seed, String encrypted) throws Exception {
    byte[] rawKey = getRawKey(seed.getBytes());
    byte[] enc = toByte(encrypted);
    byte[] result = decrypt(rawKey, enc);
    return new String(result);
  }

  private static byte[] getRawKey(byte[] seed) throws Exception {
    KeyGenerator kgen = KeyGenerator.getInstance("AES");
    SecureRandom sr;
    if (android.os.Build.VERSION.SDK_INT >= 17) { // 17 is JellyBeans 4.2
      // http://stackoverflow.com/questions/13433529/android-4-2-broke-my-encrypt-decrypt-code-and-the-provided-solutions-dont-work
      sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
    } else {
      sr = SecureRandom.getInstance("SHA1PRNG");
    }
    sr.setSeed(seed);
    kgen.init(128, sr); // 192 and 256 bits may not be available
    SecretKey skey = kgen.generateKey();
    byte[] raw = skey.getEncoded();
    return raw;
  }

  private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
    SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    Cipher cipher = Cipher.getInstance("AES");
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
    byte[] encrypted = cipher.doFinal(clear);
    return encrypted;
  }

  private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
    SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    Cipher cipher = Cipher.getInstance("AES");
    cipher.init(Cipher.DECRYPT_MODE, skeySpec);
    byte[] decrypted = cipher.doFinal(encrypted);
    return decrypted;
  }

  /**
   * Converts the specified string into its hexadecimal string.
   * @param txt the string to convert
   * @return the hexadecimal string
   */
  public static String toHex(String txt) {
    return toHex(txt.getBytes());
  }

  /**
   * Converts the specified hexadecimal string into its normal string.
   * @param hex the string to convert
   * @return the normal string
   */
  public static String fromHex(String hex) {
    return new String(toByte(hex));
  }

  /**
   * Converts the specified hexadecimal string into its byte arrays.
   * @param hexString the string to convert
   * @return the byte arrays
   */
  public static byte[] toByte(String hexString) {
    int len = hexString.length() / 2;
    byte[] result = new byte[len];
    for (int i = 0; i < len; i++) {
      result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
    }
    return result;
  }

  /**
   * Converts the specified byte arrays into its hexadecimal string.
   * @param buf the bytes to convert
   * @return the hexadecimal string
   */
  public static String toHex(byte[] buf) {
    if (buf == null) {
      return "";
    }
    StringBuffer result = new StringBuffer(2 * buf.length);
    for (int i = 0; i < buf.length; i++) {
      appendHex(result, buf[i]);
    }
    return result.toString();
  }

  private static final String HEX = "0123456789ABCDEF";

  private static void appendHex(StringBuffer sb, byte b) {
    sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
  }
}
