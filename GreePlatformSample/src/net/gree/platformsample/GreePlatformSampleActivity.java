/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.platformsample;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.auth.Authorizer;
import net.gree.asdk.api.auth.Authorizer.AuthorizeListener;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.TaskEventDispatcher;
import net.gree.platformsample.adapter.RootItemAdapter;
import net.gree.platformsample.incentive.IncentiveActivity;
import net.gree.platformsample.util.SampleUtil;
import net.gree.platformsample.wrapper.RootItem;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.example.android.apis.graphics.kube.Kube;

/**
 * Activity for the Root page
 *
 */
public class GreePlatformSampleActivity extends BaseActivity implements GreePlatformListener.TaskEventListener {
  public static final String TAG = "GreePlatformSampleActivity";
  private RootItemAdapter adapter;
  final List<RootItem> data = getData();
  private Button loginButton;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.root_page);

    Injector.getInstance(TaskEventDispatcher.class).registerTaskEventListener(this, true);

    Uri uri = getIntent().getData();
    if (uri != null) {
      String args = uri.toString();
      Log.i(TAG, "Intent Data:" + args);
    }

    // check if this activity from a gift list of game notice board 
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      if (extras.containsKey(GreePlatform.GREEPLATFORM_ARGS)) {
        try {
          JSONObject args = new JSONObject(extras.getString(GreePlatform.GREEPLATFORM_ARGS));
         
          String requestType = null;
          try {
            requestType = args.getString(getResources().getString(R.string.gift_attr_name));
          } catch (JSONException e) {
            e.printStackTrace();
          }
          if (requestType != null) {
            // if this activity from a gift list of game notice board,
            // received gift activity will showed 
            Intent intent = new Intent(GreePlatformSampleActivity.this, ReceiveGiftActivity.class);
            startActivity(intent);
          } else {
            // or just show arguments
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("GREEPLATFORM_ARGS");
            alertDialogBuilder.setMessage(URLDecoder.decode(args.toString()));
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
              }
            });
            alertDialogBuilder.create().show();
            Log.i(TAG, "Intent GreeArgs(raw):" + args.toString());
            Log.i(TAG, "Intent GreeArgs(decode):" + URLDecoder.decode(args.toString()));
          }
          
        } catch (JSONException e) {
          Log.e(TAG, "Not exist gree args data.");
        }
      }
    }

    declearProfile();

    // set up the list view
    doneLoading = true;
    loginButton = (Button) findViewById(R.id.root_login_button);

    loginButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
    	authorizeForCustomLogin(); 
      }
    });
    list = (ListView) findViewById(R.id.root_list);
    adapter = new RootItemAdapter(GreePlatformSampleActivity.this, data);
 
    // Logs out user if SharedPref data is preserved after switching environments
    // This check is only for the development version. Developers should not release their app with that kind of check
    SampleUtil.environmentCheck(GreePlatformSampleActivity.this);
    
    if (!SampleUtil.isReallyAuthorized()){
    	authorizeForCustomLogin();
    }    
  }

  private void authorize() {
    Authorizer.authorize(GreePlatformSampleActivity.this, new AuthorizeListener() {
      public void onAuthorized() {
        // do nothing on normal
    	// if sandbox, might need to invalidate the view on success
    	Message msg = mHandler.obtainMessage();
    	// notify the UI change to occur
    	mHandler.sendMessage(msg);
      }

      public void onCancel() {
    	AsyncErrorDialog errorDialog = new AsyncErrorDialog(GreePlatformSampleActivity.this);
      	errorDialog.show();
      }

      public void onError() {
        SampleUtil.showError(GreePlatformSampleActivity.this, "Login");
      }
    }, null, Authorizer.NORMAL_USER);
  }
  
  /**
   * Handles the authorization logic for the custom login screens displayed. Only called if the default value is changed 
   * to display/use this logic.
   */
  private void authorizeForCustomLogin(){
	  // if we have a token, then we can just proceed per normal
	  if(SampleUtil.hasOAuthAccessToken()){
		 authorize();
	  }
	  // if no token, let's get one
	  else{
		  Intent intent = new Intent(GreePlatformSampleActivity.this, GreeCustomSplashScreenActivity.class);
		  startActivity(intent);	  
	  }
  }


  final Handler mHandler = new Handler(){
	@Override
	public void handleMessage(Message msg){
		updateResultsInUi();  
	}
  };  
  
  /** 
   * Makes a call to refresh the UI on a successful login
   */
  private void updateResultsInUi() {
  	tryLoginAndLoadProfilePage();
  	populateRootItem(SampleUtil.isReallyAuthorized());
  }

  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    Log.i(TAG, "onResume");
    setUpBackButton();
    tryLoginAndLoadProfilePage();
    populateRootItem(SampleUtil.isReallyAuthorized());
    list.setAdapter(adapter);
  }

  // Only Override in the Root Activity
  @Override
  protected void setUpBackButton() {
    back = (Button) findViewById(R.id.btn_back);
    if (back != null) {
      back.setVisibility(View.INVISIBLE);
    } else {
      Log.e(TAG, "no back button");
    }
  }

  // Only Override in the Root Activity， because the root activity act differently
  @Override
  protected boolean tryLoginAndLoadProfilePage() {
    if (!SampleUtil.isReallyAuthorized()) {
      loadProfile();
    } else {
      Log.i(TAG, "already logged in, try load profile");
      loadProfile();
    }
    return true;
  }

  private List<RootItem> getData() {
    return new ArrayList<RootItem>();
  }

  private void populateRootItem(boolean isLogined) {
    if (!isLogined) {
      list.setVisibility(View.INVISIBLE);
      loginButton.setVisibility(View.VISIBLE);
      return;
    }
    data.clear();
    // add the leader boards
    Drawable icon = getResources().getDrawable(R.drawable.icn_root_leaderboards);
    Intent intent = new Intent(GreePlatformSampleActivity.this, LeaderBoardListActivity.class);
    String title = getResources().getString(R.string.leaderboard_title);
    RootItem item = new RootItem(title, icon, intent);
    data.add(item);

    // add achievements
    Drawable icon2 = getResources().getDrawable(R.drawable.icn_root_achievements);
    Intent intent2 = new Intent(GreePlatformSampleActivity.this, AchievementListActivity.class);
    String title2 = getResources().getString(R.string.achievement_title);
    RootItem item2 = new RootItem(title2, icon2, intent2);
    data.add(item2);

    // add sharing
    Drawable icon3 = getResources().getDrawable(R.drawable.icn_root_sharing);
    Intent intent3 = new Intent(GreePlatformSampleActivity.this, ShareDialogActivity.class);
    String title3 = getResources().getString(R.string.share_title);
    RootItem item3 = new RootItem(title3, icon3, intent3);
    data.add(item3);

    // requests activity
    Drawable icon4 = getResources().getDrawable(R.drawable.icn_root_requests);
    Intent intent4 = new Intent(GreePlatformSampleActivity.this, RequestSelectActivity.class);
    String title4 = getResources().getString(R.string.request_title);
    RootItem item4 = new RootItem(title4, icon4, intent4);
    data.add(item4);

    // invite activity
    Drawable icon5 = getResources().getDrawable(R.drawable.icn_root_invites);
    Intent intent5 = new Intent(GreePlatformSampleActivity.this, InviteDialogActivity.class);
    String title5 = getResources().getString(R.string.invite_title);
    RootItem item5 = new RootItem(title5, icon5, intent5);
    data.add(item5);

    // friends activity
    Drawable icon6 = getResources().getDrawable(R.drawable.icn_root_friends);
    Intent intent6 = new Intent(GreePlatformSampleActivity.this, FriendListActivity.class);
    String title6 = getResources().getString(R.string.friend_title);
    RootItem item6 = new RootItem(title6, icon6, intent6);
    data.add(item6);

    // payments activity
    Drawable icon7 = getResources().getDrawable(R.drawable.icn_root_payments);
    Intent intent7 = new Intent(GreePlatformSampleActivity.this, PaymentSelectActivity.class);
    String title7 = getResources().getString(R.string.payment_title);
    RootItem item7 = new RootItem(title7, icon7, intent7);
    data.add(item7);

    // GL Test activity
    Drawable icon8 = getResources().getDrawable(R.drawable.icn_root_gltest);
    Intent intent8 = new Intent(GreePlatformSampleActivity.this, Kube.class);
    String title8 = getResources().getString(R.string.gltest_title);
    RootItem item8 = new RootItem(title8, icon8, intent8);
    data.add(item8);

    // upgrade
    Drawable icon9 = getResources().getDrawable(R.drawable.icn_root_upgrade);
    Intent intent9 = new Intent(GreePlatformSampleActivity.this, UpgradeActivity.class);
    String title9 = getResources().getString(R.string.upgrade_title);
    RootItem item9 = new RootItem(title9, icon9, intent9);
    data.add(item9);

    // friend code activity
    Drawable icon10 = getResources().getDrawable(R.drawable.icn_root_friendcode);
    Intent intent10 = new Intent(GreePlatformSampleActivity.this, FriendCodeActivity.class);
    String title10 = getResources().getString(R.string.friendcode_title);
    RootItem item10 = new RootItem(title10, icon10, intent10);
    data.add(item10);
    
    //Incentive
    Drawable iconIncentive = getResources().getDrawable(R.drawable.icn_root_friends);
    Intent intentIncentive = new Intent(GreePlatformSampleActivity.this, IncentiveActivity.class);
    String titleIncentive = getResources().getString(R.string.incentive_title);
    RootItem itemIncentive = new RootItem(titleIncentive, iconIncentive, intentIncentive);
    data.add(itemIncentive);

    //about
    Drawable icon11 = getResources().getDrawable(R.drawable.icn_root_about);
    Intent intent11 = new Intent(GreePlatformSampleActivity.this, AboutActivity.class);
    String title11 = getResources().getString(R.string.about_title);
    RootItem item11 = new RootItem(title11, icon11, intent11);
    data.add(item11);
   
    list.setVisibility(View.VISIBLE);
    loginButton.setVisibility(View.GONE);
  }

  @Override
  protected void sync(boolean fromStart) {
    // DO nothing
  }

  @Override
  public void onEvent(Map<String, Object> params) {
    String className = "";
    int classType = GreePlatformListener.getClassType(params);
    switch (classType) {
      case GreePlatformListener.EVENT_LOGIN:
        SampleUtil.showSuccess(GreePlatformSampleActivity.this, "Login");
        break;
      case GreePlatformListener.EVENT_LOGOUT:
        SampleUtil.showSuccess(GreePlatformSampleActivity.this, "Logout");
        break;
      case GreePlatformListener.EVENT_USERINFO_RETRIEVED:
        tryLoginAndLoadProfilePage();
        populateRootItem(SampleUtil.isReallyAuthorized());
        break;
    }
  }
}
