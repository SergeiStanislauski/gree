/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core;

import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import net.gree.asdk.core.auth.AuthorizerCore;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.notifications.NotificationCounts;
import net.gree.asdk.core.notifications.c2dm.GreeC2DMUtil;
import net.gree.asdk.core.storage.SyncedStore;
import org.apache.http.HeaderIterator;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.content.Context;
import android.text.TextUtils;
import android.webkit.WebView;

import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.GreeUser.GreeUserListener;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.analytics.performance.PerformanceManager;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.appinfo.ApplicationInfo;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.storage.LocalStorage;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Preconditions;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;
import net.gree.vendor.com.google.gson.Gson;

/**
 * Implements GREE Core methods It uses by only GREE SDK internal classes.
 * 
 * @author GREE, Inc.
 * 
 *         !This class should be the only singleton on all classes
 */

public class Core {
  private static final String TAG = "Gree";
  private static final String SDK_VERSION = "3.4.3";
  /**
   * Returns SDK Version.
   * 
   * @return version in String format ex: "1.0.0"
   */
  public static String getSdkVersion() {
    return SDK_VERSION;
  }

  private static final String SDK_BUILD = "release/v3.4.3_46";

  /**
   * GREE official SNS Application information.
   */
  private static String GREEAPP_PACKAGENAME = "jp.gree.android.app";
  private static String GREEAPP_PRODUCTION_APP_ID = "370";
  private static String GREEAPP_DEVELOP_APP_ID = "1350";
  public static final String APPLICATIONID_MISSING = "ApplicationId parameter missing";

  private static String mAppVersion;

  private static LocalStorage mLocalStorage;

  /**
   * Returns App Version padded with zero.
   * 
   * @return version in String format ex: "0001.00.00"
   */
  public static String getAppVersion() {
    if (mAppVersion != null)
      return mAppVersion;

    String versionName = Util.getVersionName(sContext, sContext.getPackageName());
    if (!versionName.matches("^[0-9]+\\.[0-9]+\\.[0-9]+$")) {
      versionName = "0.0.0";
    }
    String[] versions = versionName.split("\\.");
    try {
      mAppVersion =
          String.format(Locale.US, "%04d.%02d.%02d", Integer.parseInt(versions[0]),
              Integer.parseInt(versions[1]), Integer.parseInt(versions[2]));
    } catch (NumberFormatException e) {
      GLog.e(TAG, versionName + " is illegal format. android:versionName must be ####.##.##");
      mAppVersion = "0000.00.00";
    }
    return mAppVersion;
  }
  
  private static boolean isDebug = false;

  // Currently logged in user
  private GreeUser mLocalUser;

  /**
   * 
   * @return is debugging
   */
  public boolean debugging() {
    return isDebug;
  }

  private static Core instance;
  private Context mContext;
  private static Context sContext;

  private SyncedStore mPrefs;

  private String mUserAgent;

  /**
   * Returns the singleton instance. This called after initialized.
   * 
   * @return GreeCore instance
   */
  public static Core getInstance() {
    if (instance == null) {
      throw new RuntimeException("Not initialized GREE!");
    }
    return instance;
  }

  private Core(Context context) {
    mContext = context;
    sContext = context;

    //enforce to run on the UI thread
    Util.runOnUiThread(new Runnable() {
      public void run() {
        mUserAgent = new WebView(mContext).getSettings().getUserAgentString();
      }
    });
  }

  public String getGreeAppPackageName() {
    return GREEAPP_PACKAGENAME;
  }
  /**
   * 
   * @return gree app id
   */
  public String getGreeAppId() {
    String developmentMode = CoreData.get(InternalSettings.DevelopmentMode);
    if (developmentMode != null && developmentMode.startsWith("develop")) {
      return GREEAPP_DEVELOP_APP_ID;
    }
    return GREEAPP_PRODUCTION_APP_ID;
  }

  /**
   * @return if current app is GreeApp, return true, otherwise return false.
   */
  public static boolean isGreeApp() {
    ApplicationInfo appInfo = Injector.getInstance(ApplicationInfo.class);
    if (appInfo == null) {
      throw new RuntimeException(APPLICATIONID_MISSING);
    }
    return appInfo.isSnsApp();
  }

  /**
   * 
   * @return sdk_build string
   */
  public static String getSdkBuild() {
    return SDK_BUILD;
  }

  /**
   * Initialize GREE internal structures.
   * 
   * @param context Android Application Context
   */
  @SuppressWarnings("unchecked")
  public static void initialize(Context context, TreeMap<String, Object> _params) {
    Preconditions.checkArgument(context, "Null context when initialize GreeCore");
    Preconditions.checkArgument(_params, "Null params when initialize GreeCore");

    // new
    instance = new Core(context);

    // initialize CoreData first before everything!
    // use clone here
    CoreData.initialize(context, (Map<String, Object>) _params.clone());

    // initialize Injector
    Injector.init(context);

    int productionGreeAppId = context.getResources().getInteger(RR.integer("gree_app_production_app_id"));
    int developGreeAppId = context.getResources().getInteger(RR.integer("gree_app_develop_app_id"));

    GREEAPP_PRODUCTION_APP_ID = String.valueOf(productionGreeAppId);
    GREEAPP_DEVELOP_APP_ID = String.valueOf(developGreeAppId);

    GREEAPP_PACKAGENAME = context.getResources().getString(RR.string("gree_app_package_name"));

    //initialize LocalStorage and load local settings to CoreData
    instance.storeLocalSettings(CoreData.getParams());
    instance.loadLocalSettings();

    // apply debug configure
    debugConfigure();

    // initialize Utility classes
    setApplicationInfo(CoreData.get(InternalSettings.ApplicationId));
    Url.initialize(CoreData.get(InternalSettings.DevelopmentMode),
        CoreData.get(InternalSettings.ServerUrlSuffix));

    // initialize AuthorizerCore which must be done until CoreData is completely initailized
    AuthorizerCore authorizerCore = (AuthorizerCore) Injector.getInstance(IAuthorizer.class);
    authorizerCore.initialize();

    CookieStorage.initialize();

    // PerformanceCheck configure.
    if (CoreData.containsKey(InternalSettings.EnablePerformanceLogging) && CoreData.get(InternalSettings.EnablePerformanceLogging).equals("true")) {
      Injector.bind(IPerformanceManager.class, new PerformanceManager(context));
    }
    else {
      Injector.bind(IPerformanceManager.class, new IPerformanceManager(context));
    }

    RemoteConfiguration.loadFromServer();
    Injector.getInstance(NotificationCounts.class).updateCounts();
    GreeC2DMUtil.initialize(context);
    Logger.startActiveTimer();
    instance.getLocalUser();
  }

  private static void debugConfigure() {
    if (CoreData.containsKey(InternalSettings.EnableLogging)) {
      if ("true".equals(CoreData.get(InternalSettings.EnableLogging))
          && CoreData.containsKey(InternalSettings.LogLevel)) {
        isDebug = true;
        String logLevel = CoreData.get(InternalSettings.LogLevel);
        GLog.i(TAG, "Log Level from configure:" + logLevel);
        int level = Integer.parseInt(logLevel);
        GLog.setLevel(level);
        GLog.i(TAG, "Set Log Level:" + GLog.getLevel().name());
        if (CoreData.containsKey(InternalSettings.WriteToFile)) {
          String filePath = CoreData.get(InternalSettings.WriteToFile);
          if (filePath != null) {
            GLog.debugFile(filePath);
          }
        }
      }
    } else {
      GLog.setLevel(GLog.ERROR);
      GLog.i(TAG, "Set Log Level:" + GLog.getLevel().name());
    }
  }
  
  /**
   * Returns context.
   * 
   * @return Application Context
   */
  public Context getContext() {
    return mContext;
  }


  /**
   * By executing this function, values of parameters are loaded from local storage.
   */
  public void loadLocalSettings() {

    Map<String, ?> map = getLocalStorage().getParams();
    if (map == null) {
      return;
    }
    Set<String> keys = map.keySet();
    if (keys != null) {
      for (String key : keys) {
        String value = map.get(key).toString();
        if (value != null) {
          CoreData.put(key, value);
        }
      }
    }
  }

  /**
   * This function store the value to local storage.
   * @param key the key of parameter
   * @param value the value of parameter
   */
  public void storeLocalSetting(String key, String value) {
    if (TextUtils.isEmpty(key) || TextUtils.isEmpty(value)) {
      return;
    }
    if (!InternalSettings.canStoreLocalStorage(key)) {
      return;
    }
    CoreData.put(key, value);

    LocalStorage localStorage = getLocalStorage();
    localStorage.putString(key, value);
  }

  /**
   * This function store the values to local storage.
   * @param params Instance of map have key/value storing to local storage.
   */
  public void storeLocalSettings(Map<String, Object> params) {
    if (params == null) {
      return;
    }
    TreeMap<String,String> storedParams = new TreeMap<String,String>();
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      String key = entry.getKey();
      if (InternalSettings.canStoreLocalStorage(key)) {
        String value = entry.getValue().toString();
        CoreData.put(key, value);
        storedParams.put(key, value);
      }
    }
    LocalStorage localStorage = getLocalStorage();
    localStorage.putAll(storedParams);
  }

  /**
   * Returns a Object represents the user.
   * 
   * @return GreeUser object
   */
  public SyncedStore getPrefs() {
    if (mPrefs == null) {
      mPrefs = new SyncedStore(getContext());
    }
    return mPrefs;
  }

  /**
   * Returns which own application process is not in foreground.
   * 
   * @return if true, the process is in background from the user.
   */
  public boolean isInBackground() {
    Context context = getInstance().getContext();

    boolean isLocked =
        ((KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE))
            .inKeyguardRestrictedInputMode();
    boolean isBackground = false;
    for (RunningAppProcessInfo info : ((ActivityManager) context
        .getSystemService(Context.ACTIVITY_SERVICE)).getRunningAppProcesses()) {
      if (info.processName.equals(context.getPackageName())) {
        if (info.importance > RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
          isBackground = true;
          break;
        }
      }
    }
    return (isBackground || isLocked);
  }

  private final static String LOCAL_USER_KEY = "GreeLocalUser";

  /**
   * Retrieve the current user from the local database.
   * 
   * @return the currently logged in user information, or null if not available
   */
  public GreeUser getLocalUser() {
    if (mLocalUser != null) {
      return mLocalUser;
    }
    Gson gson = Injector.getInstance(Gson.class);
    String localUserJson = getLocalStorage().getString(LOCAL_USER_KEY);
    if (null == localUserJson || localUserJson.trim().length() == 0) {
      return null;
    }
    return gson.fromJson(localUserJson, GreeUser.class);
  }

  /**
   * This will try to retrieve the current user info from the server and save it as the local user.
   * 
   * @param listener the listener to be called upon GreeUser request
   */
  public void updateLocalUser(final GreeUserListener listener) {
    GreeUser.loadUserWithId("@me", new GreeUserListener() {
      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, response);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }

      @Override
      public void onSuccess(int index, int totalCount, GreeUser[] users) {
        if (users.length > 0) {
          mLocalUser = users[0];
          // save self to local db
          Gson gson = Injector.getInstance(Gson.class);
          String localUserAsJson = gson.toJson(users[0]);
          getLocalStorage().putString(LOCAL_USER_KEY, localUserAsJson);
        }
        if (listener != null) {
          listener.onSuccess(index, totalCount, users);
        }
      }
    });
  }

  /**
   * Remove the local user information.
   */
  public void removeLocalUser() {
    getLocalStorage().remove(LOCAL_USER_KEY);
    mLocalUser = null;
  }

  /**
   * Returns default user agent
   * @return user agent
   */
  public String getUserAgent() {
    return mUserAgent;
  }

  private static LocalStorage getLocalStorage() {
    if (null == mLocalStorage) {
      mLocalStorage = Injector.getInstance(LocalStorage.class);
    }
    return mLocalStorage;
  }

  /**
   * Set self application id.
   */
  public static void setApplicationInfo(String id) {
    ApplicationInfo appInfo = Injector.getInstance(ApplicationInfo.class);
    appInfo.setAppId(Util.check(id, APPLICATIONID_MISSING));
  }

  /**
   * Get is greeapp.
   * @return self app id
   */
  public static String getAppId() {
    ApplicationInfo appInfo = Injector.getInstance(ApplicationInfo.class);
    if (appInfo == null) {
      throw new RuntimeException(APPLICATIONID_MISSING);
    }
    return appInfo.getAppId();
  }
}
