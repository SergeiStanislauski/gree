package net.gree.asdk.api.incentive;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HeaderIterator;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.incentive.callback.IncentiveGetListener;
import net.gree.asdk.api.incentive.callback.IncentivePostListener;
import net.gree.asdk.api.incentive.callback.IncentivePutListener;
import net.gree.asdk.api.incentive.model.IncentiveEventArray;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;
import net.gree.vendor.com.google.gson.Gson;

/**
 * Implements Gree Incentive API. This API includes get, publish and update the incentive event.
 * <p>     1. Get event, use GET method. The required parameter is the "delivered" parameter.
 *         Returned values are a JSON encoded RestfulCollection.
 * <p>
 *         2. Post event, use POST method, the required parameter is target id and payload type. The
 *         returned value is a JSON encoded value representing the number of row inserted into the
 *         event table.
 * <p>
 *         3. Update event, use PUT method, change the event to delivered status
 */
public class IncentiveController {
  private static final String action = "/incentiverequest";
  private static final String TAG = IncentiveController.class.getSimpleName();

  /**
   * PAYLOAD_TYPE_REQUEST is to send a request to another user using the same application.
   * <p>
   * PAYLOAD_TYPE_INVITE is to send an invitation to another user so that he will come play on the same application.
   * <p>
   * If this parameter is not included then all events will we returned in the request. Using values
   * outside of this range will result in an error.</p>
   */
  public static final String KEY_PAYLOAD_TYPE = "payload_type";

  /**
   * the payload type REQUEST constants, see {@link #KEY_PAYLOAD_TYPE}
   */
  public static final int PAYLOAD_TYPE_REQUEST = 1;

  /**
   * the payload type INVITE constants, see {@link #KEY_PAYLOAD_TYPE}
   */
  public static final int PAYLOAD_TYPE_INVITE = 2;


  /**
   * This is a unix timestamp that represents the ending date for the query. The query will look
   * into the past 30 from the date given. If no timestamp is included a current timestamp will be
   * used.
   */
  public static final String KEY_TIMESTAMP = "timestamp";

  /**
   * This parameter represents the number of events returned for a given request. This parameter
   * should be a positive integer. The max allowed is 100. If "count" is omitted or equal to 0, then
   * the default of 10 is used. If a number outside the allowed range is submitted, an exception is
   * thrown.
   */
  public static final String KEY_COUNT = "count";

  /**
   * This parameter represents the index of the first event that will be returned in the set. The
   * returned set has a zero based index. If this parameter is omitted, the default is 0.
   */
  public static final String KEY_STARTINDEX = "startIndex";

  /**
   * This parameter represent the state of events that are returned. If this parameter is set to "0"
   * then events that have yet to be delivered are returned. If this parameter is set to "1" then
   * events that have been delivered are returned. Using values outside of this range will result in
   * an error.
   */
  private static final String KEY_DELIVERED = "delivered";

  /**
   * This method is used to retrieve the list of events,
   * Using the {@link IncentiveGetListener} callback to return {@link IncentiveEventArray}
   * The required parameter is the "isDelivered" parameter. 
   * Returned values are a JSON encoded RestfulCollection.
   * 
   * Required param: isDelivered
   * 
   * Optional params are passed by the params:
   * 
   * @see #KEY_TIMESTAMP
   * @see #KEY_PAYLOAD_TYPE
   * @see #KEY_COUNT
   * @see #KEY_STARTINDEX
   * 
   * @param isDelivered whether to get the list of delivered or not delivered events
   * @param params the parameters to be attached to the request
   * @param callback the listener to receive the result of this call
   */
  public static void get(boolean isDelivered, Map<String, String> params,
      final IncentiveGetListener callback) {
    GLog.v(TAG, "get");
    if (params == null) {
      params = new HashMap<String, String>();
    }
    if (isDelivered) {
      params.put(KEY_DELIVERED, "1");
    } else {
      params.put(KEY_DELIVERED, "0");
    }
    get(params, callback);
  }

  // private get that get the right query
  private static void get(Map<String, String> params, final IncentiveGetListener callback) {
    GLog.v(TAG, "private get");
    String url = Url.getApiEndpointWithAction(action);
    GLog.v(TAG, url);
    url += "/?" + toQueryString(params);
    GLog.v(TAG, url);

    String userid = GreePlatform.getLocalUserId();
    GLog.v(TAG, "My User id:" + userid);

    new JsonClient().oauth(url, "get", null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        GLog.v(TAG, "onSuccess:" + response);
        Gson gson = Injector.getInstance(Gson.class);
        IncentiveEventArray array = gson.fromJson(response, IncentiveEventArray.class);
        callback.onSuccess(array);
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.v(TAG, "onFailure: code:" + responseCode + " body:" + response);
        callback.onFailure(responseCode, headers, response);
      }
    });

  }

  /**
   * Using the {@link IncentivePutListener} callback to return {@link IncentiveEventArray} 
   * This api will update the events state to "delivered".
   * 
   * Optional params are passed by the params:
   * 
   * @param eventsId List of target event ids for the events to be marked as "delivered."
   * @param callback {@link IncentivePutListener}
   */
  public static void put(List<String> eventsId, final IncentivePutListener callback) {
    GLog.v(TAG, "put");
    StringBuffer sb = new StringBuffer();
    sb.append(Url.getApiEndpointWithAction(action));
    sb.append('/');
    int len = eventsId.size();

    if (len > 0) {
      sb.append(eventsId.get(0));

      for (int i = 1; i < len; i++) {
        sb.append(',');
        sb.append(eventsId.get(i));
      }
    }

    GLog.v(TAG, "put url is:" + sb.toString());
    String method = "put";
    Map<String, String> header = null;
    String entity = null;
    boolean sync = false;
    new JsonClient().oauth(sb.toString(), method, header, entity, sync,
        new OnResponseCallback<String>() {

          @Override
          public void onSuccess(int responseCode, HeaderIterator headers, String response) {
            GLog.v(TAG, "onSuccess:" + response);
            Gson gson = Injector.getInstance(Gson.class);
            IncentiveEventArray array = gson.fromJson(response, IncentiveEventArray.class);
            callback.onSuccess(array);
          }

          @Override
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            GLog.v(TAG, "onFailure: code:" + responseCode + " body:" + response);
            callback.onFailure(responseCode, headers, response);
          }
        });
  }

  /**
   * Post a event to the specific users, with optional parameters.
   * The payload must be in JSON format and have zero or more key value pairs.
   * The key can not be more than 16 ASCII characters and the value not
   * more than 32. The payload as a whole can not exceed more than 1KB.
   * 
   * @param users List of Gree User id
   * @param payloadType This is either the number 1 or 2 depending if this is classified as an
   *        invite or request. Numbers other then 1 or 2 will not be accepted and an exception will
   *        be thrown.
   * @param json payload, The data to be sent, must be JSON and be zero or more key value pairs.
   * @param callback
   */
  public static void post(List<String> users, int payloadType, String json,
      final IncentivePostListener callback) {
    GLog.v(TAG, "post");
    StringBuffer sb = new StringBuffer();
    sb.append(Url.getApiEndpointWithAction(action));
    sb.append('/');
    int len = users.size();

    if (len > 0) {
      sb.append(users.get(0));

      for (int i = 1; i < len; i++) {
        sb.append(',');
        sb.append(users.get(i));
      }
    }

    sb.append('/');
    sb.append(payloadType);

    GLog.v(TAG, "post url is:" + sb.toString());
    new JsonClient().oauth(sb.toString(), "post", null, json, false,
        new OnResponseCallback<String>() {

          @Override
          public void onSuccess(int responseCode, HeaderIterator headers, String response) {
            GLog.v(TAG, "onSuccess:" + response);
            callback.onSuccess(response);
          }

          @Override
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            GLog.v(TAG, "onFailure: code:" + responseCode + " body:" + response);
            callback.onFailure(responseCode, headers, response);
          }
        });
  }

  // Util method
  private static String toQueryString(Map<String, String> params) {
    if (params == null || params.isEmpty())
      return "";
    String queryParam = "";
    for (Map.Entry<String, String> entry : params.entrySet()) {
      queryParam += entry.getKey() + "=" + entry.getValue() + "&";
    }
    queryParam = queryParam.substring(0, queryParam.length() - 1); // trim last &
    return queryParam;
  }
}
