/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.microedition.khronos.opengles.GL10;

import net.gree.asdk.core.GLog;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.SparseArray;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;


/**
 * This is class used to take screenshots.
 * @author GREE, Inc.
 */
public final class ScreenShot {

  private ScreenShot() {
  }
  
  private static boolean mTakeScreenShot = false;
  private static SparseArray<WeakReference<View>> mViews = new SparseArray<WeakReference<View>>();
  private static WeakReference<Canvas> mCanvas;
  private static CountDownLatch mSignal;

/**
 * This is a getter to check whether the next call to the method takeSurfaceViewScreenShot 
 * will actually take a screenshot or not.
 * @return true if the next call to takeSurfaceViewScreenShot method will take a screenshot, false otherwise.
 */
  public static boolean needSurfaceViewScreenShot() {
    return mTakeScreenShot;
  }

  /**
   * This is an interface for the Screenshot api in case you are using a SurfaceView.<br>
   * This is needed to implement your own drawToCanvas method for the ScreenShot api to work properly.<br>
   * <b>Note:</b> if not implemented, you will get a black screen at the coordinates of the SurfaceView.
   */
  public interface SurfaceViewRenderer {
   
   
    /**
     * When this method is called you need to draw on your screen, on the given canvas
     * at the coordinates provided by the SurfaceView.<br>
     * This will be called only when needed to take a screenshot.
     */
   
    void drawToCanvas(Canvas canvas, SurfaceView v);
  }

  /**
   * This is the default OpenGlRenderer.
   * You can use this if you are using an GlSurfaceView.<br>
   * Make sure to call defaultGlRenderer.setGL in your GlSurfaceView.renderer.onSurfaceCreated(GL10 gl, EGLConfig config);
   */
  public static OpenGlRenderer defaultGlRenderer = new OpenGlRenderer();

  /**
   * Use this class if you want to implement your own drawToCanvas method,
   * otherwise you can just use Screenshot.defaultGlRenderer.<br>
   * This is only needed if you are using a GlSurfaceView.
   */
  public static class OpenGlRenderer implements SurfaceViewRenderer {
    private WeakReference<GL10> mgl;

   
   
    /**
     * Call this method when you create your gl environment,
     * a weakReference of the GL10 will be kept.
     */
   
    public void setGL(GL10 gl) {
      if (gl == null) {
        mgl = null;
      } else {
        mgl = new WeakReference<GL10>(gl);
      }
    }

    /**
     * Draw OPENGL content on canvas by using glReadPixels
     * @param Canvas the canvas to write on
     * @param v the GlSurfaceView corresponding to this GL10 instance.
     * @param gl the GL10 Instance that you want to attach.
     */
    @Override
    public void drawToCanvas(Canvas canvas, SurfaceView v) {
      GL10 gl = null;
      if (mgl == null) {
        GLog.e("ScreenShot", "You must call defaultGlRenderer.setGl(gl) before taking a screenshot");
        return;
      }
      gl = mgl.get();
      if (gl == null) {
        GLog.e("ScreenShot", "You must call defaultGlRenderer.setGl(gl) before taking a screenshot");
        return;
      }
      // Get the screen size
      int width = canvas.getWidth();
      int height = canvas.getHeight();
      final int screenshotSize = width*height;
      // building the buffer to hold all the OpenGL info
      ByteBuffer bb = ByteBuffer.allocateDirect(screenshotSize * 4);
      bb.order(ByteOrder.nativeOrder());
      gl.glReadPixels(0, 0, width, height, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, bb);
      int pixelsBuffer[] = new int[screenshotSize];
      bb.asIntBuffer().get(pixelsBuffer);
      bb = null;
      Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
      bitmap.setPixels(pixelsBuffer, screenshotSize-width, -width, 0, 0, width, height);
      pixelsBuffer = null;
      // building the buffer
      short sBuffer[] = new short[screenshotSize];
      ShortBuffer sb = ShortBuffer.wrap(sBuffer);
      bitmap.copyPixelsToBuffer(sb);
      //Making created bitmap (from OpenGL points) compatible with Android bitmap
      for (int i = 0; i < screenshotSize; ++i) {   
    	  // converting BGR-565 to RGB-565
    	  short a = sBuffer[i];
          sBuffer[i] = (short) (((a&0x1f) << 11) | (a&0x7e0) | ((a&0xf800) >> 11));
      }
      sb.rewind();
      bitmap.copyPixelsFromBuffer(sb);
      // set the canvas
      canvas.drawBitmap(bitmap, 0, 0, null);
      // countdown the latch
      mSignal.countDown();
      
    }
  }

  /**
   * This must be called in your GLSurfaceView.Renderer.onDrawFrame(GL10 gl)
   * This method will call the renderer.drawToCanvas method when a screenshot has been requested.
   * @param renderer the SurfaceViewRenderer to be used
   * @param viewId the SurfaceView ressource id that you defined in your xml layout file.
   * @return true if a screenshot has been taken, false otherwise
   */
  public static boolean takeSurfaceViewScreenShot(SurfaceViewRenderer renderer, int viewId) {
    if (mTakeScreenShot) {
      mTakeScreenShot = false;
      WeakReference<View> ref = mViews.get(viewId);
      if ((ref != null) && (mCanvas != null)) {
        View v = ref.get();
        Canvas canvas = mCanvas.get();
        if ((v != null) && (canvas != null)) {
          renderer.drawToCanvas(canvas, (SurfaceView) v);
        }
      }
      mSignal.countDown();
      return true;
    }
    return false;
  }

  /**
   * Parse the view hierarchy to find any SurfaceView,
   * if there is none we don't need to call the SurfaceViewRenderer
   * @param view the root view
   * @return true if one of the child is an instanceof SurfaceView
   */
  private static boolean hasSurfaceView(View view) {
    boolean res = false;

    if (view instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) view;
      int max = group.getChildCount();
      View child;
      for (int i = 0; i < max; i++) {
        child = group.getChildAt(i);
        if (child instanceof SurfaceView) {
          SurfaceView surfView = (SurfaceView) child;
          mViews.put(Integer.valueOf(surfView.getId()), new WeakReference<View>(surfView));
          res = true;
        } else if (child instanceof ViewGroup) {
          if (hasSurfaceView(child)) {
            res = true;
          }
        }
      }
    }
    return res;
  }

/**
 * This is the method you need to call in case you are overriding the default ScreenshotButton.
 * Be aware that if the root view you are passing contains a SurfaceView or GlSurfaceView,
 * you need to use the takeSurfaceViewScreenShot in your onDraw method.
 * @return the bitmap of the screenshot.
 */
   public static Bitmap capture(View view) {
    Activity activity = (Activity) view.getContext();
    view = activity.findViewById(android.R.id.content);

    view.buildDrawingCache();
    Bitmap bmp = view.getDrawingCache();
    if (bmp != null) {
      Bitmap aScreenCacheBitmap = Bitmap.createBitmap(bmp);
      view.setDrawingCacheEnabled(false);
      view.destroyDrawingCache();
      if (hasSurfaceView(view)) {
        mCanvas = new WeakReference<Canvas>(new Canvas(aScreenCacheBitmap));
        mSignal = new CountDownLatch(1);
        mTakeScreenShot = true;
        //Give 2 seconds max to the application to draw
        try { mSignal.await(); } catch (InterruptedException ex) { 
          GLog.printStackTrace("Screenshot", ex);
        }
      }
      return aScreenCacheBitmap;
    } else {
      view.setDrawingCacheEnabled(false);
      view.destroyDrawingCache();
    }
    return null;
  }
}
