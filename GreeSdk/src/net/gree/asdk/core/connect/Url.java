/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.connect;

import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.util.CoreData;

/**
 * Connect API URL.
 */
public class Url {
  private static final String MODE_DEVELOP = "develop";
  private static final String MODE_STAGING = "staging";
  private static final String MODE_DEVELOPSANDBOX = "developSandbox";
  private static final String MODE_STAGINGSANDBOX = "stagingSandbox";
  private static final String MODE_SANDBOX = "sandbox";

  private String mDevelopmentMode;
  private String mServerUrlSuffix;
  private String mConnectUrl;
  private String mSnsUrl;

  private String mDevSuffix;

  /**
   * Create the Connect API URL.
   */
  public Url() {
    this(CoreData.get(InternalSettings.DevelopmentMode), CoreData.get(InternalSettings.ServerUrlSuffix));
  }

  public Url(String developmentMode, String serverUrlSuffix) {
    mDevelopmentMode = developmentMode;
    mServerUrlSuffix = serverUrlSuffix;

    initialize();
  }

  private void initialize() {
    if (mDevelopmentMode == null) {
      mDevelopmentMode = MODE_SANDBOX;
    }

    if (mDevelopmentMode.equals(MODE_DEVELOP)) {
      mDevSuffix = "-dev-" + mServerUrlSuffix + ".dev";
    } else if (mDevelopmentMode.equals(MODE_DEVELOPSANDBOX)) {
      mDevSuffix = "-sb-dev-" + mServerUrlSuffix + ".dev";
    } else if (mDevelopmentMode.equals(MODE_STAGINGSANDBOX)) {
      mDevSuffix = "-sb-" + mServerUrlSuffix;
    } else if (mDevelopmentMode.equals(MODE_STAGING)) {
      mDevSuffix = "-" + mServerUrlSuffix;
    } else if (mDevelopmentMode.equals(MODE_SANDBOX)) {
      mDevSuffix = "-sb";
    } else {
      mDevSuffix = "";
    }

    getConnectUrl();
    getSnsUrl();
  }

  /**
   * Get the Connect API url.
   * @return the Connect API url.
   */
  public String getConnectUrl() {
    if (mConnectUrl == null) {
      if (mDevelopmentMode.equals(MODE_DEVELOP)) {
        mConnectUrl = "http://connect" + mDevSuffix + ".gree.jp";
      } else {
        mConnectUrl = "http://connect" + mDevSuffix + ".gree.net";
      }
    }

    return mConnectUrl;
  }

  /**
   * Get the url of the SNS.
   * @return the url of the SNS.
   */
  public String getSnsUrl() {
    if (mSnsUrl == null) {
      if (mDevelopmentMode.equals(MODE_DEVELOP)) {
        mSnsUrl = "http://sns" + mDevSuffix + ".gree-dev.net";
      } else {
        mSnsUrl = "http://sns" + mDevSuffix + ".gree.net";
      }
    }

    return mSnsUrl;
  }
}
