/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api.auth;


import android.app.Activity;

import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Url;

/**
 * <p>Authorizer deal with login, logout and other authorizing action </p>
 * <br>
 * The following shows an example of codes. For other examples, see the sample application.<br>
 * <br>
 * Example of login
 * <code><pre>
 * if (!Authorizer.isAuthorized()) {
 *   Authorizer.authorize(Activity.this, new AuthorizeListener() {
 *     public void onAuthorized() {
 *       // write process after login completion
 *     }
 *
 *     public void onCancel() {
 *       Log.d(&quot;Authorizer&quot;, &quot;authorization is cancelled&quot;);
 *     }
 *
 *     public void onError() {
 *       Log.d(&quot;Authorizer&quot;, &quot;authorization error&quot;);
 *     }
 *   });
 * }
 * </pre></code>
 *
 * <br>
 * Example of logout
 * <code><pre>
 * Authorizer.logout(Activity.this, new LogoutListener() {
 *   public void onLogout() {
 *     // write process after logout completion
 *   }
 *
 *   public void onCancel() {
 *     Log.d(&quot;Authorizer&quot;, &quot;logout is cancelled&quot;);
 *   }
 *
 *   public void onError() {
 *     Log.d(&quot;Authorizer&quot;, &quot;logout error&quot;);
 *   }
 * }, new AuthorizeListener() {
 *   public void onAuthorized() {
 *     // write process after login again
 *   }
 *
 *   public void onCancel() {
 *     Log.d(&quot;Authorizer&quot;, &quot;authorization is cancelled&quot;);
 *   }
 *
 *   public void onError() {
 *     Log.d(&quot;Authorizer&quot;, &quot;authorization error&quot;);
 *   }
 * });
 * </pre></code>
 * @author GREE, Inc.
 */
public final class Authorizer {

  private static IAuthorizer sAuthorizer;
  
  /** Represents a successful logout */
  public static final int LOGOUT_SUCCESS = 0;
  /** Represents an error or failure on logout */
  public static final int LOGOUT_ERROR = 1;
  /** Represents that the logout action was cancelled during the process */
  public static final int LOGOUT_CANCELLED = 2;
  /** Represents a scenario where the logout did not occur */
  public static final int NOT_LOGGED_OUT = 3;

  /** Represents "Play as Lite user", Target Grade 1 */
  public static final int LITE_USER = 1;
  /** Represents "Play with GREE", Target Grade 2 */
  public static final int NORMAL_USER = 2;

  private static IAuthorizer getAuthorizerCore() {
    if (null == sAuthorizer) {
      sAuthorizer = Injector.getInstance(IAuthorizer.class);
    }
    return sAuthorizer;
  }

  /**
   * If it returns true, the user already authorized in the application.
   * 
   * @return true or false
   */
  public static boolean isAuthorized() {
    return getAuthorizerCore().isAuthorized() && getAuthorizerCore().hasOAuthAccessToken();
  }

  /**
   * Results of authorization are delivered here.
   */
  public interface AuthorizeListener {
   
   
    /**
     * Results of authorization are delivered here.
     */
   
    public void onAuthorized();

   
   
    /**
     * Results of an error on authorization are delivered here.
     */
   
    public void onError();

   
   
    /**
     * Results of a cancel on authorization are delivered here. 
     */
   
    public void onCancel();
  }

  /**
   * Results of local user when updated are handled by this interface. Includes a callback method for when the local user is upgraded.
   */
  public interface UpdatedLocalUserListener {
   
   
    /**
     * Called on update of the Local User. 
     */
   
    public void onUpdateLocalUser();
  }

 
  /**
   * Logs in to GREE. Be sure to log in to GREE before making a request which requires
   * authentication.
   * @deprecated
   * @param activity Activity to be called
   * @param listener results of authorization
   */
  public static void authorize(Activity activity, AuthorizeListener listener) {
    getAuthorizerCore().authorize(activity, null, listener, null);
  }

  /**
   * Logs in to GREE. Be sure to log in to GREE before making a request which requires
   * authentication.
   * @deprecated
   * @param activity Activity to be called
   * @param listener results of authorization
   * @param localUserListener notification of local user us updated. 
   */
  public static void authorize(Activity activity, AuthorizeListener listener, UpdatedLocalUserListener localUserListener) {
    getAuthorizerCore().authorize(activity, null, listener, localUserListener);
  }
  
 /**
  *  This API is used to call the login screen directly from the developer's app
  * @param activity the calling activity, generally speaking this is the main activity, or first loaded activity
  * @param listener the AuthorizeListener that holds the results of the authorization process
  * @param localUserListener notification listener to update the developer in terms of an update and how to respond in the event of one
  * @param user_grade the grade the user is trying to login - correspond to TARGET_GRADE_# 1-3, where 1 represents "Lite" and 2-3 Represent regular user logins
  * @return a int representing the result of the login - correspond to this class' final ints
  */
  public static void authorize(Activity activity, AuthorizeListener listener, UpdatedLocalUserListener localUserListener, int user_grade) {
    getAuthorizerCore().authorize(activity, null, listener, localUserListener, user_grade);
  }

  /**
   * Results of logout will be called back by this listener.
   */
  public interface LogoutListener {
    
 
   
    /**
     * Results of logout will be called back by this listener when a logout successfully occurs.
     */
   
    public void onLogout();
 
   
    /**
     * Results of logout will be called back by this listener when a error occurs.
     */
   
    public void onError();
 
   
    /**
     * Results of logout will be called back by this listener when a canceled event occurs.
     */
   
    public void onCancel();
  }
  /**
   * Logout the GREE Account. Because the logout will also trigger login after successfully logout,
   * user should pass in both logoutListener and loginListener.
   * 
   * @param activity Activity to be called
   * @param logoutListener results of logout
   * @param loginListener results of login
   */
  public static void logout(Activity activity, LogoutListener logoutListener,
      AuthorizeListener loginListener) {
    getAuthorizerCore().logout(activity, logoutListener, loginListener, null);
  }

  /**
   * Logout the GREE Account. Because the logout will also trigger login after successfully logout,
   * user should pass in both logoutListener and loginListener.
   * 
   * @param activity Activity to be called
   * @param logoutListener results of logout
   * @param loginListener results of login
   * @param localUserListener notification of local user us updated. 
   */
  public static void logout(Activity activity, LogoutListener logoutListener,
      AuthorizeListener loginListener, UpdatedLocalUserListener localUserListener) {
    getAuthorizerCore().logout(activity, logoutListener, loginListener, localUserListener);
  }
  
 
 
  /**
   * Logout the GREE Account. This call will not popup an intermediate logout check window, as is the
   * case with the default behavior. 
   * <br>Because the logout will also trigger login after successfully logout,
   * the developer should pass in both logoutListener and loginListener.
   * @param activity Activity that we are calling from
   * @param logoutListener Interface representing the results of logout
   * @param autoRestart is by normally set to true, so that the login/main activity 
   * is automatically restarted after logging out. Depending on how you code your flow you may not want this. Typically
   * Unity applications want to set this to false.
   */
 
  public static void directLogout(Activity activity, LogoutListener logoutListener,
                                   boolean autoRestart){
    getAuthorizerCore().directLogout(activity, logoutListener, autoRestart);

  }
  
 
 
  /**
   * Logout the GREE Account. This call will not popup an intermediate logout check window, as is the
   * case with the default behavior. This login automatically registers a call setting the auto restart
   * value to true. This behavior is added in so that current code will not break with the new flag added.
   * 
   * <br>Because the logout will also trigger login after successfully logout,
   * the developer should pass in both logoutListener and loginListener.
   * @param activity Activity that we are calling from
   * @param logoutListener Interface representing the results of logout
   */
 
  public static void directLogout(Activity activity, LogoutListener logoutListener){
	  getAuthorizerCore().directLogout(activity, logoutListener, true);
  }
  
 
 
  /**
   * Checks the application and returns a boolean based on whether or not there currently 
   * exists an OAuth Access Token
   * @return true if an OAuth Access Token exists, false otherwise
   */
 
  public static boolean hasOAuthAccessToken(){
	  return getAuthorizerCore().hasOAuthAccessToken();
  }

  /**
   * This interface handles the actions of the local user when they are upgraded. Includes a callback methods for successful completion, a user cancellation, and when an error occurs.
   */
  public interface UpgradeListener {
 
   
    /**
     * Callback method called when the user is successfully upgraded.
     */
   
    public void onUpgrade();

   
   
    /**
     * Callback method called when an error occurs while attempting to upgrade the user.
     */
   
    public void onError();

   
   
    /**
     * Callback method called when an cancel action occurs while attempting to upgrade the user.
     */
   
    public void onCancel();
  }
  /**
   * Upgrade current login user.
   * 
   * @param activity Activity to be called
   * @param targetGrade target grade number to upgrade
   * @param listener results of upgrade
   */
  public static void upgrade(Activity activity, int targetGrade, UpgradeListener listener) {
    getAuthorizerCore().upgrade(activity, targetGrade, null, listener, null);
  }
  
  /**
   * Upgrade current login user.
   * 
   * @param activity Activity to be called
   * @param targetGrade target grade number to upgrade
   * @param listener results of upgrade
   * @param localUserListener  notification of local user us updated. 
   */
  public static void upgrade(Activity activity, int targetGrade, UpgradeListener listener, UpdatedLocalUserListener localUserListener) {
    getAuthorizerCore().upgrade(activity, targetGrade, null, listener, localUserListener);
  }

  /**
   * Returns access token for the current user.
   * 
   * @return access token string. If user has not been authorized yet, returns null.
   */
  public static String getOAuthAccessToken() {
    return getAuthorizerCore().getOAuthAccessToken();
  }

  /**
   * Returns access token secret for the current user.
   * 
   * @return access token secret string. If user has not been authorized yet, returns null.
   */
  public static String getOAuthAccessTokenSecret() {
    return getAuthorizerCore().getOAuthAccessTokenSecret();
  }
 
  /**
   * Returns the URL to the Terms of Service URL for the specific application.
   * 
   * @return The terms of service URL.
   */
  public static String getTosUrl(){
	return Url.getTosUrl(CoreData.get(InternalSettings.ApplicationId)).replace("https:", "http:");
  }
}
