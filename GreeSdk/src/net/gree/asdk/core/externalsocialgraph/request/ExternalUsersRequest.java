package net.gree.asdk.core.externalsocialgraph.request;

import java.util.TreeMap;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONObject;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.externalsocialgraph.ExternalFriend;
import net.gree.asdk.core.externalsocialgraph.Listeners.ExternalFriendsListener;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;
import net.gree.vendor.com.google.gson.Gson;

/**
 * This class is used to get list of users from the external social graph API. Those user represent
 * the data collected from Facebook, the address books of the user, and the GreeUser informations.
 */
public class ExternalUsersRequest {
  private static final String TAG = "ExternalUsersRequest";
  private static final int PROCEDURE_ID = 1; // number of posting request at once
  private static boolean isDebug = false;

  private static final String METHOD_EXTERNAL_FRIENDS =
      "ExternalSocialgraph.getExternalFriendsInGree";
  private static final String endpoint = "user/me/friends/extended";

  private static class FriendsArrays {
    public ExternalFriend[] facebook_friends;
    public ExternalFriend[] gree_friends;
    public ExternalFriend[] addressbook_friends;
  }

  /**
   * What the sns API potentially returns for each friend.
   */
  private static class Response {
    @SuppressWarnings("unused")
    public String jsonrpc;
    public FriendsArrays entry;
    public boolean has_more;
    public int offset;
    public int limit;

   
    /**
     * This set the type of the ExternalFriend from the type requested by the developer.
     * 
     * @param type one of FACEBOOK_USER, ADDRESS_BOOK, GREE_USER
     * @return
     */
    public ExternalFriend[] setType(int type) {
      ExternalFriend[] target = null;

      if (type == ExternalFriend.FACEBOOK_USER) {
        target = entry.facebook_friends;
      } else if (type == ExternalFriend.ADDRESS_BOOK) {
        target = entry.addressbook_friends;
      } else {
        target = entry.gree_friends;
      }
      if (target != null)
        for (int i = 0; i < target.length; i++) {
          target[i].setType(type);
        }
      return target;
    }
  }

  /**
   * Request the list of external friends (Facebook, address book and Gree users)
   * 
   * @param offset
   * @param count
   * @param listener
   * @return false if the type is invalid
   */
  public boolean getExternalFriends(final int type, int offset, int count,
      final ExternalFriendsListener listener) {
    if (!ExternalFriend.isValidType(type)) {
      return false;
    }
    TreeMap<String, Object> input = new TreeMap<String, Object>();
    input.put("jsonrpc", "2.0");
    input.put("method", METHOD_EXTERNAL_FRIENDS);

    TreeMap<String, Object> input2 = new TreeMap<String, Object>();
    input2.put("offset", offset);
    input2.put("limit", count);
    input2.put("service_type", typeValueOf(type));
    input.put("params", input2);

    input.put("id", PROCEDURE_ID);

    new JsonClient().oauth(Url.getGreeSecureApiEndpointWithAction(endpoint),
        BaseClient.methods[BaseClient.METHOD_POST], null, BaseClient.toEntityJson(input), false,
        new OnResponseCallback<String>() {
          @Override
          public void onSuccess(int responseCode, HeaderIterator headers, String json) {
            if (isDebug)
              GLog.d(TAG, "Http code:" + responseCode + " response:" + json);
            try {
              Response response = Injector.getInstance(Gson.class).fromJson(json, Response.class);
              if (isDebug)
                GLog.d(TAG, "result:" + response.offset + " " + response.limit);
              if (listener != null) {
                ExternalFriend[] result = response.setType(type);
                if (result != null && result.length > 0) {
                  listener.onSuccess(response.offset, response.has_more, response.limit, result);
                } else {
                  listener.onFailure(responseCode, headers, "Type " + type + " is empty.");
                }
              }
            } catch (Exception e) {
              GLog.printStackTrace(TAG, e);
            }
          }

          @Override
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            if (listener != null) {
              listener.onFailure(responseCode, headers, response);
            }
          }
        });
    return true;
  }

  private String typeValueOf(int type) {
    if (type == ExternalFriend.FACEBOOK_USER) {
      return "facebook";
    } else if (type == ExternalFriend.ADDRESS_BOOK) {
      return "addressbook";
    }
    return "greefriends";
  }
}
