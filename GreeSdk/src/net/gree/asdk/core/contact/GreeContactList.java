/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.contact;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import net.gree.asdk.core.GLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

/**
 * A Class that get the all the contact information with Json format
 */
public class GreeContactList implements IContactList {
  private static final String TAG = GreeContactList.class.getSimpleName();

  /**
   * Get the Contact Data in Json format
   * 
   * @return contact data in json format. If context is invalid, this function return null.
   */
  public String getContactList(Context context) {
    if (context == null) {
      GLog.e(TAG, "context is null");
      return null;
    }
    String result = query(context);
    return result;
  }

  /**
   * Get the Contact Data (email,tel) for AddressbookAPI
   * 
   * @return contact data in json format. If context is invalid, this function return null.
   */
  public String getAddressbookApiData(Context context) {
    if (context == null) {
      GLog.e(TAG, "context is null");
      return null;
    }
    String result = queryForAddressbook(context);
    return result;
  }

  private String query(Context context) {
    Map<Integer, JSONObject> data = new TreeMap<Integer, JSONObject>();
    ContentResolver cr = context.getContentResolver();
    String selection =
        ContactsContract.Contacts.IN_VISIBLE_GROUP + " = ? AND " + ContactsContract.Data.MIMETYPE
            + " = ?";
    String[] selectionArgs =
        new String[] {"1", ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE};
    Cursor nameCur = null;
    try {
      nameCur =
          cr.query(ContactsContract.Data.CONTENT_URI, null, selection, selectionArgs,
              ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID);
      if (nameCur.moveToFirst()) {
        do {
          String given =
              nameCur.getString(nameCur
                  .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
          if (TextUtils.isEmpty(given)) {
            given = "";
          }
          String family =
              nameCur.getString(nameCur
                  .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
          if (TextUtils.isEmpty(family)) {
            family = "";
          }
          String id =
              nameCur.getString(nameCur
                  .getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID));
          try {
            JSONObject one = new JSONObject();
            one.put("firstName", given);
            one.put("lastName", family);
            data.put(Integer.parseInt(id), one);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        } while (nameCur.moveToNext());
      }
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    } finally {
      if (nameCur != null && !nameCur.isClosed()) {
        nameCur.close();
      }
    }
    try {
      setPhoneNumber(data, cr);
      setEmail(data, cr);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }

    JSONArray array = new JSONArray();
    Set<Integer> keySet = data.keySet();

    for (Integer key : keySet) {
      JSONObject one = data.get(key);
      array.put(one);
    }
    GLog.d(TAG, "size:" + keySet.size());
    return array.toString();
  }

  private void setPhoneNumber(Map<Integer, JSONObject> data, ContentResolver cr)
      throws JSONException {
    Cursor phoneCur = null;
    try {
      phoneCur =
          cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
              ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
      if (phoneCur.moveToFirst()) {
        do {
          String id =
              phoneCur.getString(phoneCur
                  .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
          JSONObject object = data.get(Integer.parseInt(id));
          if (object != null) {
            switch (phoneCur.getInt(phoneCur
                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA2))) {
              case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                object.put("mobilePhoneNumber", phoneCur.getString(phoneCur
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1)));
                break;
              case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                object.put("homePhoneNumber", phoneCur.getString(phoneCur
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1)));
                break;
              case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
              default:
                object.put("phoneNumber", phoneCur.getString(phoneCur
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1)));
                break;
            }
            data.put(Integer.parseInt(id), object);
          }
        } while (phoneCur.moveToNext());
      }
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    } finally {
      if (phoneCur != null && !phoneCur.isClosed()) {
        phoneCur.close();
      }
    }
  }

  private void setEmail(Map<Integer, JSONObject> data, ContentResolver cr) throws JSONException {
    Cursor emailCur = null;
    try {
      emailCur =
          cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null,
              ContactsContract.CommonDataKinds.Email.CONTACT_ID);
      while (emailCur.moveToNext()) {
        String id =
            emailCur.getString(emailCur
                .getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
        JSONObject object = data.get(Integer.parseInt(id));
        if (object != null) {
          JSONArray array = object.optJSONArray("emails");
          if (array == null) {
            array = new JSONArray();
            object.put("emails", array);
            data.put(Integer.parseInt(id), object);
          }
          array.put(emailCur.getString(emailCur
              .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA1)));
        }
      }
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    } finally {
      if (emailCur != null && !emailCur.isClosed()) {
        emailCur.close();
      }
    }
  }

  private String queryForAddressbook(Context context) {
    Map<Integer, JSONObject> buf = new TreeMap<Integer, JSONObject>();
    ContentResolver cr = context.getContentResolver();
    String id = null;
    Cursor c = null;

    try {
      c =
          cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null,
              ContactsContract.CommonDataKinds.Email.CONTACT_ID);
      c.moveToFirst();
      if (c.getCount() > 0)
        do {
          JSONObject data = new JSONObject();
          id = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
          data.put("email",
              c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA1)));
          data.put("tel", "");
          buf.put(Integer.parseInt(id), data);
        } while (c.moveToNext());
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    } finally {
      if (c != null && !c.isClosed()) {
        c.close();
      }
    }

    try {
      c =
          cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
              ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
      c.moveToFirst();
      if (c.getCount() > 0)
        do {
          id = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
          JSONObject data = buf.get(Integer.parseInt(id));
          if (data != null) {
            data.put("tel",
                c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1)));
            buf.put(Integer.parseInt(id), data);
          } else {
            data = new JSONObject();
            data.put("tel",
                c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1)));
            data.put("email", "");
            buf.put(Integer.parseInt(id), data);
          }
        } while (c.moveToNext());
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    } finally {
      if (c != null && !c.isClosed()) {
        c.close();
      }
    }

    TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    String cc = tm.getSimCountryIso().toUpperCase();
    if (cc == null)
      cc = "";

    JSONArray jarry = new JSONArray();
    Set<Integer> keySet = buf.keySet();

    for (Integer key : keySet) {
      JSONObject one = buf.get(key);
      jarry.put(one);
    }
    JSONObject data = new JSONObject();
    try {
      data.put("country_code", cc);
      data.put("addressbook", jarry);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }
    return data.toString();
  }
}
