package net.gree.asdk.core.socialgraph.request;

import java.util.TreeMap;

import org.apache.http.HeaderIterator;

import net.gree.asdk.api.Request;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.socialgraph.InGameFriend;
import net.gree.asdk.core.socialgraph.Listeners.CountListener;
import net.gree.asdk.core.socialgraph.Listeners.InGameFriendsListener;
import net.gree.asdk.core.socialgraph.Listeners.RelationshipListener;
import net.gree.vendor.com.google.gson.Gson;

/**
 * This class is used to get information about users playing a given application.
 *
 */
public class InGameUsersRequest {
  private static final String TAG = "PeopleRequest";
  private static boolean isDebug = false;
  private static boolean isVerbose = false;

  /**
   * What the GreeUser API potentially returns for each user.
   */
  private static class Response {
    public InGameFriend[] entry;
    public String startIndex;
    public String itemsPerPage;
  }

  /**
   * When getting the friend count
   */
  private static class CountResponse {
    public int entry;
  }

  /**
   * When getting the relationship between 2 users
   */
  private static class RelationshipResponse {
    public static final String[] relations = {"none", "requesting", "reserving", "requested", "friend" };
    public String entry;

    /**
     * Parse the entry for "none" or "friends" strings
     * @return NONE or FRIENDS as defined in the RelationshipListener
     */
    public int getRelationship() {
      int res = RelationshipListener.NONE;
      if ((entry != null) && (entry.length() > 0)) {
        for (int i = 0; i < relations.length; i++) {
          if (entry.equalsIgnoreCase(relations[i])) {
            res = i;
            break;
          }
        }
      }
      return res;
    }
  }

  /**
   * Retrieve a list of game friends for current application
   * request is GET /api/rest/friend/@me/@app/@all
   * 
   * @param offset the index in the list of friends (the first index is 1 to follow open social)
   * @param count the number of friends to get
   * @param listener the callback listener
   */
  public void getFriendsForGame(int offset, int count, InGameFriendsListener listener) {
    getPeople("/friend/@me/@app/@all", offset, count, listener);
  }

  /**
   * Retrieve a list of game friends for given users and application
   * request is GET /api/rest/friend/{user_id}/{app_id}/@all
   * 
   * @param userId the GreeUser identifier
   * @param appId the application identifier
   * @param offset the index in the list of friends (the first index is 1 to follow open social)
   * @param count the number of friends to get
   * @param listener the callback listener
   */
  public void getFriendsForGame(String userId, String appId, int offset, int count, InGameFriendsListener listener) {
    getPeople("/friend/"+userId+"/"+appId+"/@all", offset, count, listener);
  }

  /**
   * Get a relationship between specified users for current application
   * GET /api/rest/friend/{user_id}/@app/200 (specifying multiple users is currently not supported)
   */
  public void getRelationship(String userId1, String userId2, final RelationshipListener listener) {
    String url = "/friend/"+userId1+"/@app/"+userId2;
    Request request = new Request();
    TreeMap<String, Object> args = new TreeMap<String, Object>();
    if (isDebug) {
      GLog.d(TAG, url);
    }

    request.oauthGree(url, "GET", args, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (isDebug) {
          GLog.d(TAG, "Http response:" + json);
        }
        try {
          RelationshipResponse response = Injector.getInstance(Gson.class).fromJson(json, RelationshipResponse.class);
          if (isVerbose) {
            GLog.d(TAG, "result:" + response);
          }
          if (listener != null) {
            listener.onSuccess(response.getRelationship());
          }
        } catch (Exception ex) {
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  /**
   * Get the number of game friends of current application
   * GET /api/rest/friend_count/@me/@app/@all
   */
  public void getFriendCount(CountListener listener) {
    getFriendCount("@me", "@app", listener);
  }

  /**
   * Get the number of game friends for a specified user and a specified application.
   * GET /api/rest/friend_count/{user_id}/{app_id}/@all
   */
  public void getFriendCount(String userId, String appId, final CountListener listener) {
    String url = "/friend_count/"+userId+"/"+appId+"/@all";
    Request request = new Request();
    TreeMap<String, Object> args = new TreeMap<String, Object>();
    if (isDebug) {
      GLog.d(TAG, url);
    }

    request.oauthGree(url, "GET", args, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (isDebug) {
          GLog.d(TAG, "Http response:" + json);
        }
        try {
          CountResponse response = Injector.getInstance(Gson.class).fromJson(json, CountResponse.class);
          if (isVerbose) {
            GLog.d(TAG, "result:" + response.entry);
          }
          if (listener != null) {
            listener.onSuccess(response.entry);
          }
        } catch (Exception ex) {
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  /**
   * Core communication operation for calls that return users results.
   * 
   * @param url
   * @param offset
   * @param count
   * @param listener
   */
  private void getPeople(final String url, int offset, int count,
                         final InGameFriendsListener listener) {
    Request request = new Request();
    TreeMap<String, Object> args = new TreeMap<String, Object>();
    args.put("startIndex", Integer.toString(offset));
    args.put("count", Integer.toString(count));
    if (isDebug) {
      GLog.d(TAG, url);
    }

    request.oauthGree(url, "GET", args, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (isDebug) {
          GLog.d(TAG, "Http response:" + json);
        }
        try {
          Response response = Injector.getInstance(Gson.class).fromJson(json, Response.class);
          if (isVerbose) {
            GLog.d(TAG, "result:" + response.startIndex + " " + response.itemsPerPage);
          }
          if (listener != null) {
            listener.onSuccess(Integer.parseInt(response.startIndex), response.entry);
          }
        } catch (Exception ex) {
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
  
}
