/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.ui;

import org.apache.http.HeaderIterator;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.IconDownloadListener;

/**
 * Class to create an image with rounded corners 
 * the image can be created from an existing bitmap, 
 */
public class RoundCornerImage {

  private Bitmap mBitmap;
  private static final int ROUND_RATIO = 8;

  /**
   * constructor
   */
  public RoundCornerImage(Bitmap bitmap) {
    mBitmap = bitmap;
  }

  /**
   * Constructor by user
   * @param user the user's thumbnail to be used
   * @param size the size of the thumbnail as defined by GreeUser: THUMBNAIL_SIZE_SMALL THUMBNAIL_SIZE_NORMAl etc...
   * @param listener the IconDownloadListener to be notified when the image is ready
   */
  public RoundCornerImage(GreeUser user, int size, final IconDownloadListener listener) {
    if (user != null) {
      user.loadThumbnail(size, new IconDownloadListener() {
        @Override
        public void onSuccess(Bitmap image) {
          mBitmap = image;
          listener.onSuccess(createRoundedCornerBitmap(image));
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          listener.onFailure(responseCode, headers, response);
        }
      });
    }
  }

  /**
   * Create a rounded version of the image
   * @return a bitmap or null if the image could not be created
   */
  public Bitmap getRoundedBitmap() {
    return createRoundedCornerBitmap(mBitmap);
  }

  /**
   * Create the rounded bitmap
   * @param bitmap
   * @return
   */
  private Bitmap createRoundedCornerBitmap(Bitmap bitmap) {
    if ((bitmap == null) || (bitmap.isRecycled())) {
      return null;
    }
    Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    final RectF rectF = new RectF(rect);
    final float roundPx = bitmap.getWidth() / ROUND_RATIO;

    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(Color.BLACK);
    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);
    return output;
  }
}
