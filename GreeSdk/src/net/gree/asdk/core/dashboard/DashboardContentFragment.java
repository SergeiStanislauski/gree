/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.util.List;
import java.util.Stack;
import java.util.TreeMap;

import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.GreeUser.GreeUserListener;
import net.gree.asdk.api.auth.Authorizer.AuthorizeListener;
import net.gree.asdk.api.auth.Authorizer.LogoutListener;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.api.ui.CloseMessage;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.TaskEventDispatcher;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.dashboard.SubNaviView.SubNaviOnItemChangeListener;
import net.gree.asdk.core.notifications.NotificationCallback;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.storage.JSONStorage;
import net.gree.asdk.core.ui.CommandInterface;
import net.gree.asdk.core.ui.CommandInterface.OnCommandListenerAdapter;
import net.gree.asdk.core.ui.CommandInterfaceWebView;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.wallet.Deposit;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DashboardContentFragment extends Fragment implements NotificationCallback {
  private static final String TAG = "DashboardContentFragment";
  private static final int UPLOADABLE_PHOTO_SIZE = 5;
  public static final String EXTRA_DASHBOARD_URL = "dashboard_url";

  private FragmentActivity mActivity;
  private IAuthorizer mAuthorizer;
  private DashboardContentView mContentView = null;
  private OnCommandListenerAdapter mCommandListener = new DashboardContentViewCommandListener();
  private SubNaviOnItemChangeListener mSubNaviOnItemChangeListener = new DashboardSubNaviOnItemChangeListener();

  private Handler mUiThreadHandler = new Handler();

  private boolean mIsMultiplePosting = false;
  private JSONObject mTextInputParams = null;
  private JSONObject mTextInputResult = null;
  private boolean mIsOpenFromMenu = false;
  private ImageUploader mImageUploader = null;

  private String mCurrentContentViewUrl = null;

  private Stack<ViewData> mContentViewDataHistory = new Stack<ViewData>();
  private ViewData mCurrentViewData = null;

  private class ViewData {
    Stack<Integer> mPositionHistory = new Stack<Integer>();
    String mLastUrl;
  }

  //Parameters used by TaskEventListener
  private TaskEventDispatcher mTaskEventDispatcher;
  private TreeMap<String, Object> mTaskEventParams;

  private DashboardContentFragmentCallback mFragmentCallback;

  public enum EVENT_TYPE {
    NORMAL,
    NORMAL_PENDING_TRANSITION
  };

  public interface DashboardContentFragmentCallback {
    public void notifyDashboardContentFragmentFinishRequest(EVENT_TYPE type, Object arg);
  }

  public void setDashboardContentFragmentCallback(DashboardContentFragmentCallback callback) {
    mFragmentCallback = callback;
  }

  /**
   * When memories run short, Fragment is discarded automatically.
   * Since Callback cannot be called unless Callback exists, when Fragment is re-generated, Callback is set up in onAttach.
   */
  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    GLog.d(TAG, "onAttach");
    mFragmentCallback = null;
    if (!(activity instanceof DashboardContentFragmentCallback)) {
      GLog.e(TAG, "FragmentCallback is not implemented.");
      return;
    }
    mFragmentCallback = (DashboardContentFragmentCallback)activity;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GLog.d(TAG, "onCreate");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    GLog.d(TAG, "onCreateView");
    mActivity = getActivity();
    mAuthorizer = Injector.getInstance(IAuthorizer.class);
    //Notify that the notification board as started
    mTaskEventDispatcher = Injector.getInstance(TaskEventDispatcher.class);
    mTaskEventParams = new TreeMap<String, Object>();
    mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_DASHBOARD_ACTIVITY, GreePlatformListener.EVENT_OPEN, mActivity, mTaskEventParams);

    if (mContentView == null) {
      mContentView = new DashboardContentView(mActivity);

      CommandInterface commandInterface = mContentView.getCommandInterface();
      commandInterface.addOnCommandListener(mCommandListener);

      mContentView.initialize(Url.getSnsUrl());
      mContentView.getWebView().setBackgroundColor(getResources().getColor(RR.color("gree_webview_background")));
      mContentView.addSubNaviOnItemChangeListener(mSubNaviOnItemChangeListener);
      mImageUploader = new ImageUploader(mActivity, this);
    }
    mContentView.getPerformManager().recordData(mContentView.getPerformData(), PerformanceIndexMap.INDEX_KEY_DASHBOARD_START);

    loadUrlByIntent(getArguments(), true);
    recordLogForDashboardLaunch(getArguments());

    return mContentView;
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    GLog.d(TAG, "onViewCreated");
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    GLog.d(TAG, "onActivityCreated");
  }

  /**
   * Fragment don't have a callback of the same meaning as onNewIntent of Activity.
   * Therefore, this function should be called in onNewIntent of Activity.
   * @param args input argument
   */
  public void onNewIntent(Bundle args) {
    loadUrlByIntent(args, false);
    GLog.d(TAG, "onNewIntent");
  }

  @Override
  public void onStart () {
    super.onStart();
    GLog.d(TAG, "onStart");
  }

  @Override
  public void onResume() {
    super.onResume();
    GLog.d(TAG, "onResume");
    if (null != mContentView) {

      mContentView.onResume();

      if (!mContentView.refreshIfLocaleChanged()) {
        mContentView.refreshIfUserChanged();
      }
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    GLog.d(TAG, "onPause");
    if (null != mContentView) {
      mContentView.onPause();
    }
  }

  @Override
  public void onStop() {
    mContentView.getPerformManager().recordData(mContentView.getPerformData(),
      PerformanceIndexMap.INDEX_KEY_DISMISS);
    mContentView.getPerformManager().flushData(mContentView.getPerformData());
    // Notify that the Dashboard as stopped
    mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_DASHBOARD_ACTIVITY,
      GreePlatformListener.EVENT_CLOSE, mActivity, mTaskEventParams);
    super.onStop();
    GLog.d(TAG, "onStop");
  }

  @Override
  public void onDestroyView() {
    GLog.d(TAG, "onDestroyView");

    // Notify that the dashboard is being destroyed.
    mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_DASHBOARD_ACTIVITY,
      GreePlatformListener.EVENT_DESTROYED, mActivity, mTaskEventParams);
    mFragmentCallback = null;
    mActivity = null;

    super.onDestroyView();

    if (null != mContentView) {
      mContentView.destroy();
      mContentView = null;
    }
  }

  @Override
  public void onDestroy () {
    super.onDestroy();
    GLog.d(TAG, "onDestroy");
  }

  @Override
  public void onDetach () {
    super.onDetach();
    GLog.d(TAG, "onDetach");
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (null == mContentView) {
      return;
    }
    CommandInterface commandInterface = mContentView.getCommandInterface();

    if (requestCode == Deposit.BILLING_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
      mContentView.reload();
      return;
    }

    Resources res = mActivity.getResources();
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == res.getInteger(RR.integer("gree_request_code_get_image"))) {
        mImageUploader.uploadUri(commandInterface, data);
      } else if (requestCode == res.getInteger(RR.integer("gree_request_code_capture_image"))) {
        mImageUploader.uploadImage(commandInterface, data);
      } else if (requestCode == res.getInteger(RR.integer("gree_request_code_get_image_with_preview"))) {
        Intent intent = new Intent(mActivity, ImagePreviewActivity.class);
        intent.putExtra("uri", data.getData());
        startActivityForResult(intent, getResources().getInteger(RR.integer("gree_request_code_get_image")));
      } else if (requestCode == res.getInteger(RR.integer("gree_request_code_capture_image_with_preview"))) {
        Intent intent = new Intent(mActivity, ImagePreviewActivity.class);
        intent.putExtra("uri", mImageUploader.getTakenPhotoUri());
        startActivityForResult(intent, getResources().getInteger(RR.integer("gree_request_code_capture_image")));
      } else if (requestCode == res.getInteger(RR.integer("gree_request_show_modal_dialog"))) {
        loadViewByIntent(data);
      } else if (requestCode == res.getInteger(RR.integer("gree_request_show_text_input_view"))) {
        if (data != null && data.getStringExtra("callbackId") != null) {
          try {
            String text = data.getStringExtra("text");
            if (text != null) {
              JSONObject obj;
              if (data.getStringExtra("params") != null) {
                try {
                  obj = new JSONObject(data.getStringExtra("params"));
                } catch (JSONException e) {
                  obj = new JSONObject();
                }
              } else {
                obj = new JSONObject();
              }

              obj.put("text", text);
              obj.put("title", data.getStringExtra("title"));
              String callbackId = data.getStringExtra("callbackId");

              for (int i = 0; i < UPLOADABLE_PHOTO_SIZE; i++) {
                String base64ImageKey = "image" + i;
                String base64Image = DashboardStorage.getString(getActivity(), base64ImageKey);
                if (base64Image != null) {
                  obj.put(base64ImageKey, base64Image);
                  DashboardStorage.remove(getActivity(), base64ImageKey);
                }
              }

              String title = data.getStringExtra("title");
              if (title != null) {
                mIsMultiplePosting = true;
              } else {
                mIsMultiplePosting = false;
              }
              if (mTextInputParams != null) {
                commandInterface.executeCallback(callbackId, obj, mTextInputParams);
                mTextInputResult = obj;
              }
              mContentView.showLoadingIndicator();
            }
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
      } else if (requestCode == res.getInteger(RR.integer("gree_request_show_photo_view"))) {
        addViewHistory();
        loadViewByIntent(data);
      }
    }
  }

  public void reload() {
    if (mContentView == null) {
      return;
    }
    mContentView.reload();
  }

  public void recordPerformData(String key) {
    if (mContentView == null) {
      return;
    }
    mContentView.getPerformManager().recordData(mContentView.getPerformData(), key);
  }

  public void hideLoadingIndicator() {
    if (mContentView == null) {
      return;
    }
    mContentView.hideLoadingIndicator();
  }

  public boolean onKeyUp(int keyCode, KeyEvent event) {
    if (null == mContentView) {
      return false;
    }

    if (canGoBackContentViewHistory()) {
      goBackContentViewHistory();
      return true;
    } else {
      if (mFragmentCallback instanceof DashboardContentFragmentCallback) {
        mFragmentCallback.notifyDashboardContentFragmentFinishRequest(EVENT_TYPE.NORMAL_PENDING_TRANSITION, null);
      }
      return true;
    }
  }

  /**
   * Set the activity result before closing this activity,
   * The results are passed through the use of a CloseMessage.
   * The results will also be notified through the EventListener.
   * 
   * @param commandInterface the commandInterface to be used for the callback
   * @param params the parameters to be added to the CloseMessage
   */
  public void setActivityResultAndClose(final CommandInterface commandInterface, final JSONObject params) {
    mUiThreadHandler.post(new Runnable() {
      @Override
      public void run() {
        try {
          // return result for startActivityForResult.
          CloseMessage closemessage = new CloseMessage();
          closemessage.setData(params.toString());
          Intent i = new Intent();
          i.putExtra(CloseMessage.DATA, closemessage);
          mActivity.setResult(Activity.RESULT_OK, i);
          recordLogForDashboardClose();

          //Save the result to be reported by mTaskEventDispatcher in Activity.onStop()
          if (params != null) {
            mTaskEventParams.put(GreePlatformListener.KEY_RESULTS, params);
          }

          // close dashboard activity.
          if (mFragmentCallback instanceof DashboardContentFragmentCallback) {
            mFragmentCallback.notifyDashboardContentFragmentFinishRequest(EVENT_TYPE.NORMAL, null);
          }

          // return result for JS callback.
          String callbackId = params.getString("callback");
          JSONObject result = new JSONObject();
          commandInterface.executeCallback(callbackId, result);
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        }
      }
    });
  }

  private void loadViewByIntent(Intent data) {
    if (null == mContentView) {
      return;
    }

    CommandInterface commandInterface = mContentView.getCommandInterface();

    if (null != data && null != data.getStringExtra("view")) {
      JSONObject obj;
      if (null != data.getStringExtra("params")) {
        try {
          obj = new JSONObject(data.getStringExtra("params"));
        } catch (JSONException e) {
          obj = new JSONObject();
        }
      } else {
        obj = new JSONObject();
      }
      if (null != mContentView) {
        mContentView.loadView(data.getStringExtra("view"), obj);
      } else {
        commandInterface.loadView(data.getStringExtra("view"), obj);
      }
    }
  }

  private boolean isValidUrl(String url) {
    Uri parsedUrl = Uri.parse(url);
    return parsedUrl != null 
        && (parsedUrl.getScheme().startsWith("http") || parsedUrl.getScheme().startsWith("https"))
        && parsedUrl.getHost().endsWith(Url.getRootFqdn());
  }

  private boolean isFirstLoad = true;

  public void loadContentView(String url, boolean isOpenFromMenu) {
    if (null == mContentView) {
      return;
    }

    if (!isValidUrl(url)) {
      return;
    }

    mIsOpenFromMenu = isOpenFromMenu;
    addViewHistory();

    mContentView.loadUrl(url);
    mContentView.clearSubNavi();

    mCurrentContentViewUrl = url;
  }

  /**
   * getter
   * @return URL shown in dashbaord
   */
  public String getCurrentContentViewUrl() {
    return mCurrentContentViewUrl;
  }

  private void addViewHistory() {
    if (isFirstLoad) {
      isFirstLoad = false;
      return;
    }

    if (null == mCurrentViewData) {
      mCurrentViewData = new ViewData();
    }

    String currentUrl = mContentView.getCommandInterface().getWebView().getUrl();
    if (Url.isSnsUrl(currentUrl)) {
      int paramStartIndex = currentUrl.indexOf("#");
      if (paramStartIndex != -1) {
        String tmp = currentUrl.substring(paramStartIndex + 1);
        currentUrl = mContentView.getCommandInterface().getBaseUrl() + "?" + tmp;
      } else {
        currentUrl = mCurrentContentViewUrl;
      }
    }

    mCurrentViewData.mLastUrl = currentUrl;
    mContentViewDataHistory.push(mCurrentViewData);
    mCurrentViewData = null;
  }

  private boolean canGoBackContentViewHistory() {
    if (mCurrentViewData != null && (mCurrentViewData.mLastUrl != null || !mCurrentViewData.mPositionHistory.empty())) {
      return true;
    } else if (!mContentViewDataHistory.empty()){
      return true;
    }

    return false;
  }

  private void goBackContentViewHistory() {

    if (null == mContentView) {
      return;
    }

    if (null == mCurrentViewData) {
      mCurrentViewData = mContentViewDataHistory.pop();
    }

    if (null != mCurrentViewData.mLastUrl) {
      mContentView.loadUrl(mCurrentViewData.mLastUrl);
      mContentView.clearSubNavi();
      mCurrentContentViewUrl = mCurrentViewData.mLastUrl;
      mCurrentViewData.mLastUrl = null;

      if (mCurrentViewData.mPositionHistory.empty()) {
        mCurrentViewData = null;
      }
    } else {
      int subnaviCount = mContentView.getSubNaviItemCount();
      if (subnaviCount <= 0) {
        return;
      }
      int position = mCurrentViewData.mPositionHistory.pop();
      if (mCurrentViewData.mPositionHistory.empty()) {
        mCurrentViewData = null;
      }
      mContentView.selectSubNaviItem(position);
    }
  }

  private void loadUrlByIntent(Bundle args, boolean isDefaultUrlUsed) {
    String dashboardUrl = null;
    if (null != args) {
      dashboardUrl = args.getString(EXTRA_DASHBOARD_URL);
    }

    if (null == dashboardUrl) {
      if (isDefaultUrlUsed) {
        dashboardUrl =
            Core.isGreeApp() ? Url.getGamesUrl() : Url.getDashboardContentUrl();
      } else {
        return;
      }
    }
    loadContentView(dashboardUrl, false);
  }

  private void recordLogForDashboardLaunch(Bundle args) {
    String dashboardUrl = null;
    if (null != args) {
      dashboardUrl = args.getString(EXTRA_DASHBOARD_URL);
    }

    if (null == dashboardUrl) {
      dashboardUrl = Core.isGreeApp() ? Url.getGamesUrl() : Url.getDashboardContentUrl();
    }
    Logger.recordLog("pg", dashboardUrl, "game", null);
  }

  private void recordLogForDashboardClose() {
    CommandInterfaceWebView commandInterfaceWebview = getWebView();
    String url = commandInterfaceWebview != null ? commandInterfaceWebview.getUrl() : null;
    if (!TextUtils.isEmpty(url)) {
      String evtFrom = url;
      if (url.contains("?")) {
        evtFrom = url.substring(0, url.indexOf("?"));
      }
      Logger.recordLog("pg", "game", evtFrom, null);
    }
  }

  protected CommandInterfaceWebView getWebView() {
    return null != mContentView ? mContentView.getWebView() : null;
  }

  private class DashboardContentViewCommandListener extends CommandInterface.OnCommandListenerAdapter {

    @Override
    public void onContentsReady(final CommandInterface commandInterface, final JSONObject params) {
      mUiThreadHandler.post(new Runnable() {
        public void run() {
          if (null != mContentView) {

            mContentView.onRefreshComplete();
            mContentView.subNaviDataSetChange();
            mContentView.updateLastUpdateTextView();

            mContentView.getWebView().requestFocus(View.FOCUS_DOWN);
          }
        }
      });
    }

    @Override
    public void onPushView(final CommandInterface commandInterface, final JSONObject params) {
      mUiThreadHandler.post(new Runnable() {
        @Override
        public void run() {
          if (AsyncErrorDialog.shouldShowErrorDialog(mContentView.getContext())) {
            AsyncErrorDialog dialog = new AsyncErrorDialog(mContentView.getContext());
            dialog.show();
            return;
          }
          mContentView.getPerformManager().recordData(mContentView.getPerformData(), PerformanceIndexMap.INDEX_KEY_START_PUSH_VIEW);
          loadContentView(CommandInterface.makeViewUrl(commandInterface.getBaseUrl(), params), false);
        }
      });
    }

    @Override
    public void onPushViewWithUrl(final CommandInterface commandInterface, final JSONObject params) {
      mUiThreadHandler.post(new Runnable() {
        @Override
        public void run() {
          if (AsyncErrorDialog.shouldShowErrorDialog(mContentView.getContext())) {
            AsyncErrorDialog dialog = new AsyncErrorDialog(mContentView.getContext());
            dialog.show();
            return;
          }
          mContentView.getPerformManager().recordData(mContentView.getPerformData(), PerformanceIndexMap.INDEX_KEY_START_PUSH_VIEW);
          String url = params.optString("url");
          if (0 == url.length()) {
            return;
          }
          loadContentView(url, false);
        }
      });
    }

    @Override
    public void onPopView(final CommandInterface commandInterface, final JSONObject params) {
      mUiThreadHandler.post(new Runnable() {
        @Override
        public void run() {
          if (AsyncErrorDialog.shouldShowErrorDialog(mContentView.getContext())) {
            AsyncErrorDialog dialog = new AsyncErrorDialog(mContentView.getContext());
            dialog.show();
            return;
          }
          mContentView.getPerformManager().recordData(mContentView.getPerformData(), PerformanceIndexMap.INDEX_KEY_START_POP_VIEW);
          if (canGoBackContentViewHistory()) {
            goBackContentViewHistory();
          } else {
            recordLogForDashboardClose();
            if (mFragmentCallback instanceof DashboardContentFragmentCallback) {
              mFragmentCallback.notifyDashboardContentFragmentFinishRequest(EVENT_TYPE.NORMAL, null);
            }
          }
        }
      });
    }

    @Override
    public void onShowModalView(final CommandInterface commandInterface, final JSONObject params) {

      Intent intent = new Intent(mActivity, ModalActivity.class);
      intent.putExtra("params", params.toString());
      intent.putExtra(ModalActivity.EXTRA_BASE_URL, Url.getSnsUrl());
      startActivityForResult(intent, getResources().getInteger(RR.integer("gree_request_show_modal_dialog")));
    }

    @Override
    public void onOpenExternalView(final CommandInterface commandInterface, final JSONObject params) {

      try {
        Intent intent = new Intent(mActivity, SubBrowserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("url", params.getString("url"));
        startActivity(intent);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    @Override
    public void onShowInputView(final CommandInterface commandInterface, final JSONObject params) {

      GLog.d("params", params.toString());
      Intent intent;
      try {
        if (params.getString("type").equals("form")) {
          intent = new Intent(mActivity, PostingMultipleActivity.class);
        } else {
          intent = new Intent(mActivity, PostingActivity.class);
        }
      } catch (JSONException e) {
        intent = new Intent(mActivity, PostingActivity.class);
        GLog.printStackTrace(TAG, e);
      }

      try { intent.putExtra("callback", params.getString("callback")); } catch (JSONException e) {}
      try { intent.putExtra("title", params.getString("title")); } catch (JSONException e) {}
      try { intent.putExtra("placeholder", params.getString("placeholder")); } catch (JSONException e) {}
      try { intent.putExtra("button", params.getString("button")); } catch (JSONException e) {}
      try { intent.putExtra("titlelabel", params.getString("titlelabel")); } catch (JSONException e) {}
      try { intent.putExtra("titlevalue", params.getString("titlevalue")); } catch (JSONException e) {}
      try { intent.putExtra("titleplaceholder", params.getString("titleplaceholder")); } catch (JSONException e) {}
      try { intent.putExtra("singleline", params.getBoolean("singleline")); } catch (JSONException e) {}
      try { intent.putExtra("useEmoji", params.getBoolean("useEmoji")); } catch (JSONException e) {}
      try { intent.putExtra("usePhoto", params.getBoolean("usePhoto")); } catch (JSONException e) {}
      try { intent.putExtra("photoCount", params.getInt("photoCount")); } catch (JSONException e) {}
      try { intent.putExtra("limit", params.getInt("limit")); } catch (JSONException e) {}
      try { intent.putExtra("value", params.getString("value")); } catch (JSONException e) {}
      try { intent.putExtra("image", params.getString("image")); } catch (JSONException e) {}
      try { intent.putExtra("titleRequired", params.getBoolean("titleRequired")); } catch (JSONException e) {}
      try { intent.putExtra("textRequired", params.getBoolean("textRequired")); } catch (JSONException e) {}
      try { intent.putExtra("photoRequired", params.getBoolean("photoRequired")); } catch (JSONException e) {}
      for (int i = 0; i <= UPLOADABLE_PHOTO_SIZE; i++) {
        String imageKey = "image" + i;
        try { intent.putExtra(imageKey, params.getString(imageKey)); } catch (JSONException e) {}
      }
      try { intent.putExtra("useSpot", params.getBoolean("useSpot")); } catch (JSONException e) {}
      try { intent.putExtra("spotView", params.getString("spotView")); } catch (JSONException e) {}

      mTextInputParams = params;

      startActivityForResult(intent, getResources().getInteger(RR.integer("gree_request_show_text_input_view")));
    };

    @Override
    public void onInputSuccess(final CommandInterface commandInterface, JSONObject params) {

      mUiThreadHandler.post(new Runnable() {
        public void run() {
          if (null != mContentView) {
            mContentView.hideLoadingIndicator();
          }
        }
      });

      try {
        if (params.getString("view") != null) {
          if (null != mContentView) {
            mContentView.loadView(params.getString("view"), params);
          } else {
            commandInterface.loadView(params.getString("view"), params);
          }
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
      mTextInputParams = null;
      mTextInputResult = null;

      try {
        String callback = params.getString("post_cache_key");
        SharedPreferences pref = DashboardContentFragment.this.getActivity().getSharedPreferences(callback, Activity.MODE_PRIVATE);
        Editor edit = pref.edit();
        edit.clear();

        edit.commit();
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }

    }

    @Override
    public void onInputFailure(final CommandInterface commandInterface, JSONObject params) {
      mUiThreadHandler.post(new Runnable() {
        public void run() {
          if (null != mContentView) {
            mContentView.hideLoadingIndicator();
          }
        }
      });

      try {
        new AlertDialog.Builder(DashboardContentFragment.this.getActivity())
        .setMessage(params.getString("error"))
        .setPositiveButton(android.R.string.ok, null)
        .show();
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }

      try {
        JSONObject common_params = params.getJSONObject("params");
        String pref_key = common_params.getString("post_cache_key");
        SharedPreferences pref = DashboardContentFragment.this.getActivity().getSharedPreferences(pref_key, Activity.MODE_PRIVATE);
        Editor edit = pref.edit();
        edit.putBoolean("isFailed", true);

        edit.commit();
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    @Override
    public void onTakePhoto(final CommandInterface commandInterface, final JSONObject params) {

      mUiThreadHandler.post(new Runnable() {
        public void run() {
          mImageUploader.showSelectionDialog(commandInterface, params);
        }
      });
    }

    @Override
    public void onSetSubNavi(final CommandInterface commandInterface, final JSONObject params) {

      mUiThreadHandler.post(new Runnable() {
        public void run() {
          if (null != mContentView) {
            mContentView.updateSubNavi(params, mIsOpenFromMenu);
            mIsOpenFromMenu = false;
          }
        }
      });
    }

    @Override
    public void onSetValue(final CommandInterface commandInterface, final JSONObject params) {
      getWebView().getLocalStorage().putString(params.optString("key"), params.optString("value"));
    }

    @Override
    public void onGetValue(final CommandInterface commandInterface, final JSONObject params) {
      String value = getWebView().getLocalStorage().getString(params.optString("key"));
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("value", value);
        commandInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    @Override
    public void onPageLoaded(final CommandInterface commandInterface, final JSONObject params) {

      mUiThreadHandler.post(new Runnable() {
        public void run() {
          if (null != mContentView) {

            mContentView.onRefreshComplete();
            mContentView.subNaviDataSetChange();
            mContentView.updateLastUpdateTextView();

            mContentView.getWebView().requestFocus(View.FOCUS_DOWN);
          }
        }
      });
    }

    @Override
    public void onLogout(final CommandInterface commandInterface, JSONObject params) {

      // this one is trigger to do the logout , but not care about the callback
      mAuthorizer.logout(mActivity, new LogoutListener() {
        @Override
        public void onError() {
        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onLogout() {
          View dummyView = mActivity.findViewById(RR.id("gree_u_dummy_view"));
          if (dummyView instanceof View) {
            dummyView.setVisibility(View.VISIBLE);
          }

          JSONStorage storage = Injector.getInstance(JSONStorage.class);
          storage.dropTable();
          storage.createTable();
        }
      }, new AuthorizeListener() {
        @Override
        public void onAuthorized() {
          Intent launchIntent = getLaunchIntent();
          launchIntent.putExtra("reauthorized", true);
          startActivity(launchIntent);
          if (mFragmentCallback instanceof DashboardContentFragmentCallback) {
            mFragmentCallback.notifyDashboardContentFragmentFinishRequest(EVENT_TYPE.NORMAL, null);
          }
        }

        @Override
        public void onError() {
        }

        @Override
        public void onCancel() {
        }
      }, null
          );
    }

    private Intent getLaunchIntent() {
      PackageManager packageManager = mActivity.getPackageManager();
      return packageManager.getLaunchIntentForPackage(mActivity.getPackageName());
    }

    @Override
    public void onInviteExternalUser(final CommandInterface commandInterface, final JSONObject params) {

      mUiThreadHandler.post(new Runnable() {
        @Override
        public void run() {
          GLog.d("GDB", params.toString());
          if (params.has("URL")) {
            try {
              if (null != mContentView) {
                mContentView.loadUrl(params.getString("URL"));
              } else {
                commandInterface.loadUrl(params.getString("URL"));
              }
            }
            catch (Exception e) {
              GLog.printStackTrace(TAG, e);
            }
          }
        }
      });
    }

    @Override
    public void onDeleteCookie(final CommandInterface commandInterface, final JSONObject params) {

      try {
        String confData = CoreData.get(InternalSettings.ParametersForDeletingCookie, null);
        if (confData == null) {
          GLog.e(TAG, "onDeleteCookie: ParametersForDeletingCookie configuration not found.");
          if (params.has("callback")) {
            String callback = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", "error");
            commandInterface.executeCallback(callback, result);
          }
          return;
        }

        JSONArray keyArrays = new JSONArray(confData);
        for (int i = 0; i < keyArrays.length(); i++) {
          JSONObject filter = keyArrays.getJSONObject(i);
          if (params.getString("key").equals(filter.getString("key"))) {
            String domain = Url.getCookieExternalDomain(filter.getString("domain"));
            JSONArray valueArrays = filter.getJSONArray("names");

            for (int n = 0; n < valueArrays.length(); n++) {
              String key = valueArrays.getString(n);
              CookieStorage.setCookie(domain, key + "=" + ";");
            }

            CookieStorage.sync();
            if (params.has("callback")) {
              String callback = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "success");
              commandInterface.executeCallback(callback, result);
            }

            return;
          }
        }
        GLog.e(TAG, "onDeleteCookie: Target key is not exist. key:" + params.getString("key"));
        if (params.has("callback")) {
          String callback = params.getString("callback");
          JSONObject result = new JSONObject();
          result.put("result", "error");
          commandInterface.executeCallback(callback, result);
        }
      } catch (JSONException e1) {
        GLog.w(TAG, "onDeleteCookie: error occured.");
        try {
          if (params.has("callback")) {
            String callback = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", "error");
            commandInterface.executeCallback(callback, result);
          }
        } catch (JSONException e2) {
          GLog.printStackTrace(TAG, e2);
        }
      }
    }

    @Override
    public void onClose(final CommandInterface commandInterface, final JSONObject params) {
      setActivityResultAndClose(commandInterface, params);
    }

    @Override
    public void onUpdateUser(final CommandInterface commandInterface, final JSONObject params) {
      Core.getInstance().updateLocalUser(new GreeUserListener() {
        public void onSuccess(int index, int count, GreeUser[] users) {
          try {
            if (params.has("callback")) {
              String callback = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "success");
              commandInterface.executeCallback(callback, result);
            }
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          try {
            if (params.has("callback")) {
              String callback = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "error");
              commandInterface.executeCallback(callback, result);
            }
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
      });
    }

    @Override
    public void onGetDeviceInfo(CommandInterface commandInterface, JSONObject params) {
      LocationManager lm = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
      List<String> providers = lm.getAllProviders();
      boolean hasLocationService = !providers.isEmpty();

      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        JSONObject json = new JSONObject();
        json.put("has_location_service", hasLocationService);
        result.put("result", json);
        commandInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    public void onShowPhotoView(final CommandInterface commandInterface, final JSONObject params) {
      Intent intent = new Intent(mActivity, ImageViewActivity.class);
      intent.putExtra("params", params.toString());
      startActivityForResult(intent, getResources().getInteger(RR.integer("gree_request_show_photo_view")));
    }

    public void onReloadUniversalMenu(CommandInterface commandInterface, final JSONObject params) {
      UniversalMenuView um = (UniversalMenuView)mActivity.findViewById(RR.id("gree_u_menu"));
      um.reload();
    }
  }

  private class DashboardSubNaviOnItemChangeListener implements SubNaviOnItemChangeListener {
    @Override
    public void itemChanged(int prePosition, int position) {
      if (null == mContentView) {
        return;
      }

      int tmpSubNaviItemPosition = -1;
      if (null != mCurrentViewData && !mCurrentViewData.mPositionHistory.empty()) {
        tmpSubNaviItemPosition = mCurrentViewData.mPositionHistory.peek();
      }

      if (tmpSubNaviItemPosition != prePosition) {
        if (null == mCurrentViewData) {
          mCurrentViewData = new ViewData();
        }
        mCurrentViewData.mPositionHistory.push(prePosition);

        mContentView.getPerformManager().recordData(mContentView.getPerformData(), PerformanceIndexMap.INDEX_KEY_SELECT_SUBNAVI);
      }
    }
  }

  @Override
  public void onNotificationClick(JSONObject data) {
    try {
      String url = data.getString("url");
      boolean loadInternal = false;
      try {
        loadInternal = data.getInt("launch_external") == 0;
      } catch (JSONException e) {
        loadInternal = true;
      }
      if (loadInternal) {
        loadContentView(url, false);
      } else{
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        getActivity().startActivity(intent);
      }
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }
  }
}
