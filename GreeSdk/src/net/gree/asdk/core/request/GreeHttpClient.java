/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;


import java.io.IOException;

import net.gree.asdk.core.Core;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

public class GreeHttpClient extends DefaultHttpClient {
  final static String TAG = GreeHttpClient.class.getSimpleName();

  public GreeHttpClient() {
    HttpParams params = getParams();
    HttpProtocolParams.setVersion(params, new ProtocolVersion("HTTP", 1, 1));
    params.setBooleanParameter(ClientPNames.HANDLE_AUTHENTICATION, false);
    params.setParameter(CoreProtocolPNames.USE_EXPECT_CONTINUE, false);
    params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 4);
    params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, 2);
    HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
    // set user agent
    String userAgent = Core.getInstance().getUserAgent();
    if(userAgent!=null){
      params.setParameter("http.useragent", userAgent);
    }
  }

  public static void handleGzip(DefaultHttpClient httpclient) {
    try {
      // Handle sending and receiving gzipped content.
      httpclient.addRequestInterceptor(new HttpRequestInterceptor() {
        @Override
        public void process(final HttpRequest request, final HttpContext context)
            throws HttpException, IOException {
          if (!request.containsHeader("Accept-Encoding"))
            request.addHeader("Accept-Encoding", "gzip");
        }
      });
    } catch (Exception e) {
      RequestLogger.getInstance().printStackTrace(TAG, e);
    }
  }

  public void setConnectionTimeout(long millis) {
    this.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
        Integer.valueOf((int) millis));
    this.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, Integer.valueOf((int) millis));
  }
}
