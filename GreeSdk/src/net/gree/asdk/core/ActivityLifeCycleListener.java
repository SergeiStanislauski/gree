package net.gree.asdk.core;

import net.gree.asdk.core.notifications.MessageDispatcher;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

public class ActivityLifeCycleListener implements ActivityLifecycleCallbacks {

  @Override
  public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
  }

  @Override
  public void onActivityDestroyed(Activity activity) {
  }

  @Override
  public void onActivityPaused(Activity activity) {
  }

  @Override
  public void onActivityResumed(Activity activity) {
  }

  @Override
  public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
  }

  @Override
  public void onActivityStarted(Activity activity) {
    Injector.getInstance(MessageDispatcher.class).registerActivity(activity);
  }

  @Override
  public void onActivityStopped(Activity activity) {
    Injector.getInstance(MessageDispatcher.class).unRegisterActivity(activity);
  }

}
