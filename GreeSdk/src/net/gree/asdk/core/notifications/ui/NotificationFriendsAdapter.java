/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.notifications.ui;

import java.util.List;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.notifications.CacheReadMarker;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.storage.JSONStorage;
import net.gree.asdk.core.ui.RoundCornerImage;

import org.apache.http.HeaderIterator;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

public class NotificationFriendsAdapter extends NotificationAdapterBase {
  private final static String TAG = NotificationFriendsAdapter.class.getName();
  
  public final static String FILTER_FRIENDS = "notification_friend";
  
  public NotificationFriendsAdapter(Context context, DisplayMetrics metrics, List<NotificationData> objects) {
    super(context, metrics, objects);
  }
  

  /**
   * Load "Friends" data and place into adapter's objects list
   * @return count of notifications
   */
  public int loadNotifications() {
    JSONStorage storage = Injector.getInstance(JSONStorage.class);
    SQLiteDatabase db = storage.getReadableDatabase();
    Resources r = getContext().getResources();

    Bitmap origIcon = BitmapFactory.decodeResource(r, RR.drawable("gree_notification_user_default"));
    RoundCornerImage rci = new RoundCornerImage(origIcon);
    
    mNotificationType = NotificationFragment.NOTIFICATION_FRIENDS;
    mDisplayObjects.clear();
    int ret = loadByFilter(db, FILTER_FRIENDS, rci.getRoundedBitmap(), LOAD_LIMIT);

    if (ret >= LOAD_LIMIT) {
      NotificationData autopagerize = new NotificationData();
      autopagerize.cellType = CELL_TYPE_AUTOPAGERIZE;
      autopagerize.text = r.getString(RR.string("gree_notification_loading"));
      mDisplayObjects.add(autopagerize);
    }

    mContentCount = ret;

    if ( ret <= 0) { addNoNotification(); }
    
    markAsRead("friend", new OnResponseCallback<String>(){
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        CacheReadMarker marker = new CacheReadMarker(FILTER_FRIENDS);
        marker.execute();
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, response);
      }
      
    });

    return ret;
  }


}
