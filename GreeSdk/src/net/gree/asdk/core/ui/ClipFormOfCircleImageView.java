/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Only if setImageBitmap were used, this class I cut out a circle image.
 */
public class ClipFormOfCircleImageView extends ImageView {

  public ClipFormOfCircleImageView(Context context) {
    super(context);
  }

  public ClipFormOfCircleImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ClipFormOfCircleImageView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  public void setImageBitmap(Bitmap bm) {
    int w = bm.getWidth();
    int h = bm.getHeight();
    int size = w > h ? h : w;
    Bitmap output = Bitmap.createBitmap(size, size, Config.ARGB_8888);
    Canvas canvas = new Canvas(output);

    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, size, size);
    final RectF rectF = new RectF(rect);
    final float roundPx = size / 2;

    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(Color.BLACK);
    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
    canvas.drawBitmap(bm, rect, rect, paint);
    super.setImageBitmap(output);
  }
}
