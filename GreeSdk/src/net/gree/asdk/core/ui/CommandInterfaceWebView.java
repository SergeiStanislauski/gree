/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import net.gree.asdk.core.GLog;

/**
 * Command interface webview
 *
 */
public class CommandInterfaceWebView extends GreeWebViewBase {

  private static final String TAG = "CommandInterfaceWebView";

  private String mName = null;
  private boolean mIsSnsInterfaceAvailable = false;
  private CommandInterfaceWebViewClient mWebViewClient = null;


  /**
   * command interface webview constructor
   * @param context a Context object used to access application assets
   */
  public CommandInterfaceWebView(Context context) {
    super(context);
    super.setUp();
  }

  /**
   * command interface webview constructor
   * @param context a Context object used to access application assets
   * @param attrs an AttributeSet passed to our parent
   */
  public CommandInterfaceWebView(Context context, AttributeSet attrs) {
    super(context, attrs);
    super.setUp();
  }

  /**
   * command interface webview constructor
   * @param context a Context object used to access application assets
   * @param attrs an AttributeSet passed to our parent
   * @param defStyle the default style resource ID
   */
  public CommandInterfaceWebView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    super.setUp();
  }

  /**
   * setup and set command interface webview client
   */
  @Deprecated
  public void setUp() {
    super.setUp();
    setCommandInterfaceWebViewClient(new CommandInterfaceWebViewClient(getContext()));
  }

  /**
   * set name for log
   * @param name name to set
   */
  public void setName(String name) {
    mName = name;
  }

  /**
   * log
   * @param message message for log
   */
  public void log(String message) {
    GLog.d(TAG, null == mName ? message : mName + ":" + message);
  }

  /**
   * set sns interface available
   * @param isAvailable true if available
   */
  public void setSnsInterfaceAvailable(boolean isAvailable) {
    mIsSnsInterfaceAvailable = isAvailable;
  }

  /**
   * check if sns interface available
   * @return true if available
   */
  public boolean isSnsInterfaceAvailable() {
    return mIsSnsInterfaceAvailable;
  }

  /**
   * show received error page 
   * @param message message
   * @param failingUrl failing url
   */
  public void showReceivedErrorPage(String message, String failingUrl) {
    mWebViewClient.onReceivedError(this, WebViewClient.ERROR_UNKNOWN, message, failingUrl);
  }

  /**
   * pause
   */
  public void pause() {
    try {
      WebView.class.getMethod("onPause").invoke(this);
      GLog.d(TAG, "paused " + getUrl());
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  /**
   * resume
   */
  public void resume() {
    try {
      WebView.class.getMethod("onResume").invoke(this);
      GLog.d(TAG, "resumed" + getUrl());
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  /**
   * set webview client and command interface webview client with input client
   * @param client command interface webview client to set
   */
  public void setCommandInterfaceWebViewClient(CommandInterfaceWebViewClient client) {
    super.setWebViewClient(client);
    mWebViewClient = client;
  }

  /**
   * get command interface webview client
   * @return command interface webview client
   */
  public CommandInterfaceWebViewClient getCommandInterfaceWebViewClient() {
    return mWebViewClient;
  }

  /**
   * set web view client with input client (deprecated)
   */
  @Deprecated
  public void setWebViewClient(WebViewClient webViewClient) {
    super.setWebViewClient(webViewClient);
  }

  @Override
  public String toString() {
    return null == mName ? super.toString() : mName;
  }

  // On some device, scroll doesn't work at the top or the bottom. Also,
  // when screen orientation is changed, WebView.computeVerticalScrollRange()
  // returns a wrong value for a certain amount of time.
  // Below is a bad workaround, but seems working.
  @Override
  public void scrollTo(int x, int y) {

    int vScrollExtent = computeVerticalScrollExtent();
    int vScrollRange = computeVerticalScrollRange();

    if (vScrollExtent < vScrollRange) {
      if (y == 0) {
        y = 1;
      } else if (y + vScrollExtent >= vScrollRange) {
          y  = vScrollRange - vScrollExtent - 1;
      }
    }

    super.scrollTo(x, y);
  }

  @Override
  protected void onScrollChanged(int l, int t, int oldl, int oldt) {
    super.onScrollChanged(l, t, oldl, oldt);

    int vScrollExtent = computeVerticalScrollExtent();
    int vScrollRange = computeVerticalScrollRange();

    if (vScrollExtent < vScrollRange) {
      if (t == 0) {
        scrollTo(0, 1);
      } else if (t + vScrollExtent >= vScrollRange) {
        scrollTo(0, vScrollRange - vScrollExtent - 1);
      }
    }
  }
}
