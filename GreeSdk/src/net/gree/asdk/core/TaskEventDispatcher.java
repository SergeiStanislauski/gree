/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.GreePlatformListener.TaskEventListener;

/**
 * This class is used to track GREE's activities and dialogs "open" and "close" events.
 * It is used through the interface GreePlatform.GreePlatformListener.
 * 
 * Only one instance should exist and is hold by the Injector.
 * please call Injector.getInstance(EventListener.class) to get an instance.
 */
public class TaskEventDispatcher {

  private static final String TAG = "TaskEventDispatcher";

  /**
   * Any listener above this value are private,
   * Any listener bellow this value are public.
   */
  public static final int PRIVATE_LISTENER_BASE = 1000;

  /**
   * Use a weakHashmap to remove unReferenced listeners automatically,
   * Holds the Listener as key,
   * the boolean can be use later to see if the listener is used by external developers or internal developers.
   */
  private WeakHashMap<TaskEventListener, Boolean> listeners;
  
  /**
   * Constructor.
   * Do not call this directly,
   * use the single instance through Injector.java :
   * Injector.getInstance(EventListener.class)
   */
  public TaskEventDispatcher() {
    listeners = new WeakHashMap<TaskEventListener, Boolean>();
  }

  /**
   * Register a GreePlatformListener, it will be notified every time a GREE dialog or activity opens or closes.
   * If the boolean publicListener is set as true, the listener will only receive public events. 
   * The public events are those defined in GreePlatform.GreePlatformListener
   * (DASHBOARD_ACTIVITY, NOTIFICATION_BOARD_ACTIVITY, ... , PAYMENT_DIALOG).
   * if the type of the event is equal or above PRIVATE_EVENT then the event is considered private.
   * 
   * @param listener the listener to be registered.
   * @param publicListener set this to true to be notified of only public events.
   */
  public void registerTaskEventListener(TaskEventListener listener, boolean publicListener) {
    GLog.v(TAG, "registerEventListener "+listener);
    synchronized (listeners) {
      listeners.put(listener, publicListener);
    }
  }

  /**
   * Unregister a GreePlatformListener, it will no longer receive the events.
   * 
   * @param listener the listener to unregister.
   */
  public void unRegisterTaskEventListener(TaskEventListener listener) {
    GLog.v(TAG, "unRegisterEventListener "+listener);
    synchronized (listeners) {
      listeners.remove(listener);
    }
  }

  /**
   * Dispatch an event to all registered listeners
   * @param type : one of the GreePlatformListener class events:
   * CLASS_DASHBOARD_ACTIVITY, CLASS_NOTIFICATION_BOARD_ACTIVITY, ... , CLASS_PAYMENT_DIALOG
   * @param eventType : one of the GreePlatformListener event type : EVENT_OPEN, EVENT_CLOSE...
   * @param instance an activity or dialog instance corresponding to indicated type.
   * @param params the parameters corresponding to the event.
   */
  public void dispatchEvent(int classType, int eventType, Object instance, Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    params.put(GreePlatformListener.KEY_CLASS_TYPE, classType);
    params.put(GreePlatformListener.KEY_EVENT_TYPE, eventType);
    params.put(GreePlatformListener.KEY_INSTANCE, instance);

    GLog.v(TAG, "Trying to send event class:"+classType+ " event:"+eventType+" params"+params);

    synchronized (listeners) {
      Iterator<TaskEventListener> iterator = listeners.keySet().iterator();
      while (iterator.hasNext()) {
        TaskEventListener current = iterator.next();
        if (current != null) {
          boolean publicListener = listeners.get(current);
          //dispatch public events to all listeners, dispatch private events to private listeners only
          if (isPublic(params) || (!publicListener)) {
            current.onEvent(params);
          }
        }
      }
    }
  }

  /**
   * Check if given event is public,
   * -It needs to have a class type that is public (bellow PRIVATE_LISTENER_BASE),
   * DashboardActivity, NotificationBoardActivity, ShareDialog, RequestDialog ...
   * As defined in GreePlatform.CLASS_*
   * 
   * -It needs to be a public event (bellow PRIVATE_LISTENER_BASE),
   * Open, close, cancel
   * As defined in GreePlatform.EVENT_*
   * 
   * @param params
   * @return true it the event is public, false otherwise
   */
  public static boolean isPublic(Map<String, Object> params) {
    return ((GreePlatformListener.getEventType(params) < PRIVATE_LISTENER_BASE) &&
        (GreePlatformListener.getClassType(params) < PRIVATE_LISTENER_BASE));
  }
  
}
