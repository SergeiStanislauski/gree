/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.inject;

/**
 * Developer should create a subclass of this class to setup module, for example:
 * 
 * <pre>
 * public class CoreModule extends AbstractModule {
 * 
 *   private Context context;
 * 
 *   public CoreModule(Context context) {
 *     this.context = context;
 *   }
 * 
 *   &#064;Override
 *   protected void configure() {
 *     bind(Context.class, context);
 *     bind(GConnectivityManager.class);
 *   }
 * }
 * </pre>
 *
 * @see Binder for the bind syntax
 */
public abstract class AbstractModule {

  private Binder binder;

  public final synchronized void configure(Binder binder) {
    this.binder = binder;
    configure();
  }

  protected abstract void configure();

  protected void bind(Class<?> clz) {
    binder.bind(clz);
  }

  protected <T> void bind(Class<T> clz, T instance) {
    binder.bind(clz, instance);
  }

  protected <T> void bind(Class<T> clz, Class<? extends T> impl) {
    binder.bind(clz, impl);
  }
}
