/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.ui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.GreePlatformListener.TaskEventListener;
import net.gree.asdk.api.GreePlatformSettings;
import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.GreeUser.GreeUserListener;
import net.gree.asdk.api.ui.InviteDialog;
import net.gree.asdk.api.ui.RequestDialog;
import net.gree.asdk.api.ui.ShareDialog;
import net.gree.asdk.api.wallet.Balance;
import net.gree.asdk.api.wallet.Balance.BalanceListener;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.TaskEventDispatcher;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.contact.ContactListFactory;
import net.gree.asdk.core.notifications.LocalNotificationRegister;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.ui.ServiceResultreceiverActivity.OnServiceResultListener;
import net.gree.asdk.core.ui.WebViewDialog.OnWebViewDialogListener;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Scheme;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/*
 * This class is WebView used in Gree Application, include JavaScript interface.
 * @author GREE, Inc.
 */
public class GreeWebView extends GreeWebViewBase {
  /**
   * a tag for logging of this class
   */
  private static final String TAG = "GreeWebView";

  public static final String INTENT_TYPE_RELOAD = "GREEApp/Reload";

  /**
   * Instance of CommandInterface
   */
  private CommandInterface mInterface;
  /**
   * handler for detecting events of share dialog
   */
  private ShareDialogHandler mShareDialogHandler;
  /**
   * Instance of WebViewDialog
   */
  private WebViewDialog mWebViewDialog;
  /**
   * handler for UI thread
   */
  private Handler mUiHandler;
  /**
   * handler for detecting events for request dialog
   */
  private RequestDialogHandler mRequestDialogHandler;
  /**
   * handler for detecting events for invite dialog
   */
  private InviteDialogHandler mInviteDialogHandler;
  /**
   * listener for launch service
   */
  private OnLaunchServiceEventListener mLaunchServiceEventListener = null;

  /**
   * This function return a handler of UI thread.
   * @return
   */
  public Handler getUiHandler() { return mUiHandler; }

  /**
   * Constructor
   * @param context The context in which the activity is running.
   */
  public GreeWebView(Context context) {
    super(context);
    mShareDialogHandler = new ShareDialogHandler();
    mUiHandler = new Handler();
    mRequestDialogHandler = new RequestDialogHandler();
    mInviteDialogHandler = new InviteDialogHandler();
  }

  /**
   * Constructor
   * @param context The context in which the activity is running.
   * @param attrs attributes of this WebView
   */
  public GreeWebView(Context context, AttributeSet attrs) {
    super(context, attrs);
    mShareDialogHandler = new ShareDialogHandler();
    mUiHandler = new Handler();
    mRequestDialogHandler = new RequestDialogHandler();
    mInviteDialogHandler = new InviteDialogHandler();
  }

  /**
   * Constructor
   * @param context The context in which the activity is running.
   * @param attrs attributes of this WebView
   * @param defStyle style definition
   */
  public GreeWebView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    mShareDialogHandler = new ShareDialogHandler();
    mUiHandler = new Handler();
    mRequestDialogHandler = new RequestDialogHandler();
    mInviteDialogHandler = new InviteDialogHandler();
  }

  /**
   * This function setup this class. A consumer should call this function after constructor is called, for example, in onCreate.
   * This function have to be called before calling other functions in this class.
   */
  @Override
  public void setUp() {
    super.setUp();
    mInterface = new CommandInterface("protonapp");
    mInterface.addOnCommandListener(new GreeWebViewCommandListener());
    mInterface.setWebView(this);
  }

  /**
   * This function clean the instance of this class.
   */
  @Override
  public void cleanUp() {
    mInterface.setWebView(null);

    mInterface = null;
    super.cleanUp();
  }

  /**
   * getter for Commandinterface
   * @return The instance of CommandInterface
   */
  public CommandInterface getCommandInterface() {
    return mInterface;
  }

  /**
   * setter for adapter of CommandInterface
   * @param listener The instance of CommandInterface.OnCommandListenerAdapter
   */
  public void addNewListener(CommandInterface.OnCommandListenerAdapter listener) {
    mInterface.addOnCommandListener(listener);
  }

  /**
   * Event listener for Launch Service. It is used at javascript native bridge.
   */
  public interface OnLaunchServiceEventListener {
    /**
     * This is called when launchService is called from javascript.
     * @param from where this function is called from
     * @param action what action should do in this function
     * @param target launch target
     * @param params parameters for launching services
     * @return
     */
    public boolean onLaunchService(String from, String action, String target, JSONObject params);
    /**
     * When the cooperation processing is finished, this function is called from javascript 
     * @param from where this function is called from
     * @param action what action should do in this function
     * @param params parameters for closing services
     */
    public void onNotifyServiceResult(String from, String action, JSONObject params);
  }

  /**
   * setter for LaunchServiceEventListener
   * @param listener The instance of OnLaunchServiceEventListener
   */
  public void setOnLaunchServiceEventListener(OnLaunchServiceEventListener listener) {
    mLaunchServiceEventListener = listener;
  }

  /**
   * Inner class for javascript native bridge
   */
  public class GreeWebViewCommandListener extends CommandInterface.OnCommandListenerAdapter {
    TaskEventListener mShowDashboardListener;
    /**
     * This function store the value specified by key to LocalSettings
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have key and value.
     */
    @Override
    public void onSetConfig(final CommandInterface commandInterface, final JSONObject params) {
      String key = "";
      String value = "";
      try {
        key = params.getString("key");
        value = params.getString("value");
        if (GreeWebViewUtil.canSetConfigurationKey(key)) {
          Core.getInstance().storeLocalSetting(key, value);
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
        GLog.d(TAG, "invalid  input: key:"+key+", value:"+value);
      }
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        JSONObject json = new JSONObject();
        json.put(key, value);
        result.put("result", json);
        commandInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function get the value specified by key from LocalSettngs
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have key to get a value and callback
     */
    @Override
    public void onGetConfig(final CommandInterface commandInterface, final JSONObject params) {
      String key = "";
      String value = "";
      try {
        key = params.getString("key");
        if (GreeWebViewUtil.canGetConfigurationKey(key)) {
          value = CoreData.get(key);
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
        GLog.d(TAG, "invalid  input: key:"+key+", value:"+value);
      }
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", value);
        commandInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function get the value specified by key from LocalSettngs as list
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have not key, but should have callback
     */
    @Override
    public void onGetConfigList(final CommandInterface commandInterface, final JSONObject params) {
      Map<String, Object> map = CoreData.getParams();
      JSONObject json = new JSONObject();
      
      Set<String> set = map.keySet();
      try {
        for (String key : set) {
          if (GreeWebViewUtil.canGetConfigurationKey(key)) {
            json.put(key, CoreData.get(key));
          }
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", json);
        commandInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function get the list of application scheme that is installed in the device
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have not key, but should have callback
     */
    @Override
    public void onGetAppList(final CommandInterface commandInterface, final JSONObject params) {
      JSONArray check_app_list = params.optJSONArray("schemes");
      JSONArray applist = GreeWebViewUtil.getInstalledApps(getContext(), check_app_list);
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", applist);
        commandInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function register a timer to show notification
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have key and value to input {@link LocalNotificationRegister#regist(Map) regist}
     */
    @Override
    public void onRegisterLocalNotificationTimer(final CommandInterface commandInterface, final JSONObject params) {
      Map<String, Object> map = new TreeMap<String, Object>();
      @SuppressWarnings("unchecked")
      Iterator<String> keys = params.keys();
      while (keys.hasNext()) {
        String key = keys.next();
        Object value;
        try {
          value = params.get(key);
          map.put(key, value);
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
          try {
            String callback = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", "error");
            mInterface.executeCallback(callback, result);
          } catch (JSONException e1) {
            GLog.printStackTrace(TAG, e1);
          }
        }
      }

      boolean reg_result = LocalNotificationRegister.regist(map);
      String result_string;
      if (reg_result == true) {
        result_string = "registered";
      } else {
        result_string = "error";
      }
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", result_string);
        mInterface.executeCallback(callback, result);
      } catch (JSONException e1) {
        GLog.printStackTrace(TAG, e1);
      }
    }

    /**
     * This function cancel a timer that is registered by {@link GreeWebViewCommandListener#onRegisterLocalNotificationTimer(CommandInterface, JSONObject) onRegisterLocalNotificationTimer}
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have notifyId that is specified at {@link GreeWebViewCommandListener#onRegisterLocalNotificationTimer(CommandInterface, JSONObject) onRegisterLocalNotificationTimer}
     */
    @Override
    public void onCancelLocalNotificationTimer(final CommandInterface commandInterface, final JSONObject params) {
      String cancelled = "error";
      try {
        Integer notifyId = Integer.valueOf(params.getInt("notifyId"));
        boolean ret = LocalNotificationRegister.cancel(notifyId);
        if (ret) {
          cancelled = "cancelled";
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", cancelled);
        mInterface.executeCallback(callback, result);
      } catch (JSONException e1) {
        GLog.printStackTrace(TAG, e1);
      }
    }

    /**
     * This function return to javascript whether getting local notification is enable or not.
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have callback
     */
    @Override
    public void onGetLocalNotificationEnabled(final CommandInterface commandInterface, final JSONObject params) {
      boolean enabled = GreePlatformSettings.getLocalNotificationEnabled();
      String enable = "false";
      if (enabled == true) {
        enable = "true";
      }
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("enabled", enable);
        mInterface.executeCallback(callback, result);
      } catch (JSONException e1) {
        GLog.printStackTrace(TAG, e1);
      }
    }

    /**
     * This function set enable or not to get local notification.
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have enabled
     */
    @Override
    public void onSetLocalNotificationEnabled(final CommandInterface commandInterface, final JSONObject params) {
      try {
        String enabled = params.getString("enabled");
        if (TextUtils.isEmpty(enabled)) {
          return;
        }
        if (enabled.equals("true")) {
          GreePlatformSettings.setLocalNotificationEnabled(true);
        } else {
          GreePlatformSettings.setLocalNotificationEnabled(false);
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function launch some service to cooperate other service
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have from, action, target and params
     */
    @Override
    public void onLaunchService(final CommandInterface commandInterface, final JSONObject params) {
      if (mLaunchServiceEventListener == null) {
        GLog.e(TAG, "no listener is set");
        return;
      }
      final String from;
      final String action;
      final String target;
      try {
        from = params.getString("from");
        action = params.getString("action");
        target = params.getString("target");
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
        return;
      }
      final JSONObject ext_params = params.optJSONObject("params");
      if (TextUtils.isEmpty(target)) {
        GLog.e(TAG, "onLaunchService target is empty");
        return;
      }
      
      if (target.equals("browser")) {
        mUiHandler.post(new Runnable() {
          @Override
          public void run() {
            ServiceResultreceiverActivity.prepareServiceResultReceiver(getContext(), new OnServiceResultListener() {
              @Override
              public void notifyServiceReesult(String from, String action, Uri result_scheme) {
                if (mLaunchServiceEventListener == null) {
                  GLog.e(TAG, "no listener is set");
                  return;
                }
                mLaunchServiceEventListener.onNotifyServiceResult(from, action, null);
              }
            });
          }
        });
      } else if (!target.equals("self")) {
        GLog.e(TAG, "onLaunchService target is invalid");
        return;
      }
      mUiHandler.post(new Runnable() {
        @Override
        public void run() {
          boolean result = mLaunchServiceEventListener.onLaunchService(from, action, target, ext_params);
          if (result == false) {
            ServiceResultreceiverActivity.finishActivity();
          }
        }
      });
    }

    /**
     * This function is notified the result of LaunchService from javascript.
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have from, action and params
     */
    @Override
    public void onNotifyServiceResult(final CommandInterface commandInterface, final JSONObject params) {
      if (mLaunchServiceEventListener == null) {
        GLog.e(TAG, "no listener is set");
        return;
      }
      final String from;
      final String action;
      try {
        from = params.getString("from");
        action = params.getString("action");
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
        return;
      }
      mUiHandler.post(new Runnable() {
        @Override
        public void run() {
          JSONObject ext_params = params.optJSONObject("params");
          mLaunchServiceEventListener.onNotifyServiceResult(from, action, ext_params);
        }
      });
    }

    /**
     * This function store a value specified by key to local storage
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have key and value
     */
    @Override
    public void onSetValue(final CommandInterface commandInterface, final JSONObject params) {
      mLocalStorage.putString(params.optString("key"), params.optString("value"));
    }

    /**
     * This function get a value specified by key from local storage
     * @param commandInterface The instance of CommandInterface
     * @param params This parameter should have key to get the value
     */
    @Override
    public void onGetValue(final CommandInterface commandInterface, final JSONObject params) {
      String value = mLocalStorage.getString(params.optString("key"));
      try {
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("value", value);
        mInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function execute to get the list of contact
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback to get the value of contact list
     */
    @Override
    public void onGetContactList(final CommandInterface commandInterface, final JSONObject params) {
      String ret = ContactListFactory.getInstance().getContactList(getContext());
      try {
        String callbackId = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", ret);
        mInterface.executeCallback(callbackId, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function launch mail composer that is external application.
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback
     */
    @Override
    public void onLaunchMailer(final CommandInterface commandInterface, final JSONObject params) {
      boolean issuccess = GreeWebViewUtil.launchMailSending(GreeWebView.this.getContext(), params);
      String ret;
      if (issuccess == false) {
        ret = "fail";
      } else {
        ret = "success";
      }
      String callbackId;
      try {
        callbackId = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", ret);
        mInterface.executeCallback(callbackId, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function launch sms composer that is external application
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback
     */
    @Override
    public void onLaunchSMSComposer(final CommandInterface commandInterface, final JSONObject params) {
      int launch_ret = GreeWebViewUtil.launchSmsComposer(GreeWebView.this.getContext(), params);
      String ret;
      if (launch_ret == GreeWebViewUtil.SMS_LAUNCH_SUCCESS) {
        ret = "success";
      } else if (launch_ret == GreeWebViewUtil.SMS_NO_SMS_APP) {
        ret = "no_sms_app";
      } else {
        ret = "fail";
      }
      String callbackId;
      try {
        callbackId = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", ret);
        mInterface.executeCallback(callbackId, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function launch bowser that is external application
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have URL
     */
    @Override
    public void onLaunchNativeBrowser(final CommandInterface commandInterface, final JSONObject params) {
      int launch_ret = GreeWebViewUtil.launchNativeBrowser(GreeWebView.this.getContext(), params);
      int ret;
      String reason = null;
      if (launch_ret == GreeWebViewUtil.BROWSER_LAUNCH_SUCCESS) {
        ret = 0;// success
      } else if (launch_ret == GreeWebViewUtil.BROWSER_ERROR_INVAL_ARGS) {
        reason = "invalargs";
        ret = -1;// fail
      } else if (launch_ret == GreeWebViewUtil.BROWSER_ERROR_NO_APP) {
        reason = "no_browser_app";
        ret = -1;// fail
      } else {
        reason = "othererror";
        ret = -1;// fail
      }
      String callbackId;
      try {
        callbackId = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", ret);
        result.put("reason", reason);
        mInterface.executeCallback(callbackId, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function show the alert view.
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback, title, message, list of button and index of cancel button
     */
    @Override
    public void onShowAlertView(final CommandInterface commandInterface, final JSONObject params) {
      int show_result = GreeWebViewUtil.showAlertView(GreeWebView.this.getContext(), params, new GreeWebViewUtil.OnActionListener() {
        @Override
        public void onAction(int index) {
          String callbackId;
          try {
            callbackId = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", index);
            mInterface.executeCallback(callbackId, result);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
      });
      if (show_result != GreeWebViewUtil.MESSAGE_DIALOG_SUCCESS) {
        String callbackId;
        try {
          callbackId = params.getString("callback");
          JSONObject result = new JSONObject();
          result.put("result", "error");
          mInterface.executeCallback(callbackId, result);
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        }
      }
    }

    /**
     * This function show share dialog
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback to get event of share dialog, and parameters to set {@link ShareDialog#setParams(TreeMap) setParams}
     */
    @Override
    public void onShowShareDialog(final CommandInterface commandInterface, final JSONObject params) {
      mUiHandler.post(new Runnable () {
        @Override
        public void run() {
          ShareDialog dialog = GreeWebViewUtil.showShareDialog(GreeWebView.this.getContext(), params,
            mShareDialogHandler, new ShareDialogHandler.OnShareDialogListener() {
              @Override
              public void onAction(int action, Object obj) {
                String action_name;
                Object param = null;
                if (action == ShareDialog.OPENED) {
                  action_name = "open";
                } else if (action == ShareDialog.CLOSED) {
                  action_name = "close";
                  if (obj != null) {
                    param = obj;
                  }
                } else {
                  action_name = "error";
                }
                String callbackId;
                try {
                  callbackId = params.getString("callback");
                  JSONObject result = new JSONObject();
                  result.put("result", action_name);
                  result.put("param", param);
                  mInterface.executeCallback(callbackId, result);
                } catch (JSONException e) {
                  GLog.printStackTrace(TAG, e);
                }
              }
            });
          if (dialog == null) {
            String callbackId;
            try {
              callbackId = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "error");
              mInterface.executeCallback(callbackId, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }
        }
      });
    }

    /**
     * This function launch native GREE application
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have URL and android_src
     */
    @Override
    public void onLaunchNativeApp(final CommandInterface commandInterface, final JSONObject params) {
      int launch_ret = GreeWebViewUtil.launchNativeApplication(GreeWebView.this.getContext(), params);
      boolean ret;
      String reason = null;
      if (launch_ret == GreeWebViewUtil.NATIVEAPP_LAUNCH_SUCCESS) {
        return;
      } else if (launch_ret == GreeWebViewUtil.NATIVEAPP_ERROR_INVAL_ARGS) {
        reason = "invalargs";
        ret = false;// fail
      } else if (launch_ret == GreeWebViewUtil.NATIVEAPP_LAUNCH_MARKET) {
        reason = "no_app_and_launch_market";
        ret = false;// fail
      } else {
        reason = "othererror";
        ret = false;// fail
      }
      String callbackId;
      try {
        callbackId = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", ret);
        result.put("reason", reason);
        mInterface.executeCallback(callbackId, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function show WebViewDialog
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have URL and callback to detect result of WebViewDialog
     */
    @Override
    public void onShowWebViewDialog(final CommandInterface commandInterface, final JSONObject params) {
      mUiHandler.post(new Runnable() {
        @Override
        public void run() {
          mWebViewDialog = GreeWebViewUtil.showWebViewDialog(GreeWebView.this.getContext(), params, new OnWebViewDialogListener() {
            @Override
            public void onAction(int action) {
              String callbackId;
              String ret;
              if (action == WebViewDialog.OPENED) {
                ret = "open";
              } else if (action == WebViewDialog.CLOSED){
                ret = "close";
              } else {
                ret = "error";
              }
              try {
                callbackId = params.getString("callback");
                JSONObject result = new JSONObject();
                result.put("result", ret);
                Object returnData = null;
                if (mWebViewDialog != null) {
                    returnData = mWebViewDialog.getReturnData();
                }
                if (returnData == null) {
                  returnData = new JSONObject();
                }
                result.put("data", returnData);

                mInterface.executeCallback(callbackId, result);
              } catch (JSONException e) {
                GLog.printStackTrace(TAG, e);
              }
            }
          });
          if (mWebViewDialog == null) {
            try {
              String callbackId;
              callbackId = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "error");
              mInterface.executeCallback(callbackId, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }
        }
        
      });
    }

    /**
     * This function show request dialog
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback to get result of request dialog and parameter {@link RequestDialog#setParams(TreeMap) setParams}
     */
    @Override
    public void onShowRequestDialog(final CommandInterface commandInterface, final JSONObject params) {
      mUiHandler.post(new Runnable () {
        @Override
        public void run() {
          RequestDialog dialog = GreeWebViewUtil.showRequestDialog(GreeWebView.this.getContext(), params,
            mRequestDialogHandler, new RequestDialogHandler.OnRequestDialogListener() {
              @Override
              public void onAction(int action, Object obj) {
                String action_name;
                Object param = null;
                if (action == RequestDialog.OPENED) {
                  action_name = "open";
                } else if (action == RequestDialog.CLOSED) {
                  action_name = "close";
                  if (obj != null) {
                    param = obj;
                  }
                } else {
                  action_name = "error";
                }
                String callbackId;
                try {
                  callbackId = params.getString("callback");
                  JSONObject result = new JSONObject();
                  result.put("result", action_name);
                  result.put("param", param);
                  mInterface.executeCallback(callbackId, result);
                } catch (JSONException e) {
                  GLog.printStackTrace(TAG, e);
                }
              }
            });
          if (dialog == null) {
            String callbackId;
            try {
              callbackId = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "error");
              mInterface.executeCallback(callbackId, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }
        }
      });
    }

    /**
     * This function record analytics data
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have parameters {@link Logger#recordLogInWebView(JSONObject) recordLogInWebView}
     */
    @Override
    public void onRecordAnalyticsData(final CommandInterface commandInterface, final JSONObject params) {
      int ret = Logger.recordLogInWebView(params);
      String result_str;
      if (ret == -1) {
        result_str = "error";
      } else {
        result_str = "success";
      }
      try {
        String callbackId = params.optString("callback");
        if (TextUtils.isEmpty(callbackId)) {
          return;
        }
        JSONObject result = new JSONObject();
        result.put("result", result_str);
        mInterface.executeCallback(callbackId, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    /**
     * This function flush the analytics data which is stored in the device
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should not have any parameters
     */
    @Override
    public void onFlushAnalyticsData(final CommandInterface commandInterface, final JSONObject params) {
      Logger.flushLog();
    }

    /**
     * This function show dashboard 
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should not have any parameters, if params have URL, dashboard is shown with URL specified
     */
    @Override
    public void onShowDashboard(final CommandInterface commandInterface, final JSONObject params) {
      int show_result = GreeWebViewUtil.showDashboard(GreeWebView.this.getContext(), params);
      String error = "";
      boolean ret = false;
      if (show_result == GreeWebViewUtil.DASHBOARD_SUCCESS) {
        ret = true;
      } else if (show_result == GreeWebViewUtil.DASHBOARD_ERROR_INVALARG) {
        error = "invalidargs";
      } else {
        error = "others";
      }

      executeOpenDashboardCallback(params, "callback", ret, error);
      executeOpenDashboardCallback(params, "openCallback", ret, error);

      final String closeCallbackId = params.optString("closeCallback");
      if (!TextUtils.isEmpty(closeCallbackId)) {
        mShowDashboardListener = new TaskEventListener() {
          public void onEvent(Map<String, Object> params) {
            if (GreePlatformListener.CLASS_DASHBOARD_ACTIVITY == GreePlatformListener.getClassType(params) &&
                GreePlatformListener.EVENT_DESTROYED == GreePlatformListener.getEventType(params)) {
              mInterface.executeCallback(closeCallbackId, new JSONObject());
              new Thread(new Runnable() {
                public void run() {
                  Injector.getInstance(TaskEventDispatcher.class).unRegisterTaskEventListener(mShowDashboardListener);
                  mShowDashboardListener = null;
                }
              }).start();
            }
          }
        };
        Injector.getInstance(TaskEventDispatcher.class).registerTaskEventListener(mShowDashboardListener, true);
      }
    }

    private void executeOpenDashboardCallback(JSONObject params, String key, boolean ret, String error) {
      try {
        String callbackId = params.optString(key);
        if (!TextUtils.isEmpty(callbackId)) {
          JSONObject result = new JSONObject();
          result.put("result", ret);
          result.put("error", error);
          mInterface.executeCallback(callbackId, result);
        }
      } catch (JSONException e) {
        GLog.d(TAG, e.getMessage());
      }
    }

    /**
     * This function show invite dialog
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback to get result of request dialog and parameter {@link InviteDialog#setParams(TreeMap) setParams}
     */
    @Override
    public void onShowInviteDialog(final CommandInterface commandInterface, final JSONObject params) {
      mUiHandler.post(new Runnable () {
        @Override
        public void run() {
          InviteDialog dialog = GreeWebViewUtil.showInviteDialog(GreeWebView.this.getContext(), params,
            mInviteDialogHandler, new InviteDialogHandler.OnInviteDialogListener() {
              @Override
              public void onAction(int action, Object obj) {
                String action_name;
                Object param = null;
                if (action == InviteDialog.OPENED) {
                  action_name = "open";
                } else if (action == InviteDialog.CLOSED) {
                  action_name = "close";
                  if (obj != null) {
                    param = obj;
                  }
                } else {
                  action_name = "error";
                }
                String callbackId;
                try {
                  callbackId = params.getString("callback");
                  JSONObject result = new JSONObject();
                  result.put("result", action_name);
                  result.put("param", param);
                  mInterface.executeCallback(callbackId, result);
                } catch (JSONException e) {
                  GLog.printStackTrace(TAG, e);
                }
              }
            });
          if (dialog == null) {
            String callbackId;
            try {
              callbackId = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "error");
              mInterface.executeCallback(callbackId, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }
        }
      });
    }

    /**
     * This function delete cookie of domain specified by params
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have key
     */
    @Override
    public void onDeleteCookie(final CommandInterface commandInterface, final JSONObject params) {
      try {
        String confData = CoreData.get(InternalSettings.ParametersForDeletingCookie, null);
        if (confData == null) {
          GLog.e(TAG, "onDeleteCookie: ParametersForDeletingCookie configuration not found.");
          if (params.has("callback")) {
            String callback = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", "error");
            commandInterface.executeCallback(callback, result);
          }
          return;
        }

        JSONArray keyArrays = new JSONArray(confData);
        for (int i = 0; i < keyArrays.length(); i++) {
          JSONObject filter = keyArrays.getJSONObject(i);
          if (params.getString("key").equals(filter.getString("key"))) {
            String domain = Url.getCookieExternalDomain(filter.getString("domain"));
            JSONArray valueArrays = filter.getJSONArray("names");

            // Output cookie debug information.
            GLog.d(TAG, "onDeleteCookie: Domain[" + domain + "] has these cookies:" + CookieStorage.getCookieFor(domain));
            GLog.d(TAG, "onDeleteCookie: Target Name List:" + valueArrays.toString());

            for (int n = 0; n < valueArrays.length(); n++) {
              String key = valueArrays.getString(n);
              CookieStorage.setCookie(domain, key + "=" + ";");
            }

            CookieStorage.sync();
            if (params.has("callback")) {
              String callback = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "success");
              commandInterface.executeCallback(callback, result);
            }

            return;
          }
        }
        GLog.e(TAG, "onDeleteCookie: Target key is not exist. key:" + params.getString("key"));
        if (params.has("callback")) {
          String callback = params.getString("callback");
          JSONObject result = new JSONObject();
          result.put("result", "error");
          commandInterface.executeCallback(callback, result);
        }
      } catch (JSONException e1) {
        GLog.w(TAG, "onDeleteCookie: error occured.");
        try {
          if (params.has("callback")) {
            String callback = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", "error");
            commandInterface.executeCallback(callback, result);
          }
        } catch (JSONException e2) {
          GLog.printStackTrace(TAG, e2);
        }
      }
    }

    /**
     * This function update local user information
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback to get result of update local user information
     */
    @Override
    public void onUpdateUser(final CommandInterface commandInterface, final JSONObject params) {
      Core.getInstance().updateLocalUser(new GreeUserListener() {
        public void onSuccess(int index, int count, GreeUser[] users) {
          try {
            if (params.has("callback")) {
              String callback = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "success");
              commandInterface.executeCallback(callback, result);
            }
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          try {
            if (params.has("callback")) {
              String callback = params.getString("callback");
              JSONObject result = new JSONObject();
              result.put("result", "error");
              commandInterface.executeCallback(callback, result);
            }
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
      });
    }

    /**
     * This function get various informations regarding application runtime environment
     * @param commandInterface The instance of CommandInterface 
     * @param params This parameter should have callback to get result of update local user information
     */
    @Override
    public void onGetRuntimeInfo(final CommandInterface commandInterface, final JSONObject params) {
      Context context = Core.getInstance().getContext();
      HashMap<String, String> cookieMap = CookieStorage.toHashMap();
      JSONObject json = new JSONObject();
      String deviceName = "Android";
      String deviceArch = String.format(Locale.US, "%s/%s", Build.MANUFACTURER, Build.MODEL);
      String deviceVersion = Build.FINGERPRINT;
      String osName = Util.getOsName();
      String osVersion = Build.VERSION.RELEASE;
      String sdkVersion = Core.getSdkVersion();
      String sdkBuild = Core.getSdkBuild();
      String middlewareName = cookieMap.get(CookieStorage.KEY_FOR_MW_NAME);
      String middlewareVersion = cookieMap.get(CookieStorage.KEY_FOR_MW_VERSION);
      String appName = context.getPackageName();
      String appVersion = String.valueOf(Util.getVersionCode(context, appName));
      String urlScheme = Scheme.getCurrentAppScheme();
      String userAgent = Core.getInstance().getUserAgent();

      // android only
      Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
      DisplayMetrics metrics = new DisplayMetrics();
      display.getMetrics(metrics);
      String density = String.valueOf(metrics.density);
      String densityDpi = String.valueOf(metrics.densityDpi);
      String scaledDensity = String.valueOf(metrics.scaledDensity);
      String widthPixels = String.valueOf(metrics.widthPixels);
      String heightPixels = String.valueOf(metrics.heightPixels);
      String xDpi = String.valueOf(metrics.xdpi);
      String yDpi = String.valueOf(metrics.ydpi);
      String densityType = Util.getDensityType(metrics.densityDpi);

      try {
        json.put("device_name", deviceName);
        json.put("device_arch", deviceArch);
        json.put("device_version", deviceVersion);
        json.put("os_name", osName);
        json.put("os_version", osVersion);
        json.put("sdk_version", sdkVersion);
        json.put("sdk_build", sdkBuild);
        json.put("middleware_name", middlewareName);
        json.put("middleware_version", middlewareVersion);
        json.put("app_name", appName);
        json.put("app_version", appVersion);
        json.put("url_scheme", urlScheme);
        json.put("user_agent", userAgent);

        JSONObject displayMetrics = new JSONObject();
        displayMetrics.put("density", density);
        displayMetrics.put("density_dpi", densityDpi);
        displayMetrics.put("scaled_density", scaledDensity);
        displayMetrics.put("width_pixels", widthPixels);
        displayMetrics.put("height_pixels", heightPixels);
        displayMetrics.put("x_dpi", xDpi);
        displayMetrics.put("y_dpi", yDpi);
        displayMetrics.put("density_type", densityType);
        json.put("display_metrics", displayMetrics);
        String callback = params.getString("callback");
        JSONObject result = new JSONObject();
        result.put("result", json);
        commandInterface.executeCallback(callback, result);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    @Override
    public void onGetWalletBalance(final CommandInterface commandInterface, final JSONObject params) {
      new Balance().load(new BalanceListener() {
        public void onSuccess(long balance) {
          try {
            JSONObject json = new JSONObject();
            json.put("result", "success");
            json.put("balance", balance);
            String callback = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", json);
            commandInterface.executeCallback(callback, result);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          try {
            JSONObject json = new JSONObject();
            json.put("result", "fail");
            String callback = params.getString("callback");
            JSONObject result = new JSONObject();
            result.put("result", json);
            commandInterface.executeCallback(callback, result);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
      });
    }
  }

  public static class ReloadReceiver extends BroadcastReceiver {
    private GreeWebView mWebView;

    public ReloadReceiver(GreeWebView webView) {
      super();
      mWebView = webView;
    }

    public void onReceive(Context context, Intent intent) {
      if (Intent.ACTION_SEND.equals(intent.getAction()) &&
          GreeWebView.INTENT_TYPE_RELOAD.equals(intent.getType())) {
        Util.runOnUiThread(new Runnable() {
          public void run() {
            mWebView.reload();
          }
        });
      }
    }
  }
}
