/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.util.ArrayList;

import org.apache.http.HeaderIterator;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.connect.Like;
import net.gree.asdk.core.connect.Mood;
import net.gree.asdk.core.connect.Photo;
import net.gree.asdk.core.connect.Url;
import net.gree.asdk.core.request.OnResponseCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.ZoomControls;

/**
 * This activity is started by mood on stream.
 * Users can see their photo by using this activity.
 */
public class ImageViewActivity extends Activity implements VersionedGestureDetector.OnGestureListener {
  private static final String LOG_TAG = "ImageViewActivity";
  private static final int OPTION_MENU_INDEX_SAVE = Menu.FIRST;
  private static final int OPTION_MENU_INDEX_EDIT = Menu.FIRST + 1;

  private static final String IMAGE_TYPE_ALBUM = "album";
  private static final String IMAGE_TYPE_MOOD = "mood";

  private static final int DEFAULT_MAX_IMAGE_VIEW_SIZE = 10;
  private static final int MAX_REQUEST_LIMIT = 100;

  private static final float DEFAULT_ZOOM_CONTROL_SCALE = 1.2f;
  private static final float RESET_SCALE_ON_GLOBAL_LAYOUT = 1.1f;
  private static final int MAX_DISPLAY_LIKE_COMMENT_NUMBER = 1000;

  private Url mUrl = null;
  private Photo mPhoto = null;
  private Mood mMood = null;
  private Like mLike = null;

  private VersionedGestureDetector mGestureDetector = null;
  private ViewFlipper mImageFlipper = null;
  private ZoomControls mImageZoomControl = null;

  private int mMaxImageViewSize = DEFAULT_MAX_IMAGE_VIEW_SIZE;

  private int mCurrentOffset = 0;
  private int mCurrentVirtualOffset = 0;
  private int mCurrnetSequenceIndex = -1;
  private int mSequenceCount = -1;
  private int mStartSequenceIndex = -1;

  private FrameLayout mOverlayTopLayout = null;
  private TextView mOverlayIndexTextView = null;
  private Button mOverlayCloseButton = null;

  private LinearLayout mOverlayBottomLayout = null;
  private LinearLayout mOverlayTitleLayout = null;
  private TextView mOverlayTitleTextView = null;
  private TextView mOverlayTextTextView = null;
  private TextView mOverlayLikeCountTextView = null;
  private TextView mOverlayCommentCountTextView = null;
  private ImageView mOverlayCommentImageView = null;
  private CheckBox mOverlayLikeCheckBox = null;

  private ProgressBar mLoadingIndicator = null;

  private String mUserId = null;
  private String mImageType = null;
  private String mAlbumId = null;
  private String mMoodId = null;
  private boolean mIsEditable = false;
  private ArrayList<ImageData> mImageDataList = null;

  private boolean mIsShownOverlay = false;
  private boolean mIsLoadingNextImages = false;
  private boolean mIsLoadingPrevImages = false;
  private Object lockObject = new Object();
 
  private OnlineImageView mLoadingNextImageView = null;
  private OnlineImageView mLoadingPrevImageView = null;
  
  private OnLikeCheckedChangeListener mOnLikeCheckedChangeListener = new OnLikeCheckedChangeListener();
  private OnCommentButtonClickListener mOnCommentClickListener = new OnCommentButtonClickListener();

  private boolean mIsShownActivity = true;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(RR.layout("gree_image_view"));

    mUrl = new Url();
    mPhoto = new Photo(mUrl);
    mMood = new Mood(mUrl);
    mLike = new Like(mUrl);

    mGestureDetector = VersionedGestureDetector.newInstance(getApplicationContext(), this);

    mLoadingIndicator = (ProgressBar) findViewById(RR.id("gree_loading_indicator"));
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR_MR1) {
      Drawable drawable = getResources().getDrawable(RR.drawable("gree_spinner"));
      mLoadingIndicator.setIndeterminateDrawable(drawable);
    } else {
      Drawable drawable = getResources().getDrawable(RR.drawable("gree_loader_progress"));
      mLoadingIndicator.setIndeterminateDrawable(drawable);
      mLoadingIndicator.setIndeterminate(true);
    }
    mLoadingIndicator.setVisibility(View.GONE);

    mImageFlipper = (ViewFlipper) findViewById(RR.id("gree_image_flipper"));
    mImageFlipper.setInAnimation(null);
    mImageFlipper.setOutAnimation(null);
    mImageFlipper.setOnTouchListener(new OnTouchListener() {
      public boolean onTouch(View v, MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return true;
      }
    });

    mImageZoomControl = (ZoomControls) findViewById(RR.id("gree_image_zoom_controlls"));
    if (mGestureDetector.isScaleGestureAvailable()) {
      mImageZoomControl.setVisibility(View.GONE);
    } else {
      mImageZoomControl.setOnZoomInClickListener(new OnClickListener() {
        public void onClick(View v) {
          OnlineImageView imageView = getCurrentImageView();
          if (imageView != null) {
            imageView.scale(DEFAULT_ZOOM_CONTROL_SCALE);
          }
        }
      });

      mImageZoomControl.setOnZoomOutClickListener(new OnClickListener() {
        public void onClick(View v) {
          OnlineImageView imageView = getCurrentImageView();
          if (imageView != null) {
            imageView.scale(1.0f / DEFAULT_ZOOM_CONTROL_SCALE);
          }
        }
      });
    }

    mOverlayTopLayout = (FrameLayout) findViewById(RR.id("gree_overlay_top_layout"));
    mOverlayTopLayout.setVisibility(View.GONE);
    mOverlayIndexTextView = (TextView) findViewById(RR.id("gree_overlay_index"));
    mOverlayCloseButton = (Button) findViewById(RR.id("gree_overlay_close"));
    mOverlayCloseButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });

    mOverlayBottomLayout = (LinearLayout) findViewById(RR.id("gree_overlay_bottom_layout"));
    mOverlayBottomLayout.setVisibility(View.GONE);
    mOverlayTitleLayout = (LinearLayout) findViewById(RR.id("gree_overlay_title_layout"));
    mOverlayTitleTextView  = (TextView) findViewById(RR.id("gree_overlay_title"));
    mOverlayTextTextView  = (TextView) findViewById(RR.id("gree_overlay_text"));

    ImageView iconLikeNum = (ImageView) findViewById(RR.id("gree_overlay_icon_like"));
    iconLikeNum.setOnClickListener(mOnCommentClickListener);
    ImageView iconCommentNum = (ImageView) findViewById(RR.id("gree_overlay_icon_comment"));
    iconCommentNum.setOnClickListener(mOnCommentClickListener);

    mOverlayLikeCountTextView = (TextView) findViewById(RR.id("gree_overlay_like_count"));
    mOverlayLikeCountTextView.setOnClickListener(mOnCommentClickListener);
    mOverlayCommentCountTextView = (TextView) findViewById(RR.id("gree_overlay_comment_count"));
    mOverlayCommentCountTextView.setOnClickListener(mOnCommentClickListener);
    mOverlayCommentImageView = (ImageView) findViewById(RR.id("gree_overlay_comment"));
    mOverlayCommentImageView.setOnClickListener(mOnCommentClickListener);

    mOverlayLikeCheckBox = (CheckBox) findViewById(RR.id("gree_overlay_like"));

    Intent intent = this.getIntent();
    if (intent != null) {
      showLoadingIndicator();

      String paramsString = intent.getStringExtra("params");
      try {
        JSONObject paramsObj = new JSONObject(paramsString);
        mImageType = paramsObj.getString("type");
        mUserId = paramsObj.getString("user_id");

        if (mImageType.equals(IMAGE_TYPE_ALBUM)) {
          mAlbumId = paramsObj.getString("album_id");
          mIsEditable = paramsObj.getBoolean("is_editable");
          mCurrnetSequenceIndex = paramsObj.getInt("sequence_index");
          mSequenceCount = paramsObj.getInt("sequence_count");
          mCurrentOffset = mCurrnetSequenceIndex;

          int requestOffset = 0;
          if (mSequenceCount <= MAX_REQUEST_LIMIT) {
            mCurrentOffset = mCurrnetSequenceIndex;
          } else {
            int halfOfRequestLimit = (MAX_REQUEST_LIMIT / 2) - 1;
            if (mCurrnetSequenceIndex - halfOfRequestLimit < 0) {
              mCurrentOffset = mCurrnetSequenceIndex;
            } else {
              requestOffset = mCurrnetSequenceIndex - halfOfRequestLimit;
              mCurrentOffset = mCurrnetSequenceIndex - requestOffset;
            }
          }

          mStartSequenceIndex = requestOffset;
          requestAlbumInfo(mUserId, mAlbumId, requestOffset, MAX_REQUEST_LIMIT);
        } else if (mImageType.equals(IMAGE_TYPE_MOOD)) {
          mMoodId = paramsObj.getString("mood_id");
          requestMoodInfo(mUserId, mMoodId);
        }
      } catch (JSONException e) {
        GLog.printStackTrace(LOG_TAG, e);
        showErrorDialogDefault();
      }
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    mIsShownActivity = true;
  }

  @Override
  protected void onPause() {
    super.onPause();
    mIsShownActivity = false;
  }

  @Override
  protected void onStop() {
    super.onStop();
    mIsShownActivity = false;
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    mIsShownActivity = false;
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);

    switch (newConfig.orientation) {
    case Configuration.ORIENTATION_PORTRAIT:
    case Configuration.ORIENTATION_LANDSCAPE:
      OnlineImageView imageView = getCurrentImageView();
      if (imageView != null) {
        imageView.resetTransform();
      }
      break;
    default:
      break;
    }
  }

  /**
   * OnClickListener for the comment button.
   */
  private class OnCommentButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
      finishAndLoadPermalink();
    }
  }

  private void finishAndLoadPermalink() {
    ImageData currentImageData = getCurrentImageData();
    JSONObject params = new JSONObject();
    try {
      if (mImageType.equals(IMAGE_TYPE_ALBUM)) {
        params.put("urn", new StringBuilder("urn:gree:photo:").append(currentImageData.mId).append('_').append(mUserId).toString());
      } else if (mImageType.equals(IMAGE_TYPE_MOOD)) {
        params.put("urn", new StringBuilder("mood:md:").append(mUserId).append('_').append(currentImageData.mId).toString());
      }

      Intent intent = new Intent();
      intent.putExtra("view", "stream_permalink");
      intent.putExtra("params", params.toString());
      setResult(RESULT_OK, intent);
      finish();
    } catch (JSONException e) {
      GLog.printStackTrace(LOG_TAG, e);
    }
  }

  private void showLoadingIndicator() {
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR_MR1) {
      Animation rotation = AnimationUtils.loadAnimation(this, RR.anim("gree_rotate"));
      rotation.setRepeatCount(Animation.INFINITE);
      mLoadingIndicator.startAnimation(rotation);
    }
    mLoadingIndicator.setVisibility(View.VISIBLE);
  }

  private void hideLoadingIndicator() {
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR_MR1) {
      mLoadingIndicator.clearAnimation();
    }
    mLoadingIndicator.setVisibility(View.GONE);
  }

  private ImageData getCurrentImageData() {
    return mImageDataList.get(mCurrentOffset);
  }

  private void resetOverlayInfo() {
    ImageData currentImageData = getCurrentImageData();

    if (mImageType.equals(IMAGE_TYPE_ALBUM)) {
      mOverlayIndexTextView.setText(currentImageData.mIndex + " / " + mSequenceCount);
    } else {
      mOverlayIndexTextView.setText(null);
    }

    mOverlayTitleLayout.setVisibility(View.GONE);

    setOverlayLikesCommentsLabel();

    setLikeChecked(currentImageData.mIsLiked);
    setLikeEnabled();
  }

  private void setOverlayLikesCommentsLabel() {
    ImageData currentImageData = getCurrentImageData();

    if (currentImageData.mLikeNum >= MAX_DISPLAY_LIKE_COMMENT_NUMBER) {
      mOverlayLikeCountTextView.setText("999+");
    } else if (currentImageData.mLikeNum > 0) {
      mOverlayLikeCountTextView.setText(currentImageData.mLikeNum + " ");
    } else {
      mOverlayLikeCountTextView.setText(null);
    }

    if (currentImageData.mCommentNum >= MAX_DISPLAY_LIKE_COMMENT_NUMBER) {
      mOverlayCommentCountTextView.setText("999+");
    } else if (currentImageData.mCommentNum > 0) {
      mOverlayCommentCountTextView.setText(currentImageData.mCommentNum + " ");
    } else {
      mOverlayCommentCountTextView.setText(null);
    }
  }

  private void setLikeChecked(boolean isChecked) {
    mOverlayLikeCheckBox.setOnCheckedChangeListener(null);
    mOverlayLikeCheckBox.setChecked(isChecked);
    mOverlayLikeCheckBox.setOnCheckedChangeListener(mOnLikeCheckedChangeListener);
  }

  private void setLikeEnabled() {
    ImageData currentImageData = getCurrentImageData();
    mOverlayLikeCheckBox.setEnabled(!currentImageData.mIsRequestingLikes);
  }

  private boolean isShowDummyDisplayedChild() {
    return mCurrentOffset < 0 || mCurrentOffset >= mImageDataList.size();
  }

  private void showOverlayImageInfo() {
    if (mImageDataList == null || isShowDummyDisplayedChild()) {
      return;
    }

    if (isShownError()) {
      return;
    }

    resetOverlayInfo();
    mOverlayTopLayout.setVisibility(View.VISIBLE);
    mOverlayBottomLayout.setVisibility(View.VISIBLE);
    mIsShownOverlay = true;

    if (!mGestureDetector.isScaleGestureAvailable()) {
      mImageZoomControl.setVisibility(View.GONE);
    }
  }

  private void hideOverlayImageInfo() {
    mOverlayTopLayout.setVisibility(View.GONE);
    mOverlayBottomLayout.setVisibility(View.GONE);

    mOverlayIndexTextView.setText(null);
    mOverlayTitleTextView.setText(null);
    mOverlayTextTextView.setText(null);
    mOverlayLikeCountTextView.setText(null);
    mOverlayCommentCountTextView.setText(null);
    mIsShownOverlay = false;

    if (!mGestureDetector.isScaleGestureAvailable()) {
      mImageZoomControl.setVisibility(View.VISIBLE);
    }
  }

  private void showErrorDialog(int messageResourceId) {
    if (!mIsShownActivity) {
      return;
    }

    AlertDialog.Builder builder = new AlertDialog.Builder(ImageViewActivity.this);
    builder.setMessage(messageResourceId);
    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        ImageViewActivity.this.finish();
      }
    });
    builder.create().show();
  }

  private void showErrorDialogDefault() {
    showErrorDialog(RR.string("gree_image_view_failed_to_download_the_photo"));
  }

  private void requestAlbumInfo(String userId, String albumId, int offset, int limit) {
    mPhoto.getAlbumInfo(userId, albumId, offset, limit, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        loadAlbumInfo(response);
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        showErrorDialogDefault();
      }
    });
  }

  private void requestMoodInfo(String userId, String moodId) {
    mMood.getInfo(userId, moodId, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        loadMoodInfo(response);
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        showErrorDialogDefault();
      }
    });
  }

  private void requestLikePhoto(final ImageData imageData) {
    imageData.mIsRequestingLikes = true;

    mLike.likePhoto(mUserId, imageData.mId, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsLiked = true;
        imageData.mLikeNum++;
        imageData.mIsRequestingLikes = false;
        setOverlayLikesCommentsLabel();
        setLikeEnabled();
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsRequestingLikes = false;
        setLikeChecked(false);
        setLikeEnabled();

        Toast.makeText(ImageViewActivity.this, RR.string("gree_image_view_failed_to_like_the_photo"), Toast.LENGTH_SHORT).show();
      }
    });
  }

  private void requestUnlikePhoto(final ImageData imageData) {
    imageData.mIsRequestingLikes = true;

    mLike.unlikePhoto(mUserId, imageData.mId, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsLiked = false;
        imageData.mLikeNum--;
        imageData.mIsRequestingLikes = false;
        setOverlayLikesCommentsLabel();
        setLikeEnabled();
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsRequestingLikes = false;
        setLikeChecked(true);
        setLikeEnabled();

        Toast.makeText(ImageViewActivity.this, RR.string("gree_image_view_failed_to_unlike_the_photo"), Toast.LENGTH_SHORT).show();
      }
    });
  }

  private void requestLikeMood(final ImageData imageData) {
    imageData.mIsRequestingLikes = true;

    mLike.likeMood(mUserId, mMoodId, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsLiked = true;
        imageData.mLikeNum++;
        imageData.mIsRequestingLikes = false;
        setOverlayLikesCommentsLabel();
        setLikeEnabled();
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsRequestingLikes = false;
        setLikeChecked(false);
        setLikeEnabled();

        Toast.makeText(ImageViewActivity.this, RR.string("gree_image_view_failed_to_like_the_photo"), Toast.LENGTH_SHORT).show();
      }
    });
  }
  
  private void requestUnlikeMood(final ImageData imageData) {
    imageData.mIsRequestingLikes = true;

    mLike.unlikeMood(mUserId, mMoodId, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsLiked = false;
        imageData.mLikeNum--;
        imageData.mIsRequestingLikes = false;
        setOverlayLikesCommentsLabel();
        setLikeEnabled();
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        imageData.mIsRequestingLikes = false;
        setLikeChecked(true);
        setLikeEnabled();

        Toast.makeText(ImageViewActivity.this, RR.string("gree_image_view_failed_to_unlike_the_photo"), Toast.LENGTH_SHORT).show();
      }
    });
  }

  /**
   * OnCheckedChangeListener for the like checkbox.
   */
  private class OnLikeCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      mOverlayLikeCheckBox.setEnabled(false);      
      final ImageData currentImageData = getCurrentImageData();

      if (isChecked) {
        // like
        if (mImageType.equals(IMAGE_TYPE_ALBUM)) {
          requestLikePhoto(currentImageData);
        } else if (mImageType.equals(IMAGE_TYPE_MOOD)) {
          requestLikeMood(currentImageData);
        }
      } else {
        // unlike
        if (mImageType.equals(IMAGE_TYPE_ALBUM)) {
          requestUnlikePhoto(currentImageData);
        } else if (mImageType.equals(IMAGE_TYPE_MOOD)) {
          requestUnlikeMood(currentImageData);
        }
      }
    }
  }

  /**
   * This class hold the image data which is returned by Connect API.
   */
  private class ImageData {
    int mIndex = 0;
    String mId = null;
    String mUrl = null;
    String mTitle = null;
    String mText = null;
    int mLikeNum = 0;
    int mCommentNum = 0;
    boolean mIsLiked = false;
    boolean mIsRequestingLikes = false;

    /**
     * Create the ImageData.
     * @param index index in the album.
     * @param id photo id.
     * @param url image url.
     * @param title title of the image.
     * @param text text about the image.
     * @param likeNum number of like.
     * @param commentNum number of comment.
     * @param isLiked true: true if you liked, else false.
     */
    ImageData(int index, String id, String url, String title, String text, int likeNum, int commentNum, boolean isLiked) {
      mIndex = index;
      mId = id;
      mUrl = url;
      mTitle = title;
      mText = text;
      mLikeNum = likeNum;
      mCommentNum = commentNum;
      mIsLiked = isLiked;
    }

    ImageData(int index, String id, String url, String text, int likeNum, int commentNum, boolean isLiked) {
      this(index, id, url, null, text, likeNum, commentNum, isLiked);
    }
  }

  private ImageData convertToImageData(int index, JSONObject image) throws JSONException {
    JSONObject urls = image.getJSONObject("photo_url");
    String id = image.getString("id");
    String url = urls.getString("640");
    String title = image.getString("title");
    String text = image.getString("text");
    int likeNum = image.getInt("like_num");
    int commentNum = image.getInt("comment_num");
    boolean isLiked = image.optInt("is_like_user", 0) == 1;

    return new ImageData(index, id, url, title, text, likeNum, commentNum, isLiked);
  }

  private void loadAlbumInfo(String response) {
    try {
      JSONObject root = new JSONObject(response);
      JSONArray images = root.getJSONArray("entry");
      
      mImageDataList = new ArrayList<ImageData>();
      for (int i = 0; i < images.length(); i++) {
        JSONObject image = images.getJSONObject(i);
        mImageDataList.add(convertToImageData(mStartSequenceIndex + i + 1, image));
      }

      mMaxImageViewSize = Math.min(images.length(), DEFAULT_MAX_IMAGE_VIEW_SIZE);
      for (int i = 0; i < mMaxImageViewSize; i++) {
        addImageView(i);
      }

      hideLoadingIndicator();

      mCurrentVirtualOffset = 0;
      setCurrentVirtualOffset(mCurrentVirtualOffset);

      mImageFlipper.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
        public void onGlobalLayout() {
          OnlineImageView imageView = getCurrentImageView();
          if (imageView.getCurrentScale() <= RESET_SCALE_ON_GLOBAL_LAYOUT) {
            imageView.resetTransform();
          }
        }
      });
    } catch (JSONException e) {
      GLog.printStackTrace(LOG_TAG, e);
      showErrorDialogDefault();
    }
  }

  private void loadMoodInfo(String response) {
    mCurrentOffset = 0;
    mCurrentVirtualOffset = 0;

    try {
      JSONObject root = new JSONObject(response);
      JSONObject entry = root.getJSONObject("entry");
      JSONArray attachImages = entry.getJSONArray("attach_image_list");

      if (attachImages != null && attachImages.length() > 0) {
        mImageDataList = new ArrayList<ImageData>(attachImages.length());
        String id = entry.getString("id");
        String text = entry.getString("text");
        int likeNum = entry.getInt("like_num");
        int commentNum = entry.getInt("comment_num");
        boolean isLiked = entry.optInt("is_like_user", 0) == 1;

        for (int i = 0; i < attachImages.length(); i++) {
          JSONObject image = attachImages.getJSONObject(i);
          String url = image.getString("640");
          mImageDataList.add(new ImageData(i + 1, id, url, text, likeNum, commentNum, isLiked));
          addImageView(i);
        }
      }

      hideLoadingIndicator();

      setCurrentOffset(mCurrentOffset);
      mImageFlipper.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
        public void onGlobalLayout() {
          OnlineImageView imageView = getCurrentImageView();
          if (imageView.getCurrentScale() <= RESET_SCALE_ON_GLOBAL_LAYOUT) {
            imageView.resetTransform();
          }
        }
      });
    } catch (JSONException e) {
      GLog.printStackTrace(LOG_TAG, e);
      showErrorDialogDefault();
    }
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    menu.clear();

    if (mImageType.equals(IMAGE_TYPE_ALBUM)) {
      if (mIsEditable) {
        menu.add(Menu.NONE, OPTION_MENU_INDEX_EDIT, Menu.NONE, RR.string("gree_image_view_menu_edit")).setIcon(RR.drawable("gree_ic_menu_edit"));
      }
      menu.add(Menu.NONE, OPTION_MENU_INDEX_SAVE, Menu.NONE, RR.string("gree_image_view_menu_save")).setIcon(RR.drawable("gree_ic_menu_save"));
    } else if (mImageType.equals(IMAGE_TYPE_MOOD)) {
      menu.add(Menu.NONE, OPTION_MENU_INDEX_SAVE, Menu.NONE, RR.string("gree_image_view_menu_save")).setIcon(RR.drawable("gree_ic_menu_save"));
    }
    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    switch (itemId) {
    case OPTION_MENU_INDEX_SAVE:
      saveCurrentImage();
      return true;
    case OPTION_MENU_INDEX_EDIT:
      ImageData currentImageData = getCurrentImageData();
      try {
        JSONObject params = new JSONObject();
        params.put("owner_id", mUserId);
        params.put("photo_id", currentImageData.mId);

        Intent intent = new Intent();
        intent.putExtra("view", "photoalbum_editphoto");
        intent.putExtra("params", params.toString());
        setResult(RESULT_OK, intent);
        finish();
      } catch (JSONException e) {
        GLog.printStackTrace(LOG_TAG, e);
      }
      return true;
    default:
      break;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onSingleTapUp(MotionEvent e)  {
    if (!mIsShownOverlay) {
      showOverlayImageInfo();
    } else {
      hideOverlayImageInfo();
    }

    return false;
  }

  @Override
  public void onScaleBegin(float scaleFactor, float focusX, float focusY) {
  }

  @Override
  public void onScale(float scaleFactor, float focusX, float focusY) {
    OnlineImageView imageView = getCurrentImageView();
    if (imageView != null) {
      imageView.scale(scaleFactor);
    }
  }

  @Override
  public void onScaleEnd(float scaleFactor, float focusX, float focusY) {
  }

  @Override
  public boolean onScroll(float distanceX, float distanceY) {
    OnlineImageView imageView = getCurrentImageView();
    if (imageView != null) {
      imageView.translate(-distanceX, -distanceY);
    }
    return true;
  }

  @Override
  public boolean onFling(float velocityX, float velocityY) {
    if (mImageType.equals(IMAGE_TYPE_MOOD)) {
      return false;
    }

    if (Math.abs(velocityY) > Math.abs(velocityX)) {
      return false;
    }

    if (velocityX < 0) {
      setCurrentOffset(true);
    } else if (velocityX > 0) {
      setCurrentOffset(false);
    }

    return true;
  }

  private OnlineImageView addImageView(int index) {
    OnlineImageView imageView = new OnlineImageView(this);
    mImageFlipper.addView(imageView, index, new ViewFlipper.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    return imageView;
  }

  private int getOrientation() {
    return getResources().getConfiguration().orientation;
  }

  private void showNextImageView() {
    OnlineImageView imageView = getCurrentImageView();
    if (imageView == null) {
      return;
    }

    switch (getOrientation()) {
    case Configuration.ORIENTATION_PORTRAIT:
      if (!imageView.isFitRight() || mCurrentVirtualOffset > mSequenceCount - 1) {
        return;
      }
      break;
    case Configuration.ORIENTATION_LANDSCAPE:
      if ((!imageView.isFitRight() && !imageView.isOutOfDisplayHeight()) || mCurrentVirtualOffset > mSequenceCount - 1) {
        return;
      }
      break;
    default:
      break;
    }

    int currentHoldingImageDataSize = mImageDataList.size();
    if (mCurrentOffset + 1 >= currentHoldingImageDataSize) {
      ImageData lastImageData = mImageDataList.get(mImageDataList.size() - 1);

      if (mSequenceCount > lastImageData.mIndex) {
        if (mIsLoadingNextImages && mCurrentOffset == currentHoldingImageDataSize) {
          return;
        } else {
          new ImageDataRequestHandler(ImageDataRequestHandler.DIRECTION_NEXT, lastImageData.mIndex, MAX_REQUEST_LIMIT).sendEmptyMessage(0);
        }

        hideOverlayImageInfo();
        setNextDummyDisplayedChild();

        mLoadingNextImageView = getCurrentImageView();
        mLoadingNextImageView.clearImage();
        mLoadingNextImageView.showLoadingIndicator();
        return;
      } else {
        // already reached the last photo.
        return;
      }
    }

    moveNext();
    mLoadingPrevImageView = null;
  }

  private void showPrevImageView() {
    OnlineImageView imageView = getCurrentImageView();

    switch (getOrientation()) {
    case Configuration.ORIENTATION_PORTRAIT:
      if (!imageView.isFitLeft()) {
        return;
      }
      break;
    case Configuration.ORIENTATION_LANDSCAPE:
      if (!imageView.isFitLeft() && !imageView.isOutOfDisplayHeight()) {
        return;
      }
      break;
    default:
      break;
    }

    if (mCurrentOffset <= 0) {
      if (mStartSequenceIndex == 0) {
        return;
      }

      if (mIsLoadingPrevImages) {
        if (mCurrentOffset == -1) {
          return;
        }
      } else {
        if (mStartSequenceIndex - 1 <= MAX_REQUEST_LIMIT) {
          new ImageDataRequestHandler(ImageDataRequestHandler.DIRECTION_PREV, 0, mStartSequenceIndex).sendEmptyMessage(0);
        } else {
          new ImageDataRequestHandler(ImageDataRequestHandler.DIRECTION_PREV, mStartSequenceIndex - MAX_REQUEST_LIMIT, MAX_REQUEST_LIMIT).sendEmptyMessage(0);
        }
      }

      hideOverlayImageInfo();
      setPrevDummyDisplayedChild();

      mLoadingPrevImageView = getCurrentImageView();
      mLoadingPrevImageView.clearImage();
      mLoadingPrevImageView.showLoadingIndicator();
      return;
    }

    movePrev();
    mLoadingNextImageView = null;
  }

  private void setNextAnimation() {
    mImageFlipper.setInAnimation(this, RR.anim("gree_image_view_slide_in_right"));
    mImageFlipper.setOutAnimation(this, RR.anim("gree_image_view_slide_out_left"));
  }

  private void setPrevAnimation() {
    mImageFlipper.setInAnimation(this, RR.anim("gree_image_view_slide_in_left"));
    mImageFlipper.setOutAnimation(this, RR.anim("gree_image_view_slide_out_right"));
  }

  private void setNextVirtualOffset() {
    if (mCurrentVirtualOffset + 1 >= mMaxImageViewSize) {
      mCurrentVirtualOffset = 0;
    } else {
      mCurrentVirtualOffset++;
    }
  }

  private void setPrevVirtualOffset() {
    if (mCurrentVirtualOffset == 0) {
      mCurrentVirtualOffset = mMaxImageViewSize - 1;
    } else {
      mCurrentVirtualOffset--;
    }
  }

  private void setNextDummyDisplayedChild() {
    mCurrentOffset++;
    setNextAnimation();
    setNextVirtualOffset();
    mImageFlipper.setDisplayedChild(mCurrentVirtualOffset);
  }

  private void setPrevDummyDisplayedChild() {
    mCurrentOffset--;
    setPrevAnimation();
    setPrevVirtualOffset();
    mImageFlipper.setDisplayedChild(mCurrentVirtualOffset);
  }

  private void moveNext() {
    mCurrentOffset++;
    setNextAnimation();
    setNextVirtualOffset();
    setCurrentVirtualOffset(mCurrentVirtualOffset);

    mCurrnetSequenceIndex++;
    resetOverlayInfo();
  }

  private void movePrev() {
    mCurrentOffset--;
    setPrevAnimation();
    setPrevVirtualOffset();
    setCurrentVirtualOffset(mCurrentVirtualOffset);

    mCurrnetSequenceIndex--;
    resetOverlayInfo();
  }
  
  private boolean isShownError() {
    OnlineImageView currentImageView = getCurrentImageView();
    if (currentImageView != null) {
      return currentImageView.isShownError();
    }
    
    return false;
  }

  private void setCurrentOffset(boolean isNext) {
    synchronized (lockObject) {
      if (isNext) {
        showNextImageView();
      } else {
        showPrevImageView();
      }
    }
  }

  private void setCurrentOffset(int offset) {
    mImageFlipper.setDisplayedChild(offset);
    mCurrentOffset = offset;
    loadCurrentImage();
  }

  private void setCurrentVirtualOffset(int offset) {
    mImageFlipper.setDisplayedChild(offset);
    loadCurrentImage();
  }

  private void loadCurrentImage() {
    OnlineImageView imageView = getCurrentImageView();
    ImageData currentImageData = getCurrentImageData();
    imageView.load(currentImageData.mUrl);
  }

  private void saveCurrentImage() {
    OnlineImageView imageView = getCurrentImageView();
    imageView.save();
  }

  private OnlineImageView getCurrentImageView() {
    return (OnlineImageView) mImageFlipper.getChildAt(mCurrentVirtualOffset);
  }

  /**
   * Handler for requesting the image. 
   */
  private class ImageDataRequestHandler extends Handler {
    public static final int DIRECTION_NEXT = 0;
    public static final int DIRECTION_PREV = 1;

    private int mDirection = -1;
    private int mOffset = 0;
    private int mLimit = MAX_REQUEST_LIMIT;

    public ImageDataRequestHandler(int direction, int offset, int limit) {
      mDirection = direction;
      mOffset = offset;
      mLimit = limit;
    }

    @Override
    public void handleMessage(Message message) {
      switch (mDirection) {
      case DIRECTION_NEXT:
        if (mIsLoadingNextImages) {
          return;
        }

        mIsLoadingNextImages = true;
        break;
      case DIRECTION_PREV:
        if (mIsLoadingPrevImages) {
          return;
        }

        mIsLoadingPrevImages = true;
        break;
      default:
        break;
      }

      mPhoto.getAlbumInfo(mUserId, mAlbumId, mOffset, mLimit, new OnResponseCallback<String>() {
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, String response) {
          synchronized (lockObject) {
            try {
              switch (mDirection) {
              case DIRECTION_NEXT:
                addImageData(mOffset, response);
                mIsLoadingNextImages = false;
                
                if (mLoadingNextImageView != null) {
                  ImageData currentImageData = getCurrentImageData();
                  mLoadingNextImageView.load(currentImageData.mUrl);
                  mLoadingNextImageView = null;
                }
                break;
              case DIRECTION_PREV:
                insertImageData(mOffset, response);
                mIsLoadingPrevImages = false;

                if (mLoadingPrevImageView != null) {
                  ImageData currentImageData = getCurrentImageData();
                  mLoadingPrevImageView.load(currentImageData.mUrl);
                  mLoadingPrevImageView = null;
                }
                break;
              default:
                break;
              }
            } catch (JSONException e) {
              GLog.printStackTrace(LOG_TAG, e);
              showRequestFailedDialog();
            }
          }
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          synchronized (lockObject) {
            showRequestFailedDialog();
          }
        }
      });
    }

    private void showRequestFailedDialog() {
      switch (mDirection) {
      case DIRECTION_NEXT:
        mIsLoadingNextImages = false;

        if (mLoadingNextImageView != null) {
          mLoadingNextImageView.hideViews();
          movePrev();
          mLoadingNextImageView = null;
        }
        break;
      case DIRECTION_PREV:
        mIsLoadingPrevImages = false;

        if (mLoadingPrevImageView != null) {
          mLoadingPrevImageView.hideViews();
          moveNext();
          mLoadingPrevImageView = null;
        }
        break;
      default:
        break;
      }

      AlertDialog.Builder builder = new AlertDialog.Builder(ImageViewActivity.this);
      builder.setMessage(RR.string("gree_image_view_failed_to_download_the_photo"));
      builder.setPositiveButton(android.R.string.ok, null);
      builder.create().show();
    }
  }

  private void addImageData(int offset, String response) throws JSONException {
    ImageData lastImageData = mImageDataList.get(mImageDataList.size() - 1);
    JSONObject root = new JSONObject(response);
    JSONArray images = root.getJSONArray("entry");

    int index = offset + 1;
    if (index <= lastImageData.mIndex) {
      return;
    }
    for (int i = 0; i < images.length(); i++) {
      JSONObject image = images.getJSONObject(i);
      mImageDataList.add(convertToImageData(index, image));
      index++;
    }
  }

  private void insertImageData(int offset, String response) throws JSONException {
    JSONObject root = new JSONObject(response);
    JSONArray images = root.getJSONArray("entry");

    int index = mStartSequenceIndex;
    for (int i = images.length() - 1; i >= 0; i--) {
      JSONObject image = images.getJSONObject(i);
      mImageDataList.add(0, convertToImageData(index, image));
      index--;
    }

    mStartSequenceIndex = index;
    mCurrentOffset = mCurrentOffset + images.length();
  }

}
