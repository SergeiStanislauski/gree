/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.gree.asdk.core.notifications.ui;

import net.gree.asdk.core.GLog;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

/**
 * A column data of notifications
 */
public  class NotificationData implements Comparable<NotificationData>{
  private final static String TAG = NotificationData.class.getName();
  
  public Bitmap image;
  public JSONObject json;
  public int cellType;
  public String text;
  public int offset = NotificationAdapterBase.LOAD_LIMIT;
  public String date;

  public NotificationData(){
    cellType = NotificationAdapterBase.CELL_TYPE_ERROR;
  }

  @Override
  public int compareTo(NotificationData another) {
    if (json == null || another.json == null) { return 0; }

    try {
      return  - ( json.getString("date").compareTo(another.json.getString("date")) );
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }

    return 0;
  }
}
