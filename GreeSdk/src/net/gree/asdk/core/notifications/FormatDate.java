/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.gree.asdk.core.notifications;

import java.util.Calendar;

import net.gree.asdk.core.RR;
import android.content.res.Resources;

/**
 * Format date for notification display
 */
public class FormatDate {
  private final static long MINUTE = 60 * 1000;
  private final static long HOUR = 60 * MINUTE;
  private final static long A_DAY = 24 * HOUR;
  private final static long TWO_DAY = 2 * A_DAY;
  private final static long THREE_DAY = 3 * A_DAY;
  
  private String mResult;

  public FormatDate(Resources r, Calendar date) {
    Calendar now = Calendar.getInstance();
    long dt = now.getTimeInMillis() - date.getTimeInMillis();
    String text = null;

    if (dt < MINUTE) {
      text = r.getString(RR.string("gree_notification_just_now"));
    } else if (dt < HOUR) {
      text  = String.format(r.getString(RR.string("gree_notification_minutes_ago")), (dt / MINUTE));
    } else if (dt < A_DAY) {
      text = String.format(r.getString(RR.string("gree_notification_hours_ago")), (dt / HOUR));
    } else if (dt < TWO_DAY) {
      int minute = date.get(Calendar.MINUTE);
      text = String.format(r.getString(RR.string("gree_notification_yesterday")), 
        date.get(Calendar.HOUR_OF_DAY), "" + (minute / 10) + (minute % 10));
    } else if (dt < THREE_DAY) {
      String dayOfWeek = null;
      switch(date.get(Calendar.DAY_OF_WEEK)) {
        case Calendar.SUNDAY:
          dayOfWeek = r.getString(RR.string("gree_sunday"));
          break;
        case Calendar.MONDAY:
          dayOfWeek = r.getString(RR.string("gree_monday"));
          break;
        case Calendar.TUESDAY:
          dayOfWeek = r.getString(RR.string("gree_tuesday"));
          break;
        case Calendar.WEDNESDAY:
          dayOfWeek = r.getString(RR.string("gree_wednesday"));
          break;
        case Calendar.THURSDAY:
          dayOfWeek = r.getString(RR.string("gree_thursday"));
          break;
        case Calendar.FRIDAY:
          dayOfWeek = r.getString(RR.string("gree_friday"));
          break;
        case Calendar.SATURDAY:
          dayOfWeek = r.getString(RR.string("gree_saturday"));
          break;
      }
      
      int minute = date.get(Calendar.MINUTE);
      text  = String.format(r.getString(RR.string("gree_notification_day_of_week")),  
        dayOfWeek, date.get(Calendar.HOUR_OF_DAY), "" + (minute / 10) + (minute % 10));
    } else {
      String month = null;
      switch(date.get(Calendar.MONTH)) {
        case Calendar.JANUARY:
          month = r.getString(RR.string("gree_january"));
          break;
        case Calendar.FEBRUARY:
          month = r.getString(RR.string("gree_february"));
          break;
        case Calendar.MARCH:
          month = r.getString(RR.string("gree_march"));
          break;
        case Calendar.APRIL:
          month = r.getString(RR.string("gree_april"));
          break;
        case Calendar.MAY:
          month = r.getString(RR.string("gree_may"));
          break;
        case Calendar.JUNE:
          month = r.getString(RR.string("gree_june"));
          break;
        case Calendar.JULY:
          month = r.getString(RR.string("gree_july"));
          break;
        case Calendar.AUGUST:
          month = r.getString(RR.string("gree_august"));
          break;
        case Calendar.NOVEMBER:
          month = r.getString(RR.string("gree_november"));
          break;
        case Calendar.OCTOBER:
          month = r.getString(RR.string("gree_october"));
          break;
        case Calendar.SEPTEMBER:
          month = r.getString(RR.string("gree_september"));
          break;
        case Calendar.DECEMBER:
          month = r.getString(RR.string("gree_december"));
          break;
      }

      if (now.get(Calendar.YEAR) == date.get(Calendar.YEAR)) {
        text = String.format(r.getString(RR.string("gree_notification_date")), month, date.get(Calendar.DAY_OF_MONTH));
      } else {
        text = String.format(r.getString(RR.string("gree_notification_year_date")), 
          month, date.get(Calendar.DAY_OF_MONTH), date.get(Calendar.YEAR));
      }
    }

    mResult = text;
  }
  
  public String getResult() {
    return mResult;
  }
}
