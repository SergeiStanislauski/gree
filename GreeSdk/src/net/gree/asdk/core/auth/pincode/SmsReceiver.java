package net.gree.asdk.core.auth.pincode;

import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.util.CoreData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * This class is used to check incoming SMS,
 * if it comes from Gree upgrade flow, it will have a pin code to validate the phone number he inputed,
 * making him a grade 3 user.
 */
public class SmsReceiver extends BroadcastReceiver implements ISmsReceiver {
  static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";

  private SmsListener mListener;
  private IntentFilter mIntentFilter;

  /**
   * Interface for listening to pincode received by SMS
   */
  public interface SmsListener {
    public void onReceived(String pincode);
  }

  public SmsReceiver(SmsListener listener) {
    super();
    mListener = listener;
  }

  /* (non-Javadoc)
   * @see net.gree.asdk.core.auth.ISmsReceiver#setListener(net.gree.asdk.core.auth.SmsReceiver.SmsListener)
   */
  @Override
  public void setListener(SmsListener listener) {
    mListener = listener;
  }

  /* (non-Javadoc)
   * @see net.gree.asdk.core.auth.ISmsReceiver#getIntentFilter()
   */
  @Override
  public IntentFilter getIntentFilter() {
    if (mIntentFilter == null) {
      mIntentFilter = new IntentFilter();
      mIntentFilter.addAction(SMS_RECEIVED_ACTION);
    }
    return mIntentFilter;
  }

  /* (non-Javadoc)
   * @see net.gree.asdk.core.auth.ISmsReceiver#onReceive(android.content.Context, android.content.Intent)
   */
  @Override
  public void onReceive(Context context, Intent intent) {
    if (SMS_RECEIVED_ACTION.equals(intent.getAction())) {
      Bundle extras = intent.getExtras();
      if (extras != null) {
        Object[] pdus = (Object[]) extras.get("pdus");
        for (Object pdu : pdus) {
          SmsMessage message = SmsMessage.createFromPdu((byte[]) pdu);
          String address = message.getOriginatingAddress();
          if (isGreeAddress(context, address)) {
            String pinCode = extractPinCode(context, message);
            if (pinCode != null && mListener != null) {
              mListener.onReceived(pinCode);
            }
          }
        }
      }
    }
  }

  private String extractPinCode(Context context, SmsMessage message) {
    String code = null;
    if (message != null) {
      String body = message.getMessageBody();
      String prefix = getSmsBodyPrefix(context, body);
      if (prefix != null) {
        int startIndex = prefix.length();
        int endIndex = startIndex + "XXXX".length();
        if (endIndex <= body.length()) {
          code = body.substring(startIndex, endIndex);
        }
      }
      else {
        code = body.substring(0, "XXXX".length());
      }
      // check code is numeric
      if (code != null && !code.matches("\\d*")) { code = null; }
    }
    return code;
  }

  private String getSmsBodyPrefix(Context context, String body) {
    if (body != null) {
      String[] filters = context.getResources().getStringArray(RR.array("gree_sms_filter_array"));
      for (String filter : filters) {
        if (body.startsWith(filter)) {
          return filter;
        }
      }
    }
    return null;
  }

  private boolean isGreeAddress(Context context, String address) {
    String[] ids = getSenderIds(context);
    if (ids != null) {
      for (String id : ids) {
        if (id.equals(address)) {
          return true;
        }
      }
    }
    return false;
  }

  private String[] getSenderIds(Context context) {
    String[] ids = null;
    String smsSenderIds = CoreData.get(InternalSettings.SmsSenderIds);
    if (smsSenderIds != null) {
      ids = smsSenderIds.replace("\"","").replace("[", "").replace("]","").split(",");
    }
    else {
      ids = context.getResources().getStringArray(RR.array("gree_sms_sender_id_array"));
    }
    return ids;
  }
}
