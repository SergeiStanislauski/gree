/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.wallet.Balance;
import net.gree.asdk.api.wallet.Balance.BalanceListener;

import org.apache.http.HeaderIterator;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentSelectActivity extends BaseActivity {
  private static final String TAG = PaymentSelectActivity.class.getSimpleName();

  private LinearLayout mSampleStore, mCustomPurchase;
  private TextView mBalanceText;
  private ProgressBar mProgressBar;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Standard GGP calls and call to load the UI
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.payment_select);

    // initalize the UI
    initUI();
    // sets the clickListeners
    setClickListeners();
  }

  private void initUI() {
    mSampleStore = (LinearLayout) findViewById(R.id.payment_select_ll_wrapper_top);
    mCustomPurchase = (LinearLayout) findViewById(R.id.payment_select_ll_wrapper_bot);
    mBalanceText = (TextView) findViewById(R.id.payment_select_balance_txt);
    mProgressBar = (ProgressBar) findViewById(R.id.payment_select_loading_indicator);
  }

  private void setClickListeners() {
    mSampleStore.setOnClickListener(PurchaseClickListener);
    mCustomPurchase.setOnClickListener(PurchaseClickListener);
  }

  private OnClickListener PurchaseClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      Intent intent;
      if (v.getId() == R.id.payment_select_ll_wrapper_top) {
		intent = new Intent(PaymentSelectActivity.this, SampleStoreActivity.class);
		startActivity(intent);
	} else if (v.getId() == R.id.payment_select_ll_wrapper_bot) {
		intent = new Intent(PaymentSelectActivity.this, CustomPaymentActivity.class);
		startActivity(intent);
	}
    }
  };

  @Override
  public void onOpenHelp(View v){
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.payment_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpBackButton();
    setUpAutoLoadMore();
    updateBalance();
  }

  @Override
  protected void sync(boolean fromStart) {}

  private void updateBalance() {
    switchLoadingIndicator(true);
    new Balance().load(new BalanceListener() {
      public void onSuccess(long balance) {
        switchLoadingIndicator(false);
        mBalanceText.setText(""+balance);
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        switchLoadingIndicator(false);
        Log.d(TAG, "loading balance failure:" + responseCode + ":" + response);
        Toast.makeText(PaymentSelectActivity.this, "loading balance fail.", Toast.LENGTH_SHORT)
            .show();
      }
    });
  }

  private void switchLoadingIndicator(boolean visible) {
    if (visible) {
      mProgressBar.setVisibility(View.VISIBLE);
      mBalanceText.setVisibility(View.INVISIBLE);
    }
    else {
      mProgressBar.setVisibility(View.INVISIBLE);
      mBalanceText.setVisibility(View.VISIBLE);
    }
  }
}
