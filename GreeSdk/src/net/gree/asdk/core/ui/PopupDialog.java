/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.ui;

import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.ui.ConfigChangeListeningLayout.OnConfigurationChangedListener;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * A GREE GGP style pop-up dialog. This class provides basic functionalities that resize the dialog
 * on configuration change and close itself with its X button pressed. This takes View object as its
 * content.
 * 
 * NOTE: This dialog ignores events from the search button of the device.
 */
public class PopupDialog extends Dialog {
  private static final int THEME = RR.style("Theme.GreePopupDialog");
  @SuppressWarnings("unused")
  private static final String TAG = "PopupDialog";

  public static final int TITLE_TYPE_WEBPAGE_HEADER     = 0;
  public static final int TITLE_TYPE_LOGO               = 1;
  public static final int TITLE_TYPE_STRING             = 2;

  private static final int SIZE_TYPE_PROPOTION  = 1;
  private static final int SIZE_TYPE_PIXELS     = 2;
  private static final int SIZE_TYPE_AUTO = 3;

  private static final int NUM_TITLE_STRING_MAX_LENGTH  = 15;

  private static final int SIZE_TITLEBAR_PORTRAIT       = 40;
  private static final int SIZE_TITLEBAR_LANDSCAPE      = 32;

  private static final int FONTSIZE_TITLE_PORTRAIT      = 20;
  private static final int FONTSIZE_TITLE_LANDSCAPE     = 16;

  private static final int DISPLAY_DP_THRESHOLD = 360;
  private static final int WIDTH_MAX_DP = 360;
  private static final int HEIGHT_MAX_DP = 640;
  private static final int MARGIN_DP = 10;
  private int mStatusbarHeight;

  private int mTitleType;

  private TextView mTextView;
  private ImageView mImageView;
  private Button mDismissButton;

  private int mSizeType;
  private boolean mFitContent = false;
  private double mSizeWidthPropotion;
  private double mSizeHeightPropotion;
  private float mSizeWidthPixels;
  private float mSizeHeightPixels;

  /* Resource ID of the View that decides the height when mFixContent is true */
  private int mContentViewResId;

  private View mViewToLoad;
  private ConfigChangeListeningLayout mContentLayout;
  private View mContentView;

  protected PerformanceData mPerformData;
  protected IPerformanceManager mManager;

  public PopupDialog(Context context, View content) {
    super(context, THEME);
    mManager = Injector.getInstance(IPerformanceManager.class);
    Resources r = context.getResources();
    mStatusbarHeight = r.getInteger(RR.integer("gree_heuristic_statusbar_height"));

    mContentView = content;
    setCanceledOnTouchOutside(false);
    init();
  }

  /** @return A content view of the dialog given to its constructor */
  public View getContentView() {
    return mContentView;
  }

  @Override
  public void show() {
    updateView();
    setTitleLayout();

    super.show();
  }

  private void updateView() {
    if (mSizeType == SIZE_TYPE_PROPOTION) {
      updateViewPropotion();
    } else if (mSizeType == SIZE_TYPE_PIXELS) {
      updateViewSize();
    } else if (mSizeType == SIZE_TYPE_AUTO) {
      updateViewAutoSize();
    }
  }

  public void setTitleType(int flag) {
    mTitleType = flag;

    if ((flag == TITLE_TYPE_WEBPAGE_HEADER) || (flag == TITLE_TYPE_STRING)) {
      if (mImageView != null) {
        mImageView.setVisibility(View.INVISIBLE);
      }
      if (mTextView != null) {
        mTextView.setVisibility(View.VISIBLE);
      }
    }
    else {
      if (mImageView != null) {
        mImageView.setVisibility(View.VISIBLE);
      }
      if (mTextView != null) {
        mTextView.setVisibility(View.INVISIBLE);
      }
    }
  }

  public void setTitle(String title) {

    if (mTitleType == TITLE_TYPE_WEBPAGE_HEADER || mTitleType == TITLE_TYPE_STRING) {
      setTitleString(title);
    }
  }

  /**
   * Sets the flag indicating whether the dialog tries to fit to its content.
   * Call to this method is not a requirement. The default flag value is false.
   * 
   * @param fit Flag specifying whether to fit the dialog height to its content
   * @param resId Resource ID of the View that decides the height of the dialog when fit is true.
   *        This will be ignored when fit is false.
   */
  public void setFitContent(boolean fit, int resId) {
    mFitContent = fit;
    mContentViewResId = resId;
  }

  /**
   * Sets the size of this dialog to the fixed dimension given as parameters.
   * If set true by setFitContent, this size will be the maximum dimension.
   * 
   * @param widthPixels fixed width of the dialog in pixels
   * @param heightPixels fixed height of the dialog in pixels
   */
  public void setSize(float widthPixels, float heightPixels) {
    mSizeType = SIZE_TYPE_PIXELS;
    mSizeWidthPropotion = 0;
    mSizeHeightPropotion = 0;
    mSizeWidthPixels = widthPixels;
    mSizeHeightPixels = heightPixels;
  }

  /**
   * Set the size of this dialog to the dimension computed with the ratios given as parameters.
   * If set true by setFitContent, this size will be the maximum dimension.
   * 
   * @param width ratio (0 ~ 1) of the dialog width relative to the screen width
   * @param height ratio (0 ~ 1) of the dialog height relative to the screen height
   */
  public void setProportion(double width, double height) {
    mSizeType = SIZE_TYPE_PROPOTION;
    mSizeWidthPropotion = width;
    mSizeHeightPropotion = height;
    mSizeWidthPixels = 0;
    mSizeHeightPixels = 0;
  }

  /**
   * Set the size type to auto-size mode.
   * In auto-size,
   *  - If short side is longer than 360dp, dialog size is restricted to fixed size
   *  - Else, the dialog has 10dp spaces from the edge.
   */
  public void setAutoSize() {
    mSizeType = SIZE_TYPE_AUTO;
  }

  protected void updateViewSize() {
    if (mSizeType == SIZE_TYPE_PIXELS) {
      DisplayMetrics metrics = new DisplayMetrics();
      ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
      WindowManager.LayoutParams params = getWindow().getAttributes();

      params.gravity = Gravity.CENTER;
      if (mFitContent) {
        params.width = (int) (metrics.scaledDensity * mSizeWidthPixels);
        params.height = (int) (metrics.scaledDensity * mSizeHeightPixels);
      } else {
        params.width = (int) (metrics.scaledDensity * mSizeWidthPixels);
        params.height = (int) (metrics.scaledDensity * mSizeHeightPixels);
      }

      getWindow().setAttributes(params);
    }
  }

  protected void updateViewPropotion() {
    if (mSizeType == SIZE_TYPE_PROPOTION) {
      Display display = ((WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
      WindowManager.LayoutParams params = getWindow().getAttributes();

      params.gravity = Gravity.CENTER;
      if (mFitContent) {
        int widthMax = (int)(display.getWidth() * mSizeWidthPropotion);
        int heightMax = (int)(display.getHeight() * mSizeHeightPropotion);
        View content = mContentView.findViewById(mContentViewResId);

        View header = mViewToLoad.findViewById(RR.id("gree_dialogHeaderLayout"));
        int width, height;
        if ((null != content) && (null != header)) {
          width = content.getWidth();
          height = content.getHeight() + header.getHeight();
        } else {
          width = mContentView.getWidth();
          height = mContentView.getHeight();
        }
        params.width = ((width <= 0) || (width > widthMax))? widthMax : width;
        params.height = ((height <= 0) || (height > heightMax))? heightMax : height;
      } else {
        params.width = (int)(display.getWidth() * mSizeWidthPropotion);
        params.height = (int)(display.getHeight() * mSizeHeightPropotion);
      }

      getWindow().setAttributes(params);
    }
  }

  protected void updateViewAutoSize() {
    if (mSizeType != SIZE_TYPE_AUTO) { return; }

    Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    DisplayMetrics metrics = new DisplayMetrics();
    display.getMetrics(metrics);

    WindowManager.LayoutParams params = getWindow().getAttributes();
    params.gravity = Gravity.CENTER;

    int shortSide = metrics.widthPixels < metrics.heightPixels ? metrics.widthPixels : metrics.heightPixels;
    float shortDp = shortSide / metrics.density;

    if (shortDp > DISPLAY_DP_THRESHOLD) {
      if (metrics.widthPixels < metrics.heightPixels) {
        params.width = (int) (WIDTH_MAX_DP * metrics.density);
        if (params.width >= metrics.widthPixels) {
          params.width = params.width - getMarginWidth(metrics.density);
        }
        params.height = (int) (HEIGHT_MAX_DP * metrics.density);
        if (params.height >= metrics.heightPixels) {
          params.height = params.height - getMarginHeight(metrics.density);
        }
      } else {
        params.width = (int) (HEIGHT_MAX_DP * metrics.density);
        if (params.width >= metrics.heightPixels) {
          params.width = params.width - getMarginWidth(metrics.density);
        }
        params.height = (int) (WIDTH_MAX_DP * metrics.density);
        if (params.height >= metrics.widthPixels) {
          params.height = params.height - getMarginHeight(metrics.density);
        }
      }
    } else {
      params.width = (int) (metrics.widthPixels + (-2 * MARGIN_DP) * metrics.density);
      params.height = (int) (metrics.heightPixels + (-2 * MARGIN_DP - mStatusbarHeight) * metrics.density);    
    }

    getWindow().setAttributes(params);

  }

  private int getMarginWidth(float density) {
    return (int) ((2 * MARGIN_DP) * density);
  }

  private int getMarginHeight(float density) {
    return (int) ((2 * MARGIN_DP + mStatusbarHeight) * density);
  }

  public void switchDismissButton(boolean flag) {
    if (flag) {
      mDismissButton.setVisibility(View.VISIBLE);
    }
    else {
      mDismissButton.setVisibility(View.GONE);
    }
  }

  protected boolean isPortrait() {
    Display display = ((WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

    if (display.getWidth() <= display.getHeight()) {
      return true;
    }

    return false;
  }

  /**
   * Initializes the instance and should be called only in the constructor. User can override this
   * method, but {@code super.init()} should be called at the beginning of the method.
   */
  protected void init() {
    mViewToLoad = LayoutInflater.from(this.getContext()).inflate(RR.layout("gree_popup_dialog_layout"), null);
    setContentView(mViewToLoad);

    mImageView = (ImageView)mViewToLoad.findViewById(RR.id("gree_dialogTitleLogo"));
    mTextView = (TextView)mViewToLoad.findViewById(RR.id("gree_dialogTitleText"));
    mDismissButton = (Button)mViewToLoad.findViewById(RR.id("gree_dialogDismissButton"));

    setTitleType(TITLE_TYPE_LOGO);
    setTitleString("");

    setAutoSize();
    setDismissButton();

    mContentLayout = (ConfigChangeListeningLayout)mViewToLoad.findViewById(RR.id("gree_dialogContentLayout"));
    mContentLayout.addOnConfigurationChangedListener(new OnPopupConfigurationChangedListener());
    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT);
    mContentLayout.addView(mContentView, params);
  }

  protected void setTitleLayout() {
    FrameLayout titleBarLayout = (FrameLayout)mViewToLoad.findViewById(RR.id("gree_dialogHeaderLayout"));
    TextView titleText = (TextView)titleBarLayout.findViewById(RR.id("gree_dialogTitleText"));
    Button closeButton = (Button)titleBarLayout.findViewById(RR.id("gree_dialogDismissButton"));

    DisplayMetrics metrics = new DisplayMetrics();
    ((WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
    int barHeight;
    int buttonSize;

    if (isPortrait()) {
      barHeight = (int)(metrics.scaledDensity * SIZE_TITLEBAR_PORTRAIT);
      buttonSize = (int)(metrics.scaledDensity * SIZE_TITLEBAR_PORTRAIT);
      titleText.setTextSize(FONTSIZE_TITLE_PORTRAIT);
    }
    else {
      barHeight = (int)(metrics.scaledDensity * SIZE_TITLEBAR_LANDSCAPE);
      buttonSize = (int)(metrics.scaledDensity * SIZE_TITLEBAR_LANDSCAPE);
      titleText.setTextSize(FONTSIZE_TITLE_LANDSCAPE);
    }

    titleBarLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT , barHeight));
    closeButton.setLayoutParams(new FrameLayout.LayoutParams(buttonSize, buttonSize));
  }

  protected void updateTitle(String title) {

    if (mTitleType == TITLE_TYPE_WEBPAGE_HEADER) {
      setTitleString(title);
    }
  }

  /**
   * Set the dismiss button behavior to dismiss the dialog on pressed. Sub classes can override this
   * method to change the behavior of the dismiss button. Use {@code setDismissButtonListener()} to
   * set OnClickListener when override.
   */
  protected void setDismissButton() {
    View.OnClickListener dismissClickListener = new View.OnClickListener() {
      public void onClick(View view) {
        PopupDialog.this.dismiss();
      }
    };

    mDismissButton.setOnClickListener(dismissClickListener);
  }

  /**
   * Sets OnClickListener for the dismiss button.
   * @param listner an OnClickListener defining the reaction to onClick events from the dismiss button
   */
  public final void setDismissButtonListener(View.OnClickListener listner) {
    mDismissButton.setOnClickListener(listner);
  }

  private void setTitleString(String title) {
    if (mTextView != null) {
      if (title != null && NUM_TITLE_STRING_MAX_LENGTH < title.length()) {
        title = title.substring(0, NUM_TITLE_STRING_MAX_LENGTH);
        title += "...";
      }
      mTextView.setText(title);
    }
  }


  /* In order to try to fit the dialog to its content, resize the dialog when the content
   * size is fixed. The content size is fixed when onWidnowFocusChanged is called.
   */
  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    if (mFitContent) {
      updateView();
    }
  }

  protected class OnPopupConfigurationChangedListener implements OnConfigurationChangedListener {
    public void onChanged(Configuration newConfig) {
      if (mSizeType == SIZE_TYPE_PROPOTION) {
        setProportion(mSizeWidthPropotion, mSizeHeightPropotion);
        updateViewPropotion();
      }
      else if (mSizeType == SIZE_TYPE_PIXELS) {
        setSize(mSizeWidthPixels, mSizeHeightPixels);
        updateViewSize();
      } else if (mSizeType == SIZE_TYPE_AUTO) {
        updateViewAutoSize();
      }
      setTitleLayout();
    }
  }

  /* Ignores events from the search button of the device. This is a workaround for the problem that
   * a back button event to a search dialog also closes a dialog behind it.
   */
  @Override
  public boolean dispatchKeyEvent(KeyEvent event) {
    if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_SEARCH) {
      return true;
    }
    return super.dispatchKeyEvent(event);
  }
}
