/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import net.gree.asdk.api.GreePlatformListener.TaskEventListener;
import net.gree.asdk.core.*;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.notifications.MessageDispatcher;
import net.gree.asdk.core.notifications.NotificationCounts;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Util;
import net.gree.oauth.signpost.OAuth;
import net.gree.oauth.signpost.OAuth.DebugListener;
import net.gree.vendor.com.google.gson.Gson;
import net.gree.vendor.com.google.gson.reflect.TypeToken;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.text.TextUtils;

import org.xmlpull.v1.XmlPullParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

/**
 * This class has a weak singleton pattern.  You can create a new GreePlatform
 * at any point, which is cached as a static object.  Obtain an instance by calling the
 * instance() method.
 * @author GREE, Inc.
 *
 */
public class GreePlatform {
 
    /**
     * Initializes the GreePlatform with a valid context.
     * @param context
     */
  public GreePlatform(Context context) {
    greePlatform = this;
    greePlatform.mContext = context;
  }

 
  /**
   * <p>The Bundle's extra key-alias name, when the GreePlatform Application is launched, has special information in certain situations.</p>
   * <p>
   * These additional parameters are notified through this hash key.<br>
   * For example, the notification on launch request/message detail pages or the notification local launches.<br>
   * We obtain this parameter following way:<br>
   * Sample Code:
   * </p>
   * <code><pre>
   * public void onCreate(Bundle savedInstanceState) {
   *   super.onCreate(savedInstanceState);
   *
   *    Bundle extras = getIntent().getExtras();
   *    if (extras != null) {
   *      if (extras.containsKey(GreePlatform.GREEPLATFORM_ARGS)) {
   *        String args = extras.getString(GreePlatform.GREEPLATFORM_ARGS);
   *        Toast.makeText(getApplicationContext(), "GREEARGS:" + args, Toast.LENGTH_LONG).show();
   *      }
   *    }
   *  }
   * </pre></code>
   *
   * @return additional string data.
   */
  public static final String GREEPLATFORM_ARGS = "greeArgs";

  static final String TAG = "GreePlatform";
  static boolean isDebug = false;
  static boolean isVerbose = false;

 
  /**
   * Sets the debug flag which on the debug logging.
   * @param debug
   */
  public static void setDebug(boolean debug) {
    isDebug = debug;
    DebugListener.debugging[0] = debug;
  }

 
  /**
   * Sets the verbose flag for this class in the logger.
   * 
   * @param verbose
   */
  public static void setVerbose(boolean verbose) {
    isVerbose = verbose;
  }

  Context mContext;
  static GreePlatform greePlatform = null;

  static Gson mGson = new Gson();

 
  /**
   * The GreePlatform singleton.
   * @return reference to static instance of GreePlatform.
   */
  public static GreePlatform instance() {
    return greePlatform;
  }

 
  /**
   * Returns the context this GreePlatform was initialized with.
   * @return context for this application
   */
  public static Context getContext() {
    return greePlatform.mContext;
  }

 
  /**
   * Sets the object of the implementation class that inherits from GreeResourcesImpl class.
   * @param impl Implementation class for GreeResources
   */
  public static void setGreeResourcesImpl(GreeResourcesImpl impl) {
    Resources res = getContext().getResources();
    GreeResources resources =
        new GreeResources(res.getAssets(), res.getDisplayMetrics(), res.getConfiguration());
    resources.setGreeResourcesImpl(impl);
    RR.setResources(resources);
    com.handmark.pulltorefresh.library.RR.setResources(resources);
  }

 
  /**
   * Initializes the GreePlatform SDK.  Call this once before using SDK features.
   * @param context The application Context.
   * @param appId 'Client Application ID' for this application.
   * @param encryptedKey encrypted 'Consumer Key' for this application.
   * @param encryptedSecret encrypted 'Consumer Secret' for this application.
   * @param options Additional options (See GreePlatformSettings). May be null. 
   * @param debug Flag for debug mode.
   */
  public static void initialize(Context context, String appId, String encryptedKey,
      String encryptedSecret, TreeMap<String, Object> options, boolean debug) {
    if (options == null) {
      options = new TreeMap<String, Object>();
    }
    greePlatform = new GreePlatform(context);
    greePlatform.init(debug);
    initResources(context, options, -1, null, debug);
    if (appId != null) {
      options.put(InternalSettings.ApplicationId, appId);
    }
    if (encryptedKey != null) {
      options.put(InternalSettings.EncryptedConsumerKey, encryptedKey);
    }
    if (encryptedSecret != null) {
      options.put(InternalSettings.EncryptedConsumerSecret, encryptedSecret);
    }
    handleScramble(context, options);
    Core.initialize(context, options);
  }

 
  /**
   * Initializes the GreePlatform SDK.  Call this once before using SDK features.
   * @param context The Context the application is to run it.
   * @param appId 'Client Application ID' for this application
   * @param key encrypted 'Consumer Key' for this application
   * @param secret encrypted 'Consumer Secret' for this application
   * @param options Additional options (See GreePlatformSettings). May be null. 
   * @param debug Flag for debug mode.
   */
  public static void initializeWithUnencryptedConsumerKeyAndSecret(Context context, String appId,
      String key, String secret, TreeMap<String, Object> options, boolean debug) {
    if (options == null) {
      options = new TreeMap<String, Object>();
    }
    greePlatform = new GreePlatform(context);
    greePlatform.init(debug);
    initResources(context, options, -1, null, debug);
    if (appId != null) {
      options.put(InternalSettings.ApplicationId, appId);
    }
    if (key != null) {
      options.put(InternalSettings.ConsumerKey, key);
    }
    if (secret != null) {
      options.put(InternalSettings.ConsumerSecret, secret);
    }
    Core.initialize(context, options);
  }

 
/**
 * Initializes the GreePlatform SDK.  Call this once before using SDK features.<br>
 * The configuration file should be placed in your res/xml directory,
 * or you can put the configuration in a JSON file in assets/ directory.
 * 
 * @param context the application context
 * @param optionsResourceId the Resource Id for the xml configuration file
 * @param asset Optional path to the JSON asset file
 */
  public static void initialize(Context context, int optionsResourceId, String asset) {
    TreeMap<String, Object> options = new TreeMap<String, Object>();
    greePlatform = new GreePlatform(context);
    greePlatform.init(false);
    initResources(context, options, optionsResourceId, asset, false);
    handleScramble(context, options);
    Core.initialize(context, options);
  }

  private static void handleScramble(Context context, TreeMap<String, Object> options) {
    byte[] digest = Util.getScrambleDigest(context);
    try {
      Object cso = options.get(InternalSettings.EncryptedConsumerSecret);
      if (cso != null) {
        String conSecret =
            Util.decryptConsumer(digest,
                (String) options.get(InternalSettings.EncryptedConsumerSecret));
        options.put(InternalSettings.ConsumerSecret, conSecret);
      }
      Object sko = options.get(InternalSettings.EncryptedConsumerKey);
      if (sko != null) {
        String conKey =
            Util.decryptConsumer(digest,
                (String) options.get(InternalSettings.EncryptedConsumerKey));
        options.put(InternalSettings.ConsumerKey, conKey);
      }
    } catch (Exception ex) {
      GLog.d(TAG, "handleScramble:" + ex.toString());
    }
  }

 
  /**
   * Gets the current Gree SDK version.
   * @return current Gree SDK version.
   */
  public static String getSdkVersion() {
    return Core.getSdkVersion();
  }

 
  /**
   * Gets the application manifest version as a String.
   * @return application version, from Manifest.
   */
  public static String getAppVersion() {
    return Core.getAppVersion();
  }

 
  /**
   * Returns the Gree SDK build information
   * @return Gree SDK build information.
   */
  public static String getSdkBuild() {
    return Core.getSdkBuild();
  }

  static void initResources(Context context, TreeMap<String, Object> options,
      int optionsResourceId, String asset, boolean debug) {
    loadOptionsFromJsonAsset(options, "internalSettings.json", true);
    if (optionsResourceId >= 0) {
      loadOptionsFromXmlResource(options, optionsResourceId, true);
    }
    if (asset != null) {
      GreePlatform.loadOptionsFromJsonAsset(options, asset, true);
    }
    loadOptionsFromJsonAsset(options, "debug_config.json", true);
    loadOptionsFromXmlResource(options, getResource(context, "@xml/debug_config"), true);
  }

  static OAuth.DebugListener debugListener = new OAuth.DebugListener() {
    @Override
    public void log(String msg) {
      if (isDebug) {
        GLog.d("GreePlatform", msg);
      }
    }
  };

 
  /**
   * Returns the option values as a Map
   * 
   * @return the option values
   */
  public static Map<String, Object> getOptions() {
    return CoreData.getParams();
  }

 
  /**
   * Returns the option value as a string
   * 
   * @param key 
   * @return the value for the passed key as a String
   */
  public static String getOption(String key) {
    try {
      return CoreData.get(key).toString();
    } catch (Exception ex) {
      GLog.w(TAG, "Get option for " + key + " error: " + ex.getMessage());
    }
    return null;
  }

 
  /**
   * Sets the option value as a string
   * 
   * @param key
   * @param value
   */
  public static void setOption(String key, String value) {
    CoreData.put(key, value);
  }

  /**
   * Private method that takes given options, loads XML and JSON options files if present, and
   * configures debug flags.
   * 
   * @param debug
   */
  private void init(boolean debug) {
    setDebug(debug);
    OAuth.setDebugListener(debugListener);
  }

 
  /**
   * Loads options from an XML resource file.
   * 
   * @param defaults Options used as a base. Content of the file overwrites these for matching keys.
   * @param resourceID The resource ID (R.xml....) for the XML file to load.
   * @param ignoreExceptions Boolean flag. True to avoids issuing exceptions. Returns false on failures.
   * @return Returns true if the options were loaded successfully, false otherwise.
   */
  public static boolean loadOptionsFromXmlResource(TreeMap<String, Object> defaults,
      int resourceID, boolean ignoreExceptions) {
    boolean ok = false;
    XmlResourceParser xml = null;
    try {
      xml = instance().mContext.getResources().getXml(resourceID);
    } catch (Exception e) {
      GLog.w(TAG, "Get resources for " + resourceID + " error: " + e.getMessage());
    }
    if (xml != null) {
      // terrible parser courtesy of it being Thursday
      try {
        String k = null;
        for (int eventType = xml.getEventType(); xml.getEventType() != XmlPullParser.END_DOCUMENT; xml
            .next(), eventType = xml.getEventType()) {
          if (eventType == XmlPullParser.START_TAG) {
            k = xml.getName();
          } else if (xml.getEventType() == XmlPullParser.TEXT) {
            if (isDebug) {
              GLog.d(TAG, "Adding property:" + k + "=" + xml.getText());
            }
            if (k != null) {
              defaults.put(k, xml.getText());
              k = null;
            }
          }
        }
        ok = true;
      } catch (Exception e) {
        if (!ignoreExceptions) {
          throw new RuntimeException(e);
        }
      }
      xml.close();
    }
    return ok;
  }

 
  /**
   * Loads options from a JSON asset files.
   * 
   * @param defaults Options used as a base. Content of the file overwrites these for matching keys.
   * @param path Path to the file.
   * @param ignoreExceptions Boolean flag. True will avoid issuing exceptions. Returns false on failures.
   * @return True if the options were loaded without error, false otherwise
   */
  public static boolean loadOptionsFromJsonAsset(TreeMap<String, Object> defaults, String path,
      boolean ignoreExceptions) {
    boolean ok = false;
    InputStream is = null;
    try {
      is = instance().mContext.getAssets().open(path);
      String json = Util.slurpString(is);
      Type mapType = new TypeToken<TreeMap<String, Object>>() {}.getType();

      TreeMap<String, Object> map = mGson.fromJson(json, mapType);
      if (isDebug) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
          GLog.d(TAG, "Adding property:" + entry.getKey() + "=" + entry.getValue());
        }
      }
      defaults.putAll(map);
      ok = true;
    } catch (FileNotFoundException fnfe) {
      // GLog.d(TAG, Util.stack2string(fnfe));
      if (!ignoreExceptions) {
        throw new RuntimeException(fnfe);
      }
    } catch (NullPointerException npe) { // Normal probably.
      // GLog.d(TAG, Util.stack2string(npe));
      if (!ignoreExceptions) {
        throw new RuntimeException(npe);
      }
    } catch (IOException e) {
      GLog.printStackTrace(TAG, e);
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
      if (!ignoreExceptions) {
        throw new RuntimeException(e);
      }
    }

    return ok;
  }

  private static final int ROTATION_DELAY = 3000;

 
  /**
   * Fixes known bug in pre-Honeycomb. <br>@see <a href="http://code.google.com/p/android/issues/detail?id=6191">issue-6191</a>
   * <br>Call this in every Activity.onCreate().<p>
   * 
   * Do this in every activity:
   * <pre>
   *  protected void onDetachedFromWindow() { 
   *    if (apiLevel == 7) {
   *      try { super.onDetachedFromWindow();
   *      } catch (IllegalArgumentException e) {
   *        // Quick catch and continue on api level 7 (Eclair 2.1)
   *        DLog.w( TAG,"Android project  issue 6191  workaround." ); 
   *      }
   *    } else {
   *      super.onDetachedFromWindow();
   *    }
   * </pre>   
   * or simply:
   * <pre>
   * protected void onDetachedFromWindow() {
   * try { super.onDetachedFromWindow(); } catch (IllegalArgumentException e) {}
   * }
   * </pre>
   * @param activity your activity
   * @param lockedOrientation true if the orientation of your activity is locked, false otherwise
   */
  public static void activityOnCreate(final Activity activity, boolean lockedOrientation) {
    int apiLevel = Integer.parseInt(Build.VERSION.SDK);
    // Potential race condition: if we've enabled unlocking, could still be running in background.
    int originalOrientationTry = activity.getResources().getConfiguration().orientation;
    int currentOrientation = originalOrientationTry;
    if (apiLevel == VERSION_CODES.ECLAIR_MR1 || apiLevel == VERSION_CODES.FROYO) {
      activity.setRequestedOrientation(currentOrientation);
      final Timer orientationTimer = new Timer();
      if (!lockedOrientation && !orientationWaiting) {
        orientationWaiting = true;
        orientationTimer.schedule(new TimerTask() {
          @Override
          public void run() {
            // Defaults to: ActivityInfo.SCREEN_ORIENTATION_SENSOR
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            orientationTimer.cancel();
            orientationWaiting = false;
          }
        }, ROTATION_DELAY);
      }
    }
  }

  static boolean orientationWaiting = false;

 
  /**
   * Returns the resource ID value without referencing R.* (might be in other packages or libraries).
   * 
   * @param resourceName The String of the resource name.
   * @return the resource ID corresponding to the passed resource name.
   */
  public int getResource(String resourceName) {
    String packageName = mContext.getPackageName();
    return mContext.getResources().getIdentifier(resourceName, null, packageName);
  }

 
  /**
   * Returns the resource ID value without referencing R.* (might be in other packages or libraries).
   * 
   * @param resourceName The String of the resource name.
   * @param context 
   * @return the resource ID corresponding to the passed resource name.
   */
  public static int getResource(Context context, String resourceName) {
    String packageName = context.getPackageName();
    return context.getResources().getIdentifier(resourceName, null, packageName);
  }

 
  /**
   * Returns the resource as a string, avoiding use of R.*.
   * 
   * @param id
   * @return the resource as a string
   */
  public static String getRString(int id) {
    final Context ctx = greePlatform.mContext;
    return ctx.getResources().getString(id);
  }

 
 /**
   * The local user id representing the currently authenticated GREE user.<br>
   * This is only available after notifying Authorizer.AuthorizeListener.onAuthorized() when calling Authorizer.authorize() for the first time.
   * 
   * @return If successful it returns the current user id string. Otherwise it returns null.
   */
  public static String getLocalUserId() {
    String userId = null;
    IAuthorizer authorizer = Injector.getInstance(IAuthorizer.class);
    if (authorizer.hasOAuthAccessToken()) {
      userId = authorizer.getOAuthUserId();
    }
    if (TextUtils.isEmpty(userId)) {
      return null;
    }
    return userId;
  }

 
 /**
   * The local user object representing the currently authenticated GREE User.<br>
   * This is only available after notifying Authorizer.UpdatedLocalUserListener.onUpdateLocalUser() when calling Authorizer.authorize() for the first time.
   *
   * @return If successful, it returns the current GreeUser class. Otherwise it returns null.
   */
  public static GreeUser getLocalUser() {
    return Core.getInstance().getLocalUser();
  }

 
  /**
   * Interface for the listener called after every badge update. 
   * The badge displays the number of unread notifications for your application. 
   * Requirement: It needs to be registered with the {@link #updateBadgeValues} method.
   */
  public interface BadgeListener {
   
   
    /**
     * Called every time the notification count has been updated.
     * @param newCount Integer representing the new badge count for this application.
     */
   
    void onBadgeCountUpdated(int newCount);
  }

 
/**
 * This method will update the notification badges to the latest values.
 * You only need to pass the listener the first time you call this method. It will be kept as a weakReference.
 * @param listener (optional) this listener will receive the latest updated value. This listener will be called every time the value is updated.
 */
  public static void updateBadgeValues(BadgeListener listener) {
    if (listener != null) {
      getNotificationCounts().addApplicationCountListener(listener);
    }
    getNotificationCounts().updateCounts();
  }

 
/**
 * Get the most recently updated values of the notification badges.<p>
 * Note: that this method is a simple value getter. It will not try to update the badge count with the server.<br>
 * Use {@link #updateBadgeValues} to update the value with the server.
 * @return The number of unread notifications for this application.
 */
  public static int getBadgeValues() {
    return getNotificationCounts().getNotificationCount(NotificationCounts.TYPE_APP);
  }

  private static NotificationCounts mNotificationCounts;

  private static NotificationCounts getNotificationCounts() {
    if (null == mNotificationCounts) {
      mNotificationCounts = Injector.getInstance(NotificationCounts.class);
    }
    return mNotificationCounts;
  }
 
  /**
   * This methods is used only by middleware.
   * Set original cookies.
   *
   * @param key key name
   * @param value value for key
   * @return true by success. false by already used keys. Or some arguments are null.
   */
  public static boolean setOriginalCookie(String key, String value) {
    return CookieStorage.setOriginalCookie(key, value);
  }

 
 
  /**
   * Register a TaskEventListener, 
   * it is used to notify events like Gree Activity or dialogs when they open, close or cancelled.
   */
 
  public void registerTaskEventListener(TaskEventListener listener) {
    Injector.getInstance(TaskEventDispatcher.class).registerTaskEventListener(listener, true);
  }

 
 
  /**
   * Unregister a TaskEventListener, this listener will not receive events.
   * previously registered with {@link registerTaskEventListener}
   * @param listener
   */
 
  public void unRegisterTaskEventListener(TaskEventListener listener) {
    Injector.getInstance(TaskEventDispatcher.class).unRegisterTaskEventListener(listener);
  }
  
 
 
  /**
   * Register your Activity.
   * It allows this activity to receive clickable notifications,
   * make sure you call this method in your activity.onstart call.
   * 
   * On devices with Android prior ICS,
   * If your activity is not registered it will only receive toast notifications (unclickable)
   * @param activity register activity
   */
 
  public static void registerActivity(Activity activity) {
    Injector.getInstance(MessageDispatcher.class).registerActivity(activity);
  }

 
 
  /**
   * Unregister Activity.
   * previously registered with {@link registerActivity}
   * Make sure you call this method in your activity.onStop method.
   * @param activity unregister activity
   */
 
  public static void unRegisterActivity(Activity activity) {
    Injector.getInstance(MessageDispatcher.class).unRegisterActivity(activity);
  }
  
 
 
  /**
   * initialize Application class for SDK.
   * Call this once before using SDK features.
   * @param application the application class
   */
 
  @SuppressLint("NewApi")
  public static void initApplication(Application application) {
    
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH ) {
      application.registerActivityLifecycleCallbacks(new ActivityLifeCycleListener());
    }
  }
}

