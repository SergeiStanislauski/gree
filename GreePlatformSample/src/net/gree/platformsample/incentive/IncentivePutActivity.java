/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.incentive;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HeaderIterator;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.incentive.IncentiveController;
import net.gree.asdk.api.incentive.callback.IncentivePutListener;
import net.gree.asdk.api.incentive.model.IncentiveEventArray;
import net.gree.platformsample.BaseActivity;
import net.gree.platformsample.HelpActivity;
import net.gree.platformsample.R;

public class IncentivePutActivity extends BaseActivity {
  private static final String TAG = IncentivePutActivity.class.getSimpleName();

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.incentive_put);
  }


  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) {
      return;
    }
    setUpBackButton();
    setUpAutoLoadMore();
    ///////////////////////////////////
    //// inventive api demo
    ////////////////////////////////
    final Button putBotton = (Button) findViewById(R.id.put);
    putBotton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        Log.v(TAG, "Put is triggered");

        EditText targetIds = (EditText) findViewById(R.id.targetIds);
        String vtargetIds = targetIds.getText().toString();
        if (TextUtils.isEmpty(vtargetIds)) {
          Log.e(TAG, "target id is invalid");
          return;
        }
        List<String> eventArray = new ArrayList<String>();
        String[] eventIds = vtargetIds.split(",");
        if (eventIds != null && eventIds.length > 0) {
          for (String user : eventIds) {
            eventArray.add(user);
          }
        } else {
          Log.e(TAG, "event id is empty");
          return;
        }

        IncentivePutListener callBack = new IncentivePutListener() {
          @Override
          public void onSuccess(IncentiveEventArray result) {
            putBotton.setEnabled(true);
            Log.e(TAG, "onSuccess");
            Log.e(TAG, result.toString());

            Toast.makeText(IncentivePutActivity.this, "PUT Success!", Toast.LENGTH_SHORT).show();
            Toast.makeText(IncentivePutActivity.this, result.toString(), Toast.LENGTH_LONG).show();

          }
          @Override
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            putBotton.setEnabled(true);
            StringBuilder sb = new StringBuilder();
            sb.append("onFailure: ");
            sb.append(" responseCode:" + responseCode);
            sb.append(" response:" + response);
            Log.e(TAG, sb.toString());
            Toast.makeText(IncentivePutActivity.this, sb.toString(), Toast.LENGTH_LONG).show();
          }

        };
        IncentiveController.put(eventArray, callBack);
        putBotton.setEnabled(false);
      }
    });
  }

  @Override
  protected void sync(boolean fromStart) {}

  @Override
  public void onOpenHelp(View v) {
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.incentive_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }

}
