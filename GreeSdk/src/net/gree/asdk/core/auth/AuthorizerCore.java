/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import net.gree.asdk.core.*;
import net.gree.asdk.core.inject.Inject;

import org.apache.http.HeaderIterator;
import org.apache.http.client.methods.HttpUriRequest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.GreeUser.GreeUserListener;
import net.gree.asdk.api.auth.Authorizer.AuthorizeListener;
import net.gree.asdk.api.auth.Authorizer.LogoutListener;
import net.gree.asdk.api.auth.Authorizer.UpdatedLocalUserListener;
import net.gree.asdk.api.auth.Authorizer.UpgradeListener;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.auth.SetupActivity.SetupListener;
import net.gree.asdk.core.notifications.MessageDescription;
import net.gree.asdk.core.notifications.MessageDispatcher;
import net.gree.asdk.core.notifications.NotificationCounts;
import net.gree.asdk.core.notifications.NotificationCounts.Listener;
import net.gree.asdk.core.notifications.c2dm.GreeC2DMUtil;
import net.gree.asdk.core.request.GeneralClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Url;
import net.gree.oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import net.gree.oauth.signpost.exception.OAuthCommunicationException;
import net.gree.oauth.signpost.exception.OAuthException;
import net.gree.oauth.signpost.exception.OAuthExpectationFailedException;
import net.gree.oauth.signpost.exception.OAuthMessageSignerException;

public final class AuthorizerCore implements IAuthorizer{
  private static final String TAG = AuthorizerCore.class.getSimpleName();

  private static final String QUERY_PARAM_SERVICE_CODE = "service_code";
  static final int TARGET_GRADE_UNKNOWN = -1;
  static final int TARGET_RELOGIN = -2;

  private Context mContext;

  private OAuthStorage mOAuthStorage;
  private OAuthCache mOAuthCache;
  private OAuth mOAuth;
  private Context mAuthContext;
  private AuthorizeListener mCoreListener;

  private NotificationCounts mNotificationCounts;

  private boolean mIsAuthorized = false;

  private InternalAuthorizeListener mInternalListener;
  private TaskEventDispatcher mTaskEventDispatcher;
  private Map<String, Object> mParams = new TreeMap<String, Object>();
  
  private UpdatedLocalUserListener mUpdatedLocalUserListener;

  // Creates a new setup listener for the authorization
  private SetupListener mAuthSetupListener;

  // private final internal AuthorizeListener class, no one could override or called.
  // all the internal behavior for the login call back could be implement here.
  private class InternalAuthorizeListener implements AuthorizeListener {
    private static final int UPDATE_USER_RETRY_INTERVAL = 3000;
    private Context context;
    private boolean mNotifiedLogin = false;

    public InternalAuthorizeListener(Context context) {
      this.context = context;
    }

    public void onAuthorized() {
      //Notify the login through TaskEvent API
      mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_UNDEFINED, GreePlatformListener.EVENT_LOGIN, null, null);

      final GreeUser me = Core.getInstance().getLocalUser();
      if (me != null) {
        if (mUpdatedLocalUserListener != null) {
          mUpdatedLocalUserListener.onUpdateLocalUser();
          mUpdatedLocalUserListener = null;
          mParams.put(GreePlatformListener.KEY_RESULTS, me);
          mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_UNDEFINED, GreePlatformListener.EVENT_USERINFO_RETRIEVED, null, mParams);
        }
        
        mNotificationCounts.updateCounts(new Listener() {
          @Override
          public void onUpdate() {
            notifyLogined(me);
          }
        });
      }
      else {
        retryUpdateLocalUser();
      }
      // Check and notify network connectivity firstly after user logged in
      Injector.getInstance(GConnectivityManager.class).checkAndNotifyConnectivity();
    }

    public void onError() {}

    public void onCancel() {}

    private void retryUpdateLocalUser() {
      GLog.d(TAG, "retry updating local user.");
      Core.getInstance().updateLocalUser(new GreeUserListener() {
        public void onSuccess(int index, int count, GreeUser[] users) {
          if (mUpdatedLocalUserListener != null) {
            mUpdatedLocalUserListener.onUpdateLocalUser();
            mUpdatedLocalUserListener = null;
            mParams.put(GreePlatformListener.KEY_RESULTS, users[0]);
            mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_UNDEFINED, GreePlatformListener.EVENT_USERINFO_RETRIEVED, null, mParams);
          }
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          new Handler().postDelayed(new Runnable() {
            public void run() {
              retryUpdateLocalUser();
            }
          }, UPDATE_USER_RETRY_INTERVAL);
        }
      });
    }
    
    private void notifyLogined(GreeUser me) {
      if (me != null && !mNotifiedLogin) {
        mNotifiedLogin = true;

        int notificationCounts = mNotificationCounts.getNotificationCount(NotificationCounts.TYPE_ALL);
        String welcomeString = context.getString(RR.string("gree_notification_logged"), me.getNickname());
        final MessageDescription welcome = new MessageDescription(null, welcomeString);
        welcome.setDuration(3000);
        welcome.setBadge(notificationCounts);
        welcome.setType(MessageDescription.FLAG_NOTIFICATION_MY_LOGIN);
        if (Core.isGreeApp()) {
          welcome.setIsClickable(false);
        }
        Injector.getInstance(MessageDispatcher.class).enqueue(context, welcome);
      }
    }
  }

  @Inject
  public AuthorizerCore(Context context, NotificationCounts notificationCounts) {
    mContext = context;
    mNotificationCounts = notificationCounts;
  }

  public void initialize() {
    // initialize the internal AuthorizeListener;
    mInternalListener = new InternalAuthorizeListener(mContext);
    mOAuthStorage = new OAuthStorage(mContext);
    mOAuthCache = new OAuthCache(mContext);

    mOAuth = new OAuth();
    if (mOAuthStorage.hasToken()) {
      mOAuth.getConsumer().setTokenWithSecret(mOAuthStorage.getToken(),
          mOAuthStorage.getSecret());
    }
    mTaskEventDispatcher = Injector.getInstance(TaskEventDispatcher.class);
    mAuthSetupListener = new SetupListener() {
      public void onSuccess() {
        mIsAuthorized = true;
        GreeC2DMUtil.register(mAuthContext);
        if (mCoreListener != null) mCoreListener.onAuthorized();
        if (mInternalListener != null) mInternalListener.onAuthorized();
      }

      public void onCancel() {
        if (mCoreListener != null) mCoreListener.onCancel();
        if (mInternalListener != null) mInternalListener.onCancel();
      }

      public void onError() {
        if (mCoreListener != null) mCoreListener.onError();
        if (mInternalListener != null) mInternalListener.onError();
      }
    };
  }

  @Override
  public boolean isAuthorized() {
    return mIsAuthorized;
  }

  @Override
  public void authorize(Context context, String serviceCode, AuthorizeListener listener, final UpdatedLocalUserListener localuser_listener) {
    authorize(context, serviceCode, listener, localuser_listener, TARGET_GRADE_UNKNOWN);
  }

  @Override
  public void authorize(Context context, String serviceCode, final AuthorizeListener listener,
      UpdatedLocalUserListener localuser_listener, int user_grade) {
    // sets the Local User Listener, if we are using it
    mUpdatedLocalUserListener = localuser_listener;
    // calls the Activity that displays a popup window and logs the user in
    // sets the authorization context
    setAuthCoreContext(context);
    // stashes the listener
    setAuthCoreListener(listener);
    AuthorizeSequenceBase sequence = AuthorizeSequenceFactory.getExecuter(context, serviceCode, user_grade);
    sequence.execute(mAuthSetupListener);
  }

  @Override
  public void reauthorize(Context context, final AuthorizeListener listener) {
    logout();
    String url = Url.getConfirmReauthorizeUrl();
    SetupActivity.setupNewTask(context, url, new SetupListener() {
      public void onSuccess() {
        if (listener != null) listener.onAuthorized();
        if (mInternalListener != null) mInternalListener.onAuthorized();
      }

      public void onCancel() {
        if (listener != null) {
          listener.onCancel();
        }
        if (mInternalListener != null) {
          mInternalListener.onCancel();
        }
      }

      public void onError() {
        if (listener != null) listener.onError();
        if (mInternalListener != null) mInternalListener.onError();
      }
    });
  }

  @Override
  public void logout(final Context context, final LogoutListener logoutListener,
                     final AuthorizeListener loginListener, final UpdatedLocalUserListener localuser_listener) {
    SetupActivity.setup(context, Url.getLogoutUrl(), new SetupListener() {
      public void onSuccess() {
        // clear logout user's notification counts.
        mNotificationCounts.clearCounts();

        if (logoutListener != null) {
          logoutListener.onLogout();
        }
        
        //Notify the logout through TaskEvent API
        mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_UNDEFINED, GreePlatformListener.EVENT_LOGOUT, null, null);
        
        // login again immediately
        authorize(context, null, loginListener, localuser_listener, TARGET_RELOGIN);
      }

      public void onCancel() {
        if (logoutListener != null) {
          logoutListener.onCancel();
        }
      }

      public void onError() {
        if (logoutListener != null) {
          logoutListener.onError();
        }
      }
    });
  }

  @Override
  public void upgrade(final Context context, final int targetGrade, final String serviceCode,
                      final UpgradeListener listener, final UpdatedLocalUserListener localuser_listener) {
    if (targetGrade <= 0) {
      GLog.e(TAG, "Illegal targetGrade:" + targetGrade);
      if (listener != null) listener.onError();
      return;
    }

    if (AsyncErrorDialog.shouldShowErrorDialog(context)) {
      if (listener != null) listener.onCancel();
      new AsyncErrorDialog(context).show();
      return;
    }

    new Session().refreshSessionId(context, new OnResponseCallback<String>(){
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        mUpdatedLocalUserListener = localuser_listener;
        StringBuilder url = new StringBuilder(Url.getConfirmUpgradeUserUrl());
        url.append('&').append(Upgrader.QUERY_PARAM_TARGET_GRADE).append('=').append(targetGrade);
        if (serviceCode != null) {
          url.append('&').append(QUERY_PARAM_SERVICE_CODE).append('=').append(serviceCode);
        }
        SetupActivity.setupNewTask(context, url.toString(), new SetupListener() {
          public void onSuccess() {
            if (listener != null) listener.onUpgrade();
            if (mInternalListener != null) mInternalListener.onAuthorized();
          }

          public void onCancel() {
            if (listener != null) listener.onCancel();
          }

          public void onError() {
            if (listener != null) listener.onError();
          }
        });
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, "refreshSessionId failed in upgrade.");
        if (listener != null) listener.onError();
      }
    });
  }

  @Override
  public boolean hasOAuthAccessToken() {
    return mOAuthStorage.hasToken();
  }

  @Override
  public String getOAuthAccessToken() {
    return mOAuthStorage.getToken();
  }

  @Override
  public String getOAuthAccessTokenSecret() {
    return mOAuthStorage.getSecret();
  }

  @Override
  public String getOAuthUserId() {
    return mOAuthStorage.getUserId();
  }

  @Override
  synchronized public void signFor2Legged(HttpUriRequest request)
      throws OAuthMessageSignerException, OAuthExpectationFailedException,
      OAuthCommunicationException {
    new CommonsHttpOAuthConsumer(CoreData.get(InternalSettings.ConsumerKey),
      CoreData.get(InternalSettings.ConsumerSecret)).sign(request);
  }

  @Override
  synchronized public void signFor3Legged(HttpUriRequest request)
      throws OAuthMessageSignerException, OAuthExpectationFailedException,
      OAuthCommunicationException {
    mOAuth.getConsumer().sign(request);
  }

  public interface OnOAuthResponseListener<T> {
    public void onSuccess(T value);
    public void onFailure(OAuthException e);
  }

  @Override
  public void retrieveRequestToken(Context context, final OnOAuthResponseListener<String> listener) {
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OPEN);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_REQUESTTOKEN_GET_START);
    mOAuth.retrieveRequestToken(context, new OnOAuthResponseListener<String>() {
      public void onSuccess(final String value) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_REQUESTTOKEN_GET_END);
        manager.flushData(performData);
        new Thread(new Runnable() {
          public void run() {
            CommonsHttpOAuthConsumer consumer = mOAuth.getConsumer();
            try {
              mOAuthCache.save(consumer.getToken(), consumer.getTokenSecret());
            } catch (IOException e) {
              GLog.printStackTrace(TAG, e);
            }
          }
        }).start();
        if (listener != null) {
          listener.onSuccess(value);
        }
      }

      public void onFailure(OAuthException e) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_REQUESTTOKEN_GET_ERROR);
        manager.flushData(performData);
        if (listener != null) listener.onFailure(e);
      }
    });
  }

  @Override
  public void clearRequestTokenParams() {
    mOAuth.clearRequestTokenParams();
  }

  @Override
  public void retrieveAccessToken(Context context, String url, final OnOAuthResponseListener<Void> listener) {
    final CommonsHttpOAuthConsumer consumer = mOAuth.getConsumer();
    final String token = Uri.parse(url).getQueryParameter(net.gree.oauth.signpost.OAuth.OAUTH_TOKEN);
    String tokenOnMemory = consumer.getToken();
    if (tokenOnMemory == null) {
      String secret = mOAuthCache.get(token);
      consumer.setTokenWithSecret(token, secret);
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OPEN);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_ACCESSTOKEN_GET_START);
    mOAuth.retrieveAccessToken(context, url, new OnOAuthResponseListener<Void>() {
      public void onSuccess(Void response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_ACCESSTOKEN_GET_END);
        manager.flushData(performData);
        String userId = consumer.getUserId();
        mOAuthStorage.setToken(consumer.getToken());
        mOAuthStorage.setSecret(consumer.getTokenSecret());
        mOAuthStorage.setUserId(userId);
        mOAuthCache.clear();
        Logger.startActiveTimer();
        if (listener != null) listener.onSuccess(response);
      }

      public void onFailure(OAuthException e) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_ACCESSTOKEN_GET_ERROR);
        manager.flushData(performData);
        mOAuthCache.clear();
        if (listener != null) listener.onFailure(e);
      }
    });
  }

  @Override
  public void clearAccessTokenParams() {
    mOAuth.clearAccessTokenParams();
  }

  @Override
  public void clearOAuth() {
    Logger.stopActiveTimer();
    mOAuthStorage.clear();
    mOAuthCache.clear();
    mOAuth.initialize();
  }

  @Override
  public boolean logout() {
    mInternalListener.mNotifiedLogin = false;
    mIsAuthorized = false;
    clearOAuth();
    CookieStorage.removeAllCookie();
    CookieStorage.initialize();
    Core.getInstance().removeLocalUser();
    return true;
  }

  /**
   * This function logs the user out. It <b> will not </b> display an intermediate popup window asking the user if 
   * they are sure that they wish to log out. The boolean, autoRestart, is by default set to true so that the application
   * is automatically relogged in after starting. Depending on how you code your flow you may not want this. Typically
   * Unity applications want to set this to false. 
   * <p>
   * Note that reauthorization is not automatic. Use the auto restart to handle this is it is a desired effect.
   * 
   */
  @Override
  public void directLogout(final Activity activity, final LogoutListener logoutListener, 
                           final boolean autoRestart) {
    String url = AuthorizeContext.appendQueryParameter(Url.getLogoutCommitUrl());
    // calls the 0 legged auth call on the General Client - will make a call to the serve to log the user out
    new GeneralClient().oauth2(url, "get", null, false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        // logs the user out
        boolean isLoggedOut = logout();
        // if the user is successfully logged out
        if(isLoggedOut && logoutListener != null){
          // calls the LogoutListener
          logoutListener.onLogout();

          // clear logout user's notification counts.
          mNotificationCounts.clearCounts();

          //Notify the logout through TaskEvent API
          mTaskEventDispatcher.dispatchEvent(GreePlatformListener.CLASS_UNDEFINED, GreePlatformListener.EVENT_LOGOUT, null, null);

        }else if (!isLoggedOut && logoutListener != null){
          // registers an error
          logoutListener.onError();
        }

        else if(logoutListener != null){
          // returns a failure of the logout
          logoutListener.onCancel();
        }

        // reauthorizes so we have complete control
        //resets the activities UI, allows us to not need to force that relogin popup if we don't want to
        if(autoRestart && isLoggedOut){
          Intent refresh = new Intent(activity, activity.getClass());
          activity.startActivity(refresh);
          activity.finish();
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (logoutListener != null) {
          logoutListener.onError();
        }
      }
    });
  }
  

  private void setAuthCoreContext(Context context) {
    mAuthContext = context; 
  }
  

  private void setAuthCoreListener(AuthorizeListener listener) {
    mCoreListener = listener;
    
  }


  @Override
  public void setTokenWithSecret(String token, String tokenSecret) {
    mOAuth.getConsumer().setTokenWithSecret(token, tokenSecret);
  }

}
