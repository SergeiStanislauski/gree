package net.gree.asdk.core.dashboard;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html.ImageGetter;
import android.util.DisplayMetrics;

/**
 * Emoji getter for TextView.fromHtml &lt;img&gt; tag
 */
public class EmojiGetter implements ImageGetter {
  
  public EmojiGetter(Context context, DisplayMetrics metrics) {
    mContext = context;
    mSize = (int) (EMOJI_SIZE * metrics.density);
  }
  
  private Context mContext;
  private final static int EMOJI_SIZE = 14;
  private int mSize;
  

  @Override
  public Drawable getDrawable(String source) {
    Bitmap bmp = EmojiController.getEmoji(Integer.parseInt(source));
    Drawable ret = new BitmapDrawable(mContext.getResources(), bmp);
    ret.setBounds(0, 0, mSize, mSize);
    return ret;
  }

}
