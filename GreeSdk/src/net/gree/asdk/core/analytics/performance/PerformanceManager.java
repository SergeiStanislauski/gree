/*
* Copyright 2012 GREE, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package net.gree.asdk.core.analytics.performance;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.GreeLooperThread;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;

public class PerformanceManager extends IPerformanceManager {
  private static final String TAG = "PerformanceManager";

  private Context mContext;
  private static final String DEFAULT_FILE_PATH = Environment.getExternalStorageDirectory().getPath() + "/log.performance.";
  private static final int MAX_FLOW_INDEX_NUM = 16;
  private int[] mCountList;
  private GreeLooperThread mThread;
  private Runtime mRuntime;

  /**
   * Constractor.
   */
  public PerformanceManager(Context context) {
    super(context);
    mContext = context;
    mCountList = new int[MAX_FLOW_INDEX_NUM];
    mThread = new GreeLooperThread() {
      @Override
      protected void handleGreeMessage(Message msg) {
        ouputfile((PerformanceData)msg.obj);
      }
    };
    mThread.start();
    mRuntime = Runtime.getRuntime();
  }

  /**
   * Open method for file to write performance log data.
   */
  private OutputStreamWriter openFile() {
    Calendar cal = Calendar.getInstance();
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);
    int day = cal.get(Calendar.DAY_OF_MONTH);
    String openFilePath = DEFAULT_FILE_PATH + year + month + day + ".txt";
    GLog.d(TAG, "OpenFile:[" + openFilePath + "]");
    File performFile = new File(openFilePath);
    if (performFile != null) {
      try {
        if (!performFile.exists()) {
          performFile.createNewFile();
        }
        return new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(openFilePath, true)));
      } catch (Exception e) { GLog.printStackTrace(TAG, e); }
    }
    return null;
  }

  /**
   * Create performanceData object method.
   */
  public PerformanceData createData(PerformanceFlowIndex flowIndex) {
    PerformanceData tmpData = new PerformanceData(mContext, flowIndex, mCountList[flowIndex.getFlowIndex()]++);
    tmpData.init();
    return tmpData;
  }

  /**
   * Create performanceData object with subIndex(OS API only) method.
   */
  public PerformanceData createData(PerformanceFlowIndex flowIndex, int subIndex) {
    PerformanceData tmpData = new PerformanceData(mContext, flowIndex, mCountList[flowIndex.getFlowIndex()]++);
    tmpData.init(subIndex);
    return tmpData;
  }

  /**
   * Flush data method for stored performance log data in PerformanceData object.
   */
  public void recordData(PerformanceData data, String key) {
    if (data == null) { return; }
    data.record(key);
    return;
  }

  /**
   * Flush data method for stored performance log data in PerformanceData object.
   */
  public void flushData(PerformanceData data) {
    if (data == null) { return; }
    Handler handler = mThread.getHandler();
    if (handler == null) { GLog.w(TAG, "plog flush failed."); return; }

    handler.sendMessage(Message.obtain(handler, 0, data));
    return;
  }

  /**
   * Write method to file about performance log data.
   */
  private void ouputfile(PerformanceData data) {
    if (data == null) { return; }
    GLog.v(TAG, "Data output count:[" + data.count() + "]");
    OutputStreamWriter outputStream = openFile();

    // Loop when remains output logging data.
    while (data.count() > 0) {
      String buf = addMemoryUsage(data.output());
      GLog.v(TAG, "Data output:" + buf);
      try {
        outputStream.write(buf);
      } catch (Exception e) { GLog.printStackTrace(TAG, e); }
    }
    try {
      outputStream.flush();
      outputStream.close();
    } catch (Exception e) { GLog.printStackTrace(TAG, e); }

    return;
  }

  /**
   * Get memory usage from Runtime.[KB]
   */
  private String addMemoryUsage(String data) {
    StringBuilder ret = new StringBuilder(data);
    long mem = (mRuntime.totalMemory() - mRuntime.freeMemory());
    if (mem != 0) {
      mem = mem / 1024;
    }
    ret.insert(ret.indexOf("\n"), ",");
    ret.insert(ret.indexOf("\n"), mem);
    return ret.toString();
  }
}
