/*
* Copyright 2012 GREE, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package net.gree.asdk.core.analytics.performance;

import java.util.HashMap;
import java.util.Map;

public class PerformanceIndexMap {
  // Point Index key name.
  public static final String INDEX_KEY_URL_LOAD_START                   = "url_load_start";
  public static final String INDEX_KEY_URL_LOAD_ERROR                   = "url_laod_error";
  public static final String INDEX_KEY_URL_LOAD_END                     = "url_load_end";
  public static final String INDEX_KEY_POPUP_START                      = "popup_start";
  public static final String INDEX_KEY_POST_START                       = "post_start";
  public static final String INDEX_KEY_FINISH                           = "finish";
  public static final String INDEX_KEY_DISMISS                          = "dismiss";
  public static final String INDEX_KEY_CANCEL                           = "cancel";
  public static final String INDEX_KEY_GOTO_DEPOSIT                     = "goto_deposit";
  public static final String INDEX_KEY_DEPOSIT_START                    = "deposit_start";
  public static final String INDEX_KEY_DEPOSIT_ERROR                    = "deposit_error";
  public static final String INDEX_KEY_DEPOSIT_END                      = "deposit_end";
  public static final String INDEX_KEY_CONTACT_START                    = "contact_start";
  public static final String INDEX_KEY_HISTORY_START                    = "history_start";
  public static final String INDEX_KEY_DASHBOARD_START                  = "dashboard_start";
  public static final String INDEX_KEY_GOTO_NOTIFICATIONBOARD           = "goto_notificationboard";
  public static final String INDEX_KEY_GOTO_UM                          = "goto_um";
  public static final String INDEX_KEY_UM_START                         = "um_start";
  public static final String INDEX_KEY_LAUNCH_NATIVEAPP                 = "launch_nativeapp";
  public static final String INDEX_KEY_NOTIFICATIONBOARD_START          = "notificationboard_start";
  public static final String INDEX_KEY_ROOT_GET_START                   = "root_get_start";
  public static final String INDEX_KEY_ROOT_GET_ERROR                   = "root_get_error";
  public static final String INDEX_KEY_ROOT_GET_END                     = "root_get_end";
  public static final String INDEX_KEY_REQUESTTOKEN_GET_START           = "requesttoken_get_start";
  public static final String INDEX_KEY_REQUESTTOKEN_GET_ERROR           = "requesttoken_get_error";
  public static final String INDEX_KEY_REQUESTTOKEN_GET_END             = "requesttoken_get_end";
  public static final String INDEX_KEY_AUTHORIZE_URL_LOAD_START         = "authorize_url_load_start";
  public static final String INDEX_KEY_AUTHORIZE_URL_LOAD_ERROR         = "authorize_url_laod_error";
  public static final String INDEX_KEY_AUTHORIZE_URL_LOAD_END           = "authorize_url_load_end";
  public static final String INDEX_KEY_ACCESSTOKEN_GET_START            = "accesstoken_get_start";
  public static final String INDEX_KEY_ACCESSTOKEN_GET_ERROR            = "accesstoken_get_error";
  public static final String INDEX_KEY_ACCESSTOKEN_GET_END              = "accesstoken_get_end";
  public static final String INDEX_KEY_PURCHASELIST_GET_START           = "purchaselist_get_start";
  public static final String INDEX_KEY_PURCHASELIST_GET_ERROR           = "purchaselist_get_error";
  public static final String INDEX_KEY_PURCHASELIST_GET_END             = "purchaselist_get_end";
  public static final String INDEX_KEY_SELECT_SUBNAVI                   = "select_subnavi";
  public static final String INDEX_KEY_SELECT_UM_ITEM                   = "select_um_item";
  public static final String INDEX_KEY_START_PUSH_VIEW                  = "start_push_view";
  public static final String INDEX_KEY_START_POP_VIEW                   = "start_pop_view";

  // OS API extra index define.(each API Point Index top number)
  public static final int OS_SUB_INDEX_PEOPLE_GET                       = 0;
  public static final int OS_SUB_INDEX_IGNORELIST_GET                   = 3;
  public static final int OS_SUB_INDEX_SGPSCORE_GET                     = 6;
  public static final int OS_SUB_INDEX_SGPSCORE_POST                    = 9;
  public static final int OS_SUB_INDEX_SGPSCORE_DELETE                  = 12;
  public static final int OS_SUB_INDEX_SGPRANKING_GET                   = 15;
  public static final int OS_SUB_INDEX_SGPLEADERBOARD_GET               = 18;
  public static final int OS_SUB_INDEX_SGPLEADERBOARD_PUT               = 21;
  public static final int OS_SUB_INDEX_SGPACHIEVEMENT_GET               = 24;
  public static final int OS_SUB_INDEX_SGPACHIEVEMENT_POST              = 27;
  public static final int OS_SUB_INDEX_SGPACHIEVEMENT_PUT               = 30;
  public static final int OS_SUB_INDEX_TOUCHSESSION_GET                 = 33;
  public static final int OS_SUB_INDEX_BADGE_GET                        = 36;
  public static final int OS_SUB_INDEX_SDKBOOTSTRAP_GET                 = 39;
  public static final int OS_SUB_INDEX_MODERATION_GET                   = 42;
  public static final int OS_SUB_INDEX_MODERATION_POST                  = 45;
  public static final int OS_SUB_INDEX_MODERATION_PUT                   = 48;
  public static final int OS_SUB_INDEX_MODERATION_DELETE                = 51;
  public static final int OS_SUB_INDEX_FRIENDCODE_GET                   = 54;
  public static final int OS_SUB_INDEX_FRIENDCODE_POST                  = 57;
  public static final int OS_SUB_INDEX_FRIENDCODE_DELETE                = 60;
  public static final int OS_SUB_INDEX_IMAGE_GET                        = 63;
  public static final int OS_SUB_INDEX_PRODUCTENTRIES_GET               = 66;
  public static final int OS_SUB_INDEX_USERSTATUS_GET                   = 69;
  public static final int OS_SUB_INDEX_PRODUCTTRANSACTIONCOMMIT_PUT     = 72;
  public enum PerformanceFlowIndex {
    LOGIN {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
      }
    },
    INVITE {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
      }
    },
    SHARE {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
      }
    },
    REQUEST {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
      }
    },
    PAYMENT {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
        mPointMap.put(INDEX_KEY_GOTO_DEPOSIT, 7);
      }
    },
    PAYMENT_DEPOSIT_IAP {
    },
    PAYMENT_DEPOSIT_IAB {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_DISMISS, 4);
        mPointMap.put(INDEX_KEY_CANCEL, 5);
        mPointMap.put(INDEX_KEY_DEPOSIT_START, 6);
        mPointMap.put(INDEX_KEY_DEPOSIT_ERROR, 7);
        mPointMap.put(INDEX_KEY_DEPOSIT_END, 8);
      }
    },
    PAYMENT_HISTORY {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_DISMISS, 4);
        mPointMap.put(INDEX_KEY_CANCEL, 5);
        mPointMap.put(INDEX_KEY_CONTACT_START, 6);
        mPointMap.put(INDEX_KEY_HISTORY_START, 7);
      }
    },
    UPGRADE {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
      }
    },
    LOGOUT {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
      }
    },
    DASHBOARD {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_DASHBOARD_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_DISMISS, 4);
        mPointMap.put(INDEX_KEY_GOTO_NOTIFICATIONBOARD, 5);
        mPointMap.put(INDEX_KEY_GOTO_UM, 6);
        mPointMap.put(INDEX_KEY_SELECT_SUBNAVI, 7);
        mPointMap.put(INDEX_KEY_SELECT_UM_ITEM, 8);
        mPointMap.put(INDEX_KEY_START_PUSH_VIEW, 9);
        mPointMap.put(INDEX_KEY_START_POP_VIEW, 10);
      }
    },
    DASHBOARD_UM {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_UM_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_DISMISS, 4);
        mPointMap.put(INDEX_KEY_LAUNCH_NATIVEAPP, 5);
      }
    },
    NOTIFICATIONBOARD {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_NOTIFICATIONBOARD_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_DISMISS, 4);
        mPointMap.put(INDEX_KEY_LAUNCH_NATIVEAPP, 5);
      }
    },
    OS {
      @Override
      public void loadPointMap(int subIndex) {
        mPointMap.put(INDEX_KEY_URL_LOAD_START, subIndex);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, subIndex + 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, subIndex + 2);
      }
    },
    OPEN {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_ROOT_GET_START, 0);
        mPointMap.put(INDEX_KEY_ROOT_GET_ERROR, 1);
        mPointMap.put(INDEX_KEY_ROOT_GET_END, 2);
        mPointMap.put(INDEX_KEY_REQUESTTOKEN_GET_START, 3);
        mPointMap.put(INDEX_KEY_REQUESTTOKEN_GET_ERROR, 4);
        mPointMap.put(INDEX_KEY_REQUESTTOKEN_GET_END, 5);
        mPointMap.put(INDEX_KEY_AUTHORIZE_URL_LOAD_START, 6);
        mPointMap.put(INDEX_KEY_AUTHORIZE_URL_LOAD_ERROR, 7);
        mPointMap.put(INDEX_KEY_AUTHORIZE_URL_LOAD_END, 8);
        mPointMap.put(INDEX_KEY_ACCESSTOKEN_GET_START, 9);
        mPointMap.put(INDEX_KEY_ACCESSTOKEN_GET_ERROR, 10);
        mPointMap.put(INDEX_KEY_ACCESSTOKEN_GET_END, 11);
      }
    },
    PAYMENT_API {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_PURCHASELIST_GET_START, 3);
        mPointMap.put(INDEX_KEY_PURCHASELIST_GET_ERROR, 4);
        mPointMap.put(INDEX_KEY_PURCHASELIST_GET_END, 5);
      }
    },
    RELOGIN {
      @Override
      public void loadPointMap() {
        mPointMap.put(INDEX_KEY_POPUP_START, 0);
        mPointMap.put(INDEX_KEY_URL_LOAD_START, 1);
        mPointMap.put(INDEX_KEY_URL_LOAD_ERROR, 2);
        mPointMap.put(INDEX_KEY_URL_LOAD_END, 3);
        mPointMap.put(INDEX_KEY_POST_START, 4);
        mPointMap.put(INDEX_KEY_DISMISS, 5);
        mPointMap.put(INDEX_KEY_CANCEL, 6);
      }
    };

    protected Map<String, Integer> mPointMap = new HashMap<String, Integer>();

    // return flow index number method.
    public int getFlowIndex() { return this.ordinal(); }

    // If not override, nothing to do.
    public void loadPointMap() {}
    public void loadPointMap(int pointIndex) {}

    public boolean isExistPointMapKey(String key) { return mPointMap.containsKey(key); }
    public int getPointMapKeyData(String key) { return mPointMap.get(key).intValue(); }
  }
}
