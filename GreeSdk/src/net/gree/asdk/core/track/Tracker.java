/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.track;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.core.GConnectivityManager;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.inject.Inject;
import net.gree.asdk.core.request.Constants;
import net.gree.asdk.core.util.Preconditions;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import android.os.Handler;
import android.os.Looper;

/**
 * Tracker provides a generic database-backed store, trackQueue, and processing framework. Using
 * type, ID, and value triples, it allows many different modules to post requests to be processed
 * when networking is available, even if it is after restart.
 * 
 * @author GREE, Inc.
 */
public class Tracker {
  public static final String TAG = "Tracker";

  private GConnectivityManager connectivityManager;

  private TrackItemStorage storage;

  private final TrackQueue trackQueue;

  // A map to cache the uploader. Key is the name of the uploader.
  private final Map<String, Uploader> uploaderMap;

  private int retryDelay = 10 * 1000;
  private int maxRetryCount = 5;

  // String that is used to compute hashes of valid Tracker database entries.
  private String mixer = "";

  /**
   * Create a new instance of Tracker. There should have one and only one instance in the app.
   * 
   * @param storage {@link TrackItemStorage } that store the {@link TrackItem}
   * @param connectivityManager {@link GConnectivityManager} that provide the network info using in
   *        the track system
   * 
   * @throws NullPointerException if any of the parameters is null
   */
  @Inject
  public Tracker(TrackItemStorage storage, GConnectivityManager connectivityManager) {
    Preconditions.checkNotNull(storage, "Storage is required");
    Preconditions.checkNotNull(connectivityManager, "ConnectivityManager is required");

    GLog.d(TAG, "initializing");
    this.connectivityManager = connectivityManager;
    this.storage = storage;
    uploaderMap = new ConcurrentHashMap<String, Uploader>();
    trackQueue = new TrackQueue();
    connectivityManager.registerListener(new GConnectivityManager.ConnectivityListener() {
      @Override
      public void onConnectivityChanged(boolean isConnected) {
        if (isConnected) {
          Tracker.this.commit();
        }
      }
    });
    GLog.d(TAG, "initialized");
  }

  /**
   * Tracker uses this interface to update the internal state, so the implementation of
   * {@link Uploader} must call one of the callback methods within the same thread of
   * {@link Uploader}:
   * 
   * <pre>
   * <ul>
   *   <ol>onSuccess: while success upload the data to server</ol>
   *   <ol>onFailure: while faile to upload the data to server</ol>
   * </ul>
   * </pre>
   * 
   * The track will not work well if the uploader's implementation dosen't call the callback method.
   * It's not good that the tracker dependences the implementation of {@link Uploader}, but it works
   * and will change in future.
   */
  public interface UploadStatus {

    /**
     * while success upload the data to server
     *
     * @see Tracker#track(String, String, String, String, net.gree.asdk.core.track.Tracker.Uploader)
     */
    void onSuccess(String type, String key, String value);

    /**
     * while failed to upload the data to server
     *
     * @see Tracker#track(String, String, String, String, net.gree.asdk.core.track.Tracker.Uploader)
     */
    void onFailure(String type, String key, String value, int responseCode, String why);
  }

  /**
   * Uploader instance to do upload operation for a particular type. Should not be implemented
   * through inner class, cause it can't get new instance from reflection.
   */
  public interface Uploader {

    /**
     * Upload the data to the server. Developer should implement this interface to do the uploading
     * work.
     *
     * @see Tracker#track(String, String, String, String, net.gree.asdk.core.track.Tracker.Uploader)
     */
    void upload(String type, String key, String value, UploadStatus uploadStatus);
  }

  /**
   * Put task into database-backed queue and commit to server if network is available
   *
   * @param userId String value for the user id.
   * @param type String that identifies a particular type of logical object.
   * @param key Unique key of logical object.
   * @param value String value for the logical object. Given back to uploader.
   * @param uploader The implementation of {@link Uploader}
   *
   * @throws NullPointerException if uploader is null
   * @throws IllegalArgumentException if andy of type and key is empty
   */
  public void track(String userId, String type, String key, String value, Uploader uploader) {
    Preconditions.checkNotEmpty(type, "Type is empty");
    Preconditions.checkNotEmpty(key, "Key is empty");
    Preconditions.checkNotNull(uploader, "Uploader is null");

    GLog.d(TAG, "Track type: " + type + ", key: " + key + ", value: " + value + ", user id: "
        + userId + ", uploader: " + uploader.getClass().getName());

    String uploadClzName = uploader.getClass().getName();
    if (!uploaderMap.containsKey(uploadClzName)) {
      uploaderMap.put(uploader.getClass().getName(), uploader);
    }
    TrackItem item = new TrackItem(userId, type, key, value, mixer, uploadClzName);
    item.setStorage(storage);
    trackQueue.put(item);
    trackQueue.commit();
  }

  /**
   * Commit the track task and upload the data to server if network is aviliable
   */
  public void commit() {
    trackQueue.commit();
  }


  /**
   * Allows app developer to set a string that is used to compute hashes of valid Tracker database
   * entries.
   *
   * @param text String that's used to generate the seal of {@link TrackItem}
   */
  public void setMixer(String text) {
    if (text != null) {
      mixer = text;
    }
  }

  /**
   * Set the retry depay time
   *
   * @param retryInMs The millisecond of delay time
   *
   * @throws IllegalArgumentException if retryInMs is less than 0
   */
  public void setRetryDelay(int retryInMs) {
    if (retryInMs <= 0) {
      throw new IllegalArgumentException("retryInMs must > 0");
    }
    retryDelay = retryInMs;
  }

  /**
   * Get the retry delay time
   *
   * @return The millisecond of delay time
   */
  public int getRetryDelay() {
    return retryDelay;
  }

  /**
   * Set the max retry count
   *
   * @param maxRetryCount The max retry count
   *
   * @throws IllegalArgumentException if maxRetryCount is less than 0
   */
  public void setMaxRetryCount(int maxRetryCount) {
    if (maxRetryCount < 0) {
      throw new IllegalArgumentException("maxRetryCount must >= 0");
    }
    this.maxRetryCount = maxRetryCount;
  }

  /**
   * Get the max retry count
   *
   * @return The max retry count
   */
  public int getMaxRetryCount() {
    return maxRetryCount;
  }

  // Just for unit test
  void registerUploader(Uploader uploader) {
    uploaderMap.put(uploader.getClass().getName(), uploader);
  }

  /**
   * Database-backed BlockingQueue with one thread consumer that processes the track item by FIFO
   * order.
   */
  private class TrackQueue {
    private final static String TAG = "Tracker.TrackQueue";

    // FIFO blocking queue
    private final BlockingQueue<TrackItem> queue = new LinkedBlockingQueue<TrackItem>();

    // Track item consumer for queue
    private final TrackItemConsumer trackItemConsumer = new TrackItemConsumer(queue);

    // Single thread pool that make sure there's only one thread to load pending track items
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    // Has loaded pending upload or not
    private CountDownLatch hasLoadedPendingItems = new CountDownLatch(1);

    /**
     * Load pending upload in a background thread, cuase disk access on UIThread isn't allowed in
     * StrictMode
     */
    private TrackQueue() {
      executor.execute(new Runnable() {
        @Override
        public void run() {
          try {
            List<TrackItem> pendingUploads = storage.findAndCheckPendingUpload();
            if (pendingUploads.isEmpty()) {
              return;
            }
            for (TrackItem item : pendingUploads) {
              try {
                queue.put(item);
              } catch (InterruptedException e) {
                GLog.e(TAG, e.getMessage());
              }
            }
          } finally {
            hasLoadedPendingItems.countDown();
          }
        }
      });
    }

    private void put(TrackItem item) {
      waitToLoadPendingItems();
      try {
        item.save();
        queue.put(item);
      } catch (InterruptedException e) {
        GLog.e(TAG, "put: " + e.getMessage());
      }
    }

    private void commit() {
      waitToLoadPendingItems();
      doCommit();
    }

    private void doCommit() {
      GLog.d(TAG, "Started submitting a task to process");
      if (!connectivityManager.checkConnectivity()) {
        GLog.d(TAG, "Could not submit a new task, because network is aviliable");
        return;
      }
      GLog.d(TAG, "Submit a new consumer task");
      executor.execute(trackItemConsumer);
    }

    private void waitToLoadPendingItems() {
      try {
        hasLoadedPendingItems.await(60, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        GLog.e(TAG, "Failed to load pending track items in 60 seconds" + e.getMessage());
      }
    }

    /**
     * The consumer that processes the track items in the queue.
     */
    private class TrackItemConsumer implements Runnable {
      private final static String TAG = "Tracker.TrackQueue.TrackItemConsumer";

      private final BlockingQueue<TrackItem> queue;
      private int retryCount;
      private int retryDelay;

      private TrackItemConsumer(BlockingQueue<TrackItem> queue) {
        this.queue = queue;
        this.retryCount = 0;
        this.retryDelay = Tracker.this.retryDelay;
      }

      @Override
      public void run() {
        try {
          TrackItem item = queue.peek();
          if (null == item) {
            // TrackItemConsumer task will finish cause queue is empty
            GLog.d(TAG, "Queue is empty.");
            return;
          }
          process(item);
        } catch (Exception e) {
          GLog.e(TAG, e.getMessage());
        }
      }

      private void process(final TrackItem item) {
        GLog.d(TAG, "Started processing: " + item.toString());

        if (!connectivityManager.checkConnectivity()) {
          GLog.d(TAG, "Could not process this item cause network disconnected");
          resetRetry();
          // TrackItemConsumer task will finish cause network disconnected
          return;
        }

        if (!item.userId.equals(GreePlatform.getLocalUserId())) {
          GLog.w(TAG, "This item doesn't belong to current user.");
          processNextItem(item);
          return;
        }

        Uploader uploader = getUploader(item.uploaderClzName);
        if (null == uploader) {
          GLog.w(TAG, "Could not process this item No uploader for " + item.uploaderClzName);
          processNextItem(item);
          return;
        }

        uploader.upload(item.type, item.key, item.data, new UploadStatus() {
          @Override
          public void onSuccess(String type, String key, String value) {
            GLog.d(TAG, "Successfully processed: " + item.toString());
            processNextItem(item);
          }

          @Override
          public void onFailure(String type, String key, String value, int responseCode, String why) {
            if (needRetry(responseCode, why)) {
              GLog.d(TAG, "Failed to process: " + item.toString() + ", will retry after "
                  + retryDelay + "ms");
              new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                  retryDelay *= 2;
                  if (++retryCount > maxRetryCount) {
                    GLog.w(TAG, "Failed process data after retried " + maxRetryCount + " times");
                    processNextItem(item);
                  } else {
                    GLog.d(TAG, "Retring " + retryCount + " time(s)");
                    process(item);
                  }
                }
              }, retryDelay);
            } else {
              GLog.d(TAG, "Failed to process: " + item.toString());
              processNextItem(item);
            }
          }

          private boolean needRetry(int code, String why) {
            return maxRetryCount > 0
                && (code >= 500 || (code >= 400 && (why == null || why.length() == 0 || why
                    .equals(Constants.DisabledMessage))));

          }
        });
      }

      private void processNextItem(TrackItem currentItem) {
        resetRetry();
        removeCurrentItem(currentItem);
        if (!queue.isEmpty()) {
          GLog.d(TAG, "Started processing next item of " + currentItem);
          doCommit();
        }
      }

      private void removeCurrentItem(TrackItem item) {
        GLog.d(TAG, "Remove item from queue and db: " + item);
        queue.poll();
        item.delete();
      }

      private void resetRetry() {
        GLog.d(TAG, "Reset retry count and delay");
        retryCount = 0;
        retryDelay = Tracker.this.retryDelay;
      }

      private Uploader getUploader(String clzName) {
        Uploader uploader = uploaderMap.get(clzName);
        if (null == uploader) {
          uploader = newUploaderInstance(clzName);
          if (null != uploader) {
            uploaderMap.put(clzName, uploader);
          }
        }
        return uploader;
      }

      private Uploader newUploaderInstance(String clz) {
        try {
          GLog.d(TAG, "newUploaderInstance from " + clz);
          Class cls = Class.forName(clz);
          Constructor[] ctors = cls.getDeclaredConstructors();
          Constructor ctor = null;
          for (Constructor ct : ctors) {
            ctor = ct;
            if (ctor.getGenericParameterTypes().length == 0) {
              break;
            }
          }
          if (null == ctor) {
            GLog.w(TAG, "Can't find default constructor for " + clz);
            return null;
          }
          boolean isAccessible = ctor.isAccessible();
          try {
            if (!isAccessible) {
              ctor.setAccessible(true);
            }
            ctor.setAccessible(true);
            return (Uploader) ctor.newInstance();
          } finally {
            if (!isAccessible) {
              ctor.setAccessible(false);
            }
          }
        } catch (Exception e) {
          GLog.w(TAG, "newUploaderInstance from " + clz + " error: " + e.getMessage());
          return null;
        }
      }
    }
  }
}
