package net.gree.asdk.core.externalsocialgraph;

import org.apache.http.HeaderIterator;

public abstract class Listeners {

  /**
   * The listener for getting the list of external friends
   */
  public interface ExternalFriendsListener {
    /**
     * Received a list of friends for the given application ID
     * @param startIndex the index (from 1)
     * @param hasMore true if there is more ExternalFriend to be downloaded
     * @param limit the maximum number of ExternalFriend that be retrieved in one request
     * @param friends a list of InGameFriend
     */
    void onSuccess(int startIndex, boolean hasMore, int limit, ExternalFriend[] friends);
    /**
     * Called when an error occurred in trying to get the list of in game friends.
     * @param responseCode Integer representing the http response code.
     * @param headers Object representing the headers of the http response.
     * @param response String representing the body of the http response.
     */
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }
  
}
