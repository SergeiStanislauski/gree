package net.gree.asdk.core.auth;

import android.app.Activity;
import net.gree.asdk.api.auth.Authorizer;
import net.gree.oauth.signpost.exception.OAuthCommunicationException;
import net.gree.oauth.signpost.exception.OAuthExpectationFailedException;
import net.gree.oauth.signpost.exception.OAuthMessageSignerException;

import android.content.Context;

import org.apache.http.client.methods.HttpUriRequest;



public interface IAuthorizer {

  boolean isAuthorized();
  
  void authorize(Context context, String serviceCode, Authorizer.AuthorizeListener listener,
      Authorizer.UpdatedLocalUserListener localuser_listener);
  
  void authorize(Context context, String serviceCode, Authorizer.AuthorizeListener listener,
                Authorizer.UpdatedLocalUserListener localuser_listener, int user_grade);

  void reauthorize(Context context, Authorizer.AuthorizeListener listener);

  boolean logout();
  
  void logout(Context context, Authorizer.LogoutListener logoutListener,
      Authorizer.AuthorizeListener loginListener,
      Authorizer.UpdatedLocalUserListener localuser_listener);
  
  void directLogout(Activity activity, Authorizer.LogoutListener logoutListener, 
             boolean autoRestart);

  void upgrade(Context context, int targetGrade, String serviceCode,
      Authorizer.UpgradeListener listener, Authorizer.UpdatedLocalUserListener localuser_listener);

  boolean hasOAuthAccessToken();

  String getOAuthAccessToken();

  String getOAuthAccessTokenSecret();

  String getOAuthUserId();

  void signFor2Legged(HttpUriRequest request) throws OAuthMessageSignerException,
      OAuthExpectationFailedException, OAuthCommunicationException;

  void signFor3Legged(HttpUriRequest request) throws OAuthMessageSignerException,
      OAuthExpectationFailedException, OAuthCommunicationException;

  void retrieveRequestToken(Context context, AuthorizerCore.OnOAuthResponseListener<String> listener);

  void retrieveAccessToken(Context context, String url, AuthorizerCore.OnOAuthResponseListener<Void> listener);

  void clearOAuth();

  void clearRequestTokenParams();

  void clearAccessTokenParams();

  void setTokenWithSecret(String token, String tokenSecret);
}
