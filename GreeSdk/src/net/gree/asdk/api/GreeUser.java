/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import org.apache.http.HeaderIterator;

import android.graphics.Bitmap;
import android.text.TextUtils;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.cache.ImageFetcher;
import net.gree.asdk.core.ui.AvatarView.AvatarView.AvatarGif;
import net.gree.asdk.core.updateprofile.ProfileUpdater;
import net.gree.asdk.core.updateprofile.ProfileUpdater.UpdateListener;
import net.gree.asdk.core.socialgraph.request.IgnoredUsersRequest;
import net.gree.asdk.core.socialgraph.request.PeopleRequest;

/**
 * Implements GreeUser API, a la OpenSocial People API.
 * @see <a href="http://opensocial-resources.googlecode.com/svn/spec/0.9/REST-API.xml#rfc.section.7.1">opensocial rfc section 7.1</a>
 * @author GREE, Inc.
 */
public class GreeUser {
  private static final String TAG = "GreeUser";
  private static boolean isDebug = false;
  private static boolean isVerbose = false;

/**
 * Small size: 25x25.
 */
  public static final int THUMBNAIL_SIZE_SMALL = 0;

/**
 * Standard size: 48x48.
 */
  public static final int THUMBNAIL_SIZE_STANDARD = 1;

/**
 * Large size: 76x48.
 */
  public static final int THUMBNAIL_SIZE_LARGE = 2;

/**
 * Huge size: 190x120.
 */
  public static final int THUMBNAIL_SIZE_HUGE = 3;
  
/**
 * XLarge size: 160x160.
 */
  public static final int THUMBNAIL_SIZE_XLARGE = 4;

 
  /**
  * Load the thumbnail, asynchronize call,none-blocking, the listener will be called back when the request is done
  * pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @param size specify size of thumbnail
  * @param listener to notify when the icon is loaded
  * @return return false means the request could not fulfill(no listener,network error,permission error, etc).
  */
	public boolean loadThumbnail(final int size, final IconDownloadListener listener) {
	  // update the values incase it has changed
	  updateSelfAndLoad(listener);
	  // switch on the size and fetch the thumb from the cache/disk/internet
	  switch (size) {
		case THUMBNAIL_SIZE_SMALL:
			getImageFetcher().loadImage(thumbnailUrlSmall, listener);
			break;
		case THUMBNAIL_SIZE_LARGE:
			getImageFetcher().loadImage(thumbnailUrlLarge, listener);
			break;
		case THUMBNAIL_SIZE_XLARGE:
			getImageFetcher().loadImage(thumbnailUrlXlarge, listener);
			break;
		case THUMBNAIL_SIZE_HUGE:
			getImageFetcher().loadImage(thumbnailUrlHuge, listener);
			break;
		case THUMBNAIL_SIZE_STANDARD:
		default:
			getImageFetcher().loadImage(thumbnailUrl, listener);

		}
	  return true;
	}


  /**
   * Gets the thumbnail, synchronized call, return immediately.
   * @return if the thumbnail is ready, return the bitmap,otherwise return null
   * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
   */
  public Bitmap getThumbnail() {
    return getImageFetcher().getBitmap(thumbnailUrl);// TODO Possibly call load if it is null?

  }

  /**
   * Gets the thumbnail, synchronized call, return immediately.
   * @return if the thumbnail is ready, return the bitmap,otherwise return null
   * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
   */
  public Bitmap getSmallThumbnail() {
	return getImageFetcher().getBitmap(thumbnailUrlSmall);// TODO Possibly call load if it is null?
  }

   /**
   * Gets the large thumbnail, synchronized call, return immediately.
   * @return if the large thumbnail is ready, return the bitmap,otherwise return null
   * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
   */
  public Bitmap getLargeThumbnail() {
    return getImageFetcher().getBitmap(thumbnailUrlLarge);// TODO Possibly call load if it is null?
  }

   /**
   * Gets the huge thumbnail, synchronized call, return immediately.
   * @return if the huge thumbnail is ready, return the bitmap,otherwise return null
   * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
   */
  public Bitmap getHugeThumbnail() {
    return getImageFetcher().getBitmap(thumbnailUrlHuge);// TODO Possibly call load if it is null?
  }

  /**
  * Load the thumbnail, asynchronize call,none-blocking, the listener will be called back when the request is done
  * pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @param listener to notify when the icon is loaded
  * @return return false means the request could not fulfill(no listener,network error,permission error, etc).
  * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
  */
  public boolean loadThumbnail(final IconDownloadListener listener) {
    return loadThumbnail(THUMBNAIL_SIZE_STANDARD, listener);
  }

  /**
  * Load the small thumbnail, asynchronize call, non-blocking, the listener will be called back when the request is done
  * pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @param listener to notify when the icon is loaded
  * @return return false means the request could not fulfill(no listener,network error,permission error, etc).
  * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
  */
  public boolean loadSmallThumbnail(final IconDownloadListener listener) {
    return loadThumbnail(THUMBNAIL_SIZE_SMALL, listener);
  }

  /**
  * Load the large thumbnail, asynchronize call, non-blocking, the listener will be called back when the request is done
  * pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @param listener to notify when the icon is loaded
  * @return return false means the request could not fulfill(no listener,network error,permission error, etc).
  * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
  */
  public boolean loadLargeThumbnail(final IconDownloadListener listener) {
    return loadThumbnail(THUMBNAIL_SIZE_LARGE, listener);
  }

 
  /**
  * Load the large thumbnail, asynchronize call, non-blocking, the listener will be called back when the request is done
  * pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @param listener to notify when the icon is loaded
  * @return return false means the request could not fulfill(no listener,network error,permission error, etc).
  * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
  */
  public boolean loadHugeThumbnail(final IconDownloadListener listener) {
    return loadThumbnail(THUMBNAIL_SIZE_HUGE, listener);
  }

 /**
  * Sets the debug flag. This enables and disables debug logging.
  * @param debug if set to true, turns this on; false turns this off
  */
  public static void setDebug(boolean debug) {
    GreeUser.isDebug = debug;
  }

 /**
  * Sets the verbose debug flag. This enables and disables verbose logging.
  * @param verbose if set to true, turns this on; false turns this off
  */
  public static void setVerbose(boolean verbose) {
    GreeUser.isVerbose = verbose;
  }

  /**
   * Default constructor.
   */
  private GreeUser() { }

  /**
   * Interface definition used for the delivery of user results.<br>
   * This listener is used when calling {@link #loadFriends(int offset, int count, GreeUserListener listener)} 
   * or {@link #loadUserWithId(String pid, GreeUserListener listener)}
   */
  public interface GreeUserListener {
 
   
      /**
       * Called upon successful retrieval of the list of users.
       * @param index Integer representing the index in the list of users.
       * @param count Integer representing the total number of users.
       * @param users String Array containing the list of users.
       */
   
    void onSuccess(int index, int count, GreeUser[] users);

   
   
      /**
       * Called when something went wrong when trying to get the list of users.
       * @param responseCode Integer representing the http response code.
       * @param headers Object representing the headers of the http response.
       * @param response String representing the body of the http response.
       */
   
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Interface definition for delivery of ignored users.
   * This listener is used when calling {@link #loadIgnoredUserIds(int offset, int count, GreeIgnoredUserListener listener)} 
   * or {@link #isIgnoringUserWithId(String pid, GreeIgnoredUserListener listener)}
   */
  public interface GreeIgnoredUserListener {
 
 
    /**
     * Called upon successful retrieval of the list of ignored users.
     * @param index Integer representing the index in the list of ignored users.
     * @param count Integer representing the total number of ignored users.
     * @param ignoredUsers String Array containing the list of ignored users.
     */
 
    void onSuccess(int index, int count, String[] ignoredUsers);

   
   
      /**
       * Called when an error occurred in trying to get the list of ignored users.
       * @param responseCode Integer representing the http response code.
       * @param headers Object representing the headers of the http response.
       * @param response String representing the body of the http response.
       */
   
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  private String id;
  private String nickname;
  private String displayName;
  private String userGrade;
  private String region;
  private String subregion;
  private String language;
  private String timezone;
  private String aboutMe;
  private String birthday;
  private String profileUrl;
  private String thumbnailUrl;
  private String thumbnailUrlSmall;
  private String thumbnailUrlLarge;
  private String thumbnailUrlXlarge;
  private String thumbnailUrlHuge;
  private String gender;
  private String age;
  private String bloodType;
  private String hasApp;
  private String userHash;
  private String userType;
  private String[] userSpecified;

  /**
   * Loads a collection of all the friends of current user;
   * @param offset the number offset in the list of friends
   * @param count the number of friends to fetch
   * @param listener the listener to be notified of the results
   */
  public void loadFriends(int offset, int count, final GreeUserListener listener) {
    // /people/{guid}/@friends Collection of all friends of user {guid}; subset of @all
    String action = "/people/" + getId() + "/@friends";
    if (isDebug) {
      GLog.d(TAG, action);
    }
    PeopleRequest request = new PeopleRequest();
    request.getPeople(action, offset, count, listener);
  }

  /**
   * Loads the user information for a given userId. 
   * @param pid the user id to be used 
   * @param listener the listener to be notified of the results
   */
  public static void loadUserWithId(String pid, final GreeUserListener listener) {
    loadUserWithId(pid, false, listener);
  }

  /**
   * Loads the user information for a given userId. 
   * @param pid the user id to be used 
   * @param isLimitedInfo flag for showing full user info or limited. (limited can be used for non-friend/no hasApp users)
   * @param listener the listener to be notified of the results
   */
 public static void loadUserWithId(String pid, boolean isLimitedInfo, final GreeUserListener listener) {
   // /people/{guid}/@all/{pid} Individual user record for a specific user known to {guid};
   // shows {guid}'s view of {pid}.
   // /people/{guid}/@self Profile record for user {guid}
   String action = null;
   String uid = pid;
   if (pid == null || pid.length() == 0) {
     uid = "@me";
   }
   action = "/people/" + uid + "/@self";
   if (isDebug) {
     GLog.d(TAG, action);
   }
   PeopleRequest request = new PeopleRequest();
   request.getPeople(action, isLimitedInfo, 1, 1, listener);
 }


  /**
   * Loads the list of ignored user, returns them as a list of ids
   * @param offset the offset in the list of ignored users
   * @param count the number of user to get
   * @param listener the listener to be notified of the results
   */
  public void loadIgnoredUserIds(int offset, int count, final GreeIgnoredUserListener listener) {
    String id = getId();
    if (TextUtils.isEmpty(id)) {
      GLog.e(TAG, "Instance of User don't have id.");
      return;
    }
    IgnoredUsersRequest request = new IgnoredUsersRequest();
    request.getPeople("/ignorelist/"+id+"/@all", offset, count, listener);
  }

  /**
   * Determines if the receiver is ignoring a given user by obtaining a JSON object containing the deterministic values
   * <br>Returns the id of the ignored user (if the user is in the ignored list)
   * <br>Returns an empty string otherwise (if the user is not in the ignored list)
   * @param pid user id to verify 
   * @param listener the listener to be notified of the results
   */
  public void isIgnoringUserWithId(String pid, GreeIgnoredUserListener listener) {
    String id = getId();
    if (TextUtils.isEmpty(id)) {
      GLog.e(TAG, "Instance of User don't have id.");
      return;
    }
    IgnoredUsersRequest request = new IgnoredUsersRequest();
    request.getPeople("/ignorelist/"+id+"/@all/" + pid, -1, -1, listener);
  }
  
 /**
  * Creates an instance of the AvatarGif class based on the information provided in this GreeUser class.
  * Exposing this information via this object only allows us to hide properly hide this info and expose it where
  * required.
  * @param imageSize the int expressing one of the 4 sizes exposed to Avatar image
  * @return a AvatarGif object. This objects encapsulate all our foundations for the animated gif (Image URL, height, and width).
  */
  public AvatarGif createAvatarGif(int imageSize) {
	  // small thumbnail height and width
	  final int smallThumbHW = 25;
	  // large thumbnail height and width
	  final int largeThumbH = 76;
	  final int largeThumbW = 48;
	  // xlarge thumbnail height and width
	  final int xlargeThumbHW = 160;
	  // huge thumbnail height and width
	  final int hugeThumbH = 190;
	  final int hugeThumbW = 120;
	  // standard thumbnail height and width
	  final int standardThumbHW = 48;

	  // build based on size passed in
	  switch(imageSize){
	  case THUMBNAIL_SIZE_SMALL:
		  return new AvatarGif(thumbnailUrlSmall, smallThumbHW, smallThumbHW);
	  case THUMBNAIL_SIZE_LARGE:
		  return new AvatarGif(thumbnailUrlLarge,largeThumbH,largeThumbW);
	  case THUMBNAIL_SIZE_XLARGE:
		  return new AvatarGif(thumbnailUrlXlarge,xlargeThumbHW,xlargeThumbHW);
	  case THUMBNAIL_SIZE_HUGE:
		  return new AvatarGif(thumbnailUrlHuge,hugeThumbH,hugeThumbW);
	  case THUMBNAIL_SIZE_STANDARD:
	  default:
		  return new AvatarGif(thumbnailUrl,standardThumbHW,standardThumbHW);
	  }
  }

   
   
    /**
     * Logs based on the giver values
     * @param label String representing the log's label.
     * @param value String representing the value to be logged.
     */
   
  public interface LogLabelValue {
   
   
    /**
     * Logs based on the giver values
     * @param label String representing the log's label.
     * @param value String representing the value to be logged.
     */
   
    void log(String label, String value);
  }

   
  
  /**
   * Logs users with the default simple logging for the list of users passed in. 
   * @param index the index parameter for the log
   * @param count the count parameter 
   * @param users the list of users 
   */
  public static void logGreeUser(int index, int count, GreeUser[] users) {
    logGreeUser(index, count, users, new LogLabelValue() {
      public void log(String l, String v) {
        logn(l, v);
      }
    });
  }

  /**
   * Log users with the default simple logging for the list of users passed in. 
   * @param index the index parameter 
   * @param count the count parameter 
   * @param users the list of users 
   * @param llv the interface to be called by this method
   */
  public static void logGreeUser(int index, int count, GreeUser[] users, LogLabelValue llv) {
    llv.log("GreeUser", "" + index + " count:" + count);
    for (int i = 0; i < users.length; i++) {
      llv.log("Userf", "" + i);
      GreeUser peep = users[i];
      llv.log("id", peep.id);
      llv.log("nickname", peep.nickname);
      llv.log("displayName", peep.displayName);
      llv.log("userGrade", peep.userGrade);
      llv.log("region", peep.region);
      llv.log("subregion", peep.subregion);
      llv.log("language", peep.language);
      llv.log("timezone", peep.timezone);
      llv.log("aboutMe", peep.aboutMe);
      llv.log("birthday", peep.birthday);
      llv.log("profileUrl", peep.profileUrl);
      llv.log("thumbnailUrl", peep.thumbnailUrl);
      llv.log("thumbnailUrlSmall", peep.thumbnailUrlSmall);
      llv.log("thumbnailUrlLarge", peep.thumbnailUrlLarge);
      llv.log("thumbnailUrlXlarge", peep.thumbnailUrlXlarge);
      llv.log("thumbnailUrlHuge", peep.thumbnailUrlHuge);
      llv.log("gender", peep.gender);
      llv.log("age", peep.age);
      llv.log("bloodType", peep.bloodType);
      llv.log("hasApp", peep.hasApp);
      llv.log("userHash", peep.userHash);
      llv.log("userType", peep.userType);
    }
  }

  /**
   * Convenience method for simple logging.
   * @param msg the 
   * @param val
   */
  private static void logn(String msg, String val) {
    if (val != null && val.length() > 0) {
      debug(msg + ":" + val);
    }
  }

  private static void debug(String msg) {
    GLog.d(TAG, msg);
  }

 /**
  * Gets the user ID for the current user.
  * @return Returns the alphanumeric user ID for the current user
  */
  public String getId() {
    return id;
  }

 /**
  * Gets the nickname of the user.
  * @return the user's nickname
  */
  public String getNickname() {
    return nickname;
  }

  /**
   * Interface definition used for the delivery of nickname registration results.<br>
   * This listener is used when calling {@link #registerNickname(String newNickname, RegisterNicknameListener listener)}
   */
  public interface RegisterNicknameListener {
 
   
      /**
       * Called upon successful nickname registration.
       * The GreeUser.nickname value is also updated automatically.
       * @param newNickname the new registered nickname
       */
   
    void onSuccess(String newNickname);

   
   
      /**
       * Called when something went wrong when trying to register a new nickname.
       * @param responseCode Integer representing the http response code.
       * @param headers Object representing the headers of the http response.
       * @param response String representing the body of the http response.
       */
   
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }
  
 /**
  * This call will test if this user's nickname as been registered on the Gree Server.
  * This is to be used with the {@link registerNickname} method.
  * This has meaning only for lite users.
  * @return true if the nickname has already been registered, false otherwise.
  */
  public boolean isNicknameRegistered() {
    if (userSpecified == null) {
      return false;
    }
    for (int i = 0; i < userSpecified.length; i++) {    
      if ("nickname".equals(userSpecified[i])) {
        return true;
      }
    }
    return false;
  }

 /**
  * This call will register this user's new nickname on the Gree Server.
  * This can only be done once per user.
  * This can only be done for grade 1 users. (when their nickname is PlayerXXXXX)
  * @param newNickname the user's new nickname
  * @param listener the listener to receive the server response.
  * @return the user's nickname
  */
  public void registerNickname(final String newNickname, final RegisterNicknameListener listener) {
    new ProfileUpdater().register(newNickname, new UpdateListener() {
      @Override
      public void onSuccess() {
        nickname = newNickname;
        displayName = newNickname;
        if (listener != null) {
          listener.onSuccess(newNickname);
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

 /**
  * Gets the display name of the user. <br>
  * This value is the same as the nickname value. 
  * @return The display name
  */
  public String getDisplayName() {
    return displayName;
  }

 /**
  * A lite user who has not been registered with a username and password.
  * Limited users have no API restrictions.
  */
  public static final int USER_GRADE_LITE = 1;

 /**
  * A limited user who has been registered with a username and password.
  * Limited users have no API restrictions.
  */
  public static final int USER_GRADE_LIMITED = 2;

 /**
  * A standard user who has been registered with a username and password and has also been verified.
  * Standard users have no API restrictions, thus, the Platform SDK makes no functional distinction between GreeUserGradeLimited and GreeUserGradeStandard.
  */
  public static final int USER_GRADE_STANDARD = 3;

 /**
  * This user's grade.
  * A user's grade determines what services he/she has access to.
  * Certain API actions may trigger a prompt asking the user to upgrade.
  * @return one of USER_GRADE_LITE, USER_GRADE_LIMITED, USER_GRADE_STANDARD
  */
  public int getUserGrade() {
    int ug = USER_GRADE_LITE;
    try {
      ug = Integer.valueOf(userGrade);
    } catch (NumberFormatException nfe) {
      GLog.e(TAG, "Invalid user grade : " + userGrade);
      GLog.printStackTrace(TAG, nfe);
    }
    if ((ug != USER_GRADE_LITE) && (ug != USER_GRADE_LIMITED) && (ug != USER_GRADE_STANDARD)) {
      ug = USER_GRADE_LITE;
      GLog.e(TAG, "Invalid user grade : " + userGrade);
    }
    return ug;
  }

 /**
  * Gets the user's country of registration.
  * (i.e. US, JP, etc.)
  * @return the user's country
  */
  public String getRegion() {
    return region;
  }

 /**
  * Gets the user's state of registration.
  * (i.e. CA, NM, AZ, NY, etc.)
  * @return the user's state 
  */
  public String getSubregion() {
    return subregion;
  }

 /**
  * Gets the current user's local language.<br>
  * (i.e. jpn-Jpan-JP)
  * @return the user's language
  */
  public String getLanguage() {
    return language;
  }

 /**
  * Gets the user's local time zone.
  * @return the user's local time zone
  */
  public String getTimezone() {
    return timezone;
  }

 /**
  * Gets the user defined introductory text.
  * @return A String containing the user-entered introductory text.
  */
  public String getAboutMe() {
    return aboutMe;
  }

 /**
  * Gets the user defined date field of his/her birthday. 
  * @return String field representing the user's birthday. 
  */
  public String getBirthday() {
    return birthday;
  }

 /**
  * @deprecated This field will be removed from next version
  */
  public String getProfileUrl() {
    return null;
  }

 /**
  * Gets the gender of this user.
  * @return The gender the user selected.
  */
  public String getGender() {
    return gender;
  }

 /**
  * Gets the user defined value of his/her age.
  * @return An integer representing the age of the current user. 
  */
  public String getAge() {
    return age;
  }

 /**
  * Gets the blood type value of this user.
  * @return A String representing the blood type of this user. 
  */
  public String getBloodType() {
    return bloodType;
  }

 /**
  * Gets a boolean determinig whether the user has installed the current application or not.
  * @return true if the receiver has your application, false otherwise.
  */
  public boolean getHasApp() {
    return Boolean.valueOf(hasApp);
  }

 /**
  * Gets the user's hash of this user. <br>
  * Can be used for incentive services.
  * @return The user hash of this user. 
  */
  public String getUserHash() {
    return userHash;
  }

 /**
  * Gets the user type of this user.
  * <p>Use this field to distinguish campaign accounts such as celebrity accounts from standard users.<br>
  * When providing functions aimed at standard users, such as inviting friends, etc., 
  * change the handling method depending on the value in the userType field.</p>
  * <i> "special" means A campaign account set up for promotional use within GREE.</BR>
  * "staff" means A staff account.</BR>
  * "official" means An official account within GREE.</BR></i>
  * Standard user is an empty string.</BR>
  * 
  * @return The user type of this user. 
  */
  public String getUserType() {
    return userType;
  }

  private static ImageFetcher mImageFetcher;
  
  /**
   * Creating a new static instance of the Image Fetcher
   */
  private static ImageFetcher getImageFetcher(){
	if (null == mImageFetcher) {
		mImageFetcher = Injector.getInstance(ImageFetcher.class);
	}
	  return mImageFetcher; 
  }
  
  /**
   * Check if our user information is not too old,
   * if it is, then get the thumbnails Url from the server, update the current instance,
   * then start loading the thumbnail.
   * 
   * @param thumbnailLoader what thumbnail to load
   * @param listener the external IconDownloadListener listener
   */
  private void updateSelfAndLoad(final IconDownloadListener listener) {
    if (isVerbose) {
      GLog.v(TAG, "Update user");
    }
    //Prepare the listener
    GreeUserListener userListener = new GreeUserListener() {
      @Override
      public void onSuccess(int index, int count, GreeUser[] users) {
        if (isVerbose) {
          GLog.v(TAG, "Update user Success, try to get Thumbnail");
        }

        //update current instance with the new urls, if different
        if(!users[0].thumbnailUrl.equals(GreeUser.this.thumbnailUrl)){
        	GreeUser.this.thumbnailUrl = users[0].thumbnailUrl;
        }
        if(!users[0].thumbnailUrlSmall.equals(GreeUser.this.thumbnailUrlSmall)){
        	GreeUser.this.thumbnailUrlSmall = users[0].thumbnailUrlSmall;
        }
        if(!users[0].thumbnailUrlLarge.equals(GreeUser.this.thumbnailUrlLarge)){
        	GreeUser.this.thumbnailUrlLarge = users[0].thumbnailUrlLarge;
        }
        if(!users[0].thumbnailUrlXlarge.equals(GreeUser.this.thumbnailUrlXlarge)){
        	GreeUser.this.thumbnailUrlXlarge = users[0].thumbnailUrlXlarge;
        }
        if(!users[0].thumbnailUrlHuge.equals(GreeUser.this.thumbnailUrlHuge)){
        	GreeUser.this.thumbnailUrlHuge = users[0].thumbnailUrlHuge;
        }
        
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (isVerbose) {
          GLog.v(TAG, "Failed to get User, will not try again to get Thumbnail");
        }
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    };

    String localUserId = GreePlatform.getLocalUserId(); 
    if ((localUserId != null) && (localUserId.equals(id))) {
      //update local user
      Core.getInstance().updateLocalUser(userListener);
    } else {
      //update current user
      GreeUser.loadUserWithId(id, (userGrade == null), userListener);
    }
  }
}
