/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core;

import org.apache.http.HeaderIterator;

import net.gree.asdk.core.auth.AuthorizeContext;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Url;
import net.gree.vendor.com.google.gson.Gson;

public class DeviceUser {
  private static final String TAG = DeviceUser.class.getSimpleName();

  public static final int AUTH_BIT_UDID = 0x0001;
  public static final int AUTH_BIT_MAIL = 0x0002;
  public static final int AUTH_BIT_TELNO = 0x0004;

  private static class Response {
    public DeviceUser[] entry;
    public int totalResults;
  }

  private String id;
  private String userGrade;
  private String nickname;
  private String birthday;
  private String profileUrl;
  private String authBit;

  public String getId() { return id; }
  public String getUserGrade() { return userGrade; }
  public String getNickName() { return nickname; }
  public String getBirthDay() { return birthday; }
  public String getProfileUrl() { return profileUrl; }
  public String getAuthBit() { return authBit; }

  /**
   * Interface for listening to results for requesting account list related to the device
   */
  public interface DeviceUserListener {
    public void onSuccess(int totalResults, DeviceUser[] users);
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Trigger the requests to get account list related to the device
   * @param listener
   */
  public static void load(final DeviceUserListener listener) {
    String url = AuthorizeContext.appendQueryParameter(Url.getFindUsersByDevice());
    new JsonClient().oauth2(url, BaseClient.METHOD_GET, null, false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        try {
          Response res = Injector.getInstance(Gson.class).fromJson(response, Response.class);
          if (listener != null) {
            listener.onSuccess(res.totalResults, res.entry);
          }
        } catch(Exception e) {
          GLog.printStackTrace(TAG, e);
          onFailure(responseCode, headers, response);
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  private DeviceUser() { }
}
