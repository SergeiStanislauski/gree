/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

/**
 * This class is used for detecting swipes (In the photoview for example), regardless of android version.
 */
public abstract class VersionedGestureDetector {
  OnGestureListener mListener;

  /**
   * Create new instance.
   * @param context android.context.Context
   * @param listener listener which is receiving the result of gesture.
   * @return instance of VersionedGestureDetector
   */
  public static VersionedGestureDetector newInstance(Context context, OnGestureListener listener) {
    VersionedGestureDetector detector = null;
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
      detector = new OldDetector(context);
    } else {
      detector = new FroyoDetector(context);
    }

    detector.mListener = listener;
    return detector;
  }

  /**
   * Handle touch screen motion events.
   * @param e the current motion event.
   * @return true if the VersionedGestureDetector consumed the event, else false.
   */
  public abstract boolean onTouchEvent(MotionEvent e);

  /**
   * Returns true if scale feature is available.
   * @return true if scale feature is available, else false.
   */
  public abstract boolean isScaleGestureAvailable();

  /**
   * The listener that is used to notify when gestures occur.
   */
  public interface OnGestureListener {

    /**
     * Responds to single tapping events for a gesture.
     * @param e the current motion event.
     * @return true if the event is consumed, else false.
     */
    boolean onSingleTapUp(MotionEvent e);

    /**
     * Responds to scrolling events for a gesture in progress.
     * @param distanceX the distance of the x-axis.
     * @param distanceY the distance of the y-axis.
     * @return true if the event is consumed, else false.
     */
    boolean onScroll(float distanceX, float distanceY);

    /**
     * Responds to flinging events for a gesture.
     * @param velocityX the velocity of the x-axis.
     * @param velocityY the velocity of the y-axis.
     * @return true if the event is consumed, else false.
     */
    boolean onFling(float velocityX, float velocityY);

    /**
     * Responds to the beginning of a scaling gesture.
     * @param scaleFactor the scale factor.
     * @param focusX the focused x-axis
     * @param focusY the focused y-axis
     */
    void onScaleBegin(float scaleFactor, float focusX, float focusY);

    /**
     * Responds to scaling events for a gesture in progress.
     * @param scaleFactor scale factor.
     * @param focusX the focused x-axis
     * @param focusY the focused y-axis
     */
    void onScale(float scaleFactor, float focusX, float focusY);

    /**
     * Responds to the end of a scale gesture.
     * @param scaleFactor
     * @param focusX the focused x-axis
     * @param focusY the focused y-axis
     */
    void onScaleEnd(float scaleFactor, float focusX, float focusY);
  }

  /**
   * VersionedGestureDetector for Android 2.1 or less.
   */
  private static class OldDetector extends VersionedGestureDetector {
    private GestureDetector mGestureDetector;

    public OldDetector(Context context) {
      mGestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
        public boolean onSingleTapUp(MotionEvent e) {
          return mListener.onSingleTapUp(e);
        }

        public void onShowPress(MotionEvent e) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
          return mListener.onScroll(distanceX, distanceY);
        }

        public void onLongPress(MotionEvent e) {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
          return mListener.onFling(velocityX, velocityY);
        }

        public boolean onDown(MotionEvent e) {
          return false;
        }
      });
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
      return mGestureDetector.onTouchEvent(ev);
    }

    @Override
    public boolean isScaleGestureAvailable() {
      return false;
    }
  }

  @TargetApi(8)
  /**
   * VersionedGestureDetector for Android 2.2 or higher.
   */
  private static class FroyoDetector extends OldDetector {
    private ScaleGestureDetector mScaleDetector;

    public FroyoDetector(Context context) {
      super(context);

      mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
          mListener.onScaleBegin(detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
          return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
          mListener.onScale(detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
          return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
          mListener.onScaleEnd(detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
        }
      });
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
      mScaleDetector.onTouchEvent(ev);
      if (mScaleDetector.isInProgress()) {
        return true;
      }
      return super.onTouchEvent(ev);
    }

    @Override
    public boolean isScaleGestureAvailable() {
      return true;
    }
  }
}
