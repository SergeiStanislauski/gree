/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.widget;

import org.json.JSONException;
import org.json.JSONObject;

import net.gree.asdk.R;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.storage.JSONStorage;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.widget.RemoteViews;

/**
 * Widget service for less than ApiLevel 11
 * 
 * @author GREE, Inc.
 * 
 */
public class WidgetService extends Service {
  private static String TAG = "GreeWidgetService";
  private static String ACTION_WIDGET_TOUCH1 = "net.gree.asdk.intent.ACTION_WIDGET_TOUCH1";
  private static String ACTION_WIDGET_TOUCH2 = "net.gree.asdk.intent.ACTION_WIDGET_TOUCH2";
  private static String ACTION_WIDGET_TOUCH3 = "net.gree.asdk.intent.ACTION_WIDGET_TOUCH3";
  private static String VIEW_SELECTOR = "params";
  private static int mPosition = 0;
  private static String mMainLayout = "";
  private JSONStorage mWidgetData;
  private Cursor mCursor;

  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mWidgetData = Injector.getInstance(JSONStorage.class);
    mCursor =
        mWidgetData.getDataFilterCursor(JSONStorage.COLUM_FILTER + " = ?",
            new String[] {"widget_data"});
  }

  @Override
  public void onStart(Intent intent, int startId) {
    if (startId > 0) {
      mCursor =
          mWidgetData.getDataFilterCursor(JSONStorage.COLUM_FILTER + " = ?",
              new String[] {"widget_data"});
    }
    if (intent.getCharSequenceExtra("layout") != null) {
      mMainLayout = (String) intent.getCharSequenceExtra("layout");
    }
    RemoteViews remoteViews = new RemoteViews(getPackageName(), RR.layout(mMainLayout));
    RemoteViews hv = new RemoteViews(getPackageName(), R.layout.gree_widget_custom_header);
    remoteViews.addView(R.id.gree_widget_header, hv);

    Intent imageIntent = new Intent();
    imageIntent.setAction(ACTION_WIDGET_TOUCH1);
    imageIntent.putExtra(VIEW_SELECTOR, "image_button");
    PendingIntent iIntent = PendingIntent.getService(this, 0, imageIntent, 0);
    remoteViews.setOnClickPendingIntent(R.id.gree_widget_image_view, iIntent);

    Intent prevIntent = new Intent();
    prevIntent.setAction(ACTION_WIDGET_TOUCH2);
    prevIntent.putExtra(VIEW_SELECTOR, "prev_button");
    PendingIntent pIntent = PendingIntent.getService(this, 0, prevIntent, 0);
    remoteViews.setOnClickPendingIntent(R.id.gree_widget_prev_button, pIntent);

    Intent nextIntent = new Intent();
    nextIntent.setAction(ACTION_WIDGET_TOUCH3);
    nextIntent.putExtra(VIEW_SELECTOR, "next_button");
    PendingIntent nIntent = PendingIntent.getService(this, 0, nextIntent, 0);
    remoteViews.setOnClickPendingIntent(R.id.gree_widget_next_button, nIntent);

    if (intent.getAction() != null) {
      onClick(remoteViews, intent.getStringExtra(VIEW_SELECTOR));
    }

    ComponentName cn = new ComponentName(getPackageName(), WidgetProvider.class.getName());
    AppWidgetManager manager = AppWidgetManager.getInstance(this);
    manager.updateAppWidget(cn, remoteViews);
  }

  private void onClick(RemoteViews rv, String switchButton) {
    if (mCursor.getCount() <= 0) {
      return;
    }
    if (switchButton.equals("prev_button") && mPosition != 0) {
      mPosition--;
    } else if (switchButton.equals("next_button") && mPosition != mCursor.getCount() - 1) {
      mPosition++;
    }
    mCursor.moveToPosition(mPosition);
    String str = null;
    try {
      JSONObject json = new JSONObject(mCursor.getString(4));
      str = json.get("text").toString();
    } catch (JSONException e) {
      e.printStackTrace();
    }
    rv.setTextViewText(R.id.gree_widget_text_view, str + mPosition);
    Bitmap bmp = BitmapFactory.decodeByteArray(mCursor.getBlob(3), 0, mCursor.getBlob(3).length);
    rv.setImageViewBitmap(R.id.gree_widget_image_view, bmp);
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    GLog.i(TAG, "onDestroy");
  }
}
