/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.util.CoreData;


/**
 * This class is used for internal or developer mode, use Log for now, because when enabled, it
 * should be as verbose as possible. But when release to the developer or release to the market, it
 * could be disabled.
 * 
 */
public class RequestLogger {
  private boolean isLogging = false;

  private static final int LINE_WIDTH = 100;

  private RequestLogger() {
    if (CoreData.containsKey(InternalSettings.EnableRequestLogger)
        && CoreData.get(InternalSettings.EnableRequestLogger).equals("true")) {
      isLogging = true;
    }
  }

  private static final String TAG = RequestLogger.class.getSimpleName();
  private static RequestLogger instance;
  private long requestTime = -1;

  /**
   * Get the Singleton of RequestLogger
   * 
   * @return singleton instance of RequestLogger
   */
  public static RequestLogger getInstance() {
    if (instance == null) {
      instance = new RequestLogger();
    }
    return instance;
  }

  /**
   * Called when the request is started
   * 
   * @param request
   * @param sync
   */
  public void startRequest(HttpUriRequest request, boolean sync) {
    if (isLogging) {
      StringBuilder sb = new StringBuilder();
      sb.append("-->[request start]");
      URI uri = request.getURI();
      if (uri != null) {
        sb.append("\n [host]:" + uri.getHost());
        sb.append("\n [path]:" + uri.getPath());
        sb.append("\n [query]:" + uri.getQuery());
        sb.append("\n [method]:" + request.getMethod());
        String entityString = null;
        try {
          if (request != null && request instanceof HttpPost) {
            HttpPost post = (HttpPost) request;
            HttpEntity entity = post.getEntity();
            if (entity != null) {
              if (entity.isRepeatable()) {
                entityString = EntityUtils.toString(entity, "UTF-8");
              } else {
                entityString = " non-repeatable, skip";
              }
              sb.append("\n [entity]:" + entityString);
            }
          }
        } catch (ParseException e) {
          GLog.printStackTrace(entityString, e);
        } catch (IOException e) {
          GLog.printStackTrace(entityString, e);
        }
      }
      Header[] allHeader = request.getAllHeaders();
      if (allHeader != null) {
        for (Header header : allHeader) {
          sb.append("\n [header]k:").append(header.getName()).append(",").append("v:")
              .append(header.getValue());
        }
      }
      sb.append("\n [sync]:" + sync);
      sb.append("\n-->[request end]");
      GLog.v(TAG, sb.toString());
      requestTime = System.currentTimeMillis();
    }
  }

  /**
   * Called when the repsonse is started
   * @param <T>
   * 
   * @param response
   */
  public <T> void endRequest(ResponseHolder<T> holder) {
    if (holder == null) {
      return;
    }
    if (isLogging) {
      long elapse = System.currentTimeMillis() - requestTime;
      StringBuilder sb = new StringBuilder();
      GLog.v(TAG, "<--[time used] " + elapse + " ms ---");
      sb.append("<--[response start]");
        sb.append("\n [status_code]:" + holder.mStatusCode);
        sb.append("\n [reason_phrase]:" + holder.mReasonPhrase);
      HeaderIterator headerItor = holder.getHeaderIterator();
      while (headerItor.hasNext()) {
        Header header = headerItor.nextHeader();
        sb.append("\n [header]k:").append(header.getName()).append(",").append("v:")
            .append(header.getValue());
      }
      sb.append("\n<--[response end]");
      GLog.v(TAG, sb.toString());
    }
  }

  /**
   * Wrapper function for GLog for RequestPakcage
   * 
   * @param tag
   * @param e
   */
  public void printStackTrace(String tag, Exception e) {
    if (isLogging) {
      if (e == null) {
        return;
      }
      GLog.w(tag, bufferedSplitString(e.getMessage()));
    }
  }

  /**
   * Wrapper function for GLog for RequestPakcage
   * 
   * @param tag
   * @param msg
   */
  public void v(String tag, String msg) {
    if (isLogging) {
      GLog.v(tag, bufferedSplitString(msg));
    }
  }

  /**
   * Wrapper function for GLog for RequestPakcage
   * 
   * @param tag
   * @param msg
   */
  public void d(String tag, String msg) {
    if (isLogging) {
      GLog.d(tag, bufferedSplitString(msg));
    }
  }

  /**
   * Wrapper function for GLog for RequestPakcage
   * 
   * @param tag
   * @param msg
   */
  public void i(String tag, String msg) {
    if (isLogging) {
      GLog.i(tag, bufferedSplitString(msg));
    }
  }

  /**
   * Wrapper function for GLog for RequestPakcage
   * 
   * @param tag
   * @param msg
   */
  public void w(String tag, String msg) {
    if (isLogging) {
      GLog.w(tag, bufferedSplitString(msg));
    }
  }

  /**
   * Wrapper function for GLog for RequestPakcage
   * 
   * @param tag
   * @param msg
   */
  public void e(String tag, String msg) {
    if (isLogging) {
      GLog.e(tag, bufferedSplitString(msg));
    }
  }

  private String bufferedSplitString(String msg) {
    List<String> lines = splitString(msg, LINE_WIDTH);
    if (lines == null) {
      return "null";
    }
    StringBuilder sb = new StringBuilder();
    for (String line : lines) {
      sb.append(line);
      sb.append('\n');
    }
    return sb.toString();
  }

  // Support very long log message
  private List<String> splitString(String text, int sliceSize) {
    if (text == null) {
      return null;
    }
    if (sliceSize <= 0) {
      return new ArrayList<String>();
    }
    ArrayList<String> textList = new ArrayList<String>();
    String aux;
    int left = -1, right = 0;
    int charsLeft = text.length();
    while (charsLeft != 0) {
      left = right;
      if (charsLeft >= sliceSize) {
        right += sliceSize;
        charsLeft -= sliceSize;
      } else {
        right = text.length();
        aux = text.substring(left, right);
        charsLeft = 0;
      }
      aux = text.substring(left, right);
      textList.add(aux);
    }
    return textList;
  }
}
