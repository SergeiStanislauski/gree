/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.ui.AvatarView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HeaderIterator;

import net.gree.asdk.R;
import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.IconDownloadListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.cache.FileCache;
import net.gree.asdk.core.cache.IOCache;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

public class AvatarView extends ImageView {
  private static final String TAG = "AvatarView.java";
  // image types
  public static final int IMAGE_TYPE_UNKNOWN = 0;
  public static final int IMAGE_TYPE_STATIC = 1;
  public static final int IMAGE_TYPE_DYNAMIC = 2;
  // decoder status
  public static final int DECODE_STATUS_UNDECODE = 0;
  public static final int DECODE_STATUS_DECODING = 1;
  public static final int DECODE_STATUS_DECODED = 2;
  // global variables for the imageview
  private GifDecoder decoder;
  private Bitmap bitmap;
  // GIF specific variables
  public int imageType = IMAGE_TYPE_UNKNOWN;
  public int decodeStatus = DECODE_STATUS_UNDECODE;
  // image varialbes
  private int width;
  private int height;
  // animation variables
  private long time;
  private int index;
  // locat variables
  private int resId;
  private String filePath;
  // is the gif playing
  private boolean playFlag = false;
  // GIF cache
  IOCache IOCache = new IOCache();
  FileCache fileCache;
  ExecutorService executorService;

  public AvatarView(Context context, AttributeSet attrs) {
    super(context, attrs);
    fileCache = new FileCache(context);
    executorService = Executors.newFixedThreadPool(5);
  }

  /**
   * Constructor
   */
  public AvatarView(Context context) {
    super(context);
    fileCache = new FileCache(context);
    executorService = Executors.newFixedThreadPool(5);
  }

  /**
   * Loads the user avatar gif based on the user. Starts playing it. Changes based on size
   * 
   * @param user the GreeUser. This is used to define the the avatar we are using
   * @param size an integer based size of the gif
   */
  public void loadAvatar(GreeUser user, int size) {
    final String GIF = ".gif";
    // If it's not a GIF, load it as a standard image
    AvatarGif aGif = user.createAvatarGif(size);
    if (!aGif.url.endsWith(GIF)) {
      // Load the image as normal
      user.loadThumbnail(size, new IconDownloadListener() {
        public void onSuccess(Bitmap image) {
          // Set it as the Image Bitmap
          AvatarView.this.setImageBitmap(image);
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          // Load the default view
          AvatarView.this.setImageResource(R.drawable.ic_launcher);
        }
      });
    } else {
      // It's a gif: switch based on the size of the thumbnail gif we want to retrieve
      setGif(user.createAvatarGif(size));
    }
  }

  /**
   * Sets the refresh rate of the gif
   * 
   * @param n the milliseconds in delay between frame
   */
  public void setDelay(int n) {
    stop();
    if (decoder != null) {
      decoder.setDelay(n);
    }
    play();
  }

  /**
   * Checks where we are getting the InputStream from for the animated Gif. If it is from the web,
   * it will begin will be a URL specific request. If not we have made this extensible so that it
   * can be used in the design for a resource from a file.
   * 
   * @return InputStream object of the Animated Gif Avatar
   */
  private InputStream getInputStream() {
    if (filePath != null) {
      try {
        // check if it is a URL
        new URL(filePath);
        // get the InputStream
        InputStream in = IOCache.get(filePath);
        if (in != null) {
          return in;
        }
        // if it's blank, then we have to get it and retry
        executorService.submit(new PhotosLoader(filePath));
        // it's still blank
        return null;
      } catch (MalformedURLException e1) {
        try {
          // if it is not coming from a URL then we check the file
          return new FileInputStream(filePath);
        } catch (FileNotFoundException e) {}
      }
    }
    if (resId > 0)
      return getContext().getResources().openRawResource(resId);
    return null;
  }

  /**
   * Searches the cache first for the InputStream object, then if unable to find will attempt to
   * download it from the web. Should only be accessed from off the main thread.
   * 
   * @param url URL of the Animated Gif
   * @return InputStream of the Animated Gif
   */
  private InputStream getInputStream(String url) {
    File f = fileCache.getFile(url);

    // from SD cache
    try {
      InputStream b = new FileInputStream(f);
      if (b != null)
        return b;
    } catch (FileNotFoundException e) {
      GLog.e(TAG, e.getLocalizedMessage());
    }

    // from web
    try {
      URL imageUrl = new URL(url);
      HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
      conn.setConnectTimeout(30000);
      conn.setReadTimeout(30000);
      conn.setInstanceFollowRedirects(true);
      InputStream is = conn.getInputStream();
      OutputStream os = new FileOutputStream(f);
      IOUtil.CopyStream(is, os);
      os.close();
      return is;
    } catch (Throwable ex) {
      GLog.e(TAG, ex.getLocalizedMessage());
      if (ex instanceof OutOfMemoryError)
        IOCache.clear();
      return null;
    }
  }

  public void clearCache() {
    IOCache.clear();
    fileCache.clear();
  }

  /**
   * set gif file path
   * 
   * @param filePath
   */
  public void setGif(String filePath) {
    Bitmap bitmap = BitmapFactory.decodeFile(filePath);
    setGif(filePath, bitmap);
  }

  /**
   * set gif file path and cache image
   * 
   * @param filePath
   * @param cacheImage
   */
  private void setGif(String filePath, Bitmap cacheImage) {
    this.resId = 0;
    this.filePath = filePath;
    imageType = IMAGE_TYPE_UNKNOWN;
    decodeStatus = DECODE_STATUS_UNDECODE;
    playFlag = false;
    bitmap = cacheImage;
    width = bitmap.getWidth();
    height = bitmap.getHeight();
    setLayoutParams(new LayoutParams(width, height));
  }

  /**
   * set gif resource id from an resource file
   * 
   * @param resId
   */
  public void setGif(int resId) {
    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resId);
    setGif(resId, bitmap);
  }

  /**
   * set gif resource id and cache image
   * 
   * @param resId
   * @param cacheImage
   */
  private void setGif(int resId, Bitmap cacheImage) {
    this.filePath = null;
    this.resId = resId;
    imageType = IMAGE_TYPE_UNKNOWN;
    decodeStatus = DECODE_STATUS_UNDECODE;
    playFlag = false;
    bitmap = cacheImage;
    width = bitmap.getWidth();
    height = bitmap.getHeight();
    setLayoutParams(new LayoutParams(width, height));
  }

  /**
   * Sets the GIF resources for the AvatarGif
   * 
   * @param user the current AvatarGif file for the current user, containing all the basic info for
   *        the image we need.
   */
  private void setGif(AvatarGif user) {
    this.resId = 0;
    this.filePath = user.url;
    imageType = IMAGE_TYPE_UNKNOWN;
    decodeStatus = DECODE_STATUS_UNDECODE;
    playFlag = false;
    width = user.width;
    height = user.height;
    // check the Bitmap
    Bitmap bitmapFromCache = BitmapFactory.decodeStream(IOCache.get(user.url));
    if (bitmapFromCache != null) {
      bitmap = bitmapFromCache;
    } else {
      bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
      bitmap.eraseColor(Color.TRANSPARENT);
      executorService.submit(new PhotosLoader(user.url));
    }

    setLayoutParams(new LayoutParams(width, height));
  }

  /**
   * Will decode the InputStream so that we can animate the gif file. <b> Note: </b> the call
   * shouldn't issue a possibility where "decoder.read(getInputStream());" is returning a null value
   */
  private void decode() {
    release();
    index = 0;

    decodeStatus = DECODE_STATUS_DECODING;

    new Thread() {
      @Override
      public void run() {
        decoder = new GifDecoder();
        decoder.read(getInputStream());
        if (decoder.width == 0 || decoder.height == 0) {
          imageType = IMAGE_TYPE_STATIC;
        } else {
          imageType = IMAGE_TYPE_DYNAMIC;
        }
        postInvalidate();
        time = System.currentTimeMillis();
        decodeStatus = DECODE_STATUS_DECODED;
      }
    }.start();
  }

  public void release() {
    decoder = null;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    // if the bitmap is not null, procede
    if (bitmap != null) {
      if (decodeStatus == DECODE_STATUS_UNDECODE) {
        canvas.drawBitmap(bitmap, 0, 0, null);
        if (playFlag) {
          decode();
          invalidate();
        }
      } else if (decodeStatus == DECODE_STATUS_DECODING) {
        canvas.drawBitmap(bitmap, 0, 0, null);
        invalidate();
      } else if (decodeStatus == DECODE_STATUS_DECODED) {
        if (imageType == IMAGE_TYPE_STATIC) {
          canvas.drawBitmap(bitmap, 0, 0, null);
        } else if (imageType == IMAGE_TYPE_DYNAMIC) {
          if (playFlag) {
            long now = System.currentTimeMillis();

            if (time + decoder.getDelay(index) < now) {
              time += decoder.getDelay(index);
              incrementFrameIndex();
            }
            Bitmap bitmap = decoder.getFrame(index);
            if (bitmap != null) {
              canvas.drawBitmap(bitmap, 0, 0, null);
            }
            invalidate();
          } else {
            Bitmap bitmap = decoder.getFrame(index);
            canvas.drawBitmap(bitmap, 0, 0, null);
          }
        } else {
          canvas.drawBitmap(bitmap, 0, 0, null);
        }
      }
    }
  }

  private void incrementFrameIndex() {
    index++;
    if (index >= decoder.getFrameCount()) {
      index = 0;
    }
  }

  private void decrementFrameIndex() {
    index--;
    if (index < 0) {
      index = decoder.getFrameCount() - 1;
    }
  }

  public void play() {
    time = System.currentTimeMillis();
    playFlag = true;
    invalidate();
  }

  public void pause() {
    playFlag = false;
    invalidate();
  }

  public void stop() {
    playFlag = false;
    index = 0;
    invalidate();
  }

  public void nextFrame() {
    if (decodeStatus == DECODE_STATUS_DECODED) {
      incrementFrameIndex();
      invalidate();
    }
  }

  public void prevFrame() {
    if (decodeStatus == DECODE_STATUS_DECODED) {
      decrementFrameIndex();
      invalidate();
    }
  }

  /**
   * Class to encapsulate the GreeUser information we need for the avatar
   * 
   */
  public static class AvatarGif {
    public String url;
    public int height;
    public int width;

    /**
     * Create the class
     * 
     * @param url Url of the Image
     * @param height height of the image, set based on values in the indoc
     * @param width width of the image, set based on values in the indoc
     */
    public AvatarGif(String url, int height, int width) {
      this.url = url;
      this.height = height;
      this.width = width;
    }
  }// end class

  /**
   * Class responsible for loading the InputStream off of the main thread. Will reset and invalidate
   * the AvatarView, forcing it to be redrawn after the image is loaded and set
   */
  class PhotosLoader implements Runnable {
    String url;

    PhotosLoader(String url) {
      this.url = url;
    }

    @Override
    public void run() {
      // gets the inputstream for the image
      InputStream is = getInputStream(url);
      // stores it on the cache
      IOCache.put(url, is);
      // sets the drawable
      decode();
      invalidate();

    }
  }// end class

}
