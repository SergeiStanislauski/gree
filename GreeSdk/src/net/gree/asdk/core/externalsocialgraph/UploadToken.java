/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.externalsocialgraph;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;

import java.util.TreeMap;

import org.apache.http.HeaderIterator;

/*
 *  Class to upload Token to access External Social Graph APIs
 *  This class is expected to be used by Messanger
 *  Initial target would be Facebook
 */
public class UploadToken {
  private static final String TAG = UploadToken.class.getSimpleName();
  private static final boolean isDebug = true;
 
  private static final String endpoint = "user/me/friends/facebook/:update";

  /*
   * Interface for client to listen to results of request
   */
  public interface uploadTokenListener {
    public void onSuccess();
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /*
   * Type for future extension
   */
  public static final int FACEBOOK = 0;
  public static final int TWITTER = 1;
  public static final int GMAIL = 2;

  /**
   * Upload access token to server
   * 
   * @param token which is Facebook Access Token etc
   * @param type to identify the type of token
   * @param listener to listen to the result
   * @return true if request is accepted, otherwise false(ex. parameter is null)
   */
  public boolean upload(String token, int type, final uploadTokenListener listener) {
    if(token == null) {
      GLog.e(TAG, "invalid argument. token is null");
      return false;
    }

    TreeMap<String, Object> input = new TreeMap<String, Object>();
    input.put("token", token);
    // 3 legged OAuth 1.0 Authorization shall be required
   
   
    new JsonClient().oauth(Url.getGreeSecureApiEndpointWithAction(endpoint), BaseClient.methods[BaseClient.METHOD_POST], null, BaseClient.toEntityJson(input), false, new OnResponseCallback<String>() {

     
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        GLog.v(TAG, "[onSuccess] responseCode : " + responseCode + " response : " + response);
        if(listener != null) {
          listener.onSuccess();
        }
      }

      // status code : 403 - AccessDenied, 500 - OperationFailed, 200 - N/A, 400 - InvalidParameter
      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.v(TAG, "[onFailure] responseCode : " + responseCode + " response : " + response);
        if(listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
      
    });
    return true;
  }
}
