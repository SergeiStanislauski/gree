/*
 * Copyright (C) 2012 The Android Open Source Project
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * NOTE: This file has been modified by GREE, Inc.
 * Modifications are licensed under the License.
 */
package net.gree.asdk.core.cache;

import java.util.concurrent.RejectedExecutionException;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.util.Util;

public class Request {
  private static final String TAG = Request.class.getSimpleName();
  private AsyncTask<String, ?, ?> mAsyncTask;
  private String mUrl;

  public Request(String url, AsyncTask<String, ?, ?> task) {
    mUrl = url;
    mAsyncTask = task;
  }

  public String getUrl() {
    return mUrl;
  }

  public AsyncTask<String, ?, ?> getTask() {
    return mAsyncTask;
  }

  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
  public void execute() throws InterruptedException {
    try {
      if (Util.hasHoneycomb()) {
        // Execute in parallel
        mAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mUrl);
      } else {
        mAsyncTask.execute(mUrl);
      }
    } catch (RejectedExecutionException e) {
      GLog.printStackTrace(TAG, e);
    }
  }
}
