/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.fragment;

import java.util.ArrayList;
import java.util.List;

import net.gree.platformsample.HelpActivity;
import net.gree.platformsample.R;
import net.gree.platformsample.adapter.HelpItemAdapter;
import net.gree.platformsample.wrapper.HelpWrapper;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class HelpListFragment extends ListFragment {
  private static final String TAG = "HelpListFragment";
  final List<HelpWrapper> data = new ArrayList<HelpWrapper>();  
  private boolean mDualPane = false;
  private int mCurCheckPosition = 0;
  private boolean mOpenHelpRoot = true;
  
  /**
   * Create a new instance of HelpListFragment, initialized to
   * start off pointing to the details page of the item who sent the initial intent.
   * Name should be the string resource for the title of the page you want to open e.g. R.string.leaderboard_title. 
   * To open the root page use "" as the input string. 
   * @param name Name of the item that called 
   * @return f New fragment instance
   */
  public static HelpListFragment newInstance(String name) {
      HelpListFragment f = new HelpListFragment();
      Bundle args = new Bundle();
      args.putString("ITEM", name);
      f.setArguments(args);
      f.mOpenHelpRoot = (name.equals(""));
      return f;
  }

  /**
   * 
   * @return The "ITEM" argument stored when this instance was created
   */
  public String getName() {
      return getArguments().getString("ITEM");
  }
  
  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);
      
      createHelpList();
      setListAdapter(new HelpItemAdapter(getActivity(), data));

      // Check to see if we have a frame in which to embed the details fragment
      View detailsFrame = getActivity().findViewById(R.id.help_details_layout);
      mDualPane = (detailsFrame != null) && (detailsFrame.getVisibility() == View.VISIBLE);

      
      if (savedInstanceState != null) {
         // Restore last state for checked position.
         mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
         mOpenHelpRoot = savedInstanceState.getBoolean("openRoot",true);
      }
      
      //The details page to show based on the currently selected list position
      String details = data.get(mCurCheckPosition).getName();
      
      //We are not trying to open the root page (trying to open a specific page)
      //and there is no saved instance
      if (!mOpenHelpRoot && savedInstanceState == null){
        for (int i = 0; i<data.size(); i++){
           //Check to see if the item we are looking for is in the list
           if (data.get(i).getName().equals(getName())){
             mCurCheckPosition = i;
             details = getName();
           }
         }
      }
      
      //Open to a details page directly if there is a dual pane
      //or if there isn't a dual pane but we are directly opening a details page
      if (mDualPane || (!mDualPane && !mOpenHelpRoot)) {
        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        showDetails(mCurCheckPosition, details);
        //This is so that you always go to the root view when you rotate out of dual screen into single screen
        mOpenHelpRoot = true;
        ((HelpActivity) getActivity()).setUpBackButtonToClose();
      }else{
        ((HelpActivity) getActivity()).setUpBackButtonToBack();
      }
  }
  
  @Override
  public void onSaveInstanceState(Bundle outState) {
      super.onSaveInstanceState(outState);
      outState.putInt("curChoice", mCurCheckPosition);
      outState.putBoolean("openRoot", mOpenHelpRoot);
  }
  
  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {  
    showDetails(position, ((TextView) v.findViewById(R.id.help_item_name)).getText().toString());
  }
  
  /**
   * Populate the list based on a string array resource
   */
  public void createHelpList(){
    data.clear();
    for (String s: getResources().getStringArray(R.array.help_list)){
      HelpWrapper item = new HelpWrapper(s);
      data.add(item);
    }
  }
  
  /**
   * 
   * @param index The index of the list
   * @param name The name of the item in the list
   */
  private void showDetails(int index, String name) {
    mCurCheckPosition = index;  
    if (mDualPane) {
            
      // Check out if the details page is embedded, shown in the right pane.  Replace if needed.
      HelpDetailsFragment embedded = (HelpDetailsFragment) getFragmentManager().findFragmentByTag("help_embedded_details");
      
      //If there is no embedded details or the one we want to show is not the currently shown one
      if (embedded == null || embedded.getShownIndex() != index) {
          embedded = HelpDetailsFragment.newInstance(index, name);

          // Execute a transaction, replacing any existing fragment
          // with this one inside the frame.
          FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
          ft.replace(R.id.help_details_layout, embedded, "help_embedded_details");
          ft.commit();
      }
    } else { //We are in single pane mode
        HelpDetailsFragment details = (HelpDetailsFragment) getFragmentManager().findFragmentByTag("help_details");
        
        //If there is no details page 
        if (details == null) {
          details = HelpDetailsFragment.newInstance(index, name);
          FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
          ft.replace(R.id.help_list_layout, details, "help_details");
          //Add to the back stack so we can navigate back to the List fragment by hitting the back button
          ft.addToBackStack("details");
          ft.commit();
        } 
    }
  }
  
}
