/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.platformsample;

import java.util.TreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;
import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.api.ui.RequestDialog;

/**
 * Send gift activity
 * This activity is called after selecting "Send a gift" in a request service menu.
 * 
 * You can send a gift to someone in this activity.
 */
public class SendGiftActivity extends BaseActivity {
  private static final String TAG = "SendGiftActivity";
  // three options for the gift items
  LinearLayout mSendGiftLLTop, mSendGiftLLMid, mSendGiftLLBot;
  // hard coding the identifiers to facilitate switch statements (make them easier to read)
  private static final int GIFT1 = R.id.send_gift_ll_wrapper_top;
  private static final int GIFT2 = R.id.send_gift_ll_wrapper_mid;
  private static final int GIFT3 = R.id.send_gift_ll_wrapper_bot;

  private RequestDialog mRequestDialog;
  private Handler mHandler;
  
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Standard GGP calls and call to load the UI
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.send_gift);

    // set request dialog and handler
    mRequestDialog = new RequestDialog(this);
    mHandler = new Handler() {
      public void handleMessage(Message message) {
        if (message.what == RequestDialog.OPENED) {
			Toast.makeText(getApplicationContext(), "Request Dialog opened.", Toast.LENGTH_SHORT)
                .show();
			Log.i(TAG, "Request Dialog opened.");
		} else if (message.what == RequestDialog.CLOSED) {
			if (message.obj != null) {
              Toast.makeText(getApplicationContext(),
                  "Request Dialog closed. result:[" + message.obj.toString() + "]",
                  Toast.LENGTH_SHORT).show();
              Log.i(TAG, "Request Dialog closed. result:[" + message.obj.toString() + "]");
            } else {
              Toast.makeText(getApplicationContext(), "Request Dialog closed. result:[Nothing]",
                  Toast.LENGTH_SHORT).show();
              Log.i(TAG, "Request Dialog closed. result:[Nothing]");
            }
		}
      }
    };
    
    // initalize the UI
    initUI();
    // sets the clickListeners
    setClickListeners();
    
  }

  private void initUI() {
    mSendGiftLLTop = (LinearLayout) findViewById(R.id.send_gift_ll_wrapper_top);
    mSendGiftLLMid = (LinearLayout) findViewById(R.id.send_gift_ll_wrapper_mid);
    mSendGiftLLBot = (LinearLayout) findViewById(R.id.send_gift_ll_wrapper_bot);
  }

  private void setClickListeners() {
    mSendGiftLLTop.setOnClickListener(sendGiftClickListener);
    mSendGiftLLMid.setOnClickListener(sendGiftClickListener);
    mSendGiftLLBot.setOnClickListener(sendGiftClickListener);

  }
  
  private OnClickListener sendGiftClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      if (AsyncErrorDialog.shouldShowErrorDialog(v.getContext())) {
        AsyncErrorDialog dialog = new AsyncErrorDialog(v.getContext());
        dialog.show();
        return;
      }

      TreeMap<String, Object> map = new TreeMap<String, Object>();
      String id = "id";
      String title = "title";
      String body = "body";
      // "attrs" is used for checking if this activity from a gift list of game notice board  
      String attrs = "attrs";  
      // attrName do not allow space
      String attrName = getResources().getString(R.string.gift_attr_name);
      String attrValue = getResources().getString(R.string.gift_attr_value);
      String[] gift1Array = getResources().getStringArray(R.array.strawberry_cake);
      String[] gift2Array = getResources().getStringArray(R.array.vanilla_ice_cream);
      String[] gift3Array = getResources().getStringArray(R.array.lump_of_coal);
      
      JSONObject attr = new JSONObject();
      try {
        attr.put(attrName, attrValue);
      } catch (JSONException e) {
        e.printStackTrace();
      }
      
      if (v.getId() == GIFT1) {
		map.put(id, gift1Array[0]);
		map.put(title, gift1Array[1]);
		map.put(body, gift1Array[2]);
		map.put(attrs, attr);
	} else if (v.getId() == GIFT2) {
		map.put(id, gift2Array[0]);
		map.put(title, gift2Array[1]);
		map.put(body, gift2Array[2]);
		map.put(attrs, attr);
	} else if (v.getId() == GIFT3) {
		map.put(id, gift3Array[0]);
		map.put(title, gift3Array[1]);
		map.put(body, gift3Array[2]);
		map.put(attrs, attr);
	} else {
	}
      
      if (mRequestDialog == null) {
        mRequestDialog = new RequestDialog(v.getContext());
      }

      mRequestDialog.setParams(map);
      mRequestDialog.setHandler(mHandler);
      mRequestDialog.show();
    }
  };
  
  @Override
  public void onOpenHelp(View v){
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.request_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }

  @Override
  protected void sync(boolean fromStart) { }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpBackButton();
    setUpAutoLoadMore();
  }
  
}
