/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.codec.Base64;
import net.gree.asdk.core.ui.CommandInterface;
import net.gree.asdk.core.ui.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * class for upload image.
 * @author GREE, Inc
 *
 */
public class ImageUploader {
  private static final String TAG = "ImageUploader";

  /**
   * represent uploading image.
   * @author GREE, Inc
   */
  public class ImageData {
    public Bitmap mBitmap;
    public String mBase64;
    public JSONObject mObject;

    /**
     * setting image data
     * 
     * @param uri content uri
     * @param orientation device orientation
     */
    public ImageData(Uri uri, int orientation) {
      InputStream inputStream = null;

      try {
        ContentResolver contentResolver = mOwnerActivity.getContentResolver();
        inputStream = contentResolver.openInputStream(uri);

        float maxEdge = mOwnerActivity.getResources().getInteger(RR.integer("gree_max_image_edge"));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream, null, options);

        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;
        if (options.outWidth > maxEdge || options.outHeight > maxEdge) {
          options.inSampleSize =
              (int) Math.pow(
                  2,
                  (int) Math.round(Math.log(maxEdge
                      / (double) Math.max(options.outWidth, options.outHeight))
                      / Math.log(0.5)));
        }
        inputStream.close();
        inputStream = contentResolver.openInputStream(uri);
        mBitmap = BitmapFactory.decodeStream(inputStream, null, options);
        mBase64 = getBase64String(mBitmap, orientation);
      } catch (IOException e) {
        GLog.printStackTrace(TAG, e);
      } finally {
        try {
          if (inputStream != null) {
            inputStream.close();
          }
        } catch (Exception e) {
          GLog.printStackTrace(TAG, e);
        }
      }
    }

    /**
     * setting image data
     * 
     * @param bitmap image data
     * @param orientation device orientation
     */
    public ImageData(Bitmap bitmap, int orientation) {
      if (orientation == 0) {
        mBitmap = bitmap;
      } else {
        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);
        mBitmap =
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
      }
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      mBitmap.compress(CompressFormat.JPEG, 100, new Base64.OutputStream(outputStream));
      mBase64 =  new String(outputStream.toByteArray());
    }
    
    /**
     * set JSON Object of result.
     * @param object JSON Object
     */
    public void setJSONObject(JSONObject object) { mObject = object;}
  }

  private Activity mOwnerActivity = null;
  private Fragment mOwnerFragment = null;

  private String mCallbackId= null;

  private ProgressDialog mThumbnailProgressDialog = null;
  private Uri mTakenPhotoUri = null;

  private UriUploadTask mUriUploadTask = null;
  private BitmapUploadTask mBitmapUploadTask = null;
  private ImageUploaderCallback mCallback = null;

  public interface ImageUploaderCallback {
    public void callback(ImageData data);
  }

  /**
   * constructor of ImageUploader.
   * 
   * @param ownerActivity Activity required image upload.
   */
  public ImageUploader(Activity ownerActivity) {

    mOwnerActivity = ownerActivity;

    mThumbnailProgressDialog = new ProgressDialog(mOwnerActivity);
  }

  /**
   * constructor of ImageUploader.
   * 
   * @param ownerActivity Activity required image upload.
   * @param callback call it done upload image.
   */
  public ImageUploader(Activity ownerActivity, ImageUploaderCallback callback) {

    mOwnerActivity = ownerActivity;
    mCallback = callback;

    mThumbnailProgressDialog = new ProgressDialog(mOwnerActivity);
  }


  public ImageUploader(FragmentActivity ownerActivity, Fragment ownerFragment) {
    mOwnerActivity = ownerActivity;
    mOwnerFragment = ownerFragment;
    mThumbnailProgressDialog = new ProgressDialog(mOwnerActivity);
  }

  /**
   * show dialog for uploading image.
   */
  public void showSelectionDialog() {
    String[] items = null;
    items =
        new String[] {mOwnerActivity.getString(RR.string("gree_uploader_camera")),
                      mOwnerActivity.getString(RR.string("gree_uploader_gallery")),
                      mOwnerActivity.getString(RR.string("gree_button_cancel"))};

    new AlertDialog.Builder(mOwnerActivity).setTitle(RR.string("gree_uploader_selection_dialog_title"))
        .setItems(items, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            switch (which) {
              case 0:
                startCamera();
                break;
              case 1:
                startImageGallery();
                break;
              case 2:
                break;
              case 3:
                break;
              default:
                break;
            }
          }
        }).show();
  }

  /**
   * Get uri of photo.
   *
   * @return uri of photo which is taken by camera.
   */
  public Uri getTakenPhotoUri() {
    return mTakenPhotoUri;
  }

  /**
   * show dialog for uploading image.
   * 
   * @param comandInterface CommandInterface for executing callback javascript method
   * @param params for resetCallback method
   */
  public void showSelectionDialog(final CommandInterface commandInterface, JSONObject params) {
    final boolean usePreview = params.optBoolean("usePreview", false);

    try {
      mCallbackId = params.getString("callback");
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }

    final String resetCallbackId = params.optString("resetCallback");
    final boolean isAlreadyImageSelected = resetCallbackId.length() != 0;

    String[] items = null;
    if (isAlreadyImageSelected) {
      items =
          new String[] {mOwnerActivity.getString(RR.string("gree_uploader_camera")),
                        mOwnerActivity.getString(RR.string("gree_uploader_gallery")),
                        mOwnerActivity.getString(RR.string("gree_uploader_unselect")),
                        mOwnerActivity.getString(RR.string("gree_button_cancel"))};
    } else {
      items =
          new String[] {mOwnerActivity.getString(RR.string("gree_uploader_camera")),
                        mOwnerActivity.getString(RR.string("gree_uploader_gallery")),
                        mOwnerActivity.getString(RR.string("gree_button_cancel"))};
    }
    new AlertDialog.Builder(mOwnerActivity).setTitle(RR.string("gree_uploader_selection_dialog_title"))
        .setItems(items, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            switch (which) {
              case 0:
                startCamera(usePreview);
                break;
              case 1:
                startImageGallery(usePreview);
                break;
              case 2:
                if (isAlreadyImageSelected) {
                  commandInterface.executeCallback(resetCallbackId, new JSONObject());
                }
                break;
              case 3:
                break;
              default:
                break;
            }
          }
        }).show();
  }

  /**
   * upload Image of Gallery
   * 
   * @param commandInterface CommandInterface for executing callback javascript method
   * @param data Gallery content uri.
   */
  public void uploadUri(CommandInterface commandInterface, Intent data) {
    if (data == null) { return; }

    mUriUploadTask = new UriUploadTask(commandInterface);
    mUriUploadTask.execute(data.getData());
  }

  /**
   * upload Image of taking photo use camera
   * 
   * @param commandInterface commandInterface CommandInterface for executing callback javascript method
   * @param data image data
   */
  public void uploadImage(CommandInterface commandInterface, Intent data) {

    if (mTakenPhotoUri != null) {

      mUriUploadTask = new UriUploadTask(commandInterface);
      mUriUploadTask.execute(mTakenPhotoUri);

    } else if (data != null) {

      mBitmapUploadTask = new BitmapUploadTask(commandInterface);
      mBitmapUploadTask.execute((Bitmap) (data.getExtras().get("data")));
    }
  }

  private String getBase64String(Bitmap bitmap, int orientation) {

    if (bitmap == null) { return ""; }

    Bitmap scaledBitmap = null;
    if (orientation == 0) {
      scaledBitmap = bitmap;
    } else {
      Matrix matrix = new Matrix();
      matrix.postRotate(orientation);
      scaledBitmap =
          Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    scaledBitmap.compress(CompressFormat.JPEG, 100, new Base64.OutputStream(outputStream));

    return new String(outputStream.toByteArray());
  }

  private void startImageGallery() {
    startImageGallery(false);
  }

  private void startImageGallery(boolean usePreview) {

    Intent intent = new Intent();
    intent.setType("image/*");
    intent.setAction(Intent.ACTION_GET_CONTENT);

    String requestCodeIntegerName = usePreview ? "gree_request_code_get_image_with_preview" : "gree_request_code_get_image";
    int requestCodeGetImage = Core.getInstance().getContext().getResources().getInteger(RR.integer(requestCodeIntegerName));
    if (mOwnerActivity instanceof FragmentActivity && mOwnerFragment instanceof Fragment) {
      FragmentActivity.class.cast(mOwnerActivity).startActivityFromFragment(mOwnerFragment, intent, requestCodeGetImage);
    } else {
      mOwnerActivity.startActivityForResult(intent, requestCodeGetImage);
    }
  }

  private void startCamera() {
    startCamera(false);
  }

  private void startCamera(boolean usePreview) {

    Intent intent = new Intent();
    intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

    if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

      String fileName =
          DateFormat.format("yyyy-MM-dd_kk.mm.ss", System.currentTimeMillis()).toString() + ".jpg";
      File savedImageFile = new File(ImageProvider.getImageDirectory(), fileName);
      try {
        if (!savedImageFile.exists()) {
          savedImageFile.createNewFile();
        }
      } catch (IOException e) {
        GLog.printStackTrace(TAG, e);
      }

      mTakenPhotoUri = Uri.fromFile(savedImageFile);
      
      intent.putExtra(MediaStore.EXTRA_OUTPUT, mTakenPhotoUri);
    } else {
      mTakenPhotoUri = null;
    }

    String requestCodeIntegerName = usePreview ? "gree_request_code_capture_image_with_preview" : "gree_request_code_capture_image";
    int requestCodeCaptureImage = Core.getInstance().getContext().getResources().getInteger(RR.integer(requestCodeIntegerName));
    if (mOwnerActivity instanceof FragmentActivity && mOwnerFragment instanceof Fragment) {
      FragmentActivity.class.cast(mOwnerActivity).startActivityFromFragment(mOwnerFragment, intent, requestCodeCaptureImage);
    } else {
      mOwnerActivity.startActivityForResult(intent, requestCodeCaptureImage);
    }
  }

  private JSONObject makeUploadResult(String base64Data, String uri, Integer orientation) {

    JSONObject result = new JSONObject();

    try {
      result.put("base64_image", base64Data);
      if (uri != null) {
        result.put("uri", uri);
      }
      if (orientation != null) {
        result.put("orientation", orientation.intValue());
      }
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }

    return result;
  }

  /**
   * upload async task used content uri
   * @author GREE, Inc
   *
   */
  final class UriUploadTask extends AsyncTask<Uri, Void, ImageData> {

    private CommandInterface commandInterface_ = null;

    public UriUploadTask(CommandInterface commandInterface) {
      commandInterface_ = commandInterface;
    }

    @Override
    protected void onPreExecute() {
      GLog.d("ImageUploader", "onPreExecute");
      mThumbnailProgressDialog.init(mOwnerActivity.getString(RR.string("gree_thumbnail_progress_message")),
          null, true);
      mThumbnailProgressDialog.show();
    }

    @Override
    protected ImageData doInBackground(Uri... arg0) {
      GLog.d("ImageUploader", "doInBackground");
      int orientation = 0;
      String localUri = arg0[0].toString();

      if (arg0[0].getScheme().equals("content")) {

        ContentResolver contentResolver = mOwnerActivity.getContentResolver();

        Cursor cursor =
            contentResolver.query(arg0[0], new String[] {MediaStore.Images.Media.ORIENTATION},
                null, null, null);
        if (cursor != null) {
          cursor.moveToFirst();
          orientation = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION));
          cursor.close();
        }
      } else {

        localUri = ImageProvider.CONTENT_URI + "/" + arg0[0].getLastPathSegment();
      }

      ImageData data = new ImageData(arg0[0], orientation);
      data.setJSONObject(makeUploadResult(data.mBase64, localUri, orientation));
      return data;
    }

    @Override
    protected void onPostExecute(ImageData result) {

      if (result != null) {
        if (commandInterface_ != null) {
          commandInterface_.executeCallback(mCallbackId, result.mObject);
        } else if (mCallback != null) {
          mCallback.callback(result);
        }
      }

      cleanUp();
    }

    @Override
    protected void onCancelled() {

      cleanUp();

      super.onCancelled();
    }

    private void cleanUp() {

      if (mThumbnailProgressDialog.isShowing()) {
        mThumbnailProgressDialog.dismiss();
      }
    }
  }

  /**
   * upload async task used bitmap
   * @author GREE, Inc
   *
   */
  final class BitmapUploadTask extends AsyncTask<Bitmap, Void, ImageData> {

    CommandInterface commandInterface_ = null;

    public BitmapUploadTask(CommandInterface commandInterface) {
      commandInterface_ = commandInterface;
    }

    @Override
    protected void onPreExecute() {
      mThumbnailProgressDialog.init(mOwnerActivity.getString(RR.string("gree_thumbnail_progress_message")),
          null, true);
      mThumbnailProgressDialog.show();
    }

    @Override
    protected ImageData doInBackground(Bitmap... arg0) {
      ImageData data = new ImageData(arg0[0], 0);
      data.setJSONObject(makeUploadResult(data.mBase64, null, null));
      return data;
    }

    @Override
    protected void onPostExecute(ImageData result) {

      if (result != null) {
        if (commandInterface_ != null) {
          commandInterface_.executeCallback(mCallbackId, result.mObject);
        } else if (mCallback != null) {
          mCallback.callback(result);
        }
      }

      mThumbnailProgressDialog.dismiss();
    }

    @Override
    protected void onCancelled() {

      mThumbnailProgressDialog.dismiss();

      super.onCancelled();
    }
  }
}
