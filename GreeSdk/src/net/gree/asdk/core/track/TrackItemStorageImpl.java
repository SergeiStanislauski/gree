/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.track;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.inject.Inject;
import net.gree.asdk.core.storage.DbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class TrackItemStorageImpl implements TrackItemStorage {
  private static final String TAG = "TrackItemStorage";
  private static final String dbName = "gree.db";
  private static final String tableName = "track_items";

  DbHelper dbHelper;

  @Inject
  TrackItemStorageImpl(Context context) {
    try {
      dbHelper =
          new DbHelper(
              context,
              dbName,
              5,
              tableName,
              "CREATE TABLE "
                  + tableName
                  + " (id INTEGER PRIMARY KEY, type TEXT, key TEXT, data TEXT, seal TEXT, mixer TEXT, uploader_class_name TEXT)");
    } catch (Exception ex) {
      GLog.printStackTrace(TAG, ex);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void save(TrackItem item) {
    item.generateSeal();
    ContentValues values = new ContentValues();
    // There is a mismatch between db and TrackItem for type. It stores type and userId as a combo type in db,
    // so that it can add user id to db without change and migrate old data.
    values.put("type", generateComboType(item.userId, item.type));
    values.put("key", item.key);
    values.put("data", item.data);
    values.put("seal", item.seal);
    values.put("mixer", item.mixer);
    values.put("uploader_class_name", item.uploaderClzName);
    SQLiteDatabase db = null;
    Cursor cursor = null;
    try {
      db = dbHelper.getWritableDatabase();
      db.beginTransaction();
      @SuppressWarnings("unused")
      long count = db.insert(tableName, null, values);
      cursor = db.rawQuery("select last_insert_rowid()", null);
      cursor.moveToFirst();
      item.id = cursor.getInt(0);
      db.setTransactionSuccessful();
      db.endTransaction();
    } catch (Exception ex) {
      GLog.d(TAG, "save: " + ex.toString());
    } finally {
      if (null != cursor) {
        cursor.close();
      }
      if (null != db) {
        db.close();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void delete(TrackItem item) {
    SQLiteDatabase db = null;
    try {
      db = dbHelper.getWritableDatabase();
      long count = db.delete(tableName, "id = ?", new String[] {String.valueOf(item.id)});
      if (count != 1)
        GLog.d(TAG, "delete: didn't delete any rows for:" + item.id);
    } catch (Exception ex) {
      GLog.w(TAG, "delete:" + ex.getMessage());
    } finally {
      if (null != db) {
        db.close();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TrackItem> findAndCheckPendingUpload() {
    SQLiteDatabase db = null;
    Cursor cursor = null;
    List<TrackItem> results = new ArrayList<TrackItem>();
    try {
      db = dbHelper.getWritableDatabase();
      cursor =
          db.query(tableName, new String[] {"id", "type", "key", "data", "seal", "mixer",
              "uploader_class_name"}, null, null, null, null, null);
      if (cursor.moveToFirst()) {
        do {
          String comboType = cursor.getString(1);
          String[] userIdAndType = getUserIdAndType(comboType);
          String userId = null;
          String type = null;
          if (null != userIdAndType) {
            userId = userIdAndType[0];
            type = userIdAndType[1];
          }
          String key = cursor.getString(2);
          String data = cursor.getString(3);
          String mixer = cursor.getString(5);
          String uploaderClassName = cursor.getString(6);
          TrackItem item = new TrackItem(userId, type, key, data, mixer, uploaderClassName);
          item.id = cursor.getInt(0);
          item.setStorage(this);
          item.seal = cursor.getString(4);

          if (null == userId || null == type || !item.checkSeal()) {
            delete(item);
            continue;
          }

          results.add(item);
        } while (cursor.moveToNext());
      }
    } catch (Exception e) {
      GLog.w(TAG, "query: " + e.getMessage());
    } finally {
      if (null != cursor) {
        cursor.close();
      }
      if (null != db) {
        db.close();
      }
    }
    return results;
  }

  private static Pattern GET_USER_ID_PATTERN = Pattern.compile("(\\d+?)$");

  private static String[] getUserIdAndType(String comboType) {
    if (null == comboType) {
      return null;
    }

    Matcher matcher = GET_USER_ID_PATTERN.matcher(comboType);

    if (!matcher.find()) {
      return null;
    }

    String userId = matcher.group();
    int indexOfUserId = comboType.indexOf(userId);
    String type = comboType.substring(0, indexOfUserId);
    return new String[] {userId, type};

  }

  private String generateComboType(String userId, String type) {
    return type + userId;
  }
}
