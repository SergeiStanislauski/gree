/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;

public class GreeHttpExecuterFactory {
  private static final String TAG = GreeHttpExecuterFactory.class.getSimpleName();
  private static IGreeHttpExecuter mExecuter = null;

  public static void setExecuter(IGreeHttpExecuter executor) {
    mExecuter = executor;
  }

  public static IGreeHttpExecuter getExecutor() {
    if (mExecuter != null) {
      RequestLogger.getInstance().w(TAG, "using mock executor");
      return mExecuter;
    } else {
      return new GreeHttpExecuter();
    }
  }
}
