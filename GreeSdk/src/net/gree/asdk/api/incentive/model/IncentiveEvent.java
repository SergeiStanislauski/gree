package net.gree.asdk.api.incentive.model;

/**
 * Model object for a single incentive event.
 *  @see {@link IncentiveController} on how to create and receive those events.
 */
public class IncentiveEvent {
  private String incentive_event_id;
  private String app_id;
  private String from_user_id;
  private String to_user_id;
  private int payload_type;
  private String incentive_event_payload_id;
  private String ctime;
  private String mtime;
  private int delivered;
  private String payload;

  /**
   * Gets the payload.
   * The payload represents the custom data that you want to attach to an event.
   * The payload must be in JSON format and have zero or more key value pairs.
   * The key can not be more than 16 ASCII characters and the value not
   * more than 32. The payload as a whole can not exceed more than 1KB.
   * 
   * @return payload of the incentive event
   */
  public String getPayload() {
    return payload;
  }

  /**
   * Sets the payload.
   * The payload represents the custom data that you want to attach to an event.
   * The payload must be in JSON format and have zero or more key value pairs.
   * The key can not be more than 16 ASCII characters and the value not
   * more than 32. The payload as a whole can not exceed more than 1KB.
   * 
   * @return payload of the incentive event
   */
  public void setPayload(String payload) {
    this.payload = payload;
  }

  /**
   * Gets Whether the event is delivered or not.
   * You can update the status of an event on the server by calling the {@link IncentiveController.put} method
   * @return true if the event was delivered, false otherwise
   */
  public boolean isDelivered() {
    return (delivered == 1);
  }

  /**
   * Gets the type of payload for the incentive event.
   * It can either be a "request" or an "invite" 
   * 
   * @see {@link IncentiveController.PAYLOAD_TYPE_REQUEST}
   * @see {@link IncentiveController.PAYLOAD_TYPE_INVITE}
   * 
   * @return one of PAYLOAD_TYPE_REQUEST PAYLOAD_TYPE_INVITE
   */
  public int getPayloadType() {
    return payload_type;
  }

  /**
   * Sets the type of payload.
   * It can either be a "request" or an "invite"
   * if this is not set, by default the event will be handled as a "request".
   *
   * @see {@link IncentiveController.PAYLOAD_TYPE_REQUEST}
   * @see {@link IncentiveController.PAYLOAD_TYPE_INVITE}
   * 
   * @param payloadType one of PAYLOAD_TYPE_REQUEST PAYLOAD_TYPE_INVITE
   */
  public void setPayloadType(int payloadType) {
    this.payload_type = payloadType;
  }

  /**
   * Gets The event's target Gree user identifier.
   * @return target user id
   */
  public String getToUserId() {
    return to_user_id;
  }

  /**
   * Sets the event's target Gree user identifier.
   * @param toUserId the target GREE id 
   */
  public void setToUserId(String toUserId) {
    this.to_user_id = toUserId;
  }

  /**
   * Sets of source Gree user identifier of the event.
   * @see @{link GreeUser.id}
   * @param fromUserId parameter representing the identification source of this event.
   */
  public String getFromUserId() {
    return from_user_id;
  }

  /**
   * Sets of source Gree user identifier of the event.
   * @see @{link GreeUser.id}
   * @param fromUserId parameter representing the identification source of this event.
   */
  public void setFromUserId(String fromUserId) {
    this.from_user_id = fromUserId;
  }

  /**
   * Gets the application id attached to this event.
   * This parameter is set automatically when creating the event.
   * @return application the application's id as defined on the Gree developer center
   */
  public String getAppId() {
    return app_id;
  }

  /**
   * Gets of the incentive event id for this event.
   * This parameter is set automatically when creating an incentive event. 
   * @return incentive event id
   */
  public String getId() {
    return incentive_event_id;
  }

  /**
   * Gets incentive event's payload id
   * This parameter is set automatically when creating an incentive event.
   * @return incentive event's payload id
   */
  public String getIncentiveEventPayloadId() {
    return incentive_event_payload_id;
  }

  /**
   * Gets event's creation time String
   * This is set automatically when creating an incentive event.
   * Format: "2012-09-13 06:46:57",
   * 
   * @return event's creation time String
   */
  public String getCtime() {
    return ctime;
  }

  /**
   * Gets of event's modification time String
   * this is set automatically when creating the event or updating it with the IncentiveController.put method.
   * Format: "2012-09-13 06:46:57",
   * @return event's modification time String
   */
  public String getMtime() {
    return mtime;
  }

  /**
   * Simple toString formatter. Obtains the String in a fashion 
   * similar to JSON object's representation.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("{");
    sb.append("id:");
    sb.append(incentive_event_id);
    sb.append(",");
    sb.append("appId:");
    sb.append(app_id);
    sb.append(",");
    sb.append("fromUserId:");
    sb.append(from_user_id);
    sb.append(",");
    sb.append("toUserId:");
    sb.append(to_user_id);
    sb.append(",");
    sb.append("payloadType:");
    sb.append(payload_type);
    sb.append(",");
    sb.append("delivered:");
    sb.append(delivered);
    sb.append(",");
    sb.append("payload:");
    sb.append(payload);
    sb.append("}");
    return sb.toString();
  }
}
