/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.contact;

import org.apache.http.HeaderIterator;

import android.content.Context;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.track.Tracker;
import net.gree.asdk.core.track.Tracker.UploadStatus;
import net.gree.asdk.core.track.Tracker.Uploader;

/**
 * Addressbook API Get the phone number and e-mail address from the address book. the data send to
 * the server.
 * 
 * 
 * @author GREE, Inc.
 * 
 */
public class Addressbook {
  private static final String TAG = "AddressBook";
  private static String mApiUrl = null;
  private static String mJsonString = null;
  private int mPrevHash = 0;
  private static final String TRACK_TYPE = "addressbook";
  private static final String TRACK_KEY = "addressbook_data";
  private static OnResponseCallback<String> mListener = null;
  private String mUserId = null;
  private Context mContext;
  private static final boolean isDebug = false;

  private static Uploader uploader = new UploaderImpl();

  private static class UploaderImpl implements Uploader {
    @Override
    public void upload(String type, String key, String value, UploadStatus uploadStatus) {
      postAddressbookData(new OnResponseCallback<String>() {
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, String response) {
          if (isDebug) GLog.d(TAG, "[Response] "+responseCode+" "+response);
          if (mListener != null) {
            mListener.onSuccess(responseCode, headers, response);
          }
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          if (isDebug) GLog.d(TAG, "[Response] "+responseCode+" "+response);
          if (mListener != null) {
            mListener.onFailure(responseCode, headers, response);
          }
        }
      });
    }
  }

  /**
   * Addressbook API
   * 
   * @param context
   */
  public Addressbook(Context context) {
    mPrevHash = 0;
    mApiUrl = Url.getGreeSecureApiEndpointWithAction("user/me/friends/addressbook/:update");
    mUserId = getAuthorizer().getOAuthUserId();
    mContext = context;
  }

  /**
   * POST AddressBook Data
   * 
   * @param checkHash true Not communicate if the address book data has not changed from the
   *        previous transmission.
   * @param listener
   */
  public void postContactList(boolean checkHash, OnResponseCallback<String> listener) {
    mListener = listener;
    mJsonString = ContactListFactory.getInstance().getAddressbookApiData(mContext);
    int hash = mJsonString.hashCode();
    if ((checkHash && hash != mPrevHash) || !checkHash) {
      mPrevHash = hash;
      Injector.getInstance(Tracker.class).track(mUserId, TRACK_TYPE, TRACK_KEY, mJsonString,
          uploader);
    } else {
      // Return success with code 0 when AddressBook is not change.
      listener.onSuccess(0, null, "Did not send the data. Because no changes in the address book.");
    }
  }

  private static void postAddressbookData(OnResponseCallback<String> listener) {
    new JsonClient().oauth(mApiUrl, "POST", null, mJsonString, false, listener);
  }

  private static IAuthorizer sAuthorizer;

  private static IAuthorizer getAuthorizer() {
    if (null == sAuthorizer) {
      sAuthorizer = Injector.getInstance(IAuthorizer.class);
    }
    return sAuthorizer;
  }
}
