package net.gree.asdk.core.storage;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.inject.Inject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.HashSet;
import java.util.Set;

/**
 * This class is a composite key and value storage. The composite key has two levels: first level is
 * type and second level is key. One type maybe has many keys.
 */
public class DBStorage {

  private static final String TAG = "DBStorage";
  private static final String dbName = "gree_dbstorage.db";
  private static final String tableName = "db_storage";
  private DbHelper dbHelper;

  /**
   * Creates a new instance using {@link Context}
   * 
   * @param context
   */
  @Inject
  public DBStorage(Context context) {
    try {
      dbHelper =
          new DbHelper(
              context,
              dbName,
              4,
              tableName,
              "CREATE TABLE "
                  + tableName
                  + " (type TEXT, key TEXT, value TEXT, pendingupload INTEGER, deletewhenuploaded INTEGER, seal TEXT, PRIMARY KEY (type, key))");
    } catch (Exception ex) {
      GLog.printStackTrace(TAG, ex);
    }
  }

  /**
   * Save data to database
   * 
   * @param type first level of key
   * @param key second level of key
   * @param value the value
   */
  public void save(String type, String key, String value) {
    save(type, key, value, false, false);
  }

  /**
   * Get value for the given composite key
   * 
   * @param type first level of key
   * @param key second level of key
   * @return the value, or null if the value doesn't exist.
   */
  public String getValue(String type, String key) {
    SQLiteDatabase db = dbHelper.getReadableDatabase();
    final Cursor cursor =
        db.query(tableName, new String[] {"value"}, "type = ? and key = ?",
            new String[] {type, key}, null, null, null);
    try {
      if (cursor.getCount() > 0) {
        cursor.moveToFirst();
        return cursor.getString(0);
      }
    } catch (Exception e) {
      GLog.w(TAG, "query: " + e.getMessage());
    } finally {
      cursor.close();
    }
    return null;
  }

  /**
   * Get second level of composite key for the given type
   * 
   * @param type first level of key
   * @return the set of second level key
   */
  public Set<String> getKeys(String type) {
    SQLiteDatabase db = dbHelper.getReadableDatabase();
    final Cursor cursor =
        db.query(tableName, new String[] {"key"}, "type = ? ", new String[] {type}, null, null,
            null);
    Set<String> result = new HashSet<String>();
    try {
      if (cursor.moveToFirst()) {
        do {
          result.add(cursor.getString(0));
        } while (cursor.moveToNext());
      }
    } catch (Exception e) {
      GLog.w(TAG, "query: " + e.getMessage());
    } finally {
      cursor.close();
    }
    return result;
  }

  /**
   * Delete by given composite key
   * 
   * @param type the first level of key
   * @param key the second level of key
   */
  public void delete(String type, String key) {
    SQLiteDatabase db = dbHelper.getWritableDatabase();
    try {
      long count = db.delete(tableName, "type=? and key=?", new String[] {type, key});
      if (count != 1)
        GLog.d(TAG, "delete: didn't delete any rows for:" + type + " " + key);
    } catch (Exception ex) {
      GLog.w(TAG, "delete:" + ex.getMessage());
    }
  }

  private void save(String type, String key, String value, boolean pendingUpload,
      boolean deleteWhenUploaded) {
    ContentValues values = new ContentValues();
    values.put("type", type);
    values.put("key", key);
    values.put("value", value);
    values.put("pendingupload", pendingUpload ? 1 : 0);
    values.put("deletewhenuploaded", deleteWhenUploaded ? 1 : 0);
    values.put("seal", "");
    SQLiteDatabase db = dbHelper.getWritableDatabase();
    try {
      db.beginTransaction();
      @SuppressWarnings("unused")
      long count = db.replace(tableName, null, values);
      db.setTransactionSuccessful();
      db.endTransaction();
    } catch (Exception ex) {
      GLog.d(TAG, "save: " + ex.toString());
    }
  }
}
