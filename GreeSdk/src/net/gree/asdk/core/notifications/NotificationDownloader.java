/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.notifications;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.cache.DiskLruCache;
import net.gree.asdk.core.cache.DiskLruCache.Editor;
import net.gree.asdk.core.notifications.ui.NotificationAppsAdapter;
import net.gree.asdk.core.notifications.ui.NotificationFriendsAdapter;
import net.gree.asdk.core.request.GreeHttpExecuterFactory;
import net.gree.asdk.core.request.IGreeHttpExecuter;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.request.helper.OsRequest;
import net.gree.asdk.core.storage.JSONStorage;

import org.apache.http.HeaderIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;

/**
 * Downloads notification board contents
 */
public class NotificationDownloader {
  public final static int APP_VERSION = 1;
  public final static String CACHE_DIR = "notification";
  public final static long CACHE_MAX_SIZE = 1024*1024;
  public final static String CACHE_KEY = "notification_json_all";
  
  private final static long CACHE_EXPIRE_TIME = 1000 * 60 * 60 * 24 * 7;
  
  private final static String TAG = NotificationDownloader.class.getName();
  private OnResponseCallback<String> mListener;
  private Context mContext;
  private OnImageDownload mOnImageDownload;

  public NotificationDownloader(Context context, OnResponseCallback<String> listener){
    init(context, listener, null);
  }

  public NotificationDownloader(Context context) {
    init(context, null, null);
  }

  public NotificationDownloader(Context context, OnResponseCallback<String> listener, OnImageDownload onImg){
    init(context, listener, onImg);
  }

  private void init(Context context, OnResponseCallback<String> listener, OnImageDownload onImg) {
    mListener = listener;
    mContext = context;    
    mOnImageDownload = onImg;
  }


  /**
   * Call os-server/api/rest/notification/@app API
   * @param query HTTP GET query parameters. It will be expanded to &key=value format.
   */
  public void execute(Map<String, Object> query){
    OsRequest osRequest = new OsRequest();
    osRequest.requestGet(query, "/notification/@app", new OnResponseCallback<String>(){
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, String response) {
          if (mListener != null) { mListener.onSuccess(responseCode, headers, response); }

          ResponseProcessor processor = new ResponseProcessor(mContext, response, mOnImageDownload);
          processor.execute();
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          GLog.e(TAG, response);
          if (mListener != null) { mListener.onFailure(responseCode, headers, response); }
        }});
  }

  /**
   * Parse response and insert into SQLite database
   */
  private static class ResponseProcessor extends AsyncTask<Void, Void, Void> {
    public ResponseProcessor(Context context, String response, OnImageDownload onImg) {
      mResponse = response;
      mEntries = new ArrayList<ContentValues>();
      mContext = context;
      mOnImageDownload = onImg;
    }

    private String mResponse;
    private Context mContext;
    private List<ContentValues> mEntries;
    private OnImageDownload mOnImageDownload;
    private Handler mHandler = new Handler();

    @Override
    protected Void doInBackground(Void... params) {
      JSONStorage storage = Injector.getInstance(JSONStorage.class);
      SQLiteDatabase db = storage.getWritableDatabase();
      
      try {
        JSONObject entry = (new JSONObject(mResponse)).getJSONObject("entry");

        try {
          processJSON(storage, entry, "invite");
        } catch(JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        try {
          saveJSON(entry);
        } catch (IOException e) {
          GLog.printStackTrace(TAG, e);
        }
        
        String[] selectionTarget = {NotificationAppsAdapter.FILTER_TARGET};
        String[] selectionOthers = {NotificationAppsAdapter.FILTER_OTHERS};
        storage.delete(JSONStorage.COLUM_FILTER + " = ?", selectionTarget);
        storage.delete(JSONStorage.COLUM_FILTER + " = ?", selectionOthers);
        
        try {
          processJSON(storage, entry, "target");
        } catch(JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        try {
          processJSON(storage, entry, "other");
        } catch(JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        try {
          processJSON(storage, entry, "activity");
        } catch(JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        try {
          processJSON(storage, entry, "friend");
        } catch(JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        String[] selection = { NotificationFriendsAdapter.FILTER_FRIENDS };
        storage.delete(JSONStorage.COLUM_FILTER + " = ?", selection);
        
        storage.replace(mEntries);
        
        String cacheLimitTime = String.valueOf(System.currentTimeMillis() - CACHE_EXPIRE_TIME);
        String[] whereArgs = {cacheLimitTime};
        db.delete(JSONStorage.TABLE_NAME, JSONStorage.COLUM_DATE + " <= ?", whereArgs);

        for (ContentValues cv : mEntries) {
          try {
            downloadImage(cv);
          } catch (RuntimeException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
        
        storage.replace(mEntries);

      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }



      return null;
    }

    private void processJSON(JSONStorage storage, JSONObject entryRoot, String kind) throws JSONException {
      JSONObject activity = entryRoot.getJSONObject(kind);
      JSONArray entries = activity.getJSONArray("entries");
      for (int i = 0; i < entries.length(); ++i) {
        JSONObject entry = entries.getJSONObject(i);
        ContentValues cv = new ContentValues();
        cv.put(JSONStorage.COLUM_FILTER, "notification_" + kind);
        cv.put(JSONStorage.COLUM_JSON, entry.toString());
        cv.put(JSONStorage.COLUM_KEY, entry.getString("feed_key"));
        Calendar date = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        date.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
          date.setTime(df.parse(entry.getString("date")));
        } catch (ParseException e) {
          date.setTimeInMillis(System.currentTimeMillis());
        }

        String dateString = String.valueOf(date.getTimeInMillis());
        cv.put(JSONStorage.COLUM_DATE, dateString);

        mEntries.add(cv);
      }
    }

    private void saveJSON(JSONObject entry) throws IOException{
      File dir = mContext.getDir(CACHE_DIR, Activity.MODE_PRIVATE);
      DiskLruCache cache = null;
      try {
        cache = DiskLruCache.open(dir, APP_VERSION, 1, CACHE_MAX_SIZE);
        Editor edit = cache.edit(CACHE_KEY);
        edit.set(0, entry.toString());
        cache.flush();
        edit.commit();
        cache.flush();
      } catch (IOException e) {
        GLog.printStackTrace(TAG, e);
      } finally {
        if (cache != null) { cache.close(); }
      }
    }
    
    private void downloadImage(ContentValues cv) {
      String jsonStr = cv.getAsString(JSONStorage.COLUM_JSON);
      try {
        JSONObject entry = new JSONObject(jsonStr);
        String url = entry.getString("image");
        if (TextUtils.isEmpty(url)) { return; }

        HttpUriRequest method = new HttpGet(url);
        IGreeHttpExecuter executer = GreeHttpExecuterFactory.getExecutor();

        HttpResponse response = executer.execute(method);
        if (200 != response.getStatusLine().getStatusCode()) { return; }

        InputStream is = response.getEntity().getContent();
        final Bitmap bmp = BitmapFactory.decodeStream(is);
        byte[] blob = getByteFromBitmap(bmp);
        cv.put(JSONStorage.COLUM_BLOB, blob);

        final String feedKey = entry.getString("feed_key");
        mHandler.post(new Runnable(){
          @Override
          public void run() {
            if (mOnImageDownload != null) { mOnImageDownload.onDownload(feedKey, bmp); }
          }
        });

      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      } catch (IllegalStateException e) {
        GLog.printStackTrace(TAG, e);
      } catch (IOException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    private byte[] getByteFromBitmap(Bitmap bmp) {
      if (bmp == null) {
        return null;
      }
      byte[] ret = null;
      ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
      bmp.compress(CompressFormat.PNG, 100, byteStream);
      ret = byteStream.toByteArray();
      return ret;
    }


  }

  /**
   * Callback interface. called when a image is downloaded
   */
  public interface OnImageDownload {
    /**
     * Callback method
     * @param feedKey unique key of the notification feed
     * @param image image instance
     */
    public void onDownload(String feedKey, Bitmap image);
  }
}
