/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.widget;

import net.gree.asdk.R;
import net.gree.asdk.core.GLog;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;
import net.gree.asdk.core.widget.WidgetService;

/**
 * Widget provider
 * 
 * @author GREE, Inc.
 * 
 */
public class WidgetProvider extends AppWidgetProvider {
  private static String TAG = "GreeWidgetProvider";
  private static RemoteViews mHeaderView = null;
  private static int[] mWidgetIds;
  private static String[] mFilter;
  private static boolean mUpdateing;

  public static final String WIDGET_SETTING_ACTION = "net.gree.asdk.core.widget.SETTING";
  public static final String WIDGET_UPDATE_ACTION = "net.gree.asdk.core.widget.UPDATE";
  public static final String WIDGET_SET_HEADER_ACTION = "net.gree.asdk.core.widget.SET_HEADER";
  public static final String WIDGET_SET_FILTER_ACTION = "net.gree.asdk.core.widget.SET_FILTER";
  public static final String WIDGET_LAUNCH_ACTIVITY = "net.gree.asdk.core.widget.LAUNCH_ACTIVITY";

  public static final String EXTRA_CUSTOM_HEADER_OBJECT = "custom_header_object";
  public static final String EXTRA_LAUNCH_APP_ID = "app_id";
  public static final String EXTRA_WIDGET_FILTER = "selection";

  @Override
  public void onEnabled(Context context) {
    super.onEnabled(context);
    GLog.i(TAG, "[ENABLE WIDGET]");
  }

  @SuppressLint("NewApi")
  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
    GLog.i(TAG, "[UPDATE WIDGET]");
    super.onUpdate(context, appWidgetManager, appWidgetIds);
    mWidgetIds = appWidgetIds;
    Intent intent = new Intent();
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
      for (int i = 0; i < appWidgetIds.length; ++i) {
        RemoteViews rv =
            new RemoteViews(context.getPackageName(), R.layout.gree_widget_list_layout);
        // set Adapter
        intent.setClassName(context, WidgetRemoteViewService.class.getName());
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mWidgetIds);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        rv.setRemoteAdapter(R.id.gree_widget_list, intent);
        final Intent settingIntent = new Intent(context, WidgetConfigActivity.class);
        settingIntent.setAction(Intent.ACTION_VIEW);
        settingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (mFilter != null) {
          GLog.i(TAG, "[ADD PENDING FILTER INTENT]");
          settingIntent.putExtra(EXTRA_WIDGET_FILTER, mFilter);
        }
        PendingIntent pIntent = PendingIntent.getActivity(context, i, settingIntent, 0);
        rv.setOnClickPendingIntent(R.id.gree_widget_setting, pIntent);
        if (mHeaderView != null) {
          GLog.i(TAG, "[ADD HEADERVIEW]");
          rv.addView(R.id.gree_widget_header, mHeaderView);
        }
        final Intent onClickIntent = new Intent(context, WidgetProvider.class);
        onClickIntent.setAction(WidgetProvider.WIDGET_LAUNCH_ACTIVITY);
        onClickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
        onClickIntent.setData(Uri.parse(onClickIntent.toUri(Intent.URI_INTENT_SCHEME)));
        final PendingIntent onClickPendingIntent =
            PendingIntent
                .getBroadcast(context, 0, onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        rv.setPendingIntentTemplate(R.id.gree_widget_list, onClickPendingIntent);
        appWidgetManager.updateAppWidget(appWidgetIds[i], rv);
      }
      startWidgetService(context);
    } else {
      intent.putExtra("layout", "gree_widget_default_layout");
      intent.setClassName(context, WidgetService.class.getName());
      context.startService(intent);
    }
    mUpdateing = false;
  }

  @Override
  public void onDeleted(Context context, int[] appWidgetIds) {
    super.onDeleted(context, appWidgetIds);
    GLog.i(TAG, "Delete "+appWidgetIds.length);
  }

  @Override
  public void onDisabled(Context context) {
    super.onDisabled(context);
    GLog.i(TAG, "[DISABLE WIDGET]");
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
      return;
    }
    if (WIDGET_UPDATE_ACTION.equals(action)) {
      if (intent.hasExtra(EXTRA_CUSTOM_HEADER_OBJECT)) {
        GLog.i(TAG, "[GET HEADERVIEW]");
        mHeaderView = intent.getParcelableExtra(EXTRA_CUSTOM_HEADER_OBJECT);
      }
      update(context, action);
    } else if (WIDGET_SET_HEADER_ACTION.equals(action)) {
      if (intent.hasExtra(EXTRA_CUSTOM_HEADER_OBJECT)) {
        GLog.i(TAG, "[GET HEADERVIEW]");
        mHeaderView = intent.getParcelableExtra(EXTRA_CUSTOM_HEADER_OBJECT);
      }
      update(context, action);
    } else if (WIDGET_SET_FILTER_ACTION.equals(action)) {
      if (intent.hasExtra(EXTRA_WIDGET_FILTER)) {
        mFilter = intent.getStringArrayExtra(EXTRA_WIDGET_FILTER);
      }
    } else if (WIDGET_LAUNCH_ACTIVITY.equals(action)) {
    } else if (WIDGET_SETTING_ACTION.equals(action)) {
      if (intent.hasExtra(EXTRA_WIDGET_FILTER)) {
        mFilter = intent.getStringArrayExtra(EXTRA_WIDGET_FILTER);
      }
      update(context, action);
    }
    super.onReceive(context, intent);
  }

  public void startWidgetService(Context context) {
    Intent intent = new Intent();
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
      intent.setClassName(context, WidgetRemoteViewService.class.getName());
      intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mWidgetIds);
      intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
    } else {
    }
    if (mFilter != null) {
      intent.putExtra(EXTRA_WIDGET_FILTER, mFilter);
    }
    if (mHeaderView != null) {
      intent.putExtra(WidgetProvider.EXTRA_CUSTOM_HEADER_OBJECT, mHeaderView);
    }
    context.startService(intent);
  }

  @TargetApi(11)
  private void update(Context context, String action) {
    if (mUpdateing) {
      return;
    }
    mUpdateing = true;
    AppWidgetManager wm = AppWidgetManager.getInstance(context);
    ComponentName cn = new ComponentName(context, WidgetProvider.class);
    mWidgetIds = wm.getAppWidgetIds(cn);
    if (WIDGET_SET_HEADER_ACTION.equals(action) || WIDGET_SETTING_ACTION.equals(action)) {
      Intent i = new Intent();
      i.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
      i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, mWidgetIds);
      context.sendBroadcast(i);
      startWidgetService(context);
    } else if (WIDGET_UPDATE_ACTION.equals(action)) {
      wm.notifyAppWidgetViewDataChanged(mWidgetIds, R.id.gree_widget_list);
      mUpdateing = false;
    }
  }
}
