/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.util.TreeMap;

import net.gree.asdk.api.auth.Authorizer;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.Session;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.cache.ImageCache;
import net.gree.asdk.core.cache.ImageFetcher;
import net.gree.asdk.core.dashboard.UniversalMenuAdapter.BindData;
import net.gree.asdk.core.dashboard.UniversalMenuAdapter.ProfileData;
import net.gree.asdk.core.dashboard.UniversalMenuAdapter.SeeMoreData;
import net.gree.asdk.core.dashboard.UniversalMenuAdapter.ItemData;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.storage.JSONStorage;
import net.gree.asdk.core.ui.GreeWebViewUtil;
import net.gree.asdk.core.util.Scheme;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ViewFlipper;

class UniversalMenuView extends ViewFlipper {

  @SuppressWarnings("unused")
  private static final String LOG_TAG = "UniversalMenuView";
  private static final int WIDTH_PIX = 270;
  private static final int BUTTON_MARGIN_DP = 6;
  
  private static final int COMMAND_OPEN_FROM_MENU = 1;
  private static final int COMMAND_LAUNCH_NATIVE_APP = 2;
  private static final int COMMAND_LAUNCH_NATIVE_BROWSER = 3;
  private static final int COMMAND_NEED_UPGADE = 4;


  private boolean mIsShown = false;
  private boolean mIsRootMenu = true;
  private int mButtonWidthPix = 0;
  private int mWidth = 0;
  private DashboardActivity mDashboardActivity;
  private UniversalMenuAdapter mPrimaryAdapter;
  private UniversalMenuAdapter mSecondaryAdapter;
  private ImageFetcher mPrimaryImageFetcher;
  private ImageFetcher mSecondaryImageFetcher;

  private OnItemClickListener PrimarylistItemClickListener = new OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      BindData data = (BindData) mPrimaryAdapter.getItem(position);
      clickCommandHandler(data);
    }
  };
  
  private OnItemClickListener SecondarylistItemClickListener = new OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      BindData data = (BindData) mSecondaryAdapter.getItem(position);
      clickCommandHandler(data);
    }
  };

  private void clickCommandHandler(BindData data) {
    if (data instanceof SeeMoreData) {
      ViewFlipper flipper = (ViewFlipper) findViewById(RR.id("flipper"));
      flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(),
        RR.anim("gree_universalmenu_in_from_right")));
      flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),
        RR.anim("gree_universalmenu_out_to_left")));
      flipper.showNext();
      loadMore();
    }
    if (data instanceof ItemData) {
      ItemData item = (ItemData)data;
      JSONObject params = item.mParams;
      if (item.mCommandType == COMMAND_OPEN_FROM_MENU) {
        mDashboardActivity.openFromMenu(params.optString("url"));
      } else if (item.mCommandType == COMMAND_LAUNCH_NATIVE_APP) {
        launchNativeApp(getContext(), params);
      } else if (item.mCommandType == COMMAND_LAUNCH_NATIVE_BROWSER) {
        launchNativeBrowser(getContext(), params);
      } else if (item.mCommandType == COMMAND_NEED_UPGADE) {
        needUpgrade(getContext(), params);
      }
    }
    if (data instanceof ProfileData) {
      ProfileData profile = (ProfileData)data;
      JSONObject params = profile.mParams;
      if (profile.mCommandType == COMMAND_OPEN_FROM_MENU) {
        mDashboardActivity.openFromMenu(params.optString("url"));
      } else if (profile.mCommandType == COMMAND_LAUNCH_NATIVE_APP) {
        launchNativeApp(getContext(), params);
      } else if (profile.mCommandType == COMMAND_LAUNCH_NATIVE_BROWSER) {
        launchNativeBrowser(getContext(), params);
      } else if (profile.mCommandType == COMMAND_NEED_UPGADE) {
        needUpgrade(getContext(), params);
      }
      
    }
  }

  public UniversalMenuView(Context context) {
    super(context);
  }

  public UniversalMenuView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  /**
   * initialize of Universal menu 
   * @param buttonWidthPix open Universalmenu button pix
   * @param activity Dashobard Activity
   */
  public void initialize(int buttonWidthPix, DashboardActivity activity) {
    mDashboardActivity = activity;
    
    mButtonWidthPix = buttonWidthPix;
    setUpWidth();
    LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(RR.layout("gree_universalmenu_layout"), null);
    addView(view);
    
    mPrimaryImageFetcher = createImageFetcher();
    mSecondaryImageFetcher = createImageFetcher();

    ListView list1 = (ListView) findViewById(RR.id("gree_universalmenu_primary_list"));
    mPrimaryAdapter = new UniversalMenuAdapter(getContext(), mPrimaryImageFetcher);
    list1.setOnItemClickListener(PrimarylistItemClickListener);
    list1.setAdapter(mPrimaryAdapter);
    
    ListView list2 = (ListView) findViewById(RR.id("gree_universalmenu_secondary_list"));
    mSecondaryAdapter = new UniversalMenuAdapter(getContext(), mSecondaryImageFetcher);
    list2.setOnItemClickListener(SecondarylistItemClickListener);
    list2.setAdapter(mSecondaryAdapter);

    new Thread(new Runnable() {
      @Override
      public void run() {
        JSONStorage storage = Injector.getInstance(JSONStorage.class);
        SQLiteDatabase db = storage.getReadableDatabase();
        Cursor cursor = db.query(JSONStorage.TABLE_NAME, storage.getColums(), JSONStorage.COLUM_FILTER + " = ?", new String[]{"universalmenu_data"}, null, null, null, String.valueOf(1));
        if (cursor.getCount() > 0) {
          cursor.moveToFirst();
          final String jsonString = cursor.getString(JSONStorage.COLUM_INDEX_JSON);
          try {
            JSONObject obj = new JSONObject(jsonString);
            setUniversalMenuData(obj);
          } catch (JSONException e) {
            GLog.printStackTrace(LOG_TAG, e);
          }
        }
        cursor.close();
      }
    }).start();

    loadUniversalMenu();
  }

  private ImageFetcher createImageFetcher() {
    ImageFetcher imageFetcher = new ImageFetcher(getContext());
    ImageCache.Params cacheParams = new ImageCache.Params("UniversalMenu");
    cacheParams.mEnableDiskCache = false;
    cacheParams.mMemCacheSize = 3 * 1024 * 1024;
    cacheParams.mEnableMemCache = true;
    ImageCache imageCache = new ImageCache(getContext(), cacheParams);
    imageFetcher.setImageCache(imageCache);
    return imageFetcher;
  }
  
  public void onDestroy() {
    mPrimaryImageFetcher.clearAll();
    mPrimaryImageFetcher.clearBitmap( findViewById(RR.id("gree_universalmenu_primary_list")));
    mPrimaryImageFetcher.setImageCache(null);

    mSecondaryImageFetcher.clearAll();
    mSecondaryImageFetcher.clearBitmap(findViewById(RR.id("gree_universalmenu_secondary_list")));
    mSecondaryImageFetcher.setImageCache(null);
    
  }

  /**
   * load UniversalMenu Data used snsapi
   */
  private void loadUniversalMenu() {
    String requestString = Core.isGreeApp()
        ? "{\"jsonrpc\":\"2.0\",\"method\":\"UniversalMenu.get\",\"params\":{\"show_game_dashboard\":false},\"id\":1, renderer:\"native\"}"
        : "{\"jsonrpc\":\"2.0\",\"method\":\"UniversalMenu.get\",\"params\":{\"show_game_dashboard\":true},\"id\":1, renderer:\"native\"}";
    SnsApi api = new SnsApi();
    try {
      api.request(new JSONObject(requestString), new SnsApi.SnsApiListener() {
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, final String result) {
          try {
            JSONObject obj = new JSONObject(result);

            mPrimaryAdapter.clear();
            mSecondaryAdapter.clear();
            setUniversalMenuData(obj);
            mPrimaryAdapter.notifyDataSetChanged();
            mSecondaryAdapter.notifyDataSetChanged();

            new Thread(new Runnable() {
              @Override
              public void run() {
                JSONStorage storage = Injector.getInstance(JSONStorage.class);
                ContentValues cv = new ContentValues();
                cv.put(JSONStorage.COLUM_FILTER, "universalmenu_data");
                cv.put(JSONStorage.COLUM_DATE, String.valueOf(System.currentTimeMillis()));
                cv.put(JSONStorage.COLUM_JSON, result);
                cv.put(JSONStorage.COLUM_KEY, "key");
                storage.replace(cv);
              }
            }).start();
          } catch (JSONException e) {
            GLog.printStackTrace(LOG_TAG, e);
          }
        }
        
        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String errorObject) {
          GLog.d(LOG_TAG, "fail:" + errorObject);
        }
      });
    } catch (JSONException e) {
      GLog.printStackTrace(LOG_TAG, e);
    }
  }

  /**
   * whether open root Universal Menu View or not.
   * If run pushView of UniversalMenu, this metheod return false.
   * @return whether open root Universal Menu View or not.
   */
  public boolean isRootMenu() {
    return mIsRootMenu;
  }

  /**
   * if call pushView, must call this method.
   */
  public void loadMore() {
    mIsRootMenu = false;
    Logger.recordLog("pg", "universalmenu_top", "universalmenu_sns_top", null);
  }

  /**
   * load Root Universal menu.
   */
  public void loadRootMenu() {

    if (mIsRootMenu) {
      return;
    }
    ViewFlipper flipper = (ViewFlipper)findViewById(RR.id("flipper"));
    flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(),
      RR.anim("gree_universalmenu_in_from_left")));
    flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(),
      RR.anim("gree_universalmenu_out_to_right")));
    flipper.showPrevious();
    mIsRootMenu = true;
  }


  /**
   * set open Universal Menu button pix
   */
  public void setUpWidth() {

    int preferredUmWidth = (int) (WIDTH_PIX * this.getResources().getDisplayMetrics().density);

    DisplayMetrics dm = this.getResources().getDisplayMetrics();
    int minRightPadding = mButtonWidthPix + (int) (BUTTON_MARGIN_DP * dm.density);
    int maxWidth = dm.widthPixels - minRightPadding;

    mWidth = Math.min(maxWidth, preferredUmWidth);

    setPadding(0, 0, dm.widthPixels - mWidth, 0);
  }

  /**
   * get Universal Menu View width
   * @return
   */
  public int getMenuWidth() {
    return mWidth;
  }

  /**
   * reload of Universal Menu
   */
  public void reload() {
    loadUniversalMenu();
  }

  /**
   * set Visibility of Universal Menu.
   * @param isShown visibility of Universal Menu
   */
  public void setShown(boolean isShown) {
    mIsShown = isShown;
    if (mIsShown) {
      setVisibility(View.VISIBLE);
    } else {
      setVisibility(View.GONE);
    }
  }

  /**
   * get Visibility of Universal Menu.
   */
  public boolean isShown() {
    return mIsShown;
  }

  /**
   * Set Universal Menu Data
   * @param obj JSON Object of response SnsApi 
   * @throws JSONException JSONException
   */
  private void setUniversalMenuData(JSONObject obj) throws JSONException {
    JSONObject json_result = obj.getJSONObject("result");
    JSONObject profile = json_result.getJSONObject("profile");
    String profile_image_url = null;
    if (profile.optJSONObject("image") != null) {
      profile_image_url = profile.getJSONObject("image").optString("url");
    }
    int type = 0;
    JSONObject params = null;
    JSONObject on_click_object = profile.optJSONObject("on_click") ;
    if( on_click_object != null) {
      type = getUniversalMenuItemCommmandType(on_click_object);
      params = getUniversalMenuParams(on_click_object);
    }
    mPrimaryAdapter.addProfile(profile.optString("name"), profile.optString("nickname"), profile_image_url, type, params);

    JSONArray sections = json_result.getJSONArray("sections");
    for (int i = 0; i < sections.length(); i++) {
      JSONObject section = sections.getJSONObject(i);
      if (!section.getString("header").equals("")) {
        mPrimaryAdapter.addHeader(section.getString("header"));
      }
      JSONArray rows = section.getJSONArray("rows");
      for (int j = 0; j < rows.length(); j++) {
        JSONObject row = rows.getJSONObject(j);

        addUniversalMenuItem(row, mPrimaryAdapter, true);
      }

      JSONArray rows_more = section.optJSONArray("rows_more");
      String id = section.optString("id");
      if (rows_more != null) {
        if (id.equals("sns")) {
          mPrimaryAdapter.addSeeMore();
          for (int j = 0; j < rows.length(); j++) {
            try {
              JSONObject item = rows.getJSONObject(j);
              addUniversalMenuItem(item, mSecondaryAdapter, true);
            } catch (JSONException e) {
              GLog.printStackTrace(LOG_TAG, e);
            }
          }
          for (int j = 0; j < rows_more.length(); j++) {
            try {
              JSONObject item = rows_more.getJSONObject(j);
              addUniversalMenuItem(item, mSecondaryAdapter, true);
            } catch (JSONException e) {
              GLog.printStackTrace(LOG_TAG, e);
            }
          }
        } else {
          JSONObject last = rows_more.optJSONObject(rows_more.length() - 1);
          addUniversalMenuItem(last, mPrimaryAdapter, false);
        }
      }
    }
  }


  /**
   * add Universal Menu Item into ListAdapter
   * @param row json object of response SnsApi
   * @param adapter adapter for data set
   * @param isThumbnail whether need thumbnail or not.
   * @throws JSONException JSONException
   */
  private void addUniversalMenuItem(JSONObject row, UniversalMenuAdapter adapter, boolean isThumbnail) throws JSONException {
    JSONObject on_click_object = row.optJSONObject("on_click") ;
    int type = 0;
    JSONObject params = null;
    if( on_click_object != null) {
      type = getUniversalMenuItemCommmandType(on_click_object);
      params = getUniversalMenuParams(on_click_object);
    } else {
      isThumbnail = false;
    }
    String image_url = null;
    if (row.optJSONObject("image") != null) {
      image_url = row.getJSONObject("image").optString("url");
    }
    int badge = row.optInt("badge");
    adapter.addItem(row.optString("title"),image_url, badge, type,  params, isThumbnail);
  }

  /**
   * get params for command
   * @param on_click_object json object of response SnsApi
   * @return params of command
   */
  private JSONObject getUniversalMenuParams(JSONObject on_click_object) {
    JSONObject params = null;
    if (on_click_object.optJSONObject("params") != null) {
      params = on_click_object.optJSONObject("params");
    }
    return params;
  }

  /**
   * get command type of tapping Universal Menu item
   * @param on_click_object json object of response SnsApi
   * @return command type
   */
  private int getUniversalMenuItemCommmandType(JSONObject on_click_object) {
    int type = 0;
    if (on_click_object.optString("command") != null) {
      if (on_click_object.optString("command").equals("open_from_menu")) {
        type = COMMAND_OPEN_FROM_MENU;
      } else if (on_click_object.optString("command").equals("launch_native_app")) {
        type = COMMAND_LAUNCH_NATIVE_APP;
      } else if (on_click_object.optString("command").equals("launch_native_browser")) {
        type = COMMAND_LAUNCH_NATIVE_BROWSER;
      } else if (on_click_object.optString("command").equals("need_upgrade")) {
        type = COMMAND_NEED_UPGADE;
      }
    }
    return type;
  }
  
  /**
   * same as launchNativeApp command
   * @param context context
   * @param params command's params
   */
  private void launchNativeApp(Context context, JSONObject params) {
    GreeWebViewUtil.launchNativeApplication(context, params);

    String url = params.optString("URL");
    if (url == null || !url.startsWith(Scheme.getAppScheme())) {
      return;
    }
    String strs[] = url.split(":");
    if (strs.length < 1) {
      return;
    }
    if (!strs[0].startsWith("greeappopen")) {
      return;
    }
    String appid = strs[0].substring("greeappopen".length());
    if (appid == null) {
      return;
    }
    TreeMap<String, String> map = new TreeMap<String, String>();
    map.put("app_id", appid);
    Logger.recordLog("evt", "boot_app", "universalmenu_top", map);
   }
  
  /**
   * same as launchNativeBrowser command
   * @param context context
   * @param params command's params
   */
  private void launchNativeBrowser(Context context,JSONObject params) {
    GreeWebViewUtil.launchNativeBrowser(context, params);

    String url = params.optString("URL");
    if (url == null) {
      return;
    }
    String[] strs = url.split("/");
    if (strs.length < 4) {
      return;
    }
    String appid = strs[3];
    if (appid == null) {
      return;
    }
    TreeMap<String, String> map = new TreeMap<String, String>();
    map.put("app_id", appid);
    Logger.recordLog("evt", "boot_app", "universalmenu_top", map);
  }

  /**
   * same as needUpgrade command
   * @param context context
   * @param params command's params
   */
  private void needUpgrade(final Context context, JSONObject params) {
    int targetGrade = 0;
    final String targetGradeStr = "target_grade";
    if (params.has(targetGradeStr)) {
      try {
        targetGrade = Integer.parseInt(params.getString(targetGradeStr));
      } catch (NumberFormatException e) {
        GLog.printStackTrace(LOG_TAG, e);
      } catch (JSONException e) {
        GLog.printStackTrace(LOG_TAG, e);
      }
    }
    String serviceCode = null;
    try {
      serviceCode = params.getString("service_code");
    } catch (JSONException e) {
      GLog.printStackTrace(LOG_TAG, e);
    }
    IAuthorizer authorizer = Injector.getInstance(IAuthorizer.class);
    authorizer.upgrade(context, targetGrade, serviceCode,
      new Authorizer.UpgradeListener() {
        @Override
        public void onUpgrade() {
          new Session().refreshSessionId(context, new OnResponseCallback<String>() {
            @Override
            public void onSuccess(int responseCode, HeaderIterator headers, String response) {
            }
            @Override
            public void onFailure(int responseCode, HeaderIterator headers, String response) {
            }
          });
        }
        @Override
        public void onCancel() {
        }
        @Override
        public void onError() {
        }
    }, null);
  }

}
