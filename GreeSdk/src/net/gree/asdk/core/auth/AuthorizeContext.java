/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.net.Uri;

import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.DeviceInfo;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.codec.Base64;

public class AuthorizeContext {
  private static final String TAG = "AuthorizeContext";

  private static String getJWSHeader() {
    String header = "{\"typ\":\"JWT\",\"alg\":\"HS256\"}";
    String base64header = Base64.encodeBytes(header.getBytes());
    String encoded = base64header.replace("+", "-").replace("/", "_").replace("=", "");
    return encoded;
  }

  private static String getJWSPayload(String[] key, String hkey) {
    StringBuilder jsonString = new StringBuilder("{\"key\":[");
    int len = key.length;
    for (int i = 0; i < len; i++) {
      jsonString.append('"').append(key[i]).append('"');
      if (i+1 < len) {
        jsonString.append(',');
      }
    }
    jsonString.append("],");
    if (hkey != null) {
      jsonString.append("\"hkey\":\"").append(hkey).append("\",");
    }
    long timestamp = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());
    jsonString.append("\"timestamp\":").append(timestamp).append('}');
    String base64payload = Base64.encodeBytes(jsonString.toString().getBytes());
    String encoded = base64payload.replace("+", "-").replace("/", "_").replace("=", "");
    return encoded;
  }

  private static String getJWSSignature(String source) {
    byte[] sig = hmacsha256(source);
    String base64sig = Base64.encodeBytes(sig);
    String encoded = base64sig.replace("+", "-").replace("/", "_").replace("=", "");
    return encoded;
  }

  private static byte[] hmacsha256(String data) {
    byte[] hash = null;
    String consumer_key = CoreData.get(InternalSettings.ConsumerSecret);
    byte[] key = consumer_key.getBytes();
    SecretKeySpec secretkey = new SecretKeySpec(key, "HMAC-SHA256");
    Mac mac;
    try {
      mac = Mac.getInstance("HMAC-SHA256");
      mac.init(secretkey);
      hash = mac.doFinal(data.getBytes());
    } catch (NoSuchAlgorithmException e) {
      GLog.printStackTrace(TAG, e);
    } catch (InvalidKeyException e) {
      GLog.printStackTrace(TAG, e);
    }
    return hash;
  }

  private static String getJWSKey(String[] key) {
    String jwsheader = getJWSHeader();
    String macaddress = null;
    if (DeviceInfo.isSendableMacAddress()) {
      macaddress = DeviceInfo.getMacAddress();
    }
    String jwspayload = getJWSPayload(key, macaddress);
    StringBuilder data = new StringBuilder(jwsheader).append('.').append(jwspayload);
    String jwssig = getJWSSignature(data.toString());
    StringBuilder userkey = new StringBuilder(jwsheader).append('.').append(jwspayload).append('.').append(jwssig);
    return userkey.toString();
  }

  public static String getUserKey() {
    if (DeviceInfo.isRooted()) return "";
    ArrayList<String> keys = new ArrayList<String>();
    if (DeviceInfo.isSendableAndroidId()) {
      String androidid = DeviceInfo.getUdid();
      keys.add(androidid);
    }
    String uuid = DeviceInfo.getUuid();
    keys.add(uuid);
    return getJWSKey(keys.toArray(new String[0]));
  }

  public static String appendQueryParameter(String url) {
    return appendQueryParameter(url, getUserKey());
  }

  public static String appendQueryParameter(String url, String userKey) {
    Uri uri = Uri.parse(url);
    if (uri.getScheme().startsWith("http")) {
      if (uri.getQueryParameter("context") == null) {
        StringBuilder newUrl = new StringBuilder(url);
        if (uri.getQuery() == null) {
          newUrl.append("?context=");
        }
        else {
          newUrl.append("&context=");
        }
        newUrl.append(userKey);
        return newUrl.toString();
      }
    }
    return url;
  }
}
