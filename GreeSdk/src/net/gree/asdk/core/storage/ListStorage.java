/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.storage;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import net.gree.asdk.core.storage.DbHelper;

/**
 * class for perpetuation of list value 
 * @author  GREE, Inc
 *
 */
public class ListStorage {

  private DbHelper mDbHelper;

  private static final String DB_NAME = "gree_list_stroage.db";
  private static final String TABLE_NAME = "list_storage";
  private static final String COLUM_ID = "id";
  private static final String COLUM_VALUE = "value";

  /**
   * constructor of EmojiPaletteRecentStorage
   * @param context activity context.
   */
  public ListStorage(Context context) {
    mDbHelper =
        new DbHelper(context,
          DB_NAME,
          1,
          TABLE_NAME,
          "CREATE TABLE "
              + TABLE_NAME
              + "(" + COLUM_ID + " integer primary key autoincrement, "
              + COLUM_VALUE + " text not null)");
  }

  /**
   * get value list from Database
   * @param list value array for getting.
   */
  public void getListValue(ArrayList<String> list) {
    SQLiteDatabase db = mDbHelper.getReadableDatabase();
    String[] colums = {COLUM_ID, COLUM_VALUE};
    Cursor cursor = db.query(TABLE_NAME, colums, null, null, null, null, COLUM_ID);
    while (cursor.moveToNext()) {
      list.add(cursor.getString(1));
    }
    cursor.close();
    db.close();
  }

  /**
   * put value to Database
   * @param list value array for putting.
   */
  public void putListValue(ArrayList<String> list) {
    SQLiteDatabase db = mDbHelper.getWritableDatabase();
    db.delete(TABLE_NAME, null, null);
    for (String value : list) {
      ContentValues values = new ContentValues();
      values.put(COLUM_VALUE, value);
      db.insert(TABLE_NAME, null, values);
    }
    db.close();
  }

  public void clear() {
    SQLiteDatabase db = mDbHelper.getWritableDatabase();
    db.delete(TABLE_NAME, null, null);
    db.close();
  }
}
