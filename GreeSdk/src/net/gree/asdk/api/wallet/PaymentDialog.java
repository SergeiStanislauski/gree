/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api.wallet;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.KeyEvent;
import android.webkit.WebView;

import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.wallet.Payment.PaymentListener;
import net.gree.asdk.api.wallet.Payment.VerifyListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.ui.CommandInterface;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.ui.GreeWebView;
import net.gree.asdk.core.ui.WebViewPopupDialog;
import net.gree.asdk.core.ui.ProgressDialog;
import net.gree.asdk.core.util.Scheme;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.wallet.Deposit;
import net.gree.asdk.core.wallet.Deposit.ResendingOrderListener;

/**
 * Dialog class for displaying the dialog for purchasing items
 * @author GREE, Inc.
 * @since 2.0.0
 */
final class PaymentDialog extends WebViewPopupDialog {
  private static final String TAG = PaymentDialog.class.getSimpleName();
  private static final String KEY_PAYMENT_ID = "payment_id";
  static final String INSUFFICIENT_COINS_MESSAGE = "User does not have enough coin";
  private String mId;
  private String mUrl;
  private PaymentListener mListener;
  private int mPaymentClosedEvent = Payment.ABORTED;
  private volatile boolean mCanClose = true;
  private String mCurrentPageUrl = null;

  private PopupDialogWebViewClient mWebViewClient;

  public PaymentDialog(Context context, String id, String url, PaymentListener listener) {
    super(context);
    mId = id;
    mUrl = url;
    mListener = listener;

    mPerformData = mManager.createData(PerformanceFlowIndex.PAYMENT);
    //set the dialog type for TaskEventListeners
    mClassType = GreePlatformListener.CLASS_PAYMENT_DIALOG;
  }

  @Override
  protected void createWebViewClient() {
    mWebViewClient = new PaymentDialogWebViewClient(getContext());
  }

  @Override
  protected PopupDialogWebViewClient getWebViewClient() {
    return mWebViewClient;
  }

  @Override
  protected int getOpenedEvent() {
    return Payment.OPENED;
  }

  @Override
  protected int getClosedEvent() {
    return mPaymentClosedEvent;
  }

  @Override
  protected String getEndPoint() {
    return mUrl;
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
      WebView webView = getWebView();
      if (webView.canGoBack()) {
        webView.goBack();
        return true;
      } else if (mCanClose == false) {
        return true;
      } else {
        close(Payment.CANCELLED);
        if (mListener != null) {
          mListener.onCancel(0, null, mId);
        }
        return true;
      }
    }
    return super.onKeyDown(keyCode, event);
  }

  void close(int event) {
    //Set the TaskEventListener event a cancel event.
    if (event == Payment.DONE) {
      mCloseEvent = GreePlatformListener.EVENT_CLOSE;
    } else if (event == Payment.ABORTED) {
      mCloseEvent = GreePlatformListener.EVENT_ERROR;
    } else if (event == Payment.CANCELLED) {
      mCloseEvent = GreePlatformListener.EVENT_CANCEL;
    }
    mPaymentClosedEvent = event;
    setReturnData(mId);
    dismiss();
  }

  @Override
  protected void pushDismissButton() {
    if (mListener != null) {
      mListener.onCancel(0, null, mId);
    }
    super.pushDismissButton();
  }

  @Override
  protected CommandInterface.OnCommandListenerAdapter getOnCommandListener() {
    return new OnPopupCommandListener() {
      @Override
      public void onClose(final CommandInterface commandInterface, final JSONObject params) {
        super.onClose(commandInterface, params);
        if (mListener != null) {
          mListener.onCancel(0, null, mId);
        }
      }

      @Override
      public void onClosePopup(final CommandInterface commandInterface, final JSONObject params) {
        super.onClosePopup(commandInterface, params);
        if (mListener != null) {
          mListener.onCancel(0, null, mId);
        }
      }

      @Override
      public void onConfirmDepositOrderSending(final CommandInterface commandInterface, final JSONObject params) {
        final String callback;
        try {
          callback = params.getString("callback");
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
          return;
        }
        String appid = params.optString("applicationId"); // This appid specify the application which deposit.

        Deposit.showResendingOrderDialog(PaymentDialog.this.mContext, appid, new ResendingOrderListener() {
          public void onNotFound() {
            JSONObject result = new JSONObject();
            try {
              result.put("result", "noreceipt");
              commandInterface.executeCallback(callback, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }

          public void onCompleted() {
            JSONObject result = new JSONObject();
            try {
              result.put("result", "completed");
              commandInterface.executeCallback(callback, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }

          public void onCancelled() {
            JSONObject result = new JSONObject();
            try {
              result.put("result", "cancelled");
              commandInterface.executeCallback(callback, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }

          public void onFailed() {
            JSONObject result = new JSONObject();
            try {
              result.put("result", "failure");
              commandInterface.executeCallback(callback, result);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }
        });
      }
    };
  }

  /**
   * WebViewClient for PaymentDialog
   * @author GREE, Inc
   */
  private class PaymentDialogWebViewClient extends PopupDialogWebViewClient {
    public PaymentDialogWebViewClient(Context context) {
      super(context);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
      mCurrentPageUrl = url;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
      if (url.equals(mUrl)) {
        switchDismissButton(true);
        mCanClose = true;
      }
      if (url.equals(mCurrentPageUrl)) {
        switchDismissButton(true);
        mCanClose = true;
      }
      super.onPageFinished(view, url);
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
      switchDismissButton(true);
      mCanClose = true;
      super.onReceivedError(view, errorCode, description, failingUrl);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      if (url.startsWith(Url.getPaymentPurchaseUrl())) {
        switchDismissButton(false);
        mCanClose = false;
      }
      if (handleCompleteTransaction(url)) { return true; }
      return super.shouldOverrideUrlLoading(view, url);
    }

    private boolean handleCompleteTransaction(String url) {
      if (url.startsWith(Scheme.getCompleteTransactionScheme())) {
        Uri uri = Uri.parse(url);
        if (uri != null) {
          validateTransaction(uri.getQueryParameter(KEY_PAYMENT_ID));
        }
        return true;
      }
      return false;
    }

    private void validateTransaction(String id) {
      final ProgressDialog pd = new ProgressDialog(getContext());
      pd.init(null, null, true);
      pd.show();
      mManager.recordData(mPerformData, PerformanceIndexMap.INDEX_KEY_POST_START);
      Payment.verify(id, new VerifyListener() {
        public void onSuccess(int responseCode, HeaderIterator headers, String paymentId) {
          pd.dismiss();
          close(Payment.DONE);
          if (mListener != null) {
            mListener.onSuccess(responseCode, headers, paymentId);
          }
        }

        @Override
        public void onCancel(int responseCode, HeaderIterator headers, String paymentId) {
          pd.dismiss();
          close(Payment.CANCELLED);
          if (mListener != null) {
            mListener.onCancel(responseCode, headers, paymentId);
          }
        }

        public void onFailure(int responseCode, HeaderIterator headers, String paymentId,
            String response) {
          pd.dismiss();
          close(Payment.ABORTED);
          if (mListener != null) {
            mListener.onFailure(responseCode, headers, paymentId, response);
          }
        }
      });
    }

    @Override
    protected void onDialogClose(String url) { }
  }
}
