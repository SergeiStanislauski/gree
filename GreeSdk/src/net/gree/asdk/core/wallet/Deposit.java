/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.wallet;

import java.util.TreeMap;

import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.util.Scheme;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

/**
 * Deposit class
 * @author GREE, Inc.
 *
 */
public class Deposit {
  private static final String TAG = "Deposit";
  public static final int BILLING_REQUEST_CODE = 370;
  public static final String INTENT_TYPE_CHECK_UNCOMMITTED_ORDER = "GREEApp/CheckUncommittedOrder";
  public static final String INTENT_KEY_CHECK_UNCOMMITTED_ORDER_ONLY = "check_uncommitted_order_only";
  public static final String INTENT_KEY_UNCOMMITTED_ORDER_STATE = "uncommitted_order_state";

  public static final int UNCOMMITTED_ORDER_STATE_NOT_FOUND = 0;
  public static final int UNCOMMITTED_ORDER_STATE_COMPLETED = 1;
  public static final int UNCOMMITTED_ORDER_STATE_CANCELLED = 2;
  public static final int UNCOMMITTED_ORDER_STATE_FAILED = 3;

  static final int GREEAPP_IAB_SUPPORTED_MAJOR_VERSION = 2;
  static final int GREEAPP_IAB_SUPPORTED_MINOR_VERSION = 0;
  static final int GREEAPP_IAB_SUPPORTED_REVISION = 17;

  private static AlertDialog mAlertDialog;
  
  /**
   * launch deposit popup
   * @param context
   */
  public static void launchDepositPopup(Context context, String appId) {
    launch(context, appId, Scheme.getWalletDepositHost());
  }

  /**
   * launch deposit history popup
   * @param context
   */
  public static void launchDepositHistory(Context context, String appId) {
    launch(context, appId, Scheme.getWalletDepositHistoryHost());
  }

  private static void launch(final Context context, String appId, final String host) {
    String scheme = null;
    if (AsyncErrorDialog.shouldShowErrorDialog(context)) {
      new AsyncErrorDialog(context).show();
      return;
    }
    if (!TextUtils.isEmpty(appId)) {
      scheme = new StringBuilder(Scheme.getAppScheme()).append(appId).append("://").append(host).toString();
      try {
        startBillingActivity(context, scheme);
        return;
      } catch (ActivityNotFoundException e) {
        GLog.w(TAG, e.getMessage());
      }
    }
    PackageInfo pi = Util.getPackageInfo(context, Core.getInstance().getGreeAppPackageName());
    if (pi == null) {
      showNotFoundIABSupportedGreeSnsDialog(context);
      return;
    }
    if (!isSupportedIABVersionName(pi.versionName)) {
      showIABUnsupportedDialog(context);
      return;
    }
    scheme = new StringBuilder(Scheme.getAppScheme()).append(Core.getInstance().getGreeAppId()).append("://").append(host).toString();
    try {
      startBillingActivity(context, scheme);
    } catch (ActivityNotFoundException e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  /**
   * start Activity for In-app billing
   * @param context
   * @param scheme
   * @throws ActivityNotFoundException
   */
  public static void startBillingActivity(Context context, String scheme) throws ActivityNotFoundException {
    Intent intent = createBillingIntent(scheme);
    if (context instanceof Activity) {
      Activity activity = (Activity) context;
      activity.startActivityForResult(intent, BILLING_REQUEST_CODE);
    } else {
      context.startActivity(intent);
    }
  }

  private static Intent createBillingIntent(String scheme) {
    Bundle bundle = new Bundle();
    String userId = Injector.getInstance(IAuthorizer.class).getOAuthUserId();
    bundle.putString("user_id", userId);
    bundle.putString("app_id", Core.getAppId());
    Intent intent = new Intent(Intent.ACTION_DEFAULT, Uri.parse(scheme));
    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    intent.putExtras(bundle);
    return intent;
  }

  private interface UncommittedOrderStateListener {
    public void onReceived(int state);
  }

  /**
   * This class is used to receive result of re-sending uncommitted order on some application
   * Deposit class does not know.
   */
  private static class ResendingOrderReceiver extends BroadcastReceiver {
    private UncommittedOrderStateListener mListener;

    public ResendingOrderReceiver() {
      super();
    }

    public void setUncommittedOrderStateListener(UncommittedOrderStateListener listener) {
      mListener = listener;
    }

    public void onReceive(Context context, Intent intent) {
      if (Intent.ACTION_SEND.equals(intent.getAction()) &&
          INTENT_TYPE_CHECK_UNCOMMITTED_ORDER.equals(intent.getType())) {
        if (mListener != null) {
          mListener.onReceived(intent.getIntExtra(INTENT_KEY_UNCOMMITTED_ORDER_STATE,
              UNCOMMITTED_ORDER_STATE_FAILED));
        }
      }
    }
  }

  /**
   * Interface which receives the results of re-sending uncommitted order.
   */
  public interface ResendingOrderListener {
    public void onNotFound();
    public void onCompleted();
    public void onCancelled();
    public void onFailed();
  }

  /**
   * Show the dialog to re-send uncommitted order if it exists on the application specified.
   * The listener is called back when the dialog is closed.
   * If all the orders have already been committed, listener.onCompeted would be called without dialog.
   * @param context context to start Activity to re-send uncommitted order
   * @param appId application ID of the application re-send uncommitted order
   * @param listener result of re-sending
   */
  public static void showResendingOrderDialog(final Context context, String appId, final ResendingOrderListener listener) {
    if (Url.isSandbox()) {
      if (listener != null) {
        listener.onNotFound();
      }
      return;
    }

    String greeAppId = Core.getInstance().getGreeAppId();
    if (appId == null) {
      appId = greeAppId;
    }
    if (appId.equals(greeAppId)) {
      PackageInfo pi = Util.getPackageInfo(context, Core.getInstance().getGreeAppPackageName());
      if (pi == null) {
        // If SNS application is not installed on the device, it is not necessary to check uncommitted orders.
        if (listener != null) {
          listener.onNotFound();
        }
        return;
      }
      if (!isSupportedIABVersionName(pi.versionName)) {
        showIABUnsupportedDialog(context);
        if (listener != null) {
          listener.onFailed();
        }
        return;
      }
    }

    final ResendingOrderReceiver receiver = new ResendingOrderReceiver();
    receiver.setUncommittedOrderStateListener(new UncommittedOrderStateListener() {
      public void onReceived(int state) {
        switch (state) {
          case UNCOMMITTED_ORDER_STATE_NOT_FOUND:
            listener.onNotFound();
            break;
          case UNCOMMITTED_ORDER_STATE_COMPLETED:
            listener.onCompleted();
            break;
          case UNCOMMITTED_ORDER_STATE_CANCELLED:
            listener.onCancelled();
            break;
          case UNCOMMITTED_ORDER_STATE_FAILED:
          default:
            listener.onFailed();
            break;
        }
        context.unregisterReceiver(receiver);
      }
    });
    try {
      IntentFilter filter = new IntentFilter();
      filter.addAction(Intent.ACTION_SEND);
      filter.addDataType(INTENT_TYPE_CHECK_UNCOMMITTED_ORDER);
      context.registerReceiver(receiver, filter);

      String scheme =
          new StringBuilder(Scheme.getAppScheme()).append(appId).append("://")
              .append(Scheme.getWalletDepositHost()).toString();
      Intent intent = createBillingIntent(scheme);
      intent.putExtra(INTENT_KEY_CHECK_UNCOMMITTED_ORDER_ONLY, true);
      context.startActivity(intent);
      return;
    } catch (MalformedMimeTypeException e) {
      GLog.printStackTrace(TAG, e);
    } catch (ActivityNotFoundException e) {
      GLog.printStackTrace(TAG, e);
      context.unregisterReceiver(receiver);
    }
    if (listener != null) {
      listener.onFailed();
    }
  }

  static boolean isSupportedIABVersionName(String versionName) {
    if (versionName == null) { return false; }

    String[] greeAppVer = versionName.split("\\.");
    if (greeAppVer.length == 3) {
      try {
        int majorVer = Integer.parseInt(greeAppVer[0]);
        int minorVer = Integer.parseInt(greeAppVer[1]);
        int revision = Integer.parseInt(greeAppVer[2]);
        if (GREEAPP_IAB_SUPPORTED_MAJOR_VERSION < majorVer
            || GREEAPP_IAB_SUPPORTED_MAJOR_VERSION == majorVer
            && (GREEAPP_IAB_SUPPORTED_MINOR_VERSION < minorVer || GREEAPP_IAB_SUPPORTED_MINOR_VERSION == minorVer
                && GREEAPP_IAB_SUPPORTED_REVISION <= revision)) {
          return true;
        }
      } catch (NumberFormatException e) {
        // do nothing
      }
    }
    return false;
  }

  private static void showNotFoundIABSupportedGreeSnsDialog(final Context context) {
    showIABUnsupportedErrorDialog(context, RR.string("gree_button_install"));
  }

  private static void showIABUnsupportedDialog(final Context context) {
    showIABUnsupportedErrorDialog(context, RR.string("gree_button_update"));
  }

  private static void showIABUnsupportedErrorDialog(final Context context, int buttonResId) {
    final TreeMap<String,String> params = new TreeMap<String,String>();
    params.put("target", Core.getInstance().getGreeAppPackageName());
    Logger.recordLog("evt", "show_googleplay_dialog", "purchase_coin", params);
    mAlertDialog =
        new AlertDialog.Builder(context)
            .setIcon(android.R.drawable.ic_dialog_info)
            .setTitle(context.getString(RR.string("gree_confirm_androidmarket_open_for_iab_title")))
            .setMessage(context.getString(RR.string("gree_confirm_androidmarket_snsapp_open_for_iab_message")))
            .setPositiveButton(context.getString(buttonResId),
                new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int which) {
                    Logger.recordLog("evt", "launch_googleplay", "purchase_coin", params);
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + Core.getInstance().getGreeAppPackageName())));
                  }
                }).create();
    mAlertDialog.show();
  }

  static void dialogClose() {
    if (mAlertDialog != null) {
      GLog.e("Deposit", "dialog close");
      mAlertDialog.dismiss();
      mAlertDialog = null;
    }
  }


}
