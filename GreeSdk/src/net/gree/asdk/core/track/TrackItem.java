/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.track;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.codec.GreeHmac;

import java.security.NoSuchAlgorithmException;

/**
 * Class representing the values being tracked.
 */
class TrackItem {

  private final static String TAG = "TrackItem";

  private TrackItemStorage storage;

  /**
   * Create a new instance of TrackItem
   *
   * @see Tracker#track(String, String, String, String, net.gree.asdk.core.track.Tracker.Uploader)
   */
  public TrackItem(String userId, String type, String key, String value, String mixer, String uploaderClzName) {
    this.userId = userId;
    this.type = type;
    this.key = key;
    this.data = value;
    this.mixer = mixer;
    this.uploaderClzName = uploaderClzName;
  }

  public int id;
  public String userId;
  public String type;
  public String key;
  public String data;
  public String seal;
  public String mixer;
  public String uploaderClzName;

  /**
   * To ensure data integrity, generate seal before save and check the seal after load. If the seal
   * doesn't match, the item is dirty.
   */
  public void generateSeal() {
    this.seal = doGenerateSeal();
  }

  /**
   * @see #generateSeal()
   */
  public boolean checkSeal() {
    return doGenerateSeal().equals(this.seal);
  }

  private String doGenerateSeal() {
    String seal;
    try {
      seal = GreeHmac.sha1(type + key + data + mixer);
    } catch (NoSuchAlgorithmException nsa) {
      seal = "";
      GLog.w(TAG, "Sign TrackItem error: " + nsa.getMessage());
    }
    return seal;
  }

  /**
   * Save this item to a data storage.
   */
  public void save() {
    storage.save(this);
  }

  /**
   * Delete this item from data storage.
   */
  public void delete() {
    storage.delete(this);
  }

  /**
   * Set storage that save or delete this item.
   * 
   * @param storage Instance of @{@link TrackItemStorage}
   */
  public void setStorage(TrackItemStorage storage) {
    this.storage = storage;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("TrackItem");
    sb.append("{id=").append(id).append('\'');
    sb.append(", userId='").append(userId).append('\'');
    sb.append(", type='").append(type).append('\'');
    sb.append(", key='").append(key).append('\'');
    sb.append(", data='").append(data).append('\'');
    sb.append(", mixer='").append(mixer).append('\'');
    sb.append(", seal='").append(seal).append('\'');
    sb.append(", uploaderClzName='").append(uploaderClzName).append('\'');
    sb.append('}');
    return sb.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TrackItem trackItem = (TrackItem) o;

    if (id != trackItem.id) {
      return false;
    }
    if (userId != null ? !userId.equals(trackItem.userId) : trackItem.userId != null) {
      return false;
    }
    if (data != null ? !data.equals(trackItem.data) : trackItem.data != null) {
      return false;
    }
    if (key != null ? !key.equals(trackItem.key) : trackItem.key != null) {
      return false;
    }
    if (mixer != null ? !mixer.equals(trackItem.mixer) : trackItem.mixer != null) {
      return false;
    }
    if (seal != null ? !seal.equals(trackItem.seal) : trackItem.seal != null) {
      return false;
    }
    if (type != null ? !type.equals(trackItem.type) : trackItem.type != null) {
      return false;
    }
    if (uploaderClzName != null
        ? !uploaderClzName.equals(trackItem.uploaderClzName)
        : trackItem.uploaderClzName != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (userId != null ? userId.hashCode() : 0);
    result = 31 * result + (type != null ? type.hashCode() : 0);
    result = 31 * result + (key != null ? key.hashCode() : 0);
    result = 31 * result + (data != null ? data.hashCode() : 0);
    result = 31 * result + (seal != null ? seal.hashCode() : 0);
    result = 31 * result + (mixer != null ? mixer.hashCode() : 0);
    result = 31 * result + (uploaderClzName != null ? uploaderClzName.hashCode() : 0);
    return result;
  }
}
