/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.gree.asdk.core.request.helper;

import java.util.Map;

import org.apache.http.HeaderIterator;

import net.gree.asdk.api.Request;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.CoreData;

/**
 * Call request for os.gree.net API
 */
public class OsRequest {
  private final static String TAG = OsRequest.class.getName();
  
  public void requestPost(Map<String, Object> query, String endpoint, OnResponseCallback<String> listener) {
    if (Injector.getInstance(IAuthorizer.class).hasOAuthAccessToken()) {
      Request request = new Request(CoreData.getParams());
      final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
      // sometimes null exception occurs during unit test
      if (manager == null) {
        GLog.e(TAG, "IPerformanceManager is null.");
        return;
      }

      final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_BADGE_GET);
      manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);

      request.oauthGree(endpoint, "post", query, null, false, listener);
    }
    else {
      GLog.d(TAG, "Not login. send notification update query is skipped.");
    }
  }
  
  public void requestGet(Map<String, Object> query, String endpoint, final OnResponseCallback<String> listener) {
    if (Injector.getInstance(IAuthorizer.class).hasOAuthAccessToken()) {
      Request request = new Request(CoreData.getParams());
      final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
      // sometimes null exception occurs during unit test
      if (manager == null) {
        GLog.e(TAG, "IPerformanceManager is null.");
        return;
      }

      final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_BADGE_GET);
      manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);

      request.oauthGree(endpoint, "get", query, null, false, new OnResponseCallback<String>(){
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, String response) {
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          listener.onSuccess(responseCode, headers, response);
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
          manager.flushData(performData);
          listener.onFailure(responseCode, headers, response);
        }
        
      } );
    }
    else {
      GLog.d(TAG, "Not login. send notification update query is skipped.");
    }
    
  }
}
