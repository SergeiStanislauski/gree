package net.gree.asdk.core.socialgraph;

/**
 * This class is used to describe users relationship
 */
public class InGameFriend {
  /*
   * Warning! the field names must match the server response
   * They are filled automatically by Gson reflection
   */
  private String user_id;
  private String relation_id;
  private String graph_id;

  /**
   * Get the GreeUser id
   * @return a user identifier
   */
  public String getUserId() {
    return user_id;
  }

  /**
   * the graph relation between this user and @me
   * @return a relation identifier
   */
  public String getRelationId() {
    return relation_id;
  }

  /**
   * The relation graph identifier
   * @return a graph identifier
   */
  public String getGraphId() {
    return graph_id;
  }
}
