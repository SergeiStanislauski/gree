/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.appinfo;

import net.gree.asdk.core.Core;

/**
 * The class for ApplicationInfo for storing application information.
 */
public class ApplicationInfo {
  private String app_id;
  private String name;
  private String shorten_name;
  private Thumbnail app_thumbnail_url;
  private String last_access;

  private class Thumbnail {
    private String small;
    private String middle;
    private String large;

    public String getSmallUrl()         { return small; }
    public String getMiddleUrl()        { return middle; }
    public String getLargeUrl()         { return large; }
  }

  /**
   * Constructor.
   */
  public ApplicationInfo() {}

  /**
   * The application Id, as defined on the developer center
   * @return unique string identifier
   */
  public String getAppId()                      { return app_id; }
  
  /**
   * Set the application ID: internally used only
   * @param id
   */
  public void setAppId(String id)               { app_id = id; }
  
  /**
   * The application full name as set by the game developer on the Gree server
   * @return
   */
  public String getAppName()                    { return name; }
  
  /**
   * The application shorted name as set by the game developer on the Gree server
   * @return
   */
  public String getAppShortenName()             { return shorten_name; }
  
  /**
   * The time the application was last accessed by the user in UTC
   * e.g : "2012-02-8 14:51:38"
   * @return the last time the application was opened by the user
   */
  public String getLastAccess()                 { return last_access; }

  /** the small thumbnail size is 48x48 */
  public static final int THUMBNAIL_SIZE_SMALL  = 0;
  /** the middle thumbnail size is 96x96 */
  public static final int THUMBNAIL_SIZE_MIDDLE = 1;
  /** the large thumnbail size is 144x144 */
  public static final int THUMBNAIL_SIZE_LARGE  = 2;

  /**
   * ThumbnailUrl getter.
   * @param size select from THUMBNAIL_SIZE_* const string.
   * @return target thumbnail url.
   */
  public String getThumbnailUrl(final int size) {
    if (app_thumbnail_url == null) {
      return null;
    }
    switch (size) {
      case THUMBNAIL_SIZE_SMALL:
        return app_thumbnail_url.getSmallUrl();
      case THUMBNAIL_SIZE_MIDDLE:
        return app_thumbnail_url.getMiddleUrl();
      case THUMBNAIL_SIZE_LARGE:
        return app_thumbnail_url.getLargeUrl();
      default:
        return null;
    }
  }

  /**
   * Check application id whether self app id is sns app or not.
   * @return if this application id is as common as sns app id, return true, other wise false.
   */
  public boolean isSnsApp() {
    return (getAppId() != null) && getAppId().equals(Core.getInstance().getGreeAppId());
  }
}
