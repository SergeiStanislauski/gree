/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.util;

import java.io.File;
import java.util.Random;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.storage.SyncedStore;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.TextUtils;

import net.gree.asdk.core.auth.AuthorizeContext;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.storage.LocalStorage;

public class DeviceInfo {
  private static final String TAG = DeviceInfo.class.getSimpleName();
  private static final String SYNCEDSTORE_KEY_UDID = "udid";
  private static final String UDID_PREFIX_ANDROID_ID = "android-id-";
  private static final String UDID_PREFIX_EMULATOR = "android-emu-";
  private static final String MAC_ADDRESS_EMULATOR = "ff:ff:ff:ff:ff:ff";
  private static final String KEY_UUID = "uuid";

  private static String sUdid;
  private static String sMacAddress;

  public static String getUdid() {
    String udid = CoreData.get(InternalSettings.Udid);
    if (!TextUtils.isEmpty(udid)) { return UDID_PREFIX_ANDROID_ID + udid; }

    if (sUdid == null) {
      sUdid = findUdid();
    }
    return sUdid;
  }

  /**
   * Use SyncedStore to maintain back compatibility of previous OF apps.
   * @return UDID found in local storage or generate one if not found.
   */
  private static String findUdid() {
    String udid = null;
    SyncedStore.Reader r = Core.getInstance().getPrefs().read();
    try {
      udid = r.getString(SYNCEDSTORE_KEY_UDID, null);
    } finally {
      r.complete();
    }

    if (udid == null) {
      udid = generateUdid();

      SyncedStore.Editor e = Core.getInstance().getPrefs().edit();
      try {
        e.putString(SYNCEDSTORE_KEY_UDID, udid);
      } finally {
        e.commit();
      }
    }

    return udid;
  }

  private static boolean isDevelop() {
    return CoreData.get(InternalSettings.DevelopmentMode).equals(Url.MODE_DEVELOP);
  }

  /**
   * Generates UDID with Android ID.
   * <pre>
   * There are two types of UDID:
   *  A: android-id-[0-9a-f]{1,16}
   *  E: android-emu-[0-9a-f]{1,32}
   *
   * Matrix of resulting UDID
   * +---------+------------+------------+------------+
   * |         | UDID generate disabled  | UDID       |
   * |         +------------+------------+ generate   |
   * |         | Valid      | Invalid    | enabled &  |
   * |         | Android ID | Android ID | on develop |
   * +---------+------------+------------+------------+
   * |device   |     A      |     E      |     E      |
   * +---------+------------+------------+------------+
   * |emulator |     E      |     E      |     E      |
   * +---------+------------+------------+------------+
   * </pre>
   * Only UDIDs with valid Android ID are allowed to have "android-id-" prefix.
   *
   * @return generated UDID
   */
  public static String generateUdid() {
    String androidId =
        android.provider.Settings.Secure.getString(Core.getInstance().getContext()
            .getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

    String value = CoreData.get(InternalSettings.EnableRandomUuidGeneration, "false");
    boolean requireRandomAndroidId = isDevelop() && Boolean.parseBoolean(value);
    boolean isEmu = isEmulator();

    if (!requireRandomAndroidId && isValidAndroidId(androidId)) {
      if (isEmu) {
        return UDID_PREFIX_EMULATOR + androidId;
      } else {
        return UDID_PREFIX_ANDROID_ID + androidId;
      }
    } else {
      return UDID_PREFIX_EMULATOR + generateRandomAndroidId(16);
    }
  }

  /**
   * Empty or Android ID that equals to 9774d56d682e549c is considered invalid.
   * 9774d56d682e549c is not unique Android ID according to the thread below.
   * http://code.google.com/p/android/issues/detail?id=10603
   * @param androidId
   * @return true if the give Android ID is valid, false otherwise.
   */
  private static boolean isValidAndroidId(String androidId) {
    return !(TextUtils.isEmpty(androidId) || androidId.equals("9774d56d682e549c"));
  }

  /** Return true if the app is running on an emulator.
   * This checking condition is found by comparing device info by Android Device Info Share.
   * https://play.google.com/store/apps/details?id=com.itog_lab.android.adis
   */
  private static boolean isEmulator() {
    return (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"));
  }

  /**
   * @param halfLen Half of the desired max length of the out put string.
   * @return String representing generated Android ID in hex.
   */
  private static String generateRandomAndroidId(int halfLen) {
    byte randomBytes[] = new byte[halfLen];
    new Random().nextBytes(randomBytes);

    return new String(Hex.encodeHex(randomBytes)).replace("\r\n", "");
  }

  public static String getMacAddress() {
    String mac = CoreData.get(InternalSettings.MacAddress);
    if (!TextUtils.isEmpty(mac)) { return mac; }

    if (sMacAddress == null) {
      sMacAddress = findMacAddress();
    }
    return sMacAddress;
  }

  private static String findMacAddress() {
    WifiManager wifiManager =
        (WifiManager) Core.getInstance().getContext().getSystemService(Context.WIFI_SERVICE);
    WifiInfo wifiInfo = wifiManager.getConnectionInfo();
    String macAddress = wifiInfo.getMacAddress();
    if (macAddress == null) macAddress = MAC_ADDRESS_EMULATOR;
    return macAddress.replaceAll("[^a-zA-Z0-9]", "");
  }

  public static boolean isRooted() {
    if (isFakeRootedDevice()) {
      return true;
    }

    if (new File("/system/bin/su").exists() ||
        new File("/system/app/Superuser.apk").exists() ||
        Util.getPackageInfo(Core.getInstance().getContext(), "com.noshufou.android.su") != null) {
      return true;
    }
    return false;
  }

  public static String getUuid() {
    return Injector.getInstance(LocalStorage.class).getString(KEY_UUID);
  }

  public static void updateUuid(final OnResponseCallback<String> listener) {
    String url = AuthorizeContext.appendQueryParameter(Url.getSecureApiEndpoint()+"/generateuuid/");
    new JsonClient().oauth2(url, BaseClient.METHOD_GET, null, false, new OnResponseCallback<String>() {

      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        if (response != null) {
          try {
            JSONObject json = new JSONObject(response);
            Injector.getInstance(LocalStorage.class).putString(KEY_UUID, json.getString("entry"));
            if (listener != null) listener.onSuccess(responseCode, headers, response);
            return;
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
        if (listener != null) listener.onFailure(0, null, "generateuuid response format error");
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) listener.onFailure(responseCode, headers, response);
      }
    });
  }

  public static boolean isSendableAndroidId() {
    return !"true".equals(CoreData.get(InternalSettings.DisableSendingAndroidId));
  }

  public static boolean isSendableMacAddress() {
    return !"true".equals(CoreData.get(InternalSettings.DisableSendingMacAddress));
  }

  public static boolean isFakeRootedDevice() {
    return "true".equals(CoreData.get(InternalSettings.EnableFakeRootedDevice));
  }
}
