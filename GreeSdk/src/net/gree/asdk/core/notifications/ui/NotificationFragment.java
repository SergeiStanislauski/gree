/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package net.gree.asdk.core.notifications.ui;

import java.util.ArrayList;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.dashboard.DashboardActivity;
import net.gree.asdk.core.dashboard.DashboardNavigationBar;
import net.gree.asdk.core.notifications.NotificationCallback;

import org.json.JSONObject;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * Notification popup window class
 */
public class NotificationFragment extends Fragment implements NotificationCallback {
  public static final int NOTIFICATION_APPS = 1;
  public static final int NOTIFICATION_SNS = 2;
  public static final int NOTIFICATION_FRIENDS = 3;  

  private static final int ICON_WIDTH = 44;
  private static final String KEY_NOTIFICATION_TYPE = "notification_type";

  private int mLongSide;
  private int mShortSide;
  private int mMargin;
  private int mStatusbarHeight;
  private int mNotificationType;
  private int mNavbarHeight = 44;
  private NotificationAdapterBase mAdapter;

  /**
   * @return dialog fragment
   * Create a new instance of NotificationDialogFragment
   */
  public static NotificationFragment newInstance(int type) {
    NotificationFragment f = new NotificationFragment();

    Bundle args = new Bundle();
    args.putInt(KEY_NOTIFICATION_TYPE, type);
    f.setArguments(args);

    return f;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Resources r = getResources();
    mLongSide = r.getInteger(RR.integer("gree_notification_window_long_side"));
    mShortSide = r.getInteger(RR.integer("gree_notification_window_short_side"));
    mMargin = r.getInteger(RR.integer("gree_notification_window_margin"));
    mStatusbarHeight = r.getInteger(RR.integer("gree_heuristic_statusbar_height"));

    Bundle args = getArguments();
    mNotificationType = args.getInt(KEY_NOTIFICATION_TYPE);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(RR.layout("gree_notification_popup"), container, false);
    
    v.findViewById(RR.id("gree_notification_transparent_area")).setOnClickListener(new OnClickListener(){
      @Override
      public void onClick(View v) {
        dismiss();
      }
    });
    
    return v;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    
    Configuration config = getResources().getConfiguration();
    mNavbarHeight = DashboardNavigationBar.getNavigationBarHeight(getActivity(), config);

    rebuildView();
  }
  
  /**
   * Resize popup window and build inside views
   */
  protected void rebuildView() {
    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) (getView().getLayoutParams());
    Display display = getActivity().getWindowManager().getDefaultDisplay();
    DisplayMetrics metrics = new DisplayMetrics();
    display.getMetrics(metrics);

    float displayWidthDp = metrics.widthPixels / metrics.density;
    float displayHeightDp = metrics.heightPixels / metrics.density;
    float displayShortDp = displayWidthDp < displayHeightDp ? displayWidthDp : displayHeightDp;
    
    lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
    lp.rightMargin = mMargin * 2;

    if (displayShortDp < mLongSide) {
      lp.width = (int) (metrics.widthPixels - (mMargin * 2) * metrics.density);
      lp.height = (int) (metrics.heightPixels - mNavbarHeight - (mMargin * 2 + mStatusbarHeight) * metrics.density);
    } else {
      if (displayWidthDp > displayHeightDp) {
        lp.width = (int) ((mLongSide) * metrics.density);
        lp.height = (int) ((mShortSide - mStatusbarHeight) * metrics.density - mNavbarHeight);
      } else {
        lp.width = (int) ((mShortSide) * metrics.density);
        lp.height = (int) ((mLongSide - mStatusbarHeight) * metrics.density  - mNavbarHeight);
      }
    }

    lp.topMargin = (int) (mNavbarHeight - 10 * (metrics.density));

    TextView title = (TextView)getView().findViewById(RR.id("gree_notification_dialog_title"));

    View arrow = getView().findViewById(RR.id("gree_notification_arrow"));
    LinearLayout.LayoutParams arrowLp = (LinearLayout.LayoutParams) arrow.getLayoutParams();

    int closeBtnWidth = Core.isGreeApp() ? 0 : (int) (ICON_WIDTH * metrics.density);
    arrowLp.rightMargin = (int) (closeBtnWidth + 5 * metrics.density);
    
    switch (mNotificationType) {
      case NOTIFICATION_APPS:
        arrowLp.rightMargin += (int) ((ICON_WIDTH + 3) * 2 * metrics.density);
        title.setText(RR.string("gree_notification_title_app"));
        break;
      case NOTIFICATION_SNS:
        arrowLp.rightMargin += (int) ((ICON_WIDTH + 3) * metrics.density);
        title.setText(RR.string("gree_notification_title_sns"));
        break;
      case NOTIFICATION_FRIENDS:
        title.setText(RR.string("gree_notification_title_friends"));
        break;
      default:
        throw new RuntimeException();
    }

    arrow.setLayoutParams(arrowLp);
    getView().setLayoutParams(lp);

    initListView();    
  }
  
  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    mNavbarHeight = DashboardNavigationBar.getNavigationBarHeight(getActivity(), newConfig);
    rebuildView();
  }

  private void initListView() {
    Display display = getActivity().getWindowManager().getDefaultDisplay();
    DisplayMetrics metrics = new DisplayMetrics();
    display.getMetrics(metrics);  

    ListView listView = (ListView) getView().findViewById(RR.id("gree_notification_list"));

    NotificationCallback callback = null;
    Activity activity = getActivity();
    String currentUrl = null;
    if(activity instanceof DashboardActivity) {
      DashboardActivity dashboard = (DashboardActivity) activity;
      callback = dashboard.getDashboardContentFragment();
      currentUrl = dashboard.getCurrentContentViewUrl();
    }
    ArrayList<NotificationData> list = new ArrayList<NotificationData>();
    
    NotificationAdapterBase adapter = null;

    switch(mNotificationType) {
      case NOTIFICATION_APPS:
        adapter = new NotificationAppsAdapter(getActivity(), metrics, list);
        break;
      case NOTIFICATION_SNS:
        adapter = new NotificationSNSAdapter(getActivity(), metrics, list);
        break;
      case NOTIFICATION_FRIENDS:
        adapter = new NotificationFriendsAdapter(getActivity(), metrics, list);
        break;
    }
    adapter.loadNotifications();
    adapter.addCallback(callback);
    adapter.addCallback(this);
    listView.setAdapter(adapter);
    listView.setOnItemClickListener(adapter);
    adapter.setCurrentPageUrl(currentUrl);
    
    mAdapter = adapter;
  }
  
  /**
   * let DashboardActivity to dismiss this fragment
   */
  protected void dismiss() {
    Activity activity = getActivity();
    if (activity instanceof DashboardActivity) {
      DashboardActivity dashboard = (DashboardActivity) activity;
      dashboard.dismissNotification();
    }
  }

  @Override
  public void onNotificationClick(JSONObject data) {
    dismiss();
  }
  
  /**
   * getter
   * @return NotifiationListView adapter
   */
  public NotificationAdapterBase getAdapter() {
    return mAdapter;
  }
  
  /**
   * getter
   * @return notification type
   */
  public int getNotificationType() {
    return mNotificationType;
  }
  
}

