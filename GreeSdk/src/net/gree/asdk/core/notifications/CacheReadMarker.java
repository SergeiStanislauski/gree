package net.gree.asdk.core.notifications;

import java.util.ArrayList;
import java.util.List;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.notifications.ui.NotificationAdapterBase;
import net.gree.asdk.core.storage.JSONStorage;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class CacheReadMarker extends AsyncTask<Void, Void, Void> {
  private final static String TAG = CacheReadMarker.class.getName();

  public CacheReadMarker(String filter) {
    mFilter = filter;
  }

  private String mFilter;

  @Override
  protected Void doInBackground(Void... params) {
    JSONStorage storage = Injector.getInstance(JSONStorage.class);
    SQLiteDatabase readDb = storage.getReadableDatabase();
    String[] selectionArgs = { mFilter };
    Cursor c = readDb.query(JSONStorage.TABLE_NAME, storage.getColums(), JSONStorage.COLUM_FILTER + " = ?",
      selectionArgs, null, null, null, String.valueOf(NotificationAdapterBase.LOAD_LIMIT));

    if (c.getCount() <= 0) {
      c.close();
      return null;
    }

    c.moveToFirst();
    List<ContentValues> list = new ArrayList<ContentValues>();
    while (!c.isAfterLast()) {
      String date = c.getString(JSONStorage.COLUM_INDEX_DATE);
      byte[] blob = c.getBlob(JSONStorage.COLUM_INDEX_BLOB);
      String json = c.getString(JSONStorage.COLUM_INDEX_JSON);
      String filterSub = c.getString(JSONStorage.COLUM_INDEX_FILTER_SUB);
      String key = c.getString(JSONStorage.COLUM_INDEX_KEY);

      JSONObject jsonObj = null;
      try {
        jsonObj = new JSONObject(json);
        if (jsonObj.getInt("unread") != 0) {
          jsonObj.put("unread", 0);

          ContentValues cv = new ContentValues();
          cv.put(JSONStorage.COLUM_DATE, date);
          cv.put(JSONStorage.COLUM_BLOB, blob);
          cv.put(JSONStorage.COLUM_JSON, jsonObj.toString());
          cv.put(JSONStorage.COLUM_FILTER, mFilter);
          cv.put(JSONStorage.COLUM_FILTER_SUB, filterSub);
          cv.put(JSONStorage.COLUM_KEY, key);

          list.add(cv);
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }


      c.moveToNext();
    }
    c.close();

    if (list.size() > 0) {  storage.replace(list); }

    return null;
  }

}
