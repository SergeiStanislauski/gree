/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.ui;

import net.gree.asdk.api.auth.Authorizer.AuthorizeListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.dashboard.DashboardActivity;
import net.gree.asdk.core.ui.web.WebViewClientBase;
import net.gree.asdk.core.util.Scheme;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;
import net.gree.asdk.core.wallet.Deposit;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Command interface webview client
 *
 */
public class CommandInterfaceWebViewClient extends WebViewClientBase {

  private static final String TAG = "CommandInterfaceWebViewClient";
  private static final String LOGGING_PREFIX = "Gree[URL:CommandInterfaceWebView]: ";

  private Context context_ = null;

  /**
   * command interface webview client constructor
   */
  public CommandInterfaceWebViewClient(Context context) {
    super(context);
    context_ = context;
  }

  @Override
  public void onPageStarted(WebView webView, String url, Bitmap favicon) {
    GLog.d(TAG, LOGGING_PREFIX + " started:" + url);
    super.onPageStarted(webView, url, favicon);

    try {
      CommandInterfaceWebView commandInterfaceWebView = (CommandInterfaceWebView) webView;
      commandInterfaceWebView.restoreJavascriptInterface();

      if (!Url.isSnsUrl(url)) {
        commandInterfaceWebView.setSnsInterfaceAvailable(false);
      }

    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  /**
   * Give the host application a chance to take over the control when a new url is about to be loaded in the current CommandInterfaceWebView..
   * @return True if the host application wants to leave the current CommandInterfaceWebView and handle the url itself, otherwise return false. 
   */
  public boolean shouldOverrideUrlLoading(final WebView webView, final String url) {

    if (autoLogin(webView, url, new AuthorizeListener() {
      @Override
      public void onError() {
      }
      @Override
      public void onCancel() {
      }
      @Override
      public void onAuthorized() {
        if (context_ instanceof Activity) {
          DashboardActivity.show((Activity) context_, getBacktoUrl(webView, url), false);
        } else {
          GLog.w(TAG, "Unexpected condition: context_ is NOT an Activity");
        }
      }
    })) {
      return true;
    } else if (url != null && url.startsWith(Scheme.getAppScheme())) {
      Context context = webView.getContext();

      if (Scheme.isBillingScheme(url)) {
        try {
          Deposit.startBillingActivity(context, url);
        } catch (ActivityNotFoundException e) {
          GLog.printStackTrace(TAG, e);
        }
        return true;
      }

      GLog.d(TAG, "dashboard auto login");
    }
    
    if (Util.showRewardOfferWall(webView.getContext(), url)) {
      return true;
    }

    return super.shouldOverrideUrlLoading(webView, url);
  }

  @Override
  public void onPageFinished(WebView webView, String url) {
    GLog.d(TAG, LOGGING_PREFIX + " finished:" + url);
    super.onPageFinished(webView, url);

    webView.scrollTo(0, 1);
    webView.requestFocus(View.FOCUS_DOWN);
  }

  @Override
  public void onReceivedError(WebView webView, int errorCode, String description, String failingUrl) {
    // If need, and errorCode is "unknown error", Set particular error message before call super.onReceivedError().
    if (errorCode == WebViewClient.ERROR_UNKNOWN) {
      super.setErrorMessage(description);
    }
    super.onReceivedError(webView, errorCode, description, failingUrl);
    try {
      CommandInterfaceWebView commandInterfaceWebView = (CommandInterfaceWebView) webView;
      commandInterfaceWebView.setSnsInterfaceAvailable(false);
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }
}
