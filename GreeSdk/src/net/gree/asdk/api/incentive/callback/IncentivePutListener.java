package net.gree.asdk.api.incentive.callback;

import org.apache.http.HeaderIterator;

import net.gree.asdk.api.incentive.model.IncentiveEventArray;

/**
 * The callback interface that goes with IncentiveController.put
 */
public interface IncentivePutListener {
	/**
	 * On a successful call to IncentiveController.put this method is called. 
	 * It will be passed a collection of IncentiveEvent represented by the 
	 * IncentiveEventArray object.
	 * 
	 * @param result the IncentiveEventArray objects returned by this request.
	 */
  void onSuccess(IncentiveEventArray result);

  /**
   * On an unsuccessful call to IncentiveController.put, this method is called. 
   * 
   * @param responseCode the response code received from the server
   * @param headers a type-safe iterator for Header objects.
   * @param response a String representation of the response from the server.
   */
  void onFailure(int responseCode, HeaderIterator headers, String response);
}
