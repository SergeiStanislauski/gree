/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample;

import net.gree.asdk.api.FriendCode.CodeListener;
import net.gree.asdk.api.FriendCode.EntryListGetListener;
import net.gree.asdk.api.FriendCode.OwnerGetListener;
import net.gree.asdk.api.FriendCode.SuccessListener;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.api.FriendCode;
import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.core.GLog;

import org.apache.http.HeaderIterator;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/*
 * This Activity have test case of FriendCode class.
 */
public class FriendCodeActivity extends BaseActivity {
  private static final String TAG = "FriendCodeActivity";
  private EditText mExpireTimeEdit;
  private EditText mEntryEdit;
  private EditText mStartIdxEdit;
  private EditText mCountEdit;
  private TextView mCodeShowView;
  private TextView mMyOwnerView;
  private TextView mFriendEntryListView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.friendcode);

    getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    mCodeShowView = (TextView) findViewById(R.id.codeShowText);
    mMyOwnerView = (TextView) findViewById(R.id.myOwnerText);
    mFriendEntryListView = (TextView) findViewById(R.id.friendEntryText);

    mExpireTimeEdit= (EditText) findViewById(R.id.expireTimeEdit);
    mEntryEdit = (EditText) findViewById(R.id.entryEdit);
    mStartIdxEdit = (EditText) findViewById(R.id.startIdxEdit);
    mCountEdit = (EditText) findViewById(R.id.countEdit);

    // Create Friend Code button.
    Button createButton = (Button) findViewById(R.id.createButton);
    createButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        if (AsyncErrorDialog.shouldShowErrorDialog(v.getContext())) {
          AsyncErrorDialog dialog = new AsyncErrorDialog(v.getContext());
          dialog.show();
          return;
        }
        String expireTime = mExpireTimeEdit.getText().toString();
        if (expireTime == null || expireTime.length() <= 0) {
          expireTime = null;
        }
        boolean parseResult = FriendCode.requestCode(expireTime, new CodeListener() {
          public void onSuccess(FriendCode.Code code) {
            Toast.makeText(getApplicationContext(), "FriendCode.requestCode success.[code:" + code.getCode() + " expire:" + code.getExpireTime() + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.requestCode success.[code:" + code.getCode() + " expire:" + code.getExpireTime() + "]");
            mCodeShowView.setText("[code:" + code.getCode() + " expire:" + code.getExpireTime() + "]");
          }
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            Toast.makeText(getApplicationContext(), "FriendCode.requestCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.requestCode failed.[" + responseCode + " " + response + "]");
          }
        });
        if (parseResult == false){
          Toast.makeText(getApplicationContext(), "Requested expiration time is invalid. Format as YYYY-MM-DDThh:mm:ss+hhmm.", Toast.LENGTH_LONG).show();
        }
      }
    });

    // Inquery my Friend Code which make self previously button.
    Button inqueryButton = (Button) findViewById(R.id.inquiryCodeButton);
    inqueryButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        FriendCode.loadCode(new CodeListener() {
          public void onSuccess(FriendCode.Code code) {
            Toast.makeText(getApplicationContext(), "FriendCode.loadCode success.[code:" + code.getCode() + " expire:" + code.getExpireTime() + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.loadCode success.[code:" + code + " expire:" + code.getExpireTime() + "]");
            mCodeShowView.setText("[code:" + code.getCode() + " expire:" + code.getExpireTime() + "]");
          }
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            Toast.makeText(getApplicationContext(), "FriendCode.loadCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.loadCode failed.[" + responseCode + " " + response + "]");
            mCodeShowView.setText("");
          }
        });
      }
    });

    // Entry other person's Friend Code button.
    Button entryButton = (Button) findViewById(R.id.entryButton);
    entryButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        String entry = mEntryEdit.getText().toString();
        FriendCode.verifyCode(entry, new SuccessListener() {
          public void onSuccess() {
            Toast.makeText(getApplicationContext(), "FriendCode.verifyCode success.", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.verifyCode success.");
          }
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            Toast.makeText(getApplicationContext(), "FriendCode.verifyCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.verifyCode failed.[" + responseCode + " " + response + "]");
          }
        });
      }
    });

    // Delete myself Friend Code button.
    Button deleteButton = (Button) findViewById(R.id.deleteButton);
    deleteButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        FriendCode.deleteCode(new SuccessListener() {
          public void onSuccess() {
            Toast.makeText(getApplicationContext(), "FriendCode.deleteCode success.", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.deleteCode success.");
            mCodeShowView.setText("");
          }
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            Toast.makeText(getApplicationContext(), "FriendCode.deleteCode failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.deleteCode failed.[" + responseCode + " " + response + "]");
          }
        });
      }
    });

    // Inquery owner Id which you entried Friend Code button.
    Button inqueryOwnerButton = (Button) findViewById(R.id.inquiryOwnerButton);
    inqueryOwnerButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        FriendCode.loadOwner(new OwnerGetListener() {
          public void onSuccess(FriendCode.Data owner) {
            Toast.makeText(getApplicationContext(), "FriendCode.loadOwner success.[ownId:" + owner.getUserId() + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.loadOwner success.[ownId:" + owner.getUserId() + "]");
            mMyOwnerView.setText("My Owner is [" + owner.getUserId() + "]");
          }
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            Toast.makeText(getApplicationContext(), "FriendCode.loadOwner failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.loadOwner failed.[" + responseCode + " " + response + "]");
          }
        });
      }
    });

    // Inquery id list which entried your Friend Code button.
    Button inqueryEntryListButton = (Button) findViewById(R.id.inquiryEntryListButton);
    inqueryEntryListButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        String strStartIdx = mStartIdxEdit.getText().toString();
        String strCounts = mCountEdit.getText().toString();
        int startIdx = 0;
        int counts = 0;
        
        try {
          if (strStartIdx != null && strStartIdx.length() > 0) {
            startIdx = Integer.valueOf(strStartIdx);
          }

          if (strCounts != null && strCounts.length() > 0) {
            counts = Integer.valueOf(strCounts);
          }
        } catch (NumberFormatException e) {
          startIdx = 0;
          counts = 0;
        }

        FriendCode.loadFriends(startIdx, counts, new EntryListGetListener() {
          public void onSuccess(int startIndex, int itemsPerPage, int totalResults, FriendCode.Data[] entries) {
            Toast.makeText(getApplicationContext(), "FriendCode.loadFriends success.[results:" + totalResults + " sIdx:" + startIndex + " parPage" + itemsPerPage + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.loadFriends success.[results:" + totalResults + " sIdx:" + startIndex + " parPage" + itemsPerPage + "]");
            StringBuilder build = new StringBuilder("");
            int currentResults = totalResults - startIndex + 1;
            int loopCounts = currentResults;
            if (totalResults > itemsPerPage) {
              loopCounts = itemsPerPage;
            }
            for (int i = 0; i < loopCounts; i++) {
              build.append("index:").append(i + startIndex).append(" uid:").append(entries[i].getUserId()).append("\n");
            }
            mFriendEntryListView.setText(build.toString());
          }
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            Toast.makeText(getApplicationContext(), "FriendCode.loadFriends failed.[" + responseCode + " " + response + "]", Toast.LENGTH_LONG).show();
            GLog.i(TAG, "FriendCode.loadFriends failed.[" + responseCode + " " + response + "]");
            mFriendEntryListView.setText("");
          }
        });
      }
    });
  }

  @Override
  public void onOpenHelp(View v){
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.friendcode_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }

  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpBackButton();
  }

  @Override
  protected void sync(boolean fromStart) {
  }
}
