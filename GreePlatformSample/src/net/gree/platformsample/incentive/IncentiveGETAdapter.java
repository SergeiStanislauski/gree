package net.gree.platformsample.incentive;

import java.util.ArrayList;
import java.util.List;

import net.gree.asdk.api.incentive.IncentiveController;
import net.gree.asdk.api.incentive.callback.IncentivePutListener;
import net.gree.asdk.api.incentive.model.IncentiveEvent;
import net.gree.asdk.api.incentive.model.IncentiveEventArray;
import net.gree.platformsample.R;

import org.apache.http.HeaderIterator;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class IncentiveGETAdapter extends BaseAdapter {
	protected static final String TAG = "IncentiveGETAdapter";
	private LayoutInflater inflater;
	private  ArrayList<IncentiveEvent> data;
	Context mCtx;
	IncentiveGETAdapter self;
	
  /**
   * View holder
   */
  public static class IncentiveViewHolder {
    TextView name, name2, msg1, msg2;
    Button delivered;
    ViewSwitcher switcher;
    LinearLayout text, textAndButton;
  }
	  
	public IncentiveGETAdapter(Context context, ArrayList<IncentiveEvent> mData){
		this.data = mData;
		this.mCtx = context;
	    inflater = LayoutInflater.from(context);
	    self = this;
	}

	@Override
	public int getCount() {
	 if (data == null) {
	    return 0;
	 } else {
	    return data.size();
	 }
	}

	@Override
	public Object getItem(int position) {
      if (data == null) {
        return null;
      } else {
        return data.get(position);
      }
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		IncentiveViewHolder holder;
		
		// checking viewholder
	    if (convertView == null) {
	      convertView = inflater.inflate(R.layout.incentive_get_listview_row, null);
	      holder = new IncentiveViewHolder();
	      // displayed data from the JSON object
	      holder.name = (TextView) convertView.findViewById(R.id.incentive_get_row_id1);
	      holder.name2 = (TextView) convertView.findViewById(R.id.incentive_get_row_id2);
	      holder.msg1 = (TextView)convertView.findViewById(R.id.incentive_get_row_msg1);
	      holder.msg2 = (TextView)convertView.findViewById(R.id.incentive_get_row_msg2);
	      // other values used in UI
	      holder.delivered = (Button) convertView.findViewById(R.id.deliver_incentive_get);
	      holder.switcher = (ViewSwitcher)convertView.findViewById(R.id.incentive_row_viewswitcher);
	      holder.text = (LinearLayout)convertView.findViewById(R.id.incentive_get_row_inner_child1);
	      holder.textAndButton = (LinearLayout)convertView.findViewById(R.id.incentive_get_row_inner_child2);	      
	      convertView.setTag(holder);
	    } else {
	      holder = (IncentiveViewHolder) convertView.getTag();
	    }
	    // gets a final reference to the switcher
	    final ViewSwitcher fVS = holder.switcher;
	    // get the data
	    final IncentiveEvent item = data.get(position);
	    // if not null set the code
	    if (item != null) {
	      // set the name
	      if (item.getToUserId()!=null) {
	    	// from user id
			holder.name.setText(item.getFromUserId());
			holder.name2.setText(item.getFromUserId());
	        // payload message
	        holder.msg1.setText(item.getPayload());
	        holder.msg2.setText(item.getPayload());
	      }
	      
	      // sets the click listener for the delivered button
	      holder.delivered.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Log.d(TAG, "Clicked here!");
				List<String> eventArray = new ArrayList<String>();
				eventArray.add(item.getId());
				IncentiveController.put(eventArray, callBack);
				// updates
				data.remove(position);
				fVS.showPrevious();	
				self.notifyDataSetChanged();
			}    	  
	      });
	      
	      // show the button on a click
	      holder.text.setOnClickListener(new OnClickListener(){
	  		@Override
			public void onClick(View v) {
	  			fVS.showNext();					
			}
		});
	      // hide the button
	      holder.textAndButton.setOnClickListener(new OnClickListener(){
	  		@Override
			public void onClick(View v) {	
	  			fVS.showPrevious();		
			}
		});
	    }
	    // returns the view
	    return convertView;
	}
	
	private IncentivePutListener callBack = new IncentivePutListener() {
        @Override
        public void onSuccess(IncentiveEventArray result) {
          Log.e(TAG, "onSuccess");
          Log.e(TAG, result.toString());

          Toast.makeText(mCtx, "PUT Success!", Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          StringBuilder sb = new StringBuilder();
          sb.append("onFailure: ");
          sb.append(" responseCode:" + responseCode);
          sb.append(" response:" + response);
          Log.e(TAG, sb.toString());
          Toast.makeText(mCtx, sb.toString(), Toast.LENGTH_LONG).show();
        }

      };

}
