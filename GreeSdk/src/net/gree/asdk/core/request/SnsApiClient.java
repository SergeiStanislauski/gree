/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.entity.StringEntity;

import net.gree.asdk.core.request.helper.MethodHelper;


public class SnsApiClient extends JsonClient {

  /**
   * @param url
   * @param method
   * @param headers
   * @param entityString
   * @param sync
   * @param listener
   */
  public void oauth(String url, int method, Map<String, String> headers, String entityString,
      boolean sync, OnResponseCallback<String> listener) {
    StringEntity entity = null;
    if (headers == null) {
      headers = new HashMap<String, String>(1);
    }
    headers.put("Content-Type", "application/json");
    try {
      if (entityString != null) {
        entity = new StringEntity(entityString, "UTF-8");
      }
    } catch (UnsupportedEncodingException e1) {
      RequestLogger.getInstance().printStackTrace(entityString, e1);
    }

    StringConverter converter = new StringConverter();
    // use the overrided SnsReponseHandler here
    SnsResponseHandler handler = new SnsResponseHandler(listener);
    new GreeRequest(url, MethodHelper.parseInt(method)).entity(entity).header(headers).sync(sync)
        .request(converter, handler);
  }
}
