/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.inject;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.util.Preconditions;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Collects configuration information, binds implementation to class and creates the map of
 * implementation-class which will be used by the {@link InternalInjector}. Binder should be used by
 * {@link AbstractModule} to create the injection modules.
 * 
 * Currently, the Binder only supports constructor-based injection, for example:
 * 
 * <pre>
 * public class AImpl implements A {
 * 
 *   &#064;Inject
 *   public AImpl(B b) {
 *     // initalizing
 *   }
 * }
 * </pre>
 * 
 * Binder will automatically find or create an instance of B, then create the instance of AImpl
 * using the annotated constructor.
 *
 * It'll provide filed/method based injection in the future.
 * 
 * <h3>Binding syntax</h3>
 * 
 * <pre>
 * bind(ServiceImpl.class);
 * </pre>
 * 
 * This statement does essentially nothing; it "binds the {@code ServiceImpl} class to itself".
 * 
 * <pre>
 * bind(Service.class, ServiceImpl.class);
 * </pre>
 * 
 * Specifies that a request for a {@code Service} instance should be treated as if it were a request
 * for a {@code ServiceImpl} instance.
 * 
 * <pre>
 * bind(Service.class, new ServiceImpl());
 * </pre>
 * 
 * In this example, your module itself, <i>not the InteralInjector</i>, takes responsibility for
 * obtaining a {@code ServiceImpl} instance, then asks InternalInjector to always use this single
 * instance to fulfill all {@code Service} injection requests.
 */
public final class Binder {

  private static final String TAG = "Binder";

  private static final String CONSTRUCTOR_RULES =
      "Classes must have either one (and only one) constructor "
          + "annotated with @Inject or a zero-argument constructor that is not private.";

  private Map<Class<?>, Class<?>> classMap;
  private Map<Class<?>, Object> instances;
  private List<Class<?>> waitingList;

  private Binder() {
    classMap = new HashMap<Class<?>, Class<?>>();
    instances = new HashMap<Class<?>, Object>();
    waitingList = new ArrayList<Class<?>>();
  }

  /**
   * Create a new Binder instance which will be used by the implementation of {@link AbstractModule}
   * to bind a class to its implementation. {@link #finishBinding} must be inovked after binding.
   * 
   * @return
   */
  public static Binder startBinding() {
    return new Binder();
  }

  /**
   * Directly bind a class
   * 
   * @param clz The class to be binded
   */
  public void bind(Class<?> clz) {
    Preconditions.checkNotNull(clz, "cls is required");
    if (clz.isInterface()) {
      throw new IllegalArgumentException("Could not directly bind to interface");
    }
    classMap.put(clz, clz);
  }

  /**
   * Bind an implementation class to a super class. The super class might be a interface, abstract
   * class or concrete class.
   * 
   * @param clz The super class.
   * @param impl The subclass/implementation class of super class.
   */
  public <T> void bind(Class<T> clz, Class<? extends T> impl) {
    Preconditions.checkNotNull(clz, "clz is required");
    Preconditions.checkNotNull(impl, "impl is required");
    if (!clz.isAssignableFrom(impl)) {
      throw new IllegalArgumentException(clz.getCanonicalName() + " cannot assign from "
          + impl.getCanonicalName());
    }
    classMap.put(clz, impl);
  }

  /**
   * Bind an instance to a clz. The class might be a interface, abstract class or concrete class.
   * 
   * @param clz The class to be binded
   * @param instance An instance of the class
   */
  public <T> void bind(Class<T> clz, final T instance) {
    Preconditions.checkNotNull(clz, "clz is required");
    Preconditions.checkNotNull(instance, "instance is required");
    if (!(clz.isAssignableFrom(instance.getClass()))) {
      throw new IllegalArgumentException(instance + "is not a instance of "
          + clz.getCanonicalName());
    }
    instances.put(clz, instance);
  }

  /**
   * Finish binding implementation to class:
   * 
   * <pre>
   * <ul>
   *  <ol>Do nothing if there already exists an instance of the class
   *  <ol>Create a new instance if the class has a default constructor and without constructor annotated
   * with @Inject
   *  <ol>If there's a constructor annotated with @Inject and the parameters list is not empty, get or create 
   * the instance of every parameter type, then create the instance.
   * </ul>
   * </pre>
   */
  public void finishBinding() {
    for (Map.Entry<Class<?>, Class<?>> entry : classMap.entrySet()) {
      Class<?> clz = entry.getKey();
      Class<?> impl = entry.getValue();
      if (!instances.containsKey(clz)) {
        instances.put(clz, createInstance(impl));
      }
    }
  }

  /**
   * Get the instances map for the binder after finish binding.
   * 
   * @return a Class-Instane map
   */
  public Map<Class<?>, Object> getInstances() {
    return instances;
  }

  /**
   * Get the instance of given clz which should be a super class. Create a new instance if there
   * isn't one.
   * 
   * @param clz The super class
   * @return The instance of clz
   */
  private Object getOrCreateInstance(Class<?> clz) {
    Object instance = instances.get(clz);
    if (null != instance) {
      return instance;
    }

    Class<?> impl = classMap.get(clz);
    // No binding for clz
    if (null == impl) {
      throw new RuntimeException("Could not find implementation class for "
          + clz.getCanonicalName());
    }

    instance = createInstance(impl);
    instances.put(clz, instance);
    return instance;
  }

  /**
   * Create an instance for the given class which should be an implementation class.
   * 
   * @param impl The implementation class
   * @return The instance of impl
   */
  private Object createInstance(Class<?> impl) {
    GLog.d(TAG, "started creating instance of " + impl);
    // Find the suitble constructor.
    Constructor<?> constructor = getConstructorOf(impl);
    if (!constructor.isAnnotationPresent(Inject.class)) {
      return newInstanceOf(constructor);
    }

    if (waitingList.contains(impl)) {
      StringBuilder circleDependencies = new StringBuilder();
      for (Class<?> aClass : waitingList) {
        circleDependencies.append(aClass.getCanonicalName()).append(" ");
      }
      throw new RuntimeException("Could not create instance of " + impl.getCanonicalName()
          + ", cause there're circle dependencies: " + circleDependencies.toString());
    } else {
      waitingList.add(impl);
    }

    try {
      Class<?>[] parameterTypes = constructor.getParameterTypes();
      Object[] parameters = new Object[parameterTypes.length];
      for (int i = 0; i < parameterTypes.length; i++) {
        parameters[i] = getOrCreateInstance(parameterTypes[i]);
      }
      return newInstanceOf(constructor, parameters);
    } finally {
      waitingList.remove(impl);
    }
  }

  private Constructor<?> getConstructorOf(Class<?> rawType) {
    Constructor<?> injectableConstructor = null;
    for (Constructor<?> constructor : rawType.getDeclaredConstructors()) {
      if (!constructor.isAnnotationPresent(Inject.class)) {
        continue;
      }

      if (injectableConstructor != null) {
        throw new RuntimeException("More than one constructor annotated with @Inject in "
            + rawType.getCanonicalName() + ". " + CONSTRUCTOR_RULES);
      }

      injectableConstructor = constructor;
    }

    if (injectableConstructor != null) {
      return injectableConstructor;
    }

    // If no annotated constructor is found, look for a no-arg constructor instead.
    try {
      Constructor<?> noArgConstructor = rawType.getDeclaredConstructor();

      // Disallow private constructors on non-private classes (unless they have @Inject)
      if (Modifier.isPrivate(noArgConstructor.getModifiers())
          && !Modifier.isPrivate(rawType.getModifiers())) {
        throw new RuntimeException("Could not find a suitable constructor in "
            + rawType.getCanonicalName() + ". " + CONSTRUCTOR_RULES);
      }

      return noArgConstructor;
    } catch (NoSuchMethodException e) {
      throw new RuntimeException("Could not find a suitable constructor in "
          + rawType.getCanonicalName() + ". " + CONSTRUCTOR_RULES);
    }
  }

  private Object newInstanceOf(Constructor<?> constructor, Object... args) {
    boolean isAccessible = constructor.isAccessible();
    try {
      if (!isAccessible) {
        constructor.setAccessible(true);
      }
      return constructor.newInstance(args);
    } catch (InvocationTargetException e) {
      throw new RuntimeException("Create new instance of "
          + constructor.getDeclaringClass().getCanonicalName() + " error", e);
    } catch (InstantiationException e) {
      throw new RuntimeException("Create new instance of "
          + constructor.getDeclaringClass().getCanonicalName() + " error", e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException("Create new instance of "
          + constructor.getDeclaringClass().getCanonicalName() + " error", e);
    } finally {
      if (!isAccessible) {
        constructor.setAccessible(false);
      }
    }
  }

}
