/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core;

import net.gree.asdk.core.inject.Inject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class GConnectivityManager {

  private final static String TAG = "GConnectivityManager";

  private Context context;

  private Set<ConnectivityListener> listeners = Collections
      .synchronizedSet(new HashSet<ConnectivityListener>());

  // Current connectivity status
  private boolean currentConnectivity = false;

  private boolean isFirstTimeCheck = true;

  @Inject
  public GConnectivityManager(Context context) {
    this.context = context;
  }

  /**
   * Provides network status for registered code.
   */
  public interface ConnectivityListener {
    public void onConnectivityChanged(boolean isConnected);
  }

  /**
   * Check and notify {@link ConnectivityListener} if it's first time check or connectivity changed.
   */
  public synchronized void checkAndNotifyConnectivity() {

    boolean connected = doCheckConnectivity();

    if (isFirstTimeCheck || currentConnectivity != connected) {
      currentConnectivity = connected;
      notifyConnectivityChanged(currentConnectivity);
    }

    if (isFirstTimeCheck) {
      isFirstTimeCheck = false;
    }
  }

  public boolean checkConnectivity() {
    return doCheckConnectivity();
  }

  /**
   * Registration for network status callbacks.
   * 
   * @param connectivityListener
   */
  public void registerListener(ConnectivityListener connectivityListener) {
    GLog.d(TAG, "Register listener: " + connectivityListener);
    listeners.add(connectivityListener);
  }

  /**
   * Unregister network status callbacks.
   * 
   * @param connectivityListener
   */
  public void unregisterListener(ConnectivityListener connectivityListener) {
    listeners.remove(connectivityListener);
  }

  // Just for unit test
  void setCurrentConnectivity(boolean currentConnectivity) {
    this.currentConnectivity = currentConnectivity;
  }

  private boolean doCheckConnectivity() {
    boolean connected;
    try {
      ConnectivityManager connectivityManager =
          (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
      connected = activeNetInfo != null && activeNetInfo.isConnected();
    } catch (Exception ex) {
      connected = false;
      GLog.w(
          TAG,
          "Add <uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" /> to manifest for network status monitoring.");
    }
    return connected;
  }

  private void notifyConnectivityChanged(final boolean isConnected) {
    GLog.d(TAG, "started notifying connectivity changed: " + isConnected);
    // If current thread is main thread, then start a new thread to do it
    if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          doNotifyConnectivityChanged(isConnected);
        }
      }).start();
    } else {
      doNotifyConnectivityChanged(isConnected);
    }
  }

  private void doNotifyConnectivityChanged(boolean isConnected) {
    for (ConnectivityListener listener : listeners) {
      GLog.d(TAG, "Nofify ConnectivityListener: " + listener);
      listener.onConnectivityChanged(isConnected);
    }
  }
}
