/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Map;

import net.gree.asdk.core.Injector;
import net.gree.vendor.com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.util.Url;
import net.gree.vendor.com.google.gson.reflect.TypeToken;

public class RequestUtil {
  private static final String TAG = "RequestUtil";

  public static String toQueryString(Map<String, String> params) {
    if (params == null)
      return "";
    String queryParam = "";
    if (0 < params.size()) {
      for (Map.Entry<String, String> entry : params.entrySet()) {
        queryParam += entry.getKey() + "=" + entry.getValue() + "&";
      }
      queryParam = queryParam.substring(0, queryParam.length() - 1); // trim last &
    }
    return queryParam;
  }

  public static String action2Url(String action) {
    return action2Url(action, Constants.USE_SSL);
  }

  public static String action2Url(String action, boolean use_ssl) {
    String url = null;
    if (use_ssl) {
      url = Url.getSecureApiEndpointWithAction(action);
    } else {
      url = Url.getApiEndpointWithAction(action);
    }
    return url;
  }

  public static String toEntityJson(Map<String, String> params) {
    Type mapType = new TypeToken<Map<String, String>>() {}.getType();
    String ret = "";
    if (params == null) {
      return ret;
    }
    if (params.size() > 0) {
      ret = Injector.getInstance(Gson.class).toJson(params, mapType);
    }
    return ret;
  }

  public static HttpEntity generateEntity(Map<String, String> data) {
    if (data == null) {
      return null;
    }

    String stringParam = toEntityJson(data);
    if (stringParam == null) {
      return null;
    }
    HttpEntity entity = null;
    try {
      entity = new StringEntity(stringParam, "UTF-8");
      if (entity.isRepeatable()) {
        try {
          String estring = entity.getContent().toString();
          RequestLogger.getInstance().d(TAG, "Entity:" + estring);
        } catch (IOException ioe) {}
      }
    } catch (UnsupportedEncodingException e) {
      GLog.printStackTrace(TAG, e);
      RequestLogger.getInstance().d(TAG, e.getMessage());
    } catch (ClassCastException e) {
      GLog.printStackTrace(TAG, e);
      RequestLogger.getInstance().d(TAG, e.getMessage());
    }
    return entity;
  }

  public static HttpEntity generateEntity(String data) {
    if (data == null) {
      return null;
    }
    HttpEntity entity = null;
    try {
      entity = new StringEntity(data, "UTF-8");
      if (entity.isRepeatable()) {
        try {
          String estring = entity.getContent().toString();
          RequestLogger.getInstance().d(TAG, "Entity:" + estring);
        } catch (IOException ioe) {}
      }
    } catch (UnsupportedEncodingException e) {
      GLog.printStackTrace(TAG, e);
      RequestLogger.getInstance().d(TAG, e.getMessage());
    } catch (ClassCastException e) {
      GLog.printStackTrace(TAG, e);
      RequestLogger.getInstance().d(TAG, e.getMessage());
    }
    return entity;
  }

  public static HttpEntity generateEntity(InputStream parameter) {
    GreeChunkedInputStreamEntity reqEntity = new GreeChunkedInputStreamEntity(parameter, -1);
    return reqEntity;
  }
}
