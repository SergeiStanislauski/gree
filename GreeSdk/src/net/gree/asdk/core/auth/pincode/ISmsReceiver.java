package net.gree.asdk.core.auth.pincode;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import net.gree.asdk.core.auth.pincode.SmsReceiver.SmsListener;


public interface ISmsReceiver {

  /**
   * Replace the SmsListener
   * @param listener the new listener
   */
  public abstract void setListener(SmsListener listener);

  /**
   * Get IntentFilter to be passed to Context.registerReceiver() along with this instance.
   * @return object of IntentFilter to receive SMS
   */
  public abstract IntentFilter getIntentFilter();

  /**
   * This must be called when we a SMS is being received,
   * the intent action must be SMS_RECEIVED_ACTION
   * @param context
   * @param intent
   */
  public abstract void onReceive(Context context, Intent intent);

}
