/*
* Copyright 2012 GREE, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package net.gree.asdk.core.analytics.performance;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class PerformanceData {
  private static final String TAG = "PerformanceData";

  enum NetworkStatus {
    NONE,
    MOBILE,
    WIFI,
  };

  private Context mContext;
  private int mCount;
  private PerformanceFlowIndex mIndex;

  // FIFO blocking queue for store peformance line log parameter.
  private final BlockingQueue<PerformanceLineItem> queue = new LinkedBlockingQueue<PerformanceLineItem>();

  public PerformanceData(Context context, PerformanceFlowIndex flowIndex, int count) {
    mContext = context;
    mIndex = flowIndex;
    mCount = count;
  }

  public void init() {
    mIndex.loadPointMap();
  }

  public void init(int subIndex) {
    mIndex.loadPointMap(subIndex);
  }

  public int count() {
    return queue.size();
  }

  public int getIndexType() {
    return mIndex.getFlowIndex();
  }

  synchronized public void record(String key) {
    // Check netork status.
    ConnectivityManager cm = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo nwInfo = cm.getActiveNetworkInfo();
    int nwStatusCode = NetworkStatus.NONE.ordinal();
    if (nwInfo != null) {
      if (nwInfo.getTypeName().compareToIgnoreCase("wifi") == 0) {
        nwStatusCode = NetworkStatus.WIFI.ordinal();
      } else if (nwInfo.getTypeName().compareToIgnoreCase("mobile") == 0) {
        nwStatusCode =NetworkStatus.MOBILE.ordinal();
      }
    }
    // if not exist key hash, not recorded.
    if (!mIndex.isExistPointMapKey(key)) {
      GLog.v(TAG, "Not found map key:" + key);
      return;
    }

    PerformanceLineItem tmpItem = new PerformanceLineItem(mIndex.getPointMapKeyData(key), nwStatusCode);
    queue.add(tmpItem);
    return;
  }

  protected String output() {
    PerformanceLineItem tmpItem = dequeue();
    if (tmpItem == null) {
      GLog.e("PerformanceItem", "tmp item is null.");
      return null;
    }
    return mIndex.getFlowIndex() + "," + tmpItem.getPointIndex() + "," + mCount + "," + Long.toString(tmpItem.getTime()) + "," + tmpItem.getNwStatus() + "\n";
  }

  synchronized private PerformanceLineItem dequeue() {
    PerformanceLineItem tmpItem = null;
    try {
      tmpItem = queue.take();
    } catch (Exception e) {}

    return tmpItem;
  }

  /**
   * Object for store performance line parameter.
   */
  private class PerformanceLineItem {
    private int mPointIndex;    // Point index number.
    private long mTime;         // UNIX time (msec)
    private int mNwStatus;      // Network Status.

    /**
     * Constractor.
     */
    public PerformanceLineItem(int pointIndex, int nwStatus) {
      mPointIndex = pointIndex;
      mTime = System.currentTimeMillis();
      mNwStatus = nwStatus;
    }

    public int getPointIndex()  { return mPointIndex; }
    public long getTime()       { return mTime; }
    public int getNwStatus()    { return mNwStatus; }
  }
}
