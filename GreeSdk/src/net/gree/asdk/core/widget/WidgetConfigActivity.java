/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.widget;

import net.gree.asdk.R;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.storage.JSONStorage;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

/**
 * Widget configure activity
 * 
 * @author GREE, Inc.
 * 
 */
public class WidgetConfigActivity extends Activity {
  private static final String TAG = "WidgetConfigActivity";
  private int mAppWidgetId;
  private ImageButton mGreeSetting;
  private ListView mFilterList;
  private String[] mFilter;
  private SparseBooleanArray mSelect;
  private JSONStorage mWidgetData;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.gree_widget_setting_layout);
    Intent intent = getIntent();
    if (intent.hasExtra(WidgetProvider.EXTRA_WIDGET_FILTER)) {
      mFilter = intent.getStringArrayExtra(WidgetProvider.EXTRA_WIDGET_FILTER);
    }
    mGreeSetting = (ImageButton) findViewById(R.id.gree_widget_setting);
    mGreeSetting.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if (mSelect != null) {
          String[] str = new String[mSelect.size()];
          for (int i = 0; i < mSelect.size(); i++) {
            GLog.i(TAG, "[FILTER_SELECT] " + mFilter[mSelect.keyAt(i)]);
            str[i] = mFilter[mSelect.keyAt(i)];
          }
          Intent intent = new Intent();
          intent.setAction(WidgetProvider.WIDGET_SETTING_ACTION);
          intent.putExtra(WidgetProvider.EXTRA_WIDGET_FILTER, str);
          sendBroadcast(intent);
        }
        finish();
      }
    });

    mWidgetData = Injector.getInstance(JSONStorage.class);
    mWidgetData.setSubFilterList("widget_data");
    mFilter = mWidgetData.getSubFilterList();

    if (mFilter != null) {
      mFilterList = (ListView) findViewById(R.id.gree_widget_filter_list);
      mFilterList.setAdapter(new ArrayAdapter<String>(this,
          android.R.layout.simple_list_item_multiple_choice, mFilter));
      mFilterList.setItemsCanFocus(false);
      mFilterList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
      mFilterList.setOnItemClickListener(new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
          mSelect = mFilterList.getCheckedItemPositions();
        }
      });
    }
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    return true;
  }
}
