/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.dashboard;

import net.gree.asdk.core.RR;
import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class DashboardNavigationBar extends LinearLayout {

  private static final int LANDSCAPE_HIGHT = 32;
  private static final int PORTRAIT_HIGHT = 44;

  private void init(Context context) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(RR.layout("gree_dashboard_navigation_bar"), null, false);
    addView(view);
    adjustNavigationBarHeight(getResources().getConfiguration());
  }

  /**
   * Constructor of DashboardNavigationBar
   * @param context the context from which dashboard navigation must be displayed
   */
  public DashboardNavigationBar(Context context) {
    super(context);
    init(context);
  }

  /**
   * Constructor of DashboardNavigationBar
   * @param context the context from which dashboard navigation must be displayed
   * @param attrs attributeSet information
   */
  public DashboardNavigationBar(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }
  
  /**
   * Constructor of DashboardNavigationBar
   * @param context the context from which dashboard navigation must be displayed
   * @param attrs attributeSet information
   * @param defStyle defStyle
   */
  public DashboardNavigationBar(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(context);
  }

  /**
   * Adjust navigation bar height
   * @param config configuration infomation
   */
  public void adjustNavigationBarHeight(Configuration config) {
    FrameLayout frame = (FrameLayout) findViewById(RR.id("gree_navigation_bar_frame"));
    int height = getNavigationBarHeight(getContext(), config);
    if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
      frame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height));
    } else {
      frame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height));
    }
  }
  
  public static int getNavigationBarHeight(Context context, Configuration config) {
    final float scale = context.getResources().getDisplayMetrics().density;
    if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
      return (int) (LANDSCAPE_HIGHT * scale);
    } else {
      return (int) (PORTRAIT_HIGHT * scale);
    }    
  }
}
