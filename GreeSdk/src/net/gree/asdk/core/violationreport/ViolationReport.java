/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.violationreport;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;
import net.gree.vendor.com.google.gson.Gson;

import java.util.Map;
import java.util.TreeMap;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

/*
 *  Class to report illegal user
 *  This class is expected to be used by Messanger
 */
public class ViolationReport {
  private static final String TAG = ViolationReport.class.getSimpleName();
 
  private static final String endpoint = "/violationreport/";
  private static final String suffix = "@me/@app";

  /*
   * Interface for client to listen to results of request
   */
  public interface ViolationReportListener {
    public void onSuccess(JSONObject res);
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Report API to server
   * 
   * @param json which includes information about illegal user
   * @param listener to listen to the result
   * @return true if request is accepted, otherwise false(ex. parameter is null)
   */
  public boolean report(String json, final ViolationReportListener listener) {
   
    if(json == null) {
      return false;
    }
    new JsonClient().oauth(Url.getApiEndpoint() + endpoint + suffix, "POST", null, json, false, new OnResponseCallback<String>() {

      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        GLog.v(TAG, "[onSuccess] responseCode : " + responseCode + " response : " + response);
        if(listener == null) {
          return;
        }
        try {
          JSONObject obj = new JSONObject(response.substring(response.indexOf("{\"entry\":")));
          obj = obj.getJSONObject("entry");
          listener.onSuccess(obj);
        } catch (JSONException e) {
          GLog.v(TAG, "[onFailure from onSuccess]");
          listener.onFailure(responseCode, headers, response);
          GLog.printStackTrace(TAG, e);
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.v(TAG, "[onFailure] responseCode : " + responseCode + " response : " + response);
        if(listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
      
    });

    return true;
  }

  /**
   * API to help client generate input parameter for {@link ViolationReport#report(String, ViolationReportListener)}
   * 
   * @param contentType to identify type of content("9999" normally)
   * @param categoryId to identify category(selected by user)
   * @param comment input by user
   * @param targetUserId(optional) to identify illegal user
   * @param textId(optional) to identify this report
   * @return null if more than one of the mandatory parameters are null, otherwise String value with corresponding format to report()
   */
  public String setParam(String contentType, String categoryId, String comment, String targetUserId, String textId) {
    String str = null;
    if(contentType == null || categoryId == null || comment == null) {
      return str;
    }
    TreeMap<String, Object> input = new TreeMap<String, Object>();
    input.put("contentType", contentType);
    input.put("categoryId", categoryId);
    input.put("comment", comment);
    // taregetUserId and textId are not mandatory
    input.put("targetUserId", targetUserId);
    input.put("textId", textId);
    
    return Injector.getInstance(Gson.class).toJson(input);
  }
}
