/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.api;

import android.text.TextUtils;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.storage.LocalStorage;
import net.gree.asdk.core.util.CoreData;


/**
* This class is used to set or unset platform settings.
* The same setting can be applied from an XML resource file on initialization of the platform. 
*/
public class GreePlatformSettings {

  /**
   * Constructor of GreePlatformSettings. It is never called.
   */
  private GreePlatformSettings() {
  }
  
  /**
   * Specifies application id
   */
  public static final String ApplicationId = InternalSettings.ApplicationId;

  /**
   * Specifies consumer key. This is not encrypted.
   */
  public static final String ConsumerKey = InternalSettings.ConsumerKey;

  /**
   * Specifies consumer secret. This is not encrypted.
   */
  public static final String ConsumerSecret = InternalSettings.ConsumerSecret;

  /**
   * Specifies consumer key. This is not encrypted.
   */
  public static final String EncryptedConsumerKey = InternalSettings.EncryptedConsumerKey;

  /**
   * Specifies consumer secret. This is encrypted.
   */
  public static final String EncryptedConsumerSecret = InternalSettings.EncryptedConsumerSecret;

  /**
   * Specifies development mode
   */
  public static final String DevelopmentMode = InternalSettings.DevelopmentMode;

  /**
   * Specifies whether SDK will allow play as Grade 0 user mode or not.
   */
  public static final String EnableGrade0 = InternalSettings.EnableGrade0;

  /**
   * Specifies whether SDK will allow users to opt out of GREE
   */
  public static final String AllowUserOptOutOfGREE = InternalSettings.AllowUserOptOutOfGREE;

  /**
   * Specifies whether we are using push notification or not.
   */
  public static final String UsePushNotification = InternalSettings.UsePushNotification;

  /**
   * Specifies the sender id which is specified in the developer center
   */
  public static final String PushNotificationSenderId = InternalSettings.PushNotificationSenderId;

/**
 * Specifies whether or not to show notifications.
 */
  public static final String NotificationEnabled = InternalSettings.NotificationEnabled;

  /**
   * Specifies whether the place of notification balloon is the bottom or not.
   */
  public static final String NotificationsAtScreenBottom = InternalSettings.NotificationsAtScreenBottom;
  
/**
 * Specifies whether or not to show local notifications.
 */
  public static final String DisableLocalNotification = InternalSettings.DisableLocalNotification;

  /**
   * Specifies whether dumping log or not
   */
  public static final String EnableLogging = InternalSettings.EnableLogging;

  /**
   * Specifies whether dumping log to file or not
   */
  public static final String WriteToFile = InternalSettings.WriteToFile;

  /**
   * Specifies the level of log. Error == 0, Warn == 25, Info == 50, Debug == 100.
   */
  public static final String LogLevel = InternalSettings.LogLevel;

/**
 * Gets the value of whether or not the Notification is enabled.
 * @return true if the Notification is enabled, false otherwise.
 */
  public static final boolean getNotificationEnabled() {
    String value = CoreData.get(NotificationEnabled);
    if (TextUtils.isEmpty(value)) {
      return true;
    }
    if (value.equals("true")) {
      return true;
    } else {
      return false;
    }
  }

/**
 * Sets the value of setting to show notification
 * @param isEnable true is enable to show notification
 */
  public static final void setNotificationEnabled(boolean isEnable) {
    String value = "false";
    if (isEnable) {
      value = "true";
    }
    CoreData.put(NotificationEnabled, value);
    getLocalStorage().putString(NotificationEnabled, value);
  }

/**
 * Gets the value of whether the local notification is enabled.
 * @return true if the notification is enabled, false otherwise.
 */
  public static final boolean getLocalNotificationEnabled() {
    String value = CoreData.get(DisableLocalNotification);
    if (TextUtils.isEmpty(value)) {
      return true;
    }
    if (value.equals("true")) {
      return false;
    } else {
      return true;
    }
  }

/**
 * Sets the value of whether or not to show local notifications.
 * @param is_enable if true is passed, show local notifications are enabled.
 */
  public static final void setLocalNotificationEnabled(boolean isEnable) {
    String value = "true";
    if (isEnable) {
      value = "false";
    }
    CoreData.put(DisableLocalNotification, value);
    getLocalStorage().putString(DisableLocalNotification, value);
  }

  /**
   * Obtains the value of whether or not the Notifications are shown at the bottom of the device or not.
   * @return true if the notification is shown at bottom, false otherwise.
   */
  public static final boolean isNotificationsAtScreenBottom() {
    String value = CoreData.get(NotificationsAtScreenBottom);
    if (TextUtils.isEmpty(value)) {
      return false;
    }
    if (value.equals("true")) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Sets whether the notifications will at the bottom of the device screen (orientation independent) or not.
   * @param is_bottom  if true, will set the notifications to appear at the bottom. Default setting is false.
   */
  public static final void setNotificationsAtScreenBottom(boolean isBottom) {
    String value = "false";
    if (isBottom) {
      value = "true";
    }
    CoreData.put(NotificationsAtScreenBottom, value);
    getLocalStorage().putString(NotificationsAtScreenBottom, value);
  }

  /**
   * Toggles the debugging information on and off. Default value is false.
   * @param debugIsOn  turn debug on or off.
   */
  public static final void enableLogging(boolean debugIsOn) {
    String value = "false";
    if (debugIsOn) {
      value = "true";
    }
    CoreData.put(EnableLogging, value);
    getLocalStorage().putString(EnableLogging, value);
  }

  /**
   * Sets the output path of the log file. This should begin at the root directory. <br> 
   * Default expectation is that the files are logged to external storage.
   * 
   * @param filePath the file path e.g "/sdcard/log.txt"
   */
  public static final void setWriteToFile(String filePath) {
    CoreData.put(WriteToFile, filePath);
    getLocalStorage().putString(WriteToFile, filePath);
  }

  /**
   * Sets the Logcat logging level to be one of the following modes: Error, Warning, Info, and Debug.
   * 
   * @param loglevel the desired log level. For Error pass 0, Warning pass 25, Info pass 50, and Debug pass 100.
   */
  public static final void setLoglevel(int loglevel) {
    CoreData.put(LogLevel, String.valueOf(loglevel));
    getLocalStorage().putString(LogLevel, String.valueOf(loglevel));
  }

  private static LocalStorage getLocalStorage() {
    return Injector.getInstance(LocalStorage.class);
  }
}
