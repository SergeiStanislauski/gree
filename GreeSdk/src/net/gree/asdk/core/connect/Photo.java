/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.connect;

import java.util.HashMap;
import java.util.Map;

import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.GreeRequest;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.request.SnsResponseHandler;
import net.gree.asdk.core.request.StringConverter;
import net.gree.asdk.core.request.helper.MethodHelper;

/**
 * Photo API.
 * You can get a photo information which is posted by user by using this class.
 */
public class Photo {
  private String mApiUrl;
  private GreeRequest mGreeRequest;

  private String mRequestedUrl;
  private int mRequestedMethod;

  public String getApiUrl() {
    return mApiUrl;
  }

  public String getRequestedUrl() {
    return mRequestedUrl;
  }

  public int getRequestedMethod() {
    return mRequestedMethod;
  }

  /**
   * Create the Photo API.
   * @param url Connect API URL.
   */
  public Photo(Url url) {
    mApiUrl = url.getConnectUrl() + "/api/rest/photo/entries/";
  }

  /**
   * Get the album information.
   * The information has the following data:
   *   photo id
   *   album id
   *   title
   *   discription
   *   number of likes
   *   number of comments
   *   url of the photo
   * @param userId user id which own the album.
   * @param albumId album id.
   * @param offset the starting index of the album.
   * @param limit max size of the photo api returns.
   * @param listener listener which is receiving the result of api call.
   */
  public void getAlbumInfo(String userId, String albumId, int offset, int limit, final OnResponseCallback<String> listener) {
    StringBuilder sb = new StringBuilder(mApiUrl);
    sb.append(userId).append("?album_id=").append(albumId).append("&offset=").append(offset).append("&limit=").append(limit);
    request(sb.toString(), BaseClient.METHOD_GET, listener);
  }

  /**
   * Get the photo information.
   * The information has the following data:
   *   photo id
   *   album id
   *   title
   *   discription
   *   number of likes
   *   number of comments
   *   url of the photo
   * @param userId user id which own the photo.
   * @param photoId photo id.
   * @param listener listener which is receiving the result of api call.
   */
  public void getInfo(String userId, String photoId, OnResponseCallback<String> listener) {
    StringBuilder sb = new StringBuilder(mApiUrl);
    sb.append(userId).append('/').append(photoId);
    request(sb.toString(), BaseClient.METHOD_GET, listener);
  }

  private void request(String url, int method, OnResponseCallback<String> listener) {
    Map<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "application/json");

    StringConverter converter = new StringConverter();
    SnsResponseHandler handler = new SnsResponseHandler(listener);

    if (mGreeRequest == null) {
      new GreeRequest(url, MethodHelper.parseInt(method)).header(headers).sync(true)
      .request(converter, handler);
    } else {
      mGreeRequest.entity(null).header(headers).sync(true).request(converter, handler);
    }

    mRequestedUrl = url;
    mRequestedMethod = method;
  }
}
