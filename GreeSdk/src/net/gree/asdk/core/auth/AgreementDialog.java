/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import android.content.Context;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.WebView;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.ui.WebViewPopupDialog;
import net.gree.asdk.core.util.Scheme;

/**
 * Class for confirming agreement
 * @author GREE, Inc.
 */
class AgreementDialog extends WebViewPopupDialog {
  public static final int OPENED = 1;
  public static final int CLOSED = 2;

  private AgreementDialogWebViewClient mWebViewClient;
  protected String mUrl;

  /**
   * Initializer
   * @param context
   * @param url
   */
  public AgreementDialog(Context context, String url) {
    super(context);
    switchDismissButton(false);
    mUrl = url;
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
      WebView webView = getWebView();
      if (webView.canGoBack()) {
        webView.goBack();
        return true;
      } else {
        // ignore back key
        return true;
      }
    }
    return super.onKeyDown(keyCode, event);
  }

  @Override
  protected void createWebViewClient() {
    mWebViewClient = new AgreementDialogWebViewClient(getContext());
  }

  @Override
  protected PopupDialogWebViewClient getWebViewClient() {
    return mWebViewClient;
  }

  @Override
  protected int getOpenedEvent() { return OPENED; }

  @Override
  protected int getClosedEvent() { return CLOSED; }

  @Override
  protected String getEndPoint() { return mUrl; }

  class AgreementDialogWebViewClient extends PopupDialogWebViewClient {
    private Message mDontResend;

    public AgreementDialogWebViewClient(Context context) {
      super(context);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      if (handleLogout(url)) { return true; }
      return super.shouldOverrideUrlLoading(view, url);
    }

    private boolean handleLogout(String url) {
      if (url.startsWith(Scheme.getLogoutScheme())) {
        dismiss();
        mAuthorizer.logout();
        mAuthorizer.authorize(mContext, null, null, null);
        return true;
      }
      return false;
    }

    @Override
    public void onFormResubmission(WebView view,Message dontResend,Message resend){
      if (mDontResend != null){
        mDontResend.sendToTarget();
        return;
      }
      mDontResend = dontResend;
      if (resend != null){
        resend.sendToTarget();
        mDontResend = null;
      }
    }

    @Override
    protected void onDialogClose(String url) { }
  }
}
