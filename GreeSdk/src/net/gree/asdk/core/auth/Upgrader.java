/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.ui.GreeWebView;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;

import android.content.Context;
import android.net.Uri;

public class Upgrader {
  private Context mContext;
  private String mUpgradeUrl;
  private GreeWebView mWebView;
  public static final int REQUEST_TYPE_INTERNAL_WEBVIEW = 0;
  public static final int REQUEST_TYPE_BROWSER = 1;
  static final String QUERY_PARAM_TARGET_GRADE = "target_grade";

  Upgrader(Context context, String url, GreeWebView webview, String userKey) {
    mContext = context;
    Uri uri = Uri.parse(url);
    String targetGrade = uri.getQueryParameter(QUERY_PARAM_TARGET_GRADE);
    String regCode = uri.getQueryParameter(SetupActivity.QUERY_PARAM_REG_CODE);
    String entCode = uri.getQueryParameter(SetupActivity.QUERY_PARAM_ENT_CODE);

    StringBuilder upgradeUrl =
        new StringBuilder(Url.getUpgradeUserUrl())
            .append('&').append(QUERY_PARAM_TARGET_GRADE).append('=').append(targetGrade).append("&app_id=")
            .append(Core.getAppId()).append("&user_id=").append(Injector.getInstance(IAuthorizer.class).getOAuthUserId());
    if (regCode != null) {
      upgradeUrl.append('&').append(SetupActivity.QUERY_PARAM_REG_CODE).append('=').append(regCode);
    }
    if (entCode != null) {
      upgradeUrl.append('&').append(SetupActivity.QUERY_PARAM_ENT_CODE).append('=').append(entCode);
    }
    mUpgradeUrl = AuthorizeContext.appendQueryParameter(upgradeUrl.toString(), userKey);
    mWebView = webview;
  }

  boolean request(int type) {
    boolean ret = false;
    switch (type) {
      case REQUEST_TYPE_INTERNAL_WEBVIEW:
        ret = requestInnerWebView();
        break;
      case REQUEST_TYPE_BROWSER:
        ret = requestBrowser();
        break;
    }
    return ret;
  }

  private boolean requestInnerWebView() {
    mWebView.loadUrl(mUpgradeUrl);
    return true;
  }

  private boolean requestBrowser() {
    return Util.startBrowser(mContext, mUpgradeUrl);
  }
}
