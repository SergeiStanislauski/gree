/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.ui;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.webkit.WebView;
import android.widget.ProgressBar;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.codec.Base64;

/**
 * This class is used by InviteDialog, ShareDialog and RequestDialog
 */
public abstract class AbsServiceDialog extends AuthorizableDialog {
  private static final String TAG = "AbsServiceDialog";
  private Map<String, Object> mParams = null;
  private static final int BITMAP_COMPRESS_QUALITY = 100;

  /**
   * Default constructor.
   * @param context The Context the Dialog is to run it.
   */
  public AbsServiceDialog(Context context) {
    super(context);
  }

  /**
   * Set parameters as a TreeMap. The parameters will be post to server as query data in each service.
   * You can set any parameters, but in InviteDialog, ShareDialog and RequestDialog, you have to set the parameters which is allowed by javadoc.
   * @param params TreeMap object which having the keys as elements.
   */
  protected void setParams(TreeMap<String, Object>  params) {
    if (mParams == null) {
      mParams = params;
    } else {
      for (Map.Entry<String, Object> param : params.entrySet()) {
        mParams.put(param.getKey(), param.getValue());
      }
    }
  }

  /**
   * Set parameter. If you want to set just one parameter, use this function.
   * @param key the key of query parameter
   * @param value the value of query parameter
   */
  protected void addParam(String key, Object value) {
    mParams.put(key, value);
  }

  /**
   * This function create data to post.
   * @return The instance of String class for posting data
   */
  protected String buildPostData() {
    final StringBuilder postBuilder = new StringBuilder("app_id=").append(Core.getAppId());

    if (mParams == null){
      return postBuilder.toString();
    }
    
    try {
      for (Map.Entry<String, Object> param : mParams.entrySet()) {
        final String key = param.getKey();
        final Object value = param.getValue();
        
        String str_value = convertToString(value);
        if (str_value != null && str_value.length() > 0) {
          postBuilder.append("&").append(key).append("=").append(URLEncoder.encode(str_value, "UTF-8"));
        }
      }
    } catch (UnsupportedEncodingException e) {
      GLog.printStackTrace(TAG, e);
    }
    return postBuilder.toString();
  }

  /**
   * This function convert some data from Object to String.
   * @param value The value is input as a instance of Object, but this instance have to be Bitmap, String[], JSONObject or String.
   * @return instance of String converted from Object class
   */
  private String convertToString(Object value) {
    String ret_value = "";
    if (value instanceof Bitmap) {
      Bitmap image = (Bitmap)value;
      ret_value = getBase64String(image);
    } else if (value instanceof String[]) {
      String[] list = (String[])value;
      if (list.length == 0) {
        return ret_value;
      }
      StringBuilder builder = new StringBuilder("");
      builder.append(list[0]);
      for (int i = 1; i < list.length; i++) {
        builder.append(",");
        if ((list[i] != null) && (list[i].length() != 0)) {
          builder.append(list[i]);
        }
      }
      ret_value = builder.toString();
    } else if (value instanceof JSONObject) {
      JSONObject attrsObj = (JSONObject)value;
      JSONArray attrsArray = new JSONArray();
      if (attrsObj.length() > 0) {
        attrsArray.put(attrsObj);
        ret_value = attrsArray.toString();
      }
    } else {
      try {
        ret_value = (String) value;
      } catch (Exception e) {
        GLog.printStackTrace(TAG, e);
      }
    }
    return ret_value;
  }

  /**
   * This function convert from Bitmap to String for posting data.
   * @param bitmap The instance of Bitmap to be post
   * @return The instance of String converted from bitmap
   */
  private static String getBase64String(Bitmap bitmap) {
    if (bitmap == null) {
      return "";
    }

    ByteArrayOutputStream aBaos = new ByteArrayOutputStream();
    bitmap.compress(CompressFormat.JPEG, BITMAP_COMPRESS_QUALITY, aBaos);
    byte[] aData = aBaos.toByteArray();

    return Base64.encodeBytes(aData);
  }

  /**
   * This function will be called when the dialog is shown.
   */
  @Override
  protected void onShow() {
    setPostData(buildPostData());
  }

  /**
   * This function will be called when the dialog is initialized.
   */
  @Override
  protected void createWebViewClient() {
    PopupDialogWebViewClient webView = new ServiceDialogWebViewClient(getContext());
    setWebViewClient(webView);
    ProgressBar progress = (ProgressBar) findViewById(RR.id("gree_popup_dialog_loading_indicator"));
    setWebChromeClient(new PopupDialogWebViewChromeClient(progress));
  }

  /**
   * WebViewClient for ServiceDialog
   */
  private class ServiceDialogWebViewClient extends PopupDialogWebViewClient {
    /**
     * Default constructor
     * @param context The Context the Dialog is to run it.
     */
    public ServiceDialogWebViewClient(Context context) {
      super(context);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
      super.onPageFinished(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      return super.shouldOverrideUrlLoading(view, url);
    }

    /**
     * This function will be called when the dialog is closed.
     */
    @Override
    protected void onDialogClose(String url) {}
  }

  /**
   * This function will be called when the dialog is initialized and terminated.
   */
  @Override
  protected void clearParams() {
    super.clearParams();
    if (mParams != null) {
      Collection<String> keys = mParams.keySet();
      for (String key : keys) {
        Object obj = mParams.get(key);
        if (obj instanceof Bitmap) {
          Bitmap bitmap = Bitmap.class.cast(obj);
          bitmap.recycle();
        }
      }
      mParams = null;
    }
  }
}
