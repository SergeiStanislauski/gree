/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.request.Constants.HTTP_METHOD;
import net.gree.asdk.core.request.Constants.OAUTH_TYPE;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.util.Util;
import net.gree.oauth.signpost.exception.OAuthCommunicationException;
import net.gree.oauth.signpost.exception.OAuthExpectationFailedException;
import net.gree.oauth.signpost.exception.OAuthMessageSignerException;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import org.apache.http.*;
import org.apache.http.client.methods.*;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;

/**
 * GreeBaseClient, the work class that deal with http request The Class has no public method, never
 * should be called directly out side RequestPackage
 */
public class GreeBaseClient {
  private static final String TAG = "GreeBaseClient";

  private IGreeHttpExecuter mExecuter;
  private IAuthorizer mAuthorizerCore;

  GreeBaseClient(IGreeHttpExecuter executer, IAuthorizer authorizerCore) {
    if (executer == null) {
      throw new IllegalArgumentException("Null HttpExecuter when initialize GreeBaseClient");
    }
    if (authorizerCore == null) {
      throw new IllegalArgumentException("Null IAuthorizer when initialize GreeBaseClient");
    }
    mExecuter = executer;
    mAuthorizerCore = authorizerCore;
  }

 
  static {
    Looper looper = Looper.getMainLooper();
    Handler handler = new Handler(looper);
    handler.post(new Runnable() {
      @Override
      public void run() {
        try {
          Class.forName("android.os.AsyncTask");
        } catch (ClassNotFoundException e) {
          GLog.printStackTrace(TAG, e);
        }
      }
    });
  }

  /**
   * AsyncTask Subclass that deal with http request in separate thread and called back in main
   * thread
   * 
   * @param <T>
   */
  private class HttpRequestAsyncTask<T>
      extends AsyncTask<HttpUriRequest, Integer, ResponseHolder<T>> {

    private ResponseHandler<T> mHandler;
    private IConverter<T> mConverter;

    public HttpRequestAsyncTask(ResponseHandler<T> handler, IConverter<T> converter) {
      this.mHandler = handler;
      this.mConverter = converter;
    }

    @Override
    protected ResponseHolder<T> doInBackground(HttpUriRequest... params) {
      for (HttpUriRequest param : params) {
        RequestLogger.getInstance().startRequest(param, false);
        HttpResponse response = mExecuter.execute(param);
        ResponseHolder<T> holder = response == null ? null : new ResponseHolder<T>(mConverter, response);
        return holder;
      }
      return null;
    }

    @Override
    protected void onPostExecute(ResponseHolder<T> holder) {
      RequestLogger.getInstance().endRequest(holder);
      mHandler.onResponse(holder);
    }
  }

  private <T> boolean skipRequestWithoutAccessToken(ResponseHandler<T> listener) {
    return (Util.canOptOutOfGREE() && !mAuthorizerCore.hasOAuthAccessToken());
  }

  <T> void request(final URI uri, final HTTP_METHOD methodType, Map<String, String> headers,
      final HttpEntity entity, boolean synchronous, final ResponseHandler<T> handler,
      final IConverter<T> converter, OAUTH_TYPE oAuthType) {
    // if 3 legged oauth, check skipRequestWithoutAccessToken
    if (oAuthType == OAUTH_TYPE._3LEGGED && skipRequestWithoutAccessToken(handler)) {
      ResponseHolder<T> holder =
          new ResponseHolder<T>(null, 0, null, Constants.GRADE0_ERROR_MESSAGE);
      handler.onResponse(holder);
      return;
    }
    HttpUriRequest request = null;
    try {
      request = getRequest(uri, headers, methodType, entity);
      // sign the response
      sign(request, oAuthType);

      // decide if use the main thread or not
      if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
        // On UI thread, force to async.
        RequestLogger
            .getInstance()
            .w(TAG,
                "Call request from UI thread, but sync is set to true, SDK do not allow this, override this to use async");
        synchronous = false;
      }
      if (synchronous) {
        // Blocking here
        RequestLogger.getInstance().startRequest(request, synchronous);
        HttpResponse response = mExecuter.execute(request);
        ResponseHolder<T> holder = response == null ? null : new ResponseHolder<T>(converter, response);
        RequestLogger.getInstance().endRequest(holder);
        handler.onResponse(holder);
      } else {
        HttpRequestAsyncTask<T> asyncTask = new HttpRequestAsyncTask<T>(handler, converter);
        asyncTask.execute(request);
      }
    } catch (Exception e) {
      RequestLogger.getInstance().printStackTrace(TAG, e);
      handler.onResponse(null);
    }
  }

  private void sign(HttpUriRequest request, OAUTH_TYPE oAuthType)
      throws OAuthMessageSignerException, OAuthExpectationFailedException,
      OAuthCommunicationException {
    switch (oAuthType) {
      case _3LEGGED:
        mAuthorizerCore.signFor3Legged(request);
        break;
      case _2LEGGED:
        mAuthorizerCore.signFor2Legged(request);
        break;
      case _NONE:
      default:
        // Do Nothing
        break;
    }
  }

  private HttpUriRequest getRequest(URI uri, Map<String, String> headers, HTTP_METHOD methodType,
      HttpEntity entity) throws URISyntaxException, MalformedURLException {
    if (uri == null) {
      RequestLogger.getInstance().e(TAG, "uri is null");
      return null;
    }
    HttpUriRequest request = null;
    switch (methodType) {
      case GET:
        request = new HttpGet(uri);
        break;
      case POST:
        HttpPost post = new HttpPost(uri);
        if (entity != null) {
          post.setEntity(entity);
        }
        request = post;
        break;
      case PUT:
        HttpPut put = new HttpPut(uri);
        if (entity != null) {
          put.setEntity(entity);
        }
        request = put;
        break;
      case DELETE:
        request = new HttpDelete(uri);
        break;
      default:
        throw new RuntimeException("Specify BaseClient.METHOD_TYPE to methodType: " + methodType);
    }
    setRequestHeader(request, headers);
    return request;
  }

  void setRequestHeader(HttpUriRequest request, Map<String, String> headers) {
    String cookie = CookieStorage.getCookieFor(request.getURI().getHost());
    boolean sawCookie = false;
    if (headers != null) {
      for (Map.Entry<String, String> entry : headers.entrySet()) {
        String key = entry.getKey();
        if (key.equals("Cookie")) {
          sawCookie = true;
        }
        String value = entry.getValue();
        if (value != null) {
          request.setHeader(key, value);
        }
      }
    }
    if (!sawCookie && cookie != null && cookie.length() > 0) {
      request.setHeader("Cookie", cookie);
    }
    request.setHeader("Accept-Language", Locale.getDefault().toString());
  }
}
