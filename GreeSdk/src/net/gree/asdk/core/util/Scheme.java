/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.util;

import net.gree.asdk.core.Core;
import android.net.Uri;

/**
 * The scheme class builds the scheme for Auth and Payment.
 * The scheme are used, to be compared with url in the webviews, 
 * to redirect actions when the user click on urls.
 * See SetupActivity.shouldOverrideUrlLoading.
 * It is also used by external applications like SNS app and browser app to launch GGP applications.
 */
public class Scheme {
  private static final String GREEAPP_SCHEME = "greeapp";

  private static final String ENTER_HOST = "enter";
  private static final String SSO_REQUIRE = "sso-require";
  private static final String START_AUTHORIZATION_HOST = "start-authorization";
  private static final String GET_ACCESS_TOKEN_HOST = "get-accesstoken";
  private static final String CHOOSE_ACCOUNT = "choose-account";
  private static final String UPGRADE_HOST = "upgrade";
  private static final String LOGOUT_HOST = "logout";
  private static final String REOPEN_HOST = "reopen";
  private static final String COMPLETE_TRANSACTION_HOST = "complete-transaction";
  private static final String WALLET_DEPOSIT_HOST = "gree-wallet-deposit-launch";
  private static final String WALLET_DEPOSIT_HISTORY_HOST = "gree-wallet-deposit-history-launch";
  private static final String GREEREWARD_OFFERWALL_HOST = "gree-reward-offerwall";

  /**
   * The base scheme of ggp application,
   * this is used to call back an application from a Url.
   * @return "greeapp"
   */
  public static String getAppScheme() {
    return GREEAPP_SCHEME;
  }

  /**
   * Return the scheme for the current application.
   * e.g. greeappXXXX where XXXX is the application ID
   * @return "greeappXXXX"
   */
  public static String getCurrentAppScheme() {
    return GREEAPP_SCHEME + Core.getAppId();
  }

  /**
   * Return the scheme for the current application, with the host append.
   * @param host the host string to be append.
   * @return "greeapp123://host"
   */
  private static String getCurrentAppScheme(String host) {
    return getCurrentAppScheme() + "://" + host;
  }

  /**
   * Scheme used for detecting "play as guest" Urls
   * 
   * @return "greeapp123://enter"
   */
  public static String getEnterScheme() {
    return getCurrentAppScheme(ENTER_HOST);
  }

  /**
   * Scheme used for detecting "join now" urls
   * @return "greeapp123://sso-require"
   */
  public static String getSsoRequireScheme() {
    return getCurrentAppScheme(SSO_REQUIRE);
  }

  /**
   * This scheme is used for detecting if the SDK needs to start the OAuth process
   * (getting a request token, authorizing then getting an access token).
   * @return "greeapp123://start-authorization"
   */
  public static String getStartAuthorizationScheme() {
    return getCurrentAppScheme(START_AUTHORIZATION_HOST);
  }

  /**
   * Scheme used for detecting when the SDK needs to request an access token.
   * @return "greeapp123://get-accesstoken"
   */
  public static String getAccessTokenScheme() {
    return getCurrentAppScheme(GET_ACCESS_TOKEN_HOST);
  }

  /**
   * Scheme used for detecting when the SDK needs to choose account.
   * @return "greeapp123://choose-account"
   */
  public static String getChooseAccount() {
    return getCurrentAppScheme(CHOOSE_ACCOUNT);
  }

  /**
   * Scheme for detecting if a grade user upgrade is being requested
   * @return "greeapp123://upgrade"
   */
  public static String getUpgradeScheme() {
    return getCurrentAppScheme(UPGRADE_HOST);
  }

  /**
   * Scheme for detecting if a user wants to logout
   * @return "greeapp123://logout"
   */
  public static String getLogoutScheme() {
    return getCurrentAppScheme(LOGOUT_HOST);
  }

  /**
   * Scheme for detecting if a user wants to re open the setupActivity
   * @return "greeapp123://reopen"
   */
  public static String getReopenScheme() {
    return getCurrentAppScheme(REOPEN_HOST);
  }

  /**
   * Scheme used for detecting if a payment transaction was completed on the GREE server
   * @return "greeapp123://complete-transaction"
   */
  public static String getCompleteTransactionScheme() {
    return getCurrentAppScheme(COMPLETE_TRANSACTION_HOST);
  }

  /**
   * Scheme used for detecting if a user wants to show the deposit screen
   * (this module is held outside of ggpsdk)
   * @return "gree-wallet-deposit-launch"
   */
  public static String getWalletDepositHost() {
    return WALLET_DEPOSIT_HOST;
  }

  /**
   * Scheme used for detecting if a user wants to show the deposit history screen
   * (this module is held outside of ggpsdk)
   * @return "gree-wallet-deposit-history-launch"
   */
  public static String getWalletDepositHistoryHost() {
    return WALLET_DEPOSIT_HISTORY_HOST;
  }

  /**
   * Scheme used for detecting if a uset wants to show the Gree reward screen
   * (this module is held outisde of ggpsdk)
   * @return "gree-reward-offerwall"
   */
  public static String getRewardOfferWallScheme() {
    return getCurrentAppScheme(GREEREWARD_OFFERWALL_HOST);
  }

  /**
   * Test if the given url will redirect to the deposit screen.
   * @param url the url the user has clicked on
   * @return true if it is a deposit url
   */
  public static boolean isBillingScheme(String url) {
    if (url != null && url.startsWith(GREEAPP_SCHEME)) {
      Uri uri = Uri.parse(url);
      if (uri != null
          && (WALLET_DEPOSIT_HOST.equals(uri.getHost()) || WALLET_DEPOSIT_HISTORY_HOST.equals(uri.getHost()))) {
        return true;
      }
    }
    return false;
  }
}
