/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.widget;

import org.json.JSONException;
import org.json.JSONObject;

import net.gree.asdk.R;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.storage.JSONStorage;
import android.annotation.TargetApi;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

/**
 * RemoteView service for ApiLevel 11 or more
 * 
 * @author GREE, Inc.
 * 
 */
@TargetApi(11)
public class WidgetRemoteViewService extends RemoteViewsService {
  private static final String TAG = "WidgetRemoteViewService";
  private WidgetRemoteViewsFactory mFactory;
  private static String[] mFilter;
  private static RemoteViews mSaveHeader;

  @Override
  public void onCreate() {
    super.onCreate();
    GLog.i(TAG, "[CREATE SERVICE]");
  }

  @Override
  public void onStart(Intent intent, int startId) {
    GLog.i(TAG, "[START SERVICE]");
    if (intent != null && intent.hasExtra(WidgetProvider.EXTRA_WIDGET_FILTER)) {
      String[] filter = intent.getStringArrayExtra(WidgetProvider.EXTRA_WIDGET_FILTER);
      if (mFactory == null) {
        mFactory = new WidgetRemoteViewsFactory(this.getApplicationContext(), intent, mFilter);
      }
      mFactory.setFilter(filter);
      Intent i = new Intent();
      i.setAction(WidgetProvider.WIDGET_UPDATE_ACTION);
      if (mSaveHeader != null) {
        GLog.i(TAG, "[RESTORE HEADER]");
        i.putExtra(WidgetProvider.EXTRA_CUSTOM_HEADER_OBJECT, mSaveHeader);
      }
      sendBroadcast(i);
    }
    if (intent.hasExtra(WidgetProvider.EXTRA_CUSTOM_HEADER_OBJECT)) {
      GLog.i(TAG, "[SAVE HEADERVIEW]");
      mSaveHeader = intent.getParcelableExtra(WidgetProvider.EXTRA_CUSTOM_HEADER_OBJECT);
    }
  }

  @Override
  public RemoteViewsFactory onGetViewFactory(Intent intent) {
    if (mFactory == null) {
      mFactory = new WidgetRemoteViewsFactory(this.getApplicationContext(), intent, mFilter);
    }
    return mFactory;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    GLog.i(TAG, "[DESTROY SERVICE]");
  }
}


/**
 * RemoteView factory
 * 
 * @author GREE, Inc.
 * 
 */
class WidgetRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
  private static final String TAG = "WidgetRemoteViewsFactory";
  private Context mContext;
  private JSONStorage mWidgetData;
  private Cursor mCursor;
  private int mAppWidgetId;
  private int mCount = 0;
  private String[] mFilter;

  WidgetRemoteViewsFactory(Context context, Intent intent, String[] filter) {
    mContext = context;
    mAppWidgetId =
        intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID);
    mWidgetData = Injector.getInstance(JSONStorage.class);
    mWidgetData.setSubFilterList("widget_data");
    mCursor =
        mWidgetData.getDataFilterCursor(JSONStorage.COLUM_FILTER + "=?",
            new String[] {"widget_data"});
    mCount = mCursor.getCount();
    mFilter = new String[] {};
  }

  @Override
  public int getCount() {
    GLog.i(TAG, "getCount " + mCount);
    return mCount;
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  @Override
  public RemoteViews getLoadingView() {
    return null;
  }

  @TargetApi(11)
  @Override
  public RemoteViews getViewAt(int position) {
    String str = null;
    RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.gree_widget_line_item);
    synchronized (mCursor) {
      if (mCursor.moveToPosition(position)) {
        try {
          str = mCursor.getString(4);
          JSONObject json = new JSONObject(str);
          str = json.get("text").toString();
        } catch (JSONException e) {
          e.printStackTrace();
        }
        byte[] image = mCursor.getBlob(3);
        rv.setTextViewText(R.id.gree_line_text, str);
        Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
        rv.setImageViewBitmap(R.id.gree_line_image, bmp);
      }
    }
    Intent i = new Intent();
    rv.setOnClickFillInIntent(R.id.gree_line_text, i);
    return rv;
  }

  @Override
  public int getViewTypeCount() {
    return 0;
  }

  @Override
  public boolean hasStableIds() {
    return false;
  }

  @Override
  public void onCreate() {}

  @Override
  public void onDataSetChanged() {
    synchronized (mCursor) {
      if (mFilter.length > 0) {
        StringBuilder str = new StringBuilder();
        str.append("_filtersub=?");
        for (int i = 1; i < mFilter.length; i++) {
          str.append(" OR _filtersub=?");
        }
        mCursor = mWidgetData.getDataFilterCursor(str.toString(), mFilter);
      } else {
        mCursor =
            mWidgetData.getDataFilterCursor(JSONStorage.COLUM_FILTER + "=?",
                new String[] {"widget_data"});
      }
      mCount = mCursor.getCount();
    }
  }

  @Override
  public void onDestroy() {
    if (mCursor != null) {
      mCursor.close();
    }
  }

  public void setFilter(String[] filter) {
    synchronized (mFilter) {
      mFilter = filter;
    }
  }
}
