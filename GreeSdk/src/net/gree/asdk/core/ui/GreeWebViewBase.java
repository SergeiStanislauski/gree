/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.ui;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.storage.LocalStorage;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Util;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/*
 * This class is base of WebView.
 * @author GREE, Inc.
 */
public class GreeWebViewBase extends WebView {
  protected LocalStorage mLocalStorage;
  private static final String TAG = "GreeWebViewBase";
  private boolean mIsPaused = false;
  private static final int APP_MAX_SIZE = 1024 * 1024 * 8;
  private Map<String, String> mHeaderMap = new TreeMap<String, String>();
  private HashMap<String, Object> mJavascriptInterfaces = new HashMap<String, Object>();

  public GreeWebViewBase(Context context) {
    super(context);
  }

  public GreeWebViewBase(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public GreeWebViewBase(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @SuppressLint(value = {"SetJavaScriptEnabled"})
  public void setUp() {
    WebSettings webSettings = getSettings();
    webSettings.setJavaScriptEnabled(true);
    webSettings.setBuiltInZoomControls(true);
    webSettings.setPluginsEnabled(true);
    webSettings.setSupportZoom(false);
    webSettings.setAppCacheEnabled(true);
    webSettings.setAppCachePath(getContext().getDir("appcache", Context.MODE_PRIVATE).getPath());
    webSettings.setAppCacheMaxSize(APP_MAX_SIZE);
    webSettings.setDomStorageEnabled(true);
    webSettings.setGeolocationEnabled(true);

    setWebViewClient(new WebViewClient());
    setWebChromeClient(new WebChromeClient());
    setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);
    mLocalStorage = Injector.getInstance(LocalStorage.class);
    mHeaderMap.put("Accept-Language", Locale.getDefault().toString());
  }

  public void cleanUp() {
    setWebViewClient(null);
    setWebChromeClient(null);
    mLocalStorage = null;
  }

  public LocalStorage getLocalStorage() {
    return mLocalStorage;
  }

  /**
   * restore javascript interface
   */
  public void restoreJavascriptInterface() {
    for (Map.Entry<String, Object> entry : mJavascriptInterfaces.entrySet()) {
      super.addJavascriptInterface(entry.getValue(), entry.getKey());
    }
  }

  @Override
  public void addJavascriptInterface(Object obj, String interfaceName) {
    super.addJavascriptInterface(obj, interfaceName);
    mJavascriptInterfaces.put(interfaceName, obj);
  }



  @Override
  public void stopLoading() {
    try {
      super.stopLoading();
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  @TargetApi(8)
  @Override
  public void loadUrl(String url) {
    if (TextUtils.isEmpty(url)) {
      GLog.d(TAG, "url is empty");
      return;
    }

    try {
      if (android.os.Build.VERSION.SDK_INT >= 8) {
        super.loadUrl(url, mHeaderMap);
      } else {
        super.loadUrl(url);
      }
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  @Override
  public void reload() {
    try {
      super.reload();
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  @Override
  public void clearHistory() {
    try {
      super.clearHistory();
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  @Override
  public void freeMemory() {
    try {
      super.freeMemory();
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }

  @Override
  public void onWindowVisibilityChanged(int visibility) {
    super.onWindowVisibilityChanged(visibility);

    if (CoreData.containsKey(InternalSettings.EnableWebViewPauseTimers)
        && CoreData.get(InternalSettings.EnableWebViewPauseTimers).equals("true")) {

      // http://code.google.com/p/android/issues/detail?id=13991
      // At less than ICS, pauseTimers does not stop javascript timers, as expected.
      if (visibility == View.GONE) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && Util.isForeground()) {
          return;
        }
        try {
          this.pauseTimers();
          mIsPaused = true;
          GLog.d(TAG, "pauseTimers");
        } catch (Exception e) {
          GLog.printStackTrace(TAG, e);
        }
      } else if (visibility == View.VISIBLE) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && mIsPaused == false) {
          return;
        }
        try {
          this.resumeTimers();
          mIsPaused = false;
          GLog.d(TAG, "resumeTimers");
        } catch (Exception e) {
          GLog.printStackTrace(TAG, e);
        }
      }
    }
  }

  @Override
  protected void onDraw(Canvas canvas) {
    if (CoreData.containsKey(InternalSettings.EnableWebViewPauseTimers)
        && CoreData.get(InternalSettings.EnableWebViewPauseTimers).equals("true")) {
      if (mIsPaused == false) {
        super.onDraw(canvas);
      }
    } else {
      super.onDraw(canvas);
    }
  }

  @Override
  public void destroy() {
    if (CoreData.containsKey(InternalSettings.EnableWebViewPauseTimers)
        && CoreData.get(InternalSettings.EnableWebViewPauseTimers).equals("true")) {
      if (mIsPaused == true) {
        this.resumeTimers();
        mIsPaused = false;
      }
    }
    try {
      super.destroy();
    } catch (Exception e) {
      GLog.printStackTrace(TAG, e);
    }
  }
}
