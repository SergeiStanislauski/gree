/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.connect;

import java.util.HashMap;
import java.util.Map;

import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.GreeRequest;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.request.SnsResponseHandler;
import net.gree.asdk.core.request.StringConverter;
import net.gree.asdk.core.request.helper.MethodHelper;

/**
 * Mood API.
 * You can get a mood information which is posted by user by using this class.
 * 'Mood' is state of feeling of GREE user.
 */
public class Mood {
  private String mApiUrl;
  private GreeRequest mGreeRequest;

  private String mRequestedUrl;
  private int mRequestedMethod;

  public String getApiUrl() {
    return mApiUrl;
  }

  public String getRequestedUrl() {
    return mRequestedUrl;
  }

  public int getRequestedMethod() {
    return mRequestedMethod;
  }

  /**
   * Create the Mood API.
   * @param url Connect API URL information.
   */
  public Mood(Url url) {
    mApiUrl = url.getConnectUrl() + "/api/rest/mood/entries/";
  }

  /**
   * Get the mood information.
   * The information has the following data:
   *   mood id
   *   mood text
   *   number of likes
   *   number of comments
   *   attached url of image
   * @param userId user id which own the mood.
   * @param moodId mood id.
   * @param listener listener which is receiving the result of api call.
   */
  public void getInfo(String userId, String moodId, OnResponseCallback<String> listener) {
    StringBuilder sb = new StringBuilder(mApiUrl);
    sb.append(userId).append('/').append(moodId);
    request(sb.toString(), BaseClient.METHOD_GET, listener);
  }

  private void request(String url, int method,  OnResponseCallback<String> listener) {
    Map<String, String> headers = new HashMap<String, String>();
    headers.put("Content-Type", "application/json");

    StringConverter converter = new StringConverter();
    SnsResponseHandler handler = new SnsResponseHandler(listener);

    if (mGreeRequest == null) {
      new GreeRequest(url, MethodHelper.parseInt(method)).header(headers).sync(true)
      .request(converter, handler);
    } else {
      mGreeRequest.entity(null).header(headers).sync(true).request(converter, handler);
    }

    mRequestedUrl = url;
    mRequestedMethod = method;
  }
}
