/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

import android.content.Context;
import android.os.Build;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.Injector;
import net.gree.vendor.com.google.gson.Gson;
import net.gree.vendor.com.google.gson.reflect.TypeToken;

/**
 * Base class for HTTP client implementations.
 * 
 * @author GREE, Inc.
 * 
 */
public abstract class BaseClient<T> {
  public static boolean isDebug = false;
  public static boolean isVerbose = false;

  public static final String DisabledMessage = "Network requests disabled by application.";
  public static final String GRADE0_ERROR_MESSAGE = "User is not logged in";


  public static final int METHOD_GET = 0;
  public static final int METHOD_POST = 1;
  public static final int METHOD_PUT = 2;
  public static final int METHOD_DELETE = 3;
  public static final String[] methods = {"GET", "POST", "PUT", "DELETE"};

  protected static final int CONNECTION_TIMEOUT = 30 * 1000; // msec

  protected OnResponseCallback<T> mListener;

  static String sUserAgent = null;
  protected String mUserAgent = "GREEApp/" + Core.getSdkVersion() + " (" + Build.MODEL
      + "; Android " + Build.VERSION.RELEASE + ")";

  public BaseClient() {
    if (sUserAgent != null)
      mUserAgent = sUserAgent;
    isDebug = Core.getInstance().debugging();
  }

  static Context context = null;

  public static void init(Context context) {}

  public static final Map<String, Integer> cmd = new TreeMap<String, Integer>() {
    private static final long serialVersionUID = 1L;
    {
      put("GET", 0);
      put("POST", 1);
      put("PUT", 2);
      put("DELETE", 3);
      put("get", 0);
      put("post", 1);
      put("put", 2);
      put("delete", 3);
    }
  };


  public static String toQueryString(Map<String, Object> params) {
    if (params == null)
      return "";
    String queryParam = "";
    if (0 < params.size()) {
      for (Map.Entry<String, Object> entry : params.entrySet()) {
        queryParam += entry.getKey() + "=" + entry.getValue() + "&";
      }
      queryParam = queryParam.substring(0, queryParam.length() - 1); // trim last &
    }
    return queryParam;
  }


  public static String toEntityJson(Map<String, Object> params) {
    Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
    String ret = null;
    if (params == null)
      return null;
    if (0 < params.size()) {
      ret = Injector.getInstance(Gson.class).toJson(params, mapType);
    }
    return ret;
  }
}
