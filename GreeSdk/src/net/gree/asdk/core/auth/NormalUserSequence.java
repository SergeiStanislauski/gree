/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.auth.AuthorizerCore.OnOAuthResponseListener;
import net.gree.asdk.core.auth.OAuthUtil.OnCloseOAuthAlertListener;
import net.gree.asdk.core.auth.UserInfoUpdater.AgreementListener;
import net.gree.asdk.core.auth.sso.SingleSignOn;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.DeviceInfo;
import net.gree.asdk.core.util.Scheme;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;
import net.gree.oauth.signpost.exception.OAuthException;

public class NormalUserSequence extends AuthorizeSequenceBase {
  // Tag for debug
  protected static final String TAG = NormalUserSequence.class.getSimpleName();
  static final String TARGET_APPS = "apps";
  // variables derived from previous login
  protected PerformanceData mPerformData;
  private IPerformanceManager mManager;
  private String mUserKey = null;
  JSONObject mAppList = null;

  /**
   * Default message handler - for communication primarily when requesting authorization
   */
  Handler mHandler = new Handler() {
    @Override
    public void handleMessage(Message message) {
      if (!mAuthorizer.hasOAuthAccessToken() && message.what == SetupActivity.SHOULD_LOGIN) {
        mAuthorizer.retrieveRequestToken(null, new OnOAuthResponseListener<String>() {
          public void onSuccess(String response) {
            // request(response);
            mAuthorizer.clearRequestTokenParams();
          }

          public void onFailure(OAuthException e) {
            OAuthUtil.handleException(e, mContext, null, null, new OnCloseOAuthAlertListener() {
              public void onClose() {
                mListener.onError();
              }
            });
            mAuthorizer.clearRequestTokenParams();
          }
        });
      } else {
        mListener.onError();
      }
    }
  };

  /**
   * Default constructor
   * 
   * @param context calling context
   * @param serviceCode
   */
  public NormalUserSequence(Context context, String serviceCode) {
    super(context, serviceCode);
    mManager = Injector.getInstance(IPerformanceManager.class);
  }


  /**
   * Spawns a separate task and runs the login for a normal user. This API will NOT show any popup
   * dialogs. It will run the external authorization in a web browser.
   */
  @Override
  protected void run() {
    // if we are already authorized aka logged in
    if (mAuthorizer.hasOAuthAccessToken()) {
      AgreementListener agreementListener = new AgreementListener() {
        public void onNeedAgreement(String url) {
          // may wish to show agreement dialog here
        }
      };
      // create a new initalizer
      new UserInfoUpdater(mContext).listener(agreementListener).request();
      // call a successful login
      mListener.onSuccess();
      return;
    }

    // otherwise begin the login process
    if (!mHasUuid) {
      DeviceInfo.updateUuid(new OnResponseCallback<String>() {
        public void onSuccess(int responseCode, HeaderIterator headers, String response) {
          mUserKey = AuthorizeContext.getUserKey();
          startOAuth();
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          mListener.onError();
        }
      });
      return;
    }
    mUserKey = AuthorizeContext.getUserKey();
    startOAuth();
  }

  /**
   * Begins the OAuth process
   */
  private void startOAuth() {
    // sets the flow index for our performance data management, relates to connectvity
    mPerformData = mManager.createData(PerformanceFlowIndex.LOGIN);

    // checking the CoreData settings
    if (Url.isSandbox() ) {
      startSso();
      return;
    }
    
    // if we are receiving this request from a Single Sign on call, try the SSO feature
    if (trySso()) { return; }
    // Create the GET string with the UUID
    startSso();
    
  }

  /**
   * Attempts the single sign on option for the login
   * @return true if there are multiple devices, false otherwise
   */
  private boolean trySso() {
    if (!SingleSignOn.isAvailableSsoRequest(mContext, Core.getInstance().getGreeAppPackageName())) {
      return false;
    }

    mAuthorizer.retrieveRequestToken(null, new OnOAuthResponseListener<String>() {
      public void onSuccess(String response) {
        // Attempts to begin SSO for the given URL, handles if the event is coming for an SSO attempt
        SetupActivity.requestAuthorization(mContext, response, Core.getInstance()
            .getGreeAppPackageName(), mListener, mHandler);
      }

      public void onFailure(OAuthException e) {
        mListener.onError();
      }
    });
    return true;
  }

  /**
   * Generates the Normal User login string
   * @return a String of the format that calls for a login of Target 2, 3
   */
  private static String getEnterAsNormalUserString() {
    // Build the auth String
    StringBuilder auth = new StringBuilder();

    auth.append(Scheme.getStartAuthorizationScheme());
    auth.append("?target=");
    auth.append(TARGET_APPS);
    return auth.toString();
  }
  
  /**
   * Handles entry as an SSO option. Creates the list of Gree apps installed on the phone 
   */
  private void startSso() {
    OAuthUtil.requestAppList(mUserKey, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        try {
          GLog.d(TAG, "app list = " + response);
          mAppList = new JSONObject(response);
          startAuthorization(getEnterAsNormalUserString());
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        mListener.onError();
      }
    });
    
  }
  
  /**
   * Begins Authorization for the user. Creates our first web request to get 
   * @param url
   */
  private void startAuthorization(String url) {
    final Uri uri = Uri.parse(url);
    
    if (uri == null) {
      GLog.w(TAG, "Illegal scheme for start-authorization.");
      mListener.onError();
    }

    // sets the info in the Doa
    NormalAuthDao authDoa = (NormalAuthDao) Injector.getInstance(OAuthNormalDao.class);
    authDoa.initalize(mAuthorizer, mContext, mListener);
    // tells our Activity to redirect this
    authDoa.setRedircetToSetupActivity(false);
    
    // create a Authorizer request, handled by our AuthorizerCore instance
    mAuthorizer.retrieveRequestToken(null, new OnOAuthResponseListener<String>() {
      public void onSuccess(String response) {
        try{
          // Array list for the json objects
          JSONArray applist = null;
          // adding the response string to a URI for easier parsing
          String target = uri.getQueryParameter("target");
          // sends the request for an 
          if (TARGET_APPS.equals(target)) {
            GLog.d(TAG, "Request Verifier in Native apps url=" + response);
            applist = mAppList.getJSONArray("entry");
            // request SSO
            SingleSignOn.searchAndRequestAuthorization(mContext, response, applist, null, mHandler);
            return;
          }
          // if it isn't point to the app, we redirect it to the broswer
          else{
            // if this breaks
            if(!Util.startBrowser(mContext, response)){
              mListener.onError();
            }
          }
        }
        catch(JSONException e){
          GLog.printStackTrace(TAG, e);
          mListener.onCancel();
        }
      }
      public void onFailure(OAuthException e) {
        mListener.onError();
      }

    });
  }  

}
