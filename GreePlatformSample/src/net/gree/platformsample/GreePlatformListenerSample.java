/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample;

import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.GreeUser;

/**
 * This is a sample class to demonstrate the use of {@link GreePlatformListener} and {@link TaskEventListener} 
 */
public class GreePlatformListenerSample extends GreePlatformListener {
  //turn mNotify on to get toast notifications
  boolean mNotify = false;
  Context mContext;
  
  TaskEventListener mTaskEventListener = new TaskEventListener() {
    @Override
    public void onEvent(Map<String, Object> params) {

      //You can get the type of class by accessing the integer in GreePlatformListener.KEY_CLASS_TYPE
      String classname = "";
      int classType = getClassType(params);
      switch (classType) {
        case CLASS_UNDEFINED:
          classname = "GreePlatform";
          break;
        case CLASS_DASHBOARD_ACTIVITY:
          classname = "Dashboard";
          break;
        case CLASS_NOTIFICATION_BOARD_ACTIVITY:
          classname = "NotificationBoard";
          break;
        case CLASS_INVITE_DIALOG:
          classname = "Invite dialog";
          break;
        case CLASS_REQUEST_DIALOG:
          classname = "Request dialog";
          break;
        case CLASS_SHARE_DIALOG:
          classname = "Share dialog";
          break;
        case CLASS_PAYMENT_DIALOG:
          classname = "Payment dialog";
          break;
        case CLASS_AUTHORIZE_DIALOG:
          classname = "Authorize dialog";
          break;
        default:
          classname = "Unknown window";
      }

      //You can get an instance of the activity or dialog by accessing the Object in GreePlatformListener.KEY_INSTANCE
      String objectAsString = "";
      if ((params.containsKey(KEY_INSTANCE)) && (null != params.get(KEY_INSTANCE))) {
    	  objectAsString = "" + params.get(KEY_INSTANCE).getClass().getName();
      }

      //You can get the event type by accessing the integer in GreePlatformListener.KEY_EVENT_TYPE 
      int eventType = getEventType(params);
      String message = "";
      switch (eventType) {
        case EVENT_OPEN:
            message = classname + " : open " + objectAsString;
          break;
        case EVENT_CLOSE:
          message = classname + " : close " + objectAsString;

          //You can access some results by accessing the JsonObject in GreePlatformListener.KEY_RESULTS
          if (params.containsKey(GreePlatformListener.KEY_RESULTS)) {
            Object result = params.get(GreePlatformListener.KEY_RESULTS);
            if (result != null) {
              if (result instanceof JSONObject) {
                message = message + " " + ((JSONObject) result).toString();
              } else {
                if (result instanceof String) {
                  message = message + " " + result; 
                }
              }
            }
          }
          break;
        case EVENT_CANCEL:
          message = classname + " : cancel " + objectAsString;
          break;
        case EVENT_ERROR:
          message = classname + " : error " + objectAsString;
          break;
        case EVENT_DESTROYED:
          message = classname + " : destroyed " + objectAsString;
          break;
        case EVENT_LOGIN:
          message = classname + " : login " + objectAsString;
          break;
        case EVENT_LOGOUT:
          message = classname + " : logout " + objectAsString;
          break;
        case EVENT_USERINFO_RETRIEVED:
          message = classname + " : User info retrieved " + objectAsString;
          //You can access some results by accessing the JsonObject in GreePlatformListener.KEY_RESULTS
          if (params.containsKey(GreePlatformListener.KEY_RESULTS)) {
            Object result = params.get(GreePlatformListener.KEY_RESULTS);
            if (result != null) {
              if (result instanceof GreeUser) {
                message = message + " user:" + ((GreeUser) result).getNickname();
              }
            }
          }
          break;
        default:
          return;
      }

      if (mNotify) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
      }
      Log.d("GreePlatformListenerSample", message);
    }
  };

  public GreePlatformListenerSample(Context context) {
    mContext = context;
  }
  
}
