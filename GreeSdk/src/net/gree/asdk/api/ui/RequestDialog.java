/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api.ui;

import java.util.TreeMap;

import android.content.Context;
import android.os.Handler;
import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.ui.AbsServiceDialog;
import net.gree.asdk.core.util.Url;

/**
 * A Class which represents the Dialog for sending Request Services.
 *
 * <p>
 * The result of sending a request notifies a Handler.<br />
 * When a request is successfully sent, the request ID and the result of the request are sent as JSON data to the sender.
 * </p>
 *
 * Sample code:
 * <code><pre>
 * Handler handler = new Handler() {
 *   public void handleMessage(Message message) {
 *     switch (message.what) {
 *       case RequestDialog.OPENED:
 *         Log.d(TAG, "RequestDialog opened");
 *         break;
 *       case RequestDialog.CLOSED:
 *         Log.d(TAG, "RequestDialog closed");
 *         String returnStr = (String) message.obj;
 *         break;
 *     }
 *   }
 * };
 *
 * RequestDialog dialog = new RequestDialog(Activity.this);
 * dialog.setHandler(handler);
 *
 * TreeMap&lt;String,Object&gt; map = new TreeMap&lt;String,Object&gt;();
 * map.put("title", "Request Title");
 * map.put("body", "Send to Request");
 * dialog.setParams(map);
 * dialog.show();
 * </pre></code>
 * @author GREE, Inc.
 */
public final class RequestDialog extends AbsServiceDialog {
  private static final String TAG = "RequestDialog";

/**
 * Handler event type which is notified when request dialog is started/opened.
 */
  public static final int OPENED = 1;
/**
 * <p>
 * Handler event type which notified when request dialog is finished/closed.<br>
 * This event has the additional parameter "message.obj" as a request results in the handler's message.
 * </p>
 */
  public static final int CLOSED = 2;

/**
 * Max number which you can send request message to.
 */
  public static final int USER_ID_LIST_MAX_NUM = 15;

  private static final String SERVICE_CODE = "RQ0";

  /* Endpoint for Request Service Dialog */
  private static final String ENDPOINT = Url.getRequestDialogContentUrl();

  /**
   * Default constructor.
   * @param context The Context the Dialog runs in.
   */
  public RequestDialog(Context context) {
    super(context);

    //set the dialog type for TaskEventListeners
    mClassType = GreePlatformListener.CLASS_REQUEST_DIALOG;

    setRequestType(TYPE_REQUEST_METHOD_POST);

    setIsClearHistory(true);
    setTitleType(TITLE_TYPE_STRING);
    setTitle(GreePlatform.getRString(RR.string("gree_dialog_title_request")));
  }

  /**
   * Sets the Event Handler.
   * @param handler event handler.
   */
  @Override
  public void setHandler(Handler handler) {
    super.setHandler(handler);
  }

 
  /**
   * Shows the dialog.
   */
  @Override
  public final void show() {
    mPerformData = mManager.createData(PerformanceFlowIndex.REQUEST);
    super.show();
    GLog.d(TAG, "show()");
  }

  @Override
  protected void onShow() {
    setPostData(buildPostData());
  }

/**
 * Sets the optional parameters.
 * Parameter is passed as TreeMap object.
 * @param params TreeMap object which having the following keys as elements.
 * @param title request title. It is required param.
 * @param body a message sent with request. It is required param.
 * @param list_type Select user type which you want to send. ('all', 'joined', 'not_joined', 'specified'(used with to_user_id))
 * @param to_user_id Array as user id string which you want to send request.
 * @param expire_time unix timestamp or UTC time string.
 * @param attrs Return parameter when a user click the link in request. It is set as JSONObject. - {"param1":"value1","param2":"value2"}. The key and value of parameter accept only ASCII. The key should not be greater than 16 characters. The value should not be greater than 32 characters.
 */
  public void setParams(TreeMap<String,Object>  params) {
    super.setParams(params);
  }

  @Override protected int getOpenedEvent() { return OPENED; }

  @Override protected int getClosedEvent() { return CLOSED; }

  @Override
  final protected String getEndPoint() { return ENDPOINT; }

  @Override
  protected String getServiceCode() { return SERVICE_CODE; }
}
