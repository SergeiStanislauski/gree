/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;


import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.auth.AuthorizerCore.OnOAuthResponseListener;
import net.gree.asdk.core.auth.OAuthUtil.OnCloseOAuthAlertListener;
import net.gree.asdk.core.auth.SetupActivity.SetupDialog;
import net.gree.asdk.core.auth.UserInfoUpdater.AgreementListener;
import net.gree.asdk.core.auth.sso.SingleSignOn;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.GreeRequest;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.request.ResponseHandler;
import net.gree.asdk.core.request.ResponseHolder;
import net.gree.asdk.core.request.StringConverter;
import net.gree.asdk.core.request.Constants.OAUTH_TYPE;
import net.gree.asdk.core.request.helper.MethodHelper;
import net.gree.asdk.core.storage.CookieStorage;
import net.gree.asdk.core.util.DeviceInfo;
import net.gree.asdk.core.util.Scheme;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;
import net.gree.oauth.signpost.exception.OAuthException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

public class LiteUserSequence extends AuthorizeSequenceBase {
  private static final String TAG = LiteUserSequence.class.getSimpleName();

  protected PerformanceData mPerformData;
  private IPerformanceManager mManager;
  private String mUserKey = null;
  JSONObject mAppList = null;

  Handler mHandler = new Handler() {
    @Override
    public void handleMessage(Message message) {
      if (!mAuthorizer.hasOAuthAccessToken() && message.what == SetupActivity.SHOULD_LOGIN) {
        mAuthorizer.retrieveRequestToken(null, new OnOAuthResponseListener<String>() {
          public void onSuccess(String response) {
            request(response);
            mAuthorizer.clearRequestTokenParams();
          }

          public void onFailure(OAuthException e) {
            OAuthUtil.handleException(e, mContext, null, null, new OnCloseOAuthAlertListener() {
              public void onClose() {
                mListener.onError();
              }
            });
            mAuthorizer.clearRequestTokenParams();
          }
        });
      } else {
        mListener.onError();
      }
    }
  };

  public LiteUserSequence(Context context, String serviceCode) {
    super(context, serviceCode);
    mManager = Injector.getInstance(IPerformanceManager.class);
  }

  @Override
  public void run() {
    if (mAuthorizer.hasOAuthAccessToken()) {
      AgreementListener agreementListener = new AgreementListener() {
        public void onNeedAgreement(String url) {
//          new AgreementDialog(context, url).show();
        }
      };
      new UserInfoUpdater(mContext).listener(agreementListener).request();
      mListener.onSuccess();
      return;
    }

    // This check should be moved to super class.
    if (!mHasUuid) {
      DeviceInfo.updateUuid(new OnResponseCallback<String>() {
        public void onSuccess(int responseCode, HeaderIterator headers, String response) {
          mUserKey = AuthorizeContext.getUserKey();
          startOAuth();
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          mListener.onError();
        }
      });
      return;
    }
    mUserKey = AuthorizeContext.getUserKey();
    startOAuth();
  }

  private void startOAuth() {
    mPerformData = mManager.createData(PerformanceFlowIndex.LOGIN);

    // checking the CoreData settings
    if (Url.isSandbox()) {
      startAuthorization(getStartAuthorizationSchemeForSso());
      return;
    }

    if (trySso()) {
      return;
    }
    // Create the GET string with the UUID
    request(getEnterAsLiteUserString());
  }

  /**
   * Builds the enter String necessary for the application to enter upon first call
   * @return the URL to request entry as a lite user, plus the UUID
   */
  private String getEnterAsLiteUserString() {
    StringBuilder enterStr = new StringBuilder();
    enterStr.append(Url.getEnterAsLiteUserWithoutUI());
    enterStr.append("&tz_offset=");
    enterStr.append(Util.timezoneOffsetMinutes());

    return enterStr.toString();
  }

  /**
   * Generates scheme to start authorization by SSO
   * @return a String of the format that calls for a login of Target 2, 3
   */
  private static String getStartAuthorizationSchemeForSso() {
    // Build the auth String
    StringBuilder auth = new StringBuilder();

    auth.append(Scheme.getStartAuthorizationScheme());
    auth.append("?target=");
    auth.append(SetupDialog.TARGET_BROWSER);
    return auth.toString();
  }

  private boolean trySso() {
    if (!SingleSignOn.isAvailableSsoRequest(mContext, Core.getInstance().getGreeAppPackageName())) {
      return false;
    }

    mAuthorizer.retrieveRequestToken(null, new OnOAuthResponseListener<String>() {
      public void onSuccess(String response) {
        SetupActivity.requestAuthorization(mContext, response, Core.getInstance().getGreeAppPackageName(), mListener, mHandler);
      }
      public void onFailure(OAuthException e) {
        mListener.onError();
      }
    });
    return true;
  }

  private void request(String url) {
    StringConverter converter = new StringConverter();
    RedirectHandler<String> handler = new RedirectHandler<String>(new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, "Response success, code: " + responseCode + " with response: " + response);
        // This case(http status 200) is an error because all the request by HttpClient should be
        // handled by onRedirect() in the sequence for direct login.
        mListener.onError();
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, "Response failure, code: " + responseCode + " with response: " + response);
        mListener.onError();
      }
    });
    url = AuthorizeContext.appendQueryParameter(url, mUserKey);
    new GreeRequest(url, MethodHelper.parseInt(BaseClient.METHOD_GET))
        .oauth(OAUTH_TYPE._NONE).sync(false).request(converter, handler);

  }

  private class ExtendedHolder<T> extends ResponseHolder<T> {
    String mLocation;

    public ExtendedHolder(ResponseHolder<T> holder) {
      super(holder);
      mLocation = getLocation(getHeaderIterator());
    }

    int getStatusCode() {
      return mStatusCode;
    }

    /**
     * Takes the current holder object and pulls out the location string from the header.
     * 
     * @param headers response header
     * @return a string representation of the location value
     */
    private String getLocation(HeaderIterator headers) {
      while (headers.hasNext()) {
        Header header = headers.nextHeader();
        if ("Location".equals(header.getName())) {
          // store the header locations
          String location = header.getValue();
          // in case there are more then one location being returned??
          String[] params = location.split(";");
          if (0 < params.length) {
            return params[0];
          }
        }
      }
      return null;
    }
  }

  private class RedirectHandler<T> extends ResponseHandler<T> {
    // implicit super constructor
    public RedirectHandler(OnResponseCallback<T> listener) {
      super(listener);
    }

    @Override
    public void onResponse(ResponseHolder<T> holder) {
      if (holder == null) {
        String reason = "holder is null";
        onFailure(HttpStatus.SC_BAD_REQUEST, null, null, reason);
        return;
      }
      if (!(holder instanceof ExtendedHolder<?>)) {
        // handling redirect
        ExtendedHolder<T> exHolder = new ExtendedHolder<T>(holder);
        // stores the cookies and procedes
        CookieStorage.setCookie(exHolder.getHeaderIterator()); // set session to storage
        switch (exHolder.getStatusCode()) {
          case HttpStatus.SC_MOVED_TEMPORARILY:
          case HttpStatus.SC_MOVED_PERMANENTLY:
          case HttpStatus.SC_SEE_OTHER:
          case HttpStatus.SC_TEMPORARY_REDIRECT:
            if(exHolder.mLocation!=null){
              onRedirect(exHolder.mLocation);
            }
            else{
              String reason = "Response had no location for redirection.";
              onFailure(HttpStatus.SC_PRECONDITION_FAILED, null, null, reason);
            }
            return;
        }
      }
      super.onResponse(holder);
    }
  }

  private void onRedirect(String location) {
    if (location.startsWith(Scheme.getStartAuthorizationScheme())) {
      startAuthorization(location);
    } else if (location.startsWith(Scheme.getChooseAccount())) {
      SetupActivity.setup(mContext, Url.getEnterAsLiteUser(), mListener);
    } else if (location.startsWith(Scheme.getAccessTokenScheme())) {
      retrieveAccessToken(location);
    }
    // bounce it back and make a new request
    else{
      request(location);
    }
  }

  private void startAuthorization(final String url) {
    final Uri uri = Uri.parse(url);
    if (uri == null) {
      GLog.w(TAG, "Illegal scheme for start-authorization.");
      mListener.onError();
    }
    mAuthorizer.retrieveRequestToken(null, new OnOAuthResponseListener<String>() {
      public void onSuccess(String response) {
        String target = uri.getQueryParameter("target");
        if (SetupDialog.TARGET_BROWSER.equals(target)) {
          //On Sandox environment this will return an OAuth callback which needs to be handled by NormalAuth DOA
          NormalAuthDao authDoa = (NormalAuthDao) Injector.getInstance(OAuthNormalDao.class);
          authDoa.initalize(mAuthorizer, mContext, mListener);
          // tells our Activity to redirect this
          authDoa.setRedircetToSetupActivity(false);
          if(Util.startBrowser(mContext, response)){
            return;
          }
          mListener.onError();
          return;
        }
        request(response);
      }
      public void onFailure(OAuthException e) {
        mListener.onError();
      }
    });
  }

  private void retrieveAccessToken(String scheme) {
    final Handler uiHandler = new Handler();
    Uri uri = Uri.parse(scheme);
    if (uri == null) {
      GLog.w(TAG, "Illegal scheme for get-accesstoken.");
      mListener.onError();
    }

    String denied = uri.getQueryParameter(SetupActivity.DENIED);
    if (!TextUtils.isEmpty(denied)) {
      GLog.d(TAG, "SSO is denied");
      mListener.onError();
    }

    mAuthorizer.retrieveAccessToken(null, scheme, new OnOAuthResponseListener<Void>() {
      public void onSuccess(Void value) {
        new Thread(new Runnable() {
          public void run() {
            AgreementListener listener = new AgreementListener() {
              public void onNeedAgreement(String url) {
                AgreementDialog dialog = new AgreementDialog(mContext, url);
                dialog.setOnDismissListener(new OnDismissListener() {
                  public void onDismiss(DialogInterface dialog) {
                    mListener.onSuccess();
                  }
                });
//                dialog.show();
              }
            };
            UserInfoUpdater updater = new UserInfoUpdater(mContext).sync(true).listener(listener);
            updater.request();
//          This needs to run on the UI thread since it is interacting with it
            uiHandler.post(new Runnable() { // This thread runs in the UI
              @Override
              public void run() {
                mListener.onSuccess();
              }
             });
//            }
          }
        }).start();
      }

      public void onFailure(OAuthException e) {
        GLog.e(TAG, "Failed to get access token");
        mListener.onError();
      }
    });
  }
}
