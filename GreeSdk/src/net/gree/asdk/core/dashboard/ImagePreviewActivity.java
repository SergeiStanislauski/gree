package net.gree.asdk.core.dashboard;

import java.io.IOException;

import net.gree.asdk.core.RR;
import android.app.Activity;
import android.content.Intent;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

public class ImagePreviewActivity extends Activity {
  private ImageView mImageView = null;
  private Uri mUri = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(RR.layout("gree_image_preview"));

    mImageView = (ImageView) findViewById(RR.id("gree_image_preview"));

    ImageButton confirmButton = (ImageButton) findViewById(RR.id("gree_button_ok"));
    confirmButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent();
        intent.setData(mUri);
        setResult(RESULT_OK, intent);
        finish();
      }
    });

    ImageButton cancelButton = (ImageButton) findViewById(RR.id("gree_button_cancel"));
    cancelButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        setResult(RESULT_CANCELED);
        finish();
      }
    });

    Intent intent = getIntent();
    if (intent != null) {
      if (intent.hasExtra("uri")) {
        mUri = intent.getParcelableExtra("uri");
      } else {
        confirmButton.setVisibility(View.GONE);
      }
    } else {
      confirmButton.setVisibility(View.GONE);
    }
  }

  @Override
  protected void onStart() {
    super.onStart();

    if (mUri != null) {
      int orientation = 0;
      try {
        ExifInterface exifInterface = new ExifInterface(mUri.getPath());
        String orientationString = exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION);
        if (orientationString != null) {
          orientation = Integer.parseInt(orientationString);
        }
      } catch (IOException e) {
      } catch (NumberFormatException e) {
      }

      ImageUploader.ImageData imageData = new ImageUploader(this).new ImageData(mUri, orientation);
      mImageView.setImageBitmap(imageData.mBitmap);
    }
  }
}
