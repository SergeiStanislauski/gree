/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import java.util.Map;

import org.apache.http.HttpEntity;

import net.gree.asdk.core.request.Constants.OAUTH_TYPE;
import net.gree.asdk.core.request.helper.MethodHelper;


/**
 * This class is a HTTP client for general purpose.
 * 
 * @author GREE, Inc.
 * 
 */
public class GeneralClient {

  /**
   * @param appendOpensocialParameters
   * @param method
   * @param headers
   * @param entity
   * @param sync
   * @param listener
   */
  public void oauth(String appendOpensocialParameters, String method, Map<String, String> headers,
      HttpEntity entity, boolean sync, OnResponseCallback<String> listener) {
    StringConverter converter = new StringConverter();
    ResponseHandler<String> handler = new ResponseHandler<String>(listener);
    new GreeRequest(appendOpensocialParameters, MethodHelper.parseString(method)).header(headers).entity(entity)
        .request(converter, handler);
  }

  /**
   * @param url
   * @param method
   * @param object
   * @param b
   * @param onResponseCallback
   */
  public void oauth2(String url, String method, Object object, boolean b,
      OnResponseCallback<String> listener) {
    StringConverter converter = new StringConverter();
    OAUTH_TYPE auth = OAUTH_TYPE._2LEGGED;
    ResponseHandler<String> handler = new ResponseHandler<String>(listener).oauth(auth);
    new GreeRequest(url, MethodHelper.parseString(method)).oauth(auth).request(
        converter, handler);
  }
}
