/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.cache;

import java.io.InputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.util.Log;

/**
 * Class that manages the I/O of the disk cache. This Cache specifies mostly to temp storage of InputStream objects
 * and mitigating the need to obtain these from external sources. It will check first to see how much space we can 
 * comfortably store as well to avoid memory issues.
 */
public class IOCache {
	private static final String TAG = "DrawableManager";
	private Map<String, InputStream> cache = Collections
			.synchronizedMap(new LinkedHashMap<String, InputStream>(10, 1.5f,
					true));// Last argument true for LRU ordering
	private long size = 0;// current allocated size
	private long limit = 1000000;// max memory in bytes

	/**
	 * Constructor class for our Cache. Sets the runtime so that we don't build the
	 * cache too large and run into memory issues.
	 */
	public IOCache() {
		// use 25% of available heap size
		setLimit(Runtime.getRuntime().maxMemory() / 4);
	}

	/**
	 * Sets the Limit on our memory cache and displays size in verbose logging.
	 * @param new_limit size of memory we are willing to use up to for this cache
	 */
	public void setLimit(long new_limit) {
		limit = new_limit;
		Log.i(TAG, "MemoryCache will use up to " + limit / 1024. / 1024. + "MB");
	}

	/**
	 * Gets the InputStream from the cache. Returns null if the InputStream does
	 * not exit in the cache.
	 * 
	 * @param id unique String that identifies the InputStream in the cache
	 * @return InputStream if it exists, null otherwise.
	 */
	public InputStream get(String id) {
		try {
			if (!cache.containsKey(id))
				return null;
			// NullPointerException sometimes happen here
			// http://code.google.com/p/osmdroid/issues/detail?id=78
			return cache.get(id);
		} catch (NullPointerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * Places the InputStream in the cache.
	 * @param id unique String identifier. Used to later recall the InputStream from the cache
	 * @param in InputStream object we want to cache
	 */
	public void put(String id, InputStream in) {
		try {
			if (cache.containsKey(id))
				size -= getSizeInBytes(cache.get(id));
			cache.put(id, in);
			size += getSizeInBytes(in);
			checkSize();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	/**
	 * Checks the size of the cache to make sure we have memory to add to.
	 */
	private void checkSize() {
		Log.i(TAG, "cache size=" + size + " length=" + cache.size());
		if (size > limit) {
			Iterator<Entry<String, InputStream>> iter = cache.entrySet()
					.iterator();// least recently accessed item will be the
								// first one iterated
			while (iter.hasNext()) {
				Entry<String, InputStream> entry = iter.next();
				size -= getSizeInBytes(entry.getValue());
				iter.remove();
				if (size <= limit)
					break;
			}
			Log.i(TAG, "Clean cache. New size " + cache.size());
		}
	}

	/**
	 * Clears the cache.
	 */
	public void clear() {
		try {
			// NullPointerException sometimes happen here
			// http://code.google.com/p/osmdroid/issues/detail?id=78
			cache.clear();
			size = 0;
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Obtains the size of the InputStream object
	 * @param in the InputStream object we are checking
	 * @return a long representing its size in bytes
	 */
	long getSizeInBytes(InputStream in) {
		if (in == null)
			return 0;
		/*
		 * Java strings are physically stored in UTF-16BE encoding, which uses 2
		 * bytes per code unit, and String.length() measures the length in
		 * UTF-16 code units
		 */
		return in.toString().length() * 2;// TODO does this accuratley represent
											// the size of the array

	}

}
