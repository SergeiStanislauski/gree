package net.gree.asdk.core.externalsocialgraph;


/**
 * This object represent a single user,
 * the informations are pulled from various sources (Facebook, the address book of the user, Gree user information)
 * This object does not use the same informations as the GreeUser API. 
 */
public class ExternalFriend {
  
  /** A Gree user */
  public static final int GREE_USER = 0;
  
  /** A user pulled from the address book */
  public static final int ADDRESS_BOOK = 1;
  
  /** A user pulled from Facebook */
  public static final int FACEBOOK_USER = 2;

  private int type;
  private String user_id;
  private String nick_name;
  private String first_name;
  private String last_name;
  private String facebook_id;
  private String tel;
  private String email;
  
  private String display_name;
  private String birthday;
  private String region;
  
  /*
  X private String id;
  O private String nickname;
  O private String displayName;
  M private String userGrade;
  O private String region;
  M private String subregion;
  M private String language;
  M private String timezone;
  M private String aboutMe;
  O private String birthday;
  M private String profileUrl;
  M private String thumbnailUrl;
  M private String thumbnailUrlSmall;
  M private String thumbnailUrlLarge;
  M private String thumbnailUrlHuge;
  M private String gender;
  M private String age;
  M private String bloodType;
  M private String hasApp;
  M private String userHash;
  M private String userType;
  */
  
  /**
   * The type of the user
   * @return one of GREE_USER, ADDRESS_BOOK, FACEBOOK_USER
   */
  public int getType() {
    return type;
  }

  /**
   * The user id in the External realm.
   * this id is different from the GreeUser.id ? maybe? FIXME
   * @return a string representing a user identification
   */
  public String getUser_id() {
    return user_id;
  }

  public String getNickName() {
    return nick_name;
  }
  /**
   * The user display name,
   * this is different from GreeUser.getDisplayName? maybe? FIXME
   */
  public String getDisplayName() {
    return display_name;
  }

  /**
   * the user's first name
   * @return the user's first name
   */
  public String getFirstName() {
    return first_name;
  }

  /** 
   * The user's last name
   * @return the user's last name
   */
  public String getLastName() {
    return last_name;
  }

  /**
   * Gets the user defined date field of his/her birthday. 
   * @return String field representing the user's birthday. 
   */
  public String getBirthday() {
    return birthday;
  }

  /**
   * The user's region
   * (i.e. US, JP, etc.)
   * @return the user's region
   */
  public String getRegion() {
    return region;
  }

  /**
   * The user's facebook Id
   * This facebook Id only make sense against Gree servers?
   * @return a hash representing a facebook user id 
   */
  public String getFacebook_id() {
    return facebook_id;
  }

  /**
   * The user's phone number
   * FIXME really? not a hashId?
   * @return the user's phone number
   */
  public String getTel() {
    return tel;
  }

  /**
   * The user's email address
   * FIXME really? not a hashId?
   * @return the user's email address
   */
  public String getEmail() {
    return email;
  }

  /**
   * Set the type of this user,
   * @param type one of GREE_USER, ADDRESS_BOOK, FACEBOOK_USER
   */
  public void setType(int type) {
    this.type = type;
  }

  /**
   * Check validity of asked type
   * @param type one of GREE_USER, ADDRESS_BOOK, FACEBOOK_USER
   * @return true if it is a valid type false otherwise
   */
  public static boolean isValidType(int type) {
    if ((type == GREE_USER) || (type == ADDRESS_BOOK) || (type == FACEBOOK_USER)) {
      return true;
    }
    return false;
  }
  
  @Override
  public String toString() {
    StringBuilder res = new StringBuilder();
    res.append("ExternalFriend: ");
    res.append(user_id);
    res.append("\n type: ");
    res.append(type);
    res.append("\n display_name: ");
    res.append(display_name);
    res.append("\n first_name: ");
    res.append(first_name);
    res.append("\n last_name: ");
    res.append(last_name);
    res.append("\n birthday: ");
    res.append(birthday);
    res.append("\n region: ");
    res.append(region);
    res.append("\n facebook_id: ");
    res.append(facebook_id);
    res.append("\n tel: ");
    res.append(tel);
    res.append("\n email: ");
    res.append(email);
    return res.toString();
  }
  
}
