/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api.ui;

import android.app.Activity;
import android.content.Context;
import net.gree.asdk.core.notifications.ui.NotificationPage;

/**
 * <p>Class which launches the notification board with sns or game tab page.</p>
 * <p>
 * If you want to launch the notification board with the sns tab, you call as the following:
 * </p>
 * Sample code:
 * <code><pre>
 * NotificationBoard.launchSns(Activity.this);
 * </pre></code>
 * <p>
 * If you want to launch the notification board with game tab, you call as the following:
 * </p>
 * Sample code:
 * <code><pre>
 * NotificationBoard.launch(Activity.this);
 * </pre></code>
 * @author GREE, Inc.
 */
public class NotificationBoard {

/**
 * Launches the notification board view with the sns tab page.
 * @param context The Context the view is to run in.
 * @return If the launch is a success, returns true. Otherwise returns false. (argument is mistake or internal error).
 * @deprecated use launch(Context context) instead, The action of this function is the same as launch(Context context).
 */
  public static boolean launchSns(Context context) {
    return NotificationPage.launch((Activity) context);
  }

/**
 * Launches the notification board view with the game tab page.
 * @param context The Context the view is to run it.
 * @return If the launch is a success, returns true. Otherwise returns false. (argument is mistake or internal error).
 * @deprecated use launch(Context context) instead, The action of this function is the same as launch(Context context).
 */
  public static boolean launchGame(Context context) {
    return NotificationPage.launch((Activity) context);
  }

/**
 * Launches the notification board view.
 * @param context The Context the view is to run it.
 * @return If the launch is a success, returns true. Otherwise returns false. (argument is mistake or internal error).
 */
  public static boolean launch(Context context) {
    return NotificationPage.launch((Activity) context);
  }
}
