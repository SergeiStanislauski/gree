/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import net.gree.asdk.api.auth.Authorizer.AuthorizeListener;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.api.ui.CloseMessage;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.dashboard.DashboardContentFragment.EVENT_TYPE;
import net.gree.asdk.core.notifications.NotificationCounts;
import net.gree.asdk.core.notifications.NotificationDownloader;
import net.gree.asdk.core.notifications.RelayActivity;
import net.gree.asdk.core.notifications.ui.NotificationFragment;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.ui.CommandInterfaceWebView;
import net.gree.asdk.core.ui.PopupDialog;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;
import net.gree.asdk.core.wallet.Deposit;

import org.apache.http.HeaderIterator;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * This is the main Gree Dashboard activity.
 * Developers can trigger it through the api.ui.Dashboard api.
 * Users can trigger it through the api.ui.DashboardButton.
 */
public class DashboardActivity extends FragmentActivity implements DashboardContentFragment.DashboardContentFragmentCallback {
  private static final String TAG = "DashboardActivity";
  private static final String DASHBOAD_CONTENT_FRAGMENT_NAME = "DASHBOAD_CONTENT_FRAGMENT_NAME";

  private static final long UM_SLIDE_ANIM_DURATION = 200;

  public static final String EXTRA_DASHBOARD_URL = "dashboard_url";
  public static final String EXTRA_IS_CUSTOM_ANIMATION_ENABLED = "is_custom_animation_enabled";
  public static final String LAUNCH_FROM_NOTIFICATION = "launh_from_notification";

  public static final int CLOSE_REQUEST_MESSAGE = 1;

  private static final String SERVICE_CODE = "DB0";

  private static final int OPTION_MENU_ITEM_ID_RELOAD = Menu.FIRST;
  private static final int OPTION_MENU_ITEM_ID_SETTINGS = Menu.FIRST + 1;

  private DashboardContentFragment mDashboardContentFragment;
  private boolean mIsUnivMenuChangingVisibility = false; // true if u_content is in animation

  private Handler mUiThreadHandler = new Handler();

  @SuppressWarnings("unused")
  private EmojiController mEmojiController;
  private int mUniversalMenuWidth;
  private UniversalMenuView mUmView = null;
  private boolean mCaughtBackKeyDown = false;

  private NotificationCounts mNotificationCounts;
  private NotificationFragment mNotificationFragment;
  private int mLocalAppsCount;
  private int mLocalSnsCount;
  private int mLocalFriendsCount;

  private boolean mSuppressPriorityChange;
  private int mOrigUiThreadPriority;
  private boolean mIsCustomAnimationEnabled = false;

  private static void startActivity(Activity activity, String dashboardUrl, boolean isCustomAnimationEnabled, Bundle notificationData) {
    Intent intent = new Intent(activity, DashboardActivity.class);

    if (dashboardUrl != null) {
      intent.putExtra(EXTRA_DASHBOARD_URL, dashboardUrl);
    }

    intent = intent.
        putExtra(EXTRA_IS_CUSTOM_ANIMATION_ENABLED, isCustomAnimationEnabled).
        putExtra(RelayActivity.EXTRA_NOTIFICATION_DATA, notificationData);
    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

    activity.startActivityForResult(intent, CloseMessage.REQUEST_CODE_DASHBOARD);
  }

  public static void show(final Activity context, final String dashboardUrl, final boolean isCustomAnimationEnabled, final Bundle notificationData) {

    IAuthorizer authorizer = Injector.getInstance(IAuthorizer.class);

    if (Util.canOptOutOfGREE() && !authorizer.hasOAuthAccessToken()) {
      authorizer.authorize(context, SERVICE_CODE, new AuthorizeListener() {
        public void onAuthorized() { startActivity(context, dashboardUrl, isCustomAnimationEnabled, notificationData); }
        public void onError() {}
        public void onCancel() {}
      }, null);
      return;
    }
    startActivity(context, dashboardUrl, isCustomAnimationEnabled, notificationData);
  }

  public static void show(final Activity context, final String dashboardUrl, final boolean isCustomAnimationEnabled) {

    show(context, dashboardUrl, isCustomAnimationEnabled, null);
  }

  public static void show(Activity context, String dashboardUrl) {

    show(context, dashboardUrl, true);
  }

  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    if (mDashboardContentFragment != null) {
      String dashboardUrl = mDashboardContentFragment.getWebView().getUrl();
      if (!TextUtils.isEmpty(dashboardUrl)) {
        savedInstanceState.putString(DashboardContentFragment.EXTRA_DASHBOARD_URL, dashboardUrl);
      }
    }
    super.onSaveInstanceState(savedInstanceState);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    //Notify that the notification board as started
    mNotificationCounts = Injector.getInstance(NotificationCounts.class);

    Intent intent = getIntent();
    if (null != intent && intent.getBooleanExtra(EXTRA_IS_CUSTOM_ANIMATION_ENABLED, true)) {
      setTheme(RR.style("GreeDashboardViewTheme"));
      mIsCustomAnimationEnabled = true;
    }

    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(RR.layout("gree_dashboardview"));

    // Input data inside Intent cannot be input to Fragment when Fragment is described in a layout file.
    // Therefore, a instance of Fragment should be generated at here.
    FragmentManager fm = this.getSupportFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();
    DashboardContentFragment fragment = (DashboardContentFragment)fm.findFragmentByTag(DASHBOAD_CONTENT_FRAGMENT_NAME);
    if (fragment != null) {
      ft.remove(fragment);
    }

    mDashboardContentFragment = new DashboardContentFragment();
    Bundle args = new Bundle();
    if (savedInstanceState != null && savedInstanceState.containsKey(DashboardContentFragment.EXTRA_DASHBOARD_URL)) {
      GLog.d(TAG, "DashboardActivity is re-created.");
      args.putString(DashboardContentFragment.EXTRA_DASHBOARD_URL,
        savedInstanceState.getString(DashboardContentFragment.EXTRA_DASHBOARD_URL));
    } else if (intent != null && intent.hasExtra(DashboardContentFragment.EXTRA_DASHBOARD_URL)) {
      args.putString(DashboardContentFragment.EXTRA_DASHBOARD_URL,
        intent.getStringExtra(DashboardContentFragment.EXTRA_DASHBOARD_URL));
    }
    mDashboardContentFragment.setArguments(args);
    ft.add(RR.id("gree_dashboard_content_container"), mDashboardContentFragment, DASHBOAD_CONTENT_FRAGMENT_NAME);
    ft.commit();


    initTouchFilter();
    initUniversalmenu();
    initNotificationButtons();
    initCloseButton();

    mEmojiController = new EmojiController(this);

    String value = CoreData.get(InternalSettings.SuppressThreadPriorityChangeBySdk, "false");
    mSuppressPriorityChange = Boolean.valueOf(value);


    if (Core.isGreeApp()) {
      // Display UniversalMenu tutorial dialog if it's never been shown.
      Tutorial.UNIVERSALMENU.showDialogOnce(this);
    }

    setResult(RESULT_OK);

  }

  private void downloadNotifications() {
    new NotificationDownloader(this, new OnResponseCallback<String>(){
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        mNotificationCounts.updateCounts(new NotificationCounts.Listener() {
          @Override
          public void onUpdate() {
            updateLocalBadgeCount();
            updateNotificationCount();
          }
        });
      }
      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.d(TAG, response);
      }}, mNotificationFragment != null ? mNotificationFragment.getAdapter() : null)
    .execute(null);    
  }

  @Override
  public void finish() {
    super.finish();
    // for Android 4.0
    if (mIsCustomAnimationEnabled) {
      overridePendingTransition(RR.anim("gree_activity_close_enter"), RR.anim("gree_activity_close_exit"));
    }
  }

  @Override
  protected void onResume() {
    super.onResume();

    downloadNotifications();

    if (!mSuppressPriorityChange) {
      Thread uiThread = mUiThreadHandler.getLooper().getThread();
      mOrigUiThreadPriority = uiThread.getPriority();
      if (mOrigUiThreadPriority <= Thread.NORM_PRIORITY) {
        uiThread.setPriority(Thread.NORM_PRIORITY + 1);
      }
    }

    if (null != mUmView && !mUmView.isShown()) {
      ToggleButton universalMenuButton = (ToggleButton) findViewById(RR.id("gree_universal_menu_button"));
      // would be "null" in T store case
      if(null != universalMenuButton) {
        universalMenuButton.setChecked(false);
      }
    }
  }

  @Override
  protected void onPause() {
    super.onPause();

    if (!mSuppressPriorityChange) {
      Thread uiThread = mUiThreadHandler.getLooper().getThread();
      uiThread.setPriority(mOrigUiThreadPriority);
    }

  }

  protected void onStart() {
    super.onStart();
  }

  @Override
  protected void onStop() {
    super.onStop();
    Injector.getInstance(NotificationCounts.class).updateCounts();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    // would be null in T store case
    if(null != mUmView) {
      mUmView.onDestroy();
    }
    mDashboardContentFragment.setDashboardContentFragmentCallback(null);
    mDashboardContentFragment = null;
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);

    if (intent.getBooleanExtra(LAUNCH_FROM_NOTIFICATION, false)) {
      mNotificationCounts.updateCounts(new NotificationCounts.Listener() {
        @Override
        public void onUpdate() {
          updateLocalBadgeCount();
          updateNotificationCount();
        }
      });
    }

    Bundle args = new Bundle();
    if (intent.hasExtra(DashboardContentFragment.EXTRA_DASHBOARD_URL)) {
      args.putString(DashboardContentFragment.EXTRA_DASHBOARD_URL,
        intent.getStringExtra(DashboardContentFragment.EXTRA_DASHBOARD_URL));
    }
    mDashboardContentFragment.onNewIntent(args);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == Deposit.BILLING_REQUEST_CODE && resultCode == RESULT_OK) {
      mDashboardContentFragment.onActivityResult(requestCode, resultCode, data);
      return;
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    menu.clear();

    if (null != mUmView && mUmView.isShown()) {
      menu.add(0, OPTION_MENU_ITEM_ID_RELOAD, Menu.NONE, RR.string("gree_dashboard_menu_reload_universal_menu"))
      .setIcon(RR.drawable("gree_ic_menu_refresh"));
    } else {
      menu.add(0, OPTION_MENU_ITEM_ID_RELOAD, Menu.NONE, RR.string("gree_dashboard_menu_reload"))
      .setIcon(RR.drawable("gree_ic_menu_refresh"));
      menu.add(0, OPTION_MENU_ITEM_ID_SETTINGS, Menu.NONE, RR.string("gree_dashboard_menu_settings"))
      .setIcon(RR.drawable("gree_ic_menu_preferences"));
    }

    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case OPTION_MENU_ITEM_ID_RELOAD:
        if (null != mUmView && mUmView.isShown()) {
          mUmView.reload();
        } else {
          if (null != mDashboardContentFragment) {
            mDashboardContentFragment.reload();
          }
        }
        break;
      case OPTION_MENU_ITEM_ID_SETTINGS:
        if (mDashboardContentFragment != null) {
          mDashboardContentFragment.loadContentView(Url.getSnsUrl() + "?view=settings_top", false);
        }
        break;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event){
    if(keyCode == KeyEvent.KEYCODE_BACK){ mCaughtBackKeyDown = true; }
    return super.onKeyDown(keyCode, event);
  }

  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    switch (keyCode) {
      case KeyEvent.KEYCODE_BACK:
        if(!mCaughtBackKeyDown){ return true; }
        mCaughtBackKeyDown = false;

        if (mNotificationFragment != null) {
          dismissNotification();
          return true;
        }

        if (null != mUmView) {
          if (mUmView.isShown()) {
            if (mUmView.isRootMenu()) {
              toggleUniversalMenuVisibility(this);
            } else {
              mUmView.loadRootMenu();
              Logger.recordLog("pg", "universalmenu_sns_top", "universalmenu_top", null);
            }
            return true;
          }
        }

        if (mDashboardContentFragment.onKeyUp(keyCode, event)) {
          return true;
        }
      default:
        break;
    }
    return super.onKeyUp(keyCode, event);
  }

  public void openFromMenu(String url) {
    if (AsyncErrorDialog.shouldShowErrorDialog(this)) {
      AsyncErrorDialog dialog = new AsyncErrorDialog(this);
      dialog.show();
      return;
    }
    mDashboardContentFragment.recordPerformData(PerformanceIndexMap.INDEX_KEY_SELECT_UM_ITEM);
    closeUniversalMenu();
    if (url.length() == 0) {
      return;
    }
    mDashboardContentFragment.loadContentView(url, true);
    recordOpenFromMenuLog(url);
  }

  public void toggleUniversalMenuVisibility(Context context) {

    if (mIsUnivMenuChangingVisibility || null == mUmView) {
      return;
    }

    mIsUnivMenuChangingVisibility = true;

    final View contentView = this.findViewById(RR.id("gree_u_content"));

    final ToggleButton button = (ToggleButton) this.findViewById(RR.id("gree_universal_menu_button"));
    if (mUmView.isShown()) {
      // Set UM close animation
      TranslateAnimation animation = new TranslateAnimation(mUniversalMenuWidth, 0, 0, 0);
      animation.setDuration(UM_SLIDE_ANIM_DURATION);
      animation.setAnimationListener(new AnimationListener() {
        public void onAnimationStart(Animation animation) {}

        public void onAnimationRepeat(Animation animation) {}

        public void onAnimationEnd(Animation animation) {
          contentView.layout(0, 0, contentView.getRight(), contentView.getBottom());
          contentView.clearAnimation();
          mIsUnivMenuChangingVisibility = false;
          mUmView.setShown(false);

        }
      });
      contentView.startAnimation(animation);
      contentView.layout(0, 0, contentView.getRight(), contentView.getBottom());
      contentView.setVisibility(View.VISIBLE);
      if (button.isChecked()) {
        button.setChecked(false);
      }

    } else {     
      if (mNotificationFragment != null) {
        dismissNotification();
      }

      InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      boolean resultHideSoftInput = inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
      if (resultHideSoftInput) {
        new UnivarsalmenuOpenHandler().sendEmptyMessage(0);
      } else {
        openUnivarsalMenu();
      }
    }
  }

  private void openUnivarsalMenu() {
    if (null == mUmView) {
      return;
    }

    final View contentView = DashboardActivity.this.findViewById(RR.id("gree_u_content"));
    final ToggleButton button = (ToggleButton) DashboardActivity.this.findViewById(RR.id("gree_universal_menu_button"));

    CommandInterfaceView.hideProgressDialog();

    // Set UM open animation
    TranslateAnimation animation = new TranslateAnimation(0, mUniversalMenuWidth, 0, 0);
    animation.setDuration(UM_SLIDE_ANIM_DURATION);
    animation.setFillAfter(true);
    animation.setFillEnabled(true);
    animation.setAnimationListener(new AnimationListener() {
      public void onAnimationStart(Animation animation) {
        mUmView.setShown(true);
      }

      public void onAnimationRepeat(Animation animation) {}

      public void onAnimationEnd(Animation animation) {
        contentView.layout(mUniversalMenuWidth, 0, contentView.getRight(),
          contentView.getBottom());
        contentView.clearAnimation();
        mIsUnivMenuChangingVisibility = false;
        contentView.requestLayout();

        recordOpenUniversalMenuLog();
      }
    });
    contentView.startAnimation(animation);
    if (!button.isChecked()) {
      button.setChecked(true);
    }

    mUmView.reload();
  }

  private void recordOpenUniversalMenuLog() {
    String dashboardUrl = mDashboardContentFragment.getWebView().getUrl();
    if (dashboardUrl == null || dashboardUrl.length() == 0) {
      return;
    }

    String evtFrom = getRecordNameParam(dashboardUrl);
    if (evtFrom != null) {
      Logger.recordLog("pg", "universalmenu_top", evtFrom, null);
    }
  }

  private void recordOpenFromMenuLog(String url) {
     String name = getRecordNameParam(url);
    if (name != null) {
      Logger.recordLog("pg", name, "universalmenu_top", null);
    }
  }

  private String getRecordNameParam(String url) {
      String name = null;
      if (url.contains("#")) {
      String query = url.substring(url.indexOf("#") + 1);
      if (query != null && query.length() > 0) {
        String[] paramArray = query.split("&");
        for (int i = 0; i < paramArray.length; i++) {
          if (paramArray[i].startsWith("view=")) {
            String[] param = paramArray[i].split("=");
            name = param[1];
            break;
          }
        }
      }
      if (TextUtils.isEmpty(name)) {
        String[] paramArray = url.split("&");
        for (int i = 0; i < paramArray.length; i++) {
          if (paramArray[i].startsWith("view=")) {
            String[] param = paramArray[i].split("=");
            name = param[1];
            break;
          }
        }
      }
    } else if (url.contains("?")) {
      name = url.substring(0, url.indexOf("?"));
    } else {
      name = url;
    }
    if (TextUtils.isEmpty(name)) {
      return null;
    }

    if (name.contains("#")) {
      name = name.substring(0, name.indexOf("#"));
    }
    return name;
  }

  private class UnivarsalmenuOpenHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
      openUnivarsalMenu();
    }
  }

  private void closeUniversalMenu() {
    if (null != mUmView && mUmView.isShown()) {
      toggleUniversalMenuVisibility(DashboardActivity.this);
    }
  }

  private void updateNotificationCount() {    
    updateBadge();
    
    Button appsBtn = (Button) findViewById(RR.id("gree_u_notification_apps"));
    Button snsBtn = (Button) findViewById(RR.id("gree_u_notification_sns"));
    Button friendsBtn = (Button) findViewById(RR.id("gree_u_notification_friends"));

    // would be "null" in T store case
    if(null != appsBtn) {
      if (mLocalAppsCount > 0) {
        appsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_game_on"));
      } else {
        appsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_selector_game"));
      }
    }
    
    if(null != snsBtn) {
      if (mLocalSnsCount > 0) {
        snsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_sns_on"));
      } else {
        snsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_selector_sns"));
      }
    }
    
    if(null != friendsBtn) {
      if (mLocalFriendsCount > 0) {
        friendsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_friends_on"));
      } else {
        friendsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_selector_friends"));
      }
    }
  }

  private void badgeDisplay(int count, View root, TextView countText){
    if (count <= 0) {
      root.setVisibility(View.GONE);
    } else if (0 < count && count < 99) {
      root.setVisibility(View.VISIBLE);
      countText.setText("" + count);
    } else {
      root.setVisibility(View.VISIBLE);
      countText.setText("99+");
    }
  }
  
  private void updateBadge() {
    View appsBadge = findViewById(RR.id("gree_notificationCountAppsBadge"));
    View snsBadge = findViewById(RR.id("gree_notificationCountSnsBadge"));
    View friendsBadge = findViewById(RR.id("gree_notificationCountFriendsBadge"));    
    TextView appsText = (TextView) findViewById(RR.id("gree_notificationCountApps"));
    TextView snsText = (TextView) findViewById(RR.id("gree_notificationCountSns"));
    TextView friendsText = (TextView) findViewById(RR.id("gree_notificationCountFriends"));

    // would be "null" in T store case
    if(null != appsBadge) {
      badgeDisplay(mLocalAppsCount, appsBadge, appsText);
    }
    if(null != snsBadge) {
      badgeDisplay(mLocalSnsCount, snsBadge, snsText);
    }
    if(null != friendsBadge) {
      badgeDisplay(mLocalFriendsCount, friendsBadge, friendsText);
    }
    
  }
  
  private void updateLocalBadgeCount() {
    mLocalAppsCount = mNotificationCounts.getNotificationCount(NotificationCounts.TYPE_APP);
    mLocalSnsCount = mNotificationCounts.getNotificationCount(NotificationCounts.TYPE_SNS);
    mLocalFriendsCount = mNotificationCounts.getNotificationCount(NotificationCounts.TYPE_FRIENDS);
  }

  private void recordLogForDashboardClose() {
    CommandInterfaceWebView commandInterfaceWebview = mDashboardContentFragment.getWebView();
    String url = commandInterfaceWebview != null ? commandInterfaceWebview.getUrl() : null;
    if (!TextUtils.isEmpty(url)) {
      String evtFrom = url;
      if (url.contains("?")) {
        evtFrom = url.substring(0, url.indexOf("?"));
      }
      Logger.recordLog("pg", "game", evtFrom, null);
    }
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {

    super.onConfigurationChanged(newConfig);

    if (null != mUmView) {
      mUmView.setUpWidth();
      mUniversalMenuWidth = mUmView.getMenuWidth();
    }

    DashboardNavigationBar bar = (DashboardNavigationBar)findViewById(RR.id("gree_dashboard_navigationbar"));
    bar.adjustNavigationBarHeight(newConfig);
  }

  /**
   * Enumeration of tutorial dialogs. It shows a PopupDialog that has content View inflated with a
   * given layout. Each constant object has (should have) a unique key that is a String used to
   * generate its key string for shared preference.
   */
  private enum Tutorial {
    UNIVERSALMENU(RR.layout("gree_tutorial_universalmenu"), "um");

    private static final String SHARED_PREF_KEY_PREFIX = "key_tutorial_";
    private final String mKey;
    private final int mContentLayout;

    /**
     * Constructs member constants.
     * 
     * @param content a layout resource ID for the main content of the tutorial dialog
     * @param key a String that should be unique among the member constants
     */
    Tutorial(int content, String key) {
      this.mKey = key;
      this.mContentLayout = content;
    }

    private String getKey() {
      return SHARED_PREF_KEY_PREFIX + mKey;
    }

    /**
     * @param context Context in which the PopupDilog should be created
     * @return boolean value indicating whether the tutorial has been viewed by the user
     */
    @SuppressLint(value = {"WorldWriteableFiles"})
    public boolean hasShown(Context context) {
      return DashboardStorage.getBoolean(context, getKey(), false);
    }

    /**
     * Display tutorial dialog if it has never been shown.
     * @param activity The Activity that showing this dialog
     */
    public void showDialogOnce(Activity activity) {
      if (!hasShown(activity)) {
        showDialog(activity);
      }
    }

    /**
     * Displays a new instance of PopupDialog with the given view to the member constant.
     * 
     * @param activity Context in which the PopupDilog should be created
     */
    public void showDialog(Activity activity) {
      final Context context = activity;
      View content = activity.getLayoutInflater().inflate(mContentLayout, null);
      final PopupDialog dialog = new PopupDialog(activity, content);
      dialog.setFitContent(true, RR.id("gree_tutorial_content"));

      dialog.setDismissButtonListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          close(context, dialog);
        }
      });

      dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
          if ((KeyEvent.KEYCODE_BACK == keyCode) && (KeyEvent.ACTION_UP == event.getAction())) {
            close(context, dialog);
            return true;
          }
          return false;
        }
      });

      dialog.show();
    }

    @SuppressLint(value = {"WorldWriteableFiles"})
    private void close(Context context, DialogInterface dialog) {
      DashboardStorage.putBoolean(context, getKey(), true);
      dialog.dismiss();
    }
  }

  private void initCloseButton(){
    Button close = (Button) findViewById(RR.id("gree_u_close"));
    if (Core.isGreeApp()) {
      close.setVisibility(View.GONE);
    } else {
      close.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
          if (null != mDashboardContentFragment) {
            mDashboardContentFragment.hideLoadingIndicator();
          }
          recordLogForDashboardClose();
          finish();
        }
      });
    }    
  }


  private void initTouchFilter(){
    // touchFilter prevents touch events from propagating to the views
    // underneath of it when universal menu is visible.
    View touchFilter = findViewById(RR.id("gree_u_touch_filter"));
    touchFilter.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (null != mUmView && mUmView.isShown()) {
          toggleUniversalMenuVisibility(DashboardActivity.this);
          return true;
        } else {
          return false;
        }
      }
    });   

    Button dummyBtn = (Button) findViewById(RR.id("gree_u_navbar_dummy"));
    dummyBtn.setOnClickListener(new OnClickListener(){
      @Override
      public void onClick(View v) {
        if (mNotificationFragment != null){
          dismissNotification();
        }
      }
    });

  }

  private void initUniversalmenu(){
    ToggleButton universalMenuButton = (ToggleButton) findViewById(RR.id("gree_universal_menu_button"));
    // would be "null" in T store case
    if(null != universalMenuButton) {
      universalMenuButton.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
          if (null != mUmView && !mUmView.isShown()) {
            mDashboardContentFragment.recordPerformData(PerformanceIndexMap.INDEX_KEY_GOTO_UM);
          }
          toggleUniversalMenuVisibility(v.getContext());
        }
      });

      /* prevent propagating touch event in Universal Menu. */
      findViewById(RR.id("gree_navigation_bar_frame")).setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          /* do nothing. */
        }
      });

      mUmView = (UniversalMenuView) findViewById(RR.id("gree_u_menu"));
      if (null != mUmView) {
        mUmView.initialize(universalMenuButton.getLayoutParams().width, this);
        mUniversalMenuWidth = mUmView.getMenuWidth();
      }
    }

  }

  /**
   * get DashboardContentFragment
   * @return main dashboard fragment
   */
  public DashboardContentFragment getDashboardContentFragment() {
    return mDashboardContentFragment;
  }

  /**
   * getter
   * @return URL shown in dashbaord
   */
  public String getCurrentContentViewUrl() {
    return mDashboardContentFragment.getCurrentContentViewUrl();
  }

  /**
   * Show notification popup
   *  This will not be called in T store case
   */
  private void showNotificationPopup(int which) {
    Button appsBtn = (Button) findViewById(RR.id("gree_u_notification_apps"));
    Button snsBtn = (Button) findViewById(RR.id("gree_u_notification_sns"));
    Button friendsBtn = (Button) findViewById(RR.id("gree_u_notification_friends"));
    
    View appsBadge = findViewById(RR.id("gree_notificationCountAppsBadge"));
    View snsBadge = findViewById(RR.id("gree_notificationCountSnsBadge"));


    if (mNotificationFragment != null && which == mNotificationFragment.getNotificationType()) {
      dismissNotification();
    } else {
      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      if (mNotificationFragment != null) {
        ft.remove(mNotificationFragment);
      }

      NotificationFragment newFragment = NotificationFragment.newInstance(which);
      mNotificationFragment = newFragment;
      ft.add(RR.id("gree_u_content"), newFragment);
      ft.addToBackStack(null);    
      ft.commit();
      ft.show(newFragment);
      
      int appsDrawable;
      int snsDrawable;
      int friendsDrawable;
      
      if (mLocalAppsCount > 0) {
        appsDrawable = RR.drawable("gree_btn_navbar_notification_game_on");
      } else {
        appsDrawable = RR.drawable("gree_btn_navbar_notification_selector_game");
      }
      
      if (mLocalSnsCount > 0) {
        snsDrawable = RR.drawable("gree_btn_navbar_notification_sns_on");
      } else {
        snsDrawable = RR.drawable("gree_btn_navbar_notification_selector_sns");
      }
      
      if (mLocalFriendsCount > 0) {
        friendsDrawable = RR.drawable("gree_btn_navbar_notification_friends_on");
      } else {
        friendsDrawable = RR.drawable("gree_btn_navbar_notification_selector_friends");
      }

      switch (which) {
        case NotificationFragment.NOTIFICATION_APPS:
          mLocalAppsCount = 0;
          appsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_game_press"));
          snsBtn.setBackgroundResource(snsDrawable);
          friendsBtn.setBackgroundResource(friendsDrawable);
          appsBadge.setVisibility(View.GONE);
          break;
        case NotificationFragment.NOTIFICATION_SNS:
          mLocalSnsCount = 0;
          appsBtn.setBackgroundResource(appsDrawable);
          snsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_sns_press"));
          friendsBtn.setBackgroundResource(friendsDrawable);
          snsBadge.setVisibility(View.GONE);
          break;
        case NotificationFragment.NOTIFICATION_FRIENDS:
          mLocalFriendsCount = 0;
          appsBtn.setBackgroundResource(appsDrawable);
          snsBtn.setBackgroundResource(snsDrawable);
          friendsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_friends_press"));
          break;
      }
      
      updateBadge();
    }
  }

  public void removeFragment(Fragment fragment) {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.remove(fragment);
    ft.commit();
  }

  // This will not be called in T store case.
  public void dismissNotification() {
    Button appsBtn = (Button) findViewById(RR.id("gree_u_notification_apps"));
    Button snsBtn = (Button) findViewById(RR.id("gree_u_notification_sns"));
    Button friendsBtn = (Button) findViewById(RR.id("gree_u_notification_friends"));


    if (mNotificationFragment != null) {
      switch (mNotificationFragment.getNotificationType()) {
        case NotificationFragment.NOTIFICATION_APPS:
          appsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_selector_game"));
          break;
        case NotificationFragment.NOTIFICATION_SNS:
          snsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_selector_sns"));
          break;
        case NotificationFragment.NOTIFICATION_FRIENDS:
          friendsBtn.setBackgroundResource(RR.drawable("gree_btn_navbar_notification_selector_friends"));
          break;
      }

      removeFragment(mNotificationFragment);
      mNotificationFragment = null;
    }
  }

  private void initNotificationButtons(){
    DisplayMetrics metrics = new DisplayMetrics();
    Display display = getWindowManager().getDefaultDisplay();
    display.getMetrics(metrics);

    // would be "null" in T store case
    Button notificationButtonGame = (Button) findViewById(RR.id("gree_u_notification_apps"));
    if(null != notificationButtonGame) {
      notificationButtonGame.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          showNotificationPopup(NotificationFragment.NOTIFICATION_APPS);
          Logger.recordLog("evt", "ngame", "nb", null);
        }
      });
    }

    Button notificationButtonSNS = (Button) findViewById(RR.id("gree_u_notification_sns"));
    if(null != notificationButtonSNS) {
      notificationButtonSNS.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          showNotificationPopup(NotificationFragment.NOTIFICATION_SNS);
          Logger.recordLog("evt", "nsns", "nb", null);
        }
      });
    }

    Button notificationButtonFriends = (Button) findViewById(RR.id("gree_u_notification_friends"));
    if(null != notificationButtonFriends) {
      notificationButtonFriends.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          showNotificationPopup(NotificationFragment.NOTIFICATION_FRIENDS);
          Logger.recordLog("evt", "nfriend", "nb", null);
        }
      });
    }

  }

  @Override
  public void notifyDashboardContentFragmentFinishRequest(EVENT_TYPE type, Object arg) {
    switch (type) {
      case NORMAL_PENDING_TRANSITION:
        finish();
        overridePendingTransition(0, 0);
        break;
      case NORMAL:
        finish();
        break;
    }
  }

}
