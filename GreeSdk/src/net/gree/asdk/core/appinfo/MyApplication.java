/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.appinfo;

import java.util.Map;

import org.apache.http.HeaderIterator;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.CoreData;
import net.gree.vendor.com.google.gson.Gson;

/**
 * The class for fetching application information given the user(s) id.
 * Each user as a list of application that he is, or has used.
 * You can get this list by calling this class.
 */
public class MyApplication {
  private static final String TAG = "MyApplication";
  private static boolean isDebug = true;
  private static boolean isVerbose = true;

  /** The parameter key, if you want to specify the application offset */
  public static final String KEY_NAME_OFFSET    = "offset";     // int.

  /** The parameter key, if you want to specify a number of application to get */ 
  public static final String KEY_NAME_LIMIT     = "limit";      // int.

  /** The parameter key, if you want to filter the application supported language */
  public static final String KEY_NAME_LANGUAGE  = "language";   // String.

  /** 
   * The parameter key, if you want to filter the application device type,
   * Value can be DEVICE_TYPE_ALL, DEVICE_TYPE_FP, DEVICE_TYPE_SP, DEVICE_TYPE_IOS or DEVICE_TYPE_ANDROID
   */
  public static final String KEY_NAME_DEVICE    = "device";     // String.

  /**
   * The parameter key, if you want to disable the fetching of thumbnails url.
   * by default thumnail are enabled and you don't need to set this. 
   */
  public static final String KEY_NAME_THUMBNAIL = "thumbnail";  // boolean.

  /** Application released for any device */
  public static final String DEVICE_TYPE_ALL            = "all";

  /** Application released only on feature phone */
  public static final String DEVICE_TYPE_FP             = "fp";

  /** Application released only on smartphone */
  public static final String DEVICE_TYPE_SP             = "sp";

  /** Application released only on IOS */
  public static final String DEVICE_TYPE_IOS            = "ios";

  /** Application released only on Android */
  public static final String DEVICE_TYPE_ANDROID        = "android";

  /**
   * Constructor.
   */
  private MyApplication() {}

  /**
   * The listener for getting the list of application for one particular user
   */
  public interface ApplicationInfoListener {
    /**
     * Received a list of application info for the given user id.
     * @param startIndex the index (from 1)
     * @param hasMore true if there is more ApplicationInfo to be downloaded
     * @param limit the maximum number of ApplicationInfo that be retrieved in one request
     * @param ApplicationInfo a list of application informations
     */
    void onSuccess(int startIndex, boolean hasMore, int limit, ApplicationInfo[] appInfo);
    /**
     * Called when an error occurred in trying to get the list of application.
     * @param responseCode Integer representing the http response code.
     * @param headers Object representing the headers of the http response.
     * @param response String representing the body of the http response.
     */
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * The listener for getting the list of applications for several users
   */
  public interface UserApplicationInfoListener {
    /**
     * Received a list of application info for the given user ids.
     * @param startIndex the index (from 1)
     * @param hasMore true if there is more ApplicationInfo to be downloaded for at least one of the user
     * @param limit the maximum number of ApplicationInfo that be retrieved in one request
     * @param appInfo a Map between the requested userId and the list of application informations
     */
    void onSuccess(int startIndex, boolean hasMore, int limit, Map<String, ApplicationInfo[]> appInfo);
    /**
     * Called when an error occurred in trying to get the list of application.
     * @param responseCode Integer representing the http response code.
     * @param headers Object representing the headers of the http response.
     * @param response String representing the body of the http response.
     */
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * API for getting the application list of the user currently logged in.
   * @param params optional request parameter as hashmap.
   * @param listener callback listener.
   */
  public static void loadApplicationList(Map<String, Object> params, final ApplicationInfoListener listener) {
    getApplicationInfo("@me", params, listener);
  }

  /**
   * API for getting the application list of one user.
   * @param userId the user id
   * @param params optional request parameter as hashmap.
   * @param listener callback listener.
   */
  public static void loadApplicationList(String userId, Map<String, Object> params, final ApplicationInfoListener listener) {
    if ((userId == null) || (userId.length() == 0)) {
      userId = "@me";
    }
    getApplicationInfo(userId, params, listener);
  }

  /**
   * API for getting the application list of one user
   * @param userId the user id
   * @param params optional request parameter as hashmap.
   * @param listener callback listener.
   */
  private static void getApplicationInfo(final String userId, Map<String, Object> params, final ApplicationInfoListener listener) {
    IAuthorizer authorizer = Injector.getInstance(IAuthorizer.class);

    if (!authorizer.hasOAuthAccessToken()) {
      GLog.i(TAG, "Not authorized.");
      throw new IllegalStateException();
    }

    ApiRequest request = new ApiRequest(CoreData.getParams());
    if (isDebug) {
      GLog.d(TAG, "requesting my list of application : " + userId + "/myapp");
    }
    request.oauth(userId + "/myapp", "get", params, null, false,
      new OnResponseCallback<String>() {

      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        try {
          Response response = Injector.getInstance(Gson.class).fromJson(json, Response.class);
          if (isVerbose) {
            GLog.v(TAG, "result:" + response.offset + " " + response.limit + " " + response.hasMore());
          }
          if (listener != null) {
            listener.onSuccess(response.offset, response.hasMore(), response.limit, response.getApplicationInfo(userId));
          }
        } catch (Exception ex) {
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        listener.onFailure(responseCode, headers, response);
      }
    });
  }

  /**
   * The response sent by the server for one user or multiple users.
   */
  private class Response {
    public Map<String, ApplicationInfo[]> entry;
    public int has_more;
    public int limit;
    public int offset;

    public ApplicationInfo[] getApplicationInfo(String uid) {
      return entry.get(uid);
    }

    public boolean hasMore() {
      return "1".equals(has_more);
    }
  }

  /**
   * API for getting targeted friend's application list.
   * @param users target user ids as String array. (ex: {"1", "2", "3"})
   * @param params optional request parameter as hashmap.
   * @param listener callback listener.
   */
  public static void getMulti(String users[], Map<String, Object> params, final UserApplicationInfoListener listener) {
    StringBuilder userStr = new StringBuilder();

    for(String user : users) {
      userStr.append(user).append(",");
    }

    if (isDebug) {
      GLog.d(TAG, "requesting my list of application : " + userStr.substring(0, userStr.length() - 1) + "/myapp");
    }
    ApiRequest request = new ApiRequest(CoreData.getParams());
    request.oauth(userStr.substring(0, userStr.length() - 1) + "/myapp", "get", params, null, false,
      new OnResponseCallback<String>() {

      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        try {
          Response response = Injector.getInstance(Gson.class).fromJson(json, Response.class);
          if (isVerbose) {
            GLog.v(TAG, "result:" + response.offset + " " + response.limit + " " + response.hasMore());
          }
          if (listener != null) {
            listener.onSuccess(response.limit, response.hasMore(), response.limit, response.entry);
          }
        } catch (Exception ex) {
          if (listener != null) {
            listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
          }
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
}
