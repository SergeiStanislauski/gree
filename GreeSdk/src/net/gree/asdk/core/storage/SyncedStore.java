/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.storage;

import net.gree.asdk.core.GLog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * This class is copied from OFSDK and the main purpose is to save UUID in local file. It's just
 * used to maintain the back compatibility of previous OF apps.
 * 
 * It's better to use { @link LocalStorage } to save local data.
 */
public class SyncedStore {
  private static final String FILENAME = "gree_prefs";
  private static final String TAG = "SyncedStore";

  private HashMap<String, String> mMap;
  private Context mContext;
  private ReentrantReadWriteLock mLock;

  private void load() {
    mMap = null;
    boolean mustSaveAfterLoad = false;

    long start = System.currentTimeMillis();

    File myStore = mContext.getFileStreamPath(FILENAME);

    mLock.writeLock().lock();
    try {
      final PackageManager packageManager = mContext.getPackageManager();
      List<ApplicationInfo> apps = packageManager.getInstalledApplications(0);
      ApplicationInfo myInfo = null;

      for (ApplicationInfo ai : apps) {
        if (ai.packageName.equals(mContext.getPackageName())) {
          myInfo = ai;
          break;
        }
      }

      final String myStoreCPath = myStore.getCanonicalPath();
      if (myInfo != null && myStoreCPath.startsWith(myInfo.dataDir)) {
        String underDataDir = myStoreCPath.substring(myInfo.dataDir.length());

        for (ApplicationInfo ai : apps) {
          File otherStore = new File(ai.dataDir, underDataDir);

          // strict inequality means that this store is NOT from myInfo, therefore
          // we have to save it in our own location once we've loaded it.
          if (myStore.lastModified() < otherStore.lastModified()) {
            mustSaveAfterLoad = true;
            myStore = otherStore;
          }
        }

        mMap = mapFromStore(myStore);
      }

      if (mMap == null) {
        mMap = new HashMap<String, String>();
      }
    } catch (IOException e1) {
      GLog.e(TAG, "broken");
    } finally {
      mLock.writeLock().unlock();
    }

    if (mustSaveAfterLoad) {
      save();
    }

    long elapsed = System.currentTimeMillis() - start;
    GLog.d(TAG, "Loading prefs took " + elapsed + " millis");
  }

  @SuppressWarnings("unchecked")
  private HashMap<String, String> mapFromStore(File myStore) {
    InputStream is = null;
    ObjectInputStream ois = null;
    try {
      is = new FileInputStream(myStore);
      ois = new ObjectInputStream(is);
      Object o = ois.readObject();
      if (o != null && o instanceof Map<?, ?>) {
        return (HashMap<String, String>) o;
      }
    } catch (FileNotFoundException e) {
      GLog.e(TAG, "Couldn't open " + FILENAME);
    } catch (StreamCorruptedException e) {
      GLog.e(TAG, "StreamCorruptedException");
    } catch (IOException e) {
      GLog.e(TAG, "IOException while reading");
    } catch (ClassNotFoundException e) {
      GLog.e(TAG, "ClassNotFoundException");
    } finally {
      try {
        if (ois != null) {
          ois.close();
        } else if (is != null) {
          is.close();
        }
      } catch (IOException e) {
        GLog.e(TAG, "IOException while cleaning up");
      }
    }
    return new HashMap<String, String>();
  }

  @SuppressLint(value = { "WorldReadableFiles" })
  private void save() {
    OutputStream os = null;
    ObjectOutputStream oos = null;

    mLock.readLock().lock();
    try {
      os = mContext.openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
      oos = new ObjectOutputStream(os);
      oos.writeObject(mMap);
    } catch (IOException e) {
      GLog.e(TAG, "Couldn't open " + FILENAME + " for writing");
    } finally {
      try {
        if (oos != null) {
          oos.close();
        } else if (os != null) {
          os.close();
        }
      } catch (IOException e) {
        GLog.e(TAG, "IOException while cleaning up");
      } finally {
        mLock.readLock().unlock();
      }
    }
  }

  /**
   * The editor class is used to write strings settings to a local file.
   * It uses a Hashmap to keep the local data,
   * call the {@link commit()} method to write the whole Hashmap to the backup file. 
   */
  public class Editor {

    /**
     * Add a string to the current Hashmap
     * @param k the key
     * @param v the value to be stored
     */
    public void putString(String k, String v) {
      SyncedStore.this.mMap.put(k, v);
    }

    /**
     * Remove a string from the hashmap
     * @param k the key of the value to be removed
     */
    public void remove(String k) {
      SyncedStore.this.mMap.remove(k);
    }

    /**
     * get a keyset for the current hashmap
     * @return a keyset for the current hashmap
     */
    public Set<String> keySet() {
      // duplicate the key set, so if we remove/add keys, we don't get
      // a ConcurrentModificationException
      return new HashSet<String>(mMap.keySet());
    }

    /**
     * Save all the values in the hashmap to a file,
     * and release the lock.
     */
    public void commit() {
      save();
      mLock.writeLock().unlock();
    }
  }

  /**
   * Take the lock and create an Editor to add values.
   * dont forget to call commit to release the lock.
   * @return a new Editor object that you can use to put string into.
   */
  public Editor edit() {
    mLock.writeLock().lock();
    return new Editor();
  }

  /**
   * The reader class is used to retrieve saved values.
   * the values are read from a file and kept in a hashmap
   */
  public class Reader {
    
    /**
     * Retrieve a value for the given string from the hashmap
     * @param k the key 
     * @param defValue the default value to be returned if the key doesn't exist.
     * @return the value indexed or defValue if it is not found.
     */
    public String getString(String k, String defValue) {
      String rv = mMap.get(k);
      return rv != null ? rv : defValue;
    }

    /**
     * Retrieve the keyset of the hashmap
     * @return a keyset
     */
    public Set<String> keySet() {
      return mMap.keySet();
    }

    /**
     * Release the lock,
     * make sure you call this after reading the values.
     */
    public void complete() {
      mLock.readLock().unlock();
    }
  }

  /**
   * Take the lock and return a new Reader that can be used to access saved values.
   * @return a Reader
   */
  public Reader read() {
    mLock.readLock().lock();
    return new Reader();
  }

  /**
   * Initialize the reader from a file.
   * the values are kept in a hashmap.
   * @param c the context of current application
   */
  public SyncedStore(Context c) {
    mContext = c;
    mMap = new HashMap<String, String>();
    mLock = new ReentrantReadWriteLock();
    load();
  }

  /**
   * Delete values from the hashmap and from the file
   */
  public void clear() {
    mMap.clear();
    save();
  }

  /**
   * Delete the file.
   */
  public void delete() {
    File file = new File(mContext.getFilesDir(), FILENAME);
    file.delete();
  }

}
