/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import java.util.Map;

/**
 * This class defines the events to be used by {@link #TaskEventListener}
 */
public class GreePlatformListener {

 
 
  /**
   * KEY_CLASS_TYPE is the key referencing the type of Class.
   * The corresponding value is an Integer describing one of: <br>
   * CLASS_UNDEFINED, CLASS_DASHBOARD_ACTIVITY, CLASS_NOTIFICATION_BOARD_ACTIVITY, 
   * CLASS_INVITE_DIALOG, CLASS_SHARE_DIALOG, CLASS_REQUEST_DIALOG, CLASS_AUTHORIZATION_DIALOG or CLASS_PAYMENT_DIALOG.
   */
 
  public static final String KEY_CLASS_TYPE = "classType";

 
 
  /**
   * KEY_EVENT_TYPE is the key referencing the type of event.
   * the corresponding value is an Integer describing one of : 
   * EVENT_UNKNOWN, EVENT_OPEN, EVENT_CLOSE, EVENT_CANCEL, EVENT_DESTROYED, EVENT_LOGIN, EVENT_LOGOUT, EVENT_USERINFO_RETRIEVED
   */
 
  public static final String KEY_EVENT_TYPE = "eventType";

 
 
  /**
   * KEY_INSTANCE is the key referencing the instance.
   * The given object can be cast to their corresponding class.
   */
 
  public static final String KEY_INSTANCE = "instance";

 
 
  /**
   * KEY_RESULTS is the key referencing the results.
   * The corresponding object is a JSONObject.
   */
 
  public static final String KEY_RESULTS = "results";

 
 
  /**
   * If you receive an event with EVENT_UNKNOWN type, you should ignore it.
   */
 
  public static final int EVENT_UNKNOWN = 0;

 
 
  /**
   * A dialog is being shown with Dialog.onShow() event,
   * or an Activity was created with Activity.onCreate()
   */
 
  public static final int EVENT_OPEN = 1;

 
 
  /**
   * A dialog is being dismissed with Dialog.dismiss() event,
   * or an Activity was stopped with Activity.onStop().
   */
 
  public static final int EVENT_CLOSE = 2;

 
 
  /**
   * A dialog is being dismissed with Dialog.dismiss() event,
   * or an Activity was stopped with Activity.onStop().
   * This is called instead of EVENT_CLOSE, when a Dialog or activity is being cancelled.
   */
 
  public static final int EVENT_CANCEL = 3;

 
 
  /**
   * A dialog is being dismissed with Dialog.dismiss() event,
   * or an Activity was stopped with Activity.onStop().
   * This is called instead of EVENT_CLOSE, when a Dialog or activity failed to complete it's task.
   */
 
  public static final int EVENT_ERROR = 4;

 
 
  /**
   * This event will be triggered by an activity.onDestroy event.
   * This event will not occur for dialogs.
   */
 
  public static final int EVENT_DESTROYED = 5;

 
 
  /**
   * This event will be triggered when the login was successful.
   * No instance is attached to this event.
   */
 
  public static final int EVENT_LOGIN = 6;
  
 
 
  /**
   * This event will be triggered when the logout was successful.
   * No instance is attached to this event.
   */
 
  public static final int EVENT_LOGOUT = 7;
  
 
 
  /**
   * This event will be triggered when the user information was successfully retrieved
   * The instance will hold a GreeUser
   */
 
  public static final int EVENT_USERINFO_RETRIEVED = 8;
  
 
 
  /**
   * If you receive an event with CLASS_UNDEFINED type, you should ignore it.
   */
 
  public static final int CLASS_UNDEFINED = 0;

 
 
  /**
   * An event related to the DashboardActivity.java class.
   * You can get an Activity instance by calling params.get(KEY_INSTANCE)
   */
 
  public static final int CLASS_DASHBOARD_ACTIVITY = 1;

 
 
  /**
   * An event related to the NotificationBoard.java class.
   * You can get an Activity instance by calling params.get(KEY_INSTANCE)
   */
 
  public static final int CLASS_NOTIFICATION_BOARD_ACTIVITY = 2;

 
 
  /**
   * Any class type event above this is a dialog,
   * Any class type event this is an activity.
   */
 
  public static final int CLASS_DIALOG_BASE = 100;
  
 
 
  /**
   * An event related to the InviteDialog.java class.
   * You can get a Dialog instance by calling params.get(KEY_INSTANCE)
   */
 
  public static final int CLASS_INVITE_DIALOG = 101;

 
 
  /**
   * An event related to the ShareDialog.java class.
   * You can get a Dialog instance by calling params.get(KEY_INSTANCE)
   */
 
  public static final int CLASS_SHARE_DIALOG = 102;

 
 
  /**
   * An event related to the RequestDialog.java class.
   * You can get a Dialog instance by calling params.get(KEY_INSTANCE)
   */
 
  public static final int CLASS_REQUEST_DIALOG = 103;

 
 
  /**
   * An event related to the login dialog.
   * You can get a Dialog instance by calling params.get(KEY_INSTANCE)
   */
 
  public static final int CLASS_AUTHORIZE_DIALOG = 104;

 
 
  /**
   * An event related to the Payment dialog class.
   * You can get a Dialog instance by calling params.get(KEY_INSTANCE)
   */
 
  public static final int CLASS_PAYMENT_DIALOG = 105;
  
  
 
 
  /**
   * Interface to receive GreePlatform related events.
   * Through this interface you can receive open, close or cancel events, for Gree dialogs or activities.
   * Register your listeners with GreePlatform.registerTaskEventListener.
   * Unregister your listeners with GreePlatform.unRegisterTaskEventListener.
   */
 
  public interface TaskEventListener {

   
   
    /**
     * Called every time an event occurs on the GreePlatform.
     */
   
    void onEvent(Map<String, Object> params);
  }

 
 
  /**
   * Returns the type of the event, as defined in GreePlatformListener.CLASS_*
   * @param params
   * @return an integer representing the class type for this event
   */
 
  public static int getClassType(Map<String, Object> params) {
    if (params != null) {
      return (Integer)params.get(KEY_CLASS_TYPE);
    }
    return CLASS_UNDEFINED;
  }
  
 
 
  /**
   * Returns the type of the event, as defined in GreePlatformListener.EVENT_*
   * @param params
   * @return an integer representing the type of event.
   */
 
  public static int getEventType(Map<String, Object> params) {
    if (params != null) {
      return (Integer)params.get(KEY_EVENT_TYPE);
    }
    return EVENT_UNKNOWN;
  }

}
