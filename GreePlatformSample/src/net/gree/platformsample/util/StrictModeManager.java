/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.util;

import android.annotation.TargetApi;
import android.os.StrictMode;
import android.util.Log;

import net.gree.asdk.core.auth.SetupActivity;

/**
 * Manager of StrictMode
 */
public class StrictModeManager {
  private static final String TAG = StrictModeManager.class.getSimpleName();
  private static final boolean enable = true;

  /**
   * Enable StrictMode threadPolicy. MUST be Called on Main Thread
   */
  @TargetApi(9)
  public static void setStrictModeThreadPolicy() {
    if (enable) {
      try {
        Class
            .forName("android.os.StrictMode", true, Thread.currentThread().getContextClassLoader());
      } catch (Throwable e) {
        Log.e(TAG, "Failed to enable StrictMode ThreadPolicy");
        return;
      }
      StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog()
          .build());
      // .penaltyDeath()
      // remove penaltyDeath on Thread Policy for now.
      // there will error log on this
      // we need try to fix them if the warning is valid
      Log.i(TAG, "Enable StrictMode ThreadPolicy");
    }
  }

  /**
   * Enable StrictMode for VM.
   */
  @TargetApi(9)
  public static void setStrictModeVMPolicy() {
    if (enable) {
      try {
        Class
            .forName("android.os.StrictMode", true, Thread.currentThread().getContextClassLoader());
      } catch (Throwable e) {
        Log.e(TAG, "Failed to enable StrictMode VMPolicy");
        return;
      }
      StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog()
          .build());
      Log.i(TAG, "Enable StrictMode VMPolicy");
    }
  }
}
