/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import java.util.TreeMap;

import net.gree.asdk.api.GreeUser;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class Credential {
  private static final String TAG = Credential.class.getSimpleName();

  /**
   * Interface for listening to results for requesting login
   */
  public interface LoginListener {
    public void onSuccess(int grade);
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Trigger the requests to login by credential.
   * @param mail
   * @param password
   * @param listener
   */
  public void login(String mail, String password, final LoginListener listener) {
    TreeMap<String,Object> params = new TreeMap<String,Object>();
    params.put("format", "json");
    params.put("username", mail);
    params.put("password", password);
    new JsonClient().oauth2(Url.getOAuthAuthorizePassword(), BaseClient.METHOD_POST, BaseClient.toEntityJson(params), false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        Credential.this.onSuccess(responseCode, headers, response, listener);
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  /**
   * Trigger the requests to login by device context.
   * @param userId
   * @param listener
   */
  public void login(String userId, final LoginListener listener) {
    TreeMap<String,Object> params = new TreeMap<String,Object>();
    params.put("format", "json");
    params.put("user_id", userId);
    params.put("context", AuthorizeContext.getUserKey());
    new JsonClient().oauth2(Url.getOAuthAuthorizeContext(), BaseClient.METHOD_POST, BaseClient.toEntityJson(params), false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        Credential.this.onSuccess(responseCode, headers, response, listener);
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  private void onSuccess(int responseCode, HeaderIterator headers, String response, LoginListener listener) {
    try {
      JSONObject json = new JSONObject(response);
      String token = json.getString("oauth_token");
      String secret = json.getString("oauth_token_secret");
      String userId = json.getString("user_id");
      String gradeString = json.optString("user_grade");
      int grade;
      if (TextUtils.isEmpty(gradeString)) {
        grade = GreeUser.USER_GRADE_LITE;
      } else {
        grade = Integer.parseInt(gradeString);
      }
      OAuthStorage storage = new OAuthStorage(Core.getInstance().getContext());
      storage.setToken(token);
      storage.setSecret(secret);
      storage.setUserId(userId);
      Injector.getInstance(IAuthorizer.class).setTokenWithSecret(token, secret);
      if (listener != null) {
        listener.onSuccess(grade);
      }
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(responseCode, headers, response);
      }
    }
  }
}
