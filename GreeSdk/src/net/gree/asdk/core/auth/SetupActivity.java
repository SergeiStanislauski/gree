/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.http.HeaderIterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.GreeUser.GreeUserListener;
import net.gree.asdk.core.util.DeviceInfo;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.Session;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.auth.AuthorizerCore.OnOAuthResponseListener;
import net.gree.asdk.core.auth.OAuthUtil.OnCloseOAuthAlertListener;
import net.gree.asdk.core.auth.UserInfoUpdater.AgreementListener;
import net.gree.asdk.core.auth.pincode.SmsReceiver;
import net.gree.asdk.core.auth.pincode.SmsReceiver.SmsListener;
import net.gree.asdk.core.auth.sso.TokenCommunicater;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.ui.GreeWebView;
import net.gree.asdk.core.ui.WebViewPopupDialog;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Scheme;
import net.gree.asdk.core.util.Url;
import net.gree.asdk.core.util.Util;
import net.gree.oauth.signpost.exception.OAuthCommunicationException;
import net.gree.oauth.signpost.exception.OAuthException;

/**
 * The SetupActivity is used to redirect the user through the authentication flow.
 * It is responsible for showing the login, logout, upgrade and reAuthorize(after a ban) dialogs. 
 */
public class SetupActivity extends FragmentActivity {
  private static final String TAG = "SetupActivity";

  private static final String SETUP_URL = "setupUrl";

  public static final int SHOULD_LOGIN = 1;

  static final String DENIED = "denied";
  static final String QUERY_PARAM_REG_CODE = "reg_code";
  static final String QUERY_PARAM_ENT_CODE = "ent_code";

  private static final int SETUP_DIALOG_ID = 0;

  private static final String RESULT = "result";
  private static final String RESULT_SUCCESS = "succeeded";
  
  private static AtomicBoolean mIsShowingPopUp = new AtomicBoolean(true);
  private boolean mNoPopup = false;
  private Context mOAuthRequestContext = null;
  private boolean mIsReturningFromBack = false;
  private boolean mTokenSuccess = false;

  static SetupListener sSetupListener;
  static CountDownLatch sSignal = null;

  SetupDialog mDialog;
  boolean mInFront = false;
  final Object mUiThreadLock = new Object();
  private SmsReceiver mReceiver;
  private IAuthorizer mAuthorizer;

  private Handler mDialoghandler = null;

  Handler mHandler = new Handler() {
    @Override
    public void handleMessage(Message message) {
      if (!mAuthorizer.hasOAuthAccessToken() && message.what == SHOULD_LOGIN && mDialog != null) {
       mAuthorizer.retrieveRequestToken(mOAuthRequestContext, new OnOAuthResponseListener<String>() {
          public void onSuccess(String response) {
            mAuthorizer.clearRequestTokenParams();
            if (mDialog != null) {
              mDialog.loadUrl(response);
            }
            else {
              if (sSetupListener != null) {
                sSetupListener.onError();
              }
              finish();
            }
          }

          public void onFailure(OAuthException e) {
            WebView view = mDialog != null ? mDialog.mWebView : null;
            WebViewClient client = mDialog != null ? mDialog.getWebViewClient() : null;
            OAuthUtil.handleException(e, SetupActivity.this, view, client, new OnCloseOAuthAlertListener() {
              public void onClose() {
                if (mDialog != null) {
                  mDialog.dismiss(SetupDialog.ABORTED);
                }
                else {
                  if (sSetupListener != null) {
                    sSetupListener.onError();
                  }
                  finish();
                }
              }
            });
            mAuthorizer.clearRequestTokenParams();
          }
        });
      } else {
        if (mDialog != null) {
          mDialog.dismiss(SetupDialog.ABORTED);
        } else {
          if (sSetupListener != null) {
            sSetupListener.onError();
          }
          finish();
        }
      }
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    mAuthorizer = Injector.getInstance(IAuthorizer.class);
    Intent intent = getIntent();
    Uri uri = intent.getData();
    if (uri != null) {
      String url = uri.toString();
      if (url != null && uri.toString().startsWith(Scheme.getAccessTokenScheme())) {
        mNoPopup = true;
        retrieveAccessToken(uri);
        return;
      }
    }
    String setupUrl = intent.getStringExtra(SETUP_URL);
    if (!isValidSetupUrl(setupUrl)) {
      // This Activity should be created by calling SetupActivity.setup(),
      // because unexpected page might be shown if this Activity is reactivated after the application process is killed.
      finish();
      return;
    }

    mReceiver = new SmsReceiver(new SmsListener() {
      public void onReceived(String pincode) {
        if (mDialog != null) {
          mDialog.notifyPinCode(pincode);
        }
      }
    });

    registerReceiver(mReceiver, mReceiver.getIntentFilter());

    requestWindowFeature(Window.FEATURE_NO_TITLE);
    
    // Set it so the redirect only flows to this activity
    setRedirect();
    mOAuthRequestContext = this;
    showDialog(SETUP_DIALOG_ID);
    if (handleReopen(intent)) {
      return;
    }
  }

  boolean isValidSetupUrl(String url) {
    if (url != null) {
      Uri setupUri = Uri.parse(url);
      if (setupUri != null) {
        String scheme = setupUri.getScheme();
        String host = setupUri.getHost();
        if (scheme != null && scheme.startsWith("http") && host != null && Url.isGreeDomain(url)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  protected void onResume() {
    super.onResume();
    synchronized (mUiThreadLock) {
      mInFront = true;
    }
    // checks to see if we are returning from a focus loss
    if(mIsReturningFromBack){
      // checks to see if we are resuming from a state where there is no popup, and that we have not yet obtained a token, 
      // therefore there is no need to do more work
      if(isShowingPopup(getIntent().getStringExtra(SETUP_URL)) && !mTokenSuccess){
        //dismiss the dialog
        handleDialog();
        // finishes the activity, forcing the user to launch a new Activity to setup for login
        finish();
      }
      // resets our global boolean
      mIsReturningFromBack = false;
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    synchronized (mUiThreadLock) {
      mInFront = false;
      if (mDialog != null) {
        mDialog.cancelAuthorization();
        // if we come back it was from a pause or loss of focus state
        mIsReturningFromBack = true;
      }
    }
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  @Override
  protected void onDestroy() {
    TokenCommunicater.clearHandler();
    clearDialogHandlerMessage();
    if (mDialog != null) {
      removeDialog(SETUP_DIALOG_ID);
      mDialog = null;
    }
    if(mDialog == null){
      SetupActivity.this.finish();
    }
    if (mReceiver != null) {
      unregisterReceiver(mReceiver);
    }
    if (mAuthorizer != null) {
      mAuthorizer.clearAccessTokenParams();
      mAuthorizer.clearRequestTokenParams();
    }
    mHandler = null;
    super.onDestroy();
    if (sSignal != null) {
      sSignal.countDown();
      sSignal = null;
    }
  }

  @Override
  protected void onNewIntent(Intent intent) {
    Uri uri = intent.getData();
    if (handleReopen(intent)) { return; }
    retrieveAccessToken(uri);
  }

  /**
   * Sometime, a instance of Message is not recycled.
   * Then, we have to remove a instance of Message in Handler.
   */
  private void clearDialogHandlerMessage() {
    if (mDialoghandler != null) {
      mDialoghandler.removeMessages(SetupDialog.OPENED);
      mDialoghandler.removeMessages(SetupDialog.DONE);
      mDialoghandler.removeMessages(SetupDialog.CANCELLED);
      mDialoghandler.removeMessages(SetupDialog.ABORTED);
      mDialoghandler = null;
    }
  }

  void retrieveAccessToken(Uri uri) {
    if (uri == null) {
      return;
    }
    // Set the value to let our listener know that we received a uri that is not null
    mTokenSuccess = true;
    final String url = uri.toString();
    if (url.startsWith(Scheme.getAccessTokenScheme())) {
      String denied = uri.getQueryParameter(DENIED);
      if (!TextUtils.isEmpty(denied)) {
        GLog.d(TAG, "SSO is denied");
        return;
      }
      if (mDialog == null && mNoPopup) {
        // no dialog, we want to handle it as such
        accessTokenNoDialog(uri, url);
      }
      else if(mDialog == null){
        GLog.e(TAG, "Dialog is not created!!");
        return;
      }
      else{
        // handle dialog based entry
        accessTokenDialog(uri, url);
      }
    }
  }

  /**
   * Handles accessing the authorization token when there is a dialog present
   * @param uri the URI of the toekn
   * @param url the url, obtained from the URI
   */
  private void accessTokenDialog(Uri uri, String url) {
    mDialog.stopLoading();
    mAuthorizer.retrieveAccessToken(this, url, new OnOAuthResponseListener<Void>() {
      public void onSuccess(Void response) {
        if (mDialog != null) {
          mDialog.updateOnBackgroundThread();
        }
        mAuthorizer.clearAccessTokenParams();
        GLog.d(TAG, "OAuth Authorize is done.");
      }

      public void onFailure(final OAuthException e) {
        WebView view = mDialog != null ? mDialog.mWebView : null;
        WebViewClient client = mDialog != null ? mDialog.getWebViewClient() : null;
        OAuthUtil.handleException(e, SetupActivity.this, view, client, new OnCloseOAuthAlertListener() {
          public void onClose() {
            if (mDialog != null) {
              // need to ensure popup window remains if there is a "try again" window we are supposed to see
              if (mDialog.isShowing()) {
                if(!(e instanceof OAuthCommunicationException)){
                  mDialog.dismiss(SetupDialog.ABORTED);
                }
              }else {
                // handles the dialog if it is invisible do to a direct login API call
                mDialog.notifyError();
                // finishes the activity, forcing the user to launch a new Activity to setup for login
                finish();
              }
            }
            else {
              finish();
            }
            mAuthorizer.clearAccessTokenParams();
          }
        });
      }
    });
    
  }

  /**
   * Handles accessing the authorization token when there is not a dialog present
   * @param uri the URI of the toekn
   * @param url the url, obtained from the URI
   */
  public void accessTokenNoDialog(Uri uri, String url) {
    mAuthorizer.retrieveAccessToken(null, url, new OnOAuthResponseListener<Void>() {
      public void onSuccess(Void response) {
        updateInBackground();
        mAuthorizer.clearAccessTokenParams();
        GLog.d(TAG, "OAuth Authorize is done.");
        // restes the popup flag
        mNoPopup = false;
      }

      public void onFailure(final OAuthException e) {
        OAuthUtil.handleException(e, SetupActivity.this, null, null, new OnCloseOAuthAlertListener() {
          public void onClose() {
            if (sSetupListener != null) {
              sSetupListener.onError();
            }
            finish();
            mAuthorizer.clearAccessTokenParams();
          }
        });
      }
    });  
  }
  
  /**
   * After a successful call to authorize via the background thread, we update everything
   */
  public void updateInBackground() {
    new Thread(new Runnable() {
      public void run() {
        sSignal = new CountDownLatch(1);
        AgreementListener listener = new AgreementListener() {
          public void onNeedAgreement(String url) {
            AgreementDialog dialog = new AgreementDialog(SetupActivity.this, url);
            dialog.setOnDismissListener(new OnDismissListener() {
              public void onDismiss(DialogInterface dialog) {
                publishSuccess();
              }
            });
          }
        };
        UserInfoUpdater updater = new UserInfoUpdater(SetupActivity.this).sync(true).listener(listener);
        updater.request();
        publishSuccess();
      }
    }).start();
  }

  /**
   * Looper to notify the SetupListener of success when there is no Dialog present
   */
  public void publishSuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      public void run() {
        if(sSetupListener!=null)
        sSetupListener.onSuccess();
          finish();
        }
    });   
  }
  
  private boolean handleReopen(Intent intent) {
    Uri uri = intent.getData();
    if (uri == null) {
      return false;
    }
    final String url = uri.toString();
    if (url.startsWith(Scheme.getReopenScheme())) {
      String result = uri.getQueryParameter(RESULT);
      if (RESULT_SUCCESS.equals(result)) {
        if (mAuthorizer.hasOAuthAccessToken()) {
          new Session().refreshSessionId(this, new OnResponseCallback<String>() {
            public void onSuccess(int responseCode, HeaderIterator headers, String response) {
              Core.getInstance().updateLocalUser(new GreeUserListener() {
                @Override
                public void onSuccess(int index, int count, GreeUser[] users) {
                  mDialog.reopen(url);
                }

                @Override
                public void onFailure(int responseCode, HeaderIterator headers, String response) {
                  mDialog.reopen(url);
                }
              });
            }

            public void onFailure(int responseCode, HeaderIterator headers, String response) {
              GLog.e(TAG, "refreshSessionId failed in handleReopen.");
              mDialog.dismiss(SetupDialog.ABORTED);
            }
          });
        } else {
          mDialog.startAuthorization(url);
        }
      } else  if (mDialog != null) {
        mDialog.dismiss(SetupDialog.ABORTED);
      }
      return true;
    }
    return false;
  }
  
  /**
   * Sets the redirect value to true so that when a call is made to get the response
   * from a OAuth redirect, it comes here instead of launching the action for the direct 
   * login API
   */
  private void setRedirect(){
    Injector.getInstance(OAuthNormalDao.class).setRedircetToSetupActivity(true);
  }
  
  /**
   * Checks to see if we are using an API call that is using a popup
   * @param url - the address of the URL we want to use for the login
   * @return true if it is one of the URL static values indicating there is no popup being launched, false otherwise
   */
  private boolean isShowingPopup(String url) {
    if(url.equals(Url.getEnterAsLiteUserWithoutUI()) || url.equals(Url.getIdLoginUrl()) ){
      return true;
    }
    return false;
  }
  
  /**
   * Checks to make sure on a close that SetupActivity will not finish without any callback 
   * to close the dialog and in the event of a cancel will notify the the callback listener
   */
  private void handleDialog() {
    if (mDialog != null) {
      if (mDialog.isShowing()) {
      //dismiss the dialog
        mDialog.dismiss(SetupDialog.CANCELLED);
      } else {
        mDialog.notifyCancel();
      }
    }
  }

  /**
   * General interface for listening to results
   */
  public interface SetupListener {
    /**
     * Called when the action succeeded.
     */
    void onSuccess();

    /**
     * Called when the action was cancelled by the user.
     */
    void onCancel();

    /**
     * Called when the action failed
     */
    void onError();
  }

  /**
   * Start this activity.
   * you must call one of this setup method,
   * because unexpected pages might be shown if this Activity is reactivated after the application process is killed.
   * @param context the current application context
   * @param url the url to be loaded in the Dialog's webview
   * @param listener a successlistener to receive the results
   */
  public static void setup(Context context, String url, SetupListener listener) {
    setup(context, new Intent(context, SetupActivity.class), url, listener);
  }

  /**
   * Start this activity as a new task,
   * (the new task flag should be used when you are calling this from a non activity context).
   * you must call one of this setup method,
   * because unexpected pages might be shown if this Activity is reactivated after the application process is killed.
   * @param context the current application context
   * @param url the url to be loaded in the Dialog's webview
   * @param listener a successListener to receive the results
   */
  public static void setupNewTask(Context context, String url, SetupListener listener) {
    Intent intent = new Intent(context, SetupActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    setup(context, intent, url, listener);
  }

  protected static void setup(final Context context, final Intent intent, final String url, final SetupListener listener) {
    if (url != null && (url.startsWith(Url.getIdTopUrl()) || url.startsWith(Url.getIdLoginUrl()) 
        || url.startsWith(Url.getEnterAsLiteUserWithoutUI()) ) 
        && Injector.getInstance(IAuthorizer.class).hasOAuthAccessToken()) {
      AgreementListener agreementListener = new AgreementListener() {
        public void onNeedAgreement(String url) {
//          new AgreementDialog(context, url).show();
        }
      };
      new UserInfoUpdater(context).listener(agreementListener).request();
      // If the user has already logged in, application can proceed without waiting for result of updating user info.
      if (listener != null) {
        listener.onSuccess();
      }
      return;
    }
    new Thread(new Runnable() {
      public void run() {
        if (sSignal != null) {
          try {
            sSignal.await();
          } catch (InterruptedException e) {
            GLog.printStackTrace(TAG, e);
          }
        }

        intent.putExtra(SETUP_URL, url);
        sSetupListener = listener;

        context.startActivity(intent);
      }
    }).start();
  }

  public static void requestAuthorization(Context context, String authorizeUrl, String packagename, SetupListener listener, Handler handler) {
    sSetupListener = listener;
    TokenCommunicater.sendAuthRequest(context, authorizeUrl, packagename, handler);
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    switch (id) {
      case SETUP_DIALOG_ID:
        if (mDialog != null) {
          break;
        }
        String url = getIntent().getStringExtra(SETUP_URL);
        // case in which we want to launch a TOS window, do it in a web browser or it will crash the app
        if (url != null && url.startsWith(Url.getTosUrl(CoreData.get(InternalSettings.ApplicationId)).replace("https:", "http:"))){
          Intent i = new Intent(Intent.ACTION_VIEW);
          i.setData(Uri.parse(url));
          startActivity(i);
          // on return we set the URL for a login
          url = Url.getIdLoginUrl();
        }
        
        mDialog = new SetupDialog(this, url);

        mDialoghandler = new Handler() {
          public void handleMessage(Message message) {
            GLog.d(TAG, "Call SetupDialogHandler: event:" + message.what);
            switch (message.what) {
              case SetupDialog.OPENED:
                break;
              case SetupDialog.DONE:
                if(mDialog != null ){
                  mDialog.notifySuccess();
                  finish();
                }
                break;
              case SetupDialog.ABORTED:
                if(mDialog != null ){
                  mDialog.notifyError();
                  finish();
                }
                break;
              case SetupDialog.CANCELLED:
                if(mDialog != null ){
                  mDialog.notifyCancel();
                  finish();
                }
                break;
              default:
                break;
            }
          }
        };
        mDialog.setHandler(mDialoghandler);
        break;
      default:
        break;
    }
      if(!mIsShowingPopUp.get()){
        mIsShowingPopUp.set(true);
        return null;
      }

      return mDialog;  
  }

  protected boolean needDismissButton(String url) {
    if (url.startsWith(Url.getConfirmUpgradeUserUrl())
        || url.equals(Url.getConfirmReauthorizeUrl())) {
      return true;
    }
    return false;
  }

  /**
   * Before dialog.show is called check if, for this url, a uuid is needed or not.
   * 
   * @param url
   * @return true if a UUID is needed, false otherwise
   */
  protected boolean needUuid(String url) {
    if (url.startsWith(Url.getIdTopUrl()) || url.startsWith(Url.getIdLoginUrl()) ||
        url.startsWith(Url.getConfirmUpgradeUserUrl())) {
      return true;
    }
    return false;
  }

  /**
   * An implementation of a webviewPopupDialog,
   * used to display login, logout, upgrade, agreement or re-authorize.
   */
  public class SetupDialog extends WebViewPopupDialog {
    public static final int OPENED = 1;
    public static final int DONE = 2;
    public static final int ABORTED = 3;
    public static final int CANCELLED = 4;

    static final String TARGET = "target";
    static final String TARGET_SELF = "self";
    static final String TARGET_BROWSER = "browser";
    static final String TARGET_APPS = "apps";

    GreeWebView mWebView;
    JSONObject mAppList = null;
    Verifier mVerifier = null;
    String mUserKey = null;
    int mVerifyerType;
    private PopupDialogWebViewClient mWebViewClient;
    private SetupListener mListener = null;

    String mCheckpointUrl;
    private String mUrl;
    String getmUrl() {
      return mUrl;
    }
    private int mAuthClosedEvent = OPENED;

    boolean mIsAvailableDismissButton = false;
    boolean mNeedResetDismissButton = false;
    PerformanceData mOpenPerformData;

    private String mReloginUrl = null;

    /**
     * Initialize the dialog webview to the given url.
     * @param context the current application context
     * @param url the url to be loaded first on the popup's webview.
     */
    public SetupDialog(Context context, String url) {
      super(context);

      if(url != null && url.startsWith(Url.getIdLoginUrl())){
        mPerformData = mManager.createData(PerformanceFlowIndex.LOGIN);
        // forces the authorization so that when we login we have an auth key, etc. 
        // and brings the user right to the login screen
        forceAuth();
      }
      else if (url != null && url.startsWith(Url.getIdTopUrl())) {
        mPerformData = mManager.createData(PerformanceFlowIndex.LOGIN);
      } 
      else if (url != null && url.startsWith(Url.getLogoutUrl())) {
        mPerformData = mManager.createData(PerformanceFlowIndex.LOGOUT);
      } else if (url != null && url.startsWith(Url.getUpgradeUserUrl())) {
        mPerformData = mManager.createData(PerformanceFlowIndex.UPGRADE);
      } else if (url != null && url.contains("action=relogin")) {
        mPerformData = mManager.createData(PerformanceFlowIndex.RELOGIN);
      }

      //set the dialog type for TaskEventListeners
      mClassType = GreePlatformListener.CLASS_AUTHORIZE_DIALOG;

      mListener = sSetupListener;
      sSetupListener = null;

      mUrl = url != null ? url : "";
      
      mWebView = getWebView(); 
      initWebView();

      if (DeviceInfo.getUuid() != null) {
        mUserKey = AuthorizeContext.getUserKey();
      }
      mIsAvailableDismissButton = needDismissButton(mUrl);
      switchDismissButton(mIsAvailableDismissButton);
    }

    /**
     * Causes the user to skip right to the auth screen, ignoring the login top window
     * @param setupDialog the the dialog box holding the info we want. Will dismiss it if we aren't using a popup box
     */
    private void forceAuth() {
      // Build the auth String
      StringBuilder auth = new StringBuilder();
      
      auth.append(Scheme.getStartAuthorizationScheme());
      auth.append("?target=");
      auth.append("app");//self");
      mIsShowingPopUp.set(false);
      
      // starts the Authorization
      ((SetupDialogWebViewClient)mWebViewClient).getAppListAndRequestAuthorize(auth.toString());
    }

    /**
    * Dismiss the dialog with the given reason.
    * @param event one of DONE, ABORTED or CANCELLED
    */
    public void dismiss(int event){
      //Set the close result for TaskEventListener
      if (event == DONE) {
        mCloseEvent = GreePlatformListener.EVENT_CLOSE;
      } else if (event == ABORTED) {
        mCloseEvent = GreePlatformListener.EVENT_ERROR;
      } else if (event == CANCELLED) {
        mCloseEvent = GreePlatformListener.EVENT_CANCEL;
      }
      mAuthClosedEvent = event;
      super.dismiss();
    }

    @Override
    public void dismiss() {
      dismiss(CANCELLED);
    }

    private void initWebView() {
      mWebView.addJavascriptInterface(new Object() {
        @SuppressWarnings("unused")
        public void inputPhoneNumber() {
          SetupActivity.this.runOnUiThread(new Runnable() {
            public void run() {
              String telno =
                  ((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getLine1Number();
              mWebView.loadUrl("javascript:document.getElementById('telno').value = '" + telno
                  + "'");
            }
          });
        }
      }, "GgpSDK");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
      if ((keyCode == KeyEvent.KEYCODE_BACK)) {
        if (mWebView.canGoBack()) {
          mWebView.goBack();
          return true;
        } else if (!mIsAvailableDismissButton) {
          // ignore back key
          return true;
        } else {
          dismiss(CANCELLED);
          return true;
        }
      }
      return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void reloadWebView() {
      if (needUuid(mUrl) && updateUuid(mUrl)) {
        return;
      }
      if (needRequestToken(mUrl) && prepareRequestToken()) {
        return;
      }
      mUrl = AuthorizeContext.appendQueryParameter(mUrl, mUserKey);
      super.reloadWebView();
    }

    private boolean updateUuid(final String url) {
      if (DeviceInfo.getUuid() == null) {
        DeviceInfo.updateUuid(new OnResponseCallback<String>() {
          public void onSuccess(int responseCode, HeaderIterator headers, String response) {
            mUserKey = AuthorizeContext.getUserKey();
            reloadWebView();
          }

          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            GLog.e(TAG, response);
            getWebViewClient().onReceivedError(mWebView, 0, null, url);
            mWebView.clearHistory();
          }
        });
        return true;
      }
      return false;
    }

    protected boolean needRequestToken(String url) {
      if (url != null && url.startsWith(Url.getLogoutCompleteUrl())) {
        return true;
      }
      return false;
    }

    private boolean prepareRequestToken() {
      if (mReloginUrl == null) {
        mAuthorizer.retrieveRequestToken(mOAuthRequestContext, new OnOAuthResponseListener<String>() {
          public void onSuccess(String response) {
            StringBuilder reloginUrl = new StringBuilder(response);
            reloginUrl.append("&force_logout=1&forward_type=login");
            mReloginUrl = reloginUrl.toString();
            reloadWebView();
          }

          public void onFailure(OAuthException e) {
            OAuthUtil.handleException(e, mContext, mWebView, getWebViewClient(), new OnCloseOAuthAlertListener() {
              public void onClose() {
                dismiss(SetupDialog.ABORTED);
              }
            });
          }
        });
        return true;
      }
      return false;
    }
    /**
     * Start loading a url in the popup dialog webview
     * @param url the url to be loaded
     */
    public void loadUrl(String url) {
      mWebView.loadUrl(url);
    }

    /**
     * Stop the loading of current url
     */
    public void stopLoading() {
      mWebView.stopLoading();
    }

   public void enterAsLiteUser(String url) {
    /**
     * This method is called when the user clicks on the "Play as guest" button
     * It will generate a call to the server with the received "enter code" and "reg code"
     * (both of them are received from the server and are in the given url) 
     * It also attaches the user key which is a uuid.
     * @param url the "play as Guest" url received from the server
     */
      Uri uri = Uri.parse(url);
      StringBuilder enterUrl = new StringBuilder(Url.getEnterAsLiteUser());
      String regCode = uri.getQueryParameter(QUERY_PARAM_REG_CODE);
      if (regCode != null) {
        enterUrl.append('&').append(QUERY_PARAM_REG_CODE).append('=').append(regCode);
      }
      String entCode = uri.getQueryParameter(QUERY_PARAM_ENT_CODE);
      if (entCode != null) {
        enterUrl.append('&').append(QUERY_PARAM_ENT_CODE).append('=').append(entCode);
      }
      mWebView.loadUrl(AuthorizeContext.appendQueryParameter(enterUrl.toString(), mUserKey));
    }

    void cancelAuthorization() {
      if (mVerifier == null) {
        return;
      }
      mVerifier.cancelRequest(mVerifyerType);
    }

    public void startAuthorization(final String url) {
      if (mAuthClosedEvent != OPENED){
        return;
      }

      mAuthorizer.retrieveRequestToken(mOAuthRequestContext, new OnOAuthResponseListener<String>() {
        public void onSuccess(String response) {
          boolean ret = false;
          try {
            JSONArray applist = null;
            Uri uri = Uri.parse(url);
            StringBuilder authorizeUrl = new StringBuilder(response);
            String regCode = uri.getQueryParameter(QUERY_PARAM_REG_CODE);
            if (regCode != null) {
              authorizeUrl.append('&').append(QUERY_PARAM_REG_CODE).append('=').append(regCode);
            }
            String entCode = uri.getQueryParameter(QUERY_PARAM_ENT_CODE);
            if (entCode != null) {
              authorizeUrl.append('&').append(QUERY_PARAM_ENT_CODE).append('=').append(entCode);
            }
            mCheckpointUrl = authorizeUrl.toString();
            String target = uri.getQueryParameter(TARGET);
            if (target != null) {
              if (target.startsWith(TARGET_APPS)) {
                mVerifyerType = Verifier.REQUEST_TYPE_SEARCH_APP;
                applist = mAppList.getJSONArray("entry");
              } else if (target.startsWith(TARGET_SELF)) {
                mVerifyerType = Verifier.REQUEST_TYPE_LITE;
              } else {
                mVerifyerType = Verifier.REQUEST_TYPE_BROWSER;
              }
            } else {
              mVerifyerType = Verifier.REQUEST_TYPE_BROWSER;
            }
            mVerifier = new Verifier(getContext(), mCheckpointUrl, mWebView, applist, mHandler);
            ret = mVerifier.request(mVerifyerType);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          } finally {
            if (!ret) {
              dismiss(ABORTED);
            }
          }
          mAuthorizer.clearRequestTokenParams();
        }

        public void onFailure(OAuthException e) {
          OAuthUtil.handleException(e, getContext(), mWebView, getWebViewClient(), new OnCloseOAuthAlertListener() {
            public void onClose() {
              dismiss(ABORTED);
            }
          });
          mAuthorizer.clearRequestTokenParams();
        }
      });
    }

    void upgrade(String url) {
      boolean ret = false;
      Uri uri = Uri.parse(url);
      String target = uri.getQueryParameter(TARGET);
      int type;
      if (target.startsWith(TARGET_SELF)) {
        type = Upgrader.REQUEST_TYPE_INTERNAL_WEBVIEW;
      } else if (target.startsWith(TARGET_BROWSER)) {
        type = Upgrader.REQUEST_TYPE_BROWSER;
      } else {
        type = Upgrader.REQUEST_TYPE_BROWSER;
      }
      Upgrader upgrader = new Upgrader(getContext(), url, mWebView, mUserKey);
      ret = upgrader.request(type);
      if (!ret) {
        dismiss(ABORTED);
      }
    }

    void reopen(String url) {
      if (mAuthorizer.hasOAuthAccessToken()) {
        updateOnBackgroundThread();
      } else {
        startAuthorization(Scheme.getStartAuthorizationScheme() + "?target=" + Uri.parse(url).getQueryParameter(TARGET));
      }
    }

    void updateOnBackgroundThread() {
      new Thread(new Runnable() {
        public void run() {
          sSignal = new CountDownLatch(1);
          AgreementListener listener = new AgreementListener() {
            public void onNeedAgreement(String url) {
              AgreementDialog dialog = new AgreementDialog(getContext(), url);
              dialog.setOnDismissListener(new OnDismissListener() {
                public void onDismiss(DialogInterface dialog) {
                  postSuccessNotification();
                }
              });
//              dialog.show();
            }
          };
          UserInfoUpdater updater = new UserInfoUpdater(getContext()).sync(true).listener(listener);
          updater.request();
//          if (updater.getAgreementUrl() == null) {
            postSuccessNotification();
//          }
        }
      }).start();
    }

    void postSuccessNotification() {
      new Handler(Looper.getMainLooper()).post(new Runnable() {
        public void run() {
          if (isShowing()) {
            dismiss(DONE);
          } else {
            notifySuccess();
            finish();
          }
        }
      });
    }

    /**
     * The pin code received by SMS is forwarded to a custom page,
     * the pin code fields are replaced by the 4 values.
     * (This is to validate the user grade upgrade)
     * @param code the pin code received by SMS as a 4 characters string
     */
    public void notifyPinCode(final String code) {
      SetupActivity.this.runOnUiThread(new Runnable() {
        public void run() {
          inputPinCodeChar("pin1", code.charAt(0));
          inputPinCodeChar("pin2", code.charAt(1));
          inputPinCodeChar("pin3", code.charAt(2));
          inputPinCodeChar("pin4", code.charAt(3));
        }
      });
    }

    /**
     * Fill a field on the WebView with given character value
     * @param id the field Id to be set
     * @param code the character to be used as value
     */
    void inputPinCodeChar(String id, char code) {
      mWebView.loadUrl("javascript:document.getElementById('" + id + "').value = '" + code + "'");
    }

    /**
     * this is the custom WebViewClient to be used by the popup dialog.
     * it allows redirecting the user when clicking url. 
     */
    protected class SetupDialogWebViewClient extends PopupDialogWebViewClient {
      private Message mDontResend;

      public SetupDialogWebViewClient(Context context) {
        super(context);
      }

      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        if (url != null && url.startsWith(Url.getOauthAuthorizeEndpoint())) {
          mOpenPerformData = mManager.createData(PerformanceFlowIndex.OPEN);
          mManager.recordData(mOpenPerformData, PerformanceIndexMap.INDEX_KEY_AUTHORIZE_URL_LOAD_START);
        }
        super.onPageStarted(view, url, favicon);
        if (url != null && (url.startsWith(Url.getIdTopUrl()) || url.startsWith(Url.getIdLoginUrl()) || url.startsWith(Url.getEnterAsLiteUser())) ) {
          mIsAvailableDismissButton = false;
          switchDismissButton(false);
        }
      }

      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        url = AuthorizeContext.appendQueryParameter(url, mUserKey);
        if (handleEnter(view, url)) {
          return true;
        } else if (handleSsoRequire(view, url)) {
          return true;
        } else if (handleStartAuthorization(view, url)) {
          return true;
        } else if (handleUpgrade(url)) {
          return true;
        } else if (handleLogin(view, url)) {
          return true;
        } else if (handleLogout(url)) {
          return true;
        } else if (handleOAuthCallback(url)) {
          return true;
        }
        return super.shouldOverrideUrlLoading(view, url);
      }

      @Override
      public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        if (mOpenPerformData != null) {
          mManager.recordData(mOpenPerformData, PerformanceIndexMap.INDEX_KEY_AUTHORIZE_URL_LOAD_ERROR);
          mManager.flushData(mOpenPerformData);
        }
        if (!TextUtils.isEmpty(mCheckpointUrl)) {
          failingUrl = mCheckpointUrl;
        }
        if (Util.canOptOutOfGREE() && !mIsAvailableDismissButton) {
          mNeedResetDismissButton = true;
          switchDismissButton(true);
        }
        super.onReceivedError(view, errorCode, description, failingUrl);
      }

      @Override
      public void onFormResubmission(WebView view, Message dontResend, Message resend) {
        if (mDontResend != null) {
          mDontResend.sendToTarget();
          return;
        }
        mDontResend = dontResend;
        if (resend != null) {
          resend.sendToTarget();
          mDontResend = null;
        }
      }

      @Override
      public void onPageFinished(WebView view, String url) {
        if (mOpenPerformData != null) {
          mManager.recordData(mOpenPerformData, PerformanceIndexMap.INDEX_KEY_AUTHORIZE_URL_LOAD_END);
          mManager.flushData(mOpenPerformData);
        }
        super.onPageFinished(view, url);
        if (mNeedResetDismissButton && Util.isNetworkConnected(getContext())) {
          switchDismissButton(false);
          mNeedResetDismissButton = false;
        }
        if (handleLogoutComplete(url)) {
          return;
        }
      }
      
      public void getAppListAndRequestAuthorize(final String url) {
        OAuthUtil.requestAppList(mUserKey, new OnResponseCallback<String>() {
          public void onSuccess(int responseCode, HeaderIterator headers, String response) {
            try {
              GLog.d(TAG, "app list = " + response);
              mAppList = new JSONObject(response);
              synchronized (mUiThreadLock) {
                if (mInFront) {
                  startAuthorization(url);
                }
              }
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }

          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            StringBuilder startAuthorize = new StringBuilder(Scheme.getStartAuthorizationScheme()).append("?target=self");
            synchronized (mUiThreadLock) {
              if (mInFront) {
                startAuthorization(startAuthorize.toString());
              }
            }
          }
        });   
      }

      /**
       * This is called to check if the user has clicked on the url "Play as guest"
       * 
       * @param webview the current webview
       * @param url the url the user clicked on
       * @return true if the url matches a "play as guest" url, false otherwise
       */
      private boolean handleEnter(WebView webview, String url) {
        if (url.startsWith(Scheme.getEnterScheme())) {
          enterAsLiteUser(url);
          return true;
        }
        return false;
      }

      /**
       * This is called to check if the url is a redirection to the single sign on application.
       * 
       * @param view the current webview
       * @param url the url the user clicked on
       * @return true if the given url is a link to the sso application.
       */
      private boolean handleSsoRequire(WebView view, String url) {
        if (url.startsWith(Scheme.getSsoRequireScheme())) {
          Uri uri = Uri.parse(url);
          String target = uri.getQueryParameter(TARGET);
          String regCode = uri.getQueryParameter(QUERY_PARAM_REG_CODE);
          String entCode = uri.getQueryParameter(QUERY_PARAM_ENT_CODE);
          StringBuilder startAuthorize = new StringBuilder(Scheme.getStartAuthorizationScheme()).append("?target=");
          if (TextUtils.isEmpty(target)) {
            startAuthorize.append(TARGET_APPS);
          } else {
            startAuthorize.append(target);
          }
          if (regCode != null) {
            startAuthorize.append('&').append(QUERY_PARAM_REG_CODE).append('=').append(regCode);
          }
          if (entCode != null) {
            startAuthorize.append('&').append(QUERY_PARAM_ENT_CODE).append('=').append(entCode);
          }
          getAppListAndRequestAuthorize(startAuthorize.toString());
          
          return true;
        }
        return false;
      }

      /**
       * This will start the authorization process.
       * it will redirect the user towards the login dialog
       * 
       * @param view the current webview
       * @param url the url the user has clicked on
       * @return true to start the authorization sequence, false otherwise
       */
      private boolean handleStartAuthorization(WebView view, String url) {
        if (url.startsWith(Scheme.getStartAuthorizationScheme())) {
          startAuthorization(url);
          return true;
        }
        return false;
      }

      /**
       * This is called to check if the user as clicked on the upgrade button
       * @param url the url the user has clicked on
       * @return true if it is the upgrade url
       */
      private boolean handleUpgrade(String url) {
        if (url.startsWith(Scheme.getUpgradeScheme())) {
          upgrade(url);
          return true;
        }
        return false;
      }

      /**
       * Check if the user has clicked on the login button
       * @param view current webview
       * @param url the url the user has clicked on
       * @return true if it is a login url.
       */
      private boolean handleLogin(WebView view, String url) {
        if (url.contains(Url.getRootFqdn()) && url.contains("action=login")) {
          view.loadUrl(url);
          return true;
        }
        return false;
      }

      private boolean handleOAuthCallback(String url) {
        if (url.startsWith(Scheme.getAccessTokenScheme())) {
          Uri uri = Uri.parse(url);
          retrieveAccessToken(uri);
          return true;
        }
        return false;
      }

      private boolean handleLogoutComplete(String url) {
        if (url.startsWith(Url.getLogoutCompleteUrl())) {
          Util.startBrowser(mContext, mReloginUrl);
          return true;
        }
        return false;
      }

      /**
       * Check if the user has clicked on the logout button,
       * and do the logout.
       * @param view current webview
       * @param url the url the user has clicked on
       * @return true if it is a logout url.
       */
      private boolean handleLogout(String url) {
        if (url.startsWith(Scheme.getLogoutScheme())) {
          SetupActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
              if (isShowing()) {
                mAuthorizer.logout();
                sSignal = new CountDownLatch(1);
                dismiss(DONE);
              }
            }
          });
          return true;
        }
        return false;
      }

      @Override
      protected void onDialogClose(String url) { }
    }

    @Override
    protected void createWebViewClient() {
      mWebViewClient = new SetupDialogWebViewClient(getContext());
    }

    @Override
    protected PopupDialogWebViewClient getWebViewClient() {
      return mWebViewClient;
    }

    @Override
    protected int getOpenedEvent() {
      return OPENED;
    }

    @Override
    protected int getClosedEvent() {
      return mAuthClosedEvent;
    }

    @Override
    protected int getCancelEvent() {
      return CANCELLED;
    }

    @Override
    protected String getEndPoint() {
      return mUrl;
    }

    void notifySuccess() {
      if (mListener != null) {
        mListener.onSuccess();
      }
    }

    void notifyCancel() {
      if (mListener != null) {
        mListener.onCancel();
      }
    }

    void notifyError() {
      if (mListener != null) {
        mListener.onError();
      }
    }
  }
}
