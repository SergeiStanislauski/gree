/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.util;

/**
 * A simple tool for checking Null references,
 */
public class Preconditions {

  /**
   * Instance isn't necessary.
   */
  private Preconditions() {}

  /**
   * Check if not null
   * 
   * @param reference Object to check not null
   * @return Object.
   * @throws NullPointerException if null
   */
  public static <T> T checkNotNull(T reference) {
    if (null == reference) {
      throw new NullPointerException();
    }
    return reference;
  }

  /**
   * Check if not null
   * 
   * @param reference Object to check not null
   * @param message Throw message if reference is null
   * @return Object
   * @throws NullPointerException If null
   */
  public static <T> T checkNotNull(T reference, String message) {
    if (null == reference) {
      throw new NullPointerException(message);
    }
    return reference;
  }
  
  
  /**
   * Check if not null for argument, otherwise throw IllegalArgument IllegalArgumentException
   * 
   * @param reference Object to check not null
   * @param message Throw message if reference is null
   * @return Object
   * @throws NullPointerException If null
   */
  public static <T> void checkArgument(T reference, String message) {
    if (null == reference) {
      throw new IllegalArgumentException(message);
    }
  }
  

  /**
   * Check if not empty string
   * 
   * @param string String to check not empty
   * @return String.
   * @throws IllegalArgumentException If empty
   */
  public static String checkNotEmpty(String string) {
    if (null == string || string.trim().length() == 0) {
      throw new IllegalArgumentException();
    }
    return string;
  }

  /**
   * Check if not empty string
   * 
   * @param string String to check not empty
   * @param message Throw message if reference is null
   * @return String.
   * @throws IllegalArgumentException If empty
   */
  public static String checkNotEmpty(String string, String message) {
    if (null == string || string.trim().length() == 0) {
      throw new IllegalArgumentException(message);
    }
    return string;
  }
}
