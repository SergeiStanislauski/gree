/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.gree.platformsample.R;

public class HelpDetailsFragment extends Fragment {
  private static final String TAG = "HelpDetailsFragment";

  /**
   * Create a new instance of HelpDetailsFragment, initialized to
   * show the details page for 'name' and to store what index this represents in the list
   * Name should be the string resource for the title of the page you want to open e.g. R.string.leaderboard_title. 
   * To open the root page use "" as the input string. 
   * @param index Index this information represents in the list
   * @param name Name of the item that called
   * @return f New fragment instance
   */
  public static HelpDetailsFragment newInstance(int index, String name) {
      HelpDetailsFragment f = new HelpDetailsFragment();
      Bundle args = new Bundle();
      args.putInt("INDEX", index);
      args.putString("ITEM", name);
      f.setArguments(args);
      return f;
  }

  /**
   * 
   * @return The "INDEX" argument stored when this instance was created
   */
  public int getShownIndex() {
      return getArguments().getInt("INDEX", 0);
  }

  /**
   * 
   * @return The "ITEM" argument stored when this instance was created
   */
  public String getName(){
    return getArguments().getString("ITEM");
  }
  
  /**
   * Create the view for this fragment
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
          Bundle savedInstanceState) {
    String[] array;
    View view = inflater.inflate(R.layout.help_details, null);
    
    array = detailsPage(); 
    
    //The string builder for the html details
    SpannableStringBuilder htmlConcat = new SpannableStringBuilder();
    
    //Set the text for the TextViews that correspond to certain items in the string array for this details page
    for (int i = 0 ; i < array.length ; i++){
      if (i == 0){
        ((TextView)view.findViewById(R.id.help_details_title)).setText(array[i]);
      } else if (i == 1){
        ((TextView)view.findViewById(R.id.help_details_description)).setText(array[i]);
      } else if (i==2){
        ((TextView)view.findViewById(R.id.help_details_note)).setText(array[i]);
      } else{
        htmlConcat.append(Html.fromHtml(array[i]));  
        if (i == array.length -1){
          ((TextView)view.findViewById(R.id.help_details_other)).setText(htmlConcat);
        }
      }
    }
    
    return view;
  }
  
  @Override
  public void onResume(){
    super.onResume();

    // Check if the details page is shown in the left pane
    HelpDetailsFragment details = (HelpDetailsFragment) getFragmentManager().findFragmentByTag("help_details");
    
    //Also check if the right pane is visible
    View detailsFrame = getActivity().findViewById(R.id.help_details_layout);
    boolean rightPane = (detailsFrame != null) && (detailsFrame.getVisibility() == View.VISIBLE);
    
    //Pop the details pane off the backstack if it is shown in the left pane
    //and the right pane is visible (case for rotating from portrait to landscape)
    if (details != null && rightPane){
      Log.d("zech","details already exists remove this ish");
      getFragmentManager().popBackStack("details", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
  }
    
  /**
   * The details page information to show
   * @return
   */
  public String[] detailsPage(){
    Resources res = getResources();
    
    String pageName = getName();
    // If the name we are looking for is equal to one of these title strings
    // then return the string array containing the details for that item
    // default: user info details
    if (pageName.equals(res.getString(R.string.leaderboard_title))) {
      return res.getStringArray(R.array.leaderboard_details);
    } else if (pageName.equals(res.getString(R.string.achievement_title))) {
      return res.getStringArray(R.array.achievements_details);
    } else if (pageName.equals(res.getString(R.string.share_title))) {
      return res.getStringArray(R.array.share_details);
    } else if (pageName.equals(res.getString(R.string.request_title))) {
      return res.getStringArray(R.array.requests_details);
    } else if (pageName.equals(res.getString(R.string.invite_title))) {
      return res.getStringArray(R.array.invites_details);
    } else if (pageName.equals(res.getString(R.string.friend_title))) {
      return res.getStringArray(R.array.friends_details);
    } else if (pageName.equals(res.getString(R.string.payment_title))) {
      return res.getStringArray(R.array.payment_details);
    } else if (pageName.equals(res.getString(R.string.gltest_title))) {
      return res.getStringArray(R.array.gltest_details);
    } else if (pageName.equals(res.getString(R.string.upgrade_title))) {
      return res.getStringArray(R.array.upgrade_details);
    } else if (pageName.equals(res.getString(R.string.friendcode_title))) {
      return res.getStringArray(R.array.friendcode_details);
    } else if (pageName.equals(res.getString(R.string.incentive_title))) {
      return res.getStringArray(R.array.incentive_details);
    } else {
      return res.getStringArray(R.array.userinfo_details);
    }

  }
}
