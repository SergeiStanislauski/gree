/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.dashboard.PullToRefreshWebView.OnRefreshAfterScreenShotListener;
import net.gree.asdk.core.ui.CommandInterface;
import net.gree.asdk.core.ui.CommandInterfaceWebView;
import net.gree.asdk.core.ui.CommandInterfaceWebViewClient;
import net.gree.asdk.core.util.Url;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Modal Activity with a CommandInterfaceWebView as its content view. The content view is
 * refreshable with PullToRefresh functionality, which is disabled by default. This class does not
 * support refreshing on locale and/or user change without reloading the content of its WebView.
 * 
 */
public class ModalActivity extends Activity implements LocationListener{
  public static final int TYPE_DEFAULT = 0;
  public static final int TYPE_LOCATION = 1;
  public static final String EXTRA_TYPE = "ModalActivityType";
  private static final float GPS_ACCURACY_LIMIT = 100;
  private static final long GPS_TIMEOUT = 10000;
  private static final long TIMER_PERIOD = 1000;
  private LocationManager mLocationManager;
  private Float bestAccuracy_ = null;
  private double bestLng_;
  private double bestLat_;
  private boolean isLocationDone_ = false;
  private long locationBeginTime_;
  private Timer timer_;

  public static final String EXTRA_BASE_URL = "base_url";
  private static final boolean PULL2REFRESH_DEFAULT_AVAILABILITY= false;
  private Handler uiThreadHandler_ = new Handler();
  private String viewName_ = null;
  private CommandInterface commandInterface_ = null;
  private Button button_;
  private Button cancelButton_;
  private JSONObject openViewParams_ = null;
  private ProgressBar mLoadingIndicator = null;
  private static final String TAG = "ModalActivity";
  private PullToRefreshWebView mPullToRefreshWebView = null;

  private OnClickListener onCancelClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      finish();
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    setTheme(RR.style("GreeDashboardViewTheme"));
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(RR.layout("gree_modal_view"));

    String titleString;
    String buttonString;
    final String nsString;
    final String methodString;

    Intent data = getIntent();
    String paramsString = data.getStringExtra("params");
    if (null != paramsString) {
    try {
      JSONObject params = new JSONObject(paramsString);
      viewName_ = getString(params, "view");
        titleString = getString(params, "title");
        buttonString = getString(params,"button");
        nsString = getString(params, "ns");
        methodString = getString(params, "method");
        openViewParams_ = params;
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
        return;
      }
    } else {
      viewName_ = data.getStringExtra("view");
      titleString = data.getStringExtra("title");
      buttonString = data.getStringExtra("button");
      nsString = data.getStringExtra("ns");
      methodString = data.getStringExtra("method");
      openViewParams_ = new JSONObject();
    }

      if (viewName_ == null) {
        finish();
      }

      if (titleString != null) {
        TextView title = (TextView)findViewById(RR.id("gree_title"));
        title.setText(titleString);
      }

      button_ = (Button) findViewById(RR.id("gree_postButton"));
      cancelButton_ = (Button) findViewById(RR.id("gree_cancelButton"));
      if (buttonString != null && buttonString.length() > 0) {
        button_.setText(buttonString);
        button_.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
            showLoadingIndicator();
            if (nsString != null && methodString != null ) {
              commandInterface_.evaluateJavascript(nsString + "." + methodString + "();");
            } else {
              finish();
            }
          }
        });
        cancelButton_.setOnClickListener(onCancelClickListener);
      } else {
        cancelButton_.setVisibility(View.GONE);
        button_.setText(RR.string("gree_button_cancel"));
        button_.setOnClickListener(onCancelClickListener);
      }

      mPullToRefreshWebView = new PullToRefreshWebView(this);
      mPullToRefreshWebView.setPullToRefreshEnabled(PULL2REFRESH_DEFAULT_AVAILABILITY);
      mPullToRefreshWebView.setDisableScrollingWhileRefreshing(false);
      mPullToRefreshWebView.setOnRefreshAfterScreenShotListener(new OnRefreshAfterScreenShotListener() {
        @Override
        public void onRefreshAfterScreenShot() {
          showLoadingIndicator();
          commandInterface_.refresh();
        }
      });

      CommandInterfaceWebView web = mPullToRefreshWebView.getCommandInterfaceWebView();
      web.getSettings().setGeolocationEnabled(true);
      web.setWebChromeClient(new WebChromeClient() {
        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
          super.onGeolocationPermissionsShowPrompt(origin, callback);
          /* Assuming the client has permission of the user (e.g. by dialog)
           * to use location information before by the time web view is created. */
          callback.invoke(origin, true, false);
        }
      });
      web.setCommandInterfaceWebViewClient(new ModalActivityWebViewClient(this));
      web.setName("ModalActivity");
      String baseUrl = data.getStringExtra(EXTRA_BASE_URL);
      if (baseUrl == null) { throw new NullPointerException(); }
      setupCommandInterface(web, baseUrl);

      FrameLayout layout = (FrameLayout) findViewById(RR.id("gree_modal_content_layout"));
      LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
      layout.addView(mPullToRefreshWebView, layoutParams);

      showLoadingIndicator();
      
      mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
      if (data.getIntExtra(EXTRA_TYPE, TYPE_DEFAULT) != TYPE_LOCATION) {
        commandInterface_.loadBaseUrl();
      }
      
      initFloatingTexts();
  }

  @Override
  protected void onNewIntent(Intent intent) {
    finish();
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    Intent intent = getIntent();
    if (!isLocationDone_ &&
        intent.getIntExtra(EXTRA_TYPE, TYPE_DEFAULT) == TYPE_LOCATION) {
      mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
      mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
      locationBeginTime_ = System.currentTimeMillis();
      timer_ = new Timer();
      timer_.schedule(new LocationTimerTask(), 0, TIMER_PERIOD);
    }
  }
  
  
  
  @Override
  protected void onPause() {
    mLocationManager.removeUpdates(this);
    if (timer_ != null) { 
      timer_.cancel();
      timer_ = null;
    }
    super.onPause();
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    ModalNavigationBar bar = (ModalNavigationBar)findViewById(RR.id("gree_modalview_navigationbar"));
    bar.adjustNavigationBarHeight(newConfig);
  }

  private void setupCommandInterface(CommandInterfaceWebView web, String baseUrl) {
    commandInterface_ = new CommandInterface();
    commandInterface_.setBaseUrl(baseUrl);
    commandInterface_.addOnCommandListener(new ModalActivityCommandListener());
    commandInterface_.setWebView(web);
  }

  private String getString(JSONObject jsonObject, String name) {
    return (String)jsonObject.remove(name);
  }

  private class ModalActivityCommandListener extends CommandInterface.OnCommandListenerAdapter {
    @Override
    public void onSetPullToRefreshEnabled(CommandInterface commandInterface, JSONObject params) {
      mPullToRefreshWebView.setPullToRefreshEnabled(params.optBoolean("enabled", PULL2REFRESH_DEFAULT_AVAILABILITY));
    }

    @Override
    public void onReady(final CommandInterface commandInterface, final JSONObject params) {
      commandInterface_.loadView(viewName_, openViewParams_);
    }

    @Override
    public void onSnsapiRequest(final CommandInterface commandInterface, final JSONObject params) {
      SnsApi snsApi = new SnsApi();
      snsApi.request(params, new SnsApi.SnsApiListener() {
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, String result) {
          try {
            commandInterface_.executeCallback(params.getString("success"), result);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String result) {
          try {
            String[] results = result.split(":",2);
            String args = responseCode + ",\"" + results[0] + "\"," + results[1];
            commandInterface_.executeCallback(params.getString("failure"), args);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        }
      });
    }

    @Override
    public void onGetAppInfo(final CommandInterface commandInterface, final JSONObject params) {
      if (params.isNull("callback")) { return; }
      try {
        commandInterface_.executeCallback(
            params.getString("callback"),
            new JSONObject().
            put("id", Core.getAppId()).
            put("version", Core.getAppVersion()).
            put("sdk_version", Core.getSdkVersion())
            );
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    @Override
    public void onContentsReady(CommandInterface commandInterface, JSONObject params) {
      uiThreadHandler_.post(new Runnable() {
        public void run() {
          mPullToRefreshWebView.onRefreshComplete();
          removeScreenShot();
          hideFloatingViews();
          mPullToRefreshWebView.updateLastUpdateTextView();
        }
      });
    }

    @Override
    public void onFailedWithError(CommandInterface commandInterface, JSONObject params) {
      uiThreadHandler_.post(new Runnable() {
        public void run() {
          removeScreenShot();
          hideFloatingViews();
        }
      });
    }

    @Override
    public void onInputSuccess(final CommandInterface commandInterface, final JSONObject params) {
      Intent intent = null;
      String viewName = params.optString("view");
      if (viewName.length() > 0) {
        intent = new Intent().putExtra("view", viewName);
        if (params != null) {
          intent.putExtra("params", params.toString());
        }
      }
      setResult(RESULT_OK, intent);
      finish();
    }

    @Override
    public void onInputFailure(CommandInterface commandInterface, JSONObject params) {
      String errorMessage = params.optString("error");
      if (errorMessage != null && !errorMessage.equals("null")) {
        new AlertDialog.Builder(ModalActivity.this)
        .setMessage(errorMessage)
        .setPositiveButton(android.R.string.ok, null)
        .show();
      }else if(errorMessage != null && errorMessage.equals("null")){
        new AlertDialog.Builder(ModalActivity.this)
        .setMessage(ModalActivity.this.getResources().getString(RR.string("gree_internet_connect_failure")))
        .setPositiveButton(android.R.string.ok, null)
        .show();
      }
      uiThreadHandler_.post(new Runnable() {
        public void run() {
          hideFloatingViews();
        }
      });
    }

    @Override
    public void onDismissModalView(final CommandInterface commandInterface, final JSONObject params) {
      uiThreadHandler_.post(new Runnable() {
        public void run() {
          Intent intent = new Intent();
          String jsonStr = params.toString();
          intent.putExtra("params", jsonStr);
          @SuppressWarnings("unchecked")
          Iterator<String> iter = (Iterator<String>) params.keys();
          while (iter.hasNext()) {
            String paramKey = iter.next();
            try {
              String paramVal = params.getString(paramKey);
              if (paramVal.length() > 0) {
                intent.putExtra(paramKey, paramVal);
              }
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }
          setResult(RESULT_OK, intent);
          finish();
        }
      });
    }

    @Override
    public void onBroadcast(final CommandInterface commandInterface, final JSONObject params) {
      CommandInterfaceView.sendBroadcast(ModalActivity.this, CommandInterfaceView.EVENT_BROADCAST, params);
    }
  }

  /**
   * set animation and show loading indicator
   */
  private void showLoadingIndicator() {
    mLoadingIndicator = (ProgressBar)findViewById(RR.id("gree_modal_loading_indicator"));
    Animation rotation = AnimationUtils.loadAnimation(this, RR.anim("gree_rotate"));
    rotation.setRepeatCount(Animation.INFINITE);
    mLoadingIndicator.startAnimation(rotation);
    mLoadingIndicator.setVisibility(View.VISIBLE);
  }

  /**
   * hide all floating views
   */
  private void hideFloatingViews() {
    mLoadingIndicator.clearAnimation();
    mLoadingIndicator.setVisibility(View.GONE);
    View main = findViewById(RR.id("gree_modal_float_main"));
    View sub = findViewById(RR.id("gree_modal_float_sub"));
    main.setVisibility(View.GONE);
    sub.setVisibility(View.GONE);
  }

  /**
   * remove PullToRefresh screenshot, which is shown while loading
   */
  public void removeScreenShot(){
    if (null != mPullToRefreshWebView) {
      mPullToRefreshWebView.removeScreenShot();
    }
  }

  /**
   * specialized CommanderInterfaceWebViewClient for ModalActivity
   */
  private class ModalActivityWebViewClient extends CommandInterfaceWebViewClient {

    public ModalActivityWebViewClient(Context context) {
      super(context);
    }

    @Override
    public void onPageStarted(WebView webView, String url, Bitmap favicon) {
      super.onPageStarted(webView, url, favicon);

      if (!Url.isSnsUrl(url)) {
        showLoadingIndicator();
      }
    }

    @Override
    public void onPageFinished(WebView webView, String url) {
      super.onPageFinished(webView, url);

      if (!Url.isSnsUrl(url)) {
        hideFloatingViews();
      }
      removeScreenShot();
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
      super.onReceivedError(view, errorCode, description, failingUrl);

      mPullToRefreshWebView.setPullToRefreshEnabled(false);
      hideFloatingViews();
      removeScreenShot();
    }
  }

  @Override
  public void onLocationChanged(Location location) {
    if (location.getProvider().equals(LocationManager.GPS_PROVIDER)
       && location.getAccuracy() <= GPS_ACCURACY_LIMIT) {
      postLocation(location.getLatitude(), location.getLongitude());
    }
    
    if (bestAccuracy_ == null || bestAccuracy_ > location.getAccuracy()) {
      bestAccuracy_ = location.getAccuracy();
      bestLat_ = location.getLatitude();
      bestLng_ = location.getLongitude();
    }
    
    if (isLocationTimeout()) {
      postLocation(bestLat_, bestLng_);
    }
  }
  
  private void postLocation(double lat, double lng) {
    timer_.cancel();
    timer_ = null;
    
    mLocationManager.removeUpdates(this);
    JSONObject params = new JSONObject();
    TextView main = (TextView) findViewById(RR.id("gree_modal_float_main"));
    View sub = findViewById(RR.id("gree_modal_float_sub"));
    
    try {
      params.put("lat", lat);
      params.put("lng", lng);
      commandInterface_.loadBaseUrl();
      commandInterface_.loadView(params);
      isLocationDone_ = true;
      
      sub.setVisibility(View.GONE);
      main.setText(RR.string("gree_location_taking_places"));
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }

  }
  
  @Override
  public void onProviderDisabled(String provider) {
  }

  @Override
  public void onProviderEnabled(String provider) {
  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {
  }
  
  private class LocationTimerTask extends TimerTask {
    @Override
    public void run() {
      if (isLocationTimeout()) {
        uiThreadHandler_.post(new Runnable(){
          @Override
          public void run() {
            postLocation(bestLat_, bestLng_);
          }
        });
      } 
    }
  }
  
  private boolean isLocationTimeout() {
    long dt = System.currentTimeMillis() - locationBeginTime_;
    return (dt > GPS_TIMEOUT && bestAccuracy_ != null);
  }
  
  private void initFloatingTexts() {
    View main = findViewById(RR.id("gree_modal_float_main"));
    View sub = findViewById(RR.id("gree_modal_float_sub"));
    Intent intent = getIntent();
    if (intent.getIntExtra(EXTRA_TYPE, TYPE_DEFAULT) == TYPE_LOCATION) {
      main.setVisibility(View.VISIBLE);
      sub.setVisibility(View.VISIBLE);
    } else {
      main.setVisibility(View.GONE);
      sub.setVisibility(View.GONE);
    }

  }
}
