/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.notifications.ui;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.appinfo.ApplicationInfo;
import net.gree.asdk.core.dashboard.EmojiGetter;
import net.gree.asdk.core.notifications.FormatDate;
import net.gree.asdk.core.notifications.NotificationCallback;
import net.gree.asdk.core.notifications.NotificationDownloader;
import net.gree.asdk.core.request.GreeHttpExecuterFactory;
import net.gree.asdk.core.request.IGreeHttpExecuter;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.request.helper.OsRequest;
import net.gree.asdk.core.storage.JSONStorage;
import net.gree.asdk.core.ui.RoundCornerImage;

import org.apache.http.HeaderIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adapter for NotificationListView
 */
public abstract class NotificationAdapterBase extends BaseAdapter implements OnItemClickListener, NotificationDownloader.OnImageDownload {
  private final static String TAG = NotificationAdapterBase.class.getName();

  public final static int LOAD_LIMIT = 7;

  public static final int CELL_TYPE_ERROR = -1;
  public static final int CELL_TYPE_NORMAL = 0;
  public static final int CELL_TYPE_SEPARATOR = 1;
  public static final int CELL_TYPE_SEEALL = 2;
  public static final int CELL_TYPE_AUTOPAGERIZE = 3;
  public static final int CELL_TYPE_NO_NOTIFICATION = 4;
  public static final int CELL_TYPE_AUTOPAGERIZE_PENDING = 5;

  private static final int CELL_TYPE_COUNT = 6;

  private final static int SEPARATOR_CELL_HEIGHT = 16;

  protected List<NotificationData> mDisplayObjects;
  protected int mNotificationType;
  protected int mContentCount;

  private List<WeakReference<NotificationCallback>> mCallbacks;


  private OsRequest mAutopagerizeDownloader;
  private ImageDownloader mImageDownloader;
  private boolean mIsMarkingAsRead;

  private DisplayMetrics mMetrics;
  private EmojiGetter mEmoji;

  private String mCurrentPageUrl;

  protected Context mContext;

  public NotificationAdapterBase(Context context, DisplayMetrics metrics, List<NotificationData> objects) {
    super();
    mDisplayObjects = objects;
    mMetrics = metrics;
    mCallbacks = new ArrayList<WeakReference<NotificationCallback>>();
    mEmoji = new EmojiGetter(context, metrics);
    mContext = context;
  }

  /**
   * add OnItemClick callback
   * @param callback
   */
  public void addCallback(NotificationCallback callback) {
    mCallbacks.add(new WeakReference<NotificationCallback>(callback));
  }

  /**
   * Clear OnItemClick callbacks
   */
  public void clearCallbacks() {
    mCallbacks.clear();
  }

  public abstract int loadNotifications();

  /**
   * Load local cached all data from JSONDatabase
   * @return count of loaded elements
   */
  public int loadByFilter(SQLiteDatabase db, String filter, Bitmap defaultIcon, int loadLimit) {
    JSONStorage storage = Injector.getInstance(JSONStorage.class);

    String[] selection = { filter };
    Cursor c = db.query(JSONStorage.TABLE_NAME, storage.getColums(), JSONStorage.COLUM_FILTER + " = ?", selection, null, null, 
      JSONStorage.COLUM_DATE + " desc", String.valueOf(loadLimit));
    int ret = c.getCount();

    if (ret <= 0) { 
      c.close();
      return ret; 
    }

    List<NotificationData> tmpList = new ArrayList<NotificationData>();

    c.moveToFirst();
    while (!c.isAfterLast()) {
      NotificationData data = new NotificationData();
      data.cellType = CELL_TYPE_NORMAL;
      byte[] blob = c.getBlob(JSONStorage.COLUM_INDEX_BLOB);
      if (blob != null) {
        Bitmap bmp = BitmapFactory.decodeByteArray(blob, 0, blob.length);
        RoundCornerImage rci = new RoundCornerImage(bmp);
        data.image = rci.getRoundedBitmap();
      } else {
        data.image = defaultIcon;
      }

      try {
        data.json = new JSONObject(c.getString(JSONStorage.COLUM_INDEX_JSON));
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
        data.json = new JSONObject();
      }

      data.date = c.getString(JSONStorage.COLUM_INDEX_DATE);

      tmpList.add(data);
      mDisplayObjects.add(data);
      c.moveToNext();
    }
    c.close();

    Collections.sort(tmpList);

    return ret;
  }

  protected void addNoNotification() {
    NotificationData data = new NotificationData();
    data.cellType = CELL_TYPE_NO_NOTIFICATION;
    mDisplayObjects.add(data);
  }

  public void setCurrentPageUrl(String url) {
    mCurrentPageUrl = url;
  }


  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    final NotificationData data = getItem(position);
    View target = null;
    if (convertView != null) {
      target = convertView;
    } else {
      LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      switch (data.cellType) {
        case CELL_TYPE_NORMAL:
          target = inflater.inflate(RR.layout("gree_notification_cell_normal"), null);
          break;
        case CELL_TYPE_SEPARATOR:
          target = inflater.inflate(RR.layout("gree_notification_cell_separator"), null);
          break;
        case CELL_TYPE_SEEALL:
          target = inflater.inflate(RR.layout("gree_notification_cell_seemore"), null);
          break;
        case CELL_TYPE_AUTOPAGERIZE:
          target = inflater.inflate(RR.layout("gree_notification_cell_autopagerize"), null);
          break;
        case CELL_TYPE_NO_NOTIFICATION:
          target = inflater.inflate(RR.layout("gree_notification_cell_seemore"), null);
          break;
        case CELL_TYPE_AUTOPAGERIZE_PENDING:
          target = inflater.inflate(RR.layout("gree_notification_cell_autopagerize"), null);
          break;

      }
    }


    Resources r = getContext().getResources();

    View normal = target.findViewById(RR.id("gree_notification_cellNormal"));
    ImageView icon = (ImageView) target.findViewById(RR.id("gree_notification_icon"));
    TextView seeallText = (TextView) target.findViewById(RR.id("gree_notification_seeall_text"));
    View seeallCarret = target.findViewById(RR.id("gree_notification_seemore_carret"));
    ImageView spinner = (ImageView) target.findViewById(RR.id("gree_notification_autopaerizeSpinner"));

    switch (data.cellType) {
      case CELL_TYPE_SEPARATOR:
        TextView sepText = (TextView)target.findViewById(RR.id("gree_notification_separateText"));
        sepText.setText(data.text);

        target.setMinimumHeight((int) (SEPARATOR_CELL_HEIGHT * mMetrics.density));
        break;

      case CELL_TYPE_SEEALL:
        seeallText.setText(data.text);
        seeallText.setGravity(Gravity.LEFT);
        seeallText.setTextColor(Color.BLACK);
        seeallText.setTypeface(Typeface.DEFAULT_BOLD);
        break;

      case CELL_TYPE_AUTOPAGERIZE:
        Animation rotation = AnimationUtils.loadAnimation(getContext(), RR.anim("gree_rotate"));
        rotation.setRepeatCount(Animation.INFINITE);
        spinner.startAnimation(rotation);

        autopagerize(data);
        break;

      case CELL_TYPE_AUTOPAGERIZE_PENDING:
        spinner.clearAnimation();
        break;

      case CELL_TYPE_NO_NOTIFICATION:
        seeallText.setText(r.getString(RR.string("gree_notification_no_notifications")));
        seeallText.setGravity(Gravity.CENTER);
        seeallText.setTextColor(RR.color("gree_notification_gray"));
        seeallText.setTypeface(Typeface.DEFAULT);
        seeallCarret.setVisibility(View.GONE);
        break;

      case CELL_TYPE_NORMAL:
        if (data.image != null) { icon.setImageBitmap(data.image); }

        TextView text = (TextView) target.findViewById(RR.id("gree_notification_text"));
        String parsed = parseElement(data.json);
        parsed = parsed != null ? parsed : "";
        String emojiEmbedded = adaptEmoji(parsed);
        text.setText(Html.fromHtml(emojiEmbedded, mEmoji, null));

        ImageView unreadIcon = (ImageView)target.findViewById(RR.id("gree_notification_unread"));

        boolean unread = false;
        try {
          boolean flag = data.json.getInt("unread") != 0 ? true : false;
          unread = flag;
          if (unread) {
            normal.setBackgroundResource(RR.drawable("gree_notification_unread"));
            unreadIcon.setImageDrawable(r.getDrawable(RR.drawable("gree_btn_notification_unread")));
            if (!mIsMarkingAsRead && mNotificationType != NotificationFragment.NOTIFICATION_FRIENDS) {
              mIsMarkingAsRead = true;
            }
          } else {
            normal.setBackgroundResource(RR.drawable("gree_notification_read"));
            unreadIcon.setImageDrawable(r.getDrawable(RR.drawable("gree_btn_notification_read")));
          }
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        TextView dateText = (TextView)target.findViewById(RR.id("gree_notification_date"));
        Calendar date = Calendar.getInstance();
        try {
          SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
          df.setTimeZone(TimeZone.getTimeZone("UTC"));
          date.setTime(df.parse(data.json.getString("date")));
          FormatDate fd = new FormatDate(r, date);
          dateText.setText(fd.getResult());
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        } catch (ParseException e) {
          GLog.printStackTrace(TAG, e);
        }

        try {
          Button friendBtn = (Button) target.findViewById(RR.id("gree_notification_cell_friendBtn"));
          String namespace = data.json.getString("namespace");
          if (namespace.equals("friends_link_pending")) {
            friendBtn.setVisibility(View.VISIBLE);
            friendBtn.setBackgroundResource(RR.drawable("gree_notification_link_friend_permit"));
            friendBtn.setOnClickListener(new OnClickListener(){
              @Override
              public void onClick(View v) {
                try {
                  mDisplayObjects.remove(position);
                  notifyDataSetChanged();

                  Map<String, Object> query = new HashMap<String, Object>();
                  final String feedKey = data.json.getString("feed_key"); 
                  query.put("key", feedKey);

                  OsRequest osRequest = new OsRequest();
                  osRequest.requestPost(query, "/linkedfriend", new OnResponseCallback<String>(){
                    @Override
                    public void onSuccess(int responseCode, HeaderIterator headers, String response) {
                      JSONStorage storage = Injector.getInstance(JSONStorage.class);
                      String[] selection = { feedKey };
                      storage.delete(JSONStorage.COLUM_KEY +  " = ?", selection);
                    }

                    @Override
                    public void onFailure(int responseCode, HeaderIterator headers, String response) {
                      GLog.e(TAG, response);
                    }});

                } catch (JSONException e) {
                  GLog.printStackTrace(TAG, e);
                }
              }
            });

          } else if (namespace.equals("friends_registration")) {
            friendBtn.setVisibility(View.VISIBLE);
            friendBtn.setBackgroundResource(RR.drawable("gree_notification_link_friend_add"));
            friendBtn.setOnClickListener(new OnClickListener(){
              @Override
              public void onClick(View v) {
                try {
                  mDisplayObjects.remove(position);
                  notifyDataSetChanged();

                  Map<String, Object> query = new HashMap<String, Object>();
                  final String feedKey = data.json.getString("feed_key");
                  query.put("key", feedKey);

                  OsRequest osRequest = new OsRequest();
                  osRequest.requestPost(query, "/requestfriend", new OnResponseCallback<String>(){
                    @Override
                    public void onSuccess(int responseCode, HeaderIterator headers, String response) {
                      JSONStorage storage = Injector.getInstance(JSONStorage.class);
                      String[] selection = { feedKey };
                      storage.delete(JSONStorage.COLUM_KEY +  " = ?", selection);
                    }

                    @Override
                    public void onFailure(int responseCode, HeaderIterator headers, String response) {
                      GLog.e(TAG, response);
                    }});

                } catch (JSONException e) {
                  GLog.printStackTrace(TAG, e);
                }
              }
            });
          } else {
            friendBtn.setVisibility(View.GONE);
          }
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        TextView appName = (TextView)target.findViewById(RR.id("gree_notification_appsName"));
        if (mNotificationType == NotificationFragment.NOTIFICATION_APPS) {
          appName.setVisibility(View.VISIBLE);
          try {
            String appNameText = data.json.getString("app_name");
            appName.setText(appNameText);
          } catch (JSONException e) {
            GLog.printStackTrace(TAG, e);
          }
        } else {
          appName.setVisibility(View.GONE);
        }

        break;

      default:
        throw new RuntimeException();
    }

    return target;
  }

  /**
   * Call "markAsRead" API and update the cache
   * @param data A notification element
   */
  protected void markAsRead(String type, OnResponseCallback<String> callback) {

    Map<String, Object> query = new HashMap<String, Object>();
    query.put("type", type);

    OsRequest osRequest = new OsRequest();
    osRequest.requestPost(query, "/markasread", callback);
  }

  /**
   * Autopagerize when the tail cell apperas
   * @param autopagerize keeps offset and limit info to call API
   */
  private void autopagerize(final NotificationData autopagerize) {
    if (mAutopagerizeDownloader != null) { return; }
    if (mContentCount < LOAD_LIMIT) { return; }

    mAutopagerizeDownloader = new OsRequest();


    Map<String, Object> query = new HashMap<String, Object>();
    query.put("limit", LOAD_LIMIT);
    query.put("offset", autopagerize.offset);
    String kind = null;
    switch(mNotificationType) {
      case NotificationFragment.NOTIFICATION_APPS:
        kind = "other";
        break;
      case NotificationFragment.NOTIFICATION_SNS:
        kind = "activity";
        break;
      case NotificationFragment.NOTIFICATION_FRIENDS:
        kind = "friend";
        break;
    }    
    ApplicationInfo appInfo = Injector.getInstance(ApplicationInfo.class);
    query.put("fields", kind);
    query.put("app_id", appInfo.getAppId());

    mAutopagerizeDownloader.requestGet(query, "/notification/@app", new OnResponseCallback<String>(){

      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        try {
          JSONObject json = new JSONObject(response);
          String kind = null;
          switch(mNotificationType) {
            case NotificationFragment.NOTIFICATION_APPS:
              kind = "other";
              break;
            case NotificationFragment.NOTIFICATION_SNS:
              kind = "activity";
              break;
            case NotificationFragment.NOTIFICATION_FRIENDS:
              kind = "friend";
              break;
          }
          JSONObject field = json.getJSONObject("entry").getJSONObject(kind);
          JSONArray entries = field.getJSONArray("entries");
          List<JSONObject> imgDownloadList = new ArrayList<JSONObject>();

          Map<String, Boolean> keyMap = new HashMap<String, Boolean>();
          for (int i = 0; i < NotificationAdapterBase.this.getCount(); ++i) {
            NotificationData item = NotificationAdapterBase.this.getItem(i);
            if (item.cellType != CELL_TYPE_NORMAL) { continue; }
            try { 
              keyMap.put(item.json.getString("feed_key"), true);
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }

          mDisplayObjects.remove(mDisplayObjects.size() - 1);
          for (int i = 0; i < entries.length(); ++i) {
            JSONObject entry = entries.getJSONObject(i);
            try {
              if (!keyMap.containsKey(entry.getString("feed_key"))) {
                NotificationData data = new NotificationData();
                data.cellType = CELL_TYPE_NORMAL;
                data.json = entry;
                mDisplayObjects.add(data);
                imgDownloadList.add(entry);
              }
            } catch (JSONException e) {
              GLog.printStackTrace(TAG, e);
            }
          }

          if (field.getInt("has_more")  != 0) {
            NotificationData newAutopagerize = new NotificationData();
            newAutopagerize.offset = autopagerize.offset + LOAD_LIMIT;
            newAutopagerize.cellType = CELL_TYPE_AUTOPAGERIZE;
            mDisplayObjects.add(newAutopagerize);
          }

          mImageDownloader = new ImageDownloader(imgDownloadList, NotificationAdapterBase.this);
          mImageDownloader.execute();
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        } finally {
          NotificationAdapterBase.this.notifyDataSetChanged();
          mAutopagerizeDownloader = null;
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        NotificationData data = mDisplayObjects.get(mDisplayObjects.size() - 1);
        data.cellType = CELL_TYPE_AUTOPAGERIZE_PENDING;
        NotificationAdapterBase.this.notifyDataSetChanged();
        mAutopagerizeDownloader = null;
      }

    });

  }

  @Override
  public void onDownload(String feedKey, Bitmap image) {
    for (NotificationData data : mDisplayObjects) {
      if (data.cellType != CELL_TYPE_NORMAL) { continue; }
      try {
        if (feedKey.equals(data.json.get("feed_key"))) {
          RoundCornerImage rci = new RoundCornerImage(image);
          data.image = rci.getRoundedBitmap();
          notifyDataSetChanged();
          break;
        }
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      }
    }
  }


  /**
   * Note: TextView can use restricted HTML tags. 
   * @param json A part of otification API response
   * @return HTML formed text for TextView
   */
  protected String parseElement(JSONObject json){
    try {
      String message = json.getString("message");

      String keyRegex = "\\{(\\w+)\\}";
      String[] plain = message.split(keyRegex, -1);

      if (plain.length <= 1) {
        return message;
      }

      Pattern p = Pattern.compile(keyRegex);
      Matcher m = p.matcher(message);

      JSONObject data = json.getJSONObject("data");
      List<String> embededText = new ArrayList<String>();
      while (m.find()) {
        String key = m.group(1);

        JSONObject embededData = data.getJSONObject(key);
        String text = embededData.getString("text");

        try {
          JSONObject font = embededData.getJSONObject("font");
          try{
            boolean isBold = font.getBoolean("bold");
            if(isBold){
              text = "<b>" + text + "</b>";
            }
          } catch(Exception e) {

          }

          try{
            JSONObject color = font.getJSONObject("color");
            Integer r = color.getInt("r");
            Integer g = color.getInt("g");
            Integer b = color.getInt("b");
            String colorFormat = "\"#" + Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b) + "\"";
            text = "<font color=" + colorFormat + ">" + text + "</font>";
          }catch(JSONException e){

          }
        } catch(JSONException e) {

        }
        embededText.add(text);
      }

      String ret = "";
      for (int i = 0; i < plain.length - 1; ++i) {
        ret += plain[i];
        ret += embededText.get(i);
      }
      ret += plain[plain.length - 1];
      return ret;

    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
    }
    return null;
  }

  protected String adaptEmoji(String src) {
    String keyRegex = "<emoji id=\"([0-9]+)\">";
    String[] plain = src.split(keyRegex, -1);

    Pattern p = Pattern.compile(keyRegex);
    Matcher m = p.matcher(src);

    List<String> imgTags = new ArrayList<String>();
    while (m.find()) {
      String emojiId = m.group(1);
      String imgTag = "<img src=\"" + emojiId + "\">";
      imgTags.add(imgTag);
    }

    String ret = "";
    for (int i = 0; i < plain.length - 1; ++i) {
      ret += plain[i];
      ret += imgTags.get(i);
    }
    ret += plain[plain.length - 1];

    return ret;
  }

  @Override
  public void onItemClick(AdapterView<?> listView, View cellView, int position, long id) {
    NotificationData data = getItem(position);

    if (data.cellType == CELL_TYPE_NORMAL || data.cellType == CELL_TYPE_SEEALL) {
      try {
        String url = data.json.getString("url");

        for(WeakReference<NotificationCallback> ref : mCallbacks) {
          NotificationCallback callback = ref.get();
          if(data.json != null) {
            callback.onNotificationClick(data.json);
          }
        }

        Logger.recordLog("pg", url, mCurrentPageUrl, null);
      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);        
      }
    }

    if (data.cellType == CELL_TYPE_AUTOPAGERIZE_PENDING) {
      data.cellType = CELL_TYPE_AUTOPAGERIZE;
      notifyDataSetChanged();
    }
  }

  private class ImageDownloader extends AsyncTask<Void, Void, Void> {
    public ImageDownloader(List<JSONObject> entries, NotificationDownloader.OnImageDownload callback) {
      mEntries = entries;
      mCallback = callback;
    }

    private List<JSONObject> mEntries;
    private NotificationDownloader.OnImageDownload mCallback;
    private Handler mHandler = new Handler();

    @Override
    protected Void doInBackground(Void... params) {
      for (JSONObject entry : mEntries) {
        downloadImage(entry);
      }

      return null;
    }

    private void downloadImage(JSONObject entry) {
      try {
        String url = entry.getString("image");
        if(TextUtils.isEmpty(url)) { return; }

        HttpUriRequest method = new HttpGet(url);
        IGreeHttpExecuter executer = GreeHttpExecuterFactory.getExecutor();

        HttpResponse response = executer.execute(method);
        if (200 != response.getStatusLine().getStatusCode()) { return; }

        InputStream is = response.getEntity().getContent();
        final Bitmap bmp = BitmapFactory.decodeStream(is);

        final String feedKey = entry.getString("feed_key");
        mHandler.post(new Runnable(){
          @Override
          public void run() {
            if (mCallback != null) { mCallback.onDownload(feedKey, bmp); }
          }
        });

      } catch (JSONException e) {
        GLog.printStackTrace(TAG, e);
      } catch (IllegalStateException e) {
        GLog.printStackTrace(TAG, e);
      } catch (IOException e) {
        GLog.printStackTrace(TAG, e);
      } catch (NullPointerException e) {
        GLog.printStackTrace(TAG, e);
      }
    }

    @Override
    protected void onPostExecute(Void result) {
      mImageDownloader = null;
    }

  }

  @Override
  public NotificationData getItem(int position) {
    return mDisplayObjects.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public int getCount() {
    return mDisplayObjects.size();
  }

  protected Context getContext() {
    return mContext;
  }

  @Override
  public int getViewTypeCount() {
    return CELL_TYPE_COUNT;
  }

  @Override
  public int getItemViewType (int position) {
    NotificationData data = getItem(position);
    return data.cellType;
  }

}
