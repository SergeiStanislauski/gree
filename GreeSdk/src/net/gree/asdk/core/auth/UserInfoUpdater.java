/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import java.util.concurrent.CountDownLatch;

import net.gree.asdk.api.GreeUser;
import net.gree.asdk.api.GreeUser.GreeUserListener;
import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Session;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.storage.CookieStorage;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

/**
 * UserInfoUpdater can update web session and local user.
 * This class should be called in authorization because it does specialized handling for terms agreement.
 * @author GREE, Inc.
 */
public class UserInfoUpdater {
  private static final String TAG = UserInfoUpdater.class.getSimpleName();

  public static final String AGREEMENT_URL_KEY = "agreementUrl";

  private Context mContext;
  private boolean mSync = false;
  private AgreementListener mListener;
  private String mAgreementUrl;

  /**
   * Initializer
   * @param context
   */
  public UserInfoUpdater(Context context) {
    mContext = context;
  }

  /**
   * Chain setter for synchronized type
   * @param sync
   * @return this
   */
  public UserInfoUpdater sync(boolean sync) {
    mSync = sync;
    return this;
  }

  /**
   * Interface definition for a callback to be invoked when the response contains information for agreement.
   */
  public interface AgreementListener {
    /**
     * Callback to load the URL to confirm agreement.
     * @param url URL to confirm agreement
     */
    public void onNeedAgreement(String url);
  }

  /**
   * Chain setter for listener
   * @param listener
   * @return this
   */
  public UserInfoUpdater listener(AgreementListener listener) {
    mListener = listener;
    return this;
  }

  /**
   * Get URL to confirm agreement
   * @return URL
   */
  public String getAgreementUrl() {
    return mAgreementUrl;
  }

  /**
   * Trigger the requests to update user information.
   * @return false if waiting for response fails
   */
  public boolean request() {
    final CountDownLatch signal = mSync ? new CountDownLatch(2) : null;
    new Session().refreshSessionId(mContext, new OnResponseCallback<String>(){
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        CookieStorage.setCookie(headers);
        if (response != null) {
          try {
            mAgreementUrl =  new JSONObject(response).getJSONObject("entry").getString(AGREEMENT_URL_KEY);
            if (mListener != null && mAgreementUrl != null) {
              mListener.onNeedAgreement(mAgreementUrl);
            }
          } catch (JSONException e) {
            GLog.d(TAG, e.getMessage());
          }
        }
        if (signal != null) signal.countDown();
      }
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, "refreshSessionId failed in updateSessionAndSelf.");
        GLog.e(TAG, response);
        if (signal != null) signal.countDown();
      }
    });
    Core.getInstance().updateLocalUser(new GreeUserListener() {
      public void onSuccess(int index, int count, GreeUser[] users) {
        if (signal != null) signal.countDown();
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (signal != null) signal.countDown();
      }
    });
    if (mSync) {
      try {
        signal.await();
      } catch (InterruptedException e) {
        e.printStackTrace();
        return false;
      }
    }
    return true;
  }
}
