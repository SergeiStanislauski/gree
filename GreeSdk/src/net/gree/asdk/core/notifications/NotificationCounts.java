/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.notifications;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import net.gree.asdk.api.GreePlatform.BadgeListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.request.helper.OsRequest;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is GREE notification counts control class.
 */
public class NotificationCounts {
  private static final String TAG = "NotificationCounts";

  public static final int TYPE_ALL = 0;
  public static final int TYPE_SNS = 1;
  public static final int TYPE_APP = 2;
  public static final int TYPE_FRIENDS = 3;

  //Notification counts
  private int mSnsCount;
  private int mAppCount;
  private int mFriendsCount;

  //List of listeners to be called for any notification update.
  private List<WeakReference<Listener>> updateAllListeners = new ArrayList<WeakReference<Listener>>();

  //List of listeners to be called for Application notification updates.
  private List<WeakReference<BadgeListener>> updateAppListeners = new ArrayList<WeakReference<BadgeListener>>();

  /**
   * Listener called after every update.
   */
  public interface Listener {
    /**
     * Notify callback when return response of getting badge request.
     */
    void onUpdate();
  }

  /**
   * Register a listener to be called after the notifications count has been updated.
   */
  public void addListener(Listener newListener) {
    boolean listenerIsInList = false;
    for (int i = 0; i < updateAllListeners.size(); i++) {
      WeakReference<Listener> wrl = updateAllListeners.get(i);
      if (wrl != null) {
        Listener l = wrl.get();
        if (l != null) {
          if (l == newListener) {
            listenerIsInList = true;
          }
        }
      }
    }
    if (!listenerIsInList) {
      updateAllListeners.add(new WeakReference<Listener>(newListener));
    }
  }

  /**
   * Register a listener to be called after the notifications count has been updated.
   */
  public void addApplicationCountListener(BadgeListener newAppListener) {
    boolean listenerIsInList = false;
    for (int i = 0; i < updateAppListeners.size(); i++) {
      WeakReference<BadgeListener> wrl = updateAppListeners.get(i);
      if (wrl != null) {
        BadgeListener l = wrl.get();
        if (l != null) {
          if (l == newAppListener) {
            listenerIsInList = true;
          }
        }
      }
    }
    if (!listenerIsInList) {
      updateAppListeners.add(new WeakReference<BadgeListener>(newAppListener));
    }
  }

  /**
   * Get the number of notification.
   */
  public int getNotificationCount(int type) {
    switch (type) {
      case TYPE_ALL:
        return mSnsCount + mAppCount;
      case TYPE_SNS:
        return mSnsCount;
      case TYPE_APP:
        return mAppCount;
      case TYPE_FRIENDS:
        return mFriendsCount;
      default :
        return 0;
    }
  }

  /**
   * Get the notification counts from the server.
   * this will get the notifications for current app only.
   */
  public void updateCounts() {
    updateCounts("/badge/@app/@all", null);
  }

  /**
   * Get the notification counts from the server for the SNS application.
   * This will get the notification counts for all applications.
   */
  public void updateCountsForSns() {
    updateCounts("/badge/@app/@all", null);
  }

  /**
   * Get the notification counts and get a callback.
   * this is in case you need to get notified only once.
   * 
   * @param listener the listener to be called upon success
   */
  public void updateCounts(Listener listener) {
    updateCounts("/badge/@app/@all", listener);
  }

  /**
   * Clear the notification counts from device tmp data.
   * This will set the notification counts to all zero.
   */
  public void clearCounts() {
    GLog.d(TAG, "Clear notifications counts.");
    mSnsCount = 0;
    if (mAppCount != 0) {
      mAppCount = 0;
      updateAppListeners();
    }
    updateListeners();
  }

  private void updateCounts(String action, final Listener listener) {
    OsRequest osRequest = new OsRequest();
    osRequest.requestGet(null, action, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        try {
          JSONObject entries = new JSONObject(response);
          JSONObject entry = entries.getJSONObject("entry");
          mSnsCount = entry.getInt("sns_sns");
          mAppCount = entry.getInt("app");
          mFriendsCount = entry.getInt("sns_friend");
        } catch (JSONException e) {
          GLog.printStackTrace(TAG, e);
        }

        if (listener != null) {
          listener.onUpdate();
        }

        updateListeners();

        updateAppListeners();
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        GLog.e(TAG, response);
      }
    });

  }

  private void updateListeners() {
    for (int i = 0; i < updateAllListeners.size(); i++) {
      WeakReference<Listener> wrl = updateAllListeners.get(i);
      if (wrl != null) {
        Listener l = wrl.get();
        if (l != null) {
          l.onUpdate();
        } else {
          updateAllListeners.remove(wrl);
        }
      }
    }
  }

  private void updateAppListeners() {
    for (int i = 0; i < updateAppListeners.size(); i++) {
      WeakReference<BadgeListener> wrl = updateAppListeners.get(i);
      if (wrl != null) {
        BadgeListener l = wrl.get();
        if (l != null) {
          l.onBadgeCountUpdated(mAppCount);
        } else {
          updateAppListeners.remove(wrl);
        }
      }
    }
  }

}
