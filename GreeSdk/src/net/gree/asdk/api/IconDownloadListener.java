/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import org.apache.http.HeaderIterator;

import android.graphics.Bitmap;

/**
 * Callback interface used for icon downloading.
 * 
 * @author GREE, Inc.
 */
public interface IconDownloadListener {
  /**
   * When the download is success, the this function will be called with Bitmap object
   * 
   * @param image The Bitmap Object resulting from a successful download.
   */
  void onSuccess(Bitmap image);

  /**
   * When the download is a failure, this function will be called with an HTTP response code, HTTP response header iterator, and the HTTP response.
   * 
   * @param responseCode Integer representing the HTTP response code.
   * @param headers Object representing the HTTP response header iterator.
   * @param response String representing the HTTP response, if any.
   */
  void onFailure(int responseCode, HeaderIterator headers, String response);
}
