/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core;

import net.gree.asdk.core.appinfo.ApplicationInfo;
import net.gree.asdk.core.auth.AuthorizerCore;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.auth.NormalAuthDao;
import net.gree.asdk.core.auth.OAuthNormalDao;
import net.gree.asdk.core.cache.ImageFetcher;
import net.gree.asdk.core.cache.ImageFetcherFactory;
import net.gree.asdk.core.inject.AbstractModule;
import android.content.Context;
import net.gree.asdk.core.storage.DBStorage;
import net.gree.asdk.core.storage.JSONStorage;
import net.gree.asdk.core.storage.LocalStorage;
import net.gree.vendor.com.google.gson.Gson;

/**
 * Core module of {@link net.gree.asdk.core.inject.InternalInjector} which used to create
 * {@link net.gree.asdk.core.Injector}
 */
public class CoreModule extends AbstractModule {

  private Context context;

  public CoreModule(Context context) {
    this.context = context;
  }

  @Override
  protected void configure() {
    bind(Gson.class, new Gson());
    bind(Context.class, context);
    bind(GConnectivityManager.class, new GConnectivityManager(context));
 
    // storage
    bind(DBStorage.class, new DBStorage(context));
    bind(LocalStorage.class, new LocalStorage(context));
    bind(JSONStorage.class, new JSONStorage(context.getApplicationContext()));

    // Dialogs and Activities events
    bind(TaskEventDispatcher.class);

    // auth
    bind(IAuthorizer.class, AuthorizerCore.class);
    bind(OAuthNormalDao.class, NormalAuthDao.class);
    
    // Image cache
    bind(ImageFetcher.class, ImageFetcherFactory.createImageFetcher());

    bind(ApplicationInfo.class, new ApplicationInfo());
  }

}
