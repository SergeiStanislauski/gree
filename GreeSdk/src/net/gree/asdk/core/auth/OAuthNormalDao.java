package net.gree.asdk.core.auth;

import android.content.Context;
import android.net.Uri;
import net.gree.asdk.core.auth.SetupActivity.SetupListener;

public interface OAuthNormalDao {
  
  void initalize(IAuthorizer authorizer, Context context, SetupListener listener);
  
  boolean getIsDirectingToSetupActivity();
  
  void setRedircetToSetupActivity(boolean redirect);

  void retrieveAccessToken(Uri uri);


}
