/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.analytics.referrer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import net.gree.asdk.core.auth.AuthorizeContext;
import net.gree.asdk.core.request.GeneralClient;
import net.gree.asdk.core.util.Url;

public class ReferrerReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {
    if (!(intent instanceof Intent)) {
      return;
    }
    String encodedReferrer = intent.getStringExtra("referrer");
    if (TextUtils.isEmpty(encodedReferrer)) {
      return;
    }
    try {
      StringBuffer sb = new StringBuffer(Url.getSecureApiEndpointWithAction("sdkreferrer"));
      sb.append("?context=").append(AuthorizeContext.getUserKey());
      sb.append("&referrer=").append(encodedReferrer);
      String url = sb.toString();
      new GeneralClient().oauth2(url, "GET", null, false, null);
    } catch (Exception e) {}
  }
}
