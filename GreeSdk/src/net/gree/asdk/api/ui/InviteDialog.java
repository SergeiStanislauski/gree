/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api.ui;

import java.util.TreeMap;

import android.content.Context;
import android.os.Handler;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.GreePlatformListener;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.ui.AbsServiceDialog;
import net.gree.asdk.core.util.Url;

/**
 * Class representing the dialog for sending an Invite message.<br>
 * When a invite is successfully sent, the sender is notified via a result as JSON data.
 *
 * Sample code:
 * <code><pre>
 * Handler handler = new Handler() {
 *   public void handleMessage(Message message) {
 *     switch(message.what) {
 *       case InviteDialog.OPENED:
 *         Log.d("Invite", "InviteDialog opened.");
 *         break;
 *       case InviteDialog.CLOSED:
 *         Log.d("Invite", "InviteDialog closed.");
 *         Log.d("Invite", "result:" + message.obj.toString());
 *         break;
 *     }
 *   }
 * };
 *
 * InviteDialog dialog = new InviteDialog(Activity.this);
 * dialog.setHandler(handler);
 *
 * TreeMap&lt;String,Object&gt; map = new TreeMap&lt;String,Object&gt;();
 * String[] userList = {"1", "2", "3"};
 * map.put("body", "message of the invite dialog");
 * map.put("to_user_id", userList);
 * dialog.setParams(map);
 * dialog.show();
 * </pre></code>
 * @author GREE, Inc.
 *
 */
public final class InviteDialog extends AbsServiceDialog {
  private static final String TAG = "InviteDialog";

/**
 * Handler event type which is notified when the invite dialog is started/opens.<pre>
 */
  public static final int OPENED = 1;
/**
 * <p>
 * Handler event type which is notified when invite dialog is finished/closes.<br>
 * This event has additional parameter "message.obj" as an invite results in handler's message.
 * </p>
 */
  public static final int CLOSED = 2;

/**
 * Max number to which you can send an invite message to.
 */
  public static final int USER_ID_LIST_MAX_NUM = 15;

  private static final String SERVICE_CODE = "IV0";

  /* Endpoint for Invite Service Dialog */
  private static final String ENDPOINT = Url.getInviteDialogContentUrl();

  /**
   * Default constructor.
   * @param context The Context the Dialog is to run it.
   */
  public InviteDialog(Context context) {
    super(context);

    //set the dialog type for TaskEventListeners
    mClassType = GreePlatformListener.CLASS_INVITE_DIALOG;

    setRequestType(TYPE_REQUEST_METHOD_POST);

    setIsClearHistory(true);
    setTitleType(TITLE_TYPE_STRING);
    setTitle(GreePlatform.getRString(RR.string("gree_dialog_title_invite")));
  }

  /**
   * Set the event handler of the invite.
   * @param handler event handler.
   */
  @Override
  public void setHandler(Handler handler) {
    super.setHandler(handler);
  }

 
  /**
   * A call to show the invite dialog.
   */
  @Override
  public final void show() {
    mPerformData = mManager.createData(PerformanceFlowIndex.INVITE);
    super.show();
    GLog.d(TAG, "show()");
  }

  @Override
  protected void onShow() {
    setPostData(buildPostData());
  }

/**
 * Sets the optional parameters of the invite dialog.
 * Parameter are passed as TreeMap object.
 * @param params TreeMap object which has the following keys as elements.
 * <br> -body a message sent with invite request.
 * <br> -to_user_id Array as user id string which you want to send invite.
 */
  public void setParams(TreeMap<String, Object> params) {
    super.setParams(params);
  }

  @Override
  protected int getOpenedEvent() {
    return OPENED;
  }

  @Override
  protected int getClosedEvent() {
    return CLOSED;
  }

  @Override
  final protected String getEndPoint() {
    return ENDPOINT;
  }

  @Override
  protected String getServiceCode() {
    return SERVICE_CODE;
  }
}
