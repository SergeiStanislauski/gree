package net.gree.asdk.core.auth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;

/**
 * Action: android.intent.action.VIEW is an Activity Action as documented by 
 * http://developer.android.com/reference/android/content/Intent.html#ACTION_VIEW,
 * therefore, when returning from a the web we need to launch an Activity, instead of a service or
 * a broadcast receiver as preferred. We get the intent and then redirect ASAP, finishing the activity. 
 */

public class OAuthResponseRedirectActivity extends Activity {
  private static final String TAG = OAuthResponseRedirectActivity.class.getSimpleName();
  private static OAuthNormalDao sAuthorizer;

  @Override
  public void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    // set up a null view so the user never sees a blank screen, not even for a second
    TextView tv = new TextView(this);
    tv.setText("");
    setContentView(tv);
    
    // should have an intent here, if not fluke/error
    try{
      Uri uri = getIntent().getData();
      if(uri == null){
        //signal error
        finish();
      }
      else{   
        redirect(uri);
        finish();
      }
    }catch(NullPointerException e){
      GLog.printStackTrace(TAG, e);
      finish();
    }
  }

  @Override
  public void onPause(){
    super.onPause();
    
  }
  
  @Override
  public void onResume(){
    super.onResume();
    
  }
  
  @Override
  public void onNewIntent (Intent intent){
    // should have an intent here, if not fluke/error
    try{
      Uri uri = intent.getData();
      if(uri == null){
        //signal error
        finish();
      }
      else{   
        redirect(uri);
        finish();
      }
    }catch(NullPointerException e){
      GLog.printStackTrace(TAG, e);
      finish();
    }
  }

  
  private void redirect(Uri uri) {
    // if we don't have an instance of the direct API, send it to the SetupActivity
    if(getNormalAuthDao().getIsDirectingToSetupActivity()){
      // if we have an instance coming from the SetupActivity, redirect it back
      Intent intent = new Intent(this, SetupActivity.class);
      intent.setData(uri);
      startActivity(intent);
    }
    else{
      // launch a new thread to do work
      Runnable runnable = new DirectLoginAPIThread(uri);
      // Create the thread supplying it with the runnable object
      Thread thread = new Thread(runnable);
      // Start the thread
      thread.start();
      finish();
    } 
  }
  
  private OAuthNormalDao getNormalAuthDao() {
    if (null == sAuthorizer) {
      sAuthorizer = Injector.getInstance(OAuthNormalDao.class);
    }
    return sAuthorizer;
  }
}
