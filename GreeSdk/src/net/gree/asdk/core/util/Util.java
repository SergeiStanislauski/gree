/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.codec.Base64;
import net.gree.asdk.core.codec.Digest;

/**
 * The Util static class for the sdk
 * 
 */
public class Util {
  private static final String TAG = "gree/core/Util";


  /**
   * Returns own signature of The Android Package.
   * 
   * @param context
   * @param packageName
   * @return
   */
  public static Signature[] getAppSignatures(Context context, String packageName) {
    PackageInfo info = getPackageInfo(context, packageName);
    if (info != null) {
      return info.signatures;
    } else {
      return null;
    }
  }

  /**
   * Returns own info of The Android Package.
   * 
   * @param context
   * @param packageName
   * @return
   */
  public static PackageInfo getPackageInfo(Context context, String packageName) {
    PackageManager manager = context.getPackageManager();
    PackageInfo pInfo = null;
    try {
      pInfo = manager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
    } catch (NameNotFoundException nnfe) {
      GLog.d(TAG, packageName + " is not found.");
    }
    return pInfo;
  }

  /**
   * getVersionName
   * 
   * @param context
   * @param packageName
   * @return
   */
  public static String getVersionName(Context context, String packageName) {
    PackageInfo packageInfo = getPackageInfo(context, packageName);
    return (packageInfo != null) ? packageInfo.versionName : "0.0.0";
  }

  /**
   * getVersionCode
   * 
   * @param context
   * @param packageName
   * @return
   */
  public static int getVersionCode(Context context, String packageName) {
    PackageInfo packageInfo = getPackageInfo(context, packageName);
    return (packageInfo != null) ? packageInfo.versionCode : 0;
  }

  /**
   * 
   * @param is the input stream
   * @return
   */
  public static String slurpString(InputStream is) {
    String ret = null;
    try {
      InputStreamReader isr = new InputStreamReader(is, "UTF-8");
      int ch;
      StringBuffer sb = new StringBuffer();
      do {
        ch = isr.read();
        if (ch != -1) sb.append((char) ch);
      } while (ch != -1);
      ret = sb.toString();
      // if (verbose)GLog.v(TAG, "read file:"+path);
    } catch (Exception ex) {
      GLog.printStackTrace(TAG, ex);
      // fail(ex.toString(), false);
    }
    return ret;
  }

  /**
   * 
   * @param o
   * @return
   */
  public static String nullS(Object o) {
    String ret = null;
    try {
      ret = o.toString();
    } catch (Exception e) {}
    return ret;
  }

  /**
   * 
   * @param trys
   * @param fallback
   * @return
   */
  public static String defaultString(String trys, String fallback) {
    if (trys == null || trys.length() == 0) return fallback;
    return trys;
  }

  /**
   * Return o as a string if possible, otherwise throw detailed runtime exception.
   * 
   * @param str
   * @param msg
   * @return
   */
  public static String check(String str, String msg) {
    if (str == null || str.trim().length() == 0) { throw new IllegalArgumentException(msg); }
    return str;
  }

  /**
   * Resurn 128bit digest which is created by the signature of APK.
   * @param context the context of application
   * @return digest created by the signature of APK.
   */
  public static byte[] getScrambleDigest(Context context) {
    String signature = null;
    PackageInfo packageInfo;
    try {
      packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
      Digest digest = new Digest("SHA-1");
      byte[] digest_byte = digest.getDigestInByteArray(packageInfo.signatures[0].toByteArray());
      signature = Base64.encodeBytes(digest_byte);
    } catch (NameNotFoundException e) {
      GLog.printStackTrace(TAG, e);
      return null;
    } catch (NoSuchAlgorithmException e) {
      GLog.printStackTrace(TAG, e);
      return null;
    }
    byte[] result = null;
    try {
      Digest digest = new Digest("MD5");
      result = digest.getDigestInByteArray(signature.getBytes());
    } catch (NoSuchAlgorithmException e) {
      GLog.printStackTrace(TAG, e);
    }
    return result;
  }
  
  /**
   * decrypter for consumer key/secret
   * @param key 128bit key
   * @param src string of decrypt target
   * @return the value of decrypted
   */
  public static String decryptConsumer(byte[] key, String src) {
    if (key == null || src == null) {
      return null;
    }
    String decrypted = null;
    try {
      Cipher decryptionCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      IvParameterSpec ivspec = new IvParameterSpec(key);
      Key skey = new SecretKeySpec(key, "AES");
      decryptionCipher.init(Cipher.DECRYPT_MODE, skey, ivspec);
      byte[] src_byte = Base64.decode(src);
      byte[] decryptedText = decryptionCipher.doFinal(src_byte);
      decrypted = new String(decryptedText, "UTF-8");
    } catch (NoSuchAlgorithmException e) {
      GLog.printStackTrace(TAG, e);
    } catch (NoSuchPaddingException e) {
      GLog.printStackTrace(TAG, e);
    } catch (InvalidKeyException e) {
      GLog.printStackTrace(TAG, e);
    } catch (IllegalBlockSizeException e) {
      GLog.printStackTrace(TAG, e);
    } catch (BadPaddingException e) {
      GLog.printStackTrace(TAG, e);
    } catch (UnsupportedEncodingException e) {
      GLog.printStackTrace(TAG, e);
    } catch (IOException e) {
      GLog.printStackTrace(TAG, e);
    } catch (InvalidAlgorithmParameterException e) {
      GLog.printStackTrace(TAG, e);
    }
    return decrypted;
  }

  /**
   * 
   * @param context
   * @param url
   * @return
   */
  public static boolean startBrowser(Context context, String url) {
    if (!url.startsWith("http:") && !url.startsWith("https:")) { return false; }
    Uri uri = Uri.parse(url);
    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
    context.startActivity(intent);
    return true;
  }
  /**
   * Launch the Reward activity with given url.
   * @param context
   * @param url a url with scheme starting with greeapp607://gree-reward-offerwall
   * @return whether or not the url is GreeReward's
   */
  public static boolean showRewardOfferWall(Context context, String url) {
    if (!url.startsWith(Scheme.getRewardOfferWallScheme())) { return false; }

    Bundle bundle = new Bundle();
    String userId = Injector.getInstance(IAuthorizer.class).getOAuthUserId();
    if (userId == null) {
        GLog.e(TAG, "user id is null");
    }
    bundle.putString("IDENTIFIER", userId);
    bundle.putString("app_id", Core.getAppId());
    String scheme = Scheme.getRewardOfferWallScheme();
    Intent intent = new Intent(Intent.ACTION_DEFAULT, Uri.parse(scheme)).putExtras(bundle);
    try {
        context.startActivity(intent);
    } catch (ActivityNotFoundException e) {
        GLog.e(TAG, "error occured: open GreeReward offerwall");
        GLog.printStackTrace(TAG, e);
    }
    
    return true;
  }
  
  /**
   * It returns whether or not the network is connected.
   * 
   * @param context the context
   * @return whether or not the network is connected
   */
  public static boolean isNetworkConnected(Context context) {
    ConnectivityManager cm =
        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    if (cm.getActiveNetworkInfo() != null) {
      return cm.getActiveNetworkInfo().isConnected();
    } else {
      return false;
    }
  }

  /**
   * Returns whether the developer allow user to opt out of GREE.
   * If it is true, user can play without logging in.
   * 
   * @return whether user can opt out of GREE
   */
  public static boolean canOptOutOfGREE() {
    String enableGrade0 = CoreData.get(InternalSettings.EnableGrade0);
    if (enableGrade0 != null) {
      GLog.w(TAG, "[DEPRECATED]  enableGrade0 setting was deprecated. Instead, you should use the allowUserOptOutOfGREE setting.");
      if ("true".equals(enableGrade0)) {
        return true;
      }
    }

    String allowUserOptOutOfGREE = CoreData.get(InternalSettings.AllowUserOptOutOfGREE);
    return "true".equals(allowUserOptOutOfGREE);
  }

  /**
   * This function check whether an Activity is closing or not.
   * @param context Context of Activity
   * @return When this function return true, Activity is closing. So, please skip a sequence to show some contents to the Activity.
   */
  public static boolean activityIsClosing(Context context) {
    if (context instanceof Activity && Activity.class.cast(context).isFinishing()) {
      GLog.d(TAG, "Now Activity is closing.");
      return true;
    }
    return false;
  }
  
  
  /**
   * Return true if the URL String is Android Market
   *
   * @param url
   * @return
   */
  public static boolean isAndroidMarketUrl(String url) {
    return (url.startsWith("http://market.android.com")
        || url.startsWith("https://market.android.com") || url.startsWith("http://play.google.com")
        || url.startsWith("https://play.google.com") || url.startsWith("market://"));
  }

  /**
   * Enforce the runnable run in main thread.
   * @param runnable to be run
   */
  
  public static void runOnUiThread(Runnable runnable) {
    Looper looper = Looper.getMainLooper();
    if (looper.getThread() == Thread.currentThread())
      runnable.run();
    else {
      Handler handler = new Handler(looper);
      handler.post(runnable);
    }
  }

  /**
   * This function check whether the application is foreground or background.
   * @return if true is returned, the application is foreground.
   */
  public static boolean isForeground() {
    Context context = Core.getInstance().getContext().getApplicationContext();
    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    boolean isScreenOn = pm.isScreenOn();
    if (isScreenOn == false) {
      GLog.d(TAG, "Screen is dim");
      return false;
    }
    ActivityManager activityManager =
        (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
    if (appProcesses == null) {
      GLog.d(TAG, "app is background");
      return false;
    }
    final String packageName = context.getPackageName();

    List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(1);
    if (!tasks.isEmpty()) {
      ComponentName topActivity = tasks.get(0).topActivity;
      if (topActivity != null && topActivity.getPackageName().equals(packageName)) {
        GLog.d(TAG, "app is forground");
        return true;
      }
    }
    GLog.d(TAG, "app is background");
    return false;
  }

  public static boolean hasFroyo() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
  }

  public static boolean hasHoneycombMR1() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
  }

  public static boolean hasHoneycomb() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
  }

  public static int timezoneOffsetMinutes() {
    int tz_offset = TimeZone.getDefault().getOffset((new Date()).getTime());
    return (int)(tz_offset / 1000 / 60); // convert msec to min
  }

  public static String getOsName() {
    if (Build.VERSION.SDK_INT >= 16) { // Build.VERSION_CODES.JELLY_BEAN
      return "Jelly bean";
    } else if (Build.VERSION.SDK_INT >= 14) { // Build.VERSION_CODES.ICE_CREAM_SANDWICH
      return "Ice cream sandwich";
    } else if (Build.VERSION.SDK_INT >= 11) { // Build.VERSION_CODES.HONEYCOMB
      return "Honeycomb";
    } else if (Build.VERSION.SDK_INT >= 9) { // Build.VERSION_CODES.GINGERBREAD
      return "Gingerbread";
    } else if (Build.VERSION.SDK_INT >= 8) { // Build.VERSION_CODES.FROYO
      return "Froyo";
    } else {
      return "Eclair";
    }
  }

  public static String getDensityType(int densityDpi) {
    if (densityDpi >= 480) {
      return "xxhigh";
    } else if (densityDpi >= 320) {
      return "xhigh";
    } else if (densityDpi >= 240) {
      return "high";
    } else if (densityDpi >= 160) {
      return "medium";
    } else {
      return "low";
    }
  }
  
  public static String escape(String original) {
    String encoded = "";
    try {
      encoded = URLEncoder.encode(original, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      GLog.printStackTrace(TAG, e);
    }
    return encoded;
  }
}
