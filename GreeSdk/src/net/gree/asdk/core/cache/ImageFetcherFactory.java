package net.gree.asdk.core.cache;

import net.gree.asdk.core.Core;
import android.content.Context;

public class ImageFetcherFactory {

	/**
	 * Creates a new wrapper class of the ImageFetcher
	 */
	public static ImageFetcher createImageFetcher() {
		Context ctx = Core.getInstance().getContext();
		// creating new instance
		ImageFetcher imageFetcher = new ImageFetcher(ctx);
		// create the Image Cache for our singleton instance
		// building the image cache parameters
		ImageCache.Params cacheParams = new ImageCache.Params(
				ImageFetcherFactory.class.getSimpleName());
		cacheParams.mDiskCacheSize = 50 * 1024 * 1024;// google recommends 1MB,
														// per app
		cacheParams.mEnableDiskCache = true;
		// use fraction of supported size
		cacheParams.mMemCacheSize = (int) (Runtime.getRuntime().maxMemory() / 4); 
		cacheParams.mEnableMemCache = true;
		// setting the ImageFetcher to use the image cache
		ImageCache imageCache = new ImageCache(ctx, cacheParams);
		imageFetcher.setImageCache(imageCache);

		// returns the image fetcher product
		return imageFetcher;
	}

}
