/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.inject;

import java.util.Map;

public final class InternalInjector {

  private Map<Class<?>, Object> instances;

  public InternalInjector(Map<Class<?>, Object> instances) {
    this.instances = instances;
  }

  /**
   * Create injector through modules
   * 
   * @param modules modues to be used to created InternalInjector
   * @return
   */
  public static InternalInjector createInjector(AbstractModule... modules) {
    Binder binder = Binder.startBinding();
    for (AbstractModule module : modules) {
      module.configure(binder);
    }
    binder.finishBinding();
    return new InternalInjector(binder.getInstances());
  }

  /**
   * Get an instance for a clz.
   * 
   * @param clz
   * @return A instance of clz
   */
  @SuppressWarnings("unchecked")
  public <T> T getInstance(Class<T> clz) {
    return (T) instances.get(clz);
  }

  /**
   * Directly binds an instance to a Class. Only to be used in the unittest, cause it's necessary to
   * inject a mock implementations to injector.
   * 
   * @param clz
   * @param instance
   */
  public void bind(Class<?> clz, Object instance) {
    instances.put(clz, instance);
  }

}
