/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.updateprofile;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfileUpdater {
  private static final String TAG = ProfileUpdater.class.getSimpleName();

  /**
   * Interface for listening to results for updating profile
   * This class is not used since this feature has been merged into Pincode class.
   */
  public interface UpdateListener {
    public void onSuccess();
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Trigger the requests to update profile
   * @param nickname new nickname
   * @param year year of the birthday
   * @param month month of the birthday
   * @param day day of the birthday
   * @param listener the listener to follow the request
   */
  public void update(String nickname, int year, int month, int day, final UpdateListener listener) {
    JSONObject params = new JSONObject();
    try {
      params.put("nick_name", nickname);
      JSONObject birth = new JSONObject();
      birth.put("year", String.format("%04d", year));
      birth.put("month", String.format("%02d", month));
      birth.put("day", String.format("%02d", day));
      params.put("birth", birth);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(0, null, e.getMessage());
        return;
      }
    }
    new JsonClient().http(Url.getUpdateProfile(), BaseClient.METHOD_POST, params.toString(), false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onSuccess();
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
 
  
  /**
   * Trigger the requests to update the nickname
   * can only be used once on grade 1 users
   * @param nickname new nickname
   * @param listener the listener to follow the request
   */
  public void register(String nickname, final UpdateListener listener) {
    JSONObject params = new JSONObject();
    try {
      params.put("nickname", nickname);
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(0, null, e.getMessage());
        return;
      }
    }
    new JsonClient().oauth(Url.getRegisterNickname(), "POST", null, params.toString(), false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onSuccess();
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
}
