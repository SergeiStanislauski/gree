package net.gree.asdk.core.auth;

import android.content.Context;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.auth.SetupActivity.SetupListener;
import net.gree.asdk.core.util.DeviceInfo;
import net.gree.asdk.core.util.Util;

public abstract class AuthorizeSequenceBase {
  protected static final String QUERY_PARAM_SERVICE_CODE = "service_code";

  protected Context mContext;
  protected String mServiceCode;
  protected SetupListener mListener;

  protected IAuthorizer mAuthorizer;
  
  protected boolean mHasUuid;

  public AuthorizeSequenceBase(Context context, String serviceCode) {
    mContext = context;
    mServiceCode = serviceCode;
    mAuthorizer = Injector.getInstance(IAuthorizer.class);
    
    mHasUuid = (DeviceInfo.getUuid() != null) ;

  }

  public void execute(SetupListener listener) {
    if (Util.canOptOutOfGREE() && !Util.isNetworkConnected(mContext)) {
      if (listener != null) listener.onCancel();
      return;
    }
    mListener = listener;
    run();
  }

  protected abstract void run();
}
