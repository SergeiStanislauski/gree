/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.notifications.ui;

import java.util.TreeMap;

import net.gree.asdk.api.GreePlatformSettings;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;
import net.gree.asdk.core.analytics.Logger;
import net.gree.asdk.core.notifications.MessageDescription;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Parcelable;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.ActionMode.Callback;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * This class displays a message for notification.
 * This message is able to click it.
 */
public class ClickableNotification {
  private static final String TAG = "ClickableNotification";
  private PopupWindow mPopup;
  private Activity mActivity;
  private Window mWindow;
  private Window.Callback mCallback;

  /**
   * Trigger the notification
   *
   * @param context any valid context
   * @param md the message to be displayed
   * @param activity activity for popup window
   */
  public void notify(Context context, final MessageDescription md, Activity activity) {
    
    if (md == null) {
      GLog.d(TAG, "Not found MessageDescription.");
      return;
    }

    if (activity == null || activity.isFinishing()) {
      GLog.d(TAG, "Activity Finished.");
      return;
    }

    mActivity = activity;
    mWindow = activity.getWindow();
    mCallback = activity.getWindow().getCallback();
    mWindow.setCallback(WindowCallback);
    
    boolean isAtBottom = GreePlatformSettings.isNotificationsAtScreenBottom();

    LayoutInflater inflater = LayoutInflater.from(context);
    View notificationRoot = inflater.inflate(RR.layout("gree_internal_notification"), null);
    TextView messageView = (TextView) notificationRoot.findViewById(RR.id("gree_notificationMessageTextView"));
    messageView.setText(md.getMessage());

    ImageView iconImageView = (ImageView) notificationRoot.findViewById(RR.id("gree_notificationImageView"));
    Bitmap bitmap = md.getBitmapIcon();
    if (bitmap != null) {
      iconImageView.setImageBitmap(bitmap);
    } else if (md.getIconId() > 0) {
      iconImageView.setImageResource(md.getIconId());
    }
    
    int badge = md.getBadge();
    TextView notificationCount = (TextView) notificationRoot.findViewById(RR.id("gree_notificationCountImageView"));
    if (badge > 0) {
      notificationCount.setVisibility(View.VISIBLE);
      if (badge > 99) {
        notificationCount.setText("99+");
      } else {
        notificationCount.setText(""+badge);
      }
    } else {
      notificationCount.setVisibility(View.GONE);
    }
    
    View caret = notificationRoot.findViewById(RR.id("gree_notificationCaretImageView"));
    caret.setVisibility(View.VISIBLE);

    notificationRoot.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        mWindow.setCallback(mCallback);
        mPopup.dismiss();
        Intent intent = md.getOnClickIntent();
        if (intent != null) {
          if (md.getType() == MessageDescription.FLAG_NOTIFICATION_MY_LOGIN) {
            Logger.recordLog("pg", "http://notice.gree.net/game", "in_game_notification", null);
          } else if (md.getType() == MessageDescription.FLAG_NOTIFICATION_FRIEND_LOGIN) {
            TreeMap<String, String> map = new TreeMap<String, String>();
            map.put("user_id", md.getUid());
            Logger.recordLog("pg", "http://notice.gree.net/game", "in_game_notification", map);
          }
          NotificationPage.launch(mActivity);
        }
      }
    });

    mPopup = new PopupWindow(notificationRoot, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, false);
    mPopup.setContentView(notificationRoot);
    mPopup.setBackgroundDrawable(new BitmapDrawable(activity.getResources()));
    mPopup.setTouchable(true);
    mPopup.setFocusable(false);
    mPopup.setOutsideTouchable(true);
    mPopup.setAnimationStyle(android.R.style.Animation_Dialog);

    mPopup.setTouchInterceptor(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
          return true;
        }
        return false;
      }
    });

    View view = activity.getWindow().getDecorView();
    
    if (isAtBottom) {
      mPopup.showAtLocation(view, Gravity.BOTTOM, 0, 0);
    } else {
      Rect rect = new Rect();
      view.getWindowVisibleDisplayFrame(rect);
      mPopup.showAtLocation(view, Gravity.TOP, 0, rect.top);
    }
  }

  /**
   * Dismiss current notification
   */
  public void dismiss() {
    if (mWindow != null) {
      mWindow.setCallback(mCallback);
    }
    if (mPopup != null) {
      mPopup.dismiss();
    }
  }
  
  
  private Window.Callback WindowCallback = new Window.Callback() {
    private void dispatchBackButton(KeyEvent event) {
      mWindow.setCallback(mCallback);
      if (mPopup != null && event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
        mPopup.dismiss();
      }
    }
    
    private void dispatchBackButton(MotionEvent event) {
      mWindow.setCallback(mCallback);
      if (mPopup != null && event.getAction() == MotionEvent.BUTTON_BACK) {
        mPopup.dismiss();
      }
    }

    @TargetApi(11)
    @Override
    public ActionMode onWindowStartingActionMode(Callback callback) {
      return null;
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {}
    
    @Override
    public void onWindowAttributesChanged(android.view.WindowManager.LayoutParams attrs) {}
    
    @Override
    public boolean onSearchRequested() {
      return false;
    }
    
    @Override
    public boolean onPreparePanel(int featureId, View view, Menu menu) {
      return false;
    }
    
    @Override
    public void onPanelClosed(int featureId, Menu menu) {}
    
    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
      return false;
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
      return false;
    }
    
    @Override
    public void onDetachedFromWindow() {
      if (mPopup != null) {
        mPopup.dismiss();
        mPopup = null;
      }
      mWindow.setCallback(mCallback);
    }
    
    @Override
    public View onCreatePanelView(int featureId) {
      return null;
    }
    
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
      return false;
    }
    
    @Override
    public void onContentChanged() {}
    
    @Override
    public void onAttachedToWindow() {}
 
    @TargetApi(11)
    @Override
    public void onActionModeStarted(ActionMode mode) {}

    @TargetApi(11)
    @Override
    public void onActionModeFinished(ActionMode mode) {}
    
    @Override
    public boolean dispatchTrackballEvent(MotionEvent event) {
      dispatchBackButton(event);
      mWindow.getCallback().dispatchTrackballEvent(event);
      return false;
    }
    
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
      mWindow.setCallback(mCallback);
      mWindow.getCallback().dispatchTouchEvent(event);
      return true;
    }
    
    @Override
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
      mWindow.setCallback(mCallback);
      mWindow.getCallback().dispatchPopulateAccessibilityEvent(event);
      return false;
    }
    
    @TargetApi(11)
    @Override
    public boolean dispatchKeyShortcutEvent(KeyEvent event) {
      dispatchBackButton(event);
      mWindow.getCallback().dispatchKeyShortcutEvent(event);
      return false;
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
      dispatchBackButton(event);
      mWindow.getCallback().dispatchKeyEvent(event);
      return true;
    }
    
    @TargetApi(12)
    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent event) {
      dispatchBackButton(event);
      mWindow.getCallback().dispatchGenericMotionEvent(event);
      return true;
    }
  };
}
