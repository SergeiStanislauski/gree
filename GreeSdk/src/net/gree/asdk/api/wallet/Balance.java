/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api.wallet;

import net.gree.asdk.core.wallet.BalanceData;
import net.gree.asdk.core.wallet.BalanceData.BalanceDataListener;

import org.apache.http.HeaderIterator;

  /**
   * <p>Balance API provides the function to get balance of coins.</p>
   *
   * Sample code:<br>
   * <code><pre>
   * Balance.load(new BalanceListener() {
   *   public void onSuccess(long balance) {
   *      Toast.makeText(this, &quot;Balance.load success.[&quot; + balane + &quot;]&quot;, Toast.LENGTH_LONG).show();
   *   }
   *
   *    public void onFailure(int responseCode, HeaderIterator headers, String paymentId, String response) {
   *      Toast.makeText(this, &quot;Balance.load failed.[&quot; + responseCode + &quot; &quot; + response + &quot;]&quot;, Toast.LENGTH_LONG).show();
   *    }
   *  });
   * </pre></code>
   * @author GREE, Inc.
   */
public class Balance {
 
  /**
   * Interface which receives the results of the Balance API
   */
  public interface BalanceListener {
   
    /**
     * Called upon successful completion of getting the balance for the current user.
     * @param balance balance which you request.
     */
    public void onSuccess(long balance);
   
      /**
       * Called when something went wrong when trying to get the balance for the current user.
       * @param responseCode Integer representing the http response code.
       * @param headers Object representing the headers of the http response.
       * @param response String representing the body of the http response.
       */
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

 
  /**
   * Loads the balance for the current user.
   * @param listener receives results of this load.
   */
  public void load(final BalanceListener listener) {
    BalanceData.load(new BalanceDataListener() {
      public void onSuccess(BalanceData balance) {
        if (listener != null) {
          listener.onSuccess(balance.getBalance());
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
}
