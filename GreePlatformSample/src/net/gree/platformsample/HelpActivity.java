/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import net.gree.asdk.api.GreePlatform;
import net.gree.platformsample.fragment.HelpListFragment;

/**
 * The activity for the help page.
 * 
 * Fire off the Intent with an extra called "ITEM" to open to a specific part of the help page
 * Put the string resource for the title of the page you want to open e.g. R.string.leaderboard_title
 * Put a blank string in "ITEM" to open the root view of the help page
 */
public class HelpActivity extends BaseActivity {
  private static final String TAG = "HelpActivity";
  private String mItem;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(R.layout.help_page);
    
    //Store the location in the help page to jump directly to
    mItem = getIntent().getStringExtra("ITEM");
    
    if (mItem == null){
      mItem = "";
    }
    
    setupFragment();    
  }
  
  /**
   * Add the ListFragment to this activity
   */
  public void setupFragment(){
    Log.d(TAG,"Opened the help page from "+mItem);
    
    HelpListFragment fragment = (HelpListFragment) getSupportFragmentManager().findFragmentByTag("help_list");
    if (fragment == null){
      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      fragment = HelpListFragment.newInstance(mItem);
      ft.replace(R.id.help_list_layout, fragment, "help_list");
      ft.commit();
    }
  }

  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpAutoLoadMore();
  }

  public void setUpBackButtonToBack() {
    back = (Button) findViewById(R.id.btn_back);
    if (back != null) {
      back.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          HelpActivity.this.onBackPressed();
        }
      });
    } else {
      Log.e(TAG, "no back button");
    }
  }

  public void setUpBackButtonToClose() {
    Button back = (Button) findViewById(R.id.btn_back);
    if (back != null) {
      back.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          finish();
        }
      });
    } else {
      Log.e(TAG, "no back button");
    }
  }
  
  @Override
  protected void sync(boolean fromStart) {
   
  }

}
