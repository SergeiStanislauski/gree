/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.inject;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotates constructorss of your implementation class into which the {@link InternalInjector} should
 * inject values. The InternalInjector fulfills injection requests for every instance it constructs. The
 * class being constructed must have exactly one of its constructors marked with {@code @Inject} or
 * must have a constructor taking no parameters.
 */
@Target({CONSTRUCTOR})
@Retention(RUNTIME)
@Documented
public @interface Inject {}
