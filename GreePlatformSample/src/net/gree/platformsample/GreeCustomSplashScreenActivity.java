package net.gree.platformsample;

import net.gree.asdk.api.auth.Authorizer;
import net.gree.asdk.api.auth.Authorizer.AuthorizeListener;
import net.gree.asdk.api.auth.Authorizer.UpdatedLocalUserListener;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.platformsample.util.SampleUtil; 

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * This class is responsible for being the login screen of the application. It interacts with 
 * the code so that we provide you the developer with direct methods to replace this and implement
 * your own login protocol. Eg, when using the direct login API there is no intermediate popup type login window. 
 * All authentication logic is handled via a web browser, and then returned back to the application.
 * You are free to replace and customize this class however you see fit. Also, please customize the login (splash screen)
 * however you wish.
 * 
 *
 */
public class GreeCustomSplashScreenActivity extends BaseActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.native_splash_screen);
		// init the UI
		initUI();
		
	}
	
	private void initUI() {
	  // Create listeners for buttons
	  Button loginAsNormalUser = (Button) findViewById(R.id.splash_scn_login_btn);
	  // set click listeners
	  loginAsNormalUser.setOnClickListener(LiteUserClickListener);
	  
	  // Sets the hyperlink for the TextView "Sign in with GREE"
	  TextView login = (TextView)findViewById(R.id.splash_sign_in_txt);
	  login.setOnClickListener(NormalLoginClockListener);
	  login.setTextColor(Color.rgb(0, 190, 235));
	  
	  // Sets the hyperlink for the TextView "Terms of Use"
	  TextView tos = (TextView)findViewById(R.id.splash_tos_txt);
	  tos.setText(
          Html.fromHtml(getString(R.string.by_continuing, "<a href=" + Authorizer.getTosUrl() + ">"
	  + getString(R.string.tos)+"</a> ")));
	  tos.setLinkTextColor(Color.rgb(0, 190, 235));
	  tos.setMovementMethod(LinkMovementMethod.getInstance());
		
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    setContentView(R.layout.native_splash_screen);
	    // reset the UI
	    initUI();
	}

	// login the user with the direct login for a normal user
	private OnClickListener NormalLoginClockListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			directNormalLogin();
		}
	};
	
	// login the user with the direct login for a normal user
	private OnClickListener LiteUserClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			finish();
			//directLiteLogin();
		}
	};

	/**
	 * Launches a new direct login API call for a Normal User
	 */
	private void directNormalLogin() {
		Authorizer.authorize(this, new AuthorizeListener() {
			public void onAuthorized() {
				SampleUtil.showSuccess(GreeCustomSplashScreenActivity.this, "Login");
				// close the activity on a success
				GreeCustomSplashScreenActivity.this.finish();
			}

			public void onCancel() {
				AsyncErrorDialog errorDialog = new AsyncErrorDialog(GreeCustomSplashScreenActivity.this);
				errorDialog.show();
			}

			public void onError() {
				SampleUtil.showError(GreeCustomSplashScreenActivity.this, "Login");
			}
		}, new UpdatedLocalUserListener() {
			@Override
			public void onUpdateLocalUser() {
				tryLoginAndLoadProfilePage();
			}
		}, Authorizer.NORMAL_USER); 
	}
	
	/**
	 * Launches a new direct login API call for a Lite User
	 */
	private void directLiteLogin() {
		Authorizer.authorize(GreeCustomSplashScreenActivity.this, new AuthorizeListener() {
			public void onAuthorized() {
				SampleUtil.showSuccess(GreeCustomSplashScreenActivity.this, "Login");
				// close the activity on a success
				GreeCustomSplashScreenActivity.this.finish();
			}

			public void onCancel() {
				AsyncErrorDialog errorDialog = new AsyncErrorDialog(GreeCustomSplashScreenActivity.this);
				errorDialog.show();
			}

			public void onError() {
				SampleUtil.showError(GreeCustomSplashScreenActivity.this, "Login");
			}
		}, new UpdatedLocalUserListener() {
			@Override
			public void onUpdateLocalUser() {
				tryLoginAndLoadProfilePage();
			}
		}, Authorizer.LITE_USER);
	}



	@Override
	protected void sync(boolean fromStart) {
		// method unused in this activity 
	}

}
