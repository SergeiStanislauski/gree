/**
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.api.ui;

import java.util.TreeMap;

import net.gree.asdk.api.ScreenShot;
import net.gree.asdk.core.analytics.Logger;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

/**
 * ScreenShotButton<br>
 * This extends an ImageButton Widget.<br>
 * When clicked this will take a screenShot from the root view and create a ShareDialog with the application name as a message. <br>
 * You can find this view by searching for android:id="id/screenshot_button"<br>
 * e.g :
 * <pre> 
 * ScreenShotButton button = (ScreenShotButton) findViewById(R.id.screenshot_button);
 * button.setOnClickListener(new OnClickListener() {
 *    public void onClick(View view) {
 *          ShareDialog shareDialog = new ShareDialog(getContext());
 *          shareDialog.setMessage("This is my message");
 *          shareDialog.setImage(ScreenShot.capture(getRootView()));
 *          shareDialog.show();
 *    }
 *  });
 * </pre>
 * @author GREE, Inc.
 */
public class ScreenShotButton extends ImageButton {
  private ShareDialog mShareDialog;
  private CustomScreenShot mCustomScreenShot = null;

  {
    init();
  }

  /**
   * Default constructor
   * @param context application context
   */
  public ScreenShotButton(Context context) {
    super(context);
  }

  /**
   * Constructor with attributes
   * @param context application context
   * @param attrs this view attributes
   */
  public ScreenShotButton(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  /**
   * Constructor with attributes
   * @param context application context
   * @param attrs this view attributes
   * @param defStyle default style
   */
  public ScreenShotButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  private void init() {
    this.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        if (AsyncErrorDialog.shouldShowErrorDialog(getContext())) {
          AsyncErrorDialog dialog = new AsyncErrorDialog(getContext());
          dialog.show();
          return;
        }

        TreeMap<String, Object>  params = new TreeMap<String, Object>();

        if (mShareDialog == null) {
          mShareDialog = new ShareDialog(getContext());
        }

        Bitmap image = null;
        if (mCustomScreenShot != null) {
          image = mCustomScreenShot.getCaptureImage();
        }
        if (image == null) {
          image = ScreenShot.capture(getRootView());
        }
        params.put("image", image);
        mShareDialog.setParams(params);
        mShareDialog.show();
        
        String name = mShareDialog.getDisplayUrl();
        Logger.recordLog("pg", name, "", null);
      }
    });
  }

  /**
   * Sets the ShareDialog that will be called when clicking on this ImageButton<br>
   * <b>Note:</b> this is optional. If you do not call this, a new ShareDialog will be created.
   * @param shareDialog the shareDialog to be called
   */
  public void setShareDialog(ShareDialog shareDialog) {
    mShareDialog = shareDialog;
  }

  /**
   * Gets the ShareDialog that is called when clicking on the ImageButton,
   * this is null until the button is clicked once.
   * @return the last used ShareDialog.
   */
  public ShareDialog getShareDialog() {
    return mShareDialog;
  }

  /**
   * The interface which provides the Intermediate/Field to customize an image of screen shot.
   */
  public interface CustomScreenShot {
 
    /**
     * This is the function that is called when a user initiates a screen shot by touching the screen shot button.
     * When this function is called, you have to return Bitmap instance to display on {@link net.gree.asdk.api.ui.ShareDialog ShareDialog}.
     * Bitmap instance should be created each time to be called this function.
     * @return Bitmap instance to display on {@link net.gree.asdk.api.ui.ShareDialog ShareDialog}.
     */
 
    public Bitmap getCaptureImage();
  }

  /**
   * When you want to customize the image of screen shot, set the instance of CustomScreenShot by this function.
   * @param customScreenShot The instance of {@link net.gree.asdk.api.ui.ScreenShotButton.CustomScreenShot CustomScreenShot}.
   */
  public void setCustomScreenShot(CustomScreenShot customScreenShot) {
    mCustomScreenShot = customScreenShot;
  }
}
