/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core;

import net.gree.asdk.core.inject.InternalInjector;
import net.gree.asdk.core.notifications.NotificationModule;
import net.gree.asdk.core.track.TrackModule;
import net.gree.asdk.core.util.Preconditions;

import android.content.Context;



/**
 * Sample dependency injection.
 */
public final class Injector {

  @SuppressWarnings("unused")
  private final static String TAG = "Injector";

  private volatile static InternalInjector internalInjector;

  /**
   * Initialize the Injector, should call once.
   * 
   * @param context Android Context, should not null
   * @throws NullPointerException if context is null
   */
  public static synchronized void init(Context context) {
    Preconditions.checkNotNull(context, "Context is null");
    internalInjector =
        InternalInjector.createInjector(new CoreModule(context), new TrackModule(),
            new NotificationModule());
  }

  /**
   * Get instance for type
   * 
   * @return The implementation of cls
   */
  public static <T> T getInstance(Class<T> cls) {
    return internalInjector.getInstance(cls);
  }

  /**
   * Directly binds an instance to a Class.
   * 
   * @param clz
   * @param instance
   */
  public static void bind(Class<?> clz, Object instance) {
    internalInjector.bind(clz, instance);
  }
}
