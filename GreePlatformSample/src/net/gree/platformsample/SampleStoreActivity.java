/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample;

import java.util.ArrayList;

import org.apache.http.HeaderIterator;
import org.apache.http.HttpStatus;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.api.wallet.Payment;
import net.gree.asdk.api.wallet.PaymentItem;
import net.gree.asdk.api.wallet.Payment.PaymentListener;

public class SampleStoreActivity extends BaseActivity {
  private static final String TAG = "SampleStoreActivity";
  // three options for the sample items for purchase
  LinearLayout mSampleStoreLLTop, mSampleStoreLLMid, mSampleStoreLLBot;
  // array list containing the items for purchase
  private ArrayList<PaymentItem> mPaymentItems = new ArrayList<PaymentItem>();
  // hard coding the identifiers to facilitate switch statements (make them easier to read)
  private final static int SWORD_OF_AWESOME = R.id.sample_store_ll_wrapper_top;
  private final static int BAG_OF_SILVER = R.id.sample_store_ll_wrapper_mid;
  private final static int COMBO_PACK = R.id.sample_store_ll_wrapper_bot;
  // The URL for the payment request
  private final static String URL = null;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Standard GGP calls and call to load the UI
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.sample_store);

    // initalize the UI
    initUI();
    // sets the clickListeners
    setClickListeners();
  }

  private void initUI() {
    mSampleStoreLLTop = (LinearLayout) findViewById(R.id.sample_store_ll_wrapper_top);
    mSampleStoreLLMid = (LinearLayout) findViewById(R.id.sample_store_ll_wrapper_mid);
    mSampleStoreLLBot = (LinearLayout) findViewById(R.id.sample_store_ll_wrapper_bot);
   
  }

  private void setClickListeners() {
    mSampleStoreLLTop.setOnClickListener(SampleStoreClickListener);
    mSampleStoreLLMid.setOnClickListener(SampleStoreClickListener);
    mSampleStoreLLBot.setOnClickListener(SampleStoreClickListener);

  }
  
  /** adds the items from our sample store to the array list for purchase **/
  private void addSampleItems(int purchaseItem) {
    if (purchaseItem == SWORD_OF_AWESOME) {
		PaymentItem item = getItemFromArray(getResources().getStringArray(R.array.sword_of_awesome_array));
		addPaymentItem(item);
	} else if (purchaseItem == BAG_OF_SILVER) {
		PaymentItem item2 = getItemFromArray(getResources().getStringArray(R.array.silver_coin_array));
		addPaymentItem(item2);
	} else if (purchaseItem == COMBO_PACK) {
		PaymentItem item3 = getItemFromArray(getResources().getStringArray(R.array.sword_of_awesome_array));
		addPaymentItem(item3);
		PaymentItem item4 = getItemFromArray(getResources().getStringArray(R.array.silver_coin_array_150));
		addPaymentItem(item4);
	}
    
  }
  /** constructs a  PaymentItem from the string array */
  private PaymentItem getItemFromArray(String[] stringArray) {
    PaymentItem item = new PaymentItem(
      stringArray[0], // id
      stringArray[1], // name
      Double.parseDouble(stringArray[4]), // unit price
      Integer.parseInt(stringArray[3]) // quantity
        );
    item.setDescription(stringArray[2]); // description
    
    // sets item image URL if available
    if(!stringArray[5].equals("TBD")){
      item.setImageUrl(stringArray[5]); // set to null until we are given the URLs
    }
    //returns the item
    return item;
  }

  private void addPaymentItem(PaymentItem item) {
    mPaymentItems.add(item);
  }

  /** creates the payment request once the items have been added */
  private void makePaymentRequest(String message) {
    if (0 < mPaymentItems.size()) {
      Payment payment = new Payment(message, mPaymentItems);
      // sets the callback URl for the Payment object
      if (URL != null) {
        payment.setCallbackUrl(URL);
      }

      // sets the Handler on the Payment object, instantiates a new instance of the interface
      payment.setHandler(new Handler() {
        public void handleMessage(Message message) {
          switch (message.what) {
            case Payment.OPENED:
              Toast.makeText(SampleStoreActivity.this, "Dialog opened", Toast.LENGTH_SHORT)
                  .show();
              break;
            case Payment.CANCELLED:
            case Payment.ABORTED:
              Toast.makeText(SampleStoreActivity.this, "Dialog closed", Toast.LENGTH_SHORT)
                  .show();
              break;
            default:
              break;
          }
        }
      });
      
      // sends the payment request
      payment.request(SampleStoreActivity.this, new PaymentListener() {
        // sends a Toast message on a success from making a payment for the item
        public void onSuccess(int responseCode, HeaderIterator headers, String paymentId) {
          Toast.makeText(SampleStoreActivity.this, "success", Toast.LENGTH_SHORT).show();
        }
        // sends a Toast message on a failure from making a payment for the item
        public void onFailure(int responseCode, HeaderIterator headers, String paymentId,
            String response) {
          Log.e(TAG, "Status code:" + responseCode + ", body: " + response);
          if (AsyncErrorDialog.shouldShowErrorDialog(SampleStoreActivity.this)) {
              AsyncErrorDialog dialog = new AsyncErrorDialog(SampleStoreActivity.this);
              dialog.show();
              return;
          } else if (HttpStatus.SC_OK <= responseCode && responseCode < HttpStatus.SC_BAD_REQUEST) {
            Toast.makeText(SampleStoreActivity.this, "Transaction failed: " + response,
                Toast.LENGTH_SHORT).show();
          } else {
            Toast.makeText(SampleStoreActivity.this, "Connection failed", Toast.LENGTH_SHORT)
                .show();
          }
        }
        // a cancel Toast message is raised here if the user cancels the payment
        public void onCancel(int responseCode, HeaderIterator headers, String paymentId) {
          Toast.makeText(SampleStoreActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
        }
      });
    }
    
  }

  private OnClickListener SampleStoreClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      // reset the array list
      mPaymentItems.clear();
      if (v.getId() == SWORD_OF_AWESOME) {
		// user clicked the Sword of Awesome
          addSampleItems(SWORD_OF_AWESOME);
		makePaymentRequest("Sword of Awesome");
	} else if (v.getId() == BAG_OF_SILVER) {
		// user clicked the Bag of Silver
          addSampleItems(BAG_OF_SILVER);
		makePaymentRequest("Bag of Silver");
	} else if (v.getId() == COMBO_PACK) {
		// user clicked the Value Pack
          addSampleItems(COMBO_PACK);
		makePaymentRequest("Value Pack");
	}
    }
  };

  @Override
  public void onOpenHelp(View v){
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.payment_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }
  
  @Override
  protected void sync(boolean fromStart) {}

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpBackButton();
    setUpAutoLoadMore();
  }
}
