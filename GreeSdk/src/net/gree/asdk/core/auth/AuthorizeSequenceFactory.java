/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.auth;

import net.gree.asdk.api.auth.Authorizer;
import android.content.Context;

public class AuthorizeSequenceFactory {
  public static AuthorizeSequenceBase getExecuter(Context context, String serviceCode, int targetGrade) {
    AuthorizeSequenceBase sequence = null;
    switch (targetGrade) {
      case Authorizer.LITE_USER:
        sequence = new LiteUserSequence(context, serviceCode);
        break;
      case Authorizer.NORMAL_USER:
        sequence = new NormalUserSequence(context, serviceCode);
        break;
      case AuthorizerCore.TARGET_RELOGIN:
        sequence = new ReloginSequence(context);
        break;
      default:
        sequence = new DialogSequence(context, serviceCode);
        break;
    }
    return sequence;
  }
}
