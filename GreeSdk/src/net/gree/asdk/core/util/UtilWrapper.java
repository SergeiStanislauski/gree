/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.util;

/**
 * The wapper of Util. Almost all methods of Util is static and it's hard to mock static method for
 * unit test, so create a wrapper class.
 */
public class UtilWrapper {

  /**
   * @see Util#canOptOutOfGREE()
   */
  public boolean canOptOutOfGREE() {
    return Util.canOptOutOfGREE();
  }

}
