/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import net.gree.asdk.api.GreePlatform;

/**
 * request select activity
 *
 */
public class RequestSelectActivity extends BaseActivity {
  private LinearLayout mGiftRequest, mCustomRequest;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Standard GGP calls and call to load the UI
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.request_select);


    // initalize the UI
    initUI();
    
    // sets the clickListeners
    setClickListeners();
  }

  private void initUI() {
    mGiftRequest = (LinearLayout) findViewById(R.id.request_select_ll_wrapper_top);
    mCustomRequest = (LinearLayout) findViewById(R.id.request_select_ll_wrapper_bot);

  }

  private void setClickListeners() {
    mGiftRequest.setOnClickListener(requestClickListener);
    mCustomRequest.setOnClickListener(requestClickListener);
  }

  private OnClickListener requestClickListener = new OnClickListener() {
    @Override
    public void onClick(View v) {
      Intent intent;
      if (v.getId() == R.id.request_select_ll_wrapper_top) {
		intent = new Intent(RequestSelectActivity.this, SendGiftActivity.class);
		startActivity(intent);
	} else if (v.getId() == R.id.request_select_ll_wrapper_bot) {
		intent = new Intent(RequestSelectActivity.this, RequestDialogActivity.class);
		startActivity(intent);
	} else {
	}
    }
  };

  @Override
  public void onOpenHelp(View v){
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.request_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }
  
  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpBackButton();
    setUpAutoLoadMore();
  }

  @Override
  protected void sync(boolean fromStart) { }

}
