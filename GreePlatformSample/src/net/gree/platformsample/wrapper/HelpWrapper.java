/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.wrapper;

/**
 * The wrapper for the help item
 */
public class HelpWrapper {
  private String mName;
  
  /**
   * Initializer
   * @param name Name
   */
  public HelpWrapper(String name){
    this.setName(name);
  }

  /**
   * Getter for the Name
   * @return Name
   */
  public String getName(){
    return this.mName;
  }
  
  /**
   * Setter for the Name
   * @param name Name
   */
  public void setName(String name){
    this.mName = name;
  }
  
}
