/*
* Copyright 2012 GREE, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package net.gree.asdk.core.analytics.performance;

import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import android.content.Context;

/**
 * PerformanceManager Dummy class.
 * If InternalSettings "enablePerformanceLogging" is not set, create object instead of PerformanceManager class.
 */
public class IPerformanceManager {

  public IPerformanceManager(Context context) {}

  /**
   *  This method is called, instead of inherited class when not created "PerformanceManager" class.
   *  It act as not-created class. so it always return null.
   */
  public PerformanceData createData(PerformanceFlowIndex flowIndex) { return null; }
  /**
   *  This method is called, instead of inherited class when not created "PerformanceManager" class.
   *  It act as not-created class. so it always return null.
   */
  public PerformanceData createData(PerformanceFlowIndex flowIndex, int subIndex) { return null; }
  /**
   *  This method is called, instead of inherited class when not created "PerformanceManager" class.
   *  It act as do-nothing class.
   */
  public void recordData(PerformanceData data, String key) {}
  /**
   *  This method is called, instead of inherited class when not created "PerformanceManager" class.
   *  It act as do-nothing class.
   */
  public void flushData(PerformanceData data) {}
}
