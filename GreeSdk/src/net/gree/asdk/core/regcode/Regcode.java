/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.regcode;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.Url;
import net.gree.vendor.com.google.gson.Gson;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

public class Regcode {
  private static final String TAG = Regcode.class.getSimpleName();

  /**
   * Interface for listening to results for updating profile
   */
  public interface RegcodeListener {
    public void onSuccess(String regcode, String entcode);
    public void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * Trigger the request to get regcode and entcode
   * @param serviceCode service name
   * @param listener the listener to follow the request
   */
  public void request(String serviceCode, RegcodeListener listener) {
    http(serviceCode, null, listener);
  }

  /**
   * Trigger the request to get regcode and entcode in case of upgrade
   * @param serviceCode service name
   * @param targetGrade grade the user is required to upgrade
   * @param listener the listener to follow the request
   */
  public void request(String serviceCode, int targetGrade, RegcodeListener listener) {
    http(serviceCode, targetGrade, listener);
  }

  private class Response {
    public String reg_code;
    public String ent_code;
  }

  private void http(String serviceCode, Integer targetGrade, final RegcodeListener listener) {
    JSONObject params = new JSONObject();
    try {
      params.put("service_code", serviceCode);
      if (targetGrade != null) {
        params.put("target_grade", targetGrade);
      }
    } catch (JSONException e) {
      GLog.printStackTrace(TAG, e);
      if (listener != null) {
        listener.onFailure(0, null, e.getMessage());
        return;
      }
    }
    new JsonClient().http(Url.getRegcode(), BaseClient.METHOD_POST, params.toString(), false, new OnResponseCallback<String>() {
      public void onSuccess(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          Gson gson = Injector.getInstance(Gson.class);
          Response res = gson.fromJson(response, Response.class);
          listener.onSuccess(res.reg_code, res.ent_code);
        }
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }
}
