/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.platformsample;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.api.ui.InviteDialog;

/**
 * Invite Activity Dialog
 * 
 */
public class InviteDialogActivity extends BaseActivity {
  private static final int _15 = 15;
  private static final String TAG = "InviteDialogActivity";
  private Button mInviteDialogButton;
  private EditText mInviteMessageText;
  private EditText mUserId1Text;
  private EditText mUserId2Text;
  private EditText mUserId3Text;

  private InviteDialog mInviteDialog;
  private Handler mHandler;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.invitedialog);
    mInviteDialog = new InviteDialog(this);

    mHandler = new Handler() {
      public void handleMessage(Message message) {
        switch (message.what) {
          case InviteDialog.OPENED:
            Toast.makeText(getApplicationContext(), "Invite Dialog opened.", Toast.LENGTH_SHORT)
                .show();
            Log.i(TAG, "Invite Dialog opened.");
            break;
          case InviteDialog.CLOSED:
            if (message.obj != null) {
              Toast.makeText(getApplicationContext(),
                  "Invite Dialog closed. result:[" + message.obj.toString() + "]",
                  Toast.LENGTH_SHORT).show();
              Log.i(TAG, "Invite Dialog closed. result:[" + message.obj.toString() + "]");
            } else {
              Toast.makeText(getApplicationContext(), "Invite Dialog closed. result:[Nothing]",
                  Toast.LENGTH_SHORT).show();
              Log.i(TAG, "Invite Dialog closed. result:[Nothing]");
            }
            break;
          default:
        }
      }
    };

    mInviteMessageText = (EditText) findViewById(R.id.inviteMessage);
    mInviteDialogButton = (Button) findViewById(R.id.inviteDialogButton);
    mInviteDialogButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if (AsyncErrorDialog.shouldShowErrorDialog(v.getContext())) {
          AsyncErrorDialog dialog = new AsyncErrorDialog(v.getContext());
          dialog.show();
          return;
        }
        // obtains the body of the message you wish to send
        String body = mInviteMessageText.getText().toString();
        // obtains the list of users you wish to scan for friends from
        ArrayList<String> usernames = new ArrayList<String>();
        usernames.add(mUserId1Text.getText().toString());
        usernames.add(mUserId2Text.getText().toString());
        usernames.add(mUserId3Text.getText().toString());
        // creates a new TreeMap for processing the invite information
        TreeMap<String, Object> map = new TreeMap<String, Object>();
        // if the body isn't null lets add it
        if ((body != null) && (body.length() != 0)) {
          map.put("body", body);
        }
        // gets the array of user ids or null if all blank
        String[] userArray = getUserListArray(usernames);
        if ( userArray != null) {
          // puts the user name array into a the map with key "to_user_id"
          // doesn't have to be 3 as it is a scalable amount of information that is given
          // the return from your getUserListArray() simply has to be a String Array
          map.put("to_user_id", userArray);
        }

        if (mInviteDialog == null) {
          mInviteDialog = new InviteDialog(v.getContext());
        }
        // sets the parameters for the new dialog
        mInviteDialog.setParams(map);
        mInviteDialog.setHandler(mHandler);
        mInviteDialog.show();
      }
    });
    mUserId1Text = (EditText) findViewById(R.id.userId1Edit);
    mUserId2Text = (EditText) findViewById(R.id.userId2Edit);
    mUserId3Text = (EditText) findViewById(R.id.userId3Edit);
  }

  @Override
  public void onOpenHelp(View v){
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.invite_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }
  
  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
  }

  static void debug(String msg) {
    Log.d(TAG, msg);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) {
      return;
    }
    setUpBackButton();
  }

  @Override
  protected void sync(boolean fromStart) {
  }
  
  /**
   * Takes our expandable ArrayList, iterates through the values and returns a String array of the non
   * blank values
   * @param userIds - array list of the string entered as usernames
   * @return a string array of the usernames
   */
  private String[] getUserListArray(ArrayList<String> usernames) {
    List<String> stringList = new ArrayList<String>();
    // iterate through the array and create a new list with values that are not null or blank
    for(String string : usernames) {
      if(string != null && string.trim().length() > 0) {
         stringList.add(string);
      }
   }
    // if the new list is null, we return it as an array
    if (stringList.size() > 0){
      return stringList.toArray(new String[stringList.size()]);
    }
    // we return null otherwise
    return null;
  }
}
