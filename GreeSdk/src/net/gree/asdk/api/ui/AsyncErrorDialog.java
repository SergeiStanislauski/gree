/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.api.ui;

import net.gree.asdk.core.GConnectivityManager;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.RR;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * This class is Async Error Dialog class.
 * For good practice, before performing any action that requires network, you should call this class.
 * It will generate a toast popup if no network connection is available.
 * <pre>For example :
 * {@code
     if (AsyncErrorDialog.shouldShowErrorDialog(context)) {
       AsyncErrorDialog dialog = new AsyncErrorDialog(context);
       dialog.show();
       return;
     }
 * }
 * </pre>
 */
public class AsyncErrorDialog {
  private static Toast toast;

  /**
   * Default constructor.
   * @param context Activity context of showing error dialog.
   */
  public AsyncErrorDialog(Context context) {
    if (toast == null) {
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      LinearLayout layout = (LinearLayout)inflater.inflate(RR.layout("gree_async_error_dialog"), null);
      toast = new Toast(context);
      toast.setView(layout);
      toast.setDuration(Toast.LENGTH_SHORT);
      toast.setGravity(Gravity.CENTER, 0, 0);
    }
  }

  /**
   * Shows an Async Error Dialog.
   * This dialog is a toast message.
   */
  public void show() {
    View v = toast.getView();
    if ((v == null) || (!v.isShown())) {
      toast.show();
    }
  }

  /**
   * Checks state of network connectivity.
   * If the network connectivity doesn't exist, should show an Async Error Dialog.
   * 
   * @param context Activity context of showing error dialog.
   * @return if it is true, should show Async Error Dialog.
   */
  public static boolean shouldShowErrorDialog(Context context) {
    return !(Injector.getInstance(GConnectivityManager.class).checkConnectivity());
  }
}
