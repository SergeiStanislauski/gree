/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.incentive;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HeaderIterator;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.incentive.IncentiveController;
import net.gree.asdk.api.incentive.callback.IncentivePostListener;
import net.gree.platformsample.BaseActivity;
import net.gree.platformsample.HelpActivity;
import net.gree.platformsample.R;

public class IncentivePostActivity extends BaseActivity {
  private static final String TAG = IncentivePostActivity.class.getSimpleName();

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.incentive_post);
  }


  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) {
      return;
    }
    setUpBackButton();
    setUpAutoLoadMore();
    final Button postBotton = (Button) findViewById(R.id.post);
    EditText payload = (EditText) findViewById(R.id.payload);
    // payload.setText("{\"one\":1,\"two\":\"to\"}");

    final EditText targetIds = (EditText) findViewById(R.id.targetIds);
    //targetIds.setText(GreePlatform.getLocalUserId());
    postBotton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        Log.v(TAG, "postBotton is clicked");
        IncentivePostListener callBack = new IncentivePostListener() {

          @Override
          public void onSuccess(String response) {
            postBotton.setEnabled(true);
            Log.e(TAG, response);
            Toast.makeText(IncentivePostActivity.this, "Post Success", Toast.LENGTH_SHORT).show();
            Toast.makeText(IncentivePostActivity.this, response, Toast.LENGTH_LONG).show();
          }

          @Override
          public void onFailure(int responseCode, HeaderIterator headers, String response) {
            postBotton.setEnabled(true);
            StringBuilder sb = new StringBuilder();
            sb.append("onFailure: ");
            sb.append(" responseCode:" + responseCode);
            sb.append(" response:" + response);
            Log.e(TAG, sb.toString());
            Toast.makeText(IncentivePostActivity.this, sb.toString(), Toast.LENGTH_LONG).show();
          }

        };
        RadioGroup payloadType = (RadioGroup) findViewById(R.id.payloadType);
        int id = payloadType.getCheckedRadioButtonId();

        int vpayloadType = 1;
        if (id == R.id.radio0) {
          vpayloadType = 1;
        } else if (id == R.id.radio1) {
          vpayloadType = 2;
        }

        String vtargetIds = targetIds.getText().toString();
        if (TextUtils.isEmpty(vtargetIds)) {
          Log.e(TAG, "target id is invalid");
          return;
        }
        List<String> userArray = new ArrayList<String>();
        String[] users = vtargetIds.split(",");
        if (users != null && users.length > 0) {
          for (String user : users) {
            userArray.add(user);
          }
        } else {
          Log.e(TAG, "users id is empty");
          return;
        }

        EditText payload = (EditText) findViewById(R.id.payload);
        String userText = payload.getText().toString();
        String key = "message";
        // parse the String to json
        String json = pasreTextToJson(key, userText, vpayloadType);
        IncentiveController.post(userArray, vpayloadType, json, callBack);
        postBotton.setEnabled(false);
        
        // hide the keyboard
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((null == 
        		getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), 
        				InputMethodManager.HIDE_NOT_ALWAYS);
      }
    });
  }
  
  // Takes the user input string and parses it to a JSON readable object
  private String pasreTextToJson(String key, String userText, int vpayloadType) {
		// NOTE: if user wants to create more values in the payload array, s/he can follow the following format:
		// "{\"one\":1,\"two\":\"to\"}"	
		JSONObject json = new JSONObject();
		  try {
		    json.put(key, userText);
		  } catch (JSONException e) {
		    e.printStackTrace();
		  }
		return json.toString();
  }

  @Override
  protected void sync(boolean fromStart) {}

  @Override
  public void onOpenHelp(View v) {
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.incentive_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }

}
