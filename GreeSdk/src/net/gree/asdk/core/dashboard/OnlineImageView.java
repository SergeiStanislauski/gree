/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.RR;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

/**
 * This class is used to display an image downloaded from GREE.
 */
public class OnlineImageView extends LinearLayout {
  private static final String LOG_TAG = "OnlineImageView";

  private static final float MAX_SCALE = 5.0f;

  private String mPreUrl = null;

  private ImageView mImageView = null;
  private ProgressBar mLoadingIndicator = null;

  private ImageView mErrorLoadingImageView = null;

  private Bitmap mBitmap = null;

  private float mInitialScale = 1.0f;
  private float mInitialX = 0.0f;
  private float mInitialY = 0.0f;

  private float mCurrentScale = 1.0f;
  private float mCurrentX = 0.0f;
  private float mCurrentY = 0.0f;

  private boolean mIsFitLeft = false;
  private boolean mIsFitRight = false;
  private boolean mIsOutOfViewHeight = false;

  /**
   * Create the OnlineImageView.
   * @param context android.context.Context
   */
  public OnlineImageView(Context context) {
    super(context);
    initialize();
  }

  /**
   * Create the OnlineImageView.
   * @param context android.context.Context
   * @param attrs android.util.AttributeSet
   */
  public OnlineImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize();
  }

  /**
   * Get the current scale of image.
   * @return scale.
   */
  public float getCurrentScale() {
    return mCurrentScale;
  }

  /**
   * Load the url.
   * @param url image url.
   */
  public void load(String url) {
    if (url == null) {
      return;
    }

    if (mPreUrl == null || !mPreUrl.equals(url) || mBitmap == null) {
      new ImageDownloadTask().execute(url);
    } else {
      resetTransform();
    }

    mPreUrl = url;
  }

  /**
   * Save the image to SD card.
   */
  public void save() {
    new ImageSaveTask().execute(mBitmap);
  }

  /**
   * Clear the image.
   */
  public void clearImage() {
    mImageView.setImageBitmap(null);
    if (mBitmap != null) {
      mBitmap.recycle();
      mBitmap = null;
    }
  }

  /**
   * Reset the image transforms.
   */
  public void resetTransform() {
    if (mBitmap == null) {
      return;
    }

    mCurrentScale = 1.0f;
    mCurrentX = 0.0f;
    mCurrentY = 0.0f;

    float pageWidth = getWidth();
    float pageHeight = getHeight();

    int imageWidth = mBitmap.getWidth();
    int imageHeight = mBitmap.getHeight();

    float widthRatio = pageWidth / imageWidth;
    float heightRatio = pageHeight / imageHeight;

    if (widthRatio < heightRatio) {
      mInitialScale = widthRatio;
    } else {
      mInitialScale = heightRatio;
    }

    mInitialX = (pageWidth - imageWidth) * 0.5f;
    mInitialY = (pageHeight - imageHeight) * 0.5f;

    mIsFitLeft = true;
    mIsFitRight = true;

    applyCurrentTransform();
  }

  /**
   * Translate the image.
   * @param x x-axis.
   * @param y y-axis.
   */
  public void translate(float x, float y) {
    if (mBitmap == null) {
      return;
    }

    float widthDeltaHalf = (mBitmap.getWidth() * mInitialScale * mCurrentScale - getWidth()) * 0.5f;

    if (widthDeltaHalf > 0.0f) {
      mCurrentX += x;

      if (mCurrentX > widthDeltaHalf) {
        mCurrentX = widthDeltaHalf;
        mIsFitLeft = true;
      } else if (mCurrentX < -widthDeltaHalf) {
        mCurrentX = -widthDeltaHalf;
        mIsFitRight = true;
      }
    } else {
      mCurrentX = 0.0f;
    }

    float heightDeltaHalf = (mBitmap.getHeight() * mInitialScale * mCurrentScale - getHeight()) * 0.5f;

    if (heightDeltaHalf > 0.0f) {
      mCurrentY += y;
      if (mCurrentY > heightDeltaHalf) {
        mCurrentY = heightDeltaHalf;
      } else if (mCurrentY < -heightDeltaHalf) {
        mCurrentY = -heightDeltaHalf;
      } else {
        mIsOutOfViewHeight = true;
      }
    } else {
      mCurrentY = 0.0f;
      mIsOutOfViewHeight = false;
    }

    applyCurrentTransform();
  }

  /**
   * Scale the image.
   * @param scale scale size.
   */
  public void scale(float scale) {
    if (mBitmap == null) {
      return;
    }

    mCurrentScale *= scale;
    if (mCurrentScale > 1.0f) {
      mIsFitLeft = false;
      mIsFitRight = false;

      if (mCurrentScale > MAX_SCALE) {
        mCurrentScale = MAX_SCALE;
      }
    } else {
      mCurrentScale = 1.0f;
      mIsFitLeft = true;
      mIsFitRight = true;
      mIsOutOfViewHeight = false;
    }

    applyCurrentTransform();
  }

  /**
   * Return true if the image height is out of display.
   * @return true if the image height is out of display, else false.
   */
  public boolean isOutOfDisplayHeight() {
    return mIsOutOfViewHeight;
  }

  /**
   * Return true if the image is fit to left.
   * @return true if the image is fit to left, else false.
   */
  public boolean isFitLeft() {
    return mBitmap == null || mIsFitLeft;
  }

  /**
   * Return true if the image is fit to right.
   * @return true if the image is fit to right, else false.
   */
  public boolean isFitRight() {
    return mBitmap == null || mIsFitRight;
  }

  /**
   * Return true if the error is shown.
   * @return true if the error is shown, else false.
   */
  public boolean isShownError() {
    return mErrorLoadingImageView.getVisibility() == View.VISIBLE;
  }

  /**
   * Show the loading indicator.
   */
  public void showLoadingIndicator() {
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR_MR1) {
      Animation rotation = AnimationUtils.loadAnimation(getContext(), RR.anim("gree_rotate"));
      rotation.setRepeatCount(Animation.INFINITE);
      mLoadingIndicator.startAnimation(rotation);
    }
    mLoadingIndicator.setVisibility(View.VISIBLE);

    mImageView.setVisibility(View.GONE);
    mErrorLoadingImageView.setVisibility(View.GONE);
  }

  /**
   * Hide the loading indicator.
   */
  public void hideLoadingIndicator() {
    mLoadingIndicator.setVisibility(View.GONE);
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR_MR1) {
      mLoadingIndicator.clearAnimation();
    }
  }

  /**
   * Hide the loading indicator, the imave view, the error loading image view.
   */
  public void hideViews() {
    hideLoadingIndicator();

    mImageView.setVisibility(View.GONE);
    mErrorLoadingImageView.setVisibility(View.GONE);
  }

  /**
   * Show the image view.
   */
  public void showImageView() {
    hideLoadingIndicator();

    mImageView.setVisibility(View.VISIBLE);
    mErrorLoadingImageView.setVisibility(View.GONE);
  }

  /**
   * Show the error message.
   * @param messageResourceId resource id of error message.
   */
  public void showErrorMessage(int messageResourceId) {
    hideLoadingIndicator();

    mImageView.setVisibility(View.GONE);
    mErrorLoadingImageView.setVisibility(View.VISIBLE);
  }

  private HttpResponse requestImageData(URI uri) throws IOException {
    HttpClient client = new DefaultHttpClient();
    HttpGet get = new HttpGet();
    get.setURI(uri);
    return client.execute(get);
  }

  private void initialize() {
    Context context = getContext();
    setOrientation(LinearLayout.VERTICAL);
    setGravity(Gravity.CENTER);

    mErrorLoadingImageView = new ImageView(context);
    mErrorLoadingImageView.setImageResource(RR.drawable("gree_spinner"));
    mErrorLoadingImageView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    mErrorLoadingImageView.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        mImageView.setVisibility(View.VISIBLE);
        mErrorLoadingImageView.setVisibility(View.GONE);

        new ImageDownloadTask().execute(mPreUrl);
      }
    });

    BitmapFactory.Options opts = new BitmapFactory.Options();
    opts.inJustDecodeBounds = true;
    BitmapFactory.decodeResource(getResources(), RR.drawable("gree_spinner"), opts);

    mErrorLoadingImageView.setScaleType(ImageView.ScaleType.MATRIX);
    Matrix matrix = new Matrix();
    matrix.postScale(0.7f, 0.7f, opts.outWidth, opts.outHeight);
    mErrorLoadingImageView.setImageMatrix(matrix);
    addView(mErrorLoadingImageView);
    mErrorLoadingImageView.setVisibility(View.GONE);

    mLoadingIndicator = new ProgressBar(context);
    Resources res = this.getContext().getResources();
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR_MR1) {
      Drawable drawable = res.getDrawable(RR.drawable("gree_spinner"));
      mLoadingIndicator.setIndeterminateDrawable(drawable);
    } else {
      Drawable drawable = res.getDrawable(RR.drawable("gree_loader_progress"));
      mLoadingIndicator.setIndeterminateDrawable(drawable);
      mLoadingIndicator.setIndeterminate(true);
    }

    mLoadingIndicator.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    addView(mLoadingIndicator);
    mLoadingIndicator.setVisibility(View.GONE);

    mImageView = new ImageView(context);
    mImageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    mImageView.setScaleType(ImageView.ScaleType.MATRIX);
    addView(mImageView);
    mImageView.setVisibility(View.GONE);

    showLoadingIndicator();

    mImageView.setImageMatrix(new Matrix());
  }

  private void applyCurrentTransform() {
    if (mBitmap == null) {
      return;
    }

    Matrix matrix = mImageView.getImageMatrix();
    if (matrix == null) {
      matrix = new Matrix();
    }

    float x = mInitialX + mCurrentX;
    float y = mInitialY + mCurrentY;
    matrix.setTranslate(x, y);

    float scale = mInitialScale * mCurrentScale;
    matrix.postScale(scale, scale, x + mBitmap.getWidth() * 0.5f, y + mBitmap.getHeight() * 0.5f);

    mImageView.setImageMatrix(matrix);
    mImageView.invalidate();
  }

  /**
   * AsyncTask of downloading the image. 
   */
  private class ImageDownloadTask extends AsyncTask<String, Void, Bitmap> {
    @Override
    protected void onPreExecute() {
      showLoadingIndicator();
      clearImage();
    }

    @Override
    protected Bitmap doInBackground(String... params) {
      try {
        HttpResponse response = requestImageData(new URI(params[0]));
        if (response.getStatusLine().getStatusCode() < HttpStatus.SC_NOT_FOUND) {  
          InputStream is = response.getEntity().getContent();
          Bitmap bitmap = BitmapFactory.decodeStream(is);
          is.close();
          return bitmap;
        }
      } catch (ClientProtocolException e) {  
        GLog.printStackTrace(LOG_TAG, e);
      } catch (IOException e) {
        GLog.printStackTrace(LOG_TAG, e);
      } catch (URISyntaxException e) {
        GLog.printStackTrace(LOG_TAG, e);
      }

      return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
      if (result == null) {
        showErrorMessage(RR.string("gree_image_view_failed_to_download_the_photo"));
        return;
      }

      mBitmap = result;
      mImageView.setImageBitmap(mBitmap);
      showImageView();

      resetTransform();
    }
  }

  /**
   * AsyncTask of saving the image which is selected by user.
   */
  private class ImageSaveTask extends AsyncTask<Bitmap, Void, Boolean> {
    private static final int COMPRESS_QUOLITY = 100;
    private static final int CONTENT_VALUES_SIZE = 5;
    @Override
    protected void onPreExecute() {
      Context context = getContext();
      NotificationManager notificationManager = (NotificationManager) (context.getSystemService(Context.NOTIFICATION_SERVICE));
      notificationManager.cancel(RR.integer("gree_notification_id_downloaded"));
    }

    @Override
    protected Boolean doInBackground(Bitmap... params) {
      Bitmap bitmap = params[0];
      if (bitmap == null) {
        return false;
      }

      File dir = new File(
          (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ? Environment.getExternalStorageDirectory() : Environment.getDataDirectory()).getAbsolutePath() + "/download/"
      );
      dir.mkdirs();

      long curTime = System.currentTimeMillis();
      String fileName = DateFormat.format("yyyy-MM-dd_kk.mm.ss", curTime).toString() + ".jpg";
      String filePath = dir.getAbsolutePath() + "/" + fileName;
      FileOutputStream fout;
      try {
        fout = new FileOutputStream(filePath);
        bitmap.compress(CompressFormat.JPEG, COMPRESS_QUOLITY, fout);
        fout.flush();
        fout.close();

        // Insert image information to ContentResolver.
        // This let Gallery app display the image.
        ContentValues values = new ContentValues(CONTENT_VALUES_SIZE);
        values.put(Media.DISPLAY_NAME, fileName);
        values.put(Media.TITLE, fileName);
        values.put(Media.DATE_ADDED, curTime);
        values.put(Media.MIME_TYPE, "image/jpeg");
        values.put(Media.DATA, filePath);
        getContext().getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);
      } catch (FileNotFoundException e) {
        GLog.printStackTrace(LOG_TAG, e);
        return false;
      } catch (IOException e) {
        GLog.printStackTrace(LOG_TAG, e);
        return false;
      }

      return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
      Context context = getContext();
      Resources resources = context.getResources();

      String message = resources.getString(result ? RR.string("gree_image_view_download_complete_message") : RR.string("gree_image_view_download_failure_message"));

      NotificationManager notificationManager = (NotificationManager) (context.getSystemService(Context.NOTIFICATION_SERVICE));
      Notification notification = new Notification(RR.drawable("gree_stat_sys_download_anim0"), message, System.currentTimeMillis());
      notification.setLatestEventInfo(context, "GREE", message, PendingIntent.getActivity(context, 0, new Intent() , 0));
      notification.flags = Notification.FLAG_AUTO_CANCEL;
      notificationManager.notify(RR.integer("gree_notification_id_downloaded"), notification);
    }
  }
}
