/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import org.apache.http.HeaderIterator;
import org.apache.http.HttpResponse;

/**
 * ResponseHolder is a simple POJO class that hold converted result and other useful information.
 * This class is mainly for isolation of processing and callback;
 * 
 * @param <T>
 */
public class ResponseHolder<T> {
  protected T mResult;
  protected int mStatusCode;
  protected String mReasonPhrase;
  private HeaderIterator mHeaderItor;
  private HttpResponse mResponse;

  public ResponseHolder(T result, int statusCode, HeaderIterator headerItor, String reasonPhrase) {
    mResult = result;
    mStatusCode = statusCode;
    mHeaderItor = headerItor;
    mReasonPhrase = reasonPhrase;
  }

  protected ResponseHolder(ResponseHolder<T> holder) {
    mResult = holder.mResult;
    mStatusCode = holder.mStatusCode;
    mHeaderItor = holder.mHeaderItor;
    mReasonPhrase = holder.mReasonPhrase;
    mResponse = holder.mResponse;
  }

  protected ResponseHolder(IConverter<T> converter, HttpResponse response) {
    if (converter != null) {
      mResult = converter.convert(response);
    }
    if (response != null) {
      mStatusCode = response.getStatusLine().getStatusCode();
      mReasonPhrase = response.getStatusLine().getReasonPhrase();
    }
    mResponse = response;
  }

  public HeaderIterator getHeaderIterator() {
    if (mResponse != null) {
      mHeaderItor = mResponse.headerIterator();
    }
    return mHeaderItor;
  }
}
