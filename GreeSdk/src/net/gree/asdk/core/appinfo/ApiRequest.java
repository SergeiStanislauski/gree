/*
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.appinfo;

import java.security.InvalidParameterException;
import java.util.Map;

import org.apache.http.HeaderIterator;

import net.gree.asdk.core.*;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.JsonClient;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.util.CoreData;
import net.gree.asdk.core.util.Url;

/**
 * Request class to get application list given a user
 */
public final class ApiRequest {
  private static final String TAG = "ApiRequest";
  Map<String, Object> oparams = null;
  boolean isDebug;

  public ApiRequest() {
    this.oparams = CoreData.getParams();
    this.isDebug = Core.getInstance().debugging();
  }

  public ApiRequest(Map<String, Object> params) {
    this.oparams = params;
    this.isDebug = Core.getInstance().debugging();
  }

  public void oauth(String action, String method, Map<String, Object> queryParameters,
      Map<String, String> headers, boolean sync, final OnResponseCallback<String> listener) {
    int imethod;
    try {
      imethod = BaseClient.cmd.get(method);
    } catch (Exception e) {
      throw new IllegalArgumentException();
    }
    String url = Url.getGreeApiEndpointWithAction(action);
    switch (imethod) {
      case BaseClient.METHOD_GET:
      case BaseClient.METHOD_DELETE:
        if ((queryParameters != null) && (!queryParameters.isEmpty())) {
          url += "?" + BaseClient.toQueryString(queryParameters);
        }
        if (isDebug) {
          GLog.v(TAG, "Query string:" + url);
        }
        new JsonClient().oauth(url, method, headers, sync,
          new OnResponseCallback<String>() {

            @Override
            public void onSuccess(int responseCode, HeaderIterator headers, String response) {
              if (isDebug) {
                GLog.v(TAG, "Response:" + response);
              }
              listener.onSuccess(responseCode, headers, response);
            }
            @Override
            public void onFailure(int responseCode, HeaderIterator headers, String response) {
              if (isDebug) {
                GLog.v(TAG, "ResponseCode:" + response + " Response:" + response);
              }
              listener.onFailure(responseCode, headers, response);
            }
        });
        break;
      case BaseClient.METHOD_POST:
      case BaseClient.METHOD_PUT:
        String json = BaseClient.toEntityJson(queryParameters);
        if (isDebug) {
          GLog.d(TAG, "Json body:" + json);
        }
        new JsonClient().oauth(url, method, headers, json, sync, listener);
        break;
      default:
        throw new InvalidParameterException("Invalid method " + imethod);
    }
  }

  public void oauth(String action, String method, Map<String, Object> queryParameters,
      Map<String, String> headers, boolean sync, boolean isSecure, final OnResponseCallback<String> listener) {
    int imethod;
    try {
      imethod = BaseClient.cmd.get(method);
    } catch (Exception e) {
      throw new IllegalArgumentException();
    }
    String url = null;
    if (isSecure) {
      url = Url.getGreeSecureApiEndpointWithAction(action);
    } else {
      url = Url.getGreeApiEndpointWithAction(action);
    }
    switch (imethod) {
      case BaseClient.METHOD_GET:
      case BaseClient.METHOD_DELETE:
        if ((queryParameters != null) && (!queryParameters.isEmpty())) {
          url += "?" + BaseClient.toQueryString(queryParameters);
        }
        if (isDebug) {
          GLog.d(TAG, "Query string:" + url);
        }
        new JsonClient().oauth(url, method, headers, sync,
          new OnResponseCallback<String>() {

            @Override
            public void onSuccess(int responseCode, HeaderIterator headers, String response) {
              if (isDebug) {
                GLog.v(TAG, "Response:" + response);
              }
              listener.onSuccess(responseCode, headers, response);
            }
            @Override
            public void onFailure(int responseCode, HeaderIterator headers, String response) {
              if (isDebug) {
                GLog.v(TAG, "ResponseCode:" + response + " Response:" + response);
              }
              listener.onFailure(responseCode, headers, response);
            }
        });
        break;
      case BaseClient.METHOD_POST:
      case BaseClient.METHOD_PUT:
        String json = BaseClient.toEntityJson(queryParameters);
        if (isDebug) {
          GLog.d(TAG, "Json body:" + json);
        }
        new JsonClient().oauth(url, method, headers, json, sync, listener);
        break;
      default:
        throw new InvalidParameterException("Invalid method " + imethod);
    }
  }
}
