/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.dashboard;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import net.gree.asdk.core.storage.ListStorage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;

public class EmojiPaletteAdapter extends BaseAdapter {
  private Context context_;
  private int tabId_ = 0;
  private int pageId_ = 0;
  private final String EMOJI_DIR;
  private ArrayList<String> mEmojiRecents;
  private ListStorage mStorage;
  private static HashMap<String, WeakReference<Bitmap>> cache = new HashMap<String, WeakReference<Bitmap>>();

  private static final int EMOJI_PAGE_EMOJI_COUNT = 21;
  private static final int EMOJI_TAB_EMOJI_COUNT = EMOJI_PAGE_EMOJI_COUNT * 4;
  private static final int EMOJI_BASELAYOUT_WIDTH = 40;
  private static final int EMOJI_BASELAYOUT_HEIGHT = 36;
  private static final int EMOJI_BASELAYOUT_PADDING_LEFT = 6;
  private static final int EMOJI_BASELAYOUT_PADDING_TOP = 3;
  private static final int EMOJI_BASELAYOUT_PADDING_RIGHT = 3;
  private static final int EMOJI_BASELAYOUT_PADDING_BOTTOM = 0;
  private static final int EMOJI_LENGTH = 26;
  private static final int EMOJI_PADDING = 3;
  private static final int EMOJI_RECENT_TAB_ID = 10;

  public EmojiPaletteAdapter(Context context) {
    super();
    EMOJI_DIR = Environment.getExternalStorageDirectory().toString() + "/Android/data/"+context.getPackageName().toString()+"/files/gree/pictogram";
    context_ = context;
    mEmojiRecents = new ArrayList<String>();
    mStorage = new ListStorage(context);
    mStorage.getListValue(mEmojiRecents);
  }

  /**
   * set Recent Emoji entry
   * @param position Emoji item position of tab.
   */
  public void setRecentIconItem(int position) {
    if (tabId_ == EMOJI_RECENT_TAB_ID) {
      return;
    }
    String filename = String.format(Locale.US, "ic_emoji_%1$03d.png", getItemId(position));
    
    if (mEmojiRecents.contains(filename)) {
      mEmojiRecents.remove(filename);
    }
    mEmojiRecents.add(0, filename);

    if (mEmojiRecents.size() > EMOJI_PAGE_EMOJI_COUNT) {
      filename = mEmojiRecents.get(EMOJI_PAGE_EMOJI_COUNT);
      mEmojiRecents.remove(filename);
    }
  }

  /**
   * save Recent Emojis for Storage.
   */
  public void saveRecentIconItems() {
    (new Thread(new Runnable() {
      @Override
      public void run() {
        mStorage.putListValue(mEmojiRecents);
      }
    })).start();
  }

  public int getMaxTab() {
    int emojiCount = EmojiController.getEmojiCount(context_);
    return emojiCount / EMOJI_TAB_EMOJI_COUNT + 1;
  }

  public int getMaxPage() {
    int emojiCount = EmojiController.getEmojiCount(context_);
    return ((emojiCount - tabId_ * EMOJI_TAB_EMOJI_COUNT) > EMOJI_TAB_EMOJI_COUNT) ? 4 : ((emojiCount - tabId_ * EMOJI_TAB_EMOJI_COUNT) / EMOJI_PAGE_EMOJI_COUNT + 1);
  }

  public int getCurrentPage() {
    return pageId_;
  }

  public void changePrevPage() {
    if (pageId_ < 1) {
      pageId_ = getMaxPage() - 1;
    } else {
      pageId_--;
    }
  }

  public void changeNextPage() {
    if (pageId_ >= getMaxPage() - 1) {
      pageId_ = 0;
    } else {
      pageId_++;
    }
  }

  public void changeTab(int tabId) {
    tabId_ = tabId;
    pageId_ = 0;
  }

  /**
   * change Emoji palette tab for recent emoji tab.
   */
  public void changeRecentTab() {
    tabId_ = EMOJI_RECENT_TAB_ID;
    pageId_ = 0;
  }

  @Override
  public int getCount() {
    if (tabId_ == EMOJI_RECENT_TAB_ID)
      return mEmojiRecents.size();
    int emojiCount = EmojiController.getEmojiCount(context_);
    return (emojiCount - (tabId_ * EMOJI_TAB_EMOJI_COUNT + pageId_ * EMOJI_PAGE_EMOJI_COUNT)) > EMOJI_PAGE_EMOJI_COUNT
        ? EMOJI_PAGE_EMOJI_COUNT
        : (emojiCount - (tabId_ * EMOJI_TAB_EMOJI_COUNT + pageId_ * EMOJI_PAGE_EMOJI_COUNT));
  }

  @Override
  public Object getItem(int position) {
    String filename;
    if (tabId_ == EMOJI_RECENT_TAB_ID) {
      filename = mEmojiRecents.get(position);
    } else {
      filename = String.format(Locale.US, "ic_emoji_%1$03d.png", getItemId(position));
    }
    
    WeakReference<Bitmap> emojiBitmap = cache.get(filename);
    if (emojiBitmap != null) {
      Bitmap bitmap = emojiBitmap.get();
      if (bitmap != null) {
        return bitmap;
      }
    }
    File file = new File(EMOJI_DIR, filename);
    return BitmapFactory.decodeFile(file.getPath());
  }

  @Override
  public long getItemId(int position) {
    return tabId_ * EMOJI_TAB_EMOJI_COUNT + pageId_ * EMOJI_PAGE_EMOJI_COUNT + position + 1;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    FrameLayout layout = null;
    ImageView imageView = null;
    if (convertView == null) {
      final float scale = context_.getResources().getDisplayMetrics().density;
      layout = new FrameLayout(parent.getContext());
      layout.setLayoutParams(new GridView.LayoutParams((int)(EMOJI_BASELAYOUT_WIDTH * scale), (int)(EMOJI_BASELAYOUT_HEIGHT * scale)));
      layout.setPadding((int)(EMOJI_BASELAYOUT_PADDING_LEFT * scale),
          (int)(EMOJI_BASELAYOUT_PADDING_TOP * scale),
          (int)(EMOJI_BASELAYOUT_PADDING_RIGHT * scale),
          (int)(EMOJI_BASELAYOUT_PADDING_BOTTOM * scale));
      imageView = new ImageView(parent.getContext());
      final int emoji_length_scale = (int)(EMOJI_LENGTH * scale);
      imageView.setLayoutParams(new FrameLayout.LayoutParams(emoji_length_scale, emoji_length_scale));
      final int emoji_padding_scale = (int)(EMOJI_PADDING * scale);
      imageView.setPadding(emoji_padding_scale, emoji_padding_scale, emoji_padding_scale, emoji_padding_scale);
      layout.addView(imageView);
    } else {
      layout = (FrameLayout) convertView;
      imageView = (ImageView) layout.getChildAt(0);
    }
    imageView.setImageBitmap((Bitmap) getItem(position));
    return layout;
  }

}
