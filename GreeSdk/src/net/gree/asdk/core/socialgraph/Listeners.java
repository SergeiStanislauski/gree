package net.gree.asdk.core.socialgraph;

import org.apache.http.HeaderIterator;

/**
 * A group of listeners, that should eventually be moved to api side 
 */
public abstract class Listeners {

  /**
   * The listener for getting friends count.
   * @see InGameUsersRequest.getFriendCount
   */
  public interface CountListener {
    /**
     * On success this will return the number of friends for the requested game
     * @param count
     */
    void onSuccess(int count);
    /**
     * Called when an error occurred in trying to get the count of in game friends.
     * @param responseCode Integer representing the http response code.
     * @param headers Object representing the headers of the http response.
     * @param response String representing the body of the http response.
     */
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * The listener for getting the list of friends by category and their relationship
   */
  public interface InGameFriendsListener {
    /**
     * Received a list of friends for the given application ID
     * @param startIndex the index (from 1)
     * @param friends a list of InGameFriend
     */
    void onSuccess(int startIndex, InGameFriend[] friends);
    /**
     * Called when an error occurred in trying to get the list of in game friends.
     * @param responseCode Integer representing the http response code.
     * @param headers Object representing the headers of the http response.
     * @param response String representing the body of the http response.
     */
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * This is used to get result from the getRelationship calls
   * @see InGameSocialGraphActivity.getRelationship
   */
  public interface RelationshipListener {
    /** Users are unrelated */
    public final static int NONE = 0;
    /** You have sent a friend request */
    public final static int REQUESTING = 1;
    /** You have received a friend request and you dismissed it */
    public final static int RESERVING = 2;
    /** You have received a friend request */
    public final static int REQUESTED = 3;
    /** You are friends */
    public final static int FRIEND = 4;

    /**
     * On success, an int value describing the relation between 2 users
     * @param status one of NONE, REQUESTING, RESERVING, REQUESTED, FRIENDS
     */
    void onSuccess(int status);

    /**
     * Called when an error occurred in trying to get the relationship between 2 users
     * @param responseCode Integer representing the http response code.
     * @param headers Object representing the headers of the http response.
     * @param response String representing the body of the http response.
     */
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }
}
