/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.notifications;

import net.gree.asdk.core.Core;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.InternalSettings;
import net.gree.asdk.core.inject.Inject;
import net.gree.asdk.core.notifications.ui.ClickableNotification;
import net.gree.asdk.core.notifications.ui.StatusNotification;
import net.gree.asdk.core.notifications.ui.ToastNotification;
import net.gree.asdk.core.util.CoreData;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import net.gree.asdk.core.util.Preconditions;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Queue and dispatch notifications, either as in game notifications or status bar notifications
 * This is used for received push notifications.
 */
public class MessageDispatcher {

  private static final String TAG = "Notification";
  private static boolean debug = false;
  private Activity mActivity;

  // What notifications can be displayed in the status bar
  private static final int DEFAULT_FILTER = MessageDescription.FLAG_NOTIFICATION_CUSTOM_MESSAGE
      | MessageDescription.FLAG_NOTIFICATION_SERVICE_MESSAGE
      | MessageDescription.FLAG_NOTIFICATION_SERVICE_REQUEST
      | MessageDescription.FLAG_NOTIFICATION_REQUEST_FROM_API_B2C
      | MessageDescription.FLAG_NOTIFICATION_REQUEST_FROM_API_C2C;

  // for toast
  private static final int DISPLAY_TOAST = 0;
  private static final int DISPLAY_NEXT = 1;
  private static final int DISPLAY_CLICKABLE = 2;
  private static final int HIDE_CLICKABLE = 3;
  private Handler handler = new Handler(Looper.getMainLooper()) {
    @Override
    public void handleMessage(Message msg) {
      Context context = (Context) msg.obj;
      if (context != null) {
        switch (msg.what) {
          case DISPLAY_NEXT:
            displayNext(context);
            break;
          case DISPLAY_TOAST:
            mToastNotification.notify(context, sCurrentNotification);
            if (sCurrentNotification != null) {
              handler.sendMessageDelayed(handler.obtainMessage(DISPLAY_NEXT, context), sCurrentNotification.getDuration());
            }
            break;
          case DISPLAY_CLICKABLE:
            mClickableNotification.notify(context, sCurrentNotification, mActivity);
            if (sCurrentNotification != null) {
              handler.sendMessageDelayed(handler.obtainMessage(DISPLAY_NEXT, context), sCurrentNotification.getDuration());
              handler.sendMessageDelayed(handler.obtainMessage(HIDE_CLICKABLE, context), sCurrentNotification.getDuration());
            }
            break;
          case HIDE_CLICKABLE:
            mClickableNotification.dismiss();
            break;
          default:
            break;
        }
      }
    }
  };

  // Queue
  private Byte sLock = '0';
  private Queue<MessageDescription> sQueue = new LinkedList<MessageDescription>();
  private MessageDescription sCurrentNotification;

  private StatusNotification mStatusNotification;
  private ToastNotification mToastNotification;
  private ClickableNotification mClickableNotification;
  private NotificationCounts mNotificationCounts;

  private Core mCore;

  /**
   * An interface to follow the status of a notification
   */
  public interface NotificationUpdateListener {
    /**
     * Called when the notification is displayed
     * 
     * @param notification the notification to be displayed
     */
    void onDisplay(MessageDispatcher notification);

    /**
     * Called when the notification is cancelled
     * 
     * @param notification the notification to be cancelled
     */
    void onClose(MessageDispatcher notification);
  }

  @Inject
  public MessageDispatcher(NotificationCounts mNotificationCounts,
      StatusNotification mStatusNotification, ToastNotification mToastNotification, ClickableNotification mClickableNotification) {

    Preconditions.checkNotNull(mNotificationCounts, "mNotificationCounts is requried");
    Preconditions.checkNotNull(mStatusNotification, "mStatusNotification is required");
    Preconditions.checkNotNull(mToastNotification, "mToastNotification is required");
    Preconditions.checkNotNull(mClickableNotification, "mClickableNotification is required");

    this.mNotificationCounts = mNotificationCounts;
    this.mStatusNotification = mStatusNotification;
    this.mToastNotification = mToastNotification;
    this.mClickableNotification = mClickableNotification;
  }

  /**
   * Enqueue this notification to be displayed as soon as possible, Once this method has been called
   * the current object can be modified for re-use or disposed.
   * 
   * @param context a valid context
   * @param message the message to be added to the queue
   */
  public void enqueue(Context context, MessageDescription message) {
    debug("enqueue Message " + message);
    synchronized (sLock) {
      if (sCurrentNotification == null) {
        sCurrentNotification = message;
        display(context);
      } else {
        sQueue.add(message);
      }
    }
    mNotificationCounts.updateCounts();
  }

  /**
   * Trigger the sCurrentNotification to be displayed.
   */
  private void display(final Context context) {
    debug("Display");
    if (getCore().isInBackground()) {
      if (allowNotification(sCurrentNotification.getType())) {
        // Show a status bar notification
        mStatusNotification.notify(context, sCurrentNotification);
        sCurrentNotification = null;
      } else {
        // drop the message
        debug("Notification has been filtered out");
        displayNext(context);
      }
    } else if (mActivity != null && sCurrentNotification.isClickable()) {
      if ("false".equalsIgnoreCase(CoreData.get(InternalSettings.NotificationEnabled))) {
        return;
      }
      handler.sendMessageDelayed(handler.obtainMessage(DISPLAY_CLICKABLE, context),
          sCurrentNotification.getDuration());
    } else {
      // Not allowed to show in game notifications
      if ("false".equalsIgnoreCase(CoreData.get(InternalSettings.NotificationEnabled))) {
        return;
      }
      handler.sendMessageDelayed(handler.obtainMessage(DISPLAY_TOAST, context),
          sCurrentNotification.getDuration());
    }
  }

  /**
   * This is called automatically to display the next notification in the queue
   * 
   * @param context a valid context
   */
  public void displayNext(final Context context) {
    synchronized (sLock) {
      mStatusNotification.dismiss(context, null);
      if (sQueue.peek() != null) {
        debug("" + sQueue.size() + " Notifications in queue");
        sCurrentNotification = sQueue.poll();
        display(context);
      } else {
        sCurrentNotification = null;
        debug("Finished showing all notifications");
      }
    }
  }

  /**
   * Close any existing notification
   * 
   * @param context a valid context
   */
  public void dismiss(Context context) {
    mStatusNotification.dismiss(context, null);
  }

  /**
   * Clear all notifications
   * 
   * @param context a valid context
   */
  public void dismissAll(Context context) {
    sCurrentNotification = null;
    sQueue.clear();
    dismiss(context);
  }

  /**
   * register Activity for clickable Notification
   * 
   * @param activity register activity
   */
  public void registerActivity(Activity activity) {
    mActivity = activity;
  }

  /**
   * get register Activity 
   * @return register Activity
   */
  public Activity getRegisterActivity() {
    return mActivity;
  }

  /**
   * unregister Activity for clickable Notification
   * 
   * @param activity unregister activity
   */
  public void unRegisterActivity(Activity activity) {
    mClickableNotification.dismiss();
    if (mActivity == activity) {
      mActivity = null;
    }
  }

  private static void debug(String text) {
    if (debug) {
      GLog.d(TAG, text);
    }
  }

  /**
   * Decides which notifications are displayed when the application is in background. The
   * notifications will be displayed in the status bar.
   * 
   * @param notificationType the type of notification
   * @return true if this notification can be displayed, false otherwise
   */
  private boolean allowNotification(int notificationType) {
    int filter = DEFAULT_FILTER;
    try {
      String sFilter = CoreData.get(InternalSettings.FilterForStatusBarNotifications);
      if (sFilter != null) {
        filter = Integer.parseInt(sFilter);
      }
    } catch (NumberFormatException ne) {
      GLog.printStackTrace(TAG, ne);
    }
    return (notificationType & filter) == notificationType;
  }

  /**
   * Turn debug on or off for the notification messageDispatcher
   */
  public static void setDebug(boolean debugMode) {
    debug = debugMode;
  }

  private Core getCore() {
    if (null == mCore) {
      mCore = Core.getInstance();
    }
    return mCore;
  }
}
