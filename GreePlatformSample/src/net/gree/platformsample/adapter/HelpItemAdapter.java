/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.platformsample.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.gree.platformsample.R;
import net.gree.platformsample.wrapper.HelpWrapper;

/**
 * Adapter for help list
 * 
 *
 */
public class HelpItemAdapter extends BaseAdapter {
  
  /**
   * View holder
   * 
   *
   */
  private class HelpViewHolder{
    TextView name;
  }
  
  private List<HelpWrapper> data;
  private Activity context;
  private LayoutInflater inflater;
  
  /**
   * The Initializer
   * 
   * @param context
   * @param data
   */
  public HelpItemAdapter(Activity context, List<HelpWrapper> data){
    this.context = context;
    this.data = data;
    this.inflater = LayoutInflater.from(context);
  }
  
  @Override
  public int getCount() {
    if (data == null) {return 0;}
    return data.size();
  }

  @Override
  public Object getItem(int position) {
    return data.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    HelpViewHolder holder;
    if (convertView == null){
      convertView = inflater.inflate(R.layout.help_item_line, null);
      holder = new HelpViewHolder();
      holder.name = (TextView) convertView.findViewById(R.id.help_item_name);
      convertView.setTag(holder);
    } else{
      holder = (HelpViewHolder) convertView.getTag();
    }
    final HelpWrapper item = data.get(position);
    holder.name.setText(item.getName());
    
    return convertView;
  }

}
