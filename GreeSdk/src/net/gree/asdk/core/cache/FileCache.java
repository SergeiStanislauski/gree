/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.cache;

import java.io.File;
import android.content.Context;

/**
 * Simple File cache with take a context and creates a new Cache Directory on the local storage
 *
 */
public class FileCache {
    
    private File cacheDir;
    /**
     * Constructor for the class. Instantiates the Cache.
     * @param context context for the creation of the class. Typically we want to pass in application context
     */
    public FileCache(Context context){
        // Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"LazyList");
        else
            cacheDir=context.getCacheDir();
        if(!cacheDir.exists())
            cacheDir.mkdirs();
    }
    
    /**
     * Returns a file from the Cache if it exists. We vary each member of this cache by a distinct identifying string
     * @param url Typically we use the cache for images, so this is the URL of the image obtained from the web. Default this is the identifying
     * String on the file in the Cache we wish to obtain
     * @return the File matching the URL in the cache
     */
    public File getFile(String url){
        // Identify images by hashcode. Not a perfect solution, good for a small cache
        String filename=String.valueOf(url.hashCode());
        // Another possible solution (thanks to grantland)
        // String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;
        
    }
    
    /**
     * Clears the cache if one exists.
     */
    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }

}
