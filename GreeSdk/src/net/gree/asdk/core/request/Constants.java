/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gree.asdk.core.request;

public final class Constants {
  private Constants() {}

  public static enum HTTP_METHOD {
    GET, POST, PUT, DELETE
  };

  public static enum OAUTH_TYPE {
    _3LEGGED, _2LEGGED, _NONE
  }

  public static final String GRADE0_ERROR_MESSAGE = "User is not logged in";

  public static final boolean USE_SSL = false;

  public static final String DisabledMessage = "Network requests disabled by application.";
}
