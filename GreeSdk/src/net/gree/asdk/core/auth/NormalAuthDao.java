package net.gree.asdk.core.auth;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.auth.AuthorizerCore.OnOAuthResponseListener;
import net.gree.asdk.core.auth.OAuthUtil.OnCloseOAuthAlertListener;
import net.gree.asdk.core.auth.SetupActivity.SetupListener;
import net.gree.asdk.core.auth.UserInfoUpdater.AgreementListener;
import net.gree.asdk.core.inject.Inject;
import net.gree.asdk.core.util.Scheme;
import net.gree.oauth.signpost.exception.OAuthException;

public class NormalAuthDao implements OAuthNormalDao{

  private static final String TAG = "NormalAuthDao";
  
  IAuthorizer mAuthorizer;
  Context mContext;
  SetupListener mListener;
  // since by default still we should use our normal API
  boolean mRedirect = true;
  
  @Inject
  public NormalAuthDao(){
    // default injection method
  }
  
  public void initalize(IAuthorizer authorizer, Context context, SetupListener listener){
    mAuthorizer = authorizer;
    mContext = context;
    mListener = listener;
  }
  
  /**
   * Takes the normal Access Token uri and performs error check on it before passing 
   * it along to its accessor
   * 
   * @param uri the URI we receive from the web returned
   */
  public void retrieveAccessToken(Uri uri) {
    if (uri == null) {
      return;
    }
    final String DENIED = "denied";

    final String url = uri.toString();
    if (url.startsWith(Scheme.getAccessTokenScheme())) {
      String denied = uri.getQueryParameter(DENIED);
      if (!TextUtils.isEmpty(denied)) {
        GLog.d(TAG, "SSO is denied");
        return;
      }
      // no dialog, we want to handle it as such
      retrieveAccessToken(url);
    }
  }
  
  /**
   * Handles accessing the authorization token when there is not a dialog present
   * @param url the url, obtained from the URI
   */
  private void retrieveAccessToken(String url) {
    mAuthorizer.retrieveAccessToken(null, url, new OnOAuthResponseListener<Void>() {
      public void onSuccess(Void response) {
        updateInBackground();
        mAuthorizer.clearAccessTokenParams();
        GLog.d(TAG, "OAuth Authorize is done.");
      }

      public void onFailure(final OAuthException e) {
        OAuthUtil.handleException(e, mContext, null, null, new OnCloseOAuthAlertListener() {
          public void onClose() {
            mAuthorizer.clearAccessTokenParams();
          }
        });
      }
    });  
  }
  
  /**
   * After a successful call to authorize via the background thread, we update everything
   */
  private void updateInBackground() {
    new Thread(new Runnable() {
      public void run() {
        AgreementListener listener = new AgreementListener() {
          public void onNeedAgreement(String url) {
            AgreementDialog dialog = new AgreementDialog(mContext, url);
            dialog.setOnDismissListener(new OnDismissListener() {
              public void onDismiss(DialogInterface dialog) {
                publishSuccess();
              }
            });
          }
        };
        UserInfoUpdater updater = new UserInfoUpdater(mContext).sync(true).listener(listener);
        updater.request();
        publishSuccess();
      }
    }).start();
  }
  
  /**
   * Looper to notify the SetupListener of success when there is no Dialog present
   */
  public void publishSuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      public void run() {
        if(mListener!=null)
          mListener.onSuccess();
          return;
        }
    });   
  }

  @Override
  public boolean getIsDirectingToSetupActivity() {
    return mRedirect;
  }

  @Override
  public void setRedircetToSetupActivity(boolean shouldRedirect) {
    mRedirect = shouldRedirect;
  }
}
