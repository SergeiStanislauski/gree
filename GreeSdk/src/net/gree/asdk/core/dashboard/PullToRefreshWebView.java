package net.gree.asdk.core.dashboard;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import net.gree.asdk.core.RR;
import net.gree.asdk.core.ui.CommandInterfaceWebView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Picture;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.handmark.pulltorefresh.library.PullToRefreshBase;

class PullToRefreshWebView extends PullToRefreshBase<FrameLayout> implements PullToRefreshBase.OnRefreshListener {
  public static interface OnRefreshAfterScreenShotListener {
    /**
     * onRefreshAfterScreenShot will be called for both Pull Down from top, and Pull Up from Bottom,
     * after the screen shot is taken and displayed.
     * 
     */
    public void onRefreshAfterScreenShot();
  }

  private static final String PULL_TO_REFRESH_LAST_UPDATE_ = "gree_pull_to_refresh_last_update";
  private static final SimpleDateFormat DATE_FORMAT_ = new SimpleDateFormat("M/dd/yy h:mm a", Locale.US);

  private Handler mUiThreadHandler;
  private ImageView mScreenShot;
  private CommandInterfaceWebView mCIWebView;
  private OnRefreshAfterScreenShotListener mListener;

  public PullToRefreshWebView(Context context) {
    super(context);
  }

  // This is called from constructor.
  @Override
  protected FrameLayout createRefreshableView(Context context, AttributeSet attrs) {
    mUiThreadHandler = new Handler(context.getMainLooper());
    FrameLayout frame = new FrameLayout(context);
    mCIWebView = new CommandInterfaceWebView(context);
    mCIWebView.setWebChromeClient(new WebChromeClient() {
      @Override
      public void onProgressChanged(WebView view, int newProgress) {
        if (newProgress == 100) {
          onRefreshComplete();
        }
      }
    });

    mScreenShot = new ImageView(context);
    mScreenShot.setVisibility(View.GONE);
    mScreenShot.setScaleType(ScaleType.MATRIX);

    frame.addView(mCIWebView, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    frame.addView(mScreenShot, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

    super.setOnRefreshListener(this);

    return frame;
  }

  protected void setOnRefreshAfterScreenShotListener(OnRefreshAfterScreenShotListener listener) {
    mListener = listener;
  }

  @Override
  protected boolean isReadyForPullDown() {
    return mCIWebView.getScrollY() < 2;
  }

  @Override
  protected boolean isReadyForPullUp() {
    return mCIWebView.getScrollY() >= (mCIWebView.getContentHeight() - mCIWebView.getHeight() -1);
  }

  @Override
  public void onRefresh() {
    Picture picture = mCIWebView.capturePicture();
    if (picture != null && picture.getWidth() > 0 && picture.getHeight() > 0) {
      Bitmap bmp = Bitmap.createBitmap(picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(bmp);
      picture.draw(canvas);

      mScreenShot.setImageBitmap(bmp);
      Matrix mat = new Matrix();
      float scale = (float)mScreenShot.getWidth() / bmp.getWidth();
      mat.setScale(scale, scale);
      mScreenShot.setImageMatrix(mat);
      mScreenShot.setVisibility(View.VISIBLE);
    }

    mListener.onRefreshAfterScreenShot();
  }

  public void removeScreenShot() {
    mUiThreadHandler.post(new Runnable(){
      @Override
      public void run() {
        mScreenShot.setVisibility(View.GONE);
        mScreenShot.setImageBitmap(null);
      }
    });
  }

  public CommandInterfaceWebView getCommandInterfaceWebView() {
    return mCIWebView;
  }

  public void updateLastUpdateTextView() {
    TextView textView = (TextView)findViewById(RR.id(PULL_TO_REFRESH_LAST_UPDATE_));
    synchronized(DATE_FORMAT_) {
      textView.setText(
          getContext().getResources().getString(RR.string(PULL_TO_REFRESH_LAST_UPDATE_)) + " " +
          DATE_FORMAT_.format(new Date())
          );
    }
  }
}
