/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.api;

import net.gree.asdk.core.GConnectivityManager;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.auth.IAuthorizer;
import net.gree.asdk.core.analytics.performance.IPerformanceManager;
import net.gree.asdk.core.analytics.performance.PerformanceData;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap;
import net.gree.asdk.core.analytics.performance.PerformanceIndexMap.PerformanceFlowIndex;
import net.gree.asdk.core.cache.ImageFetcher;
import net.gree.asdk.core.codec.GreeHmac;
import net.gree.asdk.core.request.Constants;
import net.gree.asdk.core.request.OnResponseCallback;
import net.gree.asdk.core.track.Tracker;
import net.gree.asdk.core.track.Tracker.Uploader;
import net.gree.asdk.core.util.UtilWrapper;
import net.gree.vendor.com.google.gson.Gson;
import net.gree.vendor.com.google.gson.annotations.SerializedName;

import org.apache.http.HeaderIterator;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;

import java.security.InvalidParameterException;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implements Gree Leaderboard API.  This API includes leaderboards and high scores.
 * @author GREE, Inc.
 */
public class Leaderboard {
  private static final String TAG = "Leaderboard";
  private static final String TRACK_TYPE = "HIGHSCORE";
  private static boolean debug = false;
  private static boolean verbose = false;
  private static final int _404 = 404;

  /**
   * Sets the debug flag for this class.
   * 
   * @param isDebug true for on or false for off
   */
  public static void setDebug(boolean isDebug) {
    Leaderboard.debug = isDebug;
  }

  /**
   * Sets the verbose flag for this class.
   * 
   * @param isVerbose true for on or false for off
   */
  public static void setVerbose(boolean isVerbose) {
    Leaderboard.verbose = isVerbose;
  }

  /**
   * The implementation class signature of Uploader might be stored in the db and it will be
   * reflective loaded by Tracker. Moreover proguard maybe change the class signature, so it uses a
   * named static class here. In addition, the following rules should be put in the proguard.cfg.
   *
   * <pre>
   *   -keep class net.gree.asdk.core.track.Tracker$Uploader { *; }
   *   -keep class net.gree.asdk.api.Leaderboard$UploaderImpl { *; }
   * </pre>
   *
   */
  private static class UploaderImpl implements Uploader {
    @Override
    public void upload(final String type, final String key, final String value,
                       final Tracker.UploadStatus cb) {
      final long score = Long.parseLong(value);
      uploadScore(key, score, new SuccessListener() {
        public void onSuccess() {
          if (cb != null) {
            cb.onSuccess(type, key, value);
          }
          SuccessListener listener = deleteAfterGetSuccessListener(key, score);
          if (listener != null) {
            listener.onSuccess();
          }
        }

        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          SuccessListener listener = deleteAfterGetSuccessListener(key, score);
          if (listener != null) {
            listener.onFailure(responseCode, headers, response);
          }
          if (cb != null) {
            cb.onFailure(type, key, value, responseCode, response);
          }
        }
      });
    }
  }

  private static Uploader uploader = new UploaderImpl();

  private Leaderboard() {
  }

  /**
   * Interface definition for a callback to be invoked for the delivery of leaderboard results.
   */
  public interface LeaderboardListener {
   
   
    /**
     * Called when leaderboards for this application are successfully retrieved.
     * @param index the index in the list of leaderboards.
     * @param totalListSize the total number of leaderboards for this application. 
     * @param leaderboards An array of leaderboards.
     */
   
    void onSuccess(int index, int totalListSize, Leaderboard[] leaderboards);

   
   
    /**
     * Called when an error occurs trying to retrieve the list of leaderboards.
     * @param responseCode the code of the http response 
     * @param headers the headers of the http response
     * @param response the body of the http response
     */
   
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }
  /**
   * Interface definition for a callback to be invoked on delivery of array of score results.
   */
  public interface ScoreListener {
   
   
    /**
     * Called when the score for this leaderboard is successfully retrieved.
     * @param entries An array Scores.
     */
   
    void onSuccess(Score[] entries);

   
   
    /**
     * Called when an error occurred trying to retrieve the scores for this leaderboard.
     * @param responseCode the code of the http response 
     * @param headers the headers of the http response
     * @param response the body of the http response
     */
   
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }
  /**
   * Delivery of pass/fail results,
   * used when uploading or deleting a score for a leaderboard.
   */
  public interface SuccessListener {
 
 
    /**
     * Called when the score was correctly added or deleted on given leaderboard.
     */
 
    void onSuccess();

   
   
      /**
       * Called when something went wrong when trying to add or delete a score on given leaderboard.
       * @param responseCode the code of the http response 
       * @param headers the headers of the http response
       * @param response the body of the http response
       */
   
    void onFailure(int responseCode, HeaderIterator headers, String response);
  }

  /**
   * To receive leaderboard objects, created from Gson
   */
  private static class ResponseArray {
    public Leaderboard[] entry;
    public int startIndex;
    public int totalResults;
    public int itemsPerPage;
  }

  private String id;
  private String name;
  @SerializedName("thumbnail_url")
  private String thumbnailUrl;
  private String format;
  @SerializedName("format_suffix")
  private String formatSuffix;
  @SerializedName("format_decimal")
  private String formatDecimal;
  @SerializedName("time_format")
  private String timeFormat;
  private String sort;
  @SerializedName("allow_worse_score")
  private String allowWorseScore;
  private String secret;

  /**
   * To receive a score response, create from Gson
   */
  private static class ScoreResponse {
    public String totalResults;
    public Score[] entry;
  }
  /**
   * The score object  for the user. It holds one score per user and contains methods
   *  to retrieve all the information contained within.
   */
  public static class Score {
    private String id;
    private String nickname;
    private String thumbnailUrl;
    private String thumbnailUrlHuge;
    private String thumbnailUrlSmall;
    private String rank;
    private String score;
    // private transient BitmapLoader mThumbnailLoader;
    // private transient BitmapLoader mThumbnailSmallLoader;
    // private transient BitmapLoader mThumbnailHugeLoader;

    protected static final String[] SCORE_TYPES = {"@self", "@friends", "@all"};
    protected static final String[] PERIOD_TYPES = {"daily", "weekly", "total"};

    public static final int MY_SCORES = 0;
    public static final int FRIENDS_SCORES = 1;
    public static final int ALL_SCORES = 2;
    public static final int DAILY = 0;
    public static final int WEEKLY = 1;
    public static final int ALL_TIME = 2;

   
 
    /**
     * Small size
     */
   
    public static final int THUMBNAIL_SIZE_SMALL = 0;

   
 
    /**
     * Standard size
     */
   
    public static final int THUMBNAIL_SIZE_STANDARD = 1;

   
 
    /**
     * Huge size
     */
   
    public static final int THUMBNAIL_SIZE_HUGE = 2;

   
   
    /**
     * Gets the id of the user who posted this score object.
     * @return a user id
     */
   
    public String getId() {
      return id;
    }

   
   
      /**
       * Gets the nickname of the user who posted this score object.
       * @return a user nickname
       */
   
    public String getNickname() {
      return nickname;
    }

   
   
      /**
       * Gets the thumbnail URL for the avatar of the user who posted this score object.
       * @param size specify size of thumbnail
       * @return a thumbnail url
       */
   
    public String getThumbnailUrl(final int size) {
      switch (size) {
        case THUMBNAIL_SIZE_SMALL:
          return thumbnailUrlSmall;
        case THUMBNAIL_SIZE_STANDARD:
          return thumbnailUrl;
        case THUMBNAIL_SIZE_HUGE:
          return thumbnailUrlHuge;
        default:
          return null;
      }
    }

   
   
      /**
       * Gets the thumbnail URL for the avatar of the user who posted this score object.
       * @return a standard thumbnail url
       * @deprecated use getThumbnailUrl(int size) instead
       */
   
    public String getThumbnailUrl() {
      return thumbnailUrl;
    }

   
   
      /**
       * Gets the huge image thumbnail URL for the avatar of the user who posted this score object.
       * @return a small thumbnail url
       * @deprecated use getThumbnailUrl(int size) instead
       */
   
    public String getThumbnailUrlHuge() {
      return thumbnailUrlHuge;
    }

   
   
      /**
       * Gets the small image thumbnail URL for the avatar of the user who posted this score object.
       * @return a small thumbnail url
       * @deprecated use getThumbnailUrl(int size) instead
       */
   
    public String getThumbnailUrlSmall() {
      return thumbnailUrlSmall;
    }

   
   
      /**
       * Gets the rank of the user who posted this score object.
       * @return the rank
       */
   
    public long getRank() {
      try {
        return Long.parseLong(rank);
      } catch (Exception e) {
        return -1;
      }
    }

   
 
  /**
   * Retrieves the scores from the server, for this leaderboard.
   * This score represents a score value or a time in seconds,depending on the type of leaderboard score format. return the score as a Long value.
   * @return return value of score when the score can cast to seconds or Long value. or fail return -1.
   */
 
    public long getScore() {

      if (null == score) {
        return -1;
      }

      try {
        return Long.parseLong(score);
      } catch (Exception e) {
        // maybe a time score
      }

      String[] strScore = score.split(":");
      long seconds = 0;

      if (strScore.length == 3) {
        for (int i = 0; i < strScore.length; i++) {
          try {
            seconds += Long.parseLong(strScore[strScore.length - 1 - i]) * (long) Math.pow(60, i);
          } catch (Exception e) {
            return -1;
          }
        }
        return seconds;
      }
      return -1;
    }

    /**
     * Gets the score from the server for this leaderboard. <br>
     * If the leaderboard's format type is a time instead of a value,
     * this will return the score as a time formated string.
     * @return the score as a string
     */
    public String getScoreAsString() {
      return score;
    }

 
  /**
   * Gets the thumbnail, synchronize call, return immediately.
   * @param size specify size of thumbnail
   * @return if the thumbnail is ready, return the bitmap,otherwise return null
   */
    public Bitmap getThumbnail(final int size) {
      switch (size) {
        case THUMBNAIL_SIZE_SMALL:
        	return getImageFetcher().getBitmap(getThumbnailUrl(THUMBNAIL_SIZE_SMALL));
        case THUMBNAIL_SIZE_STANDARD:
        	return getImageFetcher().getBitmap(getThumbnailUrl(THUMBNAIL_SIZE_STANDARD));
        case THUMBNAIL_SIZE_HUGE:
        	return getImageFetcher().getBitmap(getThumbnailUrlHuge());
        default:
          return null;
      }
    }

  /**
   * Gets the thumbnail, synchronize call, return immediately.
   * @return if the thumbnail is ready, return the bitmap,otherwise return null
   * @deprecated use getThumbnail(int size) instead
   */
    public Bitmap getThumbnail() {
    	return getImageFetcher().getBitmap(getThumbnailUrl());
    }

  /**
   * Gets the small thumbnail Bitmap. A synchronized call, that returns immediately.
   * @return if the small thumbnail is ready to be downloaded it returns the bitmap. Otherwise it returns null
   * @deprecated use getThumbnail(int size) instead
   */
    public Bitmap getSmallThumbnail() {
    	return getImageFetcher().getBitmap(getThumbnailUrlSmall());
    }

   /**
   * Gets the huge thumbnail Bitmap. A synchronized call, that returns immediately.
   * @return if the huge thumbnail is ready to be downloaded it returns the bitmap. Otherwise it returns null
   * @deprecated use getThumbnail(int size) instead
   */
    public Bitmap getHugeThumbnail() {
      return getImageFetcher().getBitmap(getThumbnailUrlHuge());
    }


  /**
  * Loads the thumbnail of the user who posted this score. This is an asynchronous, non-blocking call. The listener will be 
  * called back when the request is done. <br>
  * Pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @param size specify size of thumbnail
  * @return returns true if there is a successful call; false if the request could not be 
  * fulfilled (there is no listener, a network error, a permission error, etc).
  */
    public boolean loadThumbnail(final int size, final IconDownloadListener listener) {
      switch (size) {
        case THUMBNAIL_SIZE_SMALL:
          getImageFetcher().loadImage(getThumbnailUrl(THUMBNAIL_SIZE_SMALL), listener);
          break;
        case THUMBNAIL_SIZE_STANDARD:
        	getImageFetcher().loadImage(getThumbnailUrl(THUMBNAIL_SIZE_STANDARD), listener);
          break;
        case THUMBNAIL_SIZE_HUGE:
        	getImageFetcher().loadImage(getThumbnailUrlHuge(), listener);
          break;
        default:
          return false;
      }
      return true;
    }

  /**
  * Loads the thumbnail of the user who posted this score. This is an asynchronous, non-blocking call. The listener will be 
  * called back when the request is done. <br>
  * Pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @return returns true if there is a successful call; false if the request could not be 
  * fulfilled (there is no listener, a network error, a permission error, etc).
  * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
  */
    public boolean loadThumbnail(final IconDownloadListener listener) {
    	getImageFetcher().loadImage(getThumbnailUrl(), listener);
      return true;
    }

   
  /**
  * Loads the small thumbnail of the user who posted this score. This is an asynchronous, non-blocking call. The listener will be 
  * called back when the request is done. <br>
  * Pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @return returns true if there is a successful call; false if the request could not be 
  * fulfilled (there is no listener, a network error, a permission error, etc).
  * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
  */
    public boolean loadSmallThumbnail(final IconDownloadListener listener) {
    	getImageFetcher().loadImage(getThumbnailUrlSmall(), listener);
      return true;
    }

   
  /**
  * Loads the large thumbnail of the user who posted this score. This is an asynchronous, non-blocking call. The listener will be 
  * called back when the request is done. <br>
  * Pass in the {@link IconDownloadListener} to get the result of the request.
  * 
  * @return returns true if there is a successful call; false if the request could not be 
  * fulfilled (there is no listener, a network error, a permission error, etc).
  * @deprecated use loadThumbnail(int size, IconDownloadListener listener) instead
  */
    public boolean loadHugeThumbnail(final IconDownloadListener listener) {
    	getImageFetcher().loadImage(getThumbnailUrlHuge(), listener);
      return true;
    }
  }

  /**
   * Loads the leaderboard info for the current user.
   * 
   * @param startIndex the start index in the leaderboard list
   * @param count the number of leaderboard to load
   * @param listener the listener to receive the results
   */
  public static void loadLeaderboards(int startIndex, int count,
      final LeaderboardListener listener) {
    if (startIndex == 0) {
      throw new InvalidParameterException("StartIndex must be 1 or higher in OpenSocial api");
    }
    getLeaderboards("@me", startIndex, count, listener);
  }

  /**
   * Retrieves leaderboards for a given userid.
   * 
   * @param userId the user Id 
   * @param startIndex the index starting from 1 for Leaderboards list.
   * @param count Number of items are requested.
   * @param the listener to receive the results
   */
  private static void getLeaderboards(String userId, int startIndex, int count,
      final LeaderboardListener listener) {
    Request request = new Request();
    if (userId == null || userId.length() == 0) {
      userId = "@me";
    }
    String action = "/sgpleaderboard/" + userId + "/@app";
    if (debug) {
      GLog.d("Leaderboard.:", action);
    }

    TreeMap<String, Object> args = new TreeMap<String, Object>();
    if (startIndex > 0) {
      args.put("startIndex", Integer.toString(startIndex));
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_SGPLEADERBOARD_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    args.put("count", Integer.toString(count));
    request.oauthGree(action, "GET", args, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) {
          GLog.d(TAG, "Http response:" + json);
        }
        try {
          Gson gson = Injector.getInstance(Gson.class);
          ResponseArray response = gson.fromJson(json, ResponseArray.class);
          if (verbose) {
            GLog.v(TAG, "response " + response.startIndex + " " + response.totalResults + " " + response.itemsPerPage);
          }
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          listener.onSuccess(response.startIndex, response.totalResults, response.entry);
        } catch (Exception ex) {
          GLog.printStackTrace(TAG, ex);
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
          manager.flushData(performData);
          listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        listener.onFailure(responseCode, headers, response);
      }
    });
  }

  // These are all currently defined fields for rankings.
  static final String scoreFields =
      "id,nickname,thumbnailUrl,thumbnailUrlSmall,thumbnailUrlLarge,thumbnailUrlHuge,rank,score";

  /**
   * Gets the scores from the server, for a given leaderboard id.
   * 
   * @param lid the id of the leaderboard
   * @param selector should be one of Ranking.MY_SCORES, Ranking.FRIENDS_SCORES or Ranking.ALL_SCORES
   * @param period should be one of Ranking.DAILY, Ranking.WEEKLY or Ranking.ALL_TIME
   * @param startIndex the index starting from 1 for the score list.
   * @param count the number of scores to get
   * @param listener the listener which receives the results
   */
  public static void getScore(String lid, int selector, int period, int startIndex, int count,
      final ScoreListener listener) {
    if (startIndex == 0) {
      throw new InvalidParameterException("StartIndex must be 1 or higher in OpenSocial api");
    }
    getScore("@me", lid, selector, period, startIndex, count, listener);
  }

 
 
  /**
   * Gets the scores from the server, for this leaderboard.
   * 
   * @param selector should be one of Ranking.MY_SCORES, Ranking.FRIENDS_SCORES or Ranking.ALL_SCORES
   * @param period should be one of Ranking.DAILY, Ranking.WEEKLY or Ranking.ALL_TIME
   * @param startIndex the index starting from 1 for the score list.
   * @param count the number of scores to get
   * @param listener the listener which receives the results
   */
 
  public void getScore(int selector, int period, int startIndex, int count,
      final ScoreListener listener) {
    if (startIndex == 0) {
      throw new InvalidParameterException("StartIndex must be 1 or higher in OpenSocial api");
    }
    getScore("@me", getId(), selector, period, startIndex, count, listener);
  }
  
  private static ImageFetcher mImageFetcher;
  
  /**
   * Creating a new static instance of the Image Fetcher
   */
  private static ImageFetcher getImageFetcher(){
	if (null == mImageFetcher) {
		mImageFetcher = Injector.getInstance(ImageFetcher.class);
	}
	  return mImageFetcher; 
  }

  private static void getScore(String uid, String lid, int selector, int period, int startIndex,
      int count, final ScoreListener listener) {
    Request request = new Request();
    String action = "/sgpranking/" + uid + "/" + Score.SCORE_TYPES[selector] + "/@app";
    if (debug) {
      GLog.d("Leaderboard.score:", action);
    }
    TreeMap<String, Object> data = new TreeMap<String, Object>();
    data.put("category", lid);
    data.put("fields", scoreFields);
    data.put("period", Score.PERIOD_TYPES[period]);
    if (startIndex > 0) {
      data.put("startIndex", Integer.toString(startIndex - 1));
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_SGPRANKING_GET);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    data.put("count", Integer.toString(count));
    request.oauthGree(action, "GET", data, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) {
          GLog.d("Score", "Http response:" + json);
        }
        try {
          ScoreResponse response = Injector.getInstance(Gson.class).fromJson(json, ScoreResponse.class);
          if (verbose) {
            GLog.v(TAG, "response totalresults=" + response.totalResults);
          }
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          listener.onSuccess(response.entry);
        } catch (Exception ex) {
          GLog.printStackTrace(TAG, ex);
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
          manager.flushData(performData);
          listener.onFailure(responseCode, headers, ex.toString() + ":" + json);
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        //This is to address ticket GGPCLIENTSDK-2027
        //server is responding 404 for a non existing score to match OpenSocial specs.
        if (responseCode == _404) {
          if (verbose) {
            GLog.v(TAG, "Received an empty score");
          }
          manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
          manager.flushData(performData);
          listener.onSuccess(new Score[0]);
          return;
        }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        listener.onFailure(responseCode, headers, response);
      }
    });
  }

  private static Map<String, SuccessListener> scoreUploaderlistener =
      new ConcurrentHashMap<String, SuccessListener>();

  private static String getSuccessListenerKey(String lid, long score) {
    return lid + score;
  }

  private static SuccessListener deleteAfterGetSuccessListener(String leaderboardId, long score) {
    String key = getSuccessListenerKey(leaderboardId, score);
    try {
      return scoreUploaderlistener.get(key);
    } finally {
      scoreUploaderlistener.remove(key);
    }
  }

 
  /**
   * Uploads a new score to this leaderboard.<br>
   * The request to upload this new score will be retried automatically if the score 
   * can not be delivered (e.g the server is not reachable) at the time of request.
   * 
   * @param score value of the score as a long
   * @param listener listener to where the results are delivered
   */
  public void createScore(final long score, final SuccessListener listener) {
    createScore(getId(), score, listener);
  }

 
  /**
   * Uploads a new score to this leaderboard.<br>
   * The request to upload this new score will be retried automatically if the score 
   * can not be delivered (e.g the server is not reachable) at the time of request.
   * 
   * @param lid id of leaderboard
   * @param score value of the score as a long
   * @param listener listener to where the results are delivered
   */
  public static void createScore(final String lid, final long score, final SuccessListener listener) {
    
    // Throwing exception if Leaderboard ID is empty
    if (lid == null || lid.trim().length() == 0) {
      throw new IllegalArgumentException("Leaderboard ID is empty ");
    }
    
    if (getUtilWrapper().canOptOutOfGREE() && !getAuthorizer().hasOAuthAccessToken()) {
      new Handler(Looper.getMainLooper()).post(new Runnable() {
        public void run() {
          if (listener != null) {
            listener.onFailure(0, null, Constants.GRADE0_ERROR_MESSAGE);
          }
        }
      });
      return;
    }
    scoreUploaderlistener.put(getSuccessListenerKey(lid, score), listener);
    String userId = getAuthorizer().getOAuthUserId();
    Injector.getInstance(Tracker.class).track(userId, TRACK_TYPE, lid, "" + score, uploader);
    //Generate a failure because there is no network connection,
    //Tracker will retry automatically when network is available
    if ((listener != null) && (!Injector.getInstance(GConnectivityManager.class).checkConnectivity())) {
      listener.onFailure(408, null, "No network connection, will retry later.");
    }
  }

  private static IAuthorizer sAuthorizer;

  private static IAuthorizer getAuthorizer() {
    if (null == sAuthorizer) {
      sAuthorizer = Injector.getInstance(IAuthorizer.class);
    }
    return sAuthorizer;
  }

  private static UtilWrapper sUtilWrapper;

  private static UtilWrapper getUtilWrapper() {
    if (null == sUtilWrapper) {
      sUtilWrapper = new UtilWrapper();
    }
    return sUtilWrapper;
  }

  private static void uploadScore(final String leaderboardId, final long score,
      final SuccessListener listener) {
    Request request = new Request();
    String action = "/sgpscore/@me/@self/@app";
    if (debug) {
      GLog.d("Leaderboard.createScore:", action);
    }
    TreeMap<String, Object> data = new TreeMap<String, Object>();
    data.put("category", leaderboardId);
    String userId = getAuthorizer().getOAuthUserId();
    if (userId == null) {
      if (listener != null) {
        listener.onFailure(0, null, Constants.GRADE0_ERROR_MESSAGE);
      }
      return;
    }
    String[] hash = GreeHmac.hmacsha1(userId, "" + score);
    if (hash[2] != null) {
      if (listener != null) {
        listener.onFailure(0, null, "Failed hash encryption:" + hash[2]);
      }
      return;
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_SGPSCORE_POST);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);

    data.put("hash", hash[0]);
    data.put("nonce", hash[1]);
    data.put("score", "" + score);

    request.oauthGree(action, "POST", data, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) {
          GLog.d("Score", "Http response:" + json);
        }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
        manager.flushData(performData);
        if (listener != null) {
          listener.onSuccess();
        }
      }

      @Override
      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        if (listener != null) {
          listener.onFailure(responseCode, headers, response);
        }
      }
    });
  }

  /**
   * Requests the deletion of score from this leaderboard. <br> This method is for testing purpose only.
   * 
   * @param listener the listener to which receives the results 
   */
  public void deleteScore(final SuccessListener listener) {
    deleteScore(getId(), listener);
  }

  /**
   * Requests the deletion of score for the user identified leaderboard. <br> This method is for testing purpose only.
   * 
   * @param leaderboardId id of leaderboard
   * @param listener the listener to which receives the results 
   */
  public static void deleteScore(String leaderboardId, final SuccessListener listener) {
    Request request = new Request();
    String action = "/sgpscore/@me/@self/@app";
    if (debug) {
      GLog.d("Leaderboard.score:", action);
    }
    TreeMap<String, Object> data = new TreeMap<String, Object>();
    data.put("category", leaderboardId);
    String userId = getAuthorizer().getOAuthUserId();
    if (userId == null) {
      listener.onFailure(0, null, "User is not logged in");
      return;
    }
    String[] hash = GreeHmac.hmacsha1(userId, leaderboardId);
    if (hash[2] != null) {
      listener.onFailure(0, null, "Failed hash encryption:" + hash[2]);
      return;
    }
    final IPerformanceManager manager = Injector.getInstance(IPerformanceManager.class);
    final PerformanceData performData = manager.createData(PerformanceFlowIndex.OS, PerformanceIndexMap.OS_SUB_INDEX_SGPSCORE_DELETE);
    manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_START);
    data.put("hash", hash[0]);
    data.put("nonce", hash[1]);

    request.oauthGree(action, "DELETE", data, null, false, new OnResponseCallback<String>() {
      @Override
      public void onSuccess(int responseCode, HeaderIterator headers, String json) {
        if (debug) {
          GLog.d("Score", "Http response:" + json);
        }
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_END);
        manager.flushData(performData);
        listener.onSuccess();
      }

      public void onFailure(int responseCode, HeaderIterator headers, String response) {
        manager.recordData(performData, PerformanceIndexMap.INDEX_KEY_URL_LOAD_ERROR);
        manager.flushData(performData);
        listener.onFailure(responseCode, headers, response);
      }
    });
  }

  /**
   * Interface for receiving logging of object values.
   */
  public interface LogLabelValue {
 
 
    /**
     * Function for receiving logging of object values.
     * @param label the log label
     * @param value the log value
     */
 
    void log(String label, String value);
  }

  /**
   * Default logger. Will send all logs to the system logging system.
   */
  public static LogLabelValue llvLogcat = new LogLabelValue() {
    public void log(String l, String v) {
      logn(l, v);
    }
  };

  /**
   * Logs the leaderboards with the default logger.
   * 
   * @param leaderboards the list of leaderboards
   */
  public static void logLeaders(Leaderboard[] leaderboards) {
    logLeaders(leaderboards, llvLogcat);
  }

  /**
   * Logs the leaderboards with the given LogLabelValue.
   * 
   * @param leaderboards the list of leaderboards
   * @param llv the logger
   */
  public static void logLeaders(Leaderboard[] leaderboards, LogLabelValue llv) {
    for (int i = 0; i < leaderboards.length; i++) {
      debug("leader:" + i);
      logLeader(leaderboards[i], llv);
    }
  }

  /**
   * Logs a single LeaderboardInfo with default logger.
   * 
   * @param leader a single leaderboard id
   */
  public static void logLeader(Leaderboard leaderboard) {
    logLeader(leaderboard, llvLogcat);
  }

  /**
   * Logs a single LeaderboardInfo with given LogLabelValue .
   * 
   * @param leaderboard a single leaderboard id
   * @param llv the logger's log label value
   */
  public static void logLeader(Leaderboard leaderboard, LogLabelValue llv) {
    llv.log("id", leaderboard.getId());
    llv.log("name", leaderboard.getName());
    llv.log("thumbnailUrl", leaderboard.getThumbnailUrl());
    llv.log("format", String.valueOf(leaderboard.getFormat()));
    llv.log("formatSuffix", String.valueOf(leaderboard.getFormatSuffix()));
    llv.log("formatDecimal", String.valueOf(leaderboard.getFormatDecimal()));
    llv.log("timeformat", String.valueOf(leaderboard.getTimeFormat()));
    llv.log("sort", String.valueOf(leaderboard.getSort()));
    llv.log("allowWorseScore", leaderboard.allowWorseScore);
    llv.log("secret", leaderboard.isSecret() ? "1" : leaderboard.secret);
  }

 
  /**
   * Logs an array of score with the passed logger.
   * 
   * @param scores the list of scores
   * @param llv the logger
   */
  public static void logScore(Score[] scores) {
    logScore(scores, llvLogcat);
  }

  /**
   * Logs an array of score with the passed logger.
   * 
   * @param scores the list of scores
   * @param llv the logger
   */
  public static void logScore(Score[] scores, LogLabelValue llv) {
    for (int i = 0; i < scores.length; i++) {
      debug("leader:" + i);
      Score score = scores[i];
      llv.log("id", score.getId());
      llv.log("nickname", score.getNickname());
      llv.log("rank", String.valueOf(score.getRank()));
      llv.log("score", String.valueOf(score.getScore()));
      llv.log("thumUrl", score.getThumbnailUrl(Score.THUMBNAIL_SIZE_STANDARD));
      llv.log("thumUrlHuge", score.getThumbnailUrlHuge());
      llv.log("thumUrlSmall", score.getThumbnailUrlSmall());
      llv.log("scoreVerify", score.getScoreAsString());
    }
  }

  /**
   * Log msg+val, used by object logging convenience methods.
   * 
   * @param msg 
   * @param val
   */
  private static void logn(String msg, String val) {
    if (val != null && val.length() > 0) {
      debug(msg + ":" + val);
    }
  }

  private static void debug(String msg) {
    GLog.d(TAG, msg);
  }

  /**
   * Gets the id for this leaderboard.
   * @return the leaderboard Id
   */
  public String getId() {
    return id;
  }

  /**
   * Gets the name for this leaderboard.
   *@return the leaderboard name
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the thumbnail url for this leaderboard.
   * @return the url of the thumbnail
   */
  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  /**
   * The score int, that is a simple value that can be accessed as a Long from the method {@link #getScore}
   */
  public static final int FORMAT_VALUE = 0;

  /**
   * The time int, that is a time value that can be accessed as a String from the method {@link #getScoreAsString}
   */
  public static final int FORMAT_TIME = 2;

  /**
   * Gets the determining flag for how score values in this leaderboard are displayed.
   * @return either FORMAT_VALUE or FORMAT_TIME
   */
  public int getFormat() {
    try {
      int formatInt = Integer.valueOf(format);
      if (formatInt == 2) {
        return FORMAT_TIME;
      }
    } catch (NumberFormatException nfe) {
      GLog.e(TAG, "Score format is invalid " + format);
    }
    return FORMAT_VALUE;
  }

  /**
   * Gets the formatting value for the suffix, either based on time or score values. These value can be set in the developer center on the leaderboard page.
   * 
   * @return the score suffix format
   * <i><br> If the Score format is FORMAT_TIME, then this returns a localized version of 'hour', 'minute', 'seconds'.
   * <br>If the score format is FORMAT_VALUE, then this returns the suffix to be appended to each score value.</i>
   */
  public String getFormatSuffix() {
    return formatSuffix;
  }
  
  /**
   * This is used only when the score format is set to FORMAT_TIME
   * @return If the Score format is FORMAT_TIME, then this returns one of 'hour', 'minute', 'seconds', null otherwise
   */
  public String getTimeFormat() {
    return timeFormat;
  }

  /**
   * Gets the number of decimal places to display.
   * @return number of decimal places. Returns '0' if there is an error (does not mean it is an error).
   */
  public int getFormatDecimal() {
    try {
      int res = Integer.valueOf(formatDecimal);
      return res;
    } catch (NumberFormatException nfe) {
      GLog.e(TAG, "Invalid format Decimal " + formatDecimal);
      return 0;
    }
  }

  /**
   * Scores will be sorted in ascending order (low values first)
   */
public static final int SORT_ORDER_ASCENDING = 1;

/**
 * Scores will be sorted in descending order (high values first)
 */
public static final int SORT_ORDER_DESCENDING = 0;

  /**
   * Gets the value of how score values are sorted.
   * @return SORT_ORDER_ASCENDING for values sorted in ascending order or 
   * SORT_ORDER_DESCENDING for values sorted in descending order
   * 
   */
  public int getSort() {
    int sortInt = SORT_ORDER_ASCENDING;
    sortInt = Integer.valueOf(sort);
    if ((sortInt != SORT_ORDER_ASCENDING) && (sortInt != SORT_ORDER_DESCENDING)) {
      GLog.e(TAG, "Invalid leaderboard sort value : " + sort);
      sortInt = SORT_ORDER_ASCENDING;
    }
    return sortInt;
  }

  /**
   * Gets the visibility of the leaderboard; whether it is secret or not.
   * @return false if the leaderboard is visible (not secret), otherwise true 
   */
  public boolean isSecret() {
    if (secret != null && secret.equals("1")) { return true; }
    return false;
  }

  /**
   * Get the bitmap thumbnail for this leaderboard. This is a synchronized call which will return immediately.
   * @return if the thumbnail is ready for download it returns the bitmap. If not it will return null.
   */
  public Bitmap getThumbnail() {
	return getImageFetcher().getBitmap(getThumbnailUrl());
  }

 /**
 * Loads the thumbnail of this leaderboard. This is an asynchronous, non-blocking call. The listener will be called back when the request is done.<br>
 * Pass in the {@link IconDownloadListener} to get the result of the request.
 * 
 * @return return true on a successful implemetation, false means the request could not be fulfilled (ie: no listener, a network error, a permission error, etc).
 */
  public boolean loadThumbnail(final IconDownloadListener listener) {
	getImageFetcher().loadImage(getThumbnailUrl(), listener);
    return true;
  }
}
