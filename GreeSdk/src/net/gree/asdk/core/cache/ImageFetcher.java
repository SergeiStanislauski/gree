/*
 * Copyright (C) 2012 The Android Open Source Project
 * Copyright 2012 GREE, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * NOTE: This file has been modified by GREE, Inc.
 * Modifications are licensed under the License.
 */
package net.gree.asdk.core.cache;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.http.HeaderIterator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.ImageView;

import net.gree.asdk.api.IconDownloadListener;
import net.gree.asdk.core.GLog;
import net.gree.asdk.core.request.BaseClient;
import net.gree.asdk.core.request.BitmapClient;
import net.gree.asdk.core.request.OnResponseCallback;

/**
 * I write three points about ImageFetcher.</br> 1)</br> This class download image as a bitmap and
 * to save a memory or a disk.</br> For example) <code><pre>
  ImageFetcher mImageFetcher;

  mImageFetcher = new ImageFetcher(getActivity());
  ImageCache.Params cacheParams = new ImageCache.Params("Stream");
  cacheParams.mDiskCacheSize = 50 * 1024 *1024;
  cacheParams.mEnableDiskCache = true;
  cacheParams.mMemCacheSize = 3 * 1024 * 1024;
  cacheParams.mEnableMemCache = true;
  ImageCache imageCache = new ImageCache(getActivity(), cacheParams);
  mImageFetcher.setImageCache(imageCache);
  
  mImageFetcher.loadImage(url, imageView, R.drawable.gree_spinner, 100, 100);
</pre></code> After downloading, downloaded image
 * will be set to imageView.</br> </br> 2)</br> Allocate a third of the per-app memory limit to the
 * bitmap memory cache.</br> This value should be chosen carefully based on a number of
 * factors.</br> Refer to the corresponding Android Training class for more discussion:
 * http://developer.android.com/training/displaying-bitmaps/ In this case, we aren't using memory
 * for much else other than this activity and the ImageDetailActivity so a third lets us keep all
 * our sample image thumbnails in memory at once.</br> For example) <code><pre>
     public static int getMemoryClass(Context context) {
        return ((ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE)).getMemoryClass();
    }
    ////////
    cacheParams.memCacheSize = 1024 * 1024 * getMemoryClass(getActivity()) / 3;
</pre></code> You can see more
 * information at <br/>
 * http://developer.android.com/training/displaying-bitmaps/ <br/>
 * https://github.com/JakeWharton/DiskLruCache<br/>
 * <br/>
 * 3)<br/>
 * You should recycle all bitmap inside ImageView and cached bitmap.<br/>
 * Please recycle in onDestroy like this<br/>
 * <code><pre>
  public void onDestroy() {
    mImageFetcher.clearAll();
    mImageFetcher.clearBitmap(mListView);
    super.onDestroy();
    
    mImageFetcher.setImageCache(null);
    mImageFetcher = null;
  }
  
</pre></code>
 */
public class ImageFetcher {
  private static final String TAG = ImageFetcher.class.getSimpleName();

  public static final int IO_BUFFER_SIZE_BYTES = 1 * 1024; // 1KB

  private Context mContext;
  private ImageCache mImageCache = null;
  private boolean mIs3Leg = true;
  private final Hashtable<String, Bitmap> mLoadingImages = new Hashtable<String, Bitmap>(2);
  private RequestQueue mRequestQueue = new RequestQueue();
  private ImageFetchThread mImageFetcherServerThread;

  public ImageFetcher(Context context) {
    mContext = context;
    mImageFetcherServerThread = new ImageFetchThread(mRequestQueue);
    mImageFetcherServerThread.start();
  }

  public ImageFetcher(Context context, boolean is3leg) {
    mContext = context;
    mIs3Leg = is3leg;
    mImageFetcherServerThread = new ImageFetchThread(mRequestQueue);
    mImageFetcherServerThread.start();
  }

  public ImageFetcher setImageCache(ImageCache imageCache) {
    mImageCache = imageCache;
    return this;
  }

  public void loadImage(String url, ImageView imageView, int resId, int reqWidth, int reqHeight) {
    GLog.d(TAG, "request       : " + url);
    if (!isValidUrl(url)) {
      GLog.e(TAG, "invalid       : " + url);
      return;
    }
    String resKey = String.valueOf(resId) + String.valueOf(reqWidth) + String.valueOf(reqHeight);
    if (!mLoadingImages.containsKey(resKey)) {
      try {
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), resId);
        if (bitmap != null) {
          int width = bitmap.getWidth();
          int height = bitmap.getHeight();
          Matrix matrix = new Matrix();
          float scaleWidth;
          float scaleHeight;
          if (width > height) {
            scaleWidth = ((float) reqWidth) / width;
            scaleHeight = ((float) reqHeight) / width;
          } else {
            scaleWidth = ((float) reqWidth) / height;
            scaleHeight = ((float) reqHeight) / height;
          }
          matrix.postScale(scaleWidth, scaleHeight);
          Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
          mLoadingImages.put(resKey, resizedBitmap);
        }
      } catch (NotFoundException e) {
        GLog.printStackTrace(TAG, e);
      }
    }
    loadImage(url, imageView, mLoadingImages.get(resKey), reqWidth, reqHeight);
  }

  public void clearBitmap(View view) {
    if (view instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) view;
      int childNum = group.getChildCount();
      for (int i = 0; i < childNum; i++) {
        View child = group.getChildAt(i);
        if (child instanceof ViewGroup) {
          clearBitmap(child);
        } else if (child instanceof ImageView) {
          ImageView imageView = (ImageView) child;
          Drawable drawable = imageView.getDrawable();
          if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (bitmap != null && !bitmap.isRecycled()) {
              bitmap.recycle();
            }
          }
          imageView.setImageDrawable(null);
        }
      }
    }
  }

  public void clearAll() {
    if (mImageCache != null) {
      mImageCache.close();
    }
    mImageFetcherServerThread.requestFinish();
  }

  private boolean isValidUrl(String url) {
    if (TextUtils.isEmpty(url)) {
      return false;
    }
    if (!url.startsWith("http")) {
      return false;
    }
    return true;
  }

  private void loadImage(String url, ImageView imageView, Bitmap loadingBitmap, int reqWidth,
      int reqHeight) {
    Bitmap bitmap = null;
    if (mImageCache != null) {
      bitmap = mImageCache.getFromMemCache(url);
    }

    if (cancelPotentialWork(url, imageView) && bitmap == null) {
      final ImageViewBitmapWorkerTask task =
          new ImageViewBitmapWorkerTask(imageView, reqWidth, reqHeight);
      Request req = new Request(url, task);
      final AsyncDrawable asyncDrawable =
          new AsyncDrawable(mContext.getResources(), loadingBitmap, req);
      if (imageView != null) {
        imageView.setImageDrawable(asyncDrawable);
      }

      mRequestQueue.putRequest(req);
    } else if (bitmap != null && imageView != null) {
      imageView.setImageBitmap(bitmap);
    }
  }

  private BitmapWorkerTask mBitmapTask;

  /**
   * Loads the image from the cache or fetches the image.
   * 
   * @param url Url of the image
   * @param listener
   */
  public void loadImage(String url, IconDownloadListener listener) {
    Bitmap bitmap = null;
    if (mImageCache != null) {
      // tries to get the bitmap from memory first
      bitmap = mImageCache.getFromMemCache(url);
    }

    if (bitmap != null) {
      if (listener != null) {
        listener.onSuccess(bitmap);
      }
    } else {
      // declare a new bitmap worker task
      mBitmapTask = new BitmapWorkerTask(url, 0, 0, listener);

      // download and put the bitmap on the cache
      Request req = new Request(url, mBitmapTask);
      mRequestQueue.putRequest(req);
    }
  }

  /**
   * Queries the cache for the Bitmap, returns it if it is found
   * 
   * @param key the key that represents the Bitmap on the cache
   * @return Bitmap from the cache if discovered, null if not found
   */
  public Bitmap getBitmap(String key) {
    // check to see if the ImageChace is null
    if (mImageCache == null) {
      GLog.e(TAG, "ImageChace is null. Must instantiate first.");
      return null;
    }
    // looks for the Bitmap in on the disk
    Bitmap b = mImageCache.getFromDisk(key);
    if (b != null) {
      return b;
    }

    // looks for the Bitmap on the cache
    b = mImageCache.getFromMemCache(key);
    // returns the image or a null value
    return b;
  }

  private boolean cancelPotentialWork(Object data, ImageView imageView) {
    final Request request = getBitmapRequest(imageView);
    if (request != null) {
      final ImageViewBitmapWorkerTask bitmapWorkerTask =
          (ImageViewBitmapWorkerTask) request.getTask();

      if (bitmapWorkerTask != null) {
        final Object bitmapData = bitmapWorkerTask.data;
        if (bitmapData == null || !bitmapData.equals(data)) {
          bitmapWorkerTask.cancel(true);
          mRequestQueue.removeRequest(request);
          GLog.d(TAG, "cancelled " + String.valueOf(bitmapData));
        } else {
          return false;
        }
      }
    }
    return true;
  }

  private static Request getBitmapRequest(ImageView imageView) {
    if (imageView != null) {
      final Drawable drawable = imageView.getDrawable();
      if (drawable instanceof AsyncDrawable) {
        final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
        return asyncDrawable.getRequest();
      }
    }
    return null;
  }

  private static class AsyncDrawable extends BitmapDrawable {
    private Request mRequest;

    public AsyncDrawable(Resources res, Bitmap bitmap, Request request) {
      super(res, bitmap);
      mRequest = request;
    }

    public Request getRequest() {
      return mRequest;
    }
  }

  private class ImageDownloader {
    private volatile Bitmap mBitmap;
    private boolean mIs3Leg = true;

    public ImageDownloader(boolean is3leg) {
      mIs3Leg = is3leg;
    }

    public Bitmap download(final String url) {
      mBitmap = null;
      BitmapClient client = new BitmapClient();
      HashMap<String, String> headerParams = new HashMap<String, String>();
      headerParams.put("cookie", CookieManager.getInstance().getCookie(url));
      GLog.d(TAG, "download start " + url);
      OnResponseCallback<Bitmap> callback = new OnResponseCallback<Bitmap>() {
        @Override
        public void onSuccess(int responseCode, HeaderIterator headers, Bitmap response) {
          mBitmap = response;
          GLog.d(TAG, "downlaod success " + url);
        }

        @Override
        public void onFailure(int responseCode, HeaderIterator headers, String response) {
          GLog.d(TAG, "downlaod fail " + url);
        }
      };
      if (mIs3Leg == true) {
        client.oauthForceSync(url, BaseClient.METHOD_GET, headerParams, callback);
      } else {
        client.oauthForceSyncNoneAuth(url, BaseClient.METHOD_GET, headerParams, callback);
      }
      return mBitmap;
    }
  }

  private Bitmap processBitmap(String url, int reqWidth, int reqHeight) {
    ImageDownloader downloader = new ImageDownloader(mIs3Leg);
    Bitmap bitmap = downloader.download(url);
    if (bitmap == null) {
      return null;
    }
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    Matrix matrix = new Matrix();
    float scaleWidth;
    float scaleHeight;
    if (width > height) {
      scaleWidth = ((float) reqWidth) / width;
      scaleHeight = ((float) reqHeight) / width;
    } else {
      scaleWidth = ((float) reqWidth) / height;
      scaleHeight = ((float) reqHeight) / height;
    }
    matrix.postScale(scaleWidth, scaleHeight);
    Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    GLog.d(TAG, "download from : " + url);
    return resizedBitmap;
  }

  private Bitmap processNoScaleBitmap(String url, int reqWidth, int reqHeight) {
    ImageDownloader downloader = new ImageDownloader(mIs3Leg);
    Bitmap bitmap = downloader.download(url);
    if (bitmap == null) {
      return null;
    }

    GLog.d(TAG, "download from : " + url);

    if (reqWidth > 0 && reqHeight > 0) {
      return bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, true);
    } else {
      return bitmap;
    }
  }

  private class ImageViewBitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
    private Object data;
    private int mWidth;
    private int mHeight;
    private final WeakReference<ImageView> mImageViewReference;

    public ImageViewBitmapWorkerTask(ImageView imageView, int reqWidth, int reqHeight) {
      mImageViewReference = new WeakReference<ImageView>(imageView);
      mWidth = reqWidth;
      mHeight = reqHeight;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
      data = params[0];
      final String dataString = String.valueOf(data);
      Bitmap bitmap = null;

      if (mImageCache != null && !isCancelled() && getAttachedImageView() != null) {
        bitmap = mImageCache.getFromDisk(dataString);

        if (bitmap == null) {
          bitmap = processBitmap(dataString, mWidth, mHeight);
        }
      }


      if (bitmap != null && mImageCache != null) {
        mImageCache.put(dataString, bitmap);
      }

      return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
      final ImageView imageView = getAttachedImageView();
      if (isCancelled()) {
        bitmap = null;
        return;
      }
      mRequestQueue.notifyCompleteRequest();

      if (bitmap != null && imageView != null) {
        imageView.setImageBitmap(bitmap);
      }
    }

    private ImageView getAttachedImageView() {
      final ImageView imageView = mImageViewReference.get();
      final Request request = getBitmapRequest(imageView);
      if (request != null) {
        final ImageViewBitmapWorkerTask bitmapWorkerTask =
            (ImageViewBitmapWorkerTask) request.getTask();

        if (this == bitmapWorkerTask && imageView != null) {
          return imageView;
        }
      }
      return null;
    }
  }

  private class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
    private int mWidth;
    private int mHeight;
    private String mUrl;
    private IconDownloadListener mListener;

    public BitmapWorkerTask(String url, int reqWidth, int reqHeight, IconDownloadListener listener) {
      mUrl = url;
      mWidth = reqWidth;
      mHeight = reqHeight;
      mListener = listener;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
      Bitmap bitmap = null;

      if (mImageCache != null && !isCancelled()) {
        bitmap = mImageCache.getFromDisk(mUrl);
      }

      if (bitmap == null) {
        bitmap = processNoScaleBitmap(mUrl, mWidth, mHeight);
      }

      if (bitmap != null && mImageCache != null) {
        mImageCache.put(mUrl, bitmap);
      }
      return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
      if (isCancelled()) {
        return;
      }
      mRequestQueue.notifyCompleteRequest();
      if (mListener != null) {
        mListener.onSuccess(bitmap);
      }
    }
  }
}
