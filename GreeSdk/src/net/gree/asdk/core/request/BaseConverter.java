/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import android.net.ParseException;

import net.gree.asdk.core.GLog;

/**
 * BaseConvert is responsible for entityTobyte, leave a abstract function byteToTarget and expose a
 * public call convert
 * 
 * @param <T>
 */

public abstract class BaseConverter<T> implements IConverter<T> {
  private static final String TAG = BaseConverter.class.getSimpleName();

  protected abstract T byteToTarget(final byte[] bytes);

  @Override
  public T convert(HttpResponse response) {
    if (response == null) {
      return null;
    }
    HttpEntity entity = response.getEntity();
    if (entity == null) {
      return null;
    }
    byte[] bytes = null;
    Header ceheader = entity.getContentEncoding();
    if (ceheader != null) {
      HeaderElement[] codecs = ceheader.getElements();
      for (int i = 0; i < codecs.length; i++) {
        if (codecs[i].getName().equalsIgnoreCase("gzip")) {
          try {
            // try gzip way
            bytes = entityToByte(new GzipDecompressingEntity(entity));
            break;
          } catch (ParseException e) {
            RequestLogger.getInstance().printStackTrace(TAG, e);
          }
        }
      }
    }
    //if gzip is not success, then try no gzip
    if (bytes == null) {
      bytes = entityToByte(entity);
    }

    // convert bytes to result
    T result = null;
    if (bytes == null) {
      result =  null;
    }else{
      result = byteToTarget(bytes);
    }
    return result;
  }

  protected byte[] entityToByte(final HttpEntity entity) {
    if (entity == null) {
      RequestLogger.getInstance().w(TAG, "entity is null, fail to convert to Byte[]");
      return null;
    }
    InputStream is = null;
    // this is a guess on the size of no info about it (gzip)
    int LENGTH4 = 4 * 1024;
    ByteArrayOutputStream baos = null;
    try {
      is = entity.getContent();
      int i = (int) entity.getContentLength();
      // decide how long is the length
      if (i < 0) {
        i = LENGTH4;
      }
      baos = new ByteArrayOutputStream(i);
      byte[] tmp = new byte[i];
      int l = 0;

      while ((l = is.read(tmp)) != -1) {
        baos.write(tmp, 0, l);
      }
    } catch (IllegalStateException e) {
      RequestLogger.getInstance().printStackTrace(TAG, e);
    } catch (IOException e) {
      RequestLogger.getInstance().printStackTrace(TAG, e);
    } finally {
      try {
        if (is != null) {
          is.close();
        }
      } catch (IOException e) {
        RequestLogger.getInstance().printStackTrace(TAG, e);
      }
    }
    byte[] result = baos.toByteArray();
    try {
      if (baos != null) {
        baos.close();
      }
    } catch (IOException e) {
      GLog.printStackTrace(TAG, e);
    }
    return result;
  }
}
