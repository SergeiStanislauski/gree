/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.asdk.core.request;

import org.apache.http.HeaderIterator;
import org.apache.http.HttpStatus;

import net.gree.asdk.core.GLog;
import net.gree.asdk.core.Injector;
import net.gree.asdk.core.request.Constants.OAUTH_TYPE;
import net.gree.vendor.com.google.gson.Gson;
import net.gree.vendor.com.google.gson.JsonSyntaxException;

/**
 * ResponseHandler will encapsulate the logic on how to deal with call back
 * 
 * @param <T>
 */
public class ResponseHandler<T> {
  private static final String TAG = ResponseHandler.class.getSimpleName();

  public ResponseHandler(OnResponseCallback<T> listener) {
    mListener = listener;
    gson = Injector.getInstance(Gson.class);
  }
  /**
   * Set the oauth type
   * @param oAuthType
   * @return
   */
  
  public ResponseHandler<T> oauth(OAUTH_TYPE oAuthType){
    mOAuthType = oAuthType;
    return this;
  }

  protected OnResponseCallback<T> mListener;
  protected OAUTH_TYPE mOAuthType =  OAUTH_TYPE._3LEGGED;
  protected Gson gson;

// this is the call back point for upper layer
  public void onResponse(ResponseHolder<T> holder) {
    if (holder == null) {
      String reason = "holder is null";
      onFailure(HttpStatus.SC_BAD_REQUEST, null, null, reason);
      return;
    }
    if (HttpStatus.SC_OK <= holder.mStatusCode && holder.mStatusCode < HttpStatus.SC_BAD_REQUEST) {
      onSuccess(holder.mStatusCode, holder.getHeaderIterator(), holder.mResult);
    } else {
      // deal with string, still need send reponseString back;
      if (holder.mResult instanceof String) {
        String strResult = (String) holder.mResult;
        if (mOAuthType == OAUTH_TYPE._3LEGGED && strResult != null) {
          try {
            FailureResponse failure = gson.fromJson(strResult, FailureResponse.class);
            if (failure != null) {
              failure.handleError();
            }
          } catch (JsonSyntaxException e) {
            GLog.w(TAG,
                "Failed to parse failure response: " + strResult + ", exception: " + e.getMessage());
          }
        }
        onFailure(holder.mStatusCode, holder.getHeaderIterator(), strResult, holder.mReasonPhrase);
        return;

      } else { // this should be image or binary
        onFailure(holder.mStatusCode, holder.getHeaderIterator(), null, holder.mReasonPhrase);
        return;
      }
    }
  }

  protected void onSuccess(int responseCode, HeaderIterator headers, T response) {
    if (mListener != null) {
      mListener.onSuccess(responseCode, headers, response);
    }
  }

  protected void onFailure(int responseCode, HeaderIterator headers, String response, String reason) {
    if (mListener != null) {
      mListener.onFailure(responseCode, headers, response);
    }
  }
}
