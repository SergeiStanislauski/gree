/*
 * Copyright 2012 GREE, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.gree.platformsample;

import java.util.TreeMap;

import net.gree.asdk.api.GreePlatform;
import net.gree.asdk.api.ui.AsyncErrorDialog;
import net.gree.asdk.api.ui.RequestDialog;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Request Dialog Activity
 * 
 */
public class RequestDialogActivity extends BaseActivity {
  private static final String TAG = "RequestDialogActivity";
  private EditText mRequestTitleText;
  private EditText mRequestBodyText;
  private RadioGroup mListTypeGroup;
  private EditText mAttrKey1Text;
  private EditText mAttrKey2Text;
  private EditText mAttrKey3Text;
  private EditText mAttrValue1Text;
  private EditText mAttrValue2Text;
  private EditText mAttrValue3Text;
  private RequestDialog mRequestDialog;
  private Handler mHandler;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GreePlatform.activityOnCreate(this, false);
    setCustomizeStyle(true, R.layout.requestdialog);
    getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    mRequestDialog = new RequestDialog(this);

    mHandler = new Handler() {
      public void handleMessage(Message message) {
        switch (message.what) {
          case RequestDialog.OPENED:
            Toast.makeText(getApplicationContext(), "Request Dialog opened.", Toast.LENGTH_SHORT)
                .show();
            Log.i(TAG, "Reqest Dialog opened.");
            break;
          case RequestDialog.CLOSED:
            if (message.obj != null) {
              Toast.makeText(getApplicationContext(),
                  "Request Dialog closed. result:[" + message.obj.toString() + "]",
                  Toast.LENGTH_SHORT).show();
              Log.i(TAG, "Request Dialog closed. result:[" + message.obj.toString() + "]");
            } else {
              Toast.makeText(getApplicationContext(), "Request Dialog closed. result:[Nothing]",
                  Toast.LENGTH_SHORT).show();
              Log.i(TAG, "Request Dialog closed. result:[Nothing]");
            }
            break;
          default:
        }
      }
    };

    declearProfile();

    mListTypeGroup = (RadioGroup) findViewById(R.id.listTypeGroup);
    // check radio button specified by ID
    mListTypeGroup.check(R.id.listType_all);

    mRequestTitleText = (EditText) findViewById(R.id.requestTitleEdit);
    mRequestBodyText = (EditText) findViewById(R.id.requestBodyEdit);
    mAttrKey1Text = (EditText) findViewById(R.id.attrKey1);
    mAttrKey2Text = (EditText) findViewById(R.id.attrKey2);
    mAttrKey3Text = (EditText) findViewById(R.id.attrKey3);
    mAttrValue1Text = (EditText) findViewById(R.id.attrValue1);
    mAttrValue2Text = (EditText) findViewById(R.id.attrValue2);
    mAttrValue3Text = (EditText) findViewById(R.id.attrValue3);
    
    // sets the Action Listeners to cause the EditTexts to move through correctly 
    setFocusListeners();

    // LocalNotification start button Setting
    Button requestButton = (Button) findViewById(R.id.requestSendButton);
    requestButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        if (AsyncErrorDialog.shouldShowErrorDialog(v.getContext())) {
          AsyncErrorDialog dialog = new AsyncErrorDialog(v.getContext());
          dialog.show();
          return;
        }

        // show list
        TreeMap<String, Object> map = new TreeMap<String, Object>();
        JSONObject attr = new JSONObject();

        String title = mRequestTitleText.getText().toString();
        String body = mRequestBodyText.getText().toString();
        String attrKey1 = mAttrKey1Text.getText().toString();
        String attrKey2 = mAttrKey2Text.getText().toString();
        String attrKey3 = mAttrKey3Text.getText().toString();
        String attrValue1 = mAttrValue1Text.getText().toString();
        String attrValue2 = mAttrValue2Text.getText().toString();
        String attrValue3 = mAttrValue3Text.getText().toString();

        String listType;

        if (mListTypeGroup.getCheckedRadioButtonId() == R.id.listType_all) {
          listType = "all";
        } else if (mListTypeGroup.getCheckedRadioButtonId() == R.id.listType_joined) {
          listType = "joined";
        } else {
          listType = "specified";
        }

        if ((title != null) && (title.length() != 0)) {
          map.put("title", title.toString());
        }

        if ((body != null) && (body.length() != 0)) {
          map.put("body", body.toString());
        }
/*
        if ((redirectUrl != null) && (redirectUrl.length() != 0)) {
          map.put("redirect_url", redirectUrl.toString());
        }
*/
        map.put("list_type", listType.toString());

        if ((attrKey1 != null) && (attrKey1.length() != 0)) {
          try {
            attr.put(attrKey1, attrValue1);
          } catch (JSONException e) {
            Log.e(TAG, "attr1 set failed.");
          }
        }

        if ((attrKey2 != null) && (attrKey2.length() != 0)) {
          try {
            attr.put(attrKey2, attrValue2);
          } catch (JSONException e) {
            Log.e(TAG, "attr2 set failed.");
          }
        }

        if ((attrKey3 != null) && (attrKey3.length() != 0)) {
          try {
            attr.put(attrKey3, attrValue3);
          } catch (JSONException e) {
            Log.e(TAG, "attr3 set failed.");
          }
        }

        if (attr.length() > 0) {
          map.put("attrs", attr);
          Log.d(TAG, "set attr:" + attr.toString());
        }

        if (mRequestDialog == null) {
          mRequestDialog = new RequestDialog(v.getContext());
        }

        mRequestDialog.setParams(map);
        mRequestDialog.setHandler(mHandler);
        mRequestDialog.show();
      }
    });
  }
  
  /** 
   * sets the EditTexts that flow incorrectly to focus on the correct "next" field 
  **/
  private void setFocusListeners(){
    mRequestBodyText.setOnEditorActionListener(EditTextActionListener);
    mAttrKey1Text.setOnEditorActionListener(EditTextActionListener);
    mAttrKey2Text.setOnEditorActionListener(EditTextActionListener);
    mAttrKey3Text.setOnEditorActionListener(EditTextActionListener);
    mAttrValue1Text.setOnEditorActionListener(EditTextActionListener);
    mAttrValue2Text.setOnEditorActionListener(EditTextActionListener);

  }
  
  /**
   * Implements a new Action listener when switching focus via the Action button or a hard ime
   */
  private EditText.OnEditorActionListener EditTextActionListener = new EditText.OnEditorActionListener(){
    
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
      Log.w(TAG, "EVENT: " + event + " Action - > "+actionId    );
      if (event != null) {
        // because we only want the code to execute once, and it executes on down as well
        if(event.getAction() == KeyEvent.ACTION_UP){
          if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
            forceFocusNext(v);
            return true;
          }          
        }
        // for hard IME in Android 2.2 phones special exception
        if(event.getKeyCode() == KeyEvent.KEYCODE_ENTER && actionId == 0){
          setFocusNext(v);
          return true;
        }
        
      }
      // if null because of a custom keyboard let's test the action
      if(isEditorInfoNextButton(actionId)){
        forceFocusNext(v);
        return true;
      }      
      return false;
   }

    private void setFocusNext(View view) {
      //switch on the r.id to determine which edittext gets focus next
      switch(view.getId()){
        default:
          break;              
      }     
    }

    /** Sets (forces) the Focus next to the EditText we want **/
    private void forceFocusNext(TextView view) {
      Log.d(TAG, "6");
      //switch on the r.id to determine which edittext gets focus next
      int id = view.getId();
	if (id == R.id.requestBodyEdit) {
		mAttrKey1Text.requestFocus();
	} else if (id == R.id.attrKey1) {
		mAttrValue1Text.requestFocus();
	} else if (id == R.id.attrValue1) {
		mAttrKey2Text.requestFocus();
	} else if (id == R.id.attrKey2) {
		mAttrValue2Text.requestFocus();
	} else if (id == R.id.attrValue2) {
		mAttrKey3Text.requestFocus();
	} else if (id == R.id.attrKey3) {
		mAttrValue3Text.requestFocus();
	} else {
	}      
    }

    /** Scans for IME options that correlate to an enter/next key being pressed **/
    private boolean isEditorInfoNextButton(int action) {
      // switch on the IME constant to determine if the input is valid
      switch(action){
        case EditorInfo.IME_ACTION_DONE:
          return true;
        case EditorInfo.IME_ACTION_GO:
          return true;
        case EditorInfo.IME_ACTION_NEXT:
          return true;
        case EditorInfo.IME_ACTION_SEARCH:
          return true;
        case EditorInfo.IME_ACTION_SEND:
          return true;
        case EditorInfo.IME_MASK_ACTION:
          return true;
      }
      return false;
    }
  };

  @Override
  public void onOpenHelp(View v){
    Bundle bundle = new Bundle();
    bundle.putString("ITEM", getResources().getString(R.string.request_title));
    Intent helpIntent = new Intent(this, HelpActivity.class);
    helpIntent.putExtras(bundle);
    this.startActivity(helpIntent);
  }

  @Override
  public void onDetachedFromWindow() {
    try {
      super.onDetachedFromWindow();
    } catch (IllegalArgumentException e) {
      Log.e(TAG, "IllegalArgumentException");
    }
  }

  @Override
  protected void sync(boolean fromStart) {
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!tryLoginAndLoadProfilePage()) { return; }
    setUpBackButton();
  }

}
